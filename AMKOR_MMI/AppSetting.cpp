// AppSetting.cpp: implementation of the CAppSetting class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "AppSetting.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// CAppSettingGC
class CAppSettingGC
{
public:
	CAppSettingGC() {}
	~CAppSettingGC() { delete CAppSetting::m_pThis; CAppSetting::m_pThis = NULL; }
};
CAppSettingGC s_GC;

//////////////////////////////////////////////////////////////////////////
// CAppSetting
CAppSetting* CAppSetting::m_pThis = NULL;
CAppSetting::CAppSetting()
{
}

CAppSetting::~CAppSetting()
{
}

CAppSetting* CAppSetting::Instance()
{
	if( !m_pThis )
		m_pThis = new CAppSetting;
	return m_pThis;
}

// Get Group Number
const int CAppSetting::GetGroupNumber() const
{
	return m_nGroupNumber;
}

const int CAppSetting::GetLogicalStationNumber(int nGroupNum) const
{
	int nRet = 0;

	if(nGroupNum == 1) nRet = m_nLogicalStationNumber_G1;
	else if(nGroupNum == 2) nRet = m_nLogicalStationNumber_G2;
	else if(nGroupNum == 3) nRet = m_nLogicalStationNumber_G3;

	return nRet;
}

// ZPL PRINTER
const IPv4Info& CAppSetting::GetIP_ZPL_Printer(int nPrintNum)
{
	if(nPrintNum == 0) return m_ZPLPrinter01_IP;
	else if(nPrintNum == 1) return m_ZPLPrinter02_IP;
	else if(nPrintNum == 2) return m_ZPLPrinter03_IP;
	else if(nPrintNum == 3) return m_ZPLPrinter04_IP;
	else if(nPrintNum == 4) return m_ZPLPrinter05_IP;

	return m_ZPLPrinter01_IP;
}

const DWORD CAppSetting::GetIP_DWORD_ZPL_Printer(int nPrintNum)
{
	if(nPrintNum == 0) return m_ZPLPrinter01_IP.GetIPAddressDWORD();
	else if(nPrintNum == 1) return m_ZPLPrinter02_IP.GetIPAddressDWORD();
	else if(nPrintNum == 2) return m_ZPLPrinter03_IP.GetIPAddressDWORD();
	else if(nPrintNum == 3) return m_ZPLPrinter04_IP.GetIPAddressDWORD();
	else if(nPrintNum == 4) return m_ZPLPrinter05_IP.GetIPAddressDWORD();

	return m_ZPLPrinter01_IP.GetIPAddressDWORD();
}

void CAppSetting::SetIP_ZPL_Printer(int nPrintNum, DWORD dwAddress)
{
	if(nPrintNum == 0) m_ZPLPrinter01_IP.SetIPAddress( dwAddress );
	else if(nPrintNum == 1) m_ZPLPrinter02_IP.SetIPAddress( dwAddress );
	else if(nPrintNum == 2) m_ZPLPrinter03_IP.SetIPAddress( dwAddress );
	else if(nPrintNum == 3) m_ZPLPrinter04_IP.SetIPAddress( dwAddress );
	else if(nPrintNum == 4) m_ZPLPrinter05_IP.SetIPAddress( dwAddress );
}

const UINT CAppSetting::GetPort_ZPL_Printer(int nPrintNum) const
{
	if(nPrintNum == 0) return m_ZPLPrinter01_PORT;
	else if(nPrintNum == 1) return m_ZPLPrinter02_PORT;
	else if(nPrintNum == 2) return m_ZPLPrinter03_PORT;
	else if(nPrintNum == 3) return m_ZPLPrinter04_PORT;
	else if(nPrintNum == 4) return m_ZPLPrinter05_PORT;

	return m_ZPLPrinter01_PORT;
}

void CAppSetting::SetPort_ZPL_Printer(int nPrintNum, UINT nPort)
{
	if(nPrintNum == 0) m_ZPLPrinter01_PORT = nPort;
	if(nPrintNum == 1) m_ZPLPrinter02_PORT = nPort;
	if(nPrintNum == 2) m_ZPLPrinter03_PORT = nPort;
	if(nPrintNum == 3) m_ZPLPrinter04_PORT = nPort;
	if(nPrintNum == 4) m_ZPLPrinter05_PORT = nPort;
}

// 현재 실행화일 경로 가져오자.
CString CAppSetting::GetExecuteDirectory()
{
	CString strFolderPath;

	// 현재 프로그램의 실행 파일이 있는 폴더명을 추출함
	::GetModuleFileName(NULL, strFolderPath.GetBuffer(MAX_PATH), MAX_PATH);
	strFolderPath.ReleaseBuffer();
	if (strFolderPath.Find('\\') != -1)
	{
		for (int i = strFolderPath.GetLength() - 1; i >= 0; i--) 
		{
			TCHAR ch = strFolderPath[i];
			strFolderPath.Delete(i);
			if (ch == '\\') break; 
		}
	}
	return strFolderPath;
}

BOOL CAppSetting::Load()
{
	CString strPath = GetExecuteDirectory();
	strPath += _T("\\Setting\\");
	strPath += _T("Config.ini");
	
	TCHAR temp[MAX_PATH];

	// Get Group Number
	::GetPrivateProfileString( _T("SYSTEM_INFO"), _T("GROUP_NUMBER"), _T("1"), temp, MAX_PATH, strPath );
	m_nGroupNumber = _tcstoul( temp, NULL, 10 );
	::GetPrivateProfileString( _T("SYSTEM_INFO"), _T("LOGICAL_STATION_NUMBER_G1"), _T("1"), temp, MAX_PATH, strPath );
	m_nLogicalStationNumber_G1 = _tcstoul( temp, NULL, 10 );
	::GetPrivateProfileString( _T("SYSTEM_INFO"), _T("LOGICAL_STATION_NUMBER_G2"), _T("1"), temp, MAX_PATH, strPath );
	m_nLogicalStationNumber_G2 = _tcstoul( temp, NULL, 10 );
	::GetPrivateProfileString( _T("SYSTEM_INFO"), _T("LOGICAL_STATION_NUMBER_G3"), _T("1"), temp, MAX_PATH, strPath );
	m_nLogicalStationNumber_G3 = _tcstoul( temp, NULL, 10 );

	// ZPL PRINTER 과 연결할 TCP 정보
	::GetPrivateProfileString( _T("ZPL_PRINTER_01"), _T("IP"), _T("127.0.0.1"), temp, MAX_PATH, strPath );
	m_ZPLPrinter01_IP.SetIPAddress( temp );
	::GetPrivateProfileString( _T("ZPL_PRINTER_01"), _T("PORT"), _T("9100"), temp, MAX_PATH, strPath );
	m_ZPLPrinter01_PORT = _tcstoul( temp, NULL, 10 );
	::GetPrivateProfileString( _T("ZPL_PRINTER_02"), _T("IP"), _T("127.0.0.1"), temp, MAX_PATH, strPath );
	m_ZPLPrinter02_IP.SetIPAddress( temp );
	::GetPrivateProfileString( _T("ZPL_PRINTER_02"), _T("PORT"), _T("9100"), temp, MAX_PATH, strPath );
	m_ZPLPrinter02_PORT = _tcstoul( temp, NULL, 10 );
	::GetPrivateProfileString( _T("ZPL_PRINTER_03"), _T("IP"), _T("127.0.0.1"), temp, MAX_PATH, strPath );
	m_ZPLPrinter03_IP.SetIPAddress( temp );
	::GetPrivateProfileString( _T("ZPL_PRINTER_03"), _T("PORT"), _T("9100"), temp, MAX_PATH, strPath );
	m_ZPLPrinter03_PORT = _tcstoul( temp, NULL, 10 );
	::GetPrivateProfileString( _T("ZPL_PRINTER_04"), _T("IP"), _T("127.0.0.1"), temp, MAX_PATH, strPath );
	m_ZPLPrinter04_IP.SetIPAddress( temp );
	::GetPrivateProfileString( _T("ZPL_PRINTER_04"), _T("PORT"), _T("9100"), temp, MAX_PATH, strPath );
	m_ZPLPrinter04_PORT = _tcstoul( temp, NULL, 10 );
	::GetPrivateProfileString( _T("ZPL_PRINTER_05"), _T("IP"), _T("127.0.0.1"), temp, MAX_PATH, strPath );
	m_ZPLPrinter05_IP.SetIPAddress( temp );
	::GetPrivateProfileString( _T("ZPL_PRINTER_05"), _T("PORT"), _T("9100"), temp, MAX_PATH, strPath );
	m_ZPLPrinter05_PORT = _tcstoul( temp, NULL, 10 );

	return TRUE;
}

BOOL CAppSetting::Save()
{
	CString strPath = GetExecuteDirectory();
	strPath += _T("\\Setting\\");
	strPath += _T("Config.ini");

	CString strTemp;

	// Get Group Number
	strTemp.Format(_T("%d"), m_nGroupNumber);
	::WritePrivateProfileString( _T("SYSTEM_INFO"), _T("GROUP_NUMBER"), strTemp, strPath );
	strTemp.Format(_T("%d"), m_nLogicalStationNumber_G1);
	::WritePrivateProfileString( _T("SYSTEM_INFO"), _T("LOGICAL_STATION_NUMBER_G1"), strTemp, strPath );
	strTemp.Format(_T("%d"), m_nLogicalStationNumber_G2);
	::WritePrivateProfileString( _T("SYSTEM_INFO"), _T("LOGICAL_STATION_NUMBER_G2"), strTemp, strPath );
	strTemp.Format(_T("%d"), m_nLogicalStationNumber_G3);
	::WritePrivateProfileString( _T("SYSTEM_INFO"), _T("LOGICAL_STATION_NUMBER_G3"), strTemp, strPath );

	// HIBIM 과 연결할 TCP 정보
	::WritePrivateProfileString( _T("ZPL_PRINTER_01"), _T("IP"), m_ZPLPrinter01_IP.GetIPAddressString(), strPath );
	strTemp.Format(_T("%d"), m_ZPLPrinter01_PORT);
	::WritePrivateProfileString( _T("ZPL_PRINTER_01"), _T("PORT"), strTemp, strPath );

	::WritePrivateProfileString( _T("ZPL_PRINTER_02"), _T("IP"), m_ZPLPrinter02_IP.GetIPAddressString(), strPath );
	strTemp.Format(_T("%d"), m_ZPLPrinter02_PORT);
	::WritePrivateProfileString( _T("ZPL_PRINTER_02"), _T("PORT"), strTemp, strPath );

	::WritePrivateProfileString( _T("ZPL_PRINTER_03"), _T("IP"), m_ZPLPrinter03_IP.GetIPAddressString(), strPath );
	strTemp.Format(_T("%d"), m_ZPLPrinter03_PORT);
	::WritePrivateProfileString( _T("ZPL_PRINTER_03"), _T("PORT"), strTemp, strPath );

	::WritePrivateProfileString( _T("ZPL_PRINTER_04"), _T("IP"), m_ZPLPrinter04_IP.GetIPAddressString(), strPath );
	strTemp.Format(_T("%d"), m_ZPLPrinter04_PORT);
	::WritePrivateProfileString( _T("ZPL_PRINTER_04"), _T("PORT"), strTemp, strPath );

	::WritePrivateProfileString( _T("ZPL_PRINTER_05"), _T("IP"), m_ZPLPrinter05_IP.GetIPAddressString(), strPath );
	strTemp.Format(_T("%d"), m_ZPLPrinter05_PORT);
	::WritePrivateProfileString( _T("ZPL_PRINTER_05"), _T("PORT"), strTemp, strPath );

	return TRUE;
}