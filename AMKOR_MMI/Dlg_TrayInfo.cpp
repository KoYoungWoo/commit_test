// Dlg_TrayInfo.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "AMKOR_MMI.h"
#include "Dlg_TrayInfo.h"
#include "afxdialogex.h"


// CDlg_TrayInfo 대화 상자입니다.

IMPLEMENT_DYNAMIC(CDlg_TrayInfo, CDialogEx)

CDlg_TrayInfo::CDlg_TrayInfo(CWnd* pParent /*=NULL*/)
	: CDialogEx(CDlg_TrayInfo::IDD, pParent)
{
	m_bCtrl_FirstLoad = FALSE;
	m_nSelTrayIndex = 0;
	m_nCurPageNum = 0;
}

CDlg_TrayInfo::~CDlg_TrayInfo()
{
}

void CDlg_TrayInfo::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_GC_TI_1, m_GC_1);
	DDX_Control(pDX, IDC_GB_TI_1, m_gb_1);
	DDX_Control(pDX, IDC_GB_TI_2, m_gb_2);
	DDX_Control(pDX, IDC_ST_TI_1, m_st_1);
	DDX_Control(pDX, IDC_ST_TI_2, m_st_2);
	DDX_Control(pDX, IDC_ST_TI_3, m_st_3);
	DDX_Control(pDX, IDC_ST_TI_4, m_st_4);
	DDX_Control(pDX, IDC_ST_TI_5, m_st_5);
	DDX_Control(pDX, IDC_EDIT_TI_1, m_edit_1);
	DDX_Control(pDX, IDC_EDIT_TI_2, m_edit_2);
	DDX_Control(pDX, IDC_EDIT_TI_3, m_edit_3);
	DDX_Control(pDX, IDC_EDIT_TI_4, m_edit_4);
	DDX_Control(pDX, IDC_EDIT_TI_5, m_edit_5);
	DDX_Control(pDX, IDC_BTN_TI_1, m_btn_1);
	DDX_Control(pDX, IDC_BTN_TI_2, m_btn_2);
	DDX_Control(pDX, IDC_BTN_TI_3, m_btn_3);
	DDX_Control(pDX, IDC_BTN_TI_4, m_btn_4);
	DDX_Control(pDX, IDC_BTN_TI_5, m_btn_5);
	DDX_Control(pDX, IDC_BTN_TI_6, m_btn_6);
	DDX_Control(pDX, IDC_BTN_TI_7, m_btn_7);
	DDX_Control(pDX, IDC_BTN_TI_8, m_btn_8);
	DDX_Control(pDX, IDC_BTN_TI_9, m_btn_9);
	DDX_Control(pDX, IDC_BTN_TI_10, m_btn_10);
	DDX_Control(pDX, IDC_ST_TI_6, m_st_6);
	DDX_Control(pDX, IDC_ST_TI_7, m_st_7);
	DDX_Control(pDX, IDC_EDIT_TI_6, m_edit_6);
	DDX_Control(pDX, IDC_EDIT_TI_7, m_edit_7);
}


BEGIN_MESSAGE_MAP(CDlg_TrayInfo, CDialogEx)
	ON_WM_ERASEBKGND()
	ON_WM_SIZE()
	ON_WM_TIMER()
	ON_NOTIFY(NM_CLICK, IDC_GC_TI_1, OnGridClick_GC_1)
	ON_BN_CLICKED(IDC_BTN_TI_1, &CDlg_TrayInfo::OnBnClickedBtnTi1)
	ON_BN_CLICKED(IDC_BTN_TI_2, &CDlg_TrayInfo::OnBnClickedBtnTi2)
	ON_BN_CLICKED(IDC_BTN_TI_3, &CDlg_TrayInfo::OnBnClickedBtnTi3)
	ON_BN_CLICKED(IDC_BTN_TI_4, &CDlg_TrayInfo::OnBnClickedBtnTi4)
	ON_BN_CLICKED(IDC_BTN_TI_5, &CDlg_TrayInfo::OnBnClickedBtnTi5)
	ON_BN_CLICKED(IDC_BTN_TI_6, &CDlg_TrayInfo::OnBnClickedBtnTi6)
	ON_BN_CLICKED(IDC_BTN_TI_7, &CDlg_TrayInfo::OnBnClickedBtnTi7)
	ON_BN_CLICKED(IDC_BTN_TI_8, &CDlg_TrayInfo::OnBnClickedBtnTi8)
	ON_BN_CLICKED(IDC_BTN_TI_9, &CDlg_TrayInfo::OnBnClickedBtnTi9)
	ON_BN_CLICKED(IDC_BTN_TI_10, &CDlg_TrayInfo::OnBnClickedBtnTi10)
END_MESSAGE_MAP()


// CDlg_TrayInfo 메시지 처리기입니다.


BOOL CDlg_TrayInfo::OnEraseBkgnd(CDC* pDC)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	// 전체 배경색
	CRect rect;
    GetClientRect(&rect);
    CBrush myBrush(CR_MAIN_BKGND);	// dialog background color
    CBrush *pOld = pDC->SelectObject(&myBrush);
    BOOL bRes  = pDC->PatBlt(0, 0, rect.Width(), rect.Height(), PATCOPY);
    pDC->SelectObject(pOld);		// restore old brush
    return FALSE;     
	//return CDialogEx::OnEraseBkgnd(pDC);
}


void CDlg_TrayInfo::OnSize(UINT nType, int cx, int cy)
{
	CDialogEx::OnSize(nType, cx, cy);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	if(m_bCtrl_FirstLoad == TRUE) Cal_CtrlArea();
}


void CDlg_TrayInfo::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CDialogEx::OnTimer(nIDEvent);
}


BOOL CDlg_TrayInfo::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	Cal_CtrlArea();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


BOOL CDlg_TrayInfo::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message == WM_SYSKEYDOWN)
	{
		if(pMsg->wParam == VK_F4) 
			return TRUE;
	}

	if(pMsg->message == WM_KEYDOWN)
	{
		if( pMsg->wParam == VK_ESCAPE ||	
			pMsg->wParam == VK_RETURN ||	
			pMsg->wParam == VK_SPACE  ||
			pMsg->wParam == VK_CANCEL )     
			return TRUE;        
	}

	return CDialogEx::PreTranslateMessage(pMsg);
}

void CDlg_TrayInfo::Cal_Area()
{
	CRect ClientRect;
	GetClientRect(&ClientRect);

	int n70pW = (int)((float)ClientRect.Width()*0.70f);

	m_rcLeft.SetRect(ClientRect.left, ClientRect.top, ClientRect.left+n70pW, ClientRect.bottom);
	m_rcRight.SetRect(ClientRect.left+n70pW+CTRL_MARGIN4, ClientRect.top, ClientRect.right, ClientRect.bottom);
}

void CDlg_TrayInfo::Cal_CtrlArea()
{
	Cal_Area();

	Cal_CtrlArea_Left();
	Cal_CtrlArea_Right();

	m_bCtrl_FirstLoad = TRUE;
}

void CDlg_TrayInfo::Cal_CtrlArea_Left()
{
	CRect CalRect;
	CRect rcBottom, rcGB, rcGBInside;
	int nBtnH = 60;
	int nBtnW = 60;

	rcBottom.SetRect(m_rcLeft.left, m_rcLeft.bottom - nBtnH, m_rcLeft.right, m_rcLeft.bottom);
	rcGB.SetRect(m_rcLeft.left, m_rcLeft.top, m_rcLeft.right, m_rcLeft.bottom - nBtnH);
	rcGBInside = rcGB;
	rcGBInside.DeflateRect(6,28,6,6);

	m_gb_1.SetXPGroupStyle(CXPGroupBox::XPGB_WINDOW_2);
	m_gb_1.SetBackgroundColor(CR_DARKGRAY, CR_GB_BKGND);
	m_gb_1.SetBorderColor(CR_BLACK);
	m_gb_1.SetCatptionTextColor(CR_BLACK);
	m_gb_1.MoveWindow(&rcGB);
	m_GC_1.MoveWindow(&rcGBInside);
	Init_GC_1(11,8);
	SetTrayInfoListSettingValue();

	CalRect.SetRect(rcBottom.left, rcBottom.top, rcBottom.left+nBtnW, rcBottom.bottom);
	m_btn_1.SetFontBtn(CTRL_FONT, 20, FW_BOLD);
	m_btn_1.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_1.SetTextColor(CR_BLACK);
	m_btn_1.SetIconPos(0);
	m_btn_1.SetIconID(IDI_PREV);
	m_btn_1.SetIconSize(48,48);
	m_btn_1.SetFlatType(TRUE);
	m_btn_1.MoveWindow(&CalRect);

	CalRect.SetRect(CalRect.right, rcBottom.top, CalRect.right+nBtnW, rcBottom.bottom);
	m_btn_2.SetFontBtn(CTRL_FONT, 20, FW_BOLD);
	m_btn_2.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_2.SetTextColor(CR_BLACK);
	m_btn_2.SetIconPos(0);
	m_btn_2.SetIconID(IDI_NEXT);
	m_btn_2.SetIconSize(48,48);
	m_btn_2.SetFlatType(TRUE);
	m_btn_2.MoveWindow(&CalRect);

	CalRect.SetRect(CalRect.right, rcBottom.top, CalRect.right+nBtnW, rcBottom.bottom);
	m_btn_3.SetFontBtn(CTRL_FONT, 20, FW_BOLD);
	m_btn_3.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_3.SetTextColor(CR_BLACK);
	m_btn_3.SetIconPos(0);
	m_btn_3.SetIconID(IDI_PLUS);
	m_btn_3.SetIconSize(48,48);
	m_btn_3.SetFlatType(TRUE);
	m_btn_3.MoveWindow(&CalRect);

	CalRect.SetRect(CalRect.right, rcBottom.top, CalRect.right+nBtnW, rcBottom.bottom);
	m_btn_4.SetFontBtn(CTRL_FONT, 20, FW_BOLD);
	m_btn_4.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_4.SetTextColor(CR_BLACK);
	m_btn_4.SetIconPos(0);
	m_btn_4.SetIconID(IDI_MINUS);
	m_btn_4.SetIconSize(48,48);
	m_btn_4.SetFlatType(TRUE);
	m_btn_4.MoveWindow(&CalRect);

	CalRect.SetRect(rcBottom.right-nBtnW, rcBottom.top, rcBottom.right, rcBottom.bottom);
	m_btn_10.SetFontBtn(CTRL_FONT, 20, FW_BOLD);
	m_btn_10.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_10.SetTextColor(CR_BLACK);
	m_btn_10.SetIconPos(0);
	m_btn_10.SetIconID(IDI_UPLOAD);
	m_btn_10.SetIconSize(48,48);
	m_btn_10.SetFlatType(TRUE);
	m_btn_10.MoveWindow(&CalRect);

	CalRect.SetRect(CalRect.left-nBtnW, rcBottom.top, CalRect.left, rcBottom.bottom);
	m_btn_9.SetFontBtn(CTRL_FONT, 20, FW_BOLD);
	m_btn_9.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_9.SetTextColor(CR_BLACK);
	m_btn_9.SetIconPos(0);
	m_btn_9.SetIconID(IDI_DOWNLOAD);
	m_btn_9.SetIconSize(48,48);
	m_btn_9.SetFlatType(TRUE);
	m_btn_9.MoveWindow(&CalRect);
}

void CDlg_TrayInfo::Cal_CtrlArea_Right()
{
	CRect CalRect;
	CRect rcBottom, rcGB, rcGBInside;
	CRect rcDiv[11];
	int nBtnH = 60;
	int nBtnW = 60;

	rcBottom.SetRect(m_rcRight.left, m_rcRight.bottom - nBtnH, m_rcRight.right, m_rcRight.bottom);
	rcGB.SetRect(m_rcRight.left, m_rcRight.top, m_rcRight.right, m_rcRight.bottom - nBtnH);
	rcGBInside = rcGB;
	rcGBInside.DeflateRect(6,28,6,6);
	int n11DivH = (int)((float)rcGBInside.Height()/11.0f);
	int n50pW = (int)((float)rcGBInside.Width()/2.0f);

	rcDiv[0].SetRect(rcGBInside.left, rcGBInside.top, rcGBInside.right, rcGBInside.top+n11DivH);
	rcDiv[1].SetRect(rcGBInside.left, rcDiv[0].bottom, rcGBInside.right, rcDiv[0].bottom+n11DivH);
	rcDiv[2].SetRect(rcGBInside.left, rcDiv[1].bottom, rcGBInside.right, rcDiv[1].bottom+n11DivH);
	rcDiv[3].SetRect(rcGBInside.left, rcDiv[2].bottom, rcGBInside.right, rcDiv[2].bottom+n11DivH);
	rcDiv[4].SetRect(rcGBInside.left, rcDiv[3].bottom, rcGBInside.right, rcDiv[3].bottom+n11DivH);
	rcDiv[5].SetRect(rcGBInside.left, rcDiv[4].bottom, rcGBInside.right, rcDiv[4].bottom+n11DivH);
	rcDiv[6].SetRect(rcGBInside.left, rcDiv[5].bottom, rcGBInside.right, rcDiv[5].bottom+n11DivH);
	rcDiv[7].SetRect(rcGBInside.left, rcDiv[6].bottom, rcGBInside.right, rcDiv[6].bottom+n11DivH);
	rcDiv[8].SetRect(rcGBInside.left, rcDiv[7].bottom, rcGBInside.right, rcDiv[7].bottom+n11DivH);
	rcDiv[9].SetRect(rcGBInside.left, rcDiv[8].bottom, rcGBInside.right, rcDiv[8].bottom+n11DivH);
	rcDiv[10].SetRect(rcGBInside.left, rcDiv[9].bottom, rcGBInside.right, rcGBInside.bottom);

	m_gb_2.SetXPGroupStyle(CXPGroupBox::XPGB_WINDOW_2);
	m_gb_2.SetBackgroundColor(CR_DARKGRAY, CR_GB_BKGND);
	m_gb_2.SetBorderColor(CR_BLACK);
	m_gb_2.SetCatptionTextColor(CR_BLACK);
	m_gb_2.MoveWindow(&rcGB);

	m_st_1.SetBkColor(CR_DARKSLATEGRAY);
	m_st_1.SetTextColor(CR_WHITE);
	m_st_1.SetFont(CTRL_FONT,20,FW_BOLD);
	m_st_1.MoveWindow(&rcDiv[0]);

	m_edit_1.SetFontStyle(CTRL_FONT, 20, FW_BOLD);
	m_edit_1.SetColor(CR_BLACK, CR_WHITE);
	m_edit_1.SetNumeric(FALSE);
	m_edit_1.MoveWindow(&rcDiv[1]);

	m_st_2.SetBkColor(CR_DARKSLATEGRAY);
	m_st_2.SetTextColor(CR_WHITE);
	m_st_2.SetFont(CTRL_FONT,20,FW_BOLD);
	m_st_2.MoveWindow(&rcDiv[2]);

	m_edit_2.SetFontStyle(CTRL_FONT, 20, FW_BOLD);
	m_edit_2.SetColor(CR_BLACK, CR_WHITE);
	m_edit_2.SetNumeric(FALSE);
	m_edit_2.MoveWindow(&rcDiv[3]);

	m_st_3.SetBkColor(CR_DARKSLATEGRAY);
	m_st_3.SetTextColor(CR_WHITE);
	m_st_3.SetFont(CTRL_FONT,20,FW_BOLD);
	m_st_3.MoveWindow(&rcDiv[4]);

	m_edit_3.SetFontStyle(CTRL_FONT, 20, FW_BOLD);
	m_edit_3.SetColor(CR_BLACK, CR_WHITE);
	m_edit_3.SetNumeric(FALSE);
	m_edit_3.MoveWindow(&rcDiv[5]);

	CalRect.SetRect(rcDiv[6].left, rcDiv[6].top, rcDiv[6].left+n50pW, rcDiv[6].bottom);
	m_st_4.SetBkColor(CR_DARKSLATEGRAY);
	m_st_4.SetTextColor(CR_WHITE);
	m_st_4.SetFont(CTRL_FONT,20,FW_BOLD);
	m_st_4.MoveWindow(&CalRect);

	CalRect.SetRect(rcDiv[6].left+n50pW, rcDiv[6].top, rcDiv[6].right, rcDiv[6].bottom);
	m_st_5.SetBkColor(CR_DARKSLATEGRAY);
	m_st_5.SetTextColor(CR_WHITE);
	m_st_5.SetFont(CTRL_FONT,20,FW_BOLD);
	m_st_5.MoveWindow(&CalRect);

	CalRect.SetRect(rcDiv[7].left, rcDiv[7].top, rcDiv[7].left+n50pW, rcDiv[7].bottom);
	m_edit_4.SetFontStyle(CTRL_FONT, 20, FW_BOLD);
	m_edit_4.SetColor(CR_BLACK, CR_WHITE);
	m_edit_4.SetNumeric(FALSE);
	m_edit_4.MoveWindow(&CalRect);

	CalRect.SetRect(rcDiv[7].left+n50pW, rcDiv[7].top, rcDiv[7].right, rcDiv[7].bottom);
	m_edit_5.SetFontStyle(CTRL_FONT, 20, FW_BOLD);
	m_edit_5.SetColor(CR_BLACK, CR_WHITE);
	m_edit_5.SetNumeric(FALSE);
	m_edit_5.MoveWindow(&CalRect);

	CalRect.SetRect(rcDiv[8].left, rcDiv[8].top, rcDiv[8].left+n50pW, rcDiv[8].bottom);
	m_st_6.SetBkColor(CR_DARKSLATEGRAY);
	m_st_6.SetTextColor(CR_WHITE);
	m_st_6.SetFont(CTRL_FONT,20,FW_BOLD);
	m_st_6.MoveWindow(&CalRect);

	CalRect.SetRect(rcDiv[8].left+n50pW, rcDiv[8].top, rcDiv[8].right, rcDiv[8].bottom);
	m_st_7.SetBkColor(CR_DARKSLATEGRAY);
	m_st_7.SetTextColor(CR_WHITE);
	m_st_7.SetFont(CTRL_FONT,20,FW_BOLD);
	m_st_7.MoveWindow(&CalRect);

	CalRect.SetRect(rcDiv[9].left, rcDiv[9].top, rcDiv[9].left+n50pW, rcDiv[9].bottom);
	m_edit_6.SetFontStyle(CTRL_FONT, 20, FW_BOLD);
	m_edit_6.SetColor(CR_BLACK, CR_WHITE);
	m_edit_6.SetNumeric(FALSE);
	m_edit_6.MoveWindow(&CalRect);

	CalRect.SetRect(rcDiv[9].left+n50pW, rcDiv[9].top, rcDiv[9].right, rcDiv[9].bottom);
	m_edit_7.SetFontStyle(CTRL_FONT, 20, FW_BOLD);
	m_edit_7.SetColor(CR_BLACK, CR_WHITE);
	m_edit_7.SetNumeric(FALSE);
	m_edit_7.MoveWindow(&CalRect);

	m_btn_5.SetFontBtn(CTRL_FONT, 20, FW_BOLD);
	m_btn_5.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_5.SetTextColor(CR_BLACK);
	m_btn_5.SetIconPos(0);
	m_btn_5.SetIconID(IDI_MODIFY);
	m_btn_5.SetIconSize(32,32);
	m_btn_5.SetFlatType(TRUE);
	m_btn_5.MoveWindow(&rcDiv[10]);

	CalRect.SetRect(rcBottom.left, rcBottom.top, rcBottom.left+nBtnW, rcBottom.bottom);
	m_btn_6.SetFontBtn(CTRL_FONT, 20, FW_BOLD);
	m_btn_6.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_6.SetTextColor(CR_BLACK);
	m_btn_6.SetIconPos(0);
	m_btn_6.SetIconID(IDI_SAVE);
	m_btn_6.SetIconSize(48,48);
	m_btn_6.SetFlatType(TRUE);
	m_btn_6.MoveWindow(&CalRect);

	CalRect.SetRect(rcBottom.right-nBtnW, rcBottom.top, rcBottom.right, rcBottom.bottom);
	m_btn_8.SetFontBtn(CTRL_FONT, 20, FW_BOLD);
	m_btn_8.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_8.SetTextColor(CR_BLACK);
	m_btn_8.SetIconPos(0);
	m_btn_8.SetIconID(IDI_CLOSE);
	m_btn_8.SetIconSize(48,48);
	m_btn_8.SetFlatType(TRUE);
	m_btn_8.MoveWindow(&CalRect);

	CalRect.SetRect(CalRect.left-nBtnW, rcBottom.top, CalRect.left, rcBottom.bottom);
	m_btn_7.SetFontBtn(CTRL_FONT, 18, FW_BOLD);
	m_btn_7.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_7.SetTextColor(CR_BLACK);
	//m_btn_3.SetIconPos(0);
	//m_btn_3.SetIconID(IDI_PLUS);
	//m_btn_3.SetIconSize(48,48);
	m_btn_7.SetFlatType(TRUE);
	m_btn_7.MoveWindow(&CalRect);
}

bool CDlg_TrayInfo::Init_GC_1(int nRows, int nCols)
{
	BOOL m_bEditable = FALSE;
    BOOL m_bListMode = TRUE;
    int m_nRows = nRows;
    int m_nCols = nCols;
    int m_nFixRows = 1;
    int m_nFixCols = 1;

    m_GC_1.SetEditable(m_bEditable);
    m_GC_1.SetListMode(m_bListMode);
    m_GC_1.EnableDragAndDrop(FALSE);
    m_GC_1.SetTextBkColor(RGB(0xFF, 0xFF, 0xE0));

    m_GC_1.SetRowCount(m_nRows);
    m_GC_1.SetColumnCount(m_nCols);
    m_GC_1.SetFixedRowCount(m_nFixRows);
    m_GC_1.SetFixedColumnCount(m_nFixCols);

	m_GC_1.EnableSelection(false);
	m_GC_1.SetSingleColSelection(true);
	m_GC_1.SetSingleRowSelection(true);
	m_GC_1.SetFixedColumnSelection(false);
    m_GC_1.SetFixedRowSelection(false);

	m_GC_1.SetRowResize(false);
	m_GC_1.SetColumnResize(false);

	CRect ClientRect;
	GetDlgItem(IDC_GC_TI_1)->GetWindowRect(&ClientRect);
	if(ClientRect.Width() < 4) return false;
	if(ClientRect.Height() < 4) return false;
	int nClientWidth = ClientRect.Width()-4;
	int nClientHeight = ClientRect.Height()-4;
	int n10pW = (int)(nClientWidth*0.1f);
	int n30pW = (int)(nClientWidth*0.3f);
	int nRemainW = nClientWidth - n10pW - n30pW - n10pW*5;
	//int nCellW = nClientWidth/m_nCols;
	int nCellH = nClientHeight/m_nRows;
	//int nLastCellW = nClientWidth-nCellW*(m_nCols-1);
	int nLastCellH = nClientHeight-nCellH*(m_nRows-1);
	//int nRemainW = nLastCellW - nCellW;
	int nRemainH = nLastCellH - nCellH;
	for (int i = 0; i < m_GC_1.GetRowCount(); i++) 
	{
		if(i == m_nRows-1)
		{
				m_GC_1.SetRowHeight(i,nCellH);
		}
		else 
		{
			if(nRemainH > 0)
			{
				m_GC_1.SetRowHeight(i,nCellH + 1);
				nRemainH --;
			}
			else
				m_GC_1.SetRowHeight(i,nCellH);
		}
	}
	for (int j = 0; j < m_GC_1.GetColumnCount(); j++)
	{
		if(j == 0) m_GC_1.SetColumnWidth(j,n10pW);
		else if(j == 1) m_GC_1.SetColumnWidth(j,n30pW);
		else if(j == 2) m_GC_1.SetColumnWidth(j,n10pW);
		else if(j == 3) m_GC_1.SetColumnWidth(j,n10pW);
		else if(j == 4) m_GC_1.SetColumnWidth(j,n10pW);
		else if(j == 5) m_GC_1.SetColumnWidth(j,n10pW);
		else if(j == 6) m_GC_1.SetColumnWidth(j,n10pW);
		else if(j == 7) m_GC_1.SetColumnWidth(j,nRemainW);
	}
    DWORD dwTextStyle = DT_CENTER|DT_VCENTER|DT_SINGLELINE;

    // fill rows/cols with text
    for (int row = 0; row < m_GC_1.GetRowCount(); row++) 
	{
        for (int col = 0; col < m_GC_1.GetColumnCount(); col++) 
		{ 
            GV_ITEM Item;
            Item.mask = GVIF_TEXT|GVIF_FORMAT;
            Item.row = row;
            Item.col = col;
			Item.nFormat = dwTextStyle;

			if(row == 0)
			{
				if(col == 0) Item.strText.Format(_T("#"));
				else if(col == 1) Item.strText.Format(_T("NAME"));
				else if(col == 2) Item.strText.Format(_T("TH"));
				else if(col == 3) Item.strText.Format(_T("OH"));
				else if(col == 4) Item.strText.Format(_T("MX"));
				else if(col == 5) Item.strText.Format(_T("MY"));
				else if(col == 6) Item.strText.Format(_T("SX"));
				else if(col == 7) Item.strText.Format(_T("SY"));
			}
			else
			{
			}

			if(row == 0 || col == 0) m_GC_1.SetItemBkColour(row, col, CR_SLATEGRAY);
			m_GC_1.SetItemFgColour(row, col, RGB(0,0,0));
			m_GC_1.SetItemFont(row, col, CTRL_FONT, 18, FW_BOLD);
            m_GC_1.SetItem(&Item);  
        }
    }
       
    return TRUE;  // return TRUE  unless you set the focus to a control
}

void CDlg_TrayInfo::OnGridClick_GC_1(NMHDR *pNotifyStruct, LRESULT* /*pResult*/)
{
	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*) pNotifyStruct;

	int nSelRow = pItem->iRow;
	int nSelCol = pItem->iColumn;

	if(nSelRow > 0) nSelRow += m_nCurPageNum*10;

	if(m_nSelTrayIndex != nSelRow)
	{
		stDeviceTrayInfo stDTI = CDeviceTraySetting::Instance()->GetDeviceTrayInfo();
		if(nSelRow > stDTI.m_nTrayIndexCount)
		{
			m_nSelTrayIndex = 0;
			Update_SelTrayIndex(m_nSelTrayIndex);
			return;
		}

		m_nSelTrayIndex = nSelRow;
		Update_SelTrayIndex(m_nSelTrayIndex);
	}
}

void CDlg_TrayInfo::SetTrayInfoListSettingValue()
{
	stDeviceTrayInfo stDTI = CDeviceTraySetting::Instance()->GetDeviceTrayInfo();
	if(stDTI.m_nTrayIndexCount <= 0) return;
	if(m_nSelTrayIndex > stDTI.m_nTrayIndexCount) return;

	// 
	DWORD dwTextStyle = DT_CENTER|DT_VCENTER|DT_SINGLELINE;
    for (int row = 0; row < m_GC_1.GetRowCount(); row++) 
	{
        for (int col = 0; col < m_GC_1.GetColumnCount(); col++) 
		{ 
            GV_ITEM Item;
            Item.mask = GVIF_TEXT|GVIF_FORMAT|GVIF_BKCLR;
            Item.row = row;
            Item.col = col;
			Item.nFormat = dwTextStyle;
			Item.crBkClr = RGB(0xFF, 0xFF, 0xE0);

			if(row == 0)
			{
				if(col == 0) Item.strText.Format(_T("#"));
				else if(col == 1) Item.strText.Format(_T("NAME"));
				else if(col == 2) Item.strText.Format(_T("TH"));
				else if(col == 3) Item.strText.Format(_T("OH"));
				else if(col == 4) Item.strText.Format(_T("MX"));
				else if(col == 5) Item.strText.Format(_T("MY"));
				else if(col == 6) Item.strText.Format(_T("SX"));
				else if(col == 7) Item.strText.Format(_T("SY"));
				Item.crBkClr = CR_SLATEGRAY;
			}
			else
			{
				if(col == 0)
				{
					Item.strText.Format(_T("%d"), row + m_nCurPageNum*10);
					Item.crBkClr = CR_SLATEGRAY;
				}
				else if(col == 1)
				{
					if(row + m_nCurPageNum*10 <= stDTI.m_nTrayIndexCount)
					{
						Item.strText = stDTI.m_vecTrayInfo[row + m_nCurPageNum*10 - 1].m_strTraySize;
						Item.crBkClr = CR_GAINSBORO;
					}
				}
				else if(col == 2)
				{
					if(row + m_nCurPageNum*10 <= stDTI.m_nTrayIndexCount)
					{
						Item.strText.Format(_T("%.2f"), stDTI.m_vecTrayInfo[row + m_nCurPageNum*10 - 1].m_fTotalHeight);
						Item.crBkClr = CR_GAINSBORO;
					}
				}
				else if(col == 3)
				{
					if(row + m_nCurPageNum*10 <= stDTI.m_nTrayIndexCount)
					{
						Item.strText.Format(_T("%.2f"), stDTI.m_vecTrayInfo[row + m_nCurPageNum*10 - 1].m_fOverlapHeight);
						Item.crBkClr = CR_GAINSBORO;
					}
				}
				else if(col == 4)
				{
					if(row + m_nCurPageNum*10 <= stDTI.m_nTrayIndexCount)
					{
						Item.strText.Format(_T("%d"), stDTI.m_vecTrayInfo[row + m_nCurPageNum*10 - 1].m_nTrayMatrixRow);
						Item.crBkClr = CR_GAINSBORO;
					}
				}
				else if(col == 5)
				{
					if(row + m_nCurPageNum*10 <= stDTI.m_nTrayIndexCount)
					{
						Item.strText.Format(_T("%d"), stDTI.m_vecTrayInfo[row + m_nCurPageNum*10 - 1].m_nTrayMatrixCol);
						Item.crBkClr = CR_GAINSBORO;
					}
				}
				else if(col == 6)
				{
					if(row + m_nCurPageNum*10 <= stDTI.m_nTrayIndexCount)
					{
						Item.strText.Format(_T("%.2f"), stDTI.m_vecTrayInfo[row + m_nCurPageNum*10 - 1].m_fTraySizeX/100.0f);
						Item.crBkClr = CR_GAINSBORO;
					}
				}
				else if(col == 7)
				{
					if(row + m_nCurPageNum*10 <= stDTI.m_nTrayIndexCount)
					{
						Item.strText.Format(_T("%.2f"), stDTI.m_vecTrayInfo[row + m_nCurPageNum*10 - 1].m_fTraySizeY/100.0f);
						Item.crBkClr = CR_GAINSBORO;
					}
				}
			}

			m_GC_1.SetItemFgColour(row, col, RGB(0,0,0));
            m_GC_1.SetItem(&Item);  
        }
    }

	m_GC_1.Invalidate();
}

void CDlg_TrayInfo::Update_SelTrayIndex(int nTrayIndex)
{
	if(nTrayIndex == 0)
	{
		m_edit_1.SetWindowText(_T(""));
		m_edit_2.SetWindowText(_T(""));
		m_edit_3.SetWindowText(_T(""));
		m_edit_4.SetWindowText(_T(""));
		m_edit_5.SetWindowText(_T(""));
		m_edit_6.SetWindowText(_T(""));
		m_edit_7.SetWindowText(_T(""));
	}
	else
	{
		CString strTemp;
		stDeviceTrayInfo stDTI = CDeviceTraySetting::Instance()->GetDeviceTrayInfo();
		m_edit_1.SetWindowText(stDTI.m_vecTrayInfo[m_nSelTrayIndex-1].m_strTraySize);
		strTemp.Format(_T("%.2f"), stDTI.m_vecTrayInfo[m_nSelTrayIndex-1].m_fTotalHeight);
		m_edit_2.SetWindowText(strTemp);
		strTemp.Format(_T("%.2f"), stDTI.m_vecTrayInfo[m_nSelTrayIndex-1].m_fOverlapHeight);
		m_edit_3.SetWindowText(strTemp);
		strTemp.Format(_T("%d"), stDTI.m_vecTrayInfo[m_nSelTrayIndex-1].m_nTrayMatrixRow);
		m_edit_4.SetWindowText(strTemp);
		strTemp.Format(_T("%d"), stDTI.m_vecTrayInfo[m_nSelTrayIndex-1].m_nTrayMatrixCol);
		m_edit_5.SetWindowText(strTemp);
		strTemp.Format(_T("%.2f"), stDTI.m_vecTrayInfo[m_nSelTrayIndex-1].m_fTraySizeX/100.0f);
		m_edit_6.SetWindowText(strTemp);
		strTemp.Format(_T("%.2f"), stDTI.m_vecTrayInfo[m_nSelTrayIndex-1].m_fTraySizeY/100.0f);
		m_edit_7.SetWindowText(strTemp);
	}
}

void CDlg_TrayInfo::OnBnClickedBtnTi1()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_nCurPageNum --;
	if(m_nCurPageNum < 0) m_nCurPageNum = 0;
	else SetTrayInfoListSettingValue();
}


void CDlg_TrayInfo::OnBnClickedBtnTi2()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	stDeviceTrayInfo stDTI = CDeviceTraySetting::Instance()->GetDeviceTrayInfo();
	m_nCurPageNum ++;
	int nMaxPage = stDTI.m_nTrayIndexCount/10;
	if(stDTI.m_nTrayIndexCount%10 > 0) nMaxPage ++;
	if(m_nCurPageNum > nMaxPage - 1) m_nCurPageNum = nMaxPage - 1;
	else SetTrayInfoListSettingValue();
}


void CDlg_TrayInfo::OnBnClickedBtnTi3()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CDeviceTraySetting::Instance()->AddTrayInfo();

	SetTrayInfoListSettingValue();
}


void CDlg_TrayInfo::OnBnClickedBtnTi4()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CDeviceTraySetting::Instance()->DelTrayInfo();

	SetTrayInfoListSettingValue();
}


void CDlg_TrayInfo::OnBnClickedBtnTi5()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	stDeviceTrayInfo stDTI = CDeviceTraySetting::Instance()->GetDeviceTrayInfo();
	if(m_nSelTrayIndex == 0) return;
	if(m_nSelTrayIndex > stDTI.m_nTrayIndexCount) return;

	CString strText, strTemp;
	m_edit_1.GetWindowText(strText);

	if(lstrlen(strText) > 16)
	{
		AfxMessageBox(_T("글자수 16자 내로 해주십시오."));
		return;
	}

	m_edit_2.GetWindowText(strTemp);
	double dTH = _wtof(strTemp);
	m_edit_3.GetWindowText(strTemp);
	double dOH = _wtof(strTemp);
	m_edit_4.GetWindowText(strTemp);
	int nX = _wtoi(strTemp);
	m_edit_5.GetWindowText(strTemp);
	int nY = _wtoi(strTemp);
	m_edit_6.GetWindowText(strTemp);
	double dSX = _wtof(strTemp)*100.0f;
	m_edit_7.GetWindowText(strTemp);
	double dSY = _wtof(strTemp)*100.0f;

	CDeviceTraySetting::Instance()->SetTrayInfo(m_nSelTrayIndex-1, strText, dTH, dOH, nX, nY, dSX, dSY);

	SetTrayInfoListSettingValue();
}


void CDlg_TrayInfo::OnBnClickedBtnTi6()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_btn_6.EnableWindow(FALSE);
	CDeviceTraySetting::Instance()->Save_TrayInfoSetting();
	m_btn_6.EnableWindow(TRUE);
}


void CDlg_TrayInfo::OnBnClickedBtnTi7()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	stDeviceTrayInfo stDTI = CDeviceTraySetting::Instance()->GetDeviceTrayInfo();
	if(m_nSelTrayIndex <= 0) m_nSelTrayIndex = 0;
	if(m_nSelTrayIndex > stDTI.m_nTrayIndexCount) m_nSelTrayIndex = 0;

	CDialogEx::OnOK();
}


void CDlg_TrayInfo::OnBnClickedBtnTi8()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CDialogEx::OnCancel();
}


void CDlg_TrayInfo::OnBnClickedBtnTi9()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(CDataManager::Instance()->IsConnected(CONN_PLC_1)) CDataManager::Instance()->m_PLCReaderZR.Read();
	else return;

	m_btn_9.EnableWindow(FALSE);

	CString strText;
	double dTotalHeight, dOverlapHeight, dHeight, dSizeX, dSizeY;
	int nMatrixRow, nMatrixCol;
	stDeviceTrayInfo stDTI = CDeviceTraySetting::Instance()->GetDeviceTrayInfo();
	int nStartAddress = 21000;
	for(int i=0;i<stDTI.m_nTrayIndexCount;i++)
	{
		nStartAddress = 21000 + i*20;
		strText = CDataManager::Instance()->m_PLCReaderZR.GetString(nStartAddress, 10);
		strText.Trim();
		dTotalHeight = (float)CDataManager::Instance()->m_PLCReaderZR.GetValue(nStartAddress+10)/100.0f;
		dOverlapHeight = (float)CDataManager::Instance()->m_PLCReaderZR.GetValue(nStartAddress+11)/100.0f;
		dHeight = (float)CDataManager::Instance()->m_PLCReaderZR.GetValue(nStartAddress+12)/100.0f;
		nMatrixRow = CDataManager::Instance()->m_PLCReaderZR.GetValue(nStartAddress+13);
		nMatrixCol = CDataManager::Instance()->m_PLCReaderZR.GetValue(nStartAddress+14);
		dSizeX = (float)CDataManager::Instance()->m_PLCReaderZR.GetValue(nStartAddress+15);
		dSizeY = (float)CDataManager::Instance()->m_PLCReaderZR.GetValue(nStartAddress+16);

		CDeviceTraySetting::Instance()->SetTrayInfo(i, strText, dTotalHeight, dOverlapHeight, nMatrixRow, nMatrixCol, dSizeX, dSizeY);
	}

	SetTrayInfoListSettingValue();

	m_btn_9.EnableWindow(TRUE);
}


void CDlg_TrayInfo::OnBnClickedBtnTi10()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(CDataManager::Instance()->IsConnected(CONN_PLC_1) == FALSE) return;

	m_btn_10.EnableWindow(FALSE);

	SHORT sTrayText[10];
	TCHAR strTraySize[256];
	CString strText;
	double dTotalHeight, dOverlapHeight, dHeight, dSizeX, dSizeY;
	int nMatrixRow, nMatrixCol;
	stDeviceTrayInfo stDTI = CDeviceTraySetting::Instance()->GetDeviceTrayInfo();
	int nStartAddress = 21000;
	for(int i=0;i<stDTI.m_nTrayIndexCount;i++)
	{
		nStartAddress = 21000 + i*20;
		strText = stDTI.m_vecTrayInfo[i].m_strTraySize;

		memset(sTrayText,0,sizeof(SHORT)*10);
		memset(strTraySize,0,sizeof(TCHAR)*256);
		if(strText.IsEmpty() == FALSE) wcscpy(strTraySize, strText);
		for(int j=0;j<10;j++)
		{
			sTrayText[j] = MAKEWORD(strTraySize[j*2], strTraySize[j*2+1]);
		}
		CDataManager::Instance()->PLC_Write_ZR2(1, nStartAddress, sTrayText, 10);

		dTotalHeight = (float)stDTI.m_vecTrayInfo[i].m_fTotalHeight*100.0f;
		CDataManager::Instance()->PLC_Write_ZR(1, nStartAddress+10, (SHORT)dTotalHeight);

		dOverlapHeight = (float)stDTI.m_vecTrayInfo[i].m_fOverlapHeight*100.0f;
		CDataManager::Instance()->PLC_Write_ZR(1, nStartAddress+11, (SHORT)dOverlapHeight);

		dHeight = (float)stDTI.m_vecTrayInfo[i].m_fHeight*100.0f;
		CDataManager::Instance()->PLC_Write_ZR(1, nStartAddress+12, (SHORT)dHeight);

		nMatrixRow = stDTI.m_vecTrayInfo[i].m_nTrayMatrixRow;
		CDataManager::Instance()->PLC_Write_ZR(1, nStartAddress+13, (SHORT)nMatrixRow);

		nMatrixCol = stDTI.m_vecTrayInfo[i].m_nTrayMatrixCol;
		CDataManager::Instance()->PLC_Write_ZR(1, nStartAddress+14, (SHORT)nMatrixCol);

		dSizeX = (float)stDTI.m_vecTrayInfo[i].m_fTraySizeX;
		CDataManager::Instance()->PLC_Write_ZR(1, nStartAddress+15, (SHORT)dSizeX);

		dSizeY = (float)stDTI.m_vecTrayInfo[i].m_fTraySizeY;
		CDataManager::Instance()->PLC_Write_ZR(1, nStartAddress+16, (SHORT)dSizeY);
	}

	m_btn_10.EnableWindow(TRUE);
}
