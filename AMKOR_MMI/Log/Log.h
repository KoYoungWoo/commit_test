#pragma once

#include "LogFile.h"

#ifndef		__T
#define		__T(x)	L ## x
#define		_T(x)	__T(x)
#endif

#define		T__FUNCTION__	_T(__FUNCTION__)

#define		MAX_LOG_STRING				1024*10//1000  Buffer Size check
#define		MAX_SERVERNAME_LENGTH		32

class LogGC;
class Log 
{
private:
	WCHAR			m_szLogDrive;
	WCHAR			m_szLogPath[512];
	CLogFile*		m_pDebugLog;
	CLogFile*		m_pAlarmLog;
	CLogFile*		m_pMesLog;
	CLogFile*		m_pPlcLog;

private:
	WCHAR			m_szServiceName[MAX_SERVERNAME_LENGTH];
	int				m_iServiceType;
	static Log*		m_pInstance;

public:
	friend class	LogGC;
	static Log*		Instance();

protected:
	Log();
	Log(Log &rhs) {};

	bool isDiskExist(WCHAR *directory);

public:
	~Log();

public:
	bool	InitLog();
	int		GetServerType();
	void	Debug(WCHAR *fmt, ...);
	void	DebugT(LPCTSTR fmt, ...);
	void	Alarm(WCHAR *fmt, ...);
	void	AlarmT(LPCTSTR fmt, ...);
	void	Mes(WCHAR *fmt, ...);
	void	MesT(LPCTSTR fmt, ...);
	void	Plc(WCHAR *fmt, ...);
	void	PlcT(LPCTSTR fmt, ...);

	CString GetCurDate(); //2018-06-19

	void SetLogPath(LPCTSTR strPath);
	LPCTSTR GetLogPath();

	LPCWSTR		GetLogFileName(LPWSTR buffer, size_t size, LPCWSTR logName);
	CLogFile*	GetDebugLog();
	CLogFile*	GetAlarmLog();
	CLogFile*	GetMesLog();
	CLogFile*	GetPlcLog();

	void InitDebugLog();
	void InitAlarmLog();
	void InitMesLog();
	void InitPlcLog();
};

inline Log* GetLog()
{
	return Log::Instance();
}

inline void Log::SetLogPath(LPCTSTR strPath)
{
	wcscpy(m_szLogPath, strPath);
	if (m_pDebugLog) 
	{
		InitDebugLog();
		m_pDebugLog->SetPrint();
		m_pDebugLog->SetTrace();
	}
	if (m_pAlarmLog) 
	{
		InitAlarmLog();
		m_pAlarmLog->SetPrint();
		m_pAlarmLog->SetTrace();
	}
	if (m_pMesLog)
	{
		InitMesLog();
		m_pMesLog->SetPrint();
		m_pMesLog->SetTrace();
	}
	if (m_pPlcLog)
	{
		InitPlcLog();
		m_pPlcLog->SetPrint();
		m_pPlcLog->SetTrace();
	}
}

inline LPCTSTR Log::GetLogPath()
{
	return m_szLogPath;
}

inline LPCWSTR Log::GetLogFileName(LPWSTR buffer, size_t size, LPCWSTR logName)
{
	//_snwprintf(buffer, size, _T("./LOG/%s.txt"), logName); // origin
	_snwprintf(buffer, size, _T("%s/%s.txt"), m_szLogPath, logName);
	return buffer;
}

inline int Log::GetServerType()
{
	return m_iServiceType;
}

inline CLogFile *Log::GetDebugLog()
{
	if (m_pDebugLog)
		return m_pDebugLog;

	m_pDebugLog = new CLogFile;
	if (m_pDebugLog) {
		InitDebugLog();
		m_pDebugLog->SetPrint();
		m_pDebugLog->SetTrace();
	}

	return m_pDebugLog;
}

inline CLogFile *Log::GetAlarmLog()
{
	if (m_pAlarmLog)
		return m_pAlarmLog;

	m_pAlarmLog = new CLogFile;
	if (m_pAlarmLog) {
		InitAlarmLog();
		m_pAlarmLog->SetPrint();
		m_pAlarmLog->SetTrace();
	}

	return m_pAlarmLog;
}

inline CLogFile *Log::GetMesLog()
{
	if (m_pMesLog)
		return m_pMesLog;

	m_pMesLog = new CLogFile;
	if (m_pMesLog) {
		InitMesLog();
		m_pMesLog->SetPrint();
		m_pMesLog->SetTrace();
	}

	return m_pMesLog;
}

inline CLogFile *Log::GetPlcLog()
{
	if (m_pPlcLog)
		return m_pPlcLog;

	m_pPlcLog = new CLogFile;
	if (m_pPlcLog) {
		InitPlcLog();
		m_pPlcLog->SetPrint();
		m_pPlcLog->SetTrace();
	}

	return m_pPlcLog;
}

inline void Log::InitDebugLog()
{
	CString strPath; 
	strPath.Format(L"DEBUG\\%s\\log_debug",GetCurDate());

	//WCHAR buffer[MAX_PATH];
	m_pDebugLog->SetLogFileCsv(
		m_szLogPath,
		//2018-06-11
		TRUE,
		_T("DEBUG"),
		TRUE,
		GetCurDate(),
		_T("log_debug"),
		_T("txt"),
		//GetLog()->GetLogFileName(buffer, MAX_PATH, _T("log_debug")),
		//GetLog()->GetLogFileName(buffer, MAX_PATH, strPath),
		//LOGFILE_PER_DAY, 
		LOGFILE_PER_HOUR,
		LOG_SIZE, 
		LOG_EVERY_TIME);
}

inline void Log::InitAlarmLog()
{
	//WCHAR buffer[MAX_PATH];
	m_pAlarmLog->SetLogFileCsv(
		m_szLogPath,
		TRUE,
		_T("ALARM"),
		FALSE,
		NULL,
		_T("log_alarm"),
		_T("txt"),
		//GetLog()->GetLogFileName(buffer, MAX_PATH, _T("log_alarm")),
		LOGFILE_PER_DAY, 
		LOG_SIZE, 
		LOG_EVERY_TIME);
}

inline void Log::InitMesLog()
{
	//2018-06-19
	CString strPath; 
	strPath.Format(L"MES\\%s\\log_mes",GetCurDate());
	//

	//WCHAR buffer[MAX_PATH];
	m_pMesLog->SetLogFileCsv(
		m_szLogPath,
		TRUE,
		_T("MES"),
		TRUE,
		GetCurDate(),
		//2018-06-11
		_T("log_mes"),
		_T("txt"),
		//GetLog()->GetLogFileName(buffer, MAX_PATH, _T("log_mes")),
		//GetLog()->GetLogFileName(buffer, MAX_PATH, strPath), 
		//LOGFILE_PER_DAY, 
		LOGFILE_PER_HOUR, //2018-06-09 
		LOG_SIZE, 
		LOG_EVERY_TIME);
}

inline void Log::InitPlcLog()
{
	CString strPath; 
	strPath.Format(L"PLC\\%s\\log_plc",GetCurDate());

	//WCHAR buffer[MAX_PATH];
	m_pPlcLog->SetLogFileCsv(
		m_szLogPath,
		TRUE,
		_T("PLC"),
		TRUE,
		GetCurDate(),
		_T("log_plc"),
		_T("txt"),
		//GetLog()->GetLogFileName(buffer, MAX_PATH, _T("log_plc")),
		//GetLog()->GetLogFileName(buffer, MAX_PATH, strPath),
		LOGFILE_PER_HOUR, 
		LOG_SIZE, 
		LOG_EVERY_TIME);
}

