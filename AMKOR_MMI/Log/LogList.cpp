#include "stdafx.h"
#include "LogList.h"
#include "CsvParser.h"

//////////////////////////////////////////////////////////////////////
// CLogListGC
class CLogListGC
{
public:
	CLogListGC() {}
	~CLogListGC() { delete CLogList::m_pThis; CLogList::m_pThis = NULL; }
};
CLogListGC s_GC;

CLogList* CLogList::m_pThis = NULL;
CLogList::CLogList(void)
{
	for(int i=0; i < 4;i++)
	{
		m_bRefreshData[i] = true;
	}
	m_nMaxStringCount = 100;

	m_vecLogList_Debug.clear();
	m_vecLogList_Alarm.clear();
	m_vecLogList_Mes.clear();
	m_vecLogList_Plc.clear();
}

CLogList::~CLogList(void)
{
}

CLogList* CLogList::Instance()
{
	if( !m_pThis )
		m_pThis = new CLogList;
	return m_pThis;
}

CStringA CLogList::GetExecuteDirectory()
{
	CStringA strFolderPath;

	// 현재 프로그램의 실행 파일이 있는 폴더명을 추출함
	::GetModuleFileNameA(NULL, strFolderPath.GetBuffer(MAX_PATH), MAX_PATH);
	strFolderPath.ReleaseBuffer();
	if (strFolderPath.Find('\\') != -1)
	{
		for (int i = strFolderPath.GetLength() - 1; i >= 0; i--) 
		{
			CHAR ch = strFolderPath[i];
			strFolderPath.Delete(i);
			if (ch == '\\') break; 
		}
	}
	return strFolderPath;
}

void CLogList::Load_Data_Debug()
{
	SYSTEMTIME stTime;
	GetSystemTime(&stTime);

	CStringA strFileName;
	strFileName.Format("\\LOG\\log_debug_%04d_%02d_%02d.txt",stTime.wYear,stTime.wMonth,stTime.wDay);
	CStringA strPath = GetExecuteDirectory();
	strPath += strFileName;

	cCsvTable table;

    if (!table.Load(strPath))
	{
		//MessageBox(_T("최근 알람 데이터가 없습니다.."));
        return;
	}
    
	table.AddAlias("date", 0);
	table.AddAlias("code", 1);
    table.AddAlias("text", 2);

	m_vecLogList_Debug.clear();
	int nCount = 0;
    while (table.Next())
    {
		std::string date = table.AsString("date");
		std::string inst = table.AsString("inst");
        std::string text = table.AsString("text");

		int nLen = MultiByteToWideChar(CP_ACP, 0, &date[0], (unsigned int)date.size(), NULL, NULL);
		std::wstring wdate(nLen,0);
		MultiByteToWideChar(CP_ACP, 0, &date[0], (unsigned int)date.size(), &wdate[0], nLen);

		nLen = MultiByteToWideChar(CP_ACP, 0, &inst[0], (unsigned int)inst.size(), NULL, NULL);
		std::wstring winst(nLen,0);
		MultiByteToWideChar(CP_ACP, 0, &inst[0], (unsigned int)inst.size(), &winst[0], nLen);

		nLen = MultiByteToWideChar(CP_ACP, 0, &text[0], (unsigned int)text.size(), NULL, NULL);
		std::wstring wtext(nLen,0);
		MultiByteToWideChar(CP_ACP, 0, &text[0], (unsigned int)text.size(), &wtext[0], nLen);

		m_vecLogList_Debug.push_back(wdate+L" "+L"["+winst+L"]"+L" "+wtext);

		nCount++;
    }

	//m_bRefreshData = false;
}

void CLogList::Load_Data_Alarm()
{
	SYSTEMTIME stTime;
	GetSystemTime(&stTime);

	CStringA strFileName;
	strFileName.Format("\\LOG\\log_alarm_%04d_%02d_%02d.txt",stTime.wYear,stTime.wMonth,stTime.wDay);
	CStringA strPath = GetExecuteDirectory();
	strPath += strFileName;

	cCsvTable table;

    if (!table.Load(strPath))
	{
		//MessageBox(_T("최근 알람 데이터가 없습니다.."));
        return;
	}
    
	table.AddAlias("date", 0);
	table.AddAlias("inst", 1);
	table.AddAlias("code", 2);
    table.AddAlias("text", 3);

	m_vecLogList_Alarm.clear();
	int nCount = 0;
    while (table.Next())
    {
		std::string date = table.AsString("date");
		std::string inst = table.AsString("inst");
		std::string code = table.AsString("code");
        std::string text = table.AsString("text");

		int nLen = MultiByteToWideChar(CP_ACP, 0, &date[0], (unsigned int)date.size(), NULL, NULL);
		std::wstring wdate(nLen,0);
		MultiByteToWideChar(CP_ACP, 0, &date[0], (unsigned int)date.size(), &wdate[0], nLen);

		nLen = MultiByteToWideChar(CP_ACP, 0, &inst[0], (unsigned int)inst.size(), NULL, NULL);
		std::wstring winst(nLen,0);
		MultiByteToWideChar(CP_ACP, 0, &inst[0], (unsigned int)inst.size(), &winst[0], nLen);

		nLen = MultiByteToWideChar(CP_ACP, 0, &code[0], (unsigned int)code.size(), NULL, NULL);
		std::wstring wcode(nLen,0);
		MultiByteToWideChar(CP_ACP, 0, &code[0], (unsigned int)code.size(), &wcode[0], nLen);

		nLen = MultiByteToWideChar(CP_ACP, 0, &text[0], (unsigned int)text.size(), NULL, NULL);
		std::wstring wtext(nLen,0);
		MultiByteToWideChar(CP_ACP, 0, &text[0], (unsigned int)text.size(), &wtext[0], nLen);

		m_vecLogList_Alarm.push_back(wdate+L" "+L"["+winst+L"]"+L" "+L"["+wcode+L"]"+L" "+wtext);

		nCount++;
    }

//	m_bRefreshData = false;
}

void CLogList::Load_Data_Mes()
{
	SYSTEMTIME stTime;
	GetSystemTime(&stTime);

	CStringA strFileName;
	//2018-06-19
	//strFileName.Format("\\LOG\\Xml\\log_mes_%04d%02d%02d_%02d.txt",stTime.wYear,stTime.wMonth,stTime.wDay,stTime.wHour);
	strFileName.Format("\\LOG\\Xml\\log_mes_%04d-%02d-%02d_%02d.txt",stTime.wYear,stTime.wMonth,stTime.wDay,stTime.wHour);
	CStringA strPath = GetExecuteDirectory();
	strPath += strFileName;

	cCsvTable table;

    if (!table.Load(strPath))
	{
		//MessageBox(_T("최근 알람 데이터가 없습니다.."));
        return;
	}
    
	table.AddAlias("date", 0);
	table.AddAlias("code", 1);
    table.AddAlias("text", 2);

	m_vecLogList_Mes.clear();
	int nCount = 0;
    while (table.Next())
    {
		std::string date = table.AsString("date");
		std::string inst = table.AsString("inst");
        std::string text = table.AsString("text");

		int nLen = MultiByteToWideChar(CP_ACP, 0, &date[0], (unsigned int)date.size(), NULL, NULL);
		std::wstring wdate(nLen,0);
		MultiByteToWideChar(CP_ACP, 0, &date[0], (unsigned int)date.size(), &wdate[0], nLen);

		nLen = MultiByteToWideChar(CP_ACP, 0, &inst[0], (unsigned int)inst.size(), NULL, NULL);
		std::wstring winst(nLen,0);
		MultiByteToWideChar(CP_ACP, 0, &inst[0], (unsigned int)inst.size(), &winst[0], nLen);

		nLen = MultiByteToWideChar(CP_ACP, 0, &text[0], (unsigned int)text.size(), NULL, NULL);
		std::wstring wtext(nLen,0);
		MultiByteToWideChar(CP_ACP, 0, &text[0], (unsigned int)text.size(), &wtext[0], nLen);

		m_vecLogList_Mes.push_back(wdate+L" "+L"["+winst+L"]"+L" "+wtext);

		nCount++;
    }

//	m_bRefreshData = false;
}

void CLogList::Load_Data_Plc()
{
	SYSTEMTIME stTime;
	GetSystemTime(&stTime);

	CStringA strFileName;
	strFileName.Format("\\LOG\\log_plc_%04d_%02d_%02d.txt",stTime.wYear,stTime.wMonth,stTime.wDay);
	CStringA strPath = GetExecuteDirectory();
	strPath += strFileName;

	cCsvTable table;

    if (!table.Load(strPath))
	{
		//MessageBox(_T("최근 알람 데이터가 없습니다.."));
        return;
	}
    
	table.AddAlias("date", 0);
	table.AddAlias("code", 1);
    table.AddAlias("text", 2);

	m_vecLogList_Plc.clear();
	int nCount = 0;
    while (table.Next())
    {
		std::string date = table.AsString("date");
		std::string inst = table.AsString("inst");
        std::string text = table.AsString("text");

		int nLen = MultiByteToWideChar(CP_ACP, 0, &date[0], (unsigned int)date.size(), NULL, NULL);
		std::wstring wdate(nLen,0);
		MultiByteToWideChar(CP_ACP, 0, &date[0], (unsigned int)date.size(), &wdate[0], nLen);

		nLen = MultiByteToWideChar(CP_ACP, 0, &inst[0], (unsigned int)inst.size(), NULL, NULL);
		std::wstring winst(nLen,0);
		MultiByteToWideChar(CP_ACP, 0, &inst[0], (unsigned int)inst.size(), &winst[0], nLen);

		nLen = MultiByteToWideChar(CP_ACP, 0, &text[0], (unsigned int)text.size(), NULL, NULL);
		std::wstring wtext(nLen,0);
		MultiByteToWideChar(CP_ACP, 0, &text[0], (unsigned int)text.size(), &wtext[0], nLen);

		m_vecLogList_Plc.push_back(wdate+L" "+L"["+winst+L"]"+L" "+wtext);

		nCount++;
    }

//	m_bRefreshData = false;
}

bool CLogList::GetDebugLogStringFromEnd(int nIndexfromEnd, std::wstring& strText)
{
	if((int)m_vecLogList_Debug.size() <= nIndexfromEnd) return false;

	strText = m_vecLogList_Debug[m_vecLogList_Debug.size() - 1 - nIndexfromEnd];

	return true;
}

bool CLogList::GetAlarmLogStringFromEnd(int nIndexfromEnd, std::wstring& strText)
{
	if((int)m_vecLogList_Alarm.size() <= nIndexfromEnd) return false;

	strText = m_vecLogList_Alarm[m_vecLogList_Alarm.size() - 1 - nIndexfromEnd];

	return true;
}

bool CLogList::GetMesLogStringFromEnd(int nIndexfromEnd, std::wstring& strText)
{
	if((int)m_vecLogList_Mes.size() <= nIndexfromEnd) return false;

	strText = m_vecLogList_Mes[m_vecLogList_Mes.size() - 1 - nIndexfromEnd];

	return true;
}

bool CLogList::GetPlcLogStringFromEnd(int nIndexfromEnd, std::wstring& strText)
{
	if((int)m_vecLogList_Plc.size() <= nIndexfromEnd) return false;

	strText = m_vecLogList_Plc[m_vecLogList_Plc.size() - 1 - nIndexfromEnd];

	return true;
}

void CLogList::Clear_LogList()
{
	m_vecLogList_Debug.clear();
	m_vecLogList_Alarm.clear();
	m_vecLogList_Mes.clear();
	m_vecLogList_Plc.clear();
}

void CLogList::Add_LogData(int nType, std::wstring& strText)
{
	if(nType < 0 || nType > 3) return;

	SYSTEMTIME st;
	GetLocalTime(&st);
	CString strPutDate;
	strPutDate.Format( _T("[%04d-%02d-%02d/%02d:%02d:%02d_%03d] %s"),
			st.wYear, st.wMonth, st.wDay, st.wHour, st.wMinute, st.wSecond, st.wMilliseconds, strText.c_str());

	std::wstring stTemp = (LPCTSTR)strPutDate;
	std::vector<std::wstring>::iterator iter;

	if(nType == 0) // Debug
	{
		if(m_vecLogList_Debug.size() >= (unsigned int)m_nMaxStringCount)
		{
			iter = m_vecLogList_Debug.begin();
			m_vecLogList_Debug.erase(iter);
		}

		m_vecLogList_Debug.push_back(stTemp);
	}
	else if(nType == 1) // Alarm
	{
		if(m_vecLogList_Alarm.size() >= (unsigned int)m_nMaxStringCount)
		{
			iter = m_vecLogList_Alarm.begin();
			m_vecLogList_Alarm.erase(iter);
		}

		m_vecLogList_Alarm.push_back(stTemp);
	}
	else if(nType == 2) // Mes
	{
		if(m_vecLogList_Mes.size() >= (unsigned int)m_nMaxStringCount)
		{
			iter = m_vecLogList_Mes.begin();
			m_vecLogList_Mes.erase(iter);
		}

		m_vecLogList_Mes.push_back(stTemp);
	}
	else if(nType == 3) // Plc
	{
		if(m_vecLogList_Plc.size() >= (unsigned int)m_nMaxStringCount)
		{
			iter = m_vecLogList_Plc.begin();
			m_vecLogList_Plc.erase(iter);
		}

		m_vecLogList_Plc.push_back(stTemp);
	}

	m_bRefreshData[nType] = true;
}

void CLogList::Add_LogData(int nType, LPCTSTR fmt, ...)
{
	if(nType < 0 || nType > 3) return;

	// EVENT LOG 생성
	//TCHAR lpszBuffer[1024];
	TCHAR lpszBuffer[1024*10]; //Buffer Size Check
	va_list vs;
	va_start(vs, fmt);
	_vstprintf_s(lpszBuffer, fmt, vs);
	va_end(vs);

	SYSTEMTIME st;
	GetLocalTime(&st);
	CString strPutDate;
	strPutDate.Format( _T("[%04d-%02d-%02d/%02d:%02d:%02d_%03d] %s"),
			st.wYear, st.wMonth, st.wDay, st.wHour, st.wMinute, st.wSecond, st.wMilliseconds, lpszBuffer);

	std::wstring stTemp = (LPCTSTR)strPutDate;
	std::vector<std::wstring>::iterator iter;

	if(nType == 0) // Debug
	{
		if(m_vecLogList_Debug.size() >= (unsigned int)m_nMaxStringCount)
		{
			iter = m_vecLogList_Debug.begin();
			m_vecLogList_Debug.erase(iter);
		}

		m_vecLogList_Debug.push_back(stTemp);
	}
	else if(nType == 1) // Alarm
	{
		if(m_vecLogList_Alarm.size() >= (unsigned int)m_nMaxStringCount)
		{
			iter = m_vecLogList_Alarm.begin();
			m_vecLogList_Alarm.erase(iter);
		}

		m_vecLogList_Alarm.push_back(stTemp);
	}
	else if(nType == 2) // Mes
	{
		if(m_vecLogList_Mes.size() >= (unsigned int)m_nMaxStringCount)
		{
			iter = m_vecLogList_Mes.begin();
			m_vecLogList_Mes.erase(iter);
		}

		m_vecLogList_Mes.push_back(stTemp);
	}
	else if(nType == 3) // Plc
	{
		if(m_vecLogList_Plc.size() >= (unsigned int)m_nMaxStringCount)
		{
			iter = m_vecLogList_Plc.begin();
			m_vecLogList_Plc.erase(iter);
		}

		m_vecLogList_Plc.push_back(stTemp);
	}

	m_bRefreshData[nType] = true;
}
