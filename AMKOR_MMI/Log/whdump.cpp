#include "stdafx.h"

#include "whdump.h"

LPCTSTR MiniDumper::m_szAppName;

MiniDumper::MiniDumper( LPCTSTR szAppName )
{
	m_szAppName = szAppName ? _wcsdup(szAppName) : _T("Application");

    ::SetUnhandledExceptionFilter( TopLevelFilter );
}

LONG MiniDumper::TopLevelFilter( struct _EXCEPTION_POINTERS *pExceptionInfo )
{
    LONG retval = EXCEPTION_CONTINUE_SEARCH;
    HWND hParent = NULL;      // find a better value for your app

    // firstly see if dbghelp.dll is around and has the function we need
    // look next to the EXE first, as the one in System32 might be old
    // (e.g. Windows 2000)
    HMODULE hDll = NULL;
    WCHAR szDbgHelpPath[_MAX_PATH];

    if (GetModuleFileName( NULL, szDbgHelpPath, _MAX_PATH ))
    {
        WCHAR *pSlash = _tcsrchr( szDbgHelpPath, '\\' );
        if (pSlash)
        {
            _tcscpy( pSlash+1, _T("DBGHELP.DLL") );
            hDll = ::LoadLibrary( szDbgHelpPath );
        }
    }

    if (hDll==NULL)
    {
        // load any version we can
        hDll = ::LoadLibrary( _T("DBGHELP.DLL") );
    }

    LPCTSTR szResult = NULL;

    if (hDll)
    {
       MINIDUMPWRITEDUMP pDump = (MINIDUMPWRITEDUMP)::GetProcAddress( hDll, "MiniDumpWriteDump" );
        if (pDump)
        {
            WCHAR szDumpPath[_MAX_PATH];
            WCHAR szScratch[_MAX_PATH];

			CString ReportDate;
			int nYear, nMonth, nDay, nHour, nMin, nSec;
			CTime cur_time = CTime::GetTickCount();       
			nYear = cur_time.GetYear();  
			nMonth = cur_time.GetMonth();  
			nDay = cur_time.GetDay();
			nHour = cur_time.GetHour();
			nMin = cur_time.GetMinute();
			nSec = cur_time.GetSecond();
			ReportDate.Format(_T("_%04d%02d%02d%02d%02d%02d"), nYear, nMonth, nDay, nHour, nMin, nSec);

            // work out a good place for the dump file
            //if (!GetTempPath( _MAX_PATH, szDumpPath ))
            _tcscpy( szDumpPath, _T("C:\\HPTi200\\") );

            _tcscat( szDumpPath, m_szAppName );
			_tcscat( szDumpPath, ReportDate );
            _tcscat( szDumpPath, _T(".dmp") );

            // ask the user if they want to save a dump file
            if (::MessageBox( NULL, _T("프로그램 문제 발생. 덤프파일 저장하시겠습니까?"), m_szAppName, MB_YESNO )==IDYES)
            {
                // create the file
               HANDLE hFile = ::CreateFile( szDumpPath, GENERIC_WRITE, FILE_SHARE_WRITE, NULL, CREATE_ALWAYS,
                    FILE_ATTRIBUTE_NORMAL, NULL );

                if (hFile!=INVALID_HANDLE_VALUE)
                {
                    _MINIDUMP_EXCEPTION_INFORMATION ExInfo;

                    ExInfo.ThreadId = ::GetCurrentThreadId();
                    ExInfo.ExceptionPointers = pExceptionInfo;
                    ExInfo.ClientPointers = NULL;

					MINIDUMP_TYPE mdt = (MINIDUMP_TYPE)( MiniDumpWithPrivateReadWriteMemory | MiniDumpWithDataSegs | MiniDumpWithFullMemoryInfo | MiniDumpWithThreadInfo ); 

                    // write the dump
                   BOOL bOK = pDump( GetCurrentProcess(), GetCurrentProcessId(), hFile, mdt, &ExInfo, NULL, NULL );
                    if (bOK)
                    {
                        swprintf( szScratch, _T("Saved dump file to '%s'"), szDumpPath );
                        szResult = szScratch;
                        retval = EXCEPTION_EXECUTE_HANDLER;
                    }
                    else
                    {
                        swprintf( szScratch, _T("Failed to save dump file to '%s' (error %d)"), szDumpPath, GetLastError() );
                        szResult = szScratch;
                    }
                    ::CloseHandle(hFile);
                }
                else
                {
                    swprintf( szScratch, _T("Failed to create dump file '%s' (error %d)"), szDumpPath, GetLastError() );
                    szResult = szScratch;
                }
            }
        }
        else
        {
            szResult = _T("DBGHELP.DLL too old");
        }
    }
    else
    {
        szResult = _T("DBGHELP.DLL not found");
    }

    if (szResult)
        ::MessageBox( NULL, szResult, m_szAppName, MB_OK );

    return retval;
}