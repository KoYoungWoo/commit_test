#pragma once

enum eLog_Type{
	eLog_DEBUG = 0,
	eLog_ALARM = 1,
	eLog_MES = 2,
	eLog_PLC = 3,
	eLog_Type_MAX = 4,
};

class CLogListGC;
class CLogList
{
public:
	friend class CLogListGC;
	static CLogList* Instance();

	void Load_Data_Debug();
	void Load_Data_Alarm();
	void Load_Data_Mes();
	void Load_Data_Plc();

	void SetRefresh(int nType, bool bRefresh) { m_bRefreshData[nType] = bRefresh; };
	bool IsRefresh(int nType) { return m_bRefreshData[nType]; };

	bool GetDebugLogStringFromEnd(int nIndexfromEnd, std::wstring& strText);
	bool GetAlarmLogStringFromEnd(int nIndexfromEnd, std::wstring& strText);
	bool GetMesLogStringFromEnd(int nIndexfromEnd, std::wstring& strText);
	bool GetPlcLogStringFromEnd(int nIndexfromEnd, std::wstring& strText);

	void Clear_LogList();

	int GetMaxStringCount() { return m_nMaxStringCount; };
	void SetMaxStringCount(int nCnt) { m_nMaxStringCount = nCnt; };

	void Add_LogData(int nType, std::wstring& strText);
	void Add_LogData(int nType, LPCTSTR fmt, ...);

private:
	//std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;
	std::vector<std::wstring> m_vecLogList_Debug;
	std::vector<std::wstring> m_vecLogList_Alarm;
	std::vector<std::wstring> m_vecLogList_Mes;
	std::vector<std::wstring> m_vecLogList_Plc;

	int m_nMaxStringCount;
	bool m_bRefreshData[4];
	static CLogList* m_pThis;

	CLogList(void);
	~CLogList(void);

	CStringA GetExecuteDirectory();
};

