#pragma once

// 컴퓨터에서 Microsoft Visual C++를 사용하여 생성한 IDispatch 래퍼 클래스입니다.

// 참고: 이 파일의 내용을 수정하지 마십시오. Microsoft Visual C++에서
//  이 클래스를 다시 생성할 때 수정한 내용을 덮어씁니다.

/////////////////////////////////////////////////////////////////////////////
// CExgem3ctrl1 래퍼 클래스입니다.

class CExgem3ctrl1 : public CWnd
{
protected:
	DECLARE_DYNCREATE(CExgem3ctrl1)
public:
	CLSID const& GetClsid()
	{
		static CLSID const clsid
			= { 0xE33F1005, 0x19A2, 0x4851, { 0x9E, 0xDE, 0xC5, 0x53, 0x39, 0xB5, 0x36, 0x44 } };
		return clsid;
	}
	virtual BOOL Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle,
						const RECT& rect, CWnd* pParentWnd, UINT nID, 
						CCreateContext* pContext = NULL)
	{ 
		return CreateControl(GetClsid(), lpszWindowName, dwStyle, rect, pParentWnd, nID); 
	}

    BOOL Create(LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, 
				UINT nID, CFile* pPersist = NULL, BOOL bStorage = FALSE,
				BSTR bstrLicKey = NULL)
	{ 
		return CreateControl(GetClsid(), lpszWindowName, dwStyle, rect, pParentWnd, nID,
		pPersist, bStorage, bstrLicKey); 
	}

// 특성입니다.
public:


// 작업입니다.
public:

// _DeXGem3

// Functions
//

	void AboutBox()
	{
		InvokeHelper(DISPID_ABOUTBOX, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	long Initialize(LPCTSTR sCfg)
	{
		long result;
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x4, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sCfg);
		return result;
	}
	long Start()
	{
		long result;
		InvokeHelper(0x5, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
		return result;
	}
	long Stop()
	{
		long result;
		InvokeHelper(0x6, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
		return result;
	}
	long Close()
	{
		long result;
		InvokeHelper(0x7, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
		return result;
	}
	long MakeObject(long * pnObjectID)
	{
		long result;
		static BYTE parms[] = VTS_PI4 ;
		InvokeHelper(0x8, DISPATCH_METHOD, VT_I4, (void*)&result, parms, pnObjectID);
		return result;
	}
	long SetList(long nObjectID, long nItemCount)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_I4 ;
		InvokeHelper(0x9, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nObjectID, nItemCount);
		return result;
	}
	long GetList(long nObjectID, long * pnItemCount)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_PI4 ;
		InvokeHelper(0xa, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nObjectID, pnItemCount);
		return result;
	}
	long GEMSetVariables(long nObjectID, long nVid)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_I4 ;
		InvokeHelper(0xb, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nObjectID, nVid);
		return result;
	}
	long SendSECSMessage(long nObjectID, long nStream, long nFunction, long nSysbyte)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_I4 VTS_I4 VTS_I4 ;
		InvokeHelper(0xc, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nObjectID, nStream, nFunction, nSysbyte);
		return result;
	}
	long GEMReqOffline()
	{
		long result;
		InvokeHelper(0xd, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
		return result;
	}
	long GEMReqLocal()
	{
		long result;
		InvokeHelper(0xe, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
		return result;
	}
	long GEMReqRemote()
	{
		long result;
		InvokeHelper(0xf, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
		return result;
	}
	long GEMSetEstablish(long bState)
	{
		long result;
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x10, DISPATCH_METHOD, VT_I4, (void*)&result, parms, bState);
		return result;
	}
	long GEMSetParam(LPCTSTR sParamName, LPCTSTR sParamValue)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_BSTR ;
		InvokeHelper(0x11, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sParamName, sParamValue);
		return result;
	}
	long GEMGetParam(LPCTSTR sParamName, BSTR * psParamValue)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_PBSTR ;
		InvokeHelper(0x12, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sParamName, psParamValue);
		return result;
	}
	long GEMEQInitialized(long nInitType)
	{
		long result;
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x13, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nInitType);
		return result;
	}
	long GEMReqGetDateTime()
	{
		long result;
		InvokeHelper(0x14, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
		return result;
	}
	long GEMRspGetDateTime(long nMsgId, LPCTSTR sSystemTime)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_BSTR ;
		InvokeHelper(0x15, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nMsgId, sSystemTime);
		return result;
	}
	long GEMRspDateTime(long nMsgId, long nResult)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_I4 ;
		InvokeHelper(0x16, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nMsgId, nResult);
		return result;
	}
	long GEMSetAlarm(long nID, long nState)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_I4 ;
		InvokeHelper(0x17, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nID, nState);
		return result;
	}
	long GEMRspRemoteCommand(long nMsgId, LPCTSTR sCmd, long nHCAck, long nCount, BSTR * psCpName, long * pnCpAck)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_BSTR VTS_I4 VTS_I4 VTS_PBSTR VTS_PI4 ;
		InvokeHelper(0x18, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nMsgId, sCmd, nHCAck, nCount, psCpName, pnCpAck);
		return result;
	}
	long GEMRspChangeECV(long nMsgId, long nResult)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_I4 ;
		InvokeHelper(0x19, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nMsgId, nResult);
		return result;
	}
	long GEMSetECVChanged(long nCount, long * pnEcIds, BSTR * psEcVals)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_PI4 VTS_PBSTR ;
		InvokeHelper(0x1a, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nCount, pnEcIds, psEcVals);
		return result;
	}
	long GEMSetPPChanged(long nMode, LPCTSTR sPpid, long nSize, LPCTSTR sBody)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_BSTR VTS_I4 VTS_BSTR ;
		InvokeHelper(0x1b, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nMode, sPpid, nSize, sBody);
		return result;
	}
	long GEMSetPPFmtChanged(long nMode, LPCTSTR sPpid, LPCTSTR sMdln, LPCTSTR sSoftRev, long nCount, BSTR * psCCode, long * pnParamCount, BSTR * psParamNames)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_BSTR VTS_BSTR VTS_BSTR VTS_I4 VTS_PBSTR VTS_PI4 VTS_PBSTR ;
		InvokeHelper(0x1c, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nMode, sPpid, sMdln, sSoftRev, nCount, psCCode, pnParamCount, psParamNames);
		return result;
	}
	long GEMReqPPLoadInquire(LPCTSTR sPpid, long nLength)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_I4 ;
		InvokeHelper(0x1d, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sPpid, nLength);
		return result;
	}
	long GEMRspPPLoadInquire(long nMsgId, LPCTSTR sPpid, long nResult)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_BSTR VTS_I4 ;
		InvokeHelper(0x1e, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nMsgId, sPpid, nResult);
		return result;
	}
	long GEMReqPPSend(LPCTSTR sPpid, LPCTSTR sBody)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_BSTR ;
		InvokeHelper(0x1f, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sPpid, sBody);
		return result;
	}
	long GEMRspPPSend(long nMsgId, LPCTSTR sPpid, long nResult)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_BSTR VTS_I4 ;
		InvokeHelper(0x20, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nMsgId, sPpid, nResult);
		return result;
	}
	long GEMReqPP(LPCTSTR sPpid)
	{
		long result;
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x21, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sPpid);
		return result;
	}
	long GEMRspPP(long nMsgId, LPCTSTR sPpid, LPCTSTR sBody)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_BSTR VTS_BSTR ;
		InvokeHelper(0x22, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nMsgId, sPpid, sBody);
		return result;
	}
	long GEMRspPPDelete(long nMsgId, long nCount, BSTR * psPpids, long nResult)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_I4 VTS_PBSTR VTS_I4 ;
		InvokeHelper(0x23, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nMsgId, nCount, psPpids, nResult);
		return result;
	}
	long GEMRspPPList(long nMsgId, long nCount, BSTR * psPpids)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_I4 VTS_PBSTR ;
		InvokeHelper(0x24, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nMsgId, nCount, psPpids);
		return result;
	}
	long GEMReqPPFmtSend(LPCTSTR sPpid, LPCTSTR sMdln, LPCTSTR sSoftRev, long nCount, BSTR * psCCode, long * pnParamCount, BSTR * psParamNames)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_BSTR VTS_BSTR VTS_I4 VTS_PBSTR VTS_PI4 VTS_PBSTR ;
		InvokeHelper(0x25, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sPpid, sMdln, sSoftRev, nCount, psCCode, pnParamCount, psParamNames);
		return result;
	}
	long GEMRspPPFmtSend(long nMsgId, LPCTSTR sPpid, long nResult)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_BSTR VTS_I4 ;
		InvokeHelper(0x26, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nMsgId, sPpid, nResult);
		return result;
	}
	long GEMReqPPFmt(LPCTSTR sPpid)
	{
		long result;
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x27, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sPpid);
		return result;
	}
	long GEMRspPPFmt(long nMsgId, LPCTSTR sPpid, LPCTSTR sMdln, LPCTSTR sSoftRev, long nCount, BSTR * psCCode, long * pnParamCount, BSTR * psParamNames)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_BSTR VTS_BSTR VTS_BSTR VTS_I4 VTS_PBSTR VTS_PI4 VTS_PBSTR ;
		InvokeHelper(0x28, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nMsgId, sPpid, sMdln, sSoftRev, nCount, psCCode, pnParamCount, psParamNames);
		return result;
	}
	long GEMReqPPFmtVerification(LPCTSTR sPpid, long nCount, long * pnAck, BSTR * psSeqNumber, BSTR * psError)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_I4 VTS_PI4 VTS_PBSTR VTS_PBSTR ;
		InvokeHelper(0x29, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sPpid, nCount, pnAck, psSeqNumber, psError);
		return result;
	}
	long GEMSetTerminalMessage(long nTid, LPCTSTR sMsg)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_BSTR ;
		InvokeHelper(0x2a, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nTid, sMsg);
		return result;
	}
	long GEMSetVariable(long nCount, long * pnVid, BSTR * psValue)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_PI4 VTS_PBSTR ;
		InvokeHelper(0x2b, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nCount, pnVid, psValue);
		return result;
	}
	long GEMEnableLog(long bEnabled)
	{
		long result;
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x2c, DISPATCH_METHOD, VT_I4, (void*)&result, parms, bEnabled);
		return result;
	}
	long GEMSetLogOption(LPCTSTR sDriectory, LPCTSTR sPrefix, LPCTSTR sExtension, long nKeepDay, long bMakeDailyLog, long bMakeSubDirectory)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_BSTR VTS_BSTR VTS_I4 VTS_I4 VTS_I4 ;
		InvokeHelper(0x2d, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sDriectory, sPrefix, sExtension, nKeepDay, bMakeDailyLog, bMakeSubDirectory);
		return result;
	}
	long GEMSetEvent(long nEventID)
	{
		long result;
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x2e, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nEventID);
		return result;
	}
	long GEMGetVariable(long nCount, long * pnVid, BSTR * psValue)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_PI4 VTS_PBSTR ;
		InvokeHelper(0x2f, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nCount, pnVid, psValue);
		return result;
	}
	long GEMSetSpecificMessage(long nObjectID, LPCTSTR sMsgName)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_BSTR ;
		InvokeHelper(0x30, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nObjectID, sMsgName);
		return result;
	}
	long GEMGetSpecificMessage(long nObjectID, long * pnRObjectID, LPCTSTR sMsgName)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_PI4 VTS_BSTR ;
		InvokeHelper(0x31, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nObjectID, pnRObjectID, sMsgName);
		return result;
	}
	long GetAllStringItem(long nObjectID, BSTR * psValue)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_PBSTR ;
		InvokeHelper(0x32, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nObjectID, psValue);
		return result;
	}
	long SetAllStringItem(long nObjectID, LPCTSTR sValue)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_BSTR ;
		InvokeHelper(0x33, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nObjectID, sValue);
		return result;
	}
	long GEMSetVarName(long nCount, BSTR * psVidName, BSTR * psValue)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_PBSTR VTS_PBSTR ;
		InvokeHelper(0x34, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nCount, psVidName, psValue);
		return result;
	}
	long GEMGetVarName(long nCount, BSTR * psVidName, BSTR * psValue)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_PBSTR VTS_PBSTR ;
		InvokeHelper(0x35, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nCount, psVidName, psValue);
		return result;
	}
	long CloseObject(long nObjectID)
	{
		long result;
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x36, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nObjectID);
		return result;
	}
	long GEMReqAllECInfo()
	{
		long result;
		InvokeHelper(0x37, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
		return result;
	}
	long SetBinary(long nObjectID, short * pnValue, long nItemCount)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_PI2 VTS_I4 ;
		InvokeHelper(0x38, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nObjectID, pnValue, nItemCount);
		return result;
	}
	long SetBool(long nObjectID, short * pnValue, long nItemCount)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_PI2 VTS_I4 ;
		InvokeHelper(0x39, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nObjectID, pnValue, nItemCount);
		return result;
	}
	long SetU1(long nObjectID, short * pnValue, long nItemCount)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_PI2 VTS_I4 ;
		InvokeHelper(0x3a, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nObjectID, pnValue, nItemCount);
		return result;
	}
	long SetU2(long nObjectID, long * pnValue, long nItemCount)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_PI4 VTS_I4 ;
		InvokeHelper(0x3b, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nObjectID, pnValue, nItemCount);
		return result;
	}
	long SetU4(long nObjectID, double * prValue, long nItemCount)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_PR8 VTS_I4 ;
		InvokeHelper(0x3c, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nObjectID, prValue, nItemCount);
		return result;
	}
	long SetU8(long nObjectID, double * prValue, long nItemCount)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_PR8 VTS_I4 ;
		InvokeHelper(0x3d, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nObjectID, prValue, nItemCount);
		return result;
	}
	long SetI1(long nObjectID, short * pnValue, long nItemCount)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_PI2 VTS_I4 ;
		InvokeHelper(0x3e, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nObjectID, pnValue, nItemCount);
		return result;
	}
	long SetI2(long nObjectID, short * pnValue, long nItemCount)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_PI2 VTS_I4 ;
		InvokeHelper(0x3f, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nObjectID, pnValue, nItemCount);
		return result;
	}
	long SetI4(long nObjectID, long * plValue, long nItemCount)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_PI4 VTS_I4 ;
		InvokeHelper(0x40, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nObjectID, plValue, nItemCount);
		return result;
	}
	long SetI8(long nObjectID, long * plValue, long nItemCount)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_PI4 VTS_I4 ;
		InvokeHelper(0x41, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nObjectID, plValue, nItemCount);
		return result;
	}
	long SetF4(long nObjectID, float * prValue, long nItemCount)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_PR4 VTS_I4 ;
		InvokeHelper(0x42, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nObjectID, prValue, nItemCount);
		return result;
	}
	long SetF8(long nObjectID, double * prValue, long nItemCount)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_PR8 VTS_I4 ;
		InvokeHelper(0x43, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nObjectID, prValue, nItemCount);
		return result;
	}
	long SetAscii(long nObjectID, LPCTSTR pszValue, long nItemCount)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_BSTR VTS_I4 ;
		InvokeHelper(0x44, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nObjectID, pszValue, nItemCount);
		return result;
	}
	long GetBinary(long nObjectID, short * pnValue, long * pnItemCount, long nSize)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_PI2 VTS_PI4 VTS_I4 ;
		InvokeHelper(0x45, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nObjectID, pnValue, pnItemCount, nSize);
		return result;
	}
	long GetBool(long nObjectID, short * pnValue, long * pnItemCount, long nSize)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_PI2 VTS_PI4 VTS_I4 ;
		InvokeHelper(0x46, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nObjectID, pnValue, pnItemCount, nSize);
		return result;
	}
	long GetU1(long nObjectID, short * pnValue, long * pnItemCount, long nSize)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_PI2 VTS_PI4 VTS_I4 ;
		InvokeHelper(0x47, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nObjectID, pnValue, pnItemCount, nSize);
		return result;
	}
	long GetU2(long nObjectID, long * pnValue, long * pnItemCount, long nSize)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_PI4 VTS_PI4 VTS_I4 ;
		InvokeHelper(0x48, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nObjectID, pnValue, pnItemCount, nSize);
		return result;
	}
	long GetU4(long nObjectID, double * prValue, long * pnItemCount, long nSize)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_PR8 VTS_PI4 VTS_I4 ;
		InvokeHelper(0x49, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nObjectID, prValue, pnItemCount, nSize);
		return result;
	}
	long GetU8(long nObjectID, double * prValue, long * pnItemCount, long nSize)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_PR8 VTS_PI4 VTS_I4 ;
		InvokeHelper(0x4a, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nObjectID, prValue, pnItemCount, nSize);
		return result;
	}
	long GetI1(long nObjectID, short * pnValue, long * pnItemCount, long nSize)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_PI2 VTS_PI4 VTS_I4 ;
		InvokeHelper(0x4b, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nObjectID, pnValue, pnItemCount, nSize);
		return result;
	}
	long GetI2(long nObjectID, short * pnValue, long * pnItemCount, long nSize)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_PI2 VTS_PI4 VTS_I4 ;
		InvokeHelper(0x4c, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nObjectID, pnValue, pnItemCount, nSize);
		return result;
	}
	long GetI4(long nObjectID, long * pnValue, long * pnItemCount, long nSize)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_PI4 VTS_PI4 VTS_I4 ;
		InvokeHelper(0x4d, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nObjectID, pnValue, pnItemCount, nSize);
		return result;
	}
	long GetI8(long nObjectID, long * pnValue, long * pnItemCount, long nSize)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_PI4 VTS_PI4 VTS_I4 ;
		InvokeHelper(0x4e, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nObjectID, pnValue, pnItemCount, nSize);
		return result;
	}
	long GetF4(long nObjectID, float * prValue, long * pnItemCount, long nSize)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_PR4 VTS_PI4 VTS_I4 ;
		InvokeHelper(0x4f, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nObjectID, prValue, pnItemCount, nSize);
		return result;
	}
	long GetF8(long nObjectID, double * prValue, long * pnItemCount, long nSize)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_PR8 VTS_PI4 VTS_I4 ;
		InvokeHelper(0x50, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nObjectID, prValue, pnItemCount, nSize);
		return result;
	}
	long GetAscii(long nObjectID, BSTR * psValue, long * pnItemCount)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_PBSTR VTS_PI4 ;
		InvokeHelper(0x51, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nObjectID, psValue, pnItemCount);
		return result;
	}
	long GEMReqPPSendEx(LPCTSTR sPpid, LPCTSTR sRecipePath)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_BSTR ;
		InvokeHelper(0x52, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sPpid, sRecipePath);
		return result;
	}
	long GEMRspPPEx(long nMsgId, LPCTSTR sPpid, LPCTSTR sRecipePath)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_BSTR VTS_BSTR ;
		InvokeHelper(0x53, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nMsgId, sPpid, sRecipePath);
		return result;
	}
	long GEMRspPPSendEx(long nMsgId, LPCTSTR sPpid, LPCTSTR sRecipePath, long nResult)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_BSTR VTS_BSTR VTS_I4 ;
		InvokeHelper(0x54, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nMsgId, sPpid, sRecipePath, nResult);
		return result;
	}
	long GEMReqPPEx(LPCTSTR sPpid, LPCTSTR sRecipePath)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_BSTR ;
		InvokeHelper(0x55, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sPpid, sRecipePath);
		return result;
	}
	long SetAsciiEx(long nObjectID, short * pnValue, long nItemCount)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_PI2 VTS_I4 ;
		InvokeHelper(0x56, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nObjectID, pnValue, nItemCount);
		return result;
	}
	long GetAsciiEx(long nObjectID, short * pnValue, long * pnItemCount, long nSize)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_PI2 VTS_PI4 VTS_I4 ;
		InvokeHelper(0x57, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nObjectID, pnValue, pnItemCount, nSize);
		return result;
	}
	long GEMSetVariableEx(long nObjectID, long nVid)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_I4 ;
		InvokeHelper(0x58, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nObjectID, nVid);
		return result;
	}
	long GEMReqLoopback(long nCount, long * pnAbs)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_PI4 ;
		InvokeHelper(0x59, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nCount, pnAbs);
		return result;
	}
	long GEMSetEventEnable(long nCount, long * pnCEIDs, long nEnable)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_PI4 VTS_I4 ;
		InvokeHelper(0x5a, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nCount, pnCEIDs, nEnable);
		return result;
	}
	long GEMSetAlarmEnable(long nCount, long * pnALIDs, long nEnable)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_PI4 VTS_I4 ;
		InvokeHelper(0x5b, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nCount, pnALIDs, nEnable);
		return result;
	}
	long GEMGetEventEnable(long nCount, long * pnCEIDs, long * pnEnable)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_PI4 VTS_PI4 ;
		InvokeHelper(0x5c, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nCount, pnCEIDs, pnEnable);
		return result;
	}
	long GEMGetAlarmEnable(long nCount, long * pnALIDs, long * pnEnable)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_PI4 VTS_PI4 ;
		InvokeHelper(0x5d, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nCount, pnALIDs, pnEnable);
		return result;
	}
	long GEMGetAlarmInfo(long nCount, long * pnALIDs, long * pnALCDs, BSTR * psALTXs)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_PI4 VTS_PI4 VTS_PBSTR ;
		InvokeHelper(0x5e, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nCount, pnALIDs, pnALCDs, psALTXs);
		return result;
	}
	long GEMGetSVInfo(long nCount, long * pnSVIDs, BSTR * psMins, BSTR * psMaxs)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_PI4 VTS_PBSTR VTS_PBSTR ;
		InvokeHelper(0x5f, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nCount, pnSVIDs, psMins, psMaxs);
		return result;
	}
	long GEMGetECVInfo(long nCount, long * pnEcIds, BSTR * psNames, BSTR * psDefs, BSTR * psMins, BSTR * psMaxs, BSTR * psUnits)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_PI4 VTS_PBSTR VTS_PBSTR VTS_PBSTR VTS_PBSTR VTS_PBSTR ;
		InvokeHelper(0x60, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nCount, pnEcIds, psNames, psDefs, psMins, psMaxs, psUnits);
		return result;
	}
	long GEMRspOffline(long nMsgId, long nAck)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_I4 ;
		InvokeHelper(0x61, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nMsgId, nAck);
		return result;
	}
	long GEMRspOnline(long nMsgId, long nAck)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_I4 ;
		InvokeHelper(0x62, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nMsgId, nAck);
		return result;
	}
	long GEMReqHostOffline()
	{
		long result;
		InvokeHelper(0x63, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
		return result;
	}
	long GEMReqStartPolling(LPCTSTR sName, long nScanTime)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_I4 ;
		InvokeHelper(0x64, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sName, nScanTime);
		return result;
	}
	long GEMReqStopPolling(LPCTSTR sName)
	{
		long result;
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x65, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sName);
		return result;
	}
	long CMSSetLPInfo(LPCTSTR sLocID, long nTransferState, long nAccessMode, long nReservationState, long nAssociationState, LPCTSTR sCarrierID)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_BSTR ;
		InvokeHelper(0x66, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sLocID, nTransferState, nAccessMode, nReservationState, nAssociationState, sCarrierID);
		return result;
	}
	long CMSSetCarrierLocationInfo(LPCTSTR sLocID, LPCTSTR sCarrierID)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_BSTR ;
		InvokeHelper(0x67, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sLocID, sCarrierID);
		return result;
	}
	long CMSSetPresenceSensor(LPCTSTR sLocID, long nState)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_I4 ;
		InvokeHelper(0x68, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sLocID, nState);
		return result;
	}
	long CMSSetCarrierOnOff(LPCTSTR sLocID, long nState)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_I4 ;
		InvokeHelper(0x69, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sLocID, nState);
		return result;
	}
	long CMSSetPIOSignalState(LPCTSTR sLocID, long nSignal, long nState)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_I4 VTS_I4 ;
		InvokeHelper(0x6a, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sLocID, nSignal, nState);
		return result;
	}
	long CMSSetReadyToLoad(LPCTSTR sLocID)
	{
		long result;
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x6b, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sLocID);
		return result;
	}
	long CMSSetReadyToUnload(LPCTSTR sLocID)
	{
		long result;
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x6c, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sLocID);
		return result;
	}
	long CMSSetCarrierMovement(LPCTSTR sLocID, LPCTSTR sCarrierID)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_BSTR ;
		InvokeHelper(0x6d, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sLocID, sCarrierID);
		return result;
	}
	long CMSSetCarrierAccessing(LPCTSTR sLocID, long nState, LPCTSTR sCarrierID)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_I4 VTS_BSTR ;
		InvokeHelper(0x6e, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sLocID, nState, sCarrierID);
		return result;
	}
	long CMSSetCarrierID(LPCTSTR sLocID, LPCTSTR sCarrierID, long nResult)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_BSTR VTS_I4 ;
		InvokeHelper(0x6f, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sLocID, sCarrierID, nResult);
		return result;
	}
	long CMSSetSlotMap(LPCTSTR sLocID, LPCTSTR sSlotMap, LPCTSTR sCarrierID, long nResult)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_BSTR VTS_BSTR VTS_I4 ;
		InvokeHelper(0x70, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sLocID, sSlotMap, sCarrierID, nResult);
		return result;
	}
	long CMSReqBind(LPCTSTR sLocID, LPCTSTR sCarrierID, LPCTSTR sSlotMap)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_BSTR VTS_BSTR ;
		InvokeHelper(0x71, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sLocID, sCarrierID, sSlotMap);
		return result;
	}
	long CMSReqCancelBind(LPCTSTR sLocID, LPCTSTR sCarrierID)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_BSTR ;
		InvokeHelper(0x72, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sLocID, sCarrierID);
		return result;
	}
	long CMSReqCancelCarrier(LPCTSTR sLocID, LPCTSTR sCarrierID)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_BSTR ;
		InvokeHelper(0x73, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sLocID, sCarrierID);
		return result;
	}
	long CMSRspCancelCarrier(long nMsgId, LPCTSTR sLocID, LPCTSTR sCarrierID, long nResult, long nErrCount, long * pnErrCode, BSTR * psErrText)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_BSTR VTS_BSTR VTS_I4 VTS_I4 VTS_PI4 VTS_PBSTR ;
		InvokeHelper(0x74, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nMsgId, sLocID, sCarrierID, nResult, nErrCount, pnErrCode, psErrText);
		return result;
	}
	long CMSRspCancelCarrierAtPort(long nMsgId, LPCTSTR sLocID, long nResult, long nErrCount, long * pnErrCode, BSTR * psErrText)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_BSTR VTS_I4 VTS_I4 VTS_PI4 VTS_PBSTR ;
		InvokeHelper(0x75, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nMsgId, sLocID, nResult, nErrCount, pnErrCode, psErrText);
		return result;
	}
	long CMSReqCarrierIn(LPCTSTR sLocID, LPCTSTR sCarrierID)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_BSTR ;
		InvokeHelper(0x76, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sLocID, sCarrierID);
		return result;
	}
	long CMSReqProceedCarrier(LPCTSTR sLocID, LPCTSTR sCarrierID, LPCTSTR sSlotMap, long nCount, BSTR * psLotID, BSTR * psSubstrateID, LPCTSTR sUsage)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_BSTR VTS_BSTR VTS_I4 VTS_PBSTR VTS_PBSTR VTS_BSTR ;
		InvokeHelper(0x77, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sLocID, sCarrierID, sSlotMap, nCount, psLotID, psSubstrateID, sUsage);
		return result;
	}
	long CMSReqChangeServiceStatus(LPCTSTR sLocID, long nState)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_I4 ;
		InvokeHelper(0x78, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sLocID, nState);
		return result;
	}
	long CMSReqCarrierOut(LPCTSTR sLocID, LPCTSTR sCarrierID)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_BSTR ;
		InvokeHelper(0x79, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sLocID, sCarrierID);
		return result;
	}
	long CMSRspCarrierOut(long nMsgId, LPCTSTR sLocID, LPCTSTR sCarrierID, long nResult, long nErrCount, long * pnErrCode, BSTR * psErrText)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_BSTR VTS_BSTR VTS_I4 VTS_I4 VTS_PI4 VTS_PBSTR ;
		InvokeHelper(0x7a, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nMsgId, sLocID, sCarrierID, nResult, nErrCount, pnErrCode, psErrText);
		return result;
	}
	long CMSReqChangeAccess(long nMode, LPCTSTR sLocID)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_BSTR ;
		InvokeHelper(0x7b, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nMode, sLocID);
		return result;
	}
	long CMSRspChangeAccess(long nMsgId, long nMode, long nResult, long nErrCount, BSTR * psLocID, long * pnErrCode, BSTR * psErrText)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_PBSTR VTS_PI4 VTS_PBSTR ;
		InvokeHelper(0x7c, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nMsgId, nMode, nResult, nErrCount, psLocID, pnErrCode, psErrText);
		return result;
	}
	long CMSRspChangeServiceStatus(long nMsgId, LPCTSTR sLocID, long nState, long nResult, long nErrCount, long * pnErrCode, BSTR * psErrText)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_BSTR VTS_I4 VTS_I4 VTS_I4 VTS_PI4 VTS_PBSTR ;
		InvokeHelper(0x7d, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nMsgId, sLocID, nState, nResult, nErrCount, pnErrCode, psErrText);
		return result;
	}
	long CMSGetAllCarrierInfo(long * pnMsgId, long * pnCount)
	{
		long result;
		static BYTE parms[] = VTS_PI4 VTS_PI4 ;
		InvokeHelper(0x7e, DISPATCH_METHOD, VT_I4, (void*)&result, parms, pnMsgId, pnCount);
		return result;
	}
	long CMSDelCarrierInfo(LPCTSTR sCarrierID)
	{
		long result;
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x7f, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sCarrierID);
		return result;
	}
	long GetCarrierID(long nMsgId, long nIndex, BSTR * psCarrierID)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_I4 VTS_PBSTR ;
		InvokeHelper(0x80, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nMsgId, nIndex, psCarrierID);
		return result;
	}
	long GetCarrierLocID(long nMsgId, long nIndex, BSTR * psLocID)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_I4 VTS_PBSTR ;
		InvokeHelper(0x81, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nMsgId, nIndex, psLocID);
		return result;
	}
	long GetCarrierIDStatus(long nMsgId, long nIndex, long * pnState)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_I4 VTS_PI4 ;
		InvokeHelper(0x82, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nMsgId, nIndex, pnState);
		return result;
	}
	long GetCarrierSlotMapStatus(long nMsgId, long nIndex, long * pnState)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_I4 VTS_PI4 ;
		InvokeHelper(0x83, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nMsgId, nIndex, pnState);
		return result;
	}
	long GetCarrierAccessingStatus(long nMsgId, long nIndex, long * pnState)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_I4 VTS_PI4 ;
		InvokeHelper(0x84, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nMsgId, nIndex, pnState);
		return result;
	}
	long GetCarrierContentsMapCount(long nMsgId, long nIndex, long * pnCount)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_I4 VTS_PI4 ;
		InvokeHelper(0x85, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nMsgId, nIndex, pnCount);
		return result;
	}
	long GetCarrierContentsMap(long nMsgId, long nIndex, long nCount, BSTR * psLotID, BSTR * psSubstrateID)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_I4 VTS_I4 VTS_PBSTR VTS_PBSTR ;
		InvokeHelper(0x86, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nMsgId, nIndex, nCount, psLotID, psSubstrateID);
		return result;
	}
	long GetCarrierSlotMap(long nMsgId, long nIndex, BSTR * psSlotMap)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_I4 VTS_PBSTR ;
		InvokeHelper(0x87, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nMsgId, nIndex, psSlotMap);
		return result;
	}
	long GetCarrierUsage(long nMsgId, long nIndex, BSTR * psUsage)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_I4 VTS_PBSTR ;
		InvokeHelper(0x88, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nMsgId, nIndex, psUsage);
		return result;
	}
	long GetCarrierClose(long nMsgId)
	{
		long result;
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x89, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nMsgId);
		return result;
	}
	long CMSDelAllCarrierInfo()
	{
		long result;
		InvokeHelper(0x8a, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
		return result;
	}
	long CMSSetCarrierInfo(LPCTSTR sCarrierID, LPCTSTR sLocID, long nIdStatus, long nSlotMapStatus, long nAccessingStatus, LPCTSTR sSlotMap, long nContentsMapCount, BSTR * psLotID, BSTR * psSubstrateID, LPCTSTR sUsage)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_BSTR VTS_I4 VTS_I4 VTS_I4 VTS_BSTR VTS_I4 VTS_PBSTR VTS_PBSTR VTS_BSTR ;
		InvokeHelper(0x8b, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sCarrierID, sLocID, nIdStatus, nSlotMapStatus, nAccessingStatus, sSlotMap, nContentsMapCount, psLotID, psSubstrateID, sUsage);
		return result;
	}
	long CMSRspCarrierTagReadData(long nMsgId, LPCTSTR sLocID, LPCTSTR sCarrierID, LPCTSTR sData, long nResult, long nErrCount, long * pnErrCode, BSTR * psErrText)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_BSTR VTS_BSTR VTS_BSTR VTS_I4 VTS_I4 VTS_PI4 VTS_PBSTR ;
		InvokeHelper(0x8c, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nMsgId, sLocID, sCarrierID, sData, nResult, nErrCount, pnErrCode, psErrText);
		return result;
	}
	long CMSRspCarrierTagWriteData(long nMsgId, LPCTSTR sLocID, LPCTSTR sCarrierID, long nResult, long nErrCount, long * pnErrCode, BSTR * psErrText)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_BSTR VTS_BSTR VTS_I4 VTS_I4 VTS_PI4 VTS_PBSTR ;
		InvokeHelper(0x8d, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nMsgId, sLocID, sCarrierID, nResult, nErrCount, pnErrCode, psErrText);
		return result;
	}
	long CMSRspCarrierRelease(long nMsgId, LPCTSTR sLocID, LPCTSTR sCarrierID, long nResult, long nErrCount, long * pnErrCode, BSTR * psErrText)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_BSTR VTS_BSTR VTS_I4 VTS_I4 VTS_PI4 VTS_PBSTR ;
		InvokeHelper(0x8e, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nMsgId, sLocID, sCarrierID, nResult, nErrCount, pnErrCode, psErrText);
		return result;
	}
	long CMSSetBufferCapacityChanged(LPCTSTR sPartID, LPCTSTR sPartType, long nAPPCapacity, long nPCapacity, long nUnPCapacity)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_BSTR VTS_I4 VTS_I4 VTS_I4 ;
		InvokeHelper(0x8f, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sPartID, sPartType, nAPPCapacity, nPCapacity, nUnPCapacity);
		return result;
	}
	long CMSRspCarrierIn(long nMsgId, LPCTSTR sLocID, LPCTSTR sCarrierID, long nResult, long nErrCount, long * pnErrCode, BSTR * psErrText)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_BSTR VTS_BSTR VTS_I4 VTS_I4 VTS_PI4 VTS_PBSTR ;
		InvokeHelper(0x90, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nMsgId, sLocID, sCarrierID, nResult, nErrCount, pnErrCode, psErrText);
		return result;
	}
	long CMSReqCarrierReCreate(LPCTSTR sLocID, LPCTSTR sCarrierID)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_BSTR ;
		InvokeHelper(0x91, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sLocID, sCarrierID);
		return result;
	}
	long CMSSetMaterialArrived(LPCTSTR sMaterialID)
	{
		long result;
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x92, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sMaterialID);
		return result;
	}
	long CMSSetCarrierIDStatus(LPCTSTR sCarrierID, long nState)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_I4 ;
		InvokeHelper(0x93, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sCarrierID, nState);
		return result;
	}
	long CMSSetSlotMapStatus(LPCTSTR sCarrierID, long nState)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_I4 ;
		InvokeHelper(0x94, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sCarrierID, nState);
		return result;
	}
	long CMSSetSubstrateCount(LPCTSTR sCarrierID, long nSubstCount)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_I4 ;
		InvokeHelper(0x95, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sCarrierID, nSubstCount);
		return result;
	}
	long CMSSetUsage(LPCTSTR sCarrierID, LPCTSTR sUsage)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_BSTR ;
		InvokeHelper(0x96, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sCarrierID, sUsage);
		return result;
	}
	long CMSSetCarrierOutStart(LPCTSTR sLocID, LPCTSTR sCarrierID)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_BSTR ;
		InvokeHelper(0x97, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sLocID, sCarrierID);
		return result;
	}
	long CMSRspCancelCarrierOut(long nMsgId, LPCTSTR sCarrierID, long nResult, long nErrCount, long * pnErrCode, BSTR * psErrText)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_BSTR VTS_I4 VTS_I4 VTS_PI4 VTS_PBSTR ;
		InvokeHelper(0x98, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nMsgId, sCarrierID, nResult, nErrCount, pnErrCode, psErrText);
		return result;
	}
	long PJReqCommand(long nCommand, LPCTSTR sPJobID)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_BSTR ;
		InvokeHelper(0x99, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nCommand, sPJobID);
		return result;
	}
	long PJReqGetJob(LPCTSTR sPJobID)
	{
		long result;
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x9a, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sPJobID);
		return result;
	}
	long PJReqGetAllJobID()
	{
		long result;
		InvokeHelper(0x9b, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
		return result;
	}
	long PJReqCreate(LPCTSTR sPJobID, long nMtrlFormat, long nAutoStart, long nMtrlOrder, long nMtrlCount, BSTR * psMtrlID, BSTR * psSlotInfo, long nRcpMethod, LPCTSTR sRcpID, long nRcpParCount, BSTR * psRcpParName, BSTR * psRcpParValue)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_PBSTR VTS_PBSTR VTS_I4 VTS_BSTR VTS_I4 VTS_PBSTR VTS_PBSTR ;
		InvokeHelper(0x9c, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sPJobID, nMtrlFormat, nAutoStart, nMtrlOrder, nMtrlCount, psMtrlID, psSlotInfo, nRcpMethod, sRcpID, nRcpParCount, psRcpParName, psRcpParValue);
		return result;
	}
	long PJRspVerify(long nMsgId, long nPJobCount, BSTR * psPJobID, long nResult, long nErrCount, long * pnErrCode, BSTR * psErrText)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_I4 VTS_PBSTR VTS_I4 VTS_I4 VTS_PI4 VTS_PBSTR ;
		InvokeHelper(0x9d, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nMsgId, nPJobCount, psPJobID, nResult, nErrCount, pnErrCode, psErrText);
		return result;
	}
	long PJSetState(LPCTSTR sPJobID, long nState)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_I4 ;
		InvokeHelper(0x9e, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sPJobID, nState);
		return result;
	}
	long PJSettingUpCompt(LPCTSTR sPJobID)
	{
		long result;
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x9f, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sPJobID);
		return result;
	}
	long PJGetAllJobInfo(long * pnObjID, long * pnPJobCount)
	{
		long result;
		static BYTE parms[] = VTS_PI4 VTS_PI4 ;
		InvokeHelper(0xa0, DISPATCH_METHOD, VT_I4, (void*)&result, parms, pnObjID, pnPJobCount);
		return result;
	}
	long GetPRJobID(long nObjID, long nIndex, BSTR * psPJobID)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_I4 VTS_PBSTR ;
		InvokeHelper(0xa1, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nObjID, nIndex, psPJobID);
		return result;
	}
	long GetPRJobState(long nObjID, long nIndex, long * pnState)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_I4 VTS_PI4 ;
		InvokeHelper(0xa2, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nObjID, nIndex, pnState);
		return result;
	}
	long GetPRJobAutoStart(long nObjID, long nIndex, long * pnAutoStart)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_I4 VTS_PI4 ;
		InvokeHelper(0xa3, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nObjID, nIndex, pnAutoStart);
		return result;
	}
	long GetPRJobMtrlOrder(long nObjID, long nIndex, long * pnMtrlOrder)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_I4 VTS_PI4 ;
		InvokeHelper(0xa4, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nObjID, nIndex, pnMtrlOrder);
		return result;
	}
	long GetPRJobMtrlFormat(long nObjID, long nIndex, long * pnMtrlFormat)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_I4 VTS_PI4 ;
		InvokeHelper(0xa5, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nObjID, nIndex, pnMtrlFormat);
		return result;
	}
	long GetPRJobCarrierCount(long nObjID, long nIndex, long * pnCount)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_I4 VTS_PI4 ;
		InvokeHelper(0xa6, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nObjID, nIndex, pnCount);
		return result;
	}
	long GetPRJobCarrier(long nObjID, long nIndex, long nCount, BSTR * psCarrierID, BSTR * psSlotInfo)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_I4 VTS_I4 VTS_PBSTR VTS_PBSTR ;
		InvokeHelper(0xa7, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nObjID, nIndex, nCount, psCarrierID, psSlotInfo);
		return result;
	}
	long GetPRJobRcpID(long nObjID, long nIndex, BSTR * psRcpID)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_I4 VTS_PBSTR ;
		InvokeHelper(0xa8, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nObjID, nIndex, psRcpID);
		return result;
	}
	long GetPRJobRcpParamCount(long nObjID, long nIndex, long * pnCount)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_I4 VTS_PI4 ;
		InvokeHelper(0xa9, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nObjID, nIndex, pnCount);
		return result;
	}
	long GetPRJobRcpParam(long nObjID, long nIndex, long nCount, BSTR * psRcpParName, BSTR * psRcpParValue)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_I4 VTS_I4 VTS_PBSTR VTS_PBSTR ;
		InvokeHelper(0xaa, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nObjID, nIndex, nCount, psRcpParName, psRcpParValue);
		return result;
	}
	long GetPRJobClose(long nObjID)
	{
		long result;
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0xab, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nObjID);
		return result;
	}
	long PJDelJobInfo(LPCTSTR sPJobID)
	{
		long result;
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0xac, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sPJobID);
		return result;
	}
	long PJDelAllJobInfo()
	{
		long result;
		InvokeHelper(0xad, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
		return result;
	}
	long PJSetJobInfo(LPCTSTR sPJobID, long nMtrlFormat, long nAutoStart, long nMtrlOrder, long nMtrlCount, BSTR * psMtrlID, BSTR * psSlotInfo, long nRcpMethod, LPCTSTR sRcpID, long nRcpParCount, BSTR * psRcpParName, BSTR * psRcpParVal)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_PBSTR VTS_PBSTR VTS_I4 VTS_BSTR VTS_I4 VTS_PBSTR VTS_PBSTR ;
		InvokeHelper(0xae, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sPJobID, nMtrlFormat, nAutoStart, nMtrlOrder, nMtrlCount, psMtrlID, psSlotInfo, nRcpMethod, sRcpID, nRcpParCount, psRcpParName, psRcpParVal);
		return result;
	}
	long PJSettingUpStart(LPCTSTR sPJobID)
	{
		long result;
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0xaf, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sPJobID);
		return result;
	}
	long PJRspCommand(long nMsgId, long nCommand, LPCTSTR sPJobID, long nResult, long nErrCount, long * pnErrCode, BSTR * psErrText)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_I4 VTS_BSTR VTS_I4 VTS_I4 VTS_PI4 VTS_PBSTR ;
		InvokeHelper(0xb0, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nMsgId, nCommand, sPJobID, nResult, nErrCount, pnErrCode, psErrText);
		return result;
	}
	long PJRspSetRcpVariable(long nMsgId, LPCTSTR sPJobID, long nResult, long nErrCount, long * pnErrCode, BSTR * psErrText)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_BSTR VTS_I4 VTS_I4 VTS_PI4 VTS_PBSTR ;
		InvokeHelper(0xb1, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nMsgId, sPJobID, nResult, nErrCount, pnErrCode, psErrText);
		return result;
	}
	long PJRspSetStartMethod(long nMsgId, long nPJobCount, BSTR * psPJobID, long nResult, long nErrCount, long * pnErrCode, BSTR * psErrText)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_I4 VTS_PBSTR VTS_I4 VTS_I4 VTS_PI4 VTS_PBSTR ;
		InvokeHelper(0xb2, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nMsgId, nPJobCount, psPJobID, nResult, nErrCount, pnErrCode, psErrText);
		return result;
	}
	long PJRspSetMtrlOrder(long nMsgId, long nResult)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_I4 ;
		InvokeHelper(0xb3, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nMsgId, nResult);
		return result;
	}
	long CJReqCommand(LPCTSTR sCJobID, long nCommand, LPCTSTR sCPName, LPCTSTR sCPVal)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_I4 VTS_BSTR VTS_BSTR ;
		InvokeHelper(0xb4, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sCJobID, nCommand, sCPName, sCPVal);
		return result;
	}
	long CJReqGetJob(LPCTSTR sCJobID)
	{
		long result;
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0xb5, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sCJobID);
		return result;
	}
	long CJReqGetAllJobID()
	{
		long result;
		InvokeHelper(0xb6, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
		return result;
	}
	long CJReqCreate(LPCTSTR sCJobID, long nStartMethod, long nCountPRJob, BSTR * psPRJobID)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_I4 VTS_I4 VTS_PBSTR ;
		InvokeHelper(0xb7, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sCJobID, nStartMethod, nCountPRJob, psPRJobID);
		return result;
	}
	long CJReqSelect(LPCTSTR sCJobID)
	{
		long result;
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0xb8, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sCJobID);
		return result;
	}
	long CJGetAllJobInfo(long * pnObjID, long * pnCount)
	{
		long result;
		static BYTE parms[] = VTS_PI4 VTS_PI4 ;
		InvokeHelper(0xb9, DISPATCH_METHOD, VT_I4, (void*)&result, parms, pnObjID, pnCount);
		return result;
	}
	long GetCtrlJobID(long nObjID, long nIndex, BSTR * psCtrlJobID)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_I4 VTS_PBSTR ;
		InvokeHelper(0xba, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nObjID, nIndex, psCtrlJobID);
		return result;
	}
	long GetCtrlJobState(long nObjID, long nIndex, long * pnState)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_I4 VTS_PI4 ;
		InvokeHelper(0xbb, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nObjID, nIndex, pnState);
		return result;
	}
	long GetCtrlJobStartMethod(long nObjID, long nIndex, long * pnAutoStart)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_I4 VTS_PI4 ;
		InvokeHelper(0xbc, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nObjID, nIndex, pnAutoStart);
		return result;
	}
	long GetCtrlJobPRJobCount(long nObjID, long nIndex, long * pnCount)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_I4 VTS_PI4 ;
		InvokeHelper(0xbd, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nObjID, nIndex, pnCount);
		return result;
	}
	long GetCtrlJobPRJobIDs(long nObjID, long nIndex, long nCount, BSTR * psPRJobIDs)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_I4 VTS_I4 VTS_PBSTR ;
		InvokeHelper(0xbe, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nObjID, nIndex, nCount, psPRJobIDs);
		return result;
	}
	long GetCtrlJobClose(long nObjID)
	{
		long result;
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0xbf, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nObjID);
		return result;
	}
	long CJDelJobInfo(LPCTSTR sCJobID)
	{
		long result;
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0xc0, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sCJobID);
		return result;
	}
	long CJDelAllJobInfo()
	{
		long result;
		InvokeHelper(0xc1, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
		return result;
	}
	long CJSetJobInfo(LPCTSTR sCJobID, long nState, long nStartMethod, long nCountPRJob, BSTR * psPRJobID)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_I4 VTS_I4 VTS_I4 VTS_PBSTR ;
		InvokeHelper(0xc2, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sCJobID, nState, nStartMethod, nCountPRJob, psPRJobID);
		return result;
	}
	long CJReqHOQJob(LPCTSTR sCJobID)
	{
		long result;
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0xc3, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sCJobID);
		return result;
	}
	long CJGetHOQJob()
	{
		long result;
		InvokeHelper(0xc4, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
		return result;
	}
	long STSSetSubstLocationInfo(LPCTSTR sSubstLocID, LPCTSTR sSubstrateID)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_BSTR ;
		InvokeHelper(0xc5, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sSubstLocID, sSubstrateID);
		return result;
	}
	long STSSetBatchLocationInfo(LPCTSTR sBatchLocID, LPCTSTR sSubstrateID)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_BSTR ;
		InvokeHelper(0xc6, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sBatchLocID, sSubstrateID);
		return result;
	}
	long STSSetTransport(LPCTSTR sSubstLocID, LPCTSTR sSubstrateID, long nState)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_BSTR VTS_I4 ;
		InvokeHelper(0xc7, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sSubstLocID, sSubstrateID, nState);
		return result;
	}
	long STSSetBatchTransport(long nCount, BSTR * psSubstLocID, BSTR * psSubstrateID, long nState)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_PBSTR VTS_PBSTR VTS_I4 ;
		InvokeHelper(0xc8, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nCount, psSubstLocID, psSubstrateID, nState);
		return result;
	}
	long STSSetProcessing(LPCTSTR sSubstLocID, LPCTSTR sSubstrateID, long nState)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_BSTR VTS_I4 ;
		InvokeHelper(0xc9, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sSubstLocID, sSubstrateID, nState);
		return result;
	}
	long STSSetBatchProcessing(long nCount, BSTR * psSubstLocID, BSTR * psSubstrateID, long nState)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_PBSTR VTS_PBSTR VTS_I4 ;
		InvokeHelper(0xca, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nCount, psSubstLocID, psSubstrateID, nState);
		return result;
	}
	long STSGetAllSubstrateInfo(long * pnObjID, long * pnCount)
	{
		long result;
		static BYTE parms[] = VTS_PI4 VTS_PI4 ;
		InvokeHelper(0xcb, DISPATCH_METHOD, VT_I4, (void*)&result, parms, pnObjID, pnCount);
		return result;
	}
	long GetSubstrateID(long nObjID, long nIndex, BSTR * psSubstrateID)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_I4 VTS_PBSTR ;
		InvokeHelper(0xcc, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nObjID, nIndex, psSubstrateID);
		return result;
	}
	long GetSubstrateState(long nObjID, long nIndex, long * pnTransportState, long * pnProcessingState, long * pnReadingState)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_I4 VTS_PI4 VTS_PI4 VTS_PI4 ;
		InvokeHelper(0xcd, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nObjID, nIndex, pnTransportState, pnProcessingState, pnReadingState);
		return result;
	}
	long GetSubstrateLocID(long nObjID, long nIndex, BSTR * psSubstLocID)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_I4 VTS_PBSTR ;
		InvokeHelper(0xce, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nObjID, nIndex, psSubstLocID);
		return result;
	}
	long GetSubstrateClose(long nObjID)
	{
		long result;
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0xcf, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nObjID);
		return result;
	}
	long STSDelSubstrateInfo(LPCTSTR sSubstrateID)
	{
		long result;
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0xd0, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sSubstrateID);
		return result;
	}
	long STSDelAllSubstrateInfo()
	{
		long result;
		InvokeHelper(0xd1, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
		return result;
	}
	long STSSetSubstrateInfo(LPCTSTR sSubstLocID, LPCTSTR sSubstrateID, long nTransportState, long nProcessingState, long nReadingState)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_BSTR VTS_I4 VTS_I4 VTS_I4 ;
		InvokeHelper(0xd2, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sSubstLocID, sSubstrateID, nTransportState, nProcessingState, nReadingState);
		return result;
	}
	long STSSetMaterialArrived(LPCTSTR sMaterialID)
	{
		long result;
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0xd3, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sMaterialID);
		return result;
	}
	long STSReqCreateSubstrate(LPCTSTR sSubstLocID, LPCTSTR sSubstrateID)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_BSTR ;
		InvokeHelper(0xd4, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sSubstLocID, sSubstrateID);
		return result;
	}
	long STSRspCreateSubstrate(long nMsgId, LPCTSTR sSubstLocID, LPCTSTR sSubstrateID, long nResult, long nErrCount, long * pnErrCode, BSTR * psErrText)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_BSTR VTS_BSTR VTS_I4 VTS_I4 VTS_PI4 VTS_PBSTR ;
		InvokeHelper(0xd5, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nMsgId, sSubstLocID, sSubstrateID, nResult, nErrCount, pnErrCode, psErrText);
		return result;
	}
	long STSReqCancelSubstrate(LPCTSTR sSubstLocID, LPCTSTR sSubstrateID)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_BSTR ;
		InvokeHelper(0xd6, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sSubstLocID, sSubstrateID);
		return result;
	}
	long STSRspCancelSubstrate(long nMsgId, LPCTSTR sSubstLocID, LPCTSTR sSubstrateID, long nResult, long nErrCount, long * pnErrCode, BSTR * psErrText)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_BSTR VTS_BSTR VTS_I4 VTS_I4 VTS_PI4 VTS_PBSTR ;
		InvokeHelper(0xd7, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nMsgId, sSubstLocID, sSubstrateID, nResult, nErrCount, pnErrCode, psErrText);
		return result;
	}
	long STSReqProceedSubstrate(LPCTSTR sSubstLocID, LPCTSTR sSubstrateID, LPCTSTR sReadSubstID)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_BSTR VTS_BSTR ;
		InvokeHelper(0xd8, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sSubstLocID, sSubstrateID, sReadSubstID);
		return result;
	}
	long STSRspUpdateSubstrate(long nMsgId, LPCTSTR sSubstLocID, LPCTSTR sSubstrateID, long nResult, long nErrCount, long * pnErrCode, BSTR * psErrText)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_BSTR VTS_BSTR VTS_I4 VTS_I4 VTS_PI4 VTS_PBSTR ;
		InvokeHelper(0xd9, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nMsgId, sSubstLocID, sSubstrateID, nResult, nErrCount, pnErrCode, psErrText);
		return result;
	}
	long STSReqDeleteSubstrate(LPCTSTR sSubstLocID, LPCTSTR sSubstrateID)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_BSTR ;
		InvokeHelper(0xda, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sSubstLocID, sSubstrateID);
		return result;
	}
	long STSRspDeleteSubstrate(long nMsgId, LPCTSTR sSubstLocID, LPCTSTR sSubstrateID, long nResult, long nErrCount, long * pnErrCode, BSTR * psErrText)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_BSTR VTS_BSTR VTS_I4 VTS_I4 VTS_PI4 VTS_PBSTR ;
		InvokeHelper(0xdb, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nMsgId, sSubstLocID, sSubstrateID, nResult, nErrCount, pnErrCode, psErrText);
		return result;
	}
	long STSSetSubstrateID(LPCTSTR sSubstLocID, LPCTSTR sSubstrateID, LPCTSTR sSubstReadID, long nResult)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_BSTR VTS_BSTR VTS_I4 ;
		InvokeHelper(0xdc, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sSubstLocID, sSubstrateID, sSubstReadID, nResult);
		return result;
	}
	long CMSSetTransferReady(LPCTSTR sLocID, long nState)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_I4 ;
		InvokeHelper(0xdd, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sLocID, nState);
		return result;
	}
	long GEMReqPPFmtSend2(LPCTSTR sPpid, LPCTSTR sMdln, LPCTSTR sSoftRev, long nCount, BSTR * psCCode, long * pnParamCount, BSTR * psParamNames, BSTR * psParamValues)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_BSTR VTS_BSTR VTS_I4 VTS_PBSTR VTS_PI4 VTS_PBSTR VTS_PBSTR ;
		InvokeHelper(0xde, DISPATCH_METHOD, VT_I4, (void*)&result, parms, sPpid, sMdln, sSoftRev, nCount, psCCode, pnParamCount, psParamNames, psParamValues);
		return result;
	}
	long GEMRspPPFmt2(long nMsgId, LPCTSTR sPpid, LPCTSTR sMdln, LPCTSTR sSoftRev, long nCount, BSTR * psCCode, long * pnParamCount, BSTR * psParamNames, BSTR * psParamValues)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_BSTR VTS_BSTR VTS_BSTR VTS_I4 VTS_PBSTR VTS_PI4 VTS_PBSTR VTS_PBSTR ;
		InvokeHelper(0xdf, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nMsgId, sPpid, sMdln, sSoftRev, nCount, psCCode, pnParamCount, psParamNames, psParamValues);
		return result;
	}
	long SendUserMessage(long nObjectID, LPCTSTR sCommand, long nTransID)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_BSTR VTS_I4 ;
		InvokeHelper(0xe0, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nObjectID, sCommand, nTransID);
		return result;
	}
	long CJRspVerify(long nMsgId, LPCTSTR sCJobID, long nResult, long nErrCount, long * pnErrCode, BSTR * psErrText)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_BSTR VTS_I4 VTS_I4 VTS_PI4 VTS_PBSTR ;
		InvokeHelper(0xe1, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nMsgId, sCJobID, nResult, nErrCount, pnErrCode, psErrText);
		return result;
	}
	long CJRspCommand(long nMsgId, LPCTSTR sCJobID, long nCommand, long nResult, long nErrCode, LPCTSTR sErrText)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_BSTR VTS_I4 VTS_I4 VTS_I4 VTS_BSTR ;
		InvokeHelper(0xe2, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nMsgId, sCJobID, nCommand, nResult, nErrCode, sErrText);
		return result;
	}
	long OpenGEMObject(long * pnMsgId, LPCTSTR sObjType, LPCTSTR sObjID)
	{
		long result;
		static BYTE parms[] = VTS_PI4 VTS_BSTR VTS_BSTR ;
		InvokeHelper(0xe3, DISPATCH_METHOD, VT_I4, (void*)&result, parms, pnMsgId, sObjType, sObjID);
		return result;
	}
	long CloseGEMObject(long nMsgId)
	{
		long result;
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0xe4, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nMsgId);
		return result;
	}
	long GetAttrData(long * pnAttrID, long nMsgId, LPCTSTR sAttrName)
	{
		long result;
		static BYTE parms[] = VTS_PI4 VTS_I4 VTS_BSTR ;
		InvokeHelper(0xe5, DISPATCH_METHOD, VT_I4, (void*)&result, parms, pnAttrID, nMsgId, sAttrName);
		return result;
	}

// Properties
//

CString GetIP()
{
	CString result;
	GetProperty(0x1, VT_BSTR, (void*)&result);
	return result;
}
void SetIP(CString propVal)
{
	SetProperty(0x1, VT_BSTR, propVal);
}
long GetPort()
{
	long result;
	GetProperty(0x2, VT_I4, (void*)&result);
	return result;
}
void SetPort(long propVal)
{
	SetProperty(0x2, VT_I4, propVal);
}
BOOL GetActive()
{
	BOOL result;
	GetProperty(0x3, VT_BOOL, (void*)&result);
	return result;
}
void SetActive(BOOL propVal)
{
	SetProperty(0x3, VT_BOOL, propVal);
}


};
