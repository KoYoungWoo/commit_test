#pragma once

//*******************************************************************
//              
//  FILE:       ADOClass.h
//              
//  COMPONENT:  CADOClass
//              
//  DATE:       2009-06-08
//              
//  COMMENTS:   ADO 클래스의 헤더파일
//     
//*******************************************************************

/*-------------------------------------------------------------------
1. ADO를 쓰기 위하여 우선 다음 코드를 삽입한다. 보통 stdAfx.h 파일에 써줌

	#pragma warning(push)
	#pragma warning(disable:4146)
	#import "c:\program files\common files\system\ado\msado15.dll" rename ("EOF","adoEOF") no_namespace
	#pragma warning(pop) 

-------------------------------------------------------------------*/

//*******************************************************************
// HISTORY - dhaps2000
// 2010-4-10 Update
// RecodeSet GetRsData 함수 추가 및 내부 코드 업데이트


class CADOClass  
{
public:
	
	CADOClass();
	virtual ~CADOClass();

	// -------------------------------------------------------------
	// RS에서 필요한 데이터 타입은 VARIANTARG를 참조하여 추가하세요
	// -------------------------------------------------------------

	// Record Getdata(int)
	BOOL GetRsDataInt(LPCTSTR lpszColumn, INT& nValue);
	BOOL GetRsDataInt(int nIndex, INT& nValue);

	// Record Getdata(CString)
	BOOL GetRsDataCString(LPCTSTR lpszColumn, CString& strValue);
	BOOL GetRsDataCString(int nIndex, CString& strValue);

	// Record Getdata(long)
	BOOL GetRsDataLong(LPCTSTR lpszColumn, LONG& lValue);
	BOOL GetRsDataLong(int nIndex, LONG& lValue);

	// Record GetdateTime(CTime)
	BOOL GetRsDataTime(LPCTSTR lpszColumn, CTime &cTime);
	BOOL GetRsDataTime(int nIndex, CTime &cTime);

	// Record GetdateTime(BOOL)
	BOOL GetRsDataBool(LPCTSTR lpszColumn, BOOL &bValue);
	BOOL GetRsDataBool(int nIndex, BOOL &bValue);

	// Record Getdata(double)
	BOOL GetRsDataFloat(LPCTSTR lpszColumn, double& dValue);
	BOOL GetRsDataFloat(int nIndex, double& dValue);

	// -------------------------------------------------------------
	
	// RS EOF인지 여부
	BOOL IsEOF();
	
	// RS(Recordset) 마지막으로 이동
	void LastRS();

	// RS 이전으로 이동
	void PrevRS();

	// RS 다음으로 이동
	void NextRS();

	// RS 처음으로 이동
	void FirstRS();

	// Close 레코드 셋
	BOOL CloseRS();

	// 쿼리를 실행하여 레코드셋을 뽑아낸다.(Command 객체 이용) - 속도 빠름, 중복 쿼리 처리 안됨
	BOOL ExecuteQryCmd(LPCSTR lpszQry, long lop=adCmdUnknown);

	// 쿼리를 실행하여 레코드셋을 뽑아낸다.(RecordSet 객체 이용) - 속도 느림, 중복 쿼리 처리 가능
	BOOL ExecuteQryRs(LPCSTR lpszQry, CursorTypeEnum emCurType=adOpenStatic, LockTypeEnum emLockType=adLockReadOnly, long lop=adCmdUnknown);

	// 쿼리를 실행한다.(Insert, Update, Delete문 : Command객체 이용)
	BOOL InUpDelQryCmd(LPCSTR lpszQry);

	// 쿼리를 실행한다.(Insert, Update, Delete문 : Connection 객체 이용)
	BOOL InUpDelQryConn(LPCSTR lpszQry);

	// DB를 연결한다.(Open포함)
	BOOL Connect(LPCSTR lpszProvider, ExecuteOptionEnum emOption = adOptionUnspecified);

	// DB를 닫아준다(m_pConnection, m_pCommand)
	BOOL DisConnect();

	// 트랜잭션 처리
	LONG BeginTrans() const { return m_pConnection->BeginTrans();}
	HRESULT RollbackTrans() const { return m_pConnection->RollbackTrans();}
	HRESULT CommitTrans() const { return m_pConnection->CommitTrans();}

	// 다음 레코드 목록을 불러오는 함수
	int NextRecord(void);

	// 해당 칼럼으로 필터링하는 함수
	int setFilter(_bstr_t strField, _bstr_t strFilter);

	// 레코드수를 리턴하는 함수
	long GetRecordCount(void);

private:
	_RecordsetPtr m_pRs;
	_CommandPtr m_pCommand;
	_ConnectionPtr m_pConnection;

	// 에러 보고 함수(예외처리시)
	void DumpError(const _com_error &e) const;
};

