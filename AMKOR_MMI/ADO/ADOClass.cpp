// ADOClass.cpp: implementation of the CADOClass class.
//
//////////////////////////////////////////////////////////////////////


//*******************************************************************
//              
//  FILE:       ADOClass.cpp
//              
//  PROJECT:    DlgTest
//              
//  COMPONENT:  CADOClass
//              
//  DATE:       15.05.2003
//              
//  COMMENTS:   - ADO 클래스의 cpp파일
//              
//              
//*******************************************************************



// Includes

#include "stdafx.h"
#include "ADOClass.h"
#include <ATLCONV.H>


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CADOClass::CADOClass()
{
	// 초기화
	m_pConnection = NULL;
	m_pCommand = NULL;
	m_pRs = NULL;

	::CoInitialize(NULL);

}

CADOClass::~CADOClass()
{
	::CoUninitialize();
}

//*******************************************************************
//              
//  FUNCTION:   - CADOClass::Connect
//              
//  RETURNS:    - bool (실행여부)
//              
//  PARAMETERS: - LPCSTR lpszProvider(연결할 서버 정보), ExecuteOptionEnum emOption(옵션)
//              
//  COMMENTS:   - 데이터 베이스를 열어준다.
//              
//              
//*******************************************************************

BOOL CADOClass::Connect(LPCSTR lpszProvider, ExecuteOptionEnum emOption)
{

//	TRACE0( "ADOClass::Connect() entered\n" );

	if(m_pConnection != NULL && m_pConnection->GetState() == adStateOpen)
		return FALSE;


	try
	{
		HRESULT hr = m_pConnection.CreateInstance(__uuidof( Connection ));
		
		m_pConnection->ConnectionTimeout = 5;
		_bstr_t btProvider=(_bstr_t)lpszProvider;
		_bstr_t btEmpty=(_bstr_t)"";
		
		m_pConnection->Open(btProvider,btEmpty,btEmpty,emOption);
		
	}
	catch( _com_error &e)
	{
		DumpError(e);
		return FALSE;
	}
	
	return TRUE;
}




//*******************************************************************
//              
//  FUNCTION:   - CADOClass::ExecuteQryCmd
//              
//  RETURNS:    - bool(실행여부)
//              
//  PARAMETERS: - LPCSTR lpszQry(쿼리문), lop(Option) 
//              
//  COMMENTS:   - Command 객체를 이용하여 Select문에 대한 레코드셋을 뽑아낸다. 단지 RS만 만들어 놓는다.
//				  GetRsDataChar or GetRsDataLong를 써서 RS 데이터를 뽑아 쓰세용~~
//              
//              
//*******************************************************************

BOOL CADOClass::ExecuteQryCmd(LPCSTR lpszQry, long lop)
{

////	TRACE0( "ADOClass::ExecuteQryCmd entered\n" );
	
	if( m_pConnection == NULL || m_pConnection->GetState()!=adStateOpen)
		return FALSE;

	if( m_pRs != NULL && m_pRs->GetState()!=adStateClosed)
	{
		m_pRs->Close();
		m_pRs.Release();
		m_pRs = NULL;
	}

	try
	{
		m_pCommand.CreateInstance(__uuidof( Command )); 
		m_pCommand->ActiveConnection = m_pConnection; 
		m_pCommand->CommandType = adCmdStoredProc; 

		m_pCommand->CommandText = _bstr_t(lpszQry); 

		_variant_t vNull; 
		vNull.vt = VT_ERROR; 
		vNull.scode = DISP_E_PARAMNOTFOUND; 
		
		m_pRs = m_pCommand->Execute(&vNull, &vNull, lop); 

	}
	catch(_com_error &e)
	{

		DumpError(e);
		return FALSE;
	}

	return TRUE;


}


//*******************************************************************
//              
//  FUNCTION:   - CADOClass::ExecuteQryRs
//              
//  RETURNS:    - bool(실행여부)
//              
//  PARAMETERS: - LPCSTR lpszQry(쿼리문),emCurType(CursorType), emLockType(LockType), lop(Option) 
//              
//  COMMENTS:   - RecordSet 객체를 이용하여 Select문에 대한 레코드셋을 뽑아낸다. 단지 RS만 만들어 놓는다.
//				  GetRsDataChar or GetRsDataLong를 써서 RS 데이터를 뽑아 쓰세용~~
//              
//*******************************************************************

BOOL CADOClass::ExecuteQryRs(LPCSTR lpszQry, CursorTypeEnum emCurType, LockTypeEnum emLockType, long lop)
{

//	TRACE0( "ADOClass::ExecuteQry entered\n" );
	
	if( m_pConnection == NULL || m_pConnection->GetState()!=adStateOpen)
		return FALSE;

	
	// 레코드셋이 이미 열려 있다면 초기화 시켜준다.
	if( m_pRs != NULL && m_pRs->GetState()!=adStateClosed)
	{
		m_pRs->Close();
		m_pRs.Release();
		m_pRs = NULL;
	}

	m_pRs.CreateInstance(__uuidof(Recordset));
	//커서를 adUseClient로 해줘야 lastRS를 사용 가능함
	m_pRs->CursorLocation = adUseClient;

	try
	{
		_bstr_t btQry=(_bstr_t)lpszQry;
        m_pRs->Open(btQry, _variant_t((IDispatch *) m_pConnection), emCurType, emLockType, lop);

	}
	catch(_com_error &e)
	{

		DumpError(e);
		return FALSE;

	}

	return TRUE;

}

//*******************************************************************
//              
//  FUNCTION:   - CADOClass::InUpDelQryCmd
//              
//  RETURNS:    - bool(실행여부)
//              
//  PARAMETERS: - LPCSTR lpszQry(쿼리문)
//              
//  COMMENTS:   - Command 객체를 이용하여 Insert, Update, Delete문에 대한 쿼리를 실행한다.
//              
//              
//*******************************************************************


BOOL CADOClass::InUpDelQryCmd(LPCSTR lpszQry)
{
//	TRACE0( "ADOClass::InUpDelQryCmd entered\n" );
	
	if( m_pConnection == NULL || m_pConnection->GetState()!=adStateOpen)
		return FALSE;


	try
	{
		m_pCommand.CreateInstance(__uuidof( Command )); 
		m_pCommand->ActiveConnection = m_pConnection; 
		m_pCommand->CommandType = adCmdStoredProc; 

		m_pCommand->CommandText = _bstr_t(lpszQry); 

		_variant_t vNull; 
		vNull.vt = VT_ERROR; 
		vNull.scode = DISP_E_PARAMNOTFOUND; 
		
		m_pCommand->Execute(&vNull, &vNull, adCmdUnknown); 

	}
	catch(_com_error &e)
	{

		DumpError(e);
		return FALSE;

	}

	return TRUE;

}


//*******************************************************************
//              
//  FUNCTION:   - CADOClass::InUpDelQryConn
//              
//  RETURNS:    - bool(실행여부)
//              
//  PARAMETERS: - LPCSTR lpszQry(쿼리문)
//              
//  COMMENTS:   - RecordSet 객체를 이용하여 Insert, Update, Delete문에 대한 쿼리를 실행한다.
//              
//              
//*******************************************************************


BOOL CADOClass::InUpDelQryConn(LPCSTR lpszQry)
{
//	TRACE0( "ADOClass::InUpDelQry entered\n" );
	
	if(m_pConnection == NULL || m_pConnection->GetState()!=adStateOpen )
		return FALSE;


	try 
	{
		_bstr_t btQry=(_bstr_t)lpszQry;
		m_pConnection->Execute(btQry,NULL,adOptionUnspecified);
	}
	catch(_com_error &e)
	{
		DumpError(e);
		return FALSE;
	}
		
	return TRUE;
}






//*******************************************************************
//              
//  FUNCTION:   - CADOClass::DisConnect
//              
//  RETURNS:    - bool (실행여부)
//              
//  PARAMETERS: - 없음
//              
//  COMMENTS:   - m_pConnection과 m_pCommand를 닫아준다.
//              
//              
//*******************************************************************

BOOL CADOClass::DisConnect()
{
//	TRACE0( "ADOClass::DisConnect entered\n" );

	try{

		//연결을 닫을 때는 m_pConnection보다 m_pRs를 항상 먼저 닫아준다...... 그래야 에러 안뜬다.
		if(m_pRs != NULL && m_pRs->GetState()!=adStateClosed)
		{
			m_pRs->Close();
			m_pRs.Release();
			m_pRs = NULL;
		}		
		
		
		if(m_pConnection != NULL && m_pConnection->GetState()!=adStateClosed )
		{
			m_pConnection->Close();
			m_pConnection.Release();
			m_pConnection = NULL;
		}


	}
	catch(_com_error &e)
	{
		DumpError(e);
		return FALSE;
	}

	return TRUE;

}







//*******************************************************************
//              
//  FUNCTION:   - CADOClass::DumpError
//              
//  RETURNS:    - void
//              
//  PARAMETERS: - const _com_error &e
//              
//  COMMENTS:   - ADO 클래스 안에서 catch문에 빠졌을 경우 보고한다. (나중에는 파일로 쓰자)
//              
//              
//*******************************************************************

void CADOClass::DumpError(const _com_error &e) const
{
	_bstr_t btSource(e.Source());
	_bstr_t btDescription(e.Description());
	CString strMsg;
	strMsg.Format(L"DATE= %s\n"
		L"%08lx= %s\n"
		L"Source= %s\n"
		L"Description= %s"
		,CTime::GetCurrentTime().Format(L"%Y-%m-%d %H:%M:%S")
		,e.Error(),e.ErrorMessage()
		,(LPCSTR)btSource
		,(LPCSTR)btDescription);


#ifdef _DEBUG
	// 로그에 띄워주기 위해...
	//AfxMessageBox(strMsg);
#endif
	CStdioFile sdFile;
	if( !sdFile.Open(L"C:\\ADO_Dump.txt", CFile::shareDenyNone | CFile::modeWrite | CFile::typeText) )
	{
		if( !sdFile.Open(L"C:\\ADO_Dump.txt", CFile::shareDenyNone | CFile::modeCreate | CFile::modeWrite | CFile::typeText) )
		{
//			TRACE("C:\\EZnetManagerDump.txt Create Fail\n");
			return;
		}
	}

	sdFile.SeekToEnd();
	sdFile.WriteString(strMsg);
	sdFile.WriteString(L"\n\n");
	sdFile.Close();


}


// =================================================
// RecordSet 마지막인지 여부 확인
// =================================================
BOOL CADOClass::IsEOF()
{
	if (NULL == m_pRs)
	{
		return TRUE;
	}
	return (m_pRs->adoEOF);
}



// =================================================
// Clsoe Recordset
// =================================================
BOOL CADOClass::CloseRS()
{

	try
	{
		if(m_pRs != NULL && m_pRs->GetState()!=adStateClosed)
		{
			m_pRs->Close();
			m_pRs.Release();
			m_pRs = NULL;
		}						
	}
	catch(_com_error &e)
	{
		DumpError(e);
		return FALSE;
	}


	return TRUE;
}

// =================================================
// Recordset First로 이동
// =================================================
void CADOClass::FirstRS()
{

	if( m_pRs != NULL )
	{
		m_pRs->MoveFirst();
	}

}

// =================================================
// Recordset Next로 이동
// =================================================
void CADOClass::NextRS()
{
	if( m_pRs != NULL )
	{
		m_pRs->MoveNext();
	}

}

// =================================================
// Recordset Prev로 이동
// =================================================
void CADOClass::PrevRS()
{
	if( m_pRs != NULL )
	{
		m_pRs->MovePrevious();
	}
}


// =================================================
// Recordset Last로 이동
// =================================================
void CADOClass::LastRS()
{
	if( m_pRs != NULL )
	{
		m_pRs->MoveLast();
	}

}


// =================================================
// LONG형 데이터 반환(RS)
// =================================================
BOOL CADOClass::GetRsDataLong( LPCTSTR lpszColumn, LONG& lValue )
{
	long val = (long)NULL;
	_variant_t vtFld;
	
	try
	{
		vtFld = m_pRs->Fields->GetItem(lpszColumn)->Value;
		if(vtFld.vt != VT_NULL && vtFld.vt != VT_EMPTY)
			val = vtFld.lVal;
		lValue = val;
		return TRUE;
	}
	catch(_com_error &e)
	{
		DumpError(e);
		return FALSE;
	}

	return TRUE;

}

BOOL CADOClass::GetRsDataLong(int nIndex, LONG& lValue)
{
	long val = (long)NULL;
	_variant_t vtFld;
	_variant_t vtIndex;
	
	vtIndex.vt = VT_I2;
	vtIndex.iVal = nIndex;

	try
	{
		vtFld = m_pRs->Fields->GetItem(vtIndex)->Value;
		if(vtFld.vt != VT_NULL && vtFld.vt != VT_EMPTY)
			val = vtFld.lVal;
		lValue = val;
		return TRUE;
	}
	catch(_com_error &e)
	{
		DumpError(e);
		return FALSE;
	}

	return TRUE;
}

// =================================================
// BOOL형 데이터 반환(RS)
// =================================================
BOOL CADOClass::GetRsDataBool(LPCTSTR lpszColumn, BOOL &bValue)
{
	_variant_t vtFld;

	try
	{
		vtFld = m_pRs->Fields->GetItem(lpszColumn)->Value;
		switch(vtFld.vt) 
		{
		case VT_BOOL:
			bValue = vtFld.boolVal == VARIANT_TRUE? true: false;
			break;
		case VT_EMPTY:
		case VT_NULL:
			bValue = false;
			break;
		default:
			return FALSE;
		}
		return TRUE;
	}
	catch(_com_error &e){
		DumpError(e);
		return FALSE;
	}

	return TRUE;
}

BOOL CADOClass::GetRsDataBool(int nIndex, BOOL &bValue)
{
	_variant_t vtFld;
	_variant_t vtIndex;
	
	vtIndex.vt = VT_I2;
	vtIndex.iVal = nIndex;
	
	try
	{
		vtFld = m_pRs->Fields->GetItem(vtIndex)->Value;
		switch(vtFld.vt) 
		{
		case VT_BOOL:
			bValue = vtFld.boolVal == VARIANT_TRUE? true: false;
			break;
		case VT_EMPTY:
		case VT_NULL:
			bValue = false;
			break;
		default:
			return FALSE;
		}
		return TRUE;
	}
	catch(_com_error &e)
	{
		DumpError(e);
		return FALSE;
	}
}

// =================================================
// INT형 데이터 반환(RS)
// =================================================
BOOL CADOClass::GetRsDataInt(LPCTSTR lpszColumn, INT &nValue)
{
	int val = NULL;
	_variant_t var;

	try
	{
		var = m_pRs->Fields->GetItem(lpszColumn)->Value;
		switch(var.vt)
		{
		case VT_BOOL:
			val = var.boolVal;
			break;
		case VT_I2:
		case VT_UI1:
			val = var.iVal;
			break;
		case VT_INT:
			val = var.intVal;
			break;
		case VT_NULL:
		case VT_EMPTY:
			val = 0;
			break;
		default:
			val = var.iVal;
		}	
		nValue = val;
		return TRUE;
	}
	catch(_com_error &e){
		DumpError(e);
		return FALSE;
	}

	return TRUE;
}

BOOL CADOClass::GetRsDataInt(int nIndex, INT& nValue)
{
	int val = (int)NULL;
	_variant_t vtFld;
	_variant_t vtIndex;
	
	vtIndex.vt = VT_I2;
	vtIndex.iVal = nIndex;

	try
	{
		vtFld = m_pRs->Fields->GetItem(vtIndex)->Value;
		switch(vtFld.vt)
		{
		case VT_BOOL:
			val = vtFld.boolVal;
			break;
		case VT_I2:
		case VT_UI1:
			val = vtFld.iVal;
			break;
		case VT_INT:
			val = vtFld.intVal;
			break;
		case VT_NULL:
		case VT_EMPTY:
			val = 0;
			break;
		default:
			val = vtFld.iVal;
		}	
		nValue = val;
		return TRUE;
	}
	catch(_com_error &e)
	{
		DumpError(e);
		return FALSE;
	}
}

// =================================================
// 실수형 데이터 반환(RS) - float
// =================================================
BOOL CADOClass::GetRsDataFloat(LPCTSTR lpszColumn, double& dValue)
{
	double val = (double)NULL;
	_variant_t vtFld;
	
	try
	{
		vtFld = m_pRs->Fields->GetItem(lpszColumn)->Value;
		switch(vtFld.vt)
		{
		case VT_R4:
			val = vtFld.fltVal;
			break;
		case VT_R8:
			val = vtFld.dblVal;
			break;
		case VT_DECIMAL:
			//Corrected by Jos?Carlos Mart?ez Gal?
			val = vtFld.decVal.Lo32;
			val *= (vtFld.decVal.sign == 128)? -1 : 1;
			val /= pow(10, vtFld.decVal.scale); 
			break;
		case VT_UI1:
			val = vtFld.iVal;
			break;
		case VT_I2:
		case VT_I4:
			val = vtFld.lVal;
			break;
		case VT_INT:
			val = vtFld.intVal;
			break;
		case VT_NULL:
		case VT_EMPTY:
			val = 0;
			break;
		default:
			val = vtFld.dblVal;
		}
		dValue = val;
		return TRUE;
	}
	catch(_com_error &e){
		DumpError(e);
		return FALSE;
	}

	return TRUE;
}

BOOL CADOClass::GetRsDataFloat(int nIndex, double& dValue)
{
	double val = (double)NULL;
	_variant_t vtFld;
	_variant_t vtIndex;

	vtIndex.vt = VT_I2;
	vtIndex.iVal = nIndex;
	
	try
	{
		vtFld = m_pRs->Fields->GetItem(vtIndex)->Value;
		switch(vtFld.vt)
		{
		case VT_R4:
			val = vtFld.fltVal;
			break;
		case VT_R8:
			val = vtFld.dblVal;
			break;
		case VT_DECIMAL:
			//Corrected by Jos?Carlos Mart?ez Gal?
			val = vtFld.decVal.Lo32;
			val *= (vtFld.decVal.sign == 128)? -1 : 1;
			val /= pow(10, vtFld.decVal.scale); 
			break;
		case VT_UI1:
			val = vtFld.iVal;
			break;
		case VT_I2:
		case VT_I4:
			val = vtFld.lVal;
			break;
		case VT_INT:
			val = vtFld.intVal;
			break;
		case VT_NULL:
		case VT_EMPTY:
			val = 0;
			break;
		default:
			val = 0;
		}
		dValue = val;
		return TRUE;
	}
	catch(_com_error &e)
	{
		DumpError(e);
		return FALSE;
	}
}

// =================================================
// 문자열 데이터 반환(RS) - CString
// =================================================
BOOL CADOClass::GetRsDataCString(LPCTSTR lpszColumn, CString& strValue)
{
	CString str = _T("");
	_variant_t vtFld;

	try
	{
		vtFld = m_pRs->Fields->GetItem(lpszColumn)->Value;
		switch(vtFld.vt) 
		{
		case VT_R4:
			str.Format(_T("%f"), vtFld.fltVal);
			break;
		case VT_R8:
			str.Format(_T("%f"), vtFld.dblVal);
			break;
		case VT_BSTR:
			str = vtFld.bstrVal;
			break;
		case VT_I2:
		case VT_UI1:
			str.Format(_T("%d"), vtFld.iVal);
			break;
		case VT_INT:
			str.Format(_T("%d"), vtFld.intVal);
			break;
		case VT_I4:
			str.Format(_T("%d"), vtFld.lVal);
			break;
		case VT_UI4:
			str.Format(_T("%d"), vtFld.ulVal);
			break;
		case VT_DECIMAL:
			{
			//Corrected by Jos?Carlos Mart?ez Gal?
			double val = vtFld.decVal.Lo32;
			val *= (vtFld.decVal.sign == 128)? -1 : 1;
			val /= pow(10, vtFld.decVal.scale); 
			str.Format(_T("%f"), val);
			}
			break;
		case VT_DATE:
			{
				COleDateTime dt(vtFld);

				CString strDateFormat = _T("%Y-%m-%d %H:%M:%S");
				str = dt.Format(strDateFormat);
			}
			break;
		case VT_EMPTY:
		case VT_NULL:
			str.Empty();
			break;
		case VT_BOOL:
			str = vtFld.boolVal == VARIANT_TRUE? 'T':'F';
			break;
		default:
			str.Empty();
			return FALSE;
		}
		strValue = str;
		return TRUE;
	}
	catch(_com_error &e)
	{
		DumpError(e);
		return FALSE;
	}

	return TRUE;
}

BOOL CADOClass::GetRsDataCString(int nIndex, CString& strValue)
{
	CString str = _T("");
	_variant_t vtFld;
	_variant_t vtIndex;

	vtIndex.vt = VT_I2;
	vtIndex.iVal = nIndex;
	
	try
	{
		vtFld = m_pRs->Fields->GetItem(vtIndex)->Value;
		switch(vtFld.vt) 
		{
		case VT_R4:
			str.Format(_T("%f"), vtFld.fltVal);
			break;
		case VT_R8:
			str.Format(_T("%f"), vtFld.dblVal);
			break;
		case VT_BSTR:
			str = vtFld.bstrVal;
			break;
		case VT_I2:
		case VT_UI1:
			str.Format(_T("%d"), vtFld.iVal);
			break;
		case VT_INT:
			str.Format(_T("%d"), vtFld.intVal);
			break;
		case VT_I4:
			str.Format(_T("%d"), vtFld.lVal);
			break;
		case VT_UI4:
			str.Format(_T("%d"), vtFld.ulVal);
			break;
		case VT_DECIMAL:
			{
			//Corrected by Jos?Carlos Mart?ez Gal?
			double val = vtFld.decVal.Lo32;
			val *= (vtFld.decVal.sign == 128)? -1 : 1;
			val /= pow(10, vtFld.decVal.scale); 
			str.Format(_T("%f"), val);
			}
			break;
		case VT_DATE:
			{
				COleDateTime dt(vtFld);
				
				CString strDateFormat = _T("%Y-%m-%d %H:%M:%S");
				str = dt.Format(strDateFormat);
			}
			break;
		case VT_BOOL:
			str = vtFld.boolVal == VARIANT_TRUE? 'T':'F';
			break;
		case VT_EMPTY:
		case VT_NULL:
			str.Empty();
			break;
		default:
			str.Empty();
			return FALSE;
		}
		strValue = str;
		return TRUE;
	}
	catch(_com_error &e)
	{
		DumpError(e);
		return FALSE;
	}
}

// =================================================
// 날짜 데이터 반환(RS) - CTime
// =================================================
BOOL CADOClass::GetRsDataTime(LPCTSTR lpszColumn, CTime &cTime)
{
	if( m_pRs == NULL) return FALSE;
	_variant_t var;

	SYSTEMTIME	SystemTime ;
	DATE		date;

	try
	{
		var = m_pRs->GetFields()->GetItem(lpszColumn)->GetValue();
		switch(var.vt) 
		{
		case VT_DATE:
			date = var.date;				
			if(VariantTimeToSystemTime(date, &SystemTime) == TRUE){
				//CTime이 표현할수 있는 범위를 넘어서면
				if ((1970 > SystemTime.wYear) | (3000 < SystemTime.wYear)){
					cTime = 0;
				}
				else{
					CTime cDTime(SystemTime.wYear, SystemTime.wMonth, SystemTime.wDay,
						SystemTime.wHour, SystemTime.wMinute, SystemTime.wSecond );

					cTime = (CTime)cDTime;
				}
			}
			break;
		case VT_EMPTY:
		case VT_NULL:
			cTime = 0;
			break;
		default:
			return FALSE;
		}
	}
	catch(_com_error &e){
		DumpError(e) ;
		return FALSE;
	}

	return TRUE;
}

BOOL CADOClass::GetRsDataTime(int nIndex, CTime &cTime)
{
	if( m_pRs == NULL) return FALSE;
	_variant_t var;
	_variant_t vtIndex;
	
	vtIndex.vt = VT_I2;
	vtIndex.iVal = nIndex;

	SYSTEMTIME	SystemTime ;
	DATE		date;

	try
	{
		var = m_pRs->GetFields()->GetItem(vtIndex)->GetValue();
		switch(var.vt) 
		{
		case VT_DATE:
			date = var.date;				
			if(VariantTimeToSystemTime(date, &SystemTime) == TRUE){
				//CTime이 표현할수 있는 범위를 넘어서면
				if ((1970 > SystemTime.wYear) | (3000 < SystemTime.wYear)){
					cTime = 0;
				}
				else{
					CTime cDTime(SystemTime.wYear, SystemTime.wMonth, SystemTime.wDay,
						SystemTime.wHour, SystemTime.wMinute, SystemTime.wSecond );

					cTime = (CTime)cDTime;
				}
			}
			break;
		case VT_EMPTY:
		case VT_NULL:
			cTime = 0;
			break;
		default:
			return FALSE;
		}
	}
	catch(_com_error &e){
		DumpError(e) ;
		return FALSE;
	}

	return TRUE;
}

// 다음 레코드 목록을 불러오는 함수
int CADOClass::NextRecord(void)
{
	DWORD_PTR lngRec = 0;
	m_pRs = m_pRs->NextRecordset((VARIANT *)lngRec);
	return 0;
}

// 해당 칼럼으로 필터링하는 함수
int CADOClass::setFilter(_bstr_t strField, _bstr_t strFilter)
{
	m_pRs->Filter  = strField + " = '" + strFilter + "'";
	
	if (m_pRs->GetRecordCount() == 0)
		return 1;

	return 0;
}

// 레코드수를 리턴하는 함수
long CADOClass::GetRecordCount(void)
{
	return m_pRs->RecordCount;
}