// Dlg_TrayStacker.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "AMKOR_MMI.h"
#include "Dlg_TrayStacker.h"
#include "afxdialogex.h"


// CDlg_TrayStacker 대화 상자입니다.

IMPLEMENT_DYNAMIC(CDlg_TrayStacker, CDialogEx)

CDlg_TrayStacker::CDlg_TrayStacker(CWnd* pParent /*=NULL*/)
	: CDialogEx(CDlg_TrayStacker::IDD, pParent)
{
	m_bCtrl_FirstLoad = FALSE;
}

CDlg_TrayStacker::~CDlg_TrayStacker()
{
}

void CDlg_TrayStacker::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_GC_TS_1, m_GC_1);
	DDX_Control(pDX, IDC_GC_TS_2, m_GC_2);
	DDX_Control(pDX, IDC_GC_TS_3, m_GC_3);
	DDX_Control(pDX, IDC_BTN_TS_1, m_btn_1);
}


BEGIN_MESSAGE_MAP(CDlg_TrayStacker, CDialogEx)
	ON_WM_ERASEBKGND()
	ON_WM_SIZE()
	ON_WM_TIMER()
	ON_NOTIFY(NM_CLICK, IDC_GC_TS_1, OnGridClick_GC_1)
	ON_NOTIFY(NM_CLICK, IDC_GC_TS_2, OnGridClick_GC_2)
	ON_NOTIFY(NM_CLICK, IDC_GC_TS_3, OnGridClick_GC_3)
	ON_BN_CLICKED(IDC_BTN_TS_1, &CDlg_TrayStacker::OnBnClickedBtnTs1)
END_MESSAGE_MAP()


// CDlg_TrayStacker 메시지 처리기입니다.


BOOL CDlg_TrayStacker::OnEraseBkgnd(CDC* pDC)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	// 전체 배경색
	CRect rect;
    GetClientRect(&rect);
    CBrush myBrush(CR_MAIN_BKGND);	// dialog background color
    CBrush *pOld = pDC->SelectObject(&myBrush);
    BOOL bRes  = pDC->PatBlt(0, 0, rect.Width(), rect.Height(), PATCOPY);
    pDC->SelectObject(pOld);		// restore old brush
    return FALSE;     
	//return CDialogEx::OnEraseBkgnd(pDC);
}


void CDlg_TrayStacker::OnSize(UINT nType, int cx, int cy)
{
	CDialogEx::OnSize(nType, cx, cy);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	if(m_bCtrl_FirstLoad == TRUE) Cal_CtrlArea();
}


void CDlg_TrayStacker::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CDialogEx::OnTimer(nIDEvent);
}


BOOL CDlg_TrayStacker::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	Cal_CtrlArea();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


BOOL CDlg_TrayStacker::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message == WM_SYSKEYDOWN)
	{
		if(pMsg->wParam == VK_F4) 
			return TRUE;
	}

	if(pMsg->message == WM_KEYDOWN)
	{
		if( pMsg->wParam == VK_ESCAPE ||	
			pMsg->wParam == VK_RETURN ||	
			pMsg->wParam == VK_SPACE  ||
			pMsg->wParam == VK_CANCEL )     
			return TRUE;        
	}

	return CDialogEx::PreTranslateMessage(pMsg);
}

void CDlg_TrayStacker::Cal_Area()
{
	CRect ClientRect;
	GetClientRect(&ClientRect);

	int n80pW = (int)((float)ClientRect.Width()*0.80f);

	m_rcLeft.SetRect(ClientRect.left, ClientRect.top, ClientRect.left+n80pW, ClientRect.bottom);
	m_rcRight.SetRect(ClientRect.left+n80pW+CTRL_MARGIN4, ClientRect.top, ClientRect.right, ClientRect.bottom);
}

void CDlg_TrayStacker::Cal_CtrlArea()
{
	Cal_Area();

	Cal_CtrlArea_Left();
	Cal_CtrlArea_Right();

	m_bCtrl_FirstLoad = TRUE;
}

void CDlg_TrayStacker::Cal_CtrlArea_Left()
{
	CRect CalRect;
	CRect rcGB, rcDivH[3];

	rcGB = m_rcLeft;
	rcGB.DeflateRect(6,6,6,6);
	int nDiv3H = (int)((float)rcGB.Height()/3.0f);

	rcDivH[0].SetRect(rcGB.left, rcGB.top, rcGB.right, rcGB.top+nDiv3H);
	rcDivH[1].SetRect(rcGB.left, rcDivH[0].bottom, rcGB.right, rcDivH[0].bottom+nDiv3H);
	rcDivH[2].SetRect(rcGB.left, rcDivH[1].bottom, rcGB.right, rcGB.bottom);

	m_GC_1.MoveWindow(&rcDivH[0]);
	Init_GC_1(4,10);

	m_GC_2.MoveWindow(&rcDivH[1]);
	Init_GC_2(4,10);

	m_GC_3.MoveWindow(&rcDivH[2]);
	Init_GC_3(4,10);
}

void CDlg_TrayStacker::Cal_CtrlArea_Right()
{
	CRect CalRect;
	CRect rcBottom;
	int nBtnH = 60;
	int nBtnW = 120;

	rcBottom.SetRect(m_rcRight.left, m_rcRight.bottom - nBtnH, m_rcRight.right, m_rcRight.bottom);

	CalRect.SetRect(rcBottom.right-nBtnW, rcBottom.top, rcBottom.right, rcBottom.bottom);
	m_btn_1.SetFontBtn(CTRL_FONT, 20, FW_BOLD);
	m_btn_1.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_1.SetTextColor(CR_BLACK);
	m_btn_1.SetIconPos(0);
	m_btn_1.SetIconID(IDI_CLOSE);
	m_btn_1.SetIconSize(48,48);
	m_btn_1.SetFlatType(TRUE);
	m_btn_1.MoveWindow(&CalRect);
}

bool CDlg_TrayStacker::Init_GC_1(int nRows, int nCols)
{
	BOOL m_bEditable = FALSE;
    BOOL m_bListMode = TRUE;
    int m_nRows = nRows;
    int m_nCols = nCols;
    int m_nFixRows = 1;
    int m_nFixCols = 0;

    m_GC_1.SetEditable(m_bEditable);
    m_GC_1.SetListMode(m_bListMode);
    m_GC_1.EnableDragAndDrop(FALSE);
    m_GC_1.SetTextBkColor(RGB(0xFF, 0xFF, 0xE0));

    m_GC_1.SetRowCount(m_nRows);
    m_GC_1.SetColumnCount(m_nCols);
    m_GC_1.SetFixedRowCount(m_nFixRows);
    m_GC_1.SetFixedColumnCount(m_nFixCols);

	m_GC_1.EnableSelection(false);
	m_GC_1.SetSingleColSelection(true);
	m_GC_1.SetSingleRowSelection(true);
	m_GC_1.SetFixedColumnSelection(false);
    m_GC_1.SetFixedRowSelection(false);

	m_GC_1.SetRowResize(false);
	m_GC_1.SetColumnResize(false);

	CRect ClientRect;
	GetDlgItem(IDC_GC_TS_1)->GetWindowRect(&ClientRect);
	if(ClientRect.Width() < 4) return false;
	if(ClientRect.Height() < 4) return false;
	int nClientWidth = ClientRect.Width()-4;
	int nClientHeight = ClientRect.Height()-4;
	int n10pW = (int)(nClientWidth*0.1f);
	int nRemainW = nClientWidth - n10pW*9;
	//int nCellW = nClientWidth/m_nCols;
	int nCellH = nClientHeight/m_nRows;
	//int nLastCellW = nClientWidth-nCellW*(m_nCols-1);
	int nLastCellH = nClientHeight-nCellH*(m_nRows-1);
	//int nRemainW = nLastCellW - nCellW;
	int nRemainH = nLastCellH - nCellH;
	for (int i = 0; i < m_GC_1.GetRowCount(); i++) 
	{
		if(i == m_nRows-1)
		{
			m_GC_1.SetRowHeight(i,nCellH);
		}
		else 
		{
			if(nRemainH > 0)
			{
				m_GC_1.SetRowHeight(i,nCellH + 1);
				nRemainH --;
			}
			else
				m_GC_1.SetRowHeight(i,nCellH);
		}
	}
	for (int j = 0; j < m_GC_1.GetColumnCount(); j++)
	{
		if(j == 0) m_GC_1.SetColumnWidth(j,n10pW);
		else if(j == 1) m_GC_1.SetColumnWidth(j,n10pW);
		else if(j == 2) m_GC_1.SetColumnWidth(j,n10pW);
		else if(j == 3) m_GC_1.SetColumnWidth(j,n10pW);
		else if(j == 4) m_GC_1.SetColumnWidth(j,n10pW);
		else if(j == 5) m_GC_1.SetColumnWidth(j,n10pW);
		else if(j == 6) m_GC_1.SetColumnWidth(j,n10pW);
		else if(j == 7) m_GC_1.SetColumnWidth(j,n10pW);
		else if(j == 8) m_GC_1.SetColumnWidth(j,n10pW);
		else if(j == 9) m_GC_1.SetColumnWidth(j,nRemainW);
	}
    DWORD dwTextStyle = DT_CENTER|DT_VCENTER|DT_SINGLELINE;

    // fill rows/cols with text
    for (int row = 0; row < m_GC_1.GetRowCount(); row++) 
	{
        for (int col = 0; col < m_GC_1.GetColumnCount(); col++) 
		{ 
            GV_ITEM Item;
            Item.mask = GVIF_TEXT|GVIF_FORMAT;
            Item.row = row;
            Item.col = col;
			Item.nFormat = dwTextStyle;

			if(row == 0)
			{
				if(col == 0) Item.strText.Format(_T("21"));
				else if(col == 1) Item.strText.Format(_T("22"));
				else if(col == 2) Item.strText.Format(_T("23"));
				else if(col == 3) Item.strText.Format(_T("24"));
				else if(col == 4) Item.strText.Format(_T("25"));
				else if(col == 5) Item.strText.Format(_T("26"));
				else if(col == 6) Item.strText.Format(_T("27"));
				else if(col == 7) Item.strText.Format(_T("28"));
				else if(col == 8) Item.strText.Format(_T("29"));
				else if(col == 9) Item.strText.Format(_T("30"));
			}
			else
			{
			}

			if(row == 0) m_GC_1.SetItemBkColour(row, col, CR_SLATEGRAY);
			m_GC_1.SetItemFgColour(row, col, RGB(0,0,0));
			m_GC_1.SetItemFont(row, col, CTRL_FONT, 18, FW_BOLD);
            m_GC_1.SetItem(&Item);  
        }
    }
       
    return TRUE;  // return TRUE  unless you set the focus to a control
}

void CDlg_TrayStacker::OnGridClick_GC_1(NMHDR *pNotifyStruct, LRESULT* /*pResult*/)
{
	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*) pNotifyStruct;

	int nSelRow = pItem->iRow;
	int nSelCol = pItem->iColumn;
}

bool CDlg_TrayStacker::Init_GC_2(int nRows, int nCols)
{
	BOOL m_bEditable = FALSE;
    BOOL m_bListMode = TRUE;
    int m_nRows = nRows;
    int m_nCols = nCols;
    int m_nFixRows = 1;
    int m_nFixCols = 0;

    m_GC_2.SetEditable(m_bEditable);
    m_GC_2.SetListMode(m_bListMode);
    m_GC_2.EnableDragAndDrop(FALSE);
    m_GC_2.SetTextBkColor(RGB(0xFF, 0xFF, 0xE0));

    m_GC_2.SetRowCount(m_nRows);
    m_GC_2.SetColumnCount(m_nCols);
    m_GC_2.SetFixedRowCount(m_nFixRows);
    m_GC_2.SetFixedColumnCount(m_nFixCols);

	m_GC_2.EnableSelection(false);
	m_GC_2.SetSingleColSelection(true);
	m_GC_2.SetSingleRowSelection(true);
	m_GC_2.SetFixedColumnSelection(false);
    m_GC_2.SetFixedRowSelection(false);

	m_GC_2.SetRowResize(false);
	m_GC_2.SetColumnResize(false);

	CRect ClientRect;
	GetDlgItem(IDC_GC_TS_2)->GetWindowRect(&ClientRect);
	if(ClientRect.Width() < 4) return false;
	if(ClientRect.Height() < 4) return false;
	int nClientWidth = ClientRect.Width()-4;
	int nClientHeight = ClientRect.Height()-4;
	int n10pW = (int)(nClientWidth*0.1f);
	int nRemainW = nClientWidth - n10pW*9;
	//int nCellW = nClientWidth/m_nCols;
	int nCellH = nClientHeight/m_nRows;
	//int nLastCellW = nClientWidth-nCellW*(m_nCols-1);
	int nLastCellH = nClientHeight-nCellH*(m_nRows-1);
	//int nRemainW = nLastCellW - nCellW;
	int nRemainH = nLastCellH - nCellH;
	for (int i = 0; i < m_GC_2.GetRowCount(); i++) 
	{
		if(i == m_nRows-1)
		{
			m_GC_2.SetRowHeight(i,nCellH);
		}
		else 
		{
			if(nRemainH > 0)
			{
				m_GC_2.SetRowHeight(i,nCellH + 1);
				nRemainH --;
			}
			else
				m_GC_2.SetRowHeight(i,nCellH);
		}
	}
	for (int j = 0; j < m_GC_2.GetColumnCount(); j++)
	{
		if(j == 0) m_GC_2.SetColumnWidth(j,n10pW);
		else if(j == 1) m_GC_2.SetColumnWidth(j,n10pW);
		else if(j == 2) m_GC_2.SetColumnWidth(j,n10pW);
		else if(j == 3) m_GC_2.SetColumnWidth(j,n10pW);
		else if(j == 4) m_GC_2.SetColumnWidth(j,n10pW);
		else if(j == 5) m_GC_2.SetColumnWidth(j,n10pW);
		else if(j == 6) m_GC_2.SetColumnWidth(j,n10pW);
		else if(j == 7) m_GC_2.SetColumnWidth(j,n10pW);
		else if(j == 8) m_GC_2.SetColumnWidth(j,n10pW);
		else if(j == 9) m_GC_2.SetColumnWidth(j,nRemainW);
	}
    DWORD dwTextStyle = DT_CENTER|DT_VCENTER|DT_SINGLELINE;

    // fill rows/cols with text
    for (int row = 0; row < m_GC_2.GetRowCount(); row++) 
	{
        for (int col = 0; col < m_GC_2.GetColumnCount(); col++) 
		{ 
            GV_ITEM Item;
            Item.mask = GVIF_TEXT|GVIF_FORMAT;
            Item.row = row;
            Item.col = col;
			Item.nFormat = dwTextStyle;

			if(row == 0)
			{
				if(col == 0) Item.strText.Format(_T("11"));
				else if(col == 1) Item.strText.Format(_T("12"));
				else if(col == 2) Item.strText.Format(_T("13"));
				else if(col == 3) Item.strText.Format(_T("14"));
				else if(col == 4) Item.strText.Format(_T("15"));
				else if(col == 5) Item.strText.Format(_T("16"));
				else if(col == 6) Item.strText.Format(_T("17"));
				else if(col == 7) Item.strText.Format(_T("18"));
				else if(col == 8) Item.strText.Format(_T("19"));
				else if(col == 9) Item.strText.Format(_T("20"));
			}
			else
			{
			}

			if(row == 0) m_GC_2.SetItemBkColour(row, col, CR_SLATEGRAY);
			m_GC_2.SetItemFgColour(row, col, RGB(0,0,0));
			m_GC_2.SetItemFont(row, col, CTRL_FONT, 18, FW_BOLD);
            m_GC_2.SetItem(&Item);  
        }
    }
       
    return TRUE;  // return TRUE  unless you set the focus to a control
}

void CDlg_TrayStacker::OnGridClick_GC_2(NMHDR *pNotifyStruct, LRESULT* /*pResult*/)
{
	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*) pNotifyStruct;

	int nSelRow = pItem->iRow;
	int nSelCol = pItem->iColumn;
}

bool CDlg_TrayStacker::Init_GC_3(int nRows, int nCols)
{
	BOOL m_bEditable = FALSE;
    BOOL m_bListMode = TRUE;
    int m_nRows = nRows;
    int m_nCols = nCols;
    int m_nFixRows = 1;
    int m_nFixCols = 0;

    m_GC_3.SetEditable(m_bEditable);
    m_GC_3.SetListMode(m_bListMode);
    m_GC_3.EnableDragAndDrop(FALSE);
    m_GC_3.SetTextBkColor(RGB(0xFF, 0xFF, 0xE0));

    m_GC_3.SetRowCount(m_nRows);
    m_GC_3.SetColumnCount(m_nCols);
    m_GC_3.SetFixedRowCount(m_nFixRows);
    m_GC_3.SetFixedColumnCount(m_nFixCols);

	m_GC_3.EnableSelection(false);
	m_GC_3.SetSingleColSelection(true);
	m_GC_3.SetSingleRowSelection(true);
	m_GC_3.SetFixedColumnSelection(false);
    m_GC_3.SetFixedRowSelection(false);

	m_GC_3.SetRowResize(false);
	m_GC_3.SetColumnResize(false);

	CRect ClientRect;
	GetDlgItem(IDC_GC_TS_3)->GetWindowRect(&ClientRect);
	if(ClientRect.Width() < 4) return false;
	if(ClientRect.Height() < 4) return false;
	int nClientWidth = ClientRect.Width()-4;
	int nClientHeight = ClientRect.Height()-4;
	int n10pW = (int)(nClientWidth*0.1f);
	int nRemainW = nClientWidth - n10pW*9;
	//int nCellW = nClientWidth/m_nCols;
	int nCellH = nClientHeight/m_nRows;
	//int nLastCellW = nClientWidth-nCellW*(m_nCols-1);
	int nLastCellH = nClientHeight-nCellH*(m_nRows-1);
	//int nRemainW = nLastCellW - nCellW;
	int nRemainH = nLastCellH - nCellH;
	for (int i = 0; i < m_GC_3.GetRowCount(); i++) 
	{
		if(i == m_nRows-1)
		{
			m_GC_3.SetRowHeight(i,nCellH);
		}
		else 
		{
			if(nRemainH > 0)
			{
				m_GC_3.SetRowHeight(i,nCellH + 1);
				nRemainH --;
			}
			else
				m_GC_3.SetRowHeight(i,nCellH);
		}
	}
	for (int j = 0; j < m_GC_3.GetColumnCount(); j++)
	{
		if(j == 0) m_GC_3.SetColumnWidth(j,n10pW);
		else if(j == 1) m_GC_3.SetColumnWidth(j,n10pW);
		else if(j == 2) m_GC_3.SetColumnWidth(j,n10pW);
		else if(j == 3) m_GC_3.SetColumnWidth(j,n10pW);
		else if(j == 4) m_GC_3.SetColumnWidth(j,n10pW);
		else if(j == 5) m_GC_3.SetColumnWidth(j,n10pW);
		else if(j == 6) m_GC_3.SetColumnWidth(j,n10pW);
		else if(j == 7) m_GC_3.SetColumnWidth(j,n10pW);
		else if(j == 8) m_GC_3.SetColumnWidth(j,n10pW);
		else if(j == 9) m_GC_3.SetColumnWidth(j,nRemainW);
	}
    DWORD dwTextStyle = DT_CENTER|DT_VCENTER|DT_SINGLELINE;

    // fill rows/cols with text
    for (int row = 0; row < m_GC_3.GetRowCount(); row++) 
	{
        for (int col = 0; col < m_GC_3.GetColumnCount(); col++) 
		{ 
            GV_ITEM Item;
            Item.mask = GVIF_TEXT|GVIF_FORMAT;
            Item.row = row;
            Item.col = col;
			Item.nFormat = dwTextStyle;

			if(row == 0)
			{
				if(col == 0) Item.strText.Format(_T("1"));
				else if(col == 1) Item.strText.Format(_T("2"));
				else if(col == 2) Item.strText.Format(_T("3"));
				else if(col == 3) Item.strText.Format(_T("4"));
				else if(col == 4) Item.strText.Format(_T("5"));
				else if(col == 5) Item.strText.Format(_T("6"));
				else if(col == 6) Item.strText.Format(_T("7"));
				else if(col == 7) Item.strText.Format(_T("8"));
				else if(col == 8) Item.strText.Format(_T("9"));
				else if(col == 9) Item.strText.Format(_T("10"));
			}
			else
			{
			}

			if(row == 0) m_GC_3.SetItemBkColour(row, col, CR_SLATEGRAY);
			m_GC_3.SetItemFgColour(row, col, RGB(0,0,0));
			m_GC_3.SetItemFont(row, col, CTRL_FONT, 18, FW_BOLD);
            m_GC_3.SetItem(&Item);  
        }
    }
       
    return TRUE;  // return TRUE  unless you set the focus to a control
}

void CDlg_TrayStacker::OnGridClick_GC_3(NMHDR *pNotifyStruct, LRESULT* /*pResult*/)
{
	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*) pNotifyStruct;

	int nSelRow = pItem->iRow;
	int nSelCol = pItem->iColumn;
}

void CDlg_TrayStacker::OnBnClickedBtnTs1()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CDialogEx::OnOK();
}
