// MxComp3.h: interface for the CMxComp3 class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MXCOMP3_H__09303D48_7AAC_49AF_B8F2_D6A8C5767509__INCLUDED_)
#define AFX_MXCOMP3_H__09303D48_7AAC_49AF_B8F2_D6A8C5767509__INCLUDED_

//#import "F:\\MELSEC\\Act\\Control\\ActMulti.dll" rename_namespace("ActEasy")
#import "C:\\MELSEC\\Act\\Control\\ActMulti.dll" rename_namespace("ActEasy")
using namespace ActEasy;

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CMxComp3  
{
private:
	IActEasyIF3Ptr		m_pIEasyIF;			// ACT Control (Custom Interface)
	BOOL				m_bOpen;
	BOOL				m_bCreate;
	LONG				m_lLastResult;

	LONG*				m_pDataBlockWBuffer;
	LONG*				m_pDataBlockRBuffer;
	LONG				m_lDataBlockWCnt;		// WORD ���� ����
	LONG				m_lDataBlockRCnt;		// WORD ���� ����

protected:
	CRITICAL_SECTION m_csLock;
	class Lock
	{
	protected:
		CRITICAL_SECTION& m_cs;
	public:
		Lock(CRITICAL_SECTION& cs) : m_cs(cs) { ::EnterCriticalSection(&m_cs); }
		~Lock() { ::LeaveCriticalSection(&m_cs); }
	};

public:
	CMxComp3();
	~CMxComp3();

	BOOL Create();
	BOOL Destroy();

	BOOL Open( LONG lLogicalStationNumber );
	LONG GetLogicalStationNumber();
	BOOL Close();

	LPCTSTR GetCpuType( LONG* lpCpuCode );

	LONG GetLastResultCode() { return m_lLastResult; }

	BOOL WriteValue( LPCTSTR lpszDevice, LONG lValue );
	BOOL ReadValue( LPCTSTR lpszDevice, LONG* lpValue );

	BOOL ReadDeviceRandom( LPCTSTR lpszDevice, LONG lSize, LONG* lplData );
	BOOL WriteDeviceRandom( LPCTSTR lpszDevice, LONG lSize, LONG* lplData );
	BOOL ReadDeviceBlock( LPCTSTR lpszDevice, LONG lSize, LONG* lplData );
	BOOL WriteDeviceBlock( LPCTSTR lpszDevice, LONG lSize, LONG* lplData );

	BOOL WriteValue2( LPCTSTR lpszDevice, SHORT sValue );
	BOOL ReadValue2( LPCTSTR lpszDevice, SHORT* psValue );

	BOOL ReadDeviceRandom2( LPCTSTR lpszDevice, LONG lSize, SHORT* lpsData );
	BOOL WriteDeviceRandom2( LPCTSTR lpszDevice, LONG lSize, SHORT* lpsData );
	BOOL ReadDeviceBlock2( LPCTSTR lpszDevice, LONG lSize, SHORT* lpsData );
	BOOL WriteDeviceBlock2( LPCTSTR lpszDevice, LONG lSize, SHORT* lpsData );

	BOOL ReadBuffer( LONG lStartIO, LONG lAddress, LONG lSize, SHORT* lpsData );
	BOOL WriteBuffer( LONG lStartIO, LONG lAddress, LONG lSize, SHORT* lpsData );

	BOOL GetClockData( time_t* ptmClock );
	BOOL SetClockData( time_t tmClock );

};

#endif // !defined(AFX_MXCOMP3_H__09303D48_7AAC_49AF_B8F2_D6A8C5767509__INCLUDED_)
