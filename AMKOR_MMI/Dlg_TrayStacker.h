#pragma once
#include "afxwin.h"
#include "UIExt/CxStatic.h"
#include "UIExt/IconButton.h"
#include "UIExt/XPGroupBox.h"
#include "UIExt\/BorderStyleEdit.h"
#include "GC/gridctrl.h"

// CDlg_TrayStacker 대화 상자입니다.

class CDlg_TrayStacker : public CDialogEx
{
	DECLARE_DYNAMIC(CDlg_TrayStacker)

public:
	CDlg_TrayStacker(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CDlg_TrayStacker();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DLG_STACKER };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);

	void Cal_Area();
	void Cal_CtrlArea();
	void Cal_CtrlArea_Left();
	void Cal_CtrlArea_Right();

	bool Init_GC_1(int nRows, int nCols);
	afx_msg void OnGridClick_GC_1(NMHDR *pNotifyStruct, LRESULT* pResult);
	bool Init_GC_2(int nRows, int nCols);
	afx_msg void OnGridClick_GC_2(NMHDR *pNotifyStruct, LRESULT* pResult);
	bool Init_GC_3(int nRows, int nCols);
	afx_msg void OnGridClick_GC_3(NMHDR *pNotifyStruct, LRESULT* pResult);

	CRect m_rcLeft;
	CRect m_rcRight;

	BOOL m_bCtrl_FirstLoad;

	CGridCtrl m_GC_1;
	CGridCtrl m_GC_2;
	CGridCtrl m_GC_3;
	CIconButton m_btn_1;

	afx_msg void OnBnClickedBtnTs1();
};
