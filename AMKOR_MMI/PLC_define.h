#pragma once

#pragma pack(1)

////////////////////////////////////////////////////////////////////////////////////
// 시작 어드레스 정의 //////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////

// D 영역
#define DADDRESS_READ_G1_L1						_T("D30000")	// TRAY LOADING
#define DADDRESS_WRITE_G1_L1					_T("D30030")	// TRAY LOADING
#define DADDRESS_READ_G1_L2						_T("D30050")	// RFID
#define DADDRESS_WRITE_G1_L2					_T("D30080")	// RFID
#define DADDRESS_READ_G1_L3						_T("D30100")	// RF TRAY 카운트
#define DADDRESS_WRITE_G1_L3					_T("D30130")	// RF TRAY 카운트
#define DADDRESS_READ_G1_L4						_T("D30150")	// RF TRAY 제거
#define DADDRESS_WRITE_G1_L4					_T("D30180")	// RF TRAY 제거
#define DADDRESS_READ_G1_L5						_T("D30200")	// TRAY 소분
#define DADDRESS_WRITE_G1_L5					_T("D30230")	// TRAY 소분
#define DADDRESS_READ_G1_L6						_T("D30250")	// Cover Tray Load
#define DADDRESS_WRITE_G1_L6					_T("D30280")	// Cover Tray Load
#define DADDRESS_READ_G1_L7						_T("D30300")	// 사면 검사
#define DADDRESS_WRITE_G1_L7					_T("D30330")	// 사면 검사
#define DADDRESS_READ_G1_L8						_T("D30350")	// LABEL 검사
#define DADDRESS_WRITE_G1_L8					_T("D30380")	// LABEL 검사
#define DADDRESS_READ_G1_L9						_T("D30400")	// 벤딩 대기 버퍼
#define DADDRESS_WRITE_G1_L9					_T("D30430")	// 벤딩 대기 버퍼
#define DADDRESS_READ_G1_L10					_T("D30450")	// 벤딩
#define DADDRESS_WRITE_G1_L10					_T("D30480")	// 벤딩

#define DADDRESS_READ_G1						_T("D30000")
#define DADDRESS_READ_G2						_T("D30000")
#define DADDRESS_READ_G3						_T("D30000")

#define MADDRESS_READ_ALARM_G1					_T("M10000")
#define MADDRESS_READ_ALARM_G2					_T("M10000")
#define MADDRESS_READ_ALARM_G3					_T("M10000")

////////////////////////////////////////////////////////////////////////////////////
// M 영역 구조체 ///////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////

struct MADDRESS{
	BYTE m_xBit_0:1;
	BYTE m_xBit_1:1;
	BYTE m_xBit_2:1;
	BYTE m_xBit_3:1;
	BYTE m_xBit_4:1;
	BYTE m_xBit_5:1;
	BYTE m_xBit_6:1;
	BYTE m_xBit_7:1;
	BYTE m_xBit_8:1;
	BYTE m_xBit_9:1;
	BYTE m_xBit_A:1;
	BYTE m_xBit_B:1;
	BYTE m_xBit_C:1;
	BYTE m_xBit_D:1;
	BYTE m_xBit_E:1;
	BYTE m_xBit_F:1;
};

////////////////////////////////////////////////////////////////////////////////////
// M 영역 구조체 ///////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////

// 
struct READ_G1_ALARM{
	MADDRESS m_stMAddress[40];
};

// 
struct READ_G2_ALARM{
	MADDRESS m_stMAddress[40];
};

// 
struct READ_G3_ALARM{
	MADDRESS m_stMAddress[40];
};

////////////////////////////////////////////////////////////////////////////////////
// D 영역 구조체 ///////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////

struct WRITE_D_G1_L1
{
	MADDRESS m_stMAddress_W;	// Reserved Write
	SHORT m_sRsv_W[19];			// Reserved Write
};

struct WRITE_D_G1_L2
{
	MADDRESS m_stMAddress_W;	// Reserved Write
	SHORT m_sLotNum;			// LOT Number
	SHORT m_sTrayCount;			// 트레이 수량
	SHORT m_sTrayHeight;		// 트레이 높이 타입
	SHORT m_sTrayType;			// 트레이 타입 : 0-Module향, 1-Customer, 2-OQ
	SHORT m_sRsv_W[15];			// Reserved Write
};

struct WRITE_D_G1_L6
{
	MADDRESS m_stMAddress_W;	// Reserved Write
	SHORT m_sRsv_W[19];			// Reserved Write
};

struct WRITE_D_G1_L8
{
	MADDRESS m_stMAddress_W;	// Reserved Write
	SHORT m_sLabelCount;		// 라벨 발행 수량
	SHORT m_sRsv_W[18];			// Reserved Write
};

struct WRITE_D_G2_L5
{
	MADDRESS m_stMAddress_W;	// Reserved Write
	SHORT m_sLabelCount;		// 라벨 발행 수량
	SHORT m_sRsv_W[18];			// Reserved Write
};

struct WRITE_D_G3_L1
{
	MADDRESS m_stMAddress_W;	// Reserved Write
	SHORT m_sAlbagType;			// 이종 ALBAG TYPE
	SHORT m_sLabelCount;		// 라벨 수량
	SHORT m_sRsv_W[17];			// Reserved Write
};

struct WRITE_D_G3_L3
{
	MADDRESS m_stMAddress_W;	// Reserved Write
	SHORT m_sBoxType;			// 이종 BOX TYPE
	SHORT m_sRsv_W[18];			// Reserved Write
};

struct WRITE_D_G3_L7
{
	MADDRESS m_stMAddress_W;	// Reserved Write
	SHORT m_sLabelCount;		// 라벨 발행 수량
	SHORT m_sRsv_W[18];			// Reserved Write
};

struct WRITE_D_G3_L8
{
	MADDRESS m_stMAddress_W;	// Reserved Write
	SHORT m_sRsv_W[19];			// Reserved Write
};

struct stG1_L1{
	MADDRESS m_stMAddress_R;	// 자재 유무
	SHORT m_sMoveInfo;			// 이송 상태
	SHORT m_sRsv_R[28];			// Reserved Read
	WRITE_D_G1_L1 m_stWrite;	// WRITE 영역
};

struct stG1_L2{
	MADDRESS m_stMAddress_R;	// 자재 유무
	SHORT m_sMoveInfo;			// 이송 상태
	SHORT m_sReserved_1;		// Reserved
	SHORT m_sRFID[20];			// RFID
	SHORT m_sRsv_R[7];			// Reserved Read
	WRITE_D_G1_L2 m_stWrite;	// WRITE 영역
};

struct stG1_L3{
	MADDRESS m_stMAddress_R;	// 자재 유무
	SHORT m_sMoveInfo;			// 이송 상태
	SHORT m_sLotNum;			// LOT Number
	SHORT m_sTrayCount;			// 트레이 수량
	SHORT m_sRsv_R[26];			// Reserved Read
	MADDRESS m_stMAddress_W;	// Reserved Write
	SHORT m_sRsv_W[19];			// Reserved Write
};

struct stG1_L4{
	MADDRESS m_stMAddress_R;	// 자재 유무
	SHORT m_sMoveInfo;			// 이송 상태
	SHORT m_sLotNum;			// LOT Number
	SHORT m_sTrayCount;			// RF TRAY 제거후 트레이 수량
	SHORT m_sRsv_R[26];			// Reserved Read
	MADDRESS m_stMAddress_W;	// Reserved Write
	SHORT m_sRsv_W[19];			// Reserved Write
};

struct stG1_L5{
	MADDRESS m_stMAddress_R;	// 자재 유무
	SHORT m_sMoveInfo;			// 이송 상태
	SHORT m_sLotNum;			// LOT Number
	SHORT m_sTrayCount;			// 소분 트레이 수량
	SHORT m_sRsv_R[26];			// Reserved Read
	MADDRESS m_stMAddress_W;	// Reserved Write
	SHORT m_sRsv_W[19];			// Reserved Write
};

struct stG1_L6{
	MADDRESS m_stMAddress_R;	// 자재 유무
	SHORT m_sMoveInfo;			// 이송 상태
	SHORT m_sLotNum;			// LOT Number
	SHORT m_sMarkingResult;		// 마킹 결과
	SHORT m_sRsv_R[26];			// Reserved Read
	WRITE_D_G1_L6 m_stWrite;	// WRITE 영역
};

struct stG1_L7{
	MADDRESS m_stMAddress_R;	// 자재 유무
	SHORT m_sMoveInfo;			// 이송 상태
	SHORT m_sLotNum;			// LOT Number
	SHORT m_sSidlingResult;		// 사면 결과
	SHORT m_sRsv_R[26];			// Reserved Read
	MADDRESS m_stMAddress_W;	// Reserved Write
	SHORT m_sRsv_W[19];			// Reserved Write
};

struct stG1_L8{
	MADDRESS m_stMAddress_R;	// 자재 유무
	SHORT m_sMoveInfo;			// 이송 상태
	SHORT m_sLotNum;			// LOT Number
	SHORT m_sLabelResult;		// 라벨 결과
	SHORT m_sRsv_R[26];			// Reserved Read
	WRITE_D_G1_L8 m_stWrite;	// WRITE 영역
};

struct stG1_L9{
	MADDRESS m_stMAddress_R;	// 자재 유무
	SHORT m_sMoveInfo;			// 이송 상태
	SHORT m_sLotNum;			// LOT Number
	SHORT m_sRsv_R[27];			// Reserved Read
	MADDRESS m_stMAddress_W;	// Reserved Write
	SHORT m_sRsv_W[19];			// Reserved Write
};

struct stG1_L10{
	MADDRESS m_stMAddress_R;	// 자재 유무
	SHORT m_sMoveInfo;			// 이송 상태
	SHORT m_sLotNum;			// LOT Number
	SHORT m_sBandingResult;		// 밴딩 결과
	SHORT m_sRsv_R[26];			// Reserved Read
	MADDRESS m_stMAddress_W;	// Reserved Write
	SHORT m_sRsv_W[19];			// Reserved Write
};

// D30500 ~ D30673
struct stG1_MS{
	SHORT m_sCT[64];			// 커버트레이 스택커 64곳 수량 현황
	SHORT m_sRsv_R[36];			// Reserved Read
	MADDRESS m_stMAddress_R;	// 장비 부자재 상태
	SHORT m_sMchStatus;			// 장비 구동 상태
	SHORT m_sAlarmSet;			// 알람 상태 : 0 - Clear, 1 - Set
	SHORT m_sAutoMode;			// AUTO MODE : 0 - Manual, 1 - Auto
	SHORT m_sRsv_R1[6];			// Reserved Read
	SHORT m_sCTI[64];			// 커버트레이 스택커 64곳 인덱스 현황
};

struct stG2_L1{
	MADDRESS m_stMAddress_R;	// 자재 유무
	SHORT m_sMoveInfo;			// 이송 상태
	SHORT m_sRsv_R[28];			// Reserved Read
	MADDRESS m_stMAddress_W;	// Reserved Write
	SHORT m_sRsv_W[19];			// Reserved Write
};

struct stG2_L2{
	MADDRESS m_stMAddress_R;	// 자재 유무
	SHORT m_sMoveInfo;			// 이송 상태
	SHORT m_sRsv_R[28];			// Reserved Read
	MADDRESS m_stMAddress_W;	// Reserved Write
	SHORT m_sRsv_W[19];			// Reserved Write
};

struct stG2_L3{
	MADDRESS m_stMAddress_R;	// 자재 유무
	SHORT m_sMoveInfo;			// 이송 상태
	SHORT m_sRsv_R[28];			// Reserved Read
	MADDRESS m_stMAddress_W;	// Reserved Write
	SHORT m_sRsv_W[19];			// Reserved Write
};

struct stG2_L4{
	MADDRESS m_stMAddress_R;	// 자재 유무
	SHORT m_sMoveInfo;			// 이송 상태
	SHORT m_sRsv_R[28];			// Reserved Read
	MADDRESS m_stMAddress_W;	// Reserved Write
	SHORT m_sRsv_W[19];			// Reserved Write
};

struct stG2_L5{
	MADDRESS m_stMAddress_R;	// 자재 유무
	SHORT m_sMoveInfo;			// 이송 상태
	SHORT m_sTrayBarcode;		// 트레이 바코드 검사 결과
	SHORT m_sRsv_R[27];			// Reserved Read
	WRITE_D_G2_L5 m_stWrite;	// WRITE 영역
};

// D30250 ~ D30600
struct stG2_MS{
	SHORT m_sRsv_R[350];		// Reserved Read
	MADDRESS m_stMAddress_R;	// 장비 부자재 상태
	SHORT m_sMchStatus;			// 장비 구동 상태
	SHORT m_sAlarmSet;			// 알람 상태 : 0 - Clear, 1 - Set
	SHORT m_sAutoMode;			// AUTO MODE : 0 - Manual, 1 - Auto
};

struct stG3_L1{
	MADDRESS m_stMAddress_R;	// 자재 유무
	SHORT m_sMoveInfo;			// 이송 상태
	SHORT m_sLabelResult;		// 라벨 결과
	SHORT m_sRsv_R[27];			// Reserved Read
	WRITE_D_G3_L1 m_stWrite;	// WRITE 영역
};

struct stG3_L2{
	MADDRESS m_stMAddress_R;	// 자재 유무
	SHORT m_sMoveInfo;			// 이송 상태
	SHORT m_sRsv_R[28];			// Reserved Read
	MADDRESS m_stMAddress_W;	// Reserved Write
	SHORT m_sRsv_W[19];			// Reserved Write
};

struct stG3_L3{
	MADDRESS m_stMAddress_R;	// 자재 유무
	SHORT m_sMoveInfo;			// 이송 상태
	SHORT m_sRsv_R[28];			// Reserved Read
	WRITE_D_G3_L3 m_stWrite;	// WRITE 영역
};

struct stG3_L4{
	MADDRESS m_stMAddress_R;	// 자재 유무
	SHORT m_sMoveInfo;			// 이송 상태
	SHORT m_sAlbagBarcode;		// 알백 바코드 검사 결과
	SHORT m_sRsv_R[27];			// Reserved Read
	MADDRESS m_stMAddress_W;	// Reserved Write
	SHORT m_sRsv_W[19];			// Reserved Write
};

struct stG3_L5{
	MADDRESS m_stMAddress_R;	// 자재 유무
	SHORT m_sMoveInfo;			// 이송 상태
	SHORT m_sRsv_R[28];			// Reserved Read
	MADDRESS m_stMAddress_W;	// Reserved Write
	SHORT m_sRsv_W[19];			// Reserved Write
};

struct stG3_L6{
	MADDRESS m_stMAddress_R;	// 자재 유무
	SHORT m_sMoveInfo;			// 이송 상태
	SHORT m_sRsv_R[28];			// Reserved Read
	MADDRESS m_stMAddress_W;	// Reserved Write
	SHORT m_sRsv_W[19];			// Reserved Write
};

struct stG3_L7{
	MADDRESS m_stMAddress_R;	// 자재 유무
	SHORT m_sMoveInfo;			// 이송 상태
	SHORT m_sLabelResult;		// 라벨 결과
	SHORT m_sRsv_R[27];			// Reserved Read
	WRITE_D_G3_L7 m_stWrite;	// WRITE 영역
};

struct stG3_L8{
	MADDRESS m_stMAddress_R;	// 자재 유무
	SHORT m_sMoveInfo;			// 이송 상태
	SHORT m_sInBoxBarcode;		// 인박스 바코드 검사 결과
	SHORT m_sOutBoxBarcode;		// 아웃박스 바코드 검사 결과
	SHORT m_sRsv_R[26];			// Reserved Read
	WRITE_D_G3_L8 m_stWrite;	// WRITE 영역
};

// D30400 ~ D30600
struct stG3_MS{
	SHORT m_sRsv_R[200];		// Reserved Read
	MADDRESS m_stMAddress_R;	// 장비 부자재 상태
	SHORT m_sMchStatus;			// 장비 구동 상태
	SHORT m_sAlarmSet;			// 알람 상태 : 0 - Clear, 1 - Set
	SHORT m_sAutoMode;			// AUTO MODE : 0 - Manual, 1 - Auto
};

// D 영역 전체 구조체
struct READ_D_G1
{
	// D30000 - D30499
	stG1_L1 m_stG1_L1;		// TRAY LOADING
	stG1_L2 m_stG1_L2;		// RFID
	stG1_L3 m_stG1_L3;		// TRAY COUNT
	stG1_L4 m_stG1_L4;		// RF TRAY 제거
	stG1_L5 m_stG1_L5;		// 트레이 소분
	stG1_L6 m_stG1_L6;		// 커버 트레이
	stG1_L7 m_stG1_L7;		// 사면 들뜸 검사
	stG1_L8 m_stG1_L8;		// 라벨 발행
	stG1_L9 m_stG1_L9;		// 벤딩 이송 버퍼
	stG1_L10 m_stG1_L10;	// 벤딩
	// D30500 ~ D30600
	stG1_MS m_stG1_MS;		// 그룹1 부자재 현황
};

struct READ_D_G2
{
	// D30000 - D30249
	stG2_L1 m_stG2_L1;		// HIC 투입
	stG2_L2 m_stG2_L2;		// 실리카겔 투입
	stG2_L3 m_stG2_L3;		// PEFOAM 이송 피커
	stG2_L4 m_stG2_L4;		// 하부 PEFOAM
	stG2_L5 m_stG2_L5;		// 상부 PEFOAM
	// D30250 ~ D30600
	stG2_MS m_stG2_MS;		// 그룹2 부자재 현황
};

struct READ_D_G3
{
	// D30000 - D30399
	stG3_L1 m_stG3_L1;		// 실링 컨베어 #1
	stG3_L2 m_stG3_L2;		// 실링 컨베어 #2
	stG3_L3 m_stG3_L3;		// 실링 컨베어 #3
	stG3_L4 m_stG3_L4;		// 제함기 컨베어 #1
	stG3_L5 m_stG3_L5;		// 제함기 컨베어 #2
	stG3_L6 m_stG3_L6;		// 제함기 컨베어 #3
	stG3_L7 m_stG3_L7;		// 제함기 컨베어 #4
	stG3_L8 m_stG3_L8;		// BOX 배출
	// D30400 ~ D30600
	stG3_MS m_stG3_MS;		// 그룹3 부자재 현황
};

#pragma pack()
