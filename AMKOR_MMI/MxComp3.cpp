// MxComp3.cpp: implementation of the CMxComp3 class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "MxComp3.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMxComp3::CMxComp3() : m_bOpen(FALSE), m_bCreate(FALSE)
{
	::CoInitialize( NULL );
	m_pDataBlockWBuffer = NULL;
	m_lDataBlockWCnt = 0;
	m_pDataBlockRBuffer = NULL;
	m_lDataBlockRCnt = 0;
	::InitializeCriticalSection(&m_csLock);
}

CMxComp3::~CMxComp3()
{
	::CoUninitialize();
	if( m_pDataBlockWBuffer )
		delete[] m_pDataBlockWBuffer;
	m_pDataBlockWBuffer = NULL;
	if( m_pDataBlockRBuffer )
		delete[] m_pDataBlockRBuffer;
	m_pDataBlockRBuffer = NULL;
	::DeleteCriticalSection(&m_csLock);
}

BOOL CMxComp3::Create()
{
	Lock lock(m_csLock);
	HRESULT hr;
	hr = m_pIEasyIF.CreateInstance(__uuidof(ActEasyIF), NULL, CLSCTX_INPROC_SERVER );

	DWORD dwError = GetLastError();

	if(!SUCCEEDED(hr))
	{
		TRACE("CMxComp3::Create CoCreateInstance() Failed.\r\n");

		return FALSE;
	}
	
	m_bCreate = TRUE;

	return TRUE;
}

BOOL CMxComp3::Destroy()
{
	Lock lock(m_csLock);

	if( m_pIEasyIF != NULL )
		m_pIEasyIF.Release();

	m_bCreate = FALSE;
	return TRUE;
}

LONG CMxComp3::GetLogicalStationNumber()
{
	Lock lock(m_csLock);
	HRESULT	hr = 0;
	LONG lLogicalStationNumber;
	hr = m_pIEasyIF->get_ActLogicalStationNumber( &lLogicalStationNumber );

	return lLogicalStationNumber;
}

BOOL CMxComp3::Open( LONG lLogicalStationNumber )
{
	Lock lock(m_csLock);
	if( !m_bCreate ) 
	{
		TRACE( "CMxComp3::Open - Object is not Create!!\r\n" );
		return FALSE;
	}

	HRESULT	hr = 0;
	m_bOpen = FALSE;
	hr = m_pIEasyIF->put_ActLogicalStationNumber( lLogicalStationNumber );
	
	LONG lRet;
	if(SUCCEEDED(hr))
	{
		lRet = m_pIEasyIF->Open();	// Exec Open Method
	}

	m_lLastResult = lRet;

	if( !SUCCEEDED(hr) || m_lLastResult != 0 )
	{
		TRACE( "CMxComp3::Open - Open Error!!\r\n" );
		return FALSE;
	}

	m_bOpen = TRUE;
	return TRUE;
}

BOOL CMxComp3::Close()
{
	Lock lock(m_csLock);
	if( !m_bCreate )
		return FALSE;

	LONG lRet;
	lRet = m_pIEasyIF->Close();
	m_lLastResult = lRet;

	if( m_lLastResult != 0 )
	{
		return FALSE;
	}

	return TRUE;
}


LPCTSTR CMxComp3::GetCpuType( LONG* lpCpuCode )
{
	Lock lock(m_csLock);
	static CString strCpuType;
	LONG lCpuCode = 0;

	if( !m_bOpen || !m_bCreate )
	{
		if( lpCpuCode ) *lpCpuCode = lCpuCode;
		return strCpuType;
	}

	LONG lCRet;
	BSTR bstrCpuName = NULL;
	
	lCRet = m_pIEasyIF->GetCpuType( &bstrCpuName, &lCpuCode );
	strCpuType = bstrCpuName;

	m_lLastResult = lCRet;

	if( lpCpuCode ) *lpCpuCode = lCpuCode;
	return strCpuType;
}

BOOL CMxComp3::WriteValue( LPCTSTR lpszDevice, LONG lValue )
{
	Lock lock(m_csLock);
	if( !m_bOpen || !m_bCreate )
	{
		return FALSE;
	}

	CString strDevice = lpszDevice;

	LONG lRet;
	BSTR bstrDevice = strDevice.AllocSysString();

	lRet = m_pIEasyIF->SetDevice( bstrDevice, lValue );

	::SysFreeString( bstrDevice );

	m_lLastResult = lRet;

	if( m_lLastResult != 0 )
	{
		return FALSE;
	}
	
	return TRUE;
}

BOOL CMxComp3::ReadValue( LPCTSTR lpszDevice, LONG* lpValue )
{
	Lock lock(m_csLock);
	if( !m_bOpen || !m_bCreate )
	{
		return FALSE;
	}

	CString strDevice = lpszDevice;

	LONG lRet;
	BSTR bstrDevice = strDevice.AllocSysString();

	lRet = m_pIEasyIF->GetDevice( bstrDevice, lpValue );

	::SysFreeString( bstrDevice );

	m_lLastResult = lRet;

	if( m_lLastResult != 0 )
	{
		return FALSE;
	}

	return TRUE;
}
        
BOOL CMxComp3::ReadDeviceRandom( LPCTSTR lpszDevice, LONG lSize, LONG* lplData )
{
	Lock lock(m_csLock);
	if( !m_bOpen || !m_bCreate )
	{
		return FALSE;
	}

	CString strDevice = lpszDevice;

	LONG lRet;
	BSTR bstrDevice = strDevice.AllocSysString();

	lRet = m_pIEasyIF->ReadDeviceRandom( bstrDevice, lSize, lplData );

	::SysFreeString( bstrDevice );

	m_lLastResult = lRet;

	if( m_lLastResult != 0 )
	{
		return FALSE;
	}

	return TRUE;
}

BOOL CMxComp3::WriteDeviceRandom( LPCTSTR lpszDevice, LONG lSize, LONG* lplData )
{
	Lock lock(m_csLock);
	if( !m_bOpen || !m_bCreate )
	{
		return FALSE;
	}

	CString strDevice = lpszDevice;

	LONG lRet;
	BSTR bstrDevice = strDevice.AllocSysString();

	lRet = m_pIEasyIF->WriteDeviceRandom( bstrDevice, lSize, lplData );

	::SysFreeString( bstrDevice );

	m_lLastResult = lRet;

	if( m_lLastResult != 0 )
	{
		return FALSE;
	}

	return TRUE;
}

BOOL CMxComp3::ReadDeviceBlock( LPCTSTR lpszDevice, LONG lSize, LONG* lplData )
{
	Lock lock(m_csLock);
	if( !m_bOpen || !m_bCreate )
	{
		return FALSE;
	}

	CString strDevice = lpszDevice;

	LONG lRet;
	BSTR bstrDevice = strDevice.AllocSysString();

	lRet = m_pIEasyIF->ReadDeviceBlock( bstrDevice, lSize, lplData );

	::SysFreeString( bstrDevice );

	m_lLastResult = lRet;

	if( m_lLastResult != 0 )
	{
		return FALSE;
	}

	return TRUE;
}

BOOL CMxComp3::WriteDeviceBlock( LPCTSTR lpszDevice, LONG lSize, LONG* lplData )
{
	Lock lock(m_csLock);
	if( !m_bOpen || !m_bCreate )
	{
		return FALSE;
	}

	CString strDevice = lpszDevice;

	LONG lRet;
	BSTR bstrDevice = strDevice.AllocSysString();

	lRet = m_pIEasyIF->WriteDeviceBlock( bstrDevice, lSize, lplData );

	::SysFreeString( bstrDevice );

	m_lLastResult = lRet;

	if( m_lLastResult != 0 )
	{
		return FALSE;
	}

	return TRUE;
}

BOOL CMxComp3::WriteValue2( LPCTSTR lpszDevice, SHORT sValue )
{
	Lock lock(m_csLock);
	if( !m_bOpen || !m_bCreate )
	{
		return FALSE;
	}

	CString strDevice = lpszDevice;

	LONG lRet;
	BSTR bstrDevice = strDevice.AllocSysString();

	lRet = m_pIEasyIF->SetDevice2( bstrDevice, sValue );

	::SysFreeString( bstrDevice );

	m_lLastResult = lRet;

	if( m_lLastResult != 0 )
	{
		return FALSE;
	}
	
	return TRUE;
}

BOOL CMxComp3::ReadValue2( LPCTSTR lpszDevice, SHORT* psValue )
{
	Lock lock(m_csLock);
	if( !m_bOpen || !m_bCreate )
	{
		return FALSE;
	}

	CString strDevice = lpszDevice;

	LONG lRet;
	BSTR bstrDevice = strDevice.AllocSysString();

	lRet = m_pIEasyIF->GetDevice2( bstrDevice, psValue );

	::SysFreeString( bstrDevice );

	m_lLastResult = lRet;

	if( m_lLastResult != 0 )
	{
		return FALSE;
	}

	return TRUE;
}
        
BOOL CMxComp3::ReadDeviceRandom2( LPCTSTR lpszDevice, LONG lSize, SHORT* lpsData )
{
	Lock lock(m_csLock);
	if( !m_bOpen || !m_bCreate )
	{
		return FALSE;
	}

	CString strDevice = lpszDevice;

	LONG lRet;
	BSTR bstrDevice = strDevice.AllocSysString();

	lRet = m_pIEasyIF->ReadDeviceRandom2( bstrDevice, lSize, lpsData );

	::SysFreeString( bstrDevice );

	m_lLastResult = lRet;

	if( m_lLastResult != 0 )
	{
		return FALSE;
	}

	return TRUE;
}

BOOL CMxComp3::WriteDeviceRandom2( LPCTSTR lpszDevice, LONG lSize, SHORT* lpsData )
{
	Lock lock(m_csLock);
	if( !m_bOpen || !m_bCreate )
	{
		return FALSE;
	}

	CString strDevice = lpszDevice;

	LONG lRet;
	BSTR bstrDevice = strDevice.AllocSysString();

	lRet = m_pIEasyIF->WriteDeviceRandom2( bstrDevice, lSize, lpsData );

	::SysFreeString( bstrDevice );

	m_lLastResult = lRet;

	if( m_lLastResult != 0 )
	{
		return FALSE;
	}

	return TRUE;
}

BOOL CMxComp3::ReadDeviceBlock2( LPCTSTR lpszDevice, LONG lSize, SHORT* lpsData )
{
	Lock lock(m_csLock);
	if( !m_bOpen || !m_bCreate )
	{
		return FALSE;
	}

	CString strDevice = lpszDevice;

	LONG lRet;
	BSTR bstrDevice = strDevice.AllocSysString();

	lRet = m_pIEasyIF->ReadDeviceBlock2( bstrDevice, lSize, lpsData );

	::SysFreeString( bstrDevice );

	m_lLastResult = lRet;

	if( m_lLastResult != 0 )
	{
		return FALSE;
	}

	return TRUE;
}

BOOL CMxComp3::WriteDeviceBlock2( LPCTSTR lpszDevice, LONG lSize, SHORT* lpsData )
{
	Lock lock(m_csLock);
	if( !m_bOpen || !m_bCreate )
	{
		return FALSE;
	}

	CString strDevice = lpszDevice;

	LONG lRet;
	BSTR bstrDevice = strDevice.AllocSysString();

	lRet = m_pIEasyIF->WriteDeviceBlock2( bstrDevice, lSize, lpsData );

	::SysFreeString( bstrDevice );

	m_lLastResult = lRet;

	if( m_lLastResult != 0 )
	{
		return FALSE;
	}

	return TRUE;
}

BOOL CMxComp3::ReadBuffer( LONG lStartIO, LONG lAddress, LONG lSize, SHORT* lpsData )
{
	Lock lock(m_csLock);
	if( !m_bOpen || !m_bCreate )
	{
		return FALSE;
	}

	LONG lRet;
	lRet = m_pIEasyIF->ReadBuffer( lStartIO, lAddress, lSize, lpsData );

	m_lLastResult = lRet;

	if( m_lLastResult != 0 )
	{
		return FALSE;
	}

	return TRUE;
}

BOOL CMxComp3::WriteBuffer( LONG lStartIO, LONG lAddress, LONG lSize, SHORT* lpsData )
{
	Lock lock(m_csLock);
	if( !m_bOpen || !m_bCreate )
	{
		return FALSE;
	}

	LONG lRet;

	lRet = m_pIEasyIF->WriteBuffer( lStartIO, lAddress, lSize, lpsData );

	m_lLastResult = lRet;

	if( m_lLastResult != 0 )
	{
		return FALSE;
	}

	return TRUE;
}

BOOL CMxComp3::GetClockData( time_t* ptmClock )
{
	Lock lock(m_csLock);
	if( !m_bOpen || !m_bCreate )
	{
		return FALSE;
	}

	ASSERT( ptmClock );

	LONG lRet;

	SHORT sYear, sMonth, sDay, sDayOfWeek, sHour, sMinute, sSecond;
	lRet = m_pIEasyIF->GetClockData( &sYear, &sMonth, &sDay, &sDayOfWeek, &sHour, &sMinute, &sSecond );

	struct tm x;
	x.tm_year = sYear;
	x.tm_mon = sMonth;
	x.tm_mday = sDay;
	x.tm_wday = sDayOfWeek;
	x.tm_hour = sHour;
	x.tm_min = sMinute;
	x.tm_sec = sSecond;

	*ptmClock = mktime( &x );

	m_lLastResult = lRet;

	if( m_lLastResult != 0 )
	{
		return FALSE;
	}

	return TRUE;
}

BOOL CMxComp3::SetClockData( time_t tmClock )
{
	Lock lock(m_csLock);
	if( !m_bOpen || !m_bCreate )
	{
		return FALSE;
	}

	LONG lRet;

	struct tm x;
	localtime_s(&x, &tmClock );

	lRet = m_pIEasyIF->SetClockData( x.tm_year, x.tm_mon, x.tm_mday, 
		x.tm_wday, x.tm_hour, x.tm_min, x.tm_sec );

	m_lLastResult = lRet;

	if( m_lLastResult != 0 )
	{
		return FALSE;
	}

	return TRUE;
}