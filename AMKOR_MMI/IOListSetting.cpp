#include "stdafx.h"
#include "IOListSetting.h"

//////////////////////////////////////////////////////////////////////
// CAlarmListGC
class CIOListGC
{
public:
	CIOListGC() {}
	~CIOListGC() { delete CIOListSetting::m_pThis; CIOListSetting::m_pThis = NULL; }
};
CIOListGC s_GC;

//////////////////////////////////////////////////////////////////////////
// CAlarmList
CIOListSetting* CIOListSetting::m_pThis = NULL;
CIOListSetting::CIOListSetting()
{
	m_stIOInfo.Clear();
}

CIOListSetting::~CIOListSetting()
{
	m_stIOInfo.Clear();
}

CIOListSetting* CIOListSetting::Instance()
{
	if( !m_pThis )
		m_pThis = new CIOListSetting;
	return m_pThis;
}

// UNICODE
CString CIOListSetting::GetExecuteDirectory()
{
	CString strFolderPath;

	// 현재 프로그램의 실행 파일이 있는 폴더명을 추출함
	::GetModuleFileName(NULL, strFolderPath.GetBuffer(MAX_PATH), MAX_PATH);
	strFolderPath.ReleaseBuffer();
	if (strFolderPath.Find('\\') != -1)
	{
		for (int i = strFolderPath.GetLength() - 1; i >= 0; i--) 
		{
			TCHAR ch = strFolderPath[i];
			strFolderPath.Delete(i);
			if (ch == '\\') break; 
		}
	}
	return strFolderPath;
}

CStringA CIOListSetting::GetExecuteDirectoryA()
{
	CStringA strFolderPath;

	// 현재 프로그램의 실행 파일이 있는 폴더명을 추출함
	::GetModuleFileNameA(NULL, strFolderPath.GetBuffer(MAX_PATH), MAX_PATH);
	strFolderPath.ReleaseBuffer();
	if (strFolderPath.Find('\\') != -1)
	{
		for (int i = strFolderPath.GetLength() - 1; i >= 0; i--) 
		{
			CHAR ch = strFolderPath[i];
			strFolderPath.Delete(i);
			if (ch == '\\') break; 
		}
	}
	return strFolderPath;
}

int CIOListSetting::LoadIOList()
{
	LoadIOList_Input();
	LoadIOList_Output();

	return 0;
}

int CIOListSetting::LoadIOList_Input()
{
	CString strPath = GetExecuteDirectory();
	strPath += _T("\\Setting\\Input.ini");

	TCHAR temp[MAX_PATH];
	CString strMain, strSub;

	::GetPrivateProfileString( _T("ADDRESS"), _T("START"), _T("0"), temp, MAX_PATH, strPath );
	m_stIOInfo.m_nStartAddress_Input = _tcstoul( temp, NULL, 16 );
	::GetPrivateProfileString( _T("ADDRESS"), _T("END"), _T("0"), temp, MAX_PATH, strPath );
	m_stIOInfo.m_nEndAddress_Input = _tcstoul( temp, NULL, 16 );
	int nSize = (m_stIOInfo.m_nEndAddress_Input - m_stIOInfo.m_nStartAddress_Input)/16 + 1;
	m_stIOInfo.m_vecInput.resize(nSize);

	for(size_t i=0;i<m_stIOInfo.m_vecInput.size();i++)
	{
		strMain.Format(_T("ADDRESS_%X"),m_stIOInfo.m_nStartAddress_Input+i*16);
		m_stIOInfo.m_vecInput[i].m_nAddress = m_stIOInfo.m_nStartAddress_Input+i*16;
		for(size_t j=0;j<16;j++)
		{
			strSub.Format(_T("NAME%02d"),j+1);
			::GetPrivateProfileString( strMain, strSub, _T(""), temp, MAX_PATH, strPath );
			wcscpy(m_stIOInfo.m_vecInput[i].m_strText[j], temp);
		}
	}

	return 0;
}

int CIOListSetting::LoadIOList_Output()
{
	CString strPath = GetExecuteDirectory();
	strPath += _T("\\Setting\\Output.ini");

	TCHAR temp[MAX_PATH];
	CString strMain, strSub;

	::GetPrivateProfileString( _T("ADDRESS"), _T("START"), _T("0"), temp, MAX_PATH, strPath );
	m_stIOInfo.m_nStartAddress_Output = _tcstoul( temp, NULL, 16 );
	::GetPrivateProfileString( _T("ADDRESS"), _T("END"), _T("0"), temp, MAX_PATH, strPath );
	m_stIOInfo.m_nEndAddress_Output = _tcstoul( temp, NULL, 16 );
	int nSize = (m_stIOInfo.m_nEndAddress_Output - m_stIOInfo.m_nStartAddress_Output)/16 + 1;
	m_stIOInfo.m_vecOutput.resize(nSize);

	for(size_t i=0;i<m_stIOInfo.m_vecOutput.size();i++)
	{
		strMain.Format(_T("ADDRESS_%X"),m_stIOInfo.m_nStartAddress_Output+i*16);
		m_stIOInfo.m_vecOutput[i].m_nAddress = m_stIOInfo.m_nStartAddress_Output+i*16;
		for(size_t j=0;j<16;j++)
		{
			strSub.Format(_T("NAME%02d"),j+1);
			::GetPrivateProfileString( strMain, strSub, _T(""), temp, MAX_PATH, strPath );
			wcscpy(m_stIOInfo.m_vecOutput[i].m_strText[j], temp);
		}
	}

	return 0;
}

BOOL CIOListSetting::SaveIOList_Input()
{
	CString strPath = GetExecuteDirectory();
	strPath += _T("\\Setting\\Input.ini");

	CString strTemp;
	CString strMain, strSub;

	strTemp.Format(_T("%X"), m_stIOInfo.m_nStartAddress_Input);
	::WritePrivateProfileString( _T("ADDRESS"), _T("START"), strTemp, strPath );
	strTemp.Format(_T("%X"), m_stIOInfo.m_nEndAddress_Input);
	::WritePrivateProfileString( _T("ADDRESS"), _T("END"), strTemp, strPath );

	for(size_t i=0;i<m_stIOInfo.m_vecInput.size();i++)
	{
		strMain.Format(_T("ADDRESS_%X"),m_stIOInfo.m_nStartAddress_Input+i*16);
		for(size_t j=0;j<16;j++)
		{
			strSub.Format(_T("NAME%02d"),j+1);
			strTemp.Format(_T("%s"), m_stIOInfo.m_vecInput[i].m_strText[j]);
			::WritePrivateProfileString( strMain, strSub, strTemp, strPath );
		}
	}

	return TRUE;
}

BOOL CIOListSetting::SaveIOList_Output()
{
	CString strPath = GetExecuteDirectory();
	strPath += _T("\\Setting\\Output.ini");

	CString strTemp;
	CString strMain, strSub;

	strTemp.Format(_T("%X"), m_stIOInfo.m_nStartAddress_Output);
	::WritePrivateProfileString( _T("ADDRESS"), _T("START"), strTemp, strPath );
	strTemp.Format(_T("%X"), m_stIOInfo.m_nEndAddress_Output);
	::WritePrivateProfileString( _T("ADDRESS"), _T("END"), strTemp, strPath );

	for(size_t i=0;i<m_stIOInfo.m_vecOutput.size();i++)
	{
		strMain.Format(_T("ADDRESS_%X"),m_stIOInfo.m_nStartAddress_Output+i*16);
		for(size_t j=0;j<16;j++)
		{
			strSub.Format(_T("NAME%02d"),j+1);
			strTemp.Format(_T("%s"), m_stIOInfo.m_vecOutput[i].m_strText[j]);
			::WritePrivateProfileString( strMain, strSub, strTemp, strPath );
		}
	}

	return TRUE;
}

void CIOListSetting::SetIOList_InputText_ByAddress(int nAddress, LPCTSTR strText)
{
	int nIndex = (nAddress - m_stIOInfo.m_nStartAddress_Input) / 16;
	int nBit = (nAddress - m_stIOInfo.m_nStartAddress_Input) % 16;

	if(nIndex < 0 || nIndex >= (int)m_stIOInfo.m_vecInput.size()) return;
	if(nBit >= 16) return;

	wcscpy(m_stIOInfo.m_vecInput[nIndex].m_strText[nBit], strText);
}

void CIOListSetting::SetIOList_OutputText_ByAddress(int nAddress, LPCTSTR strText)
{
	int nIndex = (nAddress - m_stIOInfo.m_nStartAddress_Output) / 16;
	int nBit = (nAddress - m_stIOInfo.m_nStartAddress_Output) % 16;

	if(nIndex < 0 || nIndex >= (int)m_stIOInfo.m_vecOutput.size()) return;
	if(nBit >= 16) return;

	wcscpy(m_stIOInfo.m_vecOutput[nIndex].m_strText[nBit], strText);
}

void CIOListSetting::SetIOList_InputText_ByIndexBit(int nIndex, int nBit, LPCTSTR strText)
{
	if(nIndex < 0 || nIndex >= (int)m_stIOInfo.m_vecInput.size()) return;
	if(nBit >= 16) return;

	wcscpy(m_stIOInfo.m_vecInput[nIndex].m_strText[nBit], strText);
}

void CIOListSetting::SetIOList_OutputText_ByIndexBit(int nIndex, int nBit, LPCTSTR strText)
{
	if(nIndex < 0 || nIndex >= (int)m_stIOInfo.m_vecOutput.size()) return;
	if(nBit >= 16) return;

	wcscpy(m_stIOInfo.m_vecOutput[nIndex].m_strText[nBit], strText);
}
