#pragma once

struct stIOData
{
	int m_nAddress;
	TCHAR m_strText[16][MAX_PATH];
};

struct IO_Info
{
	int m_nStartAddress_Input;
	int m_nEndAddress_Input;
	std::vector<stIOData> m_vecInput;

	int m_nStartAddress_Output;
	int m_nEndAddress_Output;
	std::vector<stIOData> m_vecOutput;

	void Clear()
	{
		m_nStartAddress_Input = 0;
		m_nEndAddress_Input = 0;
		m_vecInput.clear();

		m_nStartAddress_Output = 0;
		m_nEndAddress_Output = 0;
		m_vecOutput.clear();
	}
};

class CIOListGC;
class CIOListSetting
{
public:
	friend class CIOListGC;
	static CIOListSetting* Instance();

	int LoadIOList();
	int LoadIOList_Input();
	int LoadIOList_Output();
	BOOL SaveIOList_Input();
	BOOL SaveIOList_Output();
	void SetIOList_InputText_ByAddress(int nAddress, LPCTSTR strText);
	void SetIOList_OutputText_ByAddress(int nAddress, LPCTSTR strText);
	void SetIOList_InputText_ByIndexBit(int nIndex, int nBit, LPCTSTR strText);
	void SetIOList_OutputText_ByIndexBit(int nIndex, int nBit, LPCTSTR strText);

	IO_Info m_stIOInfo;
	
private:

	static CIOListSetting* m_pThis;
	CIOListSetting();
	~CIOListSetting();

	CString GetExecuteDirectory();
	CStringA GetExecuteDirectoryA();
};

