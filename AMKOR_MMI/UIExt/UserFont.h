// HumanFont.h: interface for the CHumanFont class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_HUMANFONT_H__B53CDAA5_8B4A_46AA_8E19_D56B0106E416__INCLUDED_)
#define AFX_HUMANFONT_H__B53CDAA5_8B4A_46AA_8E19_D56B0106E416__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// 사용자가 간단한 내용만 입력함으로 폰트를 임의로 만들 수 있다.
// 리턴값은 CFont의 포인터형으로 반환한다.
class CUserFont  
{
public:
	CUserFont(CFont* pFont, CString strFontName, int iSize, BOOL bBold=FALSE);
	virtual ~CUserFont();

private:
	LOGFONT		m_LogFont;

public:
	void GetLogFont(LOGFONT* LogFont);

};

#endif // !defined(AFX_HUMANFONT_H__B53CDAA5_8B4A_46AA_8E19_D56B0106E416__INCLUDED_)
