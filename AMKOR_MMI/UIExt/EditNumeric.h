#if !defined(AFX_EDITEX_H__67C6C21E_BC02_48A2_86EF_073F7D3ABAA1__INCLUDED_)
#define AFX_EDITEX_H__67C6C21E_BC02_48A2_86EF_073F7D3ABAA1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// EditNumeric.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CEditNumeric window

class CEditNumeric : public CEdit
{
	DECLARE_DYNAMIC(CEditNumeric)
// Construction
public:
	CEditNumeric();

protected:
	void DrawEditFrame( CDC* pDC );

	CFont	m_fntEdit;
	CBrush	m_BgBrush;

	BOOL	m_bNowEditing;

	COLORREF	m_dwTextColor;
	COLORREF	m_dwActiveTextColor;
	COLORREF	m_dwBorderColor;
	COLORREF	m_dwActiveBorderColor;
	COLORREF	m_dwBgColor;
	BOOL		m_bShowBorder;
	UINT		m_nBorderWidth;

	CString		m_strAllowString;

	double		m_dValueRangeMin;
	double		m_dValueRangeMax;

	double		m_dValue;
	BOOL		m_bFloatStyle;

	BOOL		m_bNotUse;
// Operations
public:
	void SetFloatStyle( BOOL bFloatStyle );
	void ShowBorder( BOOL bShow );
	void SetBorderWidth( UINT nWidth );
	void SetTextColor( COLORREF dwTextColor, COLORREF dwActiveTextColor );
	void SetBorderColor( COLORREF dwBorderColor, COLORREF dwActiveBorderColor );
	void SetFontStyle( LPCTSTR lpszFace, int nFontHeight, int nWeight );
	void SetBgColor( COLORREF dwBgColor );

	void SetValue( double dValue );
	void SetValue( int nValue );
	double GetValueFloat();
	int GetValueInt();

	BOOL EnableWindow(BOOL bNotUse = TRUE, BOOL bEnable = TRUE);

//	void SetAllowString( LPCTSTR lpszAllowString );

	void SetValueRange( double dMin, double dMax );

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CEditNumeric)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void PreSubclassWindow();
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CEditNumeric();

	// Generated message map functions
protected:
	//{{AFX_MSG(CEditNumeric)
	afx_msg HBRUSH CtlColor(CDC* pDC, UINT nCtlColor);
	afx_msg void OnPaint();
	afx_msg void OnKillfocus();
	afx_msg void OnSetfocus();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_EDITEX_H__67C6C21E_BC02_48A2_86EF_073F7D3ABAA1__INCLUDED_)
