#if !defined(AFX_GRAPHSTATIC_H__16F4FCD1_AB89_46F4_8346_51B4742E0F5B__INCLUDED_)
#define AFX_GRAPHSTATIC_H__16F4FCD1_AB89_46F4_8346_51B4742E0F5B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif 

#define GRID_INTERVAL    10
#define MAX_COUNT        1024

enum eGRAPHTYPE{
	GRAPHTYPE_LINE = 0,
	GRAPHTYPE_BAR = 1,
	GRAPHTYPE_MAX = 2,
};

typedef struct{
	COLORREF colGrid;

	int nMaxVal;
	BOOL bGrid;
	CPen gridPen;
	CBrush gridBrush;
	int data[MAX_COUNT];
	int nCount;

	eGRAPHTYPE m_eGraphType;
	int m_nBarWidth;
	
}GRIDXY, *LPGRIDXY;

class CStatsGraph : public CStatic
{
public:
	CStatsGraph();
	virtual ~CStatsGraph();
	
	void Initialize();
	void Clear();
	void Reset_GridList();
	GRIDXY * AddNewGraph(COLORREF col, int nLineWidth, eGRAPHTYPE type, int nBarWidth);
	void AddData(GRIDXY * pGrid, float fValue);
	
	//////////////////////////////////////////////////////////////////////////////////////////
	void AddAreaData(float fMinValue, float fMaxValue, BOOL bSpecWarn, BOOL bEdgeLoop = TRUE);
	void SetOffAreaLine(BOOL bEdgeLoop, BOOL bAreaOff = FALSE);

	void DisplayGrid(CDC &dc);
	void DisplayOutLine(CDC &dc);
	void DisplayXTick(CDC &dc);
	void DisplayYTick(CDC &dc);
	void DisplayAreaLine(CDC &dc, BOOL bEdgeLoop);
	void Display();	

	// Type값을 셋팅 해준다.
	void SetType(int nType){m_nTest_Type = nType;}
	
	void SetGrid(BOOL bGrid);
	void SetGridCol(COLORREF col);
	void SetGridLineCol(GRIDXY * pGrid, COLORREF col);
	void SetGridLine(GRIDXY * pGrid, BOOL bGrid);
	void SetBkCol(COLORREF col);
	void SetViewGrid_Row(BOOL bView) { m_bViewGrid_Row = bView; };
	void SetViewGrid_Col(BOOL bView) { m_bViewGrid_Col = bView; };

	COLORREF GetBkCol() { return m_ColBack; }
	float GetMaxValX() { return m_fMaxValX; }
	float GetMaxValY() { return m_fMaxValY; }
	int GetInterval() { return m_nInterval; }
	int GetType() { return m_nTest_Type;}

	void SetMaxValueX(float nMax, float nInterVal);
	void SetMaxValueY(float nMax, float nInterVal);
	void SetMinValueY(float fMin);
	void SetSampling(int nRate);
	
	//20140321 dhaps2000
	void SetViewOutLine(BOOL bBool) { m_bViewOutLine = bBool; };
	BOOL GetViewOutLine() { return m_bViewOutLine; };
	void SetOutLineColor(COLORREF col) { m_ColOutLine = col; };
	COLORREF GetOutLineColor() { return m_ColOutLine; };

	//{{AFX_VIRTUAL(CGraphStatic)
	protected:
	virtual void PreSubclassWindow();
	//}}AFX_VIRTUAL

protected:
	CDC m_dcGraph;
	CBitmap m_bitmap, *m_pOldBitmap;

	COLORREF m_ColBack;
	COLORREF m_ColGrid;
	COLORREF m_ColGridMain;
	CRect m_Rect, m_TextRectX, m_TextRectY, m_GraphRect;

	int m_nInterval;
	BOOL m_bGrid;
	
	float m_fMaxValX;
	float m_fMaxValY;
	float m_fMinValY;
	int m_nCol;
	int m_nRow;
	float m_fXInterval;
	float m_fYInterval;
	int m_nHeight;
	int m_nWidth;

	CPtrList m_GraphList;

	//20140318 dhaps2000
	BOOL m_bViewGrid_Row;
	BOOL m_bViewGrid_Col;

	// 20160517 giheon TestType 타입대로 변경.
	int m_nTest_Type;


	//20140321 dhaps2000
	BOOL m_bViewOutLine;
	COLORREF m_ColOutLine;

	//20140901 giheon
	//Edge Min, Max Loop Min, MAX Color
	BOOL m_bEdgeSpec; // Edge Spec
	BOOL m_bEdgeWarn;
	BOOL m_bLoopSpec;
	BOOL m_bLoopWarn;
	BOOL m_bEdgeSpecWarn;
	BOOL m_bLoopSpecWarn;
	COLORREF m_crSpecEdge;	// 기본값 CR_DARKRED
	COLORREF m_crWarnEdge;	// 기본값 CR_ORANGE
	COLORREF m_crSpecLoop;
	COLORREF m_crWarnLoop;

	int m_nRealMinSpecEdge;
	int m_nRealMaxSpecEdge;
	int m_nRealMinWarnEdge;
	int m_nRealMaxWarnEdge;
	
	int m_nRealMinSpecLoop;
	int m_nRealMaxSpecLoop;
	int m_nRealMinWarnLoop;
	int m_nRealMaxWarnLoop;


	// 그래프 색깔 및 해당 좌표값을 변경해주는 함수
	int ChkAreaSelLine(int *nSpecMin, int *nSpecMax, int *nWarnMin, int *nWarnMax, BOOL EdgeLoop);

	//{{AFX_MSG(CGraphStatic)
	afx_msg void OnPaint();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};
//{{AFX_INSERT_LOCATION}}
#endif 
