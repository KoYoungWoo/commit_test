#pragma once

#include "xSingletoneObject.h"
#include <gdiplus.h>

namespace UIExt
{

class CResourceManager : public CxSingleton<CResourceManager>
{
protected:
	Gdiplus::FontFamily*	m_pFontFamilySystem;

public:
	Gdiplus::FontFamily* GetSystemFontFamily();
	static Gdiplus::FontFamily* SystemFontFamily();
public:
	CResourceManager(void);
	virtual ~CResourceManager(void);
};

} // namespace UIExt