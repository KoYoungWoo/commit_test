// EditEx.cpp : implementation file
//

#include "stdafx.h"
#include "EditNumeric.h"

#include <float.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CEditNumeric
IMPLEMENT_DYNAMIC( CEditNumeric, CEdit )
CEditNumeric::CEditNumeric() : m_dwBorderColor( RGB(255, 255, 0) ), m_dwActiveBorderColor( RGB(255, 0, 0) ),
					 m_dwTextColor( RGB(255, 255, 0) ), m_dwActiveTextColor( RGB(255, 0, 0) ),
					 m_dwBgColor( RGB(0, 0, 0) )
{
	m_BgBrush.CreateSolidBrush( RGB(0,0,0) );
	m_bShowBorder = FALSE;
	m_nBorderWidth = 2;
	m_bNowEditing = FALSE;
	m_dValueRangeMin = -DBL_MAX;
	m_dValueRangeMax = +DBL_MAX;
	m_strAllowString = _T("0123456789.-");
	m_bFloatStyle = TRUE;
	m_bNotUse = FALSE;
}

CEditNumeric::~CEditNumeric()
{
}


BEGIN_MESSAGE_MAP(CEditNumeric, CEdit)
	//{{AFX_MSG_MAP(CEditNumeric)
	ON_WM_CTLCOLOR_REFLECT()
	ON_WM_PAINT()
	ON_CONTROL_REFLECT(EN_KILLFOCUS, OnKillfocus)
	ON_CONTROL_REFLECT(EN_SETFOCUS, OnSetfocus)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CEditNumeric message handlers

HBRUSH CEditNumeric::CtlColor(CDC* pDC, UINT nCtlColor) 
{
	DrawEditFrame( pDC );
	
	return m_BgBrush;
}

BOOL CEditNumeric::EnableWindow(BOOL bNotUse, BOOL bEnable)
{
	m_bNotUse = bNotUse;
	BOOL bRet = CEdit::EnableWindow( bEnable );
	Invalidate();
	return bRet;
}

void CEditNumeric::SetFloatStyle( BOOL bFloatStyle )
{
	m_bFloatStyle = bFloatStyle;
	if( bFloatStyle )
		m_strAllowString = _T("0123456789.-");
	else
		m_strAllowString = _T("0123456789-");
}

void CEditNumeric::DrawEditFrame( CDC* pDC )
{	
	CRect rcItem;
	GetClientRect( &rcItem );

	if( m_bNowEditing )
		pDC->SetTextColor( m_dwActiveTextColor );
	else
		pDC->SetTextColor( m_dwTextColor );
	//pDC->SetBkMode( TRANSPARENT );
	pDC->SetBkColor( m_dwBgColor );

	if( m_bShowBorder )
	{
		rcItem.InflateRect( 1, 1, 1, 1 );//1, 1, 1, 1 );
		//pDC->Draw3dRect( rcItem, RGB(255, 255, 0), RGB(255, 255, 0) );//m_dwBorderColor );
		CBrush* pOldBrush = (CBrush*)pDC->SelectStockObject( NULL_BRUSH );
		CPen BorderPen;
		if( m_bNowEditing )
			BorderPen.CreatePen( PS_SOLID, m_nBorderWidth, m_dwActiveBorderColor );
		else
			BorderPen.CreatePen( PS_SOLID, m_nBorderWidth, m_dwBorderColor );
		CPen* pOldPen = pDC->SelectObject( &BorderPen );

		pDC->Rectangle( rcItem );

		pDC->SelectObject( pOldPen );
		pDC->SelectObject( pOldBrush );
	}
	//pDC->FillSolidRect( rcItem, RGB(0,0,0) );
}

void CEditNumeric::SetValue( int nValue )
{
	if( (double)nValue < m_dValueRangeMin || (double)nValue > m_dValueRangeMax )
		return;
	m_dValue = (double)nValue;
	CString strValue;
	strValue.Format( _T("%04d"), nValue );
	SetWindowText( strValue );
}

void CEditNumeric::SetValue( double dValue )
{
	if( dValue < m_dValueRangeMin || dValue > m_dValueRangeMax )
		return;
	m_dValue = dValue;
	CString strValue;
	strValue.Format( _T("%2.3f"), dValue );
	SetWindowText( strValue );
}

double CEditNumeric::GetValueFloat()
{
	CString strValue;
	GetWindowText( strValue );
	if( strValue.IsEmpty() )
	{
		m_dValue = 0.0;
		SetWindowText( _T("0.000") );
		strValue = _T("0.000");
	}
	
	m_dValue = _tcstod( strValue, NULL );
	if( m_dValue < m_dValueRangeMin || m_dValue > m_dValueRangeMax )
	{
		m_dValue = 0.0;
	}

	return m_dValue;
}

int CEditNumeric::GetValueInt()
{
	CString strValue;
	GetWindowText( strValue );
	if( strValue.IsEmpty() )
	{
		m_dValue = 0.0;
		SetWindowText( _T("0000") );
		strValue = _T("0000");
	}
	
	m_dValue = (double)_tcstol( strValue, NULL, 10 );
	if( m_dValue < m_dValueRangeMin || m_dValue > m_dValueRangeMax )
	{
		m_dValue = 0.0;
	}

	return (int)m_dValue;
}

void CEditNumeric::SetBorderWidth( UINT nWidth )
{
	m_nBorderWidth = nWidth;
	RedrawWindow();
}

void CEditNumeric::ShowBorder( BOOL bShow )
{
	m_bShowBorder = bShow;
	RedrawWindow();
}

void CEditNumeric::SetTextColor( COLORREF dwTextColor, COLORREF dwActiveTextColor )
{
	m_dwTextColor = dwTextColor;
	m_dwActiveTextColor = dwActiveTextColor;
	RedrawWindow();
}

void CEditNumeric::SetBorderColor( COLORREF dwBorderColor, COLORREF dwActiveBorderColor )
{
	m_dwBorderColor = dwBorderColor;
	m_dwActiveBorderColor = dwActiveBorderColor;
	RedrawWindow();
}

void CEditNumeric::SetFontStyle( LPCTSTR lpszFace, int nFontHeight, int nWeight )
{
	if( m_fntEdit.GetSafeHandle() )
		m_fntEdit.DeleteObject();

	LOGFONT  lgFont;

	CRect rcClient;
	GetClientRect( rcClient );

	int nCHeight = rcClient.Height();
	if( nCHeight <= 0 ) nCHeight = 10;

	lgFont.lfCharSet = DEFAULT_CHARSET;
	lgFont.lfClipPrecision = 0;
	lgFont.lfEscapement = 0;
	strcpy( lgFont.lfFaceName, lpszFace );
	lgFont.lfHeight = nFontHeight<0 ? nCHeight : nFontHeight;
	lgFont.lfItalic = FALSE;
	lgFont.lfOrientation = 0;
	lgFont.lfOutPrecision = 0;
	lgFont.lfPitchAndFamily = 2;
	lgFont.lfQuality = 0;
	lgFont.lfStrikeOut = 0;
	lgFont.lfUnderline = FALSE;
	lgFont.lfWidth = 0;
	lgFont.lfWeight = nWeight;

	m_fntEdit.CreateFontIndirect( &lgFont );

	SetFont( &m_fntEdit );
}

void CEditNumeric::SetBgColor( COLORREF dwBgColor )
{
	m_dwBgColor = dwBgColor;
	if( m_BgBrush.GetSafeHandle() )
		m_BgBrush.DeleteObject();

	m_BgBrush.CreateSolidBrush( dwBgColor );
	RedrawWindow();
}

void CEditNumeric::PreSubclassWindow() 
{
	CEdit::PreSubclassWindow();
}

void CEditNumeric::OnPaint() 
{
	//CPaintDC dc(this); // device context for painting

	if( FALSE )//if( IsWindowEnabled() )
	{
		CEdit::OnPaint();
	}
	else
	{
		CEdit::OnPaint();

		if( m_bNotUse )
		{
			CClientDC dc(this);

			CRect rcClient;
			GetClientRect( rcClient );
			int nW = rcClient.Width() > rcClient.Height() ? rcClient.Height() : rcClient.Width();
			CPoint ptC = rcClient.CenterPoint();
			int nOff = nW/2;

			CPen RedPen;
			RedPen.CreatePen( PS_SOLID, 4, RGB(255, 0, 0) );
			CPen* pOldPen = dc.SelectObject( &RedPen );
			dc.MoveTo( ptC.x-nOff, ptC.y-nOff );
			dc.LineTo( ptC.x+nOff, ptC.y+nOff );
			dc.MoveTo( ptC.x-nOff, ptC.y+nOff );
			dc.LineTo( ptC.x+nOff, ptC.y-nOff );

			dc.SelectObject( pOldPen );
		}
	}
		
	// Do not call CEdit::OnPaint() for painting messages
}

void CEditNumeric::OnKillfocus() 
{
	CString strValue;
	GetWindowText( strValue );
	if( strValue.IsEmpty() )
	{
		m_dValue = 0.0;
		if( m_bFloatStyle )
		{
			SetWindowText( _T("0.000") );
			strValue = _T("0.000");
		}
		else
		{
			SetWindowText( _T("0000") );
			strValue = _T("0000");
		}
	}
	
	m_dValue = _tcstod( strValue, NULL );
	if( m_dValue < m_dValueRangeMin || m_dValue > m_dValueRangeMax )
	{
		CString strMessage;
		if( m_bFloatStyle )
			strMessage.Format( _T("데이터 입력 범위를 초과하였습니다. 범위[%.3f~%.3f]"), m_dValueRangeMin, m_dValueRangeMax );
		else
			strMessage.Format( _T("데이터 입력 범위를 초과하였습니다. 범위[%d~%d]"), (int)m_dValueRangeMin, (int)m_dValueRangeMax );
		::MessageBox( NULL, strMessage, _T("입력 에러"), MB_OK|MB_ICONSTOP );
		//AfxMessageBox( strMessage, MB_OK|MB_ICONSTOP );
		m_dValue = 0.0;
		if( m_bFloatStyle )
			SetWindowText( _T("0.000") );
		else
			SetWindowText( _T("0000") );
		SetSel(0,-1,TRUE);
		SetFocus();
		return ;
	}

	if( m_bFloatStyle )
		strValue.Format( _T("%2.3f"), m_dValue );
	else
		strValue.Format( _T("%04d"), (int)m_dValue );
	SetWindowText( strValue );

	m_bNowEditing = FALSE;
	RedrawWindow();
}

void CEditNumeric::OnSetfocus() 
{
	m_bNowEditing = TRUE;
	RedrawWindow();
}

void CEditNumeric::SetValueRange( double dMin, double dMax )
{
	m_dValueRangeMin = dMin;
	m_dValueRangeMax = dMax;
}

BOOL CEditNumeric::PreTranslateMessage(MSG* pMsg) 
{
	if( pMsg->message == WM_CHAR )
	{
		if( !m_strAllowString.IsEmpty() )
		{
			TCHAR chCharCode = (TCHAR)pMsg->wParam;
			if( chCharCode == VK_BACK )
				return CEdit::PreTranslateMessage(pMsg);
			if( m_strAllowString.Find( chCharCode ) < 0 )
			{
				return TRUE;
			}
		}
	}
	
	return CEdit::PreTranslateMessage(pMsg);
}
