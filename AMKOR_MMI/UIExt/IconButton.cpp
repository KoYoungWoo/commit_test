#include "stdafx.h"
#include "IconButton.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CIconButton::CIconButton()
{
	m_fnt.FromHandle ( (HFONT)GetStockObject ( SYSTEM_FONT ) );
	memset ( &m_TextMetrics, 0, sizeof ( TEXTMETRIC ) );
	//
	m_nIconPos			= 0;
	m_bIconRight		= false;
	m_nIconID			= 0;									
	m_nIcon.cx			= ::GetSystemMetrics ( SM_CXICON );		
	m_nIcon.cy			= ::GetSystemMetrics ( SM_CYICON );
	m_nColor			= ::GetSysColor ( COLOR_BTNTEXT );		// 텍스트 칼라
	m_nBtnColor_BK		= ::GetSysColor ( COLOR_BTNFACE );		// 버튼 노말 배경색
	m_nBtnColor_SEL		= ::GetSysColor ( COLOR_BTNFACE );		// 버튼 선택 배경색
	//
	m_sTip = "";												
	m_bTipIsOpen = false;										
	m_bWithTip = false;											
	//
	m_bDodgeImage = false;
	// CHECKBOX STYLE
	m_bOnOffStyle = false;
	// LAMP ATTRIBUTES
	m_bLampOnOff	  = FALSE;
	m_bLampBlink	  = FALSE;
	//20140317 dhaps2000 - Flat Type
	m_bFlatType		= FALSE;
	//
	m_hIcon = NULL;
	//
	m_bButtonPressed = FALSE;
	m_bUseBeep = FALSE;
}

CIconButton::~CIconButton()
{
	if(m_hIcon != NULL) DestroyIcon(m_hIcon);

	m_fnt.DeleteObject(); 
}

BEGIN_MESSAGE_MAP(CIconButton, CButton)
	//{{AFX_MSG_MAP(CIconButton)
	ON_WM_NCHITTEST()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_MOUSEMOVE()
	ON_WM_TIMER()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CIconButton::SetIconID ( const UINT nID )
{ 
	m_nIconID = nID;

	if(m_hIcon != NULL) DestroyIcon(m_hIcon);

	HRESULT hr = LoadIconWithScaleDown( AfxGetInstanceHandle(), MAKEINTRESOURCE(m_nIconID),
                                    m_nIcon.cx, m_nIcon.cy, &m_hIcon );
}
void CIconButton::SetIconSize ( const int x, const int y )
{
	m_nIcon.cx = x;
	m_nIcon.cy = y;

	if(m_hIcon != NULL) DestroyIcon(m_hIcon);

	HRESULT hr = LoadIconWithScaleDown( AfxGetInstanceHandle(), MAKEINTRESOURCE(m_nIconID),
                                    m_nIcon.cx, m_nIcon.cy, &m_hIcon );
}

void CIconButton::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct) 
{
	if(m_bLampOnOff)
		DrawItem_Lamp(lpDrawItemStruct);
	else if(m_bOnOffStyle)
		DrawItem_OnOff(lpDrawItemStruct);
	else
		DrawItem_Normal(lpDrawItemStruct);
}

void CIconButton::DrawItem_Normal(LPDRAWITEMSTRUCT lpDrawItemStruct)
{
	CRect		rect		= lpDrawItemStruct->rcItem;
	CRect		rcItem		= lpDrawItemStruct->rcItem;
	CRect		rectCaption = lpDrawItemStruct->rcItem;
	CDC			*pDC		= CDC::FromHandle ( lpDrawItemStruct->hDC );
	UINT		state		= lpDrawItemStruct->itemState;
	UINT		uStyle		= DFCS_BUTTONPUSH;
	bool		bWithStr	= true;
	//HICON		hIcon		= NULL;
	int			nHotkeyPos	= 0;
	CString		strText;
	CPoint		TextPos;
	CPoint		RecPos;
	CSize		TextExtent;
	CPen		HighlightPen	( PS_SOLID | PS_COSMETIC, 1, ::GetSysColor ( COLOR_3DHIGHLIGHT ) );
	CPen		DarkShadowPen	( PS_SOLID | PS_COSMETIC, 1, ::GetSysColor ( COLOR_3DDKSHADOW ) );
	CFont		*pOldFont = NULL;
	//
	ASSERT ( lpDrawItemStruct->CtlType == ODT_BUTTON );

	//그리기전에 팁 없애준다..
	//if ( state & ODS_SELECTED ) // 이걸 주석치니 편해짐...
	{
		if ( ( m_bWithTip ) && ( m_bTipIsOpen ) )
		{
			HideTip ();
		}
	}
	//
	if ( lpDrawItemStruct->itemState & ODS_SELECTED )
	  uStyle |= DFCS_PUSHED;
	
	//버튼 프레임 그리자
	::DrawFrameControl ( lpDrawItemStruct->hDC, &lpDrawItemStruct->rcItem, DFC_BUTTON, uStyle );
	//
	GetWindowText ( strText );
	bWithStr = ! strText.IsEmpty ();

	//20131218 dhaps2000 - 멀티라인추가
	DWORD dwStyle = GetStyle();
	CIconButton::LB_VERT_ALIGN vAlign = GetVertAlign( dwStyle );
	CIconButton::LB_HORZ_ALIGN hAlign = GetHorzAlign( dwStyle );
	BOOL bIsMultiline = IsMultiLine( dwStyle );
	UINT uTextFormat = DT_WORD_ELLIPSIS;
	switch( hAlign )
	{
	case ALIGN_LEFT:	uTextFormat |= DT_LEFT; break;
	case ALIGN_RIGHT:	uTextFormat |= DT_RIGHT; break;
	case ALIGN_CENTER:	uTextFormat |= DT_CENTER; break;
	}
	switch( vAlign )
	{
	case ALIGN_TOP:		uTextFormat |= DT_TOP; break;
	case ALIGN_BOTTOM:	uTextFormat |= DT_BOTTOM; break;
	case ALIGN_VCENTER:	uTextFormat |= DT_VCENTER; break;
	}
	if( !bIsMultiline )	uTextFormat |= DT_SINGLELINE;
	else uTextFormat |= DT_WORDBREAK;

	if(m_bDodgeImage) bWithStr = false; // 풀이미지 사용시는 텍스트 안뿌리게...

	// 버튼 배경색 처리.....
	if(!m_bDodgeImage)
	{
		// 선택되었을 시 처리
		if ( state & ODS_SELECTED ) 
		{
			CBrush hbrush(m_nBtnColor_SEL);
			pDC->FillRect(rect, &hbrush);
		}
		// 비활성시 처리
		else if ( state & ODS_DISABLED ) 
		{
			pDC->SelectStockObject ( NULL_BRUSH );
			pDC->FillSolidRect ( rect, ::GetSysColor ( COLOR_BTNFACE ) );
		}
		// 비선택시..
		else
		{
			CBrush hbrush(m_nBtnColor_BK);
			pDC->FillRect(rect, &hbrush);
		}
	}
	else // 풀 이미지라면 배경색 비활성배경으로 바꾼다..
	{
		pDC->SelectStockObject ( NULL_BRUSH );
		pDC->FillSolidRect ( rect, ::GetSysColor ( COLOR_BTNFACE ) );
	}
	
	// 텍스트가 있다면...
	if ( bWithStr )
	{
		nHotkeyPos = DelAmpersand ( strText );
		pOldFont = pDC->SelectObject ( &m_fnt );
		//TextExtent = pDC->GetTextExtent ( strText );
		TextExtent.cy = pDC->DrawText ( strText, strText.GetLength(), &rectCaption, uTextFormat | DT_CALCRECT);   
		TextExtent.cx = rectCaption.Width();
		if( bIsMultiline && vAlign == ALIGN_VCENTER )
		{
			int nOffsetY = (rcItem.Height() - TextExtent.cy)/2;   
			rcItem.top += nOffsetY;   
			rcItem.bottom = rcItem.top + TextExtent.cy;
		}

		pDC->SetBkMode ( TRANSPARENT );
		pDC->SetTextColor ( m_nColor );
		
		if ( m_nIconID ) // 아이콘이 있을때의 텍스트 포지션 결정
		{
			if ( m_nIconPos == 0 )
			{
				//TextPos = CPoint (	(int)( ( rect.right - m_nIcon.cx ) / 2 - TextExtent.cx / 2 ) + m_nIcon.cx,
				//					(int)( rect.bottom/2 - TextExtent.cy / 2 ) );

				rcItem.left = (int)( ( rect.right - m_nIcon.cx ) / 2 - TextExtent.cx / 2 ) + m_nIcon.cx;
				rcItem.right = rcItem.left + TextExtent.cx;
				TextPos = CPoint(rcItem.left, rcItem.top);
			}
			else if ( m_nIconPos == 1 )
			{
				//TextPos = CPoint (	(int)( ( rect.right - m_nIcon.cx ) / 2 - TextExtent.cx / 2 ),
				//					(int)( rect.bottom/2 - TextExtent.cy / 2 ) );

				rcItem.left = (int)( ( rect.right - m_nIcon.cx ) / 2 - TextExtent.cx / 2 );
				rcItem.right = rcItem.left + TextExtent.cx;

				TextPos = CPoint(rcItem.left, rcItem.top);
			}
		}
		else // 아이콘이 없을시 텍스트가 중앙에 온다
		{
			//TextPos = CPoint (	(int)(rect.right/2 - TextExtent.cx/2),
			//					(int)(rect.bottom/2 - TextExtent.cy/2) );
			TextExtent = pDC->GetTextExtent ( strText );

			rcItem.left = (int)(rect.right/2 - TextExtent.cx/2);
			rcItem.right = rcItem.left + TextExtent.cx;
			//rcItem.top += (int)(rect.bottom/2 - TextExtent.cy/2);
			//rcItem.bottom = rcItem.top + TextExtent.cy;

			TextPos = CPoint(rcItem.left, rcItem.top);
		}
		
		// 아이콘 위치 설정
		if ( m_nIconPos == 0 )
			RecPos = CPoint ( 6, (rect.Height () - m_nIcon.cy + 1 ) / 2 );
		else if ( m_nIconPos == 1 )
			RecPos = CPoint ( (rect.right - m_nIcon.cx - 6 ), (rect.Height () - m_nIcon.cy + 1 ) / 2 );
		else if ( m_nIconPos == 2 )
		{
			/*if(rect.Height () - m_TextMetrics.tmHeight - 6 > 0) 
			{
				m_nIcon.cx = rect.Height () - m_TextMetrics.tmHeight - 6;
				m_nIcon.cy = rect.Height () - m_TextMetrics.tmHeight - 6;
			}*/
			RecPos = CPoint ( (rect.right - m_nIcon.cx ) / 2, 6 );
		}
	}
	else // 텍스트가 없을시 아이콘을 중앙에 오게 한다..
	{
		if(m_bDodgeImage) // 이미지 사이즈를 버튼 크기에 맞춘다..
		{
			m_nIcon.cx = rect.Width()-16;
			m_nIcon.cy = rect.Height()-16;
		}
		
		RecPos = CPoint (	(int)( ( rect.right - m_nIcon.cx ) / 2 ),
							(int)( ( rect.bottom - m_nIcon.cy ) / 2 ) );
	}
	// 아이콘 이미지를 불러오자..
	//if ( m_nIconID != NULL )
		//hIcon = AfxGetApp()->LoadIcon ( MAKEINTRESOURCE ( m_nIconID ) );
		//hIcon = AfxGetApp()->LoadIconWithScaleDowm ( MAKEINTRESOURCE ( m_nIconID ) );
	//HRESULT hr = LoadIconWithScaleDown( AfxGetInstanceHandle(), MAKEINTRESOURCE(m_nIconID),
    //                                m_nIcon.cx, m_nIcon.cy, &hIcon );

	
	// 선택되었을시 아이콘과 텍스트 위치를 살짝 옮기자 (눌러지는 효과)
	if ( state & ODS_SELECTED ) 
	{
		rcItem.OffsetRect( 1, 1 );
		TextPos.Offset ( 1, 1 );
		RecPos.Offset ( 1, 1);
	}
	// 비활성시 처리
	if ( state & ODS_DISABLED ) 
	{
		pDC->SetTextColor ( RGB(200,200,200) );
		CSize  size ( m_nIcon.cx, m_nIcon.cy );
		if ( bWithStr ) 
		{
			/*CPoint ptTemp = TextPos;
			ptTemp.y += m_nIcon.cy;
			pDC->DrawState ( TextPos, TextExtent, strText, DSS_DISABLED, TRUE, 0, (HBRUSH)NULL);*/

			pDC->DrawText( strText, strText.GetLength(), &rcItem, uTextFormat );
		}
		if ( m_hIcon != NULL )
		{
			/*HICON	hHelper = (HICON)CopyImage ( hIcon, IMAGE_ICON, m_nIcon.cx, m_nIcon.cy, LR_MONOCHROME );
			pDC->DrawState ( RecPos, size, hHelper, DSS_DISABLED, (HBRUSH)NULL );
			DestroyIcon ( (HICON)hHelper );*/

			DrawIconEx ( pDC->GetSafeHdc (), rect.left + RecPos.x, rect.top + RecPos.y, 
							m_hIcon, (m_nIcon.cx) ? m_nIcon.cx : 32, (m_nIcon.cy) ? m_nIcon.cy : 32, 
							0, NULL, DI_NORMAL );
		}
	}
	else 
	{
		pDC->SetTextColor ( m_nColor );
		if ( bWithStr )	
		{
			//pDC->TextOut ( TextPos.x, TextPos.y, strText );
			pDC->DrawText( strText, strText.GetLength(), &rcItem, uTextFormat );
		}
		if ( m_hIcon != NULL )
		{
			DrawIconEx ( pDC->GetSafeHdc (), rect.left + RecPos.x, rect.top + RecPos.y, 
							m_hIcon, (m_nIcon.cx) ? m_nIcon.cx : 32, (m_nIcon.cy) ? m_nIcon.cy : 32, 
							0, NULL, DI_NORMAL );
		}
	}
	
	// 포커스 처리
	/*if(!m_bDodgeImage)
	{
		if ( state & ODS_FOCUS ) 
		{
			CBrush brush;
			brush.CreateSysColorBrush ( COLOR_3DDKSHADOW );
			pDC->FrameRect ( rect, &brush );
			rect.DeflateRect ( 4, 4, 4, 4 );
			pDC->DrawFocusRect ( rect );
		}
	}*/

	// 버튼 외곽선 그리자
	if(m_bFlatType == FALSE)
	{
		if ( lpDrawItemStruct->itemState & ODS_SELECTED )
		{	
			CPen *pOldPen = pDC->SelectObject ( &DarkShadowPen );
			pDC->MoveTo ( CPoint ( lpDrawItemStruct->rcItem.left, lpDrawItemStruct->rcItem.bottom-1 ) );
			pDC->LineTo ( CPoint ( lpDrawItemStruct->rcItem.left, lpDrawItemStruct->rcItem.top ) );
			pDC->LineTo ( CPoint ( lpDrawItemStruct->rcItem.right-1, lpDrawItemStruct->rcItem.top ) );
			pDC->SelectObject ( &HighlightPen );
			pDC->LineTo ( CPoint ( lpDrawItemStruct->rcItem.right-1, lpDrawItemStruct->rcItem.bottom-1 ) );
			pDC->LineTo ( CPoint ( lpDrawItemStruct->rcItem.left, lpDrawItemStruct->rcItem.bottom-1 ) );
			pDC->SelectObject ( pOldPen );
		}
		else
		{
			CPen *pOldPen = pDC->SelectObject ( &HighlightPen );
			pDC->MoveTo ( CPoint ( lpDrawItemStruct->rcItem.left, lpDrawItemStruct->rcItem.bottom-1 ) );
			pDC->LineTo ( CPoint ( lpDrawItemStruct->rcItem.left, lpDrawItemStruct->rcItem.top ) );
			pDC->LineTo ( CPoint ( lpDrawItemStruct->rcItem.right-1, lpDrawItemStruct->rcItem.top ) );
			pDC->SelectObject ( &DarkShadowPen );
			pDC->LineTo ( CPoint ( lpDrawItemStruct->rcItem.right-1, lpDrawItemStruct->rcItem.bottom-1 ) );
			pDC->LineTo ( CPoint ( lpDrawItemStruct->rcItem.left, lpDrawItemStruct->rcItem.bottom-1 ) );
			pDC->SelectObject ( pOldPen );
		}
	}
	else
	{
		CPen *pOldPen = pDC->SelectObject ( &DarkShadowPen );
		pDC->MoveTo ( CPoint ( lpDrawItemStruct->rcItem.left, lpDrawItemStruct->rcItem.bottom-1 ) );
		pDC->LineTo ( CPoint ( lpDrawItemStruct->rcItem.left, lpDrawItemStruct->rcItem.top ) );
		pDC->LineTo ( CPoint ( lpDrawItemStruct->rcItem.right-1, lpDrawItemStruct->rcItem.top ) );
		pDC->LineTo ( CPoint ( lpDrawItemStruct->rcItem.right-1, lpDrawItemStruct->rcItem.bottom-1 ) );
		pDC->LineTo ( CPoint ( lpDrawItemStruct->rcItem.left, lpDrawItemStruct->rcItem.bottom-1 ) );
		pDC->SelectObject ( pOldPen );
	}

	// 핫키 마크 그리자
	if ( ( nHotkeyPos != -1 ) && ( bWithStr ) )
	{
		CString sHlp = strText.Mid ( nHotkeyPos, 1 );
		CSize nWidth = pDC->GetTextExtent ( sHlp );
		sHlp = strText.Left ( nHotkeyPos );
		CSize nStart = pDC->GetTextExtent ( sHlp );
		//
		if ( state & ODS_DISABLED ) {}
		else
		{
			CPen HotkeyPen ( PS_SOLID | PS_COSMETIC, 1, m_nColor );
			CPen *pOldPen = pDC->SelectObject ( &HotkeyPen );
			pDC->MoveTo ( TextPos.x + nStart.cx, TextPos.y + TextExtent.cy  );
			pDC->LineTo ( TextPos.x + nStart.cx + nWidth.cx, TextPos.y + TextExtent.cy  );
			pDC->SelectObject ( pOldPen );
		}
	}

	if(pOldFont) pDC->SelectObject ( pOldFont );
}

void CIconButton::DrawItem_OnOff(LPDRAWITEMSTRUCT lpDrawItemStruct)
{
	CRect		rect		= lpDrawItemStruct->rcItem;
	CRect		rcItem		= lpDrawItemStruct->rcItem;
	CRect		rectCaption = lpDrawItemStruct->rcItem;
	CDC			*pDC		= CDC::FromHandle ( lpDrawItemStruct->hDC );
	UINT		state		= lpDrawItemStruct->itemState;
	UINT		uStyle		= DFCS_BUTTONPUSH;
	bool		bWithStr	= true;
	//HICON		hIcon		= NULL;
	int			nHotkeyPos	= 0;
	CString		strText;
	CPoint		TextPos;
	CPoint		RecPos;
	CSize		TextExtent;
	CPen		HighlightPen	( PS_SOLID | PS_COSMETIC, 1, ::GetSysColor ( COLOR_3DHIGHLIGHT ) );
	CPen		DarkShadowPen	( PS_SOLID | PS_COSMETIC, 1, ::GetSysColor ( COLOR_3DDKSHADOW ) );
	CFont		*pOldFont = NULL;
	//
	ASSERT ( lpDrawItemStruct->CtlType == ODT_BUTTON );
	//그리기전에 팁 없애준다..
	//if ( state & ODS_SELECTED ) 
	{
		if ( ( m_bWithTip ) && ( m_bTipIsOpen ) )
		{
			HideTip ();
		}
	}
	//
	if ( lpDrawItemStruct->itemState & ODS_SELECTED )
	  uStyle |= DFCS_PUSHED;
	//버튼 프레임 그리자
	::DrawFrameControl ( lpDrawItemStruct->hDC, &lpDrawItemStruct->rcItem, DFC_BUTTON, uStyle );
	//
	GetWindowText ( strText );
	bWithStr = ! strText.IsEmpty ();

	//20131218 dhaps2000 - 멀티라인추가
	DWORD dwStyle = GetStyle();
	CIconButton::LB_VERT_ALIGN vAlign = GetVertAlign( dwStyle );
	CIconButton::LB_HORZ_ALIGN hAlign = GetHorzAlign( dwStyle );
	BOOL bIsMultiline = IsMultiLine( dwStyle );
	UINT uTextFormat = DT_WORD_ELLIPSIS;
	switch( hAlign )
	{
	case ALIGN_LEFT:	uTextFormat |= DT_LEFT; break;
	case ALIGN_RIGHT:	uTextFormat |= DT_RIGHT; break;
	case ALIGN_CENTER:	uTextFormat |= DT_CENTER; break;
	}
	switch( vAlign )
	{
	case ALIGN_TOP:		uTextFormat |= DT_TOP; break;
	case ALIGN_BOTTOM:	uTextFormat |= DT_BOTTOM; break;
	case ALIGN_VCENTER:	uTextFormat |= DT_VCENTER; break;
	}
	if( !bIsMultiline )	uTextFormat |= DT_SINGLELINE;
	else uTextFormat |= DT_WORDBREAK;

	if(m_bDodgeImage) bWithStr = false; // 풀이미지 사용시는 텍스트 안뿌리게...

	// 버튼 배경색 처리.....
	if(!m_bDodgeImage)
	{
		if ( state & ODS_DISABLED ) 
		{
			pDC->SelectStockObject ( NULL_BRUSH );
			pDC->FillSolidRect ( rect, ::GetSysColor ( COLOR_BTNFACE ) );
		}
		else
		{
			if ( m_bOnOff ) 
			{
				CBrush hbrush(m_nBtnColor_SEL);
				pDC->FillRect(rect, &hbrush);
			}
			else
			{
				CBrush hbrush(m_nBtnColor_BK);
				pDC->FillRect(rect, &hbrush);
			}
		}
	}
	else
	{
		pDC->SelectStockObject ( NULL_BRUSH );
		pDC->FillSolidRect ( rect, ::GetSysColor ( COLOR_BTNFACE ) );
	}
	
	// 텍스트가 있다면...
	if ( bWithStr )
	{
		nHotkeyPos = DelAmpersand ( strText );
		pOldFont = pDC->SelectObject ( &m_fnt );
		//TextExtent = pDC->GetTextExtent ( strText );
		TextExtent.cy = pDC->DrawText ( strText, strText.GetLength(), &rectCaption, uTextFormat | DT_CALCRECT);   
		TextExtent.cx = rectCaption.Width();
		if( bIsMultiline && vAlign == ALIGN_VCENTER )
		{
			int nOffsetY = (rcItem.Height() - TextExtent.cy)/2;   
			rcItem.top += nOffsetY;   
			rcItem.bottom = rcItem.top + TextExtent.cy;
		}

		pDC->SetBkMode ( TRANSPARENT );
		pDC->SetTextColor ( m_nColor );

		if ( m_nIconID ) // 아이콘이 있을때의 텍스트 포지션 결정
		{
			if ( m_nIconPos == 0 )
			{
				//TextPos = CPoint (	(int)( ( rect.right - m_nIcon.cx ) / 2 - TextExtent.cx / 2 ) + m_nIcon.cx,
				//					(int)( rect.bottom/2 - TextExtent.cy / 2 ) );

				rcItem.left = (int)( ( rect.right - m_nIcon.cx ) / 2 - TextExtent.cx / 2 ) + m_nIcon.cx;
				rcItem.right = rcItem.left + TextExtent.cx;

				TextPos = CPoint(rcItem.left, rcItem.top);
			}
			else if ( m_nIconPos == 1 )
			{
				//TextPos = CPoint (	(int)( ( rect.right - m_nIcon.cx ) / 2 - TextExtent.cx / 2 ),
				//					(int)( rect.bottom/2 - TextExtent.cy / 2 ) );

				rcItem.left = (int)( ( rect.right - m_nIcon.cx ) / 2 - TextExtent.cx / 2 );
				rcItem.right = rcItem.left + TextExtent.cx;

				TextPos = CPoint(rcItem.left, rcItem.top);
			}
		}
		else // 아이콘이 없을시 텍스트가 중앙에 온다
		{
			//TextPos = CPoint (	(int)(rect.right/2 - TextExtent.cx/2),
			//					(int)(rect.bottom/2 - TextExtent.cy/2) );

			rcItem.left = (int)(rect.right/2 - TextExtent.cx/2);
			rcItem.right = rcItem.left + TextExtent.cx;
			//rcItem.top += (int)(rect.bottom/2 - TextExtent.cy/2);
			//rcItem.bottom = rcItem.top + TextExtent.cy;

			TextPos = CPoint(rcItem.left, rcItem.top);
		}
		
		// 아이콘 위치 설정
		if ( m_nIconPos == 0 )
			RecPos = CPoint ( 6, (rect.Height () - m_nIcon.cy + 1 ) / 2 );
		else if ( m_nIconPos == 1 )
			RecPos = CPoint ( (rect.right - m_nIcon.cx - 6 ), (rect.Height () - m_nIcon.cy + 1 ) / 2 );
	}
	else // 텍스트가 없을시 아이콘을 중앙에 오게 한다..
	{
		if(m_bDodgeImage) // 이미지 사이즈를 버튼 크기에 맞춘다..
		{
			m_nIcon.cx = rect.Width();
			m_nIcon.cy = rect.Height();
		}
		
		RecPos = CPoint (	(int)( ( rect.right - m_nIcon.cx ) / 2 ),
							(int)( ( rect.bottom - m_nIcon.cy ) / 2 ) );
	}
	// 아이콘 이미지를 불러오자.
	//if ( m_nIconID != NULL )
	//{
		//hIcon = AfxGetApp()->LoadIcon ( MAKEINTRESOURCE ( m_nIconID ) );
	//	HRESULT hr = LoadIconWithScaleDown( AfxGetInstanceHandle(), MAKEINTRESOURCE(m_nIconID),
    //                                m_nIcon.cx, m_nIcon.cy, &hIcon );
	//}
	// 선택되었을시 아이콘과 텍스트 위치를 살짝 옮기자 (눌러지는 효과)
	if ( state & ODS_SELECTED ) 
	{
		rcItem.OffsetRect( 1, 1 );
		TextPos.Offset ( 1, 1 );
		RecPos.Offset ( 1, 1);
	}
	// 비활성시 처리
	if ( state & ODS_DISABLED ) 
	{
		CSize  size ( m_nIcon.cx, m_nIcon.cy );
		if ( bWithStr ) 
			pDC->DrawState ( TextPos, TextExtent, strText, DSS_DISABLED, TRUE, 0, (HBRUSH)NULL);
		if ( m_hIcon != NULL )
		{
			HICON	hHelper = (HICON)CopyImage ( m_hIcon, IMAGE_ICON, m_nIcon.cx, m_nIcon.cy, LR_MONOCHROME );
			pDC->DrawState ( RecPos, size, hHelper, DSS_DISABLED, (HBRUSH)NULL );
			DestroyIcon ( (HICON)hHelper );
		}
	}
	else 
	{
		if ( bWithStr )
		{
			//pDC->TextOut ( TextPos.x, TextPos.y, strText );
			pDC->DrawText( strText, strText.GetLength(), &rcItem, uTextFormat );
		}
		if ( m_hIcon != NULL )
		{
			DrawIconEx ( pDC->GetSafeHdc (), rect.left + RecPos.x, rect.top + RecPos.y, 
							m_hIcon, (m_nIcon.cx) ? m_nIcon.cx : 32, (m_nIcon.cy) ? m_nIcon.cy : 32, 
							0, NULL, DI_NORMAL );
		}
	}
	// 포커스 처리
	/*if(!m_bDodgeImage)
	{
		if ( state & ODS_FOCUS ) 
		{
			CBrush brush;
			brush.CreateSysColorBrush ( COLOR_3DDKSHADOW );
			pDC->FrameRect ( rect, &brush );
			rect.DeflateRect ( 4, 4, 4, 4 );
			pDC->DrawFocusRect ( rect );
		}
	}*/
	// 버튼 외곽선 그리자
	if(m_bFlatType == FALSE)
	{
		if ( m_bOnOff )
		{	
			CPen *pOldPen = pDC->SelectObject ( &DarkShadowPen );
			pDC->MoveTo ( CPoint ( lpDrawItemStruct->rcItem.left, lpDrawItemStruct->rcItem.bottom-1 ) );
			pDC->LineTo ( CPoint ( lpDrawItemStruct->rcItem.left, lpDrawItemStruct->rcItem.top ) );
			pDC->LineTo ( CPoint ( lpDrawItemStruct->rcItem.right-1, lpDrawItemStruct->rcItem.top ) );
			pDC->SelectObject ( &HighlightPen );
			pDC->LineTo ( CPoint ( lpDrawItemStruct->rcItem.right-1, lpDrawItemStruct->rcItem.bottom-1 ) );
			pDC->LineTo ( CPoint ( lpDrawItemStruct->rcItem.left, lpDrawItemStruct->rcItem.bottom-1 ) );
			pDC->SelectObject ( pOldPen );
		}
		else
		{
			CPen *pOldPen = pDC->SelectObject ( &HighlightPen );
			pDC->MoveTo ( CPoint ( lpDrawItemStruct->rcItem.left, lpDrawItemStruct->rcItem.bottom-1 ) );
			pDC->LineTo ( CPoint ( lpDrawItemStruct->rcItem.left, lpDrawItemStruct->rcItem.top ) );
			pDC->LineTo ( CPoint ( lpDrawItemStruct->rcItem.right-1, lpDrawItemStruct->rcItem.top ) );
			pDC->SelectObject ( &DarkShadowPen );
			pDC->LineTo ( CPoint ( lpDrawItemStruct->rcItem.right-1, lpDrawItemStruct->rcItem.bottom-1 ) );
			pDC->LineTo ( CPoint ( lpDrawItemStruct->rcItem.left, lpDrawItemStruct->rcItem.bottom-1 ) );
			pDC->SelectObject ( pOldPen );
		}
	}
	else
	{
		CPen *pOldPen = pDC->SelectObject ( &DarkShadowPen );
		pDC->MoveTo ( CPoint ( lpDrawItemStruct->rcItem.left, lpDrawItemStruct->rcItem.bottom-1 ) );
		pDC->LineTo ( CPoint ( lpDrawItemStruct->rcItem.left, lpDrawItemStruct->rcItem.top ) );
		pDC->LineTo ( CPoint ( lpDrawItemStruct->rcItem.right-1, lpDrawItemStruct->rcItem.top ) );
		pDC->LineTo ( CPoint ( lpDrawItemStruct->rcItem.right-1, lpDrawItemStruct->rcItem.bottom-1 ) );
		pDC->LineTo ( CPoint ( lpDrawItemStruct->rcItem.left, lpDrawItemStruct->rcItem.bottom-1 ) );
		pDC->SelectObject ( pOldPen );
	}

	// 핫키 마크 그리자
	if ( ( nHotkeyPos != -1 ) && ( bWithStr ) )
	{
		CString sHlp = strText.Mid ( nHotkeyPos, 1 );
		CSize nWidth = pDC->GetTextExtent ( sHlp );
		sHlp = strText.Left ( nHotkeyPos );
		CSize nStart = pDC->GetTextExtent ( sHlp );
		//
		if ( state & ODS_DISABLED ) {}
		else
		{
			CPen HotkeyPen ( PS_SOLID | PS_COSMETIC, 1, m_nColor );
			CPen *pOldPen = pDC->SelectObject ( &HotkeyPen );
			pDC->MoveTo ( TextPos.x + nStart.cx, TextPos.y + TextExtent.cy  );
			pDC->LineTo ( TextPos.x + nStart.cx + nWidth.cx, TextPos.y + TextExtent.cy  );
			pDC->SelectObject ( pOldPen );
		}
	}

	if(pOldFont) pDC->SelectObject ( pOldFont );
}

void CIconButton::DrawItem_Lamp(LPDRAWITEMSTRUCT lpDrawItemStruct)
{
	CRect		rect		= lpDrawItemStruct->rcItem;
	CRect		rcItem		= lpDrawItemStruct->rcItem;
	CRect		rectCaption = lpDrawItemStruct->rcItem;
	CDC			*pDC		= CDC::FromHandle ( lpDrawItemStruct->hDC );
	UINT		state		= lpDrawItemStruct->itemState;
	UINT		uStyle		= DFCS_BUTTONPUSH;
	bool		bWithStr	= true;
	//HICON		hIcon		= NULL;
	int			nHotkeyPos	= 0;
	CString		strText;
	CPoint		TextPos;
	CPoint		RecPos;
	CSize		TextExtent;
	CPen		HighlightPen	( PS_SOLID | PS_COSMETIC, 1, ::GetSysColor ( COLOR_3DHIGHLIGHT ) );
	CPen		DarkShadowPen	( PS_SOLID | PS_COSMETIC, 2, ::GetSysColor ( COLOR_3DDKSHADOW ) );
	CFont		*pOldFont = NULL;
	//
	ASSERT ( lpDrawItemStruct->CtlType == ODT_BUTTON );
	//그리기전에 팁 없애준다..
	//if ( state & ODS_SELECTED ) 
	{
		if ( ( m_bWithTip ) && ( m_bTipIsOpen ) )
		{
			HideTip ();
		}
	}
	//
	if ( lpDrawItemStruct->itemState & ODS_SELECTED )
	  uStyle |= DFCS_PUSHED;
	//버튼 프레임 그리자
	::DrawFrameControl ( lpDrawItemStruct->hDC, &lpDrawItemStruct->rcItem, DFC_BUTTON, uStyle );
	//
	GetWindowText ( strText );
	bWithStr = ! strText.IsEmpty ();

	//20131218 dhaps2000 - 멀티라인추가
	DWORD dwStyle = GetStyle();
	CIconButton::LB_VERT_ALIGN vAlign = GetVertAlign( dwStyle );
	CIconButton::LB_HORZ_ALIGN hAlign = GetHorzAlign( dwStyle );
	BOOL bIsMultiline = IsMultiLine( dwStyle );
	UINT uTextFormat = DT_WORD_ELLIPSIS;
	switch( hAlign )
	{
	case ALIGN_LEFT:	uTextFormat |= DT_LEFT; break;
	case ALIGN_RIGHT:	uTextFormat |= DT_RIGHT; break;
	case ALIGN_CENTER:	uTextFormat |= DT_CENTER; break;
	}
	switch( vAlign )
	{
	case ALIGN_TOP:		uTextFormat |= DT_TOP; break;
	case ALIGN_BOTTOM:	uTextFormat |= DT_BOTTOM; break;
	case ALIGN_VCENTER:	uTextFormat |= DT_VCENTER; break;
	}
	if( !bIsMultiline )	uTextFormat |= DT_SINGLELINE;
	else uTextFormat |= DT_WORDBREAK;

	if(m_bDodgeImage) bWithStr = false; // 풀이미지 사용시는 텍스트 안뿌리게...

	// 버튼 배경색 처리.....
	if(!m_bDodgeImage)
	{
		if ( state & ODS_DISABLED ) 
		{
			pDC->SelectStockObject ( NULL_BRUSH );
			pDC->FillSolidRect ( rect, ::GetSysColor ( COLOR_BTNFACE ) );
		}
		else
		{
			if ( m_bLampBlink ) 
			{
				CBrush hbrush(m_nBtnColor_SEL);
				pDC->FillRect(rect, &hbrush);
			}
			else
			{
				CBrush hbrush(m_nBtnColor_BK);
				pDC->FillRect(rect, &hbrush);
			}
		}
	}
	else
	{
		pDC->SelectStockObject ( NULL_BRUSH );
		pDC->FillSolidRect ( rect, ::GetSysColor ( COLOR_BTNFACE ) );
	}
	
	// 텍스트가 있다면...
	if ( bWithStr )
	{
		nHotkeyPos = DelAmpersand ( strText );
		pOldFont = pDC->SelectObject ( &m_fnt );
		//TextExtent = pDC->GetTextExtent ( strText );
		TextExtent.cy = pDC->DrawText ( strText, strText.GetLength(), &rectCaption, uTextFormat | DT_CALCRECT);   
		TextExtent.cx = rectCaption.Width();
		if( bIsMultiline && vAlign == ALIGN_VCENTER )
		{
			int nOffsetY = (rcItem.Height() - TextExtent.cy)/2;   
			rcItem.top += nOffsetY;   
			rcItem.bottom = rcItem.top + TextExtent.cy;
		}

		pDC->SetBkMode ( TRANSPARENT );
		pDC->SetTextColor ( m_nColor );

		if ( m_nIconID ) // 아이콘이 있을때의 텍스트 포지션 결정
		{
			if ( m_nIconPos == 0 )
			{
				//TextPos = CPoint (	(int)( ( rect.right - m_nIcon.cx ) / 2 - TextExtent.cx / 2 ) + m_nIcon.cx,
				//					(int)( rect.bottom/2 - TextExtent.cy / 2 ) );

				rcItem.left = (int)( ( rect.right - m_nIcon.cx ) / 2 - TextExtent.cx / 2 ) + m_nIcon.cx;
				rcItem.right = rcItem.left + TextExtent.cx;

				TextPos = CPoint(rcItem.left, rcItem.top);
			}
			else if ( m_nIconPos == 1 )
			{
				//TextPos = CPoint (	(int)( ( rect.right - m_nIcon.cx ) / 2 - TextExtent.cx / 2 ),
				//					(int)( rect.bottom/2 - TextExtent.cy / 2 ) );

				rcItem.left = (int)( ( rect.right - m_nIcon.cx ) / 2 - TextExtent.cx / 2 );
				rcItem.right = rcItem.left + TextExtent.cx;

				TextPos = CPoint(rcItem.left, rcItem.top);
			}
		}
		else // 아이콘이 없을시 텍스트가 중앙에 온다
		{
			//TextPos = CPoint (	(int)(rect.right/2 - TextExtent.cx/2),
			//					(int)(rect.bottom/2 - TextExtent.cy/2) );

			rcItem.left = (int)(rect.right/2 - TextExtent.cx/2);
			rcItem.right = rcItem.left + TextExtent.cx;
			//rcItem.top += (int)(rect.bottom/2 - TextExtent.cy/2);
			//rcItem.bottom = rcItem.top + TextExtent.cy;

			TextPos = CPoint(rcItem.left, rcItem.top);
		}
		
		// 아이콘 위치 설정
		if ( m_nIconPos == 0 )
			RecPos = CPoint ( 6, (rect.Height () - m_nIcon.cy + 1 ) / 2 );
		else if ( m_nIconPos == 1 )
			RecPos = CPoint ( (rect.right - m_nIcon.cx - 6 ), (rect.Height () - m_nIcon.cy + 1 ) / 2 );
	}
	else // 텍스트가 없을시 아이콘을 중앙에 오게 한다..
	{
		if(m_bDodgeImage) // 이미지 사이즈를 버튼 크기에 맞춘다..
		{
			m_nIcon.cx = rect.Width();
			m_nIcon.cy = rect.Height();
		}
		
		RecPos = CPoint (	(int)( ( rect.right - m_nIcon.cx ) / 2 ),
							(int)( ( rect.bottom - m_nIcon.cy ) / 2 ) );
	}
	// 아이콘 이미지를 불러오자.
	//if ( m_nIconID != NULL )
	//{
		//hIcon = AfxGetApp()->LoadIcon ( MAKEINTRESOURCE ( m_nIconID ) );
	//	HRESULT hr = LoadIconWithScaleDown( AfxGetInstanceHandle(), MAKEINTRESOURCE(m_nIconID),
    //                                 m_nIcon.cx, m_nIcon.cy, &hIcon );
	//}
	// 선택되었을시 아이콘과 텍스트 위치를 살짝 옮기자 (눌러지는 효과)
	if ( m_bOnOff ) 
	{
		rcItem.OffsetRect( 1, 1 );
		TextPos.Offset ( 1, 1 );
		RecPos.Offset ( 1, 1);
	}
	// 비활성시 처리
	if ( state & ODS_DISABLED ) 
	{
		CSize  size ( m_nIcon.cx, m_nIcon.cy );
		if ( bWithStr ) 
			pDC->DrawState ( TextPos, TextExtent, strText, DSS_DISABLED, TRUE, 0, (HBRUSH)NULL);
		if ( m_hIcon != NULL )
		{
			HICON	hHelper = (HICON)CopyImage ( m_hIcon, IMAGE_ICON, m_nIcon.cx, m_nIcon.cy, LR_MONOCHROME );
			pDC->DrawState ( RecPos, size, hHelper, DSS_DISABLED, (HBRUSH)NULL );
			DestroyIcon ( (HICON)hHelper );
		}
	}
	else 
	{
		if ( bWithStr )
		{
			//pDC->TextOut ( TextPos.x, TextPos.y, strText );
			pDC->DrawText( strText, strText.GetLength(), &rcItem, uTextFormat );
		}
		if ( m_hIcon != NULL )
		{
			DrawIconEx ( pDC->GetSafeHdc (), rect.left + RecPos.x, rect.top + RecPos.y, 
							m_hIcon, (m_nIcon.cx) ? m_nIcon.cx : 32, (m_nIcon.cy) ? m_nIcon.cy : 32, 
							0, NULL, DI_NORMAL );
		}
	}
	// 포커스 처리
	if(!m_bDodgeImage)
	{
		if ( state & ODS_FOCUS ) 
		{
			CBrush brush;
			brush.CreateSysColorBrush ( COLOR_3DDKSHADOW );
			pDC->FrameRect ( rect, &brush );
			rect.DeflateRect ( 4, 4, 4, 4 );
			pDC->DrawFocusRect ( rect );
		}
	}
	// 버튼 외곽선 그리자
	if(m_bFlatType == FALSE)
	{
		if ( m_bOnOff )
		{	
			CPen *pOldPen = pDC->SelectObject ( &DarkShadowPen );
			pDC->MoveTo ( CPoint ( lpDrawItemStruct->rcItem.left, lpDrawItemStruct->rcItem.bottom-1 ) );
			pDC->LineTo ( CPoint ( lpDrawItemStruct->rcItem.left, lpDrawItemStruct->rcItem.top ) );
			pDC->LineTo ( CPoint ( lpDrawItemStruct->rcItem.right-1, lpDrawItemStruct->rcItem.top ) );
			pDC->SelectObject ( &HighlightPen );
			pDC->LineTo ( CPoint ( lpDrawItemStruct->rcItem.right-1, lpDrawItemStruct->rcItem.bottom-1 ) );
			pDC->LineTo ( CPoint ( lpDrawItemStruct->rcItem.left, lpDrawItemStruct->rcItem.bottom-1 ) );
			pDC->SelectObject ( pOldPen );
		}
		else
		{
			CPen *pOldPen = pDC->SelectObject ( &HighlightPen );
			pDC->MoveTo ( CPoint ( lpDrawItemStruct->rcItem.left, lpDrawItemStruct->rcItem.bottom-1 ) );
			pDC->LineTo ( CPoint ( lpDrawItemStruct->rcItem.left, lpDrawItemStruct->rcItem.top ) );
			pDC->LineTo ( CPoint ( lpDrawItemStruct->rcItem.right-1, lpDrawItemStruct->rcItem.top ) );
			pDC->SelectObject ( &DarkShadowPen );
			pDC->LineTo ( CPoint ( lpDrawItemStruct->rcItem.right-1, lpDrawItemStruct->rcItem.bottom-1 ) );
			pDC->LineTo ( CPoint ( lpDrawItemStruct->rcItem.left, lpDrawItemStruct->rcItem.bottom-1 ) );
			pDC->SelectObject ( pOldPen );
		}
	}
	// 핫키 마크 그리자
	if ( ( nHotkeyPos != -1 ) && ( bWithStr ) )
	{
		CString sHlp = strText.Mid ( nHotkeyPos, 1 );
		CSize nWidth = pDC->GetTextExtent ( sHlp );
		sHlp = strText.Left ( nHotkeyPos );
		CSize nStart = pDC->GetTextExtent ( sHlp );
		//
		if ( state & ODS_DISABLED ) {}
		else
		{
			CPen HotkeyPen ( PS_SOLID | PS_COSMETIC, 1, m_nColor );
			CPen *pOldPen = pDC->SelectObject ( &HotkeyPen );
			pDC->MoveTo ( TextPos.x + nStart.cx, TextPos.y + TextExtent.cy  );
			pDC->LineTo ( TextPos.x + nStart.cx + nWidth.cx, TextPos.y + TextExtent.cy  );
			pDC->SelectObject ( pOldPen );
		}
	}

	if(pOldFont) pDC->SelectObject ( pOldFont );
}

void CIconButton::NewFont()
{
	m_fnt.DeleteObject ();
	m_fnt.CreateFont  (	m_TextMetrics.tmHeight,			// nHeight
						m_TextMetrics.tmMaxCharWidth,	// nWidth
						0,								// nEscapement
						0,								// nOrientation
						m_TextMetrics.tmWeight,			// nWeight
						m_TextMetrics.tmItalic,			// bItalic
						m_TextMetrics.tmUnderlined,		// bUnderline
						m_TextMetrics.tmStruckOut,		// bStrikeOut
						m_TextMetrics.tmCharSet,		// nCharSet
						OUT_DEFAULT_PRECIS,				// nOutPrecision
						CLIP_DEFAULT_PRECIS,			// nClipPrecision
						DEFAULT_QUALITY,				// nQuality
						m_TextMetrics.tmPitchAndFamily,	// nPitchAndFamily
						(LPCTSTR)m_sFaceName );			// lpszFacename
  
}

void CIconButton::Disable ( void )
{ 
	EnableWindow ( false );
	Invalidate ();
	//UpdateWindow ();
}

void CIconButton::Enable ( void )
{ 
	EnableWindow ( true );
	Invalidate ();
	//UpdateWindow ();
}

int CIconButton::DelAmpersand ( CString &sTxt )
{
	int nPos = 0;
	if ( ( nPos = sTxt.Find ( _T("&") )	) != -1 )
	{
		CString sLeft = sTxt.Left ( nPos );
		CString sRight = sTxt.Mid ( nPos + 1 );
		sTxt = sLeft + sRight;
	}
	return nPos;
}

///////////////////////////////////////////////////////////////////////////////
//
void CIconButton::HideTip ( void )
{
	RestoreBitmap ();
	m_bTipIsOpen = false;
}

///////////////////////////////////////////////////////////////////////////////
//
void CIconButton::ShowTip ( void )
{
	if ( m_bWithTip )
	{
		SetCapture ();
		GetCurrentTextMetric ();
		DrawTipBoxAndText ();
		m_bTipIsOpen = true;
	}
}

///////////////////////////////////////////////////////////////////////////////
//
void CIconButton::GetCurrentTextMetric ( void )
{
	if ( m_bWithTip )
	{
		CDC	*pDC = GetParent ()->GetDC ();
		if ( pDC )
		{
			m_TipDim.cx = pDC->GetTextExtent ( m_sTip ).cx + 4;
			m_TipDim.cy = pDC->GetTextExtent ( m_sTip ).cy + 4;
		}
	}
}

///////////////////////////////////////////////////////////////////////////////
//
void CIconButton::DrawTipBoxAndText ( void )
{
	if ( m_bWithTip )
	{
		if ( ! m_bTipIsOpen ) 
		{
			//CDC *pDC = GetDC ();
			CDC *pDC = GetOwner ()->GetDC ();
			if ( pDC )
			{
				CBrush BkGrBrush ( TIPBGRCOLOR );
				CBrush *pOldBrush = pDC->SelectObject ( &BkGrBrush );
				CPen BlackPen ( PS_SOLID, 1, RGB ( 0, 0, 0 ) );
				CPen *pOldPen = pDC->SelectObject ( &BlackPen );
				//
				m_TipRect.SetRect ( 0, 0, m_TipDim.cx, m_TipDim.cy );
				m_TipRect.OffsetRect ( m_TipPos.x, m_TipPos.y );
				//
				CRect ParentRect;
				GetParent ()->GetWindowRect ( ParentRect );
				if ( m_TipRect.right > ParentRect.right )
				{
					int nDiv = m_TipRect.right - ParentRect.right + 5;
					m_TipRect.right -= nDiv;
					m_TipRect.left -= nDiv;
				}
				//
				GetOwner ()->ScreenToClient ( m_TipRect );
				SaveBitmap ();
				pDC->Rectangle ( m_TipRect );
				pDC->SetBkMode ( TRANSPARENT );
				pDC->DrawText ( m_sTip, m_TipRect, DT_CENTER );
				pDC->SelectObject ( pOldBrush );
				pDC->SelectObject ( pOldPen );
			}
		}
	}
}

///////////////////////////////////////////////////////////////////////////////
//
LRESULT CIconButton::OnNcHitTest(CPoint point) 
{
	if ( m_bWithTip )
	{
		if ( ! m_bTipIsOpen )
		{
			m_TipPos = point;
			ShowTip ();
		}
	}
	return CButton::OnNcHitTest(point);
}

void CIconButton::OnLButtonDown(UINT nFlags, CPoint point)
{
	NMHDR hdr;
	hdr.hwndFrom = m_hWnd;
	hdr.idFrom = (UINT)GetDlgCtrlID();
	hdr.code = TCN_SELCHANGE;
	GetParent()->SendMessage(WM_NOTIFY, GetDlgCtrlID(), (LPARAM)&hdr);

	m_bButtonPressed = TRUE;

	CButton::OnLButtonDown(nFlags, point);
}


void CIconButton::OnLButtonUp(UINT nFlags, CPoint point)
{
	NMHDR hdr;
	hdr.hwndFrom = m_hWnd;
	hdr.idFrom = (UINT)GetDlgCtrlID();
	hdr.code = TCN_SELCHANGING;
	GetParent()->SendMessage(WM_NOTIFY, GetDlgCtrlID(), (LPARAM)&hdr);

	m_bButtonPressed = FALSE;

	CButton::OnLButtonUp(nFlags, point);
}

///////////////////////////////////////////////////////////////////////////////
//
void CIconButton::OnMouseMove(UINT nFlags, CPoint point) 
{
	CRect ClientRect;
	GetClientRect ( &ClientRect );
	if ( ! ClientRect.PtInRect ( point ) )	
	{	
		HideTip ();
		ReleaseCapture ();
	}
	CButton::OnMouseMove ( nFlags, point );
}

///////////////////////////////////////////////////////////////////////////////
//
bool CIconButton::RestoreBitmap ( void )
{
	bool bResult = false;
	//
	::BitBlt (	m_hOwnerDC,
				m_TipRect.left, m_TipRect.top, 
				m_TipDim.cx, m_TipDim.cy,
				m_hMemDC,
				0, 0,
				SRCCOPY );
	DeleteObject ( m_hMemBmp );
	DeleteDC ( m_hMemDC );
	DeleteDC ( m_hOwnerDC );
	//
	return bResult;
}

///////////////////////////////////////////////////////////////////////////////
//
bool CIconButton::SaveBitmap ( void )
{
	bool bResult = true;
	//
 	m_hOwnerDC	= GetOwner ()->GetDC ()->m_hDC;				
	m_hMemDC	= ::CreateCompatibleDC ( m_hOwnerDC );
	m_hMemBmp	= ::CreateCompatibleBitmap ( m_hOwnerDC, m_TipDim.cx, m_TipDim.cy );
	SelectObject ( m_hMemDC, m_hMemBmp );
	::BitBlt (	m_hMemDC,
				0, 0,
				m_TipDim.cx, m_TipDim.cy,
				m_hOwnerDC,
				m_TipRect.left, m_TipRect.top,
				SRCCOPY );
	//
	return bResult;
}

void CIconButton::SetEnable(bool bEnable)
{
	if(bEnable) Enable();
	else Disable();
}

//20131202 dhaps2000 - LAMP
void CIconButton::SetLampOnOff(BOOL bOnOff)
{
	if(m_bLampOnOff == bOnOff) return;
	m_bLampOnOff = bOnOff;

	if(m_bLampOnOff) SetTimer(1, 500, NULL);
	else KillTimer(1);
}

void CIconButton::SetUseBeep(BOOL bBeep)
{
	m_bUseBeep = bBeep;

	if(m_bUseBeep) SetTimer(2, 100, NULL);
	else KillTimer(2);
}

CIconButton::LB_HORZ_ALIGN CIconButton::GetHorzAlign( DWORD dwStyle )
{
	if( (dwStyle & BS_CENTER) == BS_LEFT )
	{
		return ALIGN_LEFT;
	}
	if( (dwStyle & BS_CENTER) == BS_RIGHT )
	{
		return ALIGN_RIGHT;
	}
	if( (dwStyle & BS_CENTER) == BS_CENTER )
	{
		return ALIGN_CENTER;
	}

	return ALIGN_CENTER;
}

BOOL CIconButton::IsMultiLine( DWORD dwStyle )
{
	return ((dwStyle & BS_MULTILINE) == BS_MULTILINE ? TRUE : FALSE);
}

CIconButton::LB_VERT_ALIGN CIconButton::GetVertAlign( DWORD dwStyle )
{
	if( (dwStyle & BS_VCENTER) == BS_VCENTER )
	{
		return ALIGN_VCENTER;
	}
	if( (dwStyle & BS_VCENTER) == BS_TOP )
	{
		return ALIGN_TOP;
	}
	if( (dwStyle & BS_VCENTER) == BS_BOTTOM )
	{
		return ALIGN_BOTTOM;
	}

	return ALIGN_VCENTER;
}

void CIconButton::OnTimer(UINT_PTR nIDEvent) 
{
	if(nIDEvent == 1)
	{
		m_bLampBlink = !m_bLampBlink;
		RedrawWindow();
	}
	else if(nIDEvent == 2)
	{
		if(m_bButtonPressed) Beep(392,200);
	}
		
	CButton::OnTimer(nIDEvent);
}
