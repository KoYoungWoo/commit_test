// FlatLabelExt.cpp : 구현 파일입니다.
// 2018.03.29 dhaps2000 

#include "stdafx.h"
#include "FlatLabelExt.h"
#include "ResourceManager.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

using namespace UIExt;
// CFlatLabelExt

IMPLEMENT_DYNAMIC(CFlatLabelExt, CGdipStatic)

CFlatLabelExt::CFlatLabelExt()
{
	m_dwTextColor = RGB(255,255,255);
	m_dwBorderColor = (DWORD)-1;
	m_nBorderWidth = 1;
	m_dwBodyColor = RGB(89,101,117);
	m_nFontHeight = 0;
	m_bIsBold = FALSE;
	m_bOnOffStyle = FALSE;
	m_dwOnColor = RGB(10,225,10);
	m_dwOffColor = RGB(255,10,10);
	m_dwDisableColor = RGB(100,100,100);
	m_nOnOffSignal = 0;
}

CFlatLabelExt::~CFlatLabelExt()
{
}


BEGIN_MESSAGE_MAP(CFlatLabelExt, CGdipStatic)
	ON_WM_UPDATEUISTATE()
END_MESSAGE_MAP()

void CFlatLabelExt::SetColor( COLORREF dwBodyColor, COLORREF dwTextColor, COLORREF dwBorderColor/*=-1*/ )
{
	m_dwBodyColor = dwBodyColor;
	m_dwTextColor = dwTextColor;
	m_dwBorderColor = dwBorderColor;
	if (GetSafeHwnd())
		Invalidate();
}

void CFlatLabelExt::SetOnOffColor( COLORREF dwOnColor, COLORREF dwOffColor, COLORREF dwDisableColor)
{
	m_dwOnColor = dwOnColor;
	m_dwOffColor = dwOffColor;
	m_dwDisableColor = dwDisableColor;
	if (GetSafeHwnd())
		Invalidate();
}

void CFlatLabelExt::SetBorderWidth( int nWidth )
{
	if (m_nBorderWidth == nWidth)
		return;

	m_nBorderWidth = nWidth;
	if (GetSafeHwnd())
		Invalidate();
}

void CFlatLabelExt::SetBold( BOOL bBold )
{
	if (m_bIsBold == bBold)
		return;

	m_bIsBold = bBold;
	if (GetSafeHwnd())
		Invalidate();
}

void CFlatLabelExt::SetFontHeight( int nHeight/*=0*/ )
{
	if (m_nFontHeight == nHeight)
		return;

	m_nFontHeight = nHeight;
	if (GetSafeHwnd())
		Invalidate();
}

void CFlatLabelExt::SetOnOffStyle( BOOL bOnOffStyle )
{
	m_bOnOffStyle = bOnOffStyle;
	if (GetSafeHwnd())
		Invalidate();
}

void CFlatLabelExt::SetOnOffSignal( int nSignal )
{
	if(nSignal < 0 || nSignal > 2) return;

	m_nOnOffSignal = nSignal;
	if (GetSafeHwnd())
		Invalidate();
}

void CFlatLabelExt::OnDraw(Gdiplus::Graphics& g, Rect rect)
{
	using namespace Gdiplus;

	Rect rectBody = rect;

	BOOL bDisabled = IsWindowEnabled() ? FALSE : TRUE;

	int nOnOffCircleAreaWidth = 0;
	if (m_bOnOffStyle)
	{
		// draw icon area
		Rect rectIcon;
		Color colorCircle;
		rectIcon = rectBody;
		rectIcon.Width = (int)(rectBody.Height*1.1f+0.5f);

		int nIX = rectIcon.X + 1;
		int nIY = rectIcon.Y + 1;
		int nIW = rectBody.Height-2;
		int nIH = rectBody.Height-2;

		if (!bDisabled)
		{
			if(m_nOnOffSignal == 0)	colorCircle.SetFromCOLORREF(m_dwDisableColor);
			if(m_nOnOffSignal == 1)	colorCircle.SetFromCOLORREF(m_dwOnColor);
			if(m_nOnOffSignal == 2)	colorCircle.SetFromCOLORREF(m_dwOffColor);
		}
		else
		{
			colorCircle.SetFromCOLORREF(m_dwDisableColor);
		}

		g.FillEllipse(&SolidBrush(colorCircle), nIX, nIY, nIW, nIH);

		nOnOffCircleAreaWidth = rectIcon.Width;
	}

	Gdiplus::Font fontCaption( 
		CResourceManager::SystemFontFamily(), 
		m_nFontHeight == 0 ? (float)rectBody.Height*0.60f : (float)m_nFontHeight, 
		m_bIsBold ? FontStyleBold : FontStyleRegular, 
		UnitPixel );

	CString strCaption;
	GetWindowText( strCaption );
	BSTR bstrCaption = strCaption.AllocSysString();

	Color colorCaption;
	if (!bDisabled)
	{
		colorCaption.SetFromCOLORREF( m_dwTextColor );
	}
	else
	{
		BYTE cGray = (GetRValue(m_dwTextColor)+GetGValue(m_dwTextColor)+GetBValue(m_dwTextColor)) / 3;
		DWORD dwGrayTextColor = RGB(cGray, cGray, cGray);
		colorCaption.SetFromCOLORREF( dwGrayTextColor );
	}
	SolidBrush brushCaption(colorCaption);

	RectF rectCaptionF( (float)rectBody.X+nOnOffCircleAreaWidth, (float)rectBody.Y, (float)rectBody.Width-nOnOffCircleAreaWidth, (float)rectBody.Height );
	rectCaptionF.Inflate( -rectCaptionF.Height/8, 0 );

	UINT nStyle = GetStyle();
	StringFormat stringFormat;
	if ( (nStyle & 0x00F) == SS_LEFT )
		stringFormat.SetAlignment( StringAlignmentNear );
	else if ( (nStyle & 0x00F) == SS_RIGHT )
		stringFormat.SetAlignment( StringAlignmentFar );
	else
		stringFormat.SetAlignment( StringAlignmentCenter );
	if ( (nStyle & 0xF00) == SS_CENTERIMAGE )
		stringFormat.SetLineAlignment( StringAlignmentCenter );
	else
		stringFormat.SetLineAlignment( StringAlignmentNear );
	stringFormat.SetTrimming( StringTrimmingEllipsisCharacter );

	RectF rectCaptionBBF;
	g.MeasureString( bstrCaption, -1, &fontCaption, rectCaptionF, &stringFormat, &rectCaptionBBF );

	float fY = rectCaptionF.Y + (rectCaptionF.Height-rectCaptionBBF.Height) / 2.f;

	rectCaptionF.Y = fY;
	rectCaptionF.Height = rectCaptionBBF.Height;

	g.SetSmoothingMode( SmoothingModeAntiAlias );
	g.SetTextRenderingHint( TextRenderingHintClearTypeGridFit );

	g.SetTextContrast(8);		// 0~12
	g.DrawString( bstrCaption, -1, &fontCaption, rectCaptionF, &stringFormat, &brushCaption );

	if (m_dwBorderColor != (DWORD)-1)
	{
		if (m_nBorderWidth > 0)
		{
			Color colorBorder;
			if (!bDisabled)
			{
				colorBorder.SetFromCOLORREF( m_dwBorderColor );
			}
			else
			{
				BYTE cGray = (GetRValue(m_dwBorderColor)+GetGValue(m_dwBorderColor)+GetBValue(m_dwBorderColor)) / 3;
				DWORD dwGrayBorderColor = RGB(cGray, cGray, cGray);
				colorBorder.SetFromCOLORREF( dwGrayBorderColor );
			}
			rectBody.Width--;
			rectBody.Height--;
			rectBody.Inflate(-m_nBorderWidth/2, -m_nBorderWidth/2);
			g.DrawRectangle( &Pen(colorBorder, (float)m_nBorderWidth), rectBody );
		}
	}

	::SysFreeString( bstrCaption );
}


void CFlatLabelExt::OnGdipEraseBkgnd( Gdiplus::Graphics& g, Rect rect )
{
	using namespace Gdiplus;

	BOOL bDisabled = IsWindowEnabled() ? FALSE : TRUE;
	Color colorBody;
	if (!bDisabled)
	{
		colorBody.SetFromCOLORREF(m_dwBodyColor);
	}
	else
	{
		BYTE cGray = (GetRValue(m_dwBodyColor)+GetGValue(m_dwBodyColor)+GetBValue(m_dwBodyColor)) / 3;
		DWORD dwGrayBodyColor = RGB(cGray, cGray, cGray);
		colorBody.SetFromCOLORREF(dwGrayBodyColor);
	}
	g.FillRectangle( &SolidBrush(colorBody), rect );
}

void CFlatLabelExt::PreSubclassWindow()
{
	ModifyStyleEx( WS_EX_STATICEDGE, 0, SWP_FRAMECHANGED );

	CGdipStatic::PreSubclassWindow();
}


void CFlatLabelExt::OnUpdateUIState(UINT nAction, UINT nUIElement)
{
	if ( nAction != UIS_SET )
		CGdipStatic::OnUpdateUIState(nAction, nUIElement);
}
