#include "stdafx.h"
#include "StatsGraph.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CStatsGraph::CStatsGraph()
{
	m_ColBack = RGB(176,196,222);
	m_ColGrid = RGB(0,160,0);
	m_ColGridMain = RGB(0,0,0);

	m_nInterval = GRID_INTERVAL;
	m_bGrid = TRUE;
	m_bViewGrid_Row = TRUE;
	m_bViewGrid_Col = TRUE;

	m_fMaxValX = 24.0f;
	m_fMaxValY = 100.0f;
	m_fMinValY = 0.0f;
	m_nCol = 0;
	m_nRow = 0;
	m_fXInterval = 0.0f;
	m_fYInterval = 0.0f;

	m_nWidth = 0;
	m_nHeight = 0;	

	m_nTest_Type = 0;

	///////////////////////////////////////////////////////////////////////

	m_bViewOutLine = FALSE;
	m_ColOutLine = RGB(0,0,0);

	m_crSpecEdge = CR_DARKRED;
	m_crSpecLoop = CR_DARKRED;
	m_crWarnEdge = CR_ORANGE;
	m_crWarnLoop = CR_ORANGE;

	m_bEdgeSpec = FALSE; // Edge Spec
	m_bEdgeWarn = FALSE;
	m_bLoopSpec = FALSE;
	m_bLoopWarn = FALSE;
	m_bEdgeSpecWarn = FALSE;
	m_bLoopSpecWarn = FALSE;

	m_nRealMinSpecEdge = 0;
	m_nRealMaxSpecEdge = 0;
	m_nRealMinWarnEdge = 0;
	m_nRealMaxWarnEdge = 0;

	m_nRealMinSpecLoop = 0;
	m_nRealMaxSpecLoop = 0;
	m_nRealMinWarnLoop = 0;
	m_nRealMaxWarnLoop = 0;
}

CStatsGraph::~CStatsGraph()
{
	Clear();
}

void CStatsGraph::Initialize()
{
	GetClientRect(&m_Rect);
	m_TextRectY = m_GraphRect = m_Rect;

	m_TextRectY.right = m_TextRectY.left + 50;
	m_GraphRect.left = m_TextRectY.right + 2;
	m_GraphRect.right -= 2;

	m_TextRectY.top += 10;
	m_TextRectY.bottom -= 20;

	m_GraphRect.top += 10;
	m_GraphRect.bottom -= 20;

	m_TextRectX.left = m_GraphRect.left + 2;
	m_TextRectX.top = m_GraphRect.bottom + 2;
	m_TextRectX.right = m_Rect.right - 2;
	m_TextRectX.bottom = m_Rect.bottom - 2;

	m_fYInterval = (float)m_nInterval;
	m_fXInterval = m_fYInterval;

	m_nWidth = m_Rect.Width();
	m_nHeight = m_Rect.Height();
	//m_nWidth = m_GraphRect.Width();
	//m_nHeight = m_GraphRect.Height();

	m_nCol = (int)(m_GraphRect.Width()/m_fXInterval);
	m_nRow = (int)(m_GraphRect.bottom/m_fYInterval);

	Clear();

	CClientDC dc(this);
	m_dcGraph.CreateCompatibleDC(&dc);
	m_bitmap.CreateCompatibleBitmap(&dc, m_nWidth, m_nHeight);
	m_pOldBitmap = m_dcGraph.SelectObject(&m_bitmap);
}

void CStatsGraph::Clear()
{
	if(m_dcGraph != NULL)
	{
		m_dcGraph.SelectObject(m_pOldBitmap);
		m_bitmap.DeleteObject();
		m_dcGraph.DeleteDC();
	}
	
	POSITION pos = m_GraphList.GetHeadPosition();
	while(pos)
	{
		GRIDXY * pGrid = (GRIDXY *)m_GraphList.GetNext(pos);
		if(pGrid) delete pGrid;
	}

	m_GraphList.RemoveAll();
}

void CStatsGraph::Reset_GridList()
{
	POSITION pos = m_GraphList.GetHeadPosition();
	while(pos)
	{
		GRIDXY * pGrid = (GRIDXY *)m_GraphList.GetNext(pos);
		if(pGrid) delete pGrid;
	}

	m_GraphList.RemoveAll();
}

BEGIN_MESSAGE_MAP(CStatsGraph, CStatic)
	//{{AFX_MSG_MAP(CStatsGraph)
	ON_WM_PAINT()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

GRIDXY * CStatsGraph::AddNewGraph(COLORREF col, int nLineWidth, eGRAPHTYPE type, int nBarWidth)
{
	GRIDXY * pGrid = new GRIDXY;
	if(!pGrid) return NULL;

	pGrid->colGrid = col;
	pGrid->bGrid = TRUE;	
	pGrid->gridPen.CreatePen(PS_SOLID, nLineWidth, pGrid->colGrid);
	pGrid->gridBrush.CreateSolidBrush(pGrid->colGrid); 
	pGrid->nCount = 0;
	pGrid->m_eGraphType = type;

	if(nBarWidth <= 0) pGrid->m_nBarWidth = (int)m_fXInterval;
	else if(nBarWidth >= m_fXInterval)
	{
		if(m_fXInterval <= 0) pGrid->m_nBarWidth = 1;
		else pGrid->m_nBarWidth = (int)m_fXInterval;
	}
	else pGrid->m_nBarWidth = nBarWidth;

	ZeroMemory(pGrid->data, sizeof(int)*MAX_COUNT);
	
	m_GraphList.AddTail(pGrid);

	return pGrid;
}

// AddAreaData : 구역 형성 라인 그리기.
// 그래프별 Min, Max Edge값을 구함. giheon 140903
void CStatsGraph::AddAreaData(float fMinValue, float fMaxValue, BOOL bSpecWarn, BOOL bEdgeLoop)
{
	int nRealMinEdge = 0;
	int nRealMaxEdge = 0;
	float fRealInterVal = 0;
	if(fMinValue >= 0 && fMaxValue >= 0)
	{
		fRealInterVal = ((m_GraphRect.bottom/(m_nRow + 1))/m_fYInterval);
		//nRealInterVal = (int)((m_GraphRect.bottom/(m_nRow + 1)));
		
		nRealMinEdge = (int)(fMinValue * fRealInterVal);
		nRealMaxEdge = (int)(fMaxValue * fRealInterVal);
		//nRealMinEdge = (int)((fMinValue*m_GraphRect.Height())/(m_fMaxValY + nRealInterVal));
		//nRealMaxEdge = (int)((fMaxValue*m_GraphRect.Height())/(m_fMaxValY + nRealInterVal));
		
		if(nRealMinEdge > m_GraphRect.Height())
			nRealMinEdge = m_GraphRect.Height();
		
		if(nRealMaxEdge > m_GraphRect.Height())
			nRealMaxEdge = m_GraphRect.Height();

		if(nRealMinEdge < 0) nRealMinEdge = 0;

		if(nRealMaxEdge < 0) nRealMaxEdge = 0;

		if(bEdgeLoop)
		{
			if(bSpecWarn)
			{
				m_nRealMinSpecEdge = nRealMinEdge;
				m_nRealMaxSpecEdge = nRealMaxEdge;
				m_bEdgeSpec = TRUE;
			}
			else
			{
				m_nRealMinWarnEdge = nRealMinEdge;
				m_nRealMaxWarnEdge = nRealMaxEdge;
				m_bEdgeWarn = TRUE;
			}
		}
		else
		{
			if(bSpecWarn)
			{
				m_nRealMinSpecLoop = nRealMinEdge;
				m_nRealMaxSpecLoop = nRealMaxEdge;
				m_bLoopSpec = TRUE;
			}
			else
			{
				m_nRealMinWarnLoop = nRealMinEdge;
				m_nRealMaxWarnLoop = nRealMaxEdge;
				m_bLoopWarn = TRUE;
			}
		}
	}
}

void CStatsGraph::OnPaint() 
{
	CPaintDC dc(this); 
	dc.BitBlt(0, 0, m_nWidth, m_nHeight, &m_dcGraph, 0, 0, SRCCOPY);
}

void CStatsGraph::Display()
{
	m_dcGraph.FillSolidRect(&m_Rect, m_ColBack);

	DisplayGrid(m_dcGraph);
	DisplayXTick(m_dcGraph);
	DisplayYTick(m_dcGraph);	
	DisplayOutLine(m_dcGraph);

	POSITION pos = m_GraphList.GetHeadPosition();
	if(NULL == pos) return;
	
	while(pos)
	{
		GRIDXY * pGrid = (GRIDXY *)m_GraphList.GetNext(pos);
		if(pGrid->bGrid)
		{			
			CPen * pOldPen = m_dcGraph.SelectObject(&pGrid->gridPen);
			CBrush * pOldBrush = m_dcGraph.SelectObject(&pGrid->gridBrush);

			POINT ptNew, ptOld;
			ptNew.x = m_GraphRect.left;
			ptNew.y = m_GraphRect.bottom;
			ptOld = ptNew;
			
			for(int i = 0; i < pGrid->nCount; i++)
			{
				ptNew.x += (LONG)m_fXInterval;
				ptNew.y = m_GraphRect.bottom - pGrid->data[i];

				if(pGrid->m_eGraphType == eGRAPHTYPE::GRAPHTYPE_LINE)
				{
					if(i != 0)
					{
						//m_dcGraph.Ellipse(ptNew.x-2, ptNew.y+2, ptNew.x+2, ptNew.y-2);
						m_dcGraph.MoveTo(ptNew);
						m_dcGraph.LineTo(ptOld);
					}
				}
				else if(pGrid->m_eGraphType == eGRAPHTYPE::GRAPHTYPE_BAR)
				{
					m_dcGraph.FillSolidRect(ptNew.x-pGrid->m_nBarWidth/2, ptNew.y, pGrid->m_nBarWidth, pGrid->data[i], RGB(0,0,0)); // outline
					m_dcGraph.FillSolidRect(ptNew.x-pGrid->m_nBarWidth/2+1, ptNew.y+1, pGrid->m_nBarWidth-2, pGrid->data[i]-2, pGrid->colGrid);
				}
				
				ptOld = ptNew;
			}

			m_dcGraph.SelectObject(pOldPen);
			m_dcGraph.SelectObject(pOldBrush);
		}
	}

	// (Edge, Loop)의 값중 Spec과 Warn값의 Min, Max 값을 그려준다. giheon 140903
	if(m_bEdgeSpecWarn || m_bLoopSpecWarn)
	{
		if(m_bEdgeSpecWarn)	DisplayAreaLine(m_dcGraph, TRUE);
		if(m_bLoopSpecWarn)	DisplayAreaLine(m_dcGraph, FALSE);
	}

	RedrawWindow();
}

// 해당 구역 Min, Max 라인 값을 구하고, 각 라인의 색상값도 구해준다. giheon 140903
int CStatsGraph::ChkAreaSelLine(int *nSpecMin, int *nSpecMax, int *nWarnMin, int *nWarnMax, BOOL bEdgeLoop)
{
	int nStempMin, nStempMax, nWtempMin, nWtempMax;
	int nResult;
	
	if(bEdgeLoop)
	{
		nStempMin = m_GraphRect.bottom - m_nRealMinSpecEdge;
		nStempMax = m_GraphRect.bottom - m_nRealMaxSpecEdge;
		nWtempMin = m_GraphRect.bottom - m_nRealMinWarnEdge;
		nWtempMax = m_GraphRect.bottom - m_nRealMaxWarnEdge;
	}
	else
	{
		nStempMin = m_GraphRect.bottom - m_nRealMinSpecLoop;
		nStempMax = m_GraphRect.bottom - m_nRealMaxSpecLoop;
		nWtempMin = m_GraphRect.bottom - m_nRealMinWarnLoop;
		nWtempMax = m_GraphRect.bottom - m_nRealMaxWarnLoop;
	}
	// Result Code = 0 (ALL),1 (Same Min), 2 (Same Max), 3 (Not Same)
	if(nStempMin == nWtempMin)
	{
		if(nStempMax == nWtempMax)
			nResult = 0;
		else
			nResult = 1;
	}
	else
	{
		if(nStempMax == nWtempMax)
			nResult = 2;
		else
			nResult = 3;
	}

	//각 변수값을 설정함
	*nSpecMin = nStempMin;
	*nSpecMax = nStempMax;
	*nWarnMin = nWtempMin;
	*nWarnMax = nWtempMax;

	if(m_nTest_Type == 1)
		return 4;
	else
		return nResult;
}

void CStatsGraph::DisplayGrid(CDC &dc)
{
	if(FALSE == m_bGrid) return;

	CPen gridPen(PS_SOLID, 1, m_ColGrid);
	CPen gridPenMain(PS_SOLID, 1, m_ColGridMain);

	CPen * pOldPen = dc.SelectObject(&gridPen);

	int nX = m_GraphRect.left;
	while(TRUE)
	{
		if(nX == m_GraphRect.left)
		{
			dc.SelectObject(&gridPenMain);
			dc.MoveTo(nX, m_GraphRect.top);
			dc.LineTo(nX, m_GraphRect.bottom);
		}
		else
		{
			dc.SelectObject(&gridPen);

			if(m_bViewGrid_Col == TRUE)
			{
				dc.MoveTo(nX, m_GraphRect.top);
				dc.LineTo(nX, m_GraphRect.bottom);
			}
		}

		nX += (int)m_fXInterval;

		if(nX > m_GraphRect.right) break;
	}

	int nY = m_GraphRect.bottom;
	int nYInterval = m_GraphRect.bottom/(m_nRow+1);
	while(TRUE)
	{
		if(nY == m_GraphRect.bottom)
		{
			dc.SelectObject(&gridPenMain);
			dc.MoveTo(m_GraphRect.left, nY);
			dc.LineTo(m_GraphRect.right, nY);
		}
		else
		{
			dc.SelectObject(&gridPen);

			if(m_bViewGrid_Row == TRUE)
			{
				dc.MoveTo(m_GraphRect.left, nY);
				dc.LineTo(m_GraphRect.right, nY);
			}
		}

		nY -= nYInterval;

		if(nY < m_GraphRect.top) break;
	}

	dc.SelectObject(pOldPen);
}
void CStatsGraph::DisplayAreaLine(CDC &dc, BOOL nEdgeLoop)
{
	COLORREF crSpec;
	COLORREF crWarn;

	if(nEdgeLoop)
	{
		crSpec = m_crSpecEdge;
		crWarn = m_crWarnEdge;
	}
	else
	{
		crSpec = m_crSpecLoop;
		crWarn = m_crWarnLoop;
	}

	CPen DrawPen ( PS_SOLID | PS_COSMETIC, 1, crSpec);
	CPen DrawPen2 ( PS_SOLID | PS_COSMETIC, 1, crWarn);
	CPen DrawAllPen ( PS_SOLID | PS_COSMETIC, 1, RGB(255,0,0)); //동일값일시 RED
	CPen *pOldPen = m_dcGraph.SelectObject(&DrawPen);
		
	int nRealMinSpec = 0;
	int nRealMaxSpec = 0;
	int nRealMinWarn = 0;
	int nRealMaxWarn = 0;
	int nChkLine = 0;
	
	nChkLine= ChkAreaSelLine(&nRealMinSpec, &nRealMaxSpec, &nRealMinWarn, &nRealMaxWarn, nEdgeLoop);

	switch (nChkLine)
	{
		case 0: //ALL
			pOldPen = m_dcGraph.SelectObject(&DrawAllPen);
			m_dcGraph.MoveTo(m_GraphRect.left, nRealMinSpec);
			m_dcGraph.LineTo(m_GraphRect.right, nRealMinSpec);
			m_dcGraph.MoveTo(m_GraphRect.left, nRealMaxSpec);
			m_dcGraph.LineTo(m_GraphRect.right, nRealMaxSpec);
			break;
		case 1: //Same Min
			pOldPen = m_dcGraph.SelectObject(&DrawAllPen);
			m_dcGraph.MoveTo(m_GraphRect.left, nRealMinSpec);
			m_dcGraph.LineTo(m_GraphRect.right, nRealMinSpec);

			pOldPen = m_dcGraph.SelectObject(&DrawPen);
			m_dcGraph.MoveTo(m_GraphRect.left, nRealMaxSpec);
			m_dcGraph.LineTo(m_GraphRect.right, nRealMaxSpec);

			pOldPen = m_dcGraph.SelectObject(&DrawPen2);
			m_dcGraph.MoveTo(m_GraphRect.left, nRealMaxWarn);
			m_dcGraph.LineTo(m_GraphRect.right, nRealMaxWarn);
			break;
		case 2: //Same Max
			pOldPen = m_dcGraph.SelectObject(&DrawPen);
			m_dcGraph.MoveTo(m_GraphRect.left, nRealMinSpec);
			m_dcGraph.LineTo(m_GraphRect.right, nRealMinSpec);

			pOldPen = m_dcGraph.SelectObject(&DrawPen2);
			m_dcGraph.MoveTo(m_GraphRect.left, nRealMinWarn);
			m_dcGraph.LineTo(m_GraphRect.right, nRealMinWarn);

			pOldPen = m_dcGraph.SelectObject(&DrawAllPen);
			m_dcGraph.MoveTo(m_GraphRect.left, nRealMaxSpec);
			m_dcGraph.LineTo(m_GraphRect.right, nRealMaxSpec);
			break;
		case 3:
			pOldPen = m_dcGraph.SelectObject(&DrawPen);
			m_dcGraph.MoveTo(m_GraphRect.left, nRealMinSpec);
			m_dcGraph.LineTo(m_GraphRect.right, nRealMinSpec);
			m_dcGraph.MoveTo(m_GraphRect.left, nRealMaxSpec);
			m_dcGraph.LineTo(m_GraphRect.right, nRealMaxSpec);

			pOldPen = m_dcGraph.SelectObject(&DrawPen2);
			m_dcGraph.MoveTo(m_GraphRect.left, nRealMinWarn);
			m_dcGraph.LineTo(m_GraphRect.right, nRealMinWarn);
			m_dcGraph.MoveTo(m_GraphRect.left, nRealMaxWarn);
			m_dcGraph.LineTo(m_GraphRect.right, nRealMaxWarn);
			break;
	}
}

void CStatsGraph::DisplayOutLine(CDC &dc)
{
	if(FALSE == m_bViewOutLine) return;

	CPen outline_pen(PS_SOLID, 1, m_ColOutLine);
	CPen * pOldPen = dc.SelectObject(&outline_pen);

	dc.MoveTo(m_Rect.left, m_Rect.top);
	dc.LineTo(m_Rect.right-1, m_Rect.top);

	dc.MoveTo(m_Rect.right-1, m_Rect.top);
	dc.LineTo(m_Rect.right-1, m_Rect.bottom-1);

	dc.MoveTo(m_Rect.right-1, m_Rect.bottom-1);
	dc.LineTo(m_Rect.left, m_Rect.bottom-1);

	dc.MoveTo(m_Rect.left, m_Rect.bottom-1);
	dc.LineTo(m_Rect.left, m_Rect.top);

	dc.SelectObject(pOldPen);
}

void CStatsGraph::DisplayXTick(CDC &dc)
{
	CRect rect(m_TextRectX);
	CString str;
	CSize size;
	int nTick;

	dc.SetTextColor(RGB(0,0,0));
	int nMode = dc.SetBkMode(TRANSPARENT);
	
	TEXTMETRIC tm;
	dc.GetTextMetrics(&tm);

	int nW06p = (int)((float)m_TextRectX.Width()*0.06f);

	int nXInterval = (m_TextRectX.Width()-nW06p)/(m_nCol+1);

	for(int i = 1; i < m_nCol+1; i++)
	{
		str.Format(_T("%d"), i);
				
		GetTextExtentPoint32(dc.GetSafeHdc(),str.GetBuffer(),str.GetLength(),&size);
		str.ReleaseBuffer();
		nTick = m_TextRectX.left + (i*nXInterval) + size.cx/2;
		rect.right = nTick;

		if(m_nCol <=  20) dc.DrawText(str, &rect, DT_RIGHT | DT_VCENTER);
		else
		{
			if(i%5 == 0)
			{
				dc.DrawText(str, &rect, DT_RIGHT | DT_VCENTER);
			}

		}
	}

	// 뒤에 Wire NO. 값 표현 giheon 140903
	str.Format(_T("( Day )"));
	rect.right = m_TextRectX.right;

	dc.DrawText(str, &rect, DT_RIGHT | DT_VCENTER);
}

void CStatsGraph::DisplayYTick(CDC &dc)
{
	CRect rect(m_TextRectY);
	CString str;
	int nTick;

	dc.SetTextColor(RGB(0,0,0));
	int nMode = dc.SetBkMode(TRANSPARENT);
	
	TEXTMETRIC tm;
	dc.GetTextMetrics(&tm);

	int nYInterval = m_TextRectY.bottom/(m_nRow+1);
		
	for(int i = 1; i < m_nRow+1; i++)
	{
		if(m_nTest_Type == 0)
		{
			nTick = m_TextRectY.bottom - (i*nYInterval) - (tm.tmHeight/2);
			rect.top = nTick;
			str.Format(_T("%d"), (int)m_fYInterval*i);
			dc.DrawText(str, &rect, DT_RIGHT | DT_VCENTER);
		}
		else if(m_nTest_Type == 1)
		{
			nTick = m_TextRectY.bottom - (i*nYInterval) - (tm.tmHeight/2);
			rect.top = nTick;
			str.Format(_T("%d"), (int)(m_fMinValY + (m_fYInterval*i)));
			dc.DrawText(str, &rect, DT_RIGHT | DT_VCENTER);
		}
	}

	// (㎛) 추가. giheon 140903
	nTick = m_TextRectY.bottom - ((m_nRow+1)*nYInterval) - (tm.tmHeight/2);
	rect.top = nTick;

	str.Format(_T("(개)"));
	dc.DrawText(str, &rect, DT_RIGHT | DT_VCENTER);
}

void CStatsGraph::AddData(GRIDXY * pGrid, float fValue)
{
	float fRealInterval = 0;
	if(0 == m_GraphList.GetCount()) return;
	
	if(m_GraphList.Find(pGrid))
	{	
		if(m_nTest_Type == 0)
		{
			// 정확한 그래프 작업을 위한 Interval 작업. giheon 140903
			fRealInterval = ((m_GraphRect.bottom/(m_nRow + 1))/m_fYInterval);
			// 최종행에서 해당 소수점으로 변경되는 값에 -1값으로 정확하게 표현(해상도 1280X1024)
			//nRealInterval = (int)((m_GraphRect.Height()/(m_nRow + 1))-2);
			int nRealVal = (int)(fValue * fRealInterval);

			if(nRealVal > m_GraphRect.Height())
				nRealVal = m_GraphRect.Height();
				
			if(nRealVal < 0) nRealVal = 0;

			pGrid->data[pGrid->nCount++] = nRealVal;		
			if(MAX_COUNT <= pGrid->nCount) pGrid->nCount = 0;
		}
		else if(m_nTest_Type == 1)
		{
			// 정확한 그래프 작업을 위한 Interval 작업. giheon 140903
			fRealInterval = ((m_GraphRect.bottom/(m_nRow + 1))/m_fYInterval);
			// 최종행에서 해당 소수점으로 변경되는 값에 -1값으로 정확하게 표현(해상도 1280X1024)
			//nRealInterval = (int)((m_GraphRect.Height()/(m_nRow + 1))-2);
			// 최소값을 뺀 값으로 나머지 데이터를 구한다.
			int nRealVal = (int)((fValue - m_fMinValY) * fRealInterval);

			if(nRealVal > m_GraphRect.Height())
				nRealVal = m_GraphRect.Height();
				
			if(nRealVal < 0) nRealVal = 0;

			pGrid->data[pGrid->nCount++] = nRealVal;		
			if(MAX_COUNT <= pGrid->nCount) pGrid->nCount = 0;
		}

		Display();
	}
}

void CStatsGraph::SetBkCol(COLORREF col)
{
	m_ColBack = col;
	Display();
}

void CStatsGraph::SetGridCol(COLORREF col)
{
	m_ColGrid = col;
	Display();
}

void CStatsGraph::SetGridLineCol(GRIDXY * pGrid, COLORREF col)
{
	if(0 == m_GraphList.GetCount()) return;
	
	if(m_GraphList.Find(pGrid))
	{
		pGrid->colGrid = col;
		Display();
	}
}

void CStatsGraph::SetGrid(BOOL bGrid)
{
	m_bGrid = bGrid;
	if(m_bGrid)
	{
		m_bViewGrid_Row = TRUE;
		m_bViewGrid_Col = TRUE;
	}
	else
	{
		m_bViewGrid_Row = FALSE;
		m_bViewGrid_Col = FALSE;
	}
	Display();
}

void CStatsGraph::SetGridLine(GRIDXY * pGrid, BOOL bGrid)
{
	if(0 == m_GraphList.GetCount()) return;
	
	if(m_GraphList.Find(pGrid))
	{
		pGrid->bGrid = bGrid;
		Display();
	}
}

void CStatsGraph::SetMaxValueX(float fMax, float fInterVal)
{
	m_fMaxValX = fMax;
	m_fXInterval = fInterVal;
	m_nCol = (int)(m_fMaxValX/m_fXInterval);
	float fRate = fInterVal/fMax;
	int nW06p = (int)((float)m_GraphRect.Width()*0.06f);
	m_fXInterval = (float)((m_GraphRect.Width()-nW06p)*fRate);

	Display();
}

void CStatsGraph::SetMaxValueY(float fMax, float fInterVal)
{
	m_fMaxValY = fMax;
	m_fYInterval = fInterVal;
	if(m_nTest_Type == 0)
		m_nRow = (int)(m_fMaxValY/m_fYInterval);
	else if(m_nTest_Type == 1)
		m_nRow = (int)((m_fMaxValY - m_fMinValY)/m_fYInterval);


	Display();
}

void CStatsGraph::SetMinValueY(float fMin)
{
	m_fMinValY = fMin;


	Display();
}

void CStatsGraph::SetSampling(int nRate)
{
	m_fXInterval = (float)nRate;
	m_nCol = (int)(m_nWidth/m_fXInterval);
	Display();
}

void CStatsGraph::PreSubclassWindow() 
{	
	CStatic::PreSubclassWindow();

	//Initialize();
}

void CStatsGraph::SetOffAreaLine(BOOL bEdgeLoop, BOOL bAreaOff)
{
	if(bEdgeLoop)
		m_bEdgeSpecWarn = bAreaOff;
	else
		m_bLoopSpecWarn = bAreaOff;

	Display();
}