#pragma once


// CBorderStyleEdit

#define WM_EDIT_MODIFIED		(WM_USER+0xED)

class CBorderStyleEdit : public CEdit
{
	DECLARE_DYNAMIC(CBorderStyleEdit)
protected:
	CBrush	m_BgBrush;
	CFont	m_fntEdit;
	COLORREF	m_dwTextColor;
	COLORREF	m_dwActiveTextColor;
	COLORREF	m_dwBorderColor;
	COLORREF	m_dwBgColor;

	BOOL		m_bShowBorder;
	UINT		m_nBorderWidth;

	BOOL		m_bModified;
	double		m_dOldValue;

	BOOL		m_bIsNumeric;
	BOOL		m_bFloatStyle;

	double		m_dValueRangeMin;
	double		m_dValueRangeMax;

	BOOL		m_bNowEditing;

	CString		m_strAllowString;

	CString		m_strOldValue;

	UINT_PTR	m_nWarningTimer;
	BOOL		m_bBlink;
	BOOL		m_bWarning;

	void DrawEditFrame( CDC* pDC );

public:
	CBorderStyleEdit();
	virtual ~CBorderStyleEdit();

	void SetFontStyle( LPCTSTR lpszFace, int nFontHeight, int nWeight );
	void SetColor( COLORREF dwBorderColor, COLORREF dwBgColor );
	void SetNumeric( BOOL bNumeric );
	void SetFloatStyle( BOOL bFloatStyle );

	void SetWarning( BOOL bWarning );

	double GetOldValue() const { return m_dOldValue; }

protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg HBRUSH CtlColor(CDC* /*pDC*/, UINT /*nCtlColor*/);
	afx_msg void OnPaint();
	afx_msg void OnKillfocus();
	afx_msg void OnSetfocus();

	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnTimer(UINT nIDEvent);
};


