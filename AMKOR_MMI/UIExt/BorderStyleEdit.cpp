// BorderStyleEdit.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "BorderStyleEdit.h"

#include <float.h>

#define		ID_TIMER_WARNING		(100)

// CBorderStyleEdit

IMPLEMENT_DYNAMIC(CBorderStyleEdit, CEdit)
CBorderStyleEdit::CBorderStyleEdit() : m_dwBorderColor( RGB(150, 150, 150) ),
					 m_dwTextColor( RGB(0, 0, 0) ),
					 m_dwActiveTextColor( RGB(255, 0, 0) ),
					 m_dwBgColor( RGB(240, 240, 240) )

{
	m_BgBrush.CreateSolidBrush( m_dwBgColor );
	m_bShowBorder = FALSE;
	m_nBorderWidth = 2;

	m_bNowEditing = FALSE;

	m_dValueRangeMin = -DBL_MAX;
	m_dValueRangeMax = +DBL_MAX;
	m_strAllowString = _T("0123456789.-");
	m_bFloatStyle = TRUE;

	m_bModified = FALSE;

	m_nWarningTimer = 0;
	m_bBlink = FALSE;
	m_bWarning = FALSE;

	m_bIsNumeric = FALSE;
}

CBorderStyleEdit::~CBorderStyleEdit()
{
	// dhaps2000
	m_BgBrush.DeleteObject();
}


BEGIN_MESSAGE_MAP(CBorderStyleEdit, CEdit)
	ON_WM_CTLCOLOR_REFLECT()
	ON_WM_PAINT()
	ON_CONTROL_REFLECT(EN_KILLFOCUS, OnKillfocus)
	ON_CONTROL_REFLECT(EN_SETFOCUS, OnSetfocus)
	ON_WM_TIMER()
END_MESSAGE_MAP()

void CBorderStyleEdit::SetNumeric( BOOL bNumeric )
{
	m_bIsNumeric = bNumeric;
}

void CBorderStyleEdit::SetFloatStyle( BOOL bFloatStyle )
{
	m_bFloatStyle = bFloatStyle;
	if( bFloatStyle )
		m_strAllowString = _T("0123456789.-");
	else
		m_strAllowString = _T("0123456789-");
}

HBRUSH CBorderStyleEdit::CtlColor(CDC* pDC, UINT /*nCtlColor*/)
{
	DrawEditFrame( pDC );

	return m_BgBrush;
}

void CBorderStyleEdit::OnPaint()
{
	CEdit::OnPaint();
}

void CBorderStyleEdit::SetFontStyle( LPCTSTR lpszFace, int nFontHeight, int nWeight )
{
	if( m_fntEdit.GetSafeHandle() )
		m_fntEdit.DeleteObject();

	LOGFONT  lgFont;

	CRect rcClient;
	GetClientRect( rcClient );

	int nCHeight = rcClient.Height();
	if( nCHeight <= 0 ) nCHeight = 10;

	lgFont.lfCharSet = DEFAULT_CHARSET;
	lgFont.lfClipPrecision = 0;
	lgFont.lfEscapement = 0;
	_tcscpy( lgFont.lfFaceName, lpszFace );
	lgFont.lfHeight = nFontHeight<0 ? nCHeight : nFontHeight;
	lgFont.lfItalic = FALSE;
	lgFont.lfOrientation = 0;
	lgFont.lfOutPrecision = 0;
	lgFont.lfPitchAndFamily = 2;
	lgFont.lfQuality = 0;
	lgFont.lfStrikeOut = 0;
	lgFont.lfUnderline = FALSE;
	lgFont.lfWidth = 0;
	lgFont.lfWeight = nWeight;

	m_fntEdit.CreateFontIndirect( &lgFont );

	SetFont( &m_fntEdit );
}

void CBorderStyleEdit::SetColor( COLORREF dwBorderColor, COLORREF dwBgColor )
{
	m_dwBgColor = dwBgColor;
	m_dwBorderColor = dwBorderColor;
	if( m_BgBrush.GetSafeHandle() )
		m_BgBrush.DeleteObject();

	m_BgBrush.CreateSolidBrush( dwBgColor );

	if( IsWindow(GetSafeHwnd()) )
		RedrawWindow();
}

void CBorderStyleEdit::DrawEditFrame( CDC* pDC )
{	
	CRect rcItem;
	GetClientRect( &rcItem );

	if( m_bNowEditing )
		pDC->SetTextColor( m_dwActiveTextColor );
	else
		pDC->SetTextColor( m_dwTextColor );

	pDC->SetBkColor( m_dwBgColor );

	if( m_bShowBorder )
	{
		DWORD dwBorderColor = m_dwBorderColor;
		if( m_bWarning )
		{
			dwBorderColor = (m_bBlink == TRUE ? RGB(255, 0, 0) : m_dwBorderColor);
		}

		rcItem.InflateRect( 1, 1, 1, 1 );
		CBrush* pOldBrush = (CBrush*)pDC->SelectStockObject( NULL_BRUSH );
		CPen BorderPen;
		BorderPen.CreatePen( PS_SOLID, m_nBorderWidth, dwBorderColor );
		CPen* pOldPen = pDC->SelectObject( &BorderPen );

		pDC->Rectangle( rcItem );

		pDC->SelectObject( pOldPen );
		pDC->SelectObject( pOldBrush );
	}
}

void CBorderStyleEdit::OnKillfocus() 
{
	CString strValue;
	GetWindowText( strValue );
	if( strValue.IsEmpty() )
	{
		if(m_bIsNumeric)
		{
			if( m_bFloatStyle )
			{
				SetWindowText( _T("0.00") );
				strValue = _T("0.00");
			}
			else
			{
				SetWindowText( _T("0") );
				strValue = _T("0");
			}
		}
	}

	double dValue = 0.0f;
	if(m_bIsNumeric)
	{	
		dValue = _tcstod( strValue, NULL );
		if( dValue < m_dValueRangeMin ) dValue = m_dValueRangeMin;
		if( dValue > m_dValueRangeMax ) dValue = m_dValueRangeMax;

		if( m_bFloatStyle )
			strValue.Format( _T("%.3f"), dValue );
		else
			strValue.Format( _T("%d"), (int)dValue );

		if( m_dOldValue != dValue )
		{
			NMHDR hdr;
			hdr.hwndFrom = m_hWnd;
			hdr.idFrom = (UINT)GetDlgCtrlID();
			hdr.code = WM_EDIT_MODIFIED;
			GetParent()->SendMessage(WM_NOTIFY, GetDlgCtrlID(), (LPARAM)&hdr);
			//GetParent()->SendMessage( WM_EDIT_MODIFIED, (WPARAM)GetDlgCtrlID(), (LPARAM)&dValue );
		}
	}
	else
	{
		if( m_strOldValue != strValue )
		{
			NMHDR hdr;
			hdr.hwndFrom = m_hWnd;
			hdr.idFrom = (UINT)GetDlgCtrlID();
			hdr.code = WM_EDIT_MODIFIED;
			GetParent()->SendMessage(WM_NOTIFY, GetDlgCtrlID(), (LPARAM)&hdr);
			//GetParent()->SendMessage( WM_EDIT_MODIFIED, (WPARAM)GetDlgCtrlID(), (LPARAM)&dValue );
		}
	}
	SetWindowText( strValue );

	m_bNowEditing = FALSE;
	RedrawWindow();
}

void CBorderStyleEdit::OnSetfocus() 
{
	m_bNowEditing = TRUE;
	m_bModified = FALSE;

	CString strValue;
	GetWindowText( strValue );

	if(m_bIsNumeric)
	{
		if( strValue.IsEmpty() )
		{
			if( m_bFloatStyle )
			{
				SetWindowText( _T("0.000") );
				strValue = _T("0.000");
			}
			else
			{
				SetWindowText( _T("0") );
				strValue = _T("0");
			}
		}

		m_dOldValue = _tcstod( strValue, NULL );
	}
	else
		m_strOldValue = strValue;

	RedrawWindow();
}

BOOL CBorderStyleEdit::PreTranslateMessage(MSG* pMsg)
{
	if( pMsg->message == WM_CHAR )
	{
		if(m_bIsNumeric)
		{
			if( !m_strAllowString.IsEmpty() )
			{
				TCHAR chCharCode = (TCHAR)pMsg->wParam;
				if( chCharCode == VK_BACK )
					return CEdit::PreTranslateMessage(pMsg);
				if( m_strAllowString.Find( chCharCode ) < 0 )
				{
					return TRUE;
				}
			}
		}
	}

	return CEdit::PreTranslateMessage(pMsg);
}

void CBorderStyleEdit::SetWarning( BOOL bWarning )
{
	m_bWarning = bWarning;
	if( m_bWarning )
	{
		if( m_nWarningTimer )
			KillTimer( ID_TIMER_WARNING );
		SetTimer( ID_TIMER_WARNING, 500, NULL );
	}
	else
	{
		if( m_nWarningTimer )
			KillTimer( ID_TIMER_WARNING );
		m_nWarningTimer = 0;
	}
}

void CBorderStyleEdit::OnTimer(UINT nIDEvent)
{
	if( nIDEvent == ID_TIMER_WARNING )
	{
		m_bBlink = m_bBlink ? FALSE : TRUE;
		RedrawWindow();
	}

	CEdit::OnTimer(nIDEvent);
}
