#pragma once

const int TIPBGRCOLOR = RGB ( 255, 255, 255 );

class CIconButton : public CButton
{
public:
	CIconButton();
	virtual ~CIconButton();

	void SetItalic ( bool bVal = true )				{ m_TextMetrics.tmItalic = bVal; NewFont (); };
	void SetUnderline ( bool bVal = true )			{ m_TextMetrics.tmUnderlined = bVal; NewFont (); };
	void SetStrikeOut ( bool bVal = true )			{ m_TextMetrics.tmStruckOut = bVal; NewFont (); };
	void SetFaceName ( const CString &sVal )		{ m_sFaceName = sVal; NewFont (); };
	void SetWeight ( const int nVal )				{ m_TextMetrics.tmWeight = nVal; NewFont (); };
	void SetHeight ( const int nVal)				{ m_TextMetrics.tmHeight = nVal; NewFont (); };
	void SetWidth ( const int nVal)					{ m_TextMetrics.tmMaxCharWidth = nVal; NewFont (); };
	void SetFontBtn(const CString &FontName, const int nSize, const int nWeight) { m_sFaceName = FontName; m_TextMetrics.tmHeight = nSize; m_TextMetrics.tmWeight = nWeight; NewFont (); };
	
	void SetIconID ( const UINT nID );
	void SetTextColor ( const COLORREF color )		{ m_nColor = color; };
	void SetIconSize ( const int x, const int y );
	void SetIconRight ( bool bVal = true )			{ m_bIconRight = bVal; };
	void SetIconPos ( int nVal = 0 )				{ m_nIconPos = nVal; };
	void Disable ( void );
	void Enable ( void );
	void SetTipText ( const CString &sTxt )			{ m_sTip = sTxt; m_bWithTip = true; };
	//20131112 dhaps2000 - 버튼칼라 추가
	void SetButtonColor( const COLORREF def_color, const COLORREF sel_color) { m_nBtnColor_BK = def_color; m_nBtnColor_SEL = sel_color; };
	void SetDodgeImage(bool bDodge) { m_bDodgeImage = bDodge; };
	//20131113 dhaps2000 - 버튼 온 오프 스타일 추가
	void SetOnOffStyle() { m_bOnOffStyle = true; };
	void SetOnOff(bool bOnOff) { m_bOnOff = bOnOff; };
	void SetOnOff(int nOnOff) { if(nOnOff != 0) m_bOnOff = true; else m_bOnOff = false; };
	bool GetOnOff() { return m_bOnOff; };
	void SetEnable(bool bEnable);
	//20131202 dhaps2000 - LAMP
	void SetLampOnOff(BOOL bOnOff);
	BOOL IsLampState() { return m_bLampOnOff; };
	//20140317 dhaps2000 - Flat TYpe
	void SetFlatType(BOOL bFlat) { m_bFlatType = bFlat; };
	BOOL GetFlatType() { return m_bFlatType; };
	// dhaps2000 - Beep
	void SetUseBeep(BOOL bBeep);

	//{{AFX_VIRTUAL(CIconButton)
	public:
	virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);
	//}}AFX_VIRTUAL


protected:
	//{{AFX_MSG(CIconButton)
	afx_msg LRESULT OnNcHitTest(CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	enum LB_HORZ_ALIGN { ALIGN_LEFT, ALIGN_RIGHT, ALIGN_CENTER };
	enum LB_VERT_ALIGN { ALIGN_TOP, ALIGN_BOTTOM, ALIGN_VCENTER };

	LB_HORZ_ALIGN GetHorzAlign( DWORD dwStyle );
	LB_VERT_ALIGN GetVertAlign( DWORD dwStyle );
	BOOL IsMultiLine( DWORD dwStyle );

	void	NewFont ( void );
	int		DelAmpersand ( CString &sTxt );
	void	HideTip ( void );
	void	ShowTip ( void );
	void	GetCurrentTextMetric ( void );
	void	DrawTipBoxAndText ( void );
	bool	RestoreBitmap ( void );
	bool	SaveBitmap ( void );
	//
	bool			m_bIconRight;
	int				m_nIconPos;	// 0: LEFT, 1: RIGHT, 2: TOP, 3: BOTTOM
	TEXTMETRIC		m_TextMetrics;
	CSize			m_nIcon;
	UINT			m_nIconID;
	COLORREF		m_nColor;
	CFont			m_fnt;
	CString			m_sFaceName;
	//
	bool			m_bWithTip;
	bool			m_bTipIsOpen;
	CString			m_sTip;
	CSize			m_TipDim;
	CPoint			m_TipPos;
	CRect			m_TipRect;
	HBITMAP			m_hMemBmp;
	HDC				m_hMemDC;
	HDC				m_hOwnerDC;
	//20131112 dhaps2000 - 버튼칼라 추가
	COLORREF		m_nBtnColor_BK; // 디폴트 배경색깔
	COLORREF		m_nBtnColor_SEL;// 선택시 배경색깔
	bool			m_bDodgeImage; // 아이콘 이미지를 버튼크기에 맞게 설정여부
	//20131113 dhaps2000 - 버튼 온 오프 스타일 추가
	bool			m_bOnOffStyle; // 버튼을 온/오프 상태로만 설정
	bool			m_bOnOff; // 버튼스타일이 온/오프 일때 값 설정
	//20131202 dhaps2000 - LAMP
	// LAMP ATTRIBUTES
	BOOL			m_bLampOnOff;
	BOOL			m_bLampBlink;
	//20140317 dhaps2000 - Flat Type
	BOOL			m_bFlatType;
	//dhaps2000
	HICON			m_hIcon;
	//dhaps2000
	BOOL			m_bButtonPressed;
	BOOL			m_bUseBeep;

	void DrawItem_Normal(LPDRAWITEMSTRUCT lpDrawItemStruct);
	void DrawItem_OnOff(LPDRAWITEMSTRUCT lpDrawItemStruct);
	void DrawItem_Lamp(LPDRAWITEMSTRUCT lpDrawItemStruct);
};
