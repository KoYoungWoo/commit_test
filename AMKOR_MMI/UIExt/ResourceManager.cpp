#include "stdafx.h"
#include "ResourceManager.h"

using namespace UIExt;

CResourceManager::CResourceManager(void)
{
	NONCLIENTMETRICS ncm = { sizeof(NONCLIENTMETRICS) };
	SystemParametersInfo(SPI_GETNONCLIENTMETRICS, 0, &ncm, 0);

	USES_CONVERSION;

	WCHAR* wszFaceName;
	CString strX = ncm.lfMessageFont.lfFaceName;
	if ( strX.IsEmpty() )
		wszFaceName = L"Arial";
	else
		wszFaceName = (WCHAR*)T2W(ncm.lfMessageFont.lfFaceName);

	m_pFontFamilySystem = ::new Gdiplus::FontFamily( wszFaceName );

}

CResourceManager::~CResourceManager(void)
{
	if ( m_pFontFamilySystem )
	{
		::delete m_pFontFamilySystem;
		m_pFontFamilySystem = NULL;
	}
}

Gdiplus::FontFamily* CResourceManager::SystemFontFamily()
{
	return CResourceManager::Instance()->m_pFontFamilySystem;
}

Gdiplus::FontFamily* CResourceManager::GetSystemFontFamily()
{
	return m_pFontFamilySystem;
}
