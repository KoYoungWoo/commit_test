/********************************************************************
	created:	2005/06/03
	created:	3:6:2005   13:22
	filename: 	x:\Software\Mfc\Source\Controls\Buttons\RoundButton2.cpp
	file path:	x:\Software\Mfc\Source\Controls\Buttons
	file base:	RoundButton2
	file ext:	cpp
	author:		Markus Zocholl
	
	purpose:	CRoundButton defines a universal Button-Control with the 
				following features:

				* Shape is a rounded Rectangle
				* Button includes Border and Button-Face
				* Many parameters to get an individual look
				* Functions of Button to be en- or disabled:
					- Button (disabled means a static control with userdefined styles)
					- Hover
*********************************************************************/

#include "StdAfx.h"
//#include "resource.h"
#include <math.h>
#include "RoundButton.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/************************************************************************/
/* Construction and Destruction                                         */
/************************************************************************/

#pragma warning( disable : 4996 )

//! Construction
CRoundButton::CRoundButton(void):
	  m_bDefaultButton(false)
	, m_bIsCheckButton(false)
	, m_bIsRadioButton(false)
	, m_bIsHotButton(false)
	, m_bMouseOnButton(false)
	, m_bIsChecked(false)	
	, m_ptRoundButtonStyle(NULL)
	, m_rBtnSize(CRect(0, 0, 0, 0))
	, m_bRedraw(false)
	, m_sOldCaption(_T(""))
	  {
	// Set Standards in Font-Style
	m_tLogFont.lfHeight			= 16;
	m_tLogFont.lfWidth			= 0;
	m_tLogFont.lfEscapement		= 0;
	m_tLogFont.lfOrientation	= 0;
	m_tLogFont.lfWeight			= FW_BOLD;
	m_tLogFont.lfItalic			= false;
	m_tLogFont.lfUnderline		= false;
	m_tLogFont.lfStrikeOut		= false;
	m_tLogFont.lfCharSet		= DEFAULT_CHARSET;
	m_tLogFont.lfOutPrecision	= OUT_DEFAULT_PRECIS;
	m_tLogFont.lfClipPrecision	= CLIP_DEFAULT_PRECIS;
	m_tLogFont.lfQuality		= ANTIALIASED_QUALITY;
	m_tLogFont.lfPitchAndFamily	= DEFAULT_PITCH;

#ifdef UNICODE
	wcscpy(m_tLogFont.lfFaceName, _T("Tahoma"));
#else
	strcpy_s(m_tLogFont.lfFaceName, _T("Tahoma"));
#endif

	m_tBtnFont.CreateFontIndirect(&m_tLogFont);

	// Set Standard Font-Color
	m_tTextColor.m_tDisabled	= RGB(64, 64, 64);
	m_tTextColor.m_tEnabled		= RGB( 0,  0,  0);
	m_tTextColor.m_tClicked		= RGB( 0,  0,  0);
	m_tTextColor.m_tPressed		= RGB( 0,  0,  0);
	m_tTextColor.m_tHot			= RGB( 0,  0,  0);
	
	ZeroMemory(&m_stIcon, sizeof(STRUCT_ICONS));
	
	m_iDivFactor_X	= 1;
	m_iDivFactor_Y	= 1;
	m_iPosTxt_X		= 1;
	m_iPosTxt_Y		= 1;

	m_bMultiText	= false;
	m_bButtonLDown = false; // YTH 수정 2010.03.30
}

//! Destruction
CRoundButton::~CRoundButton(void)
{
}

/************************************************************************/
/* public Functions                                                     */
/************************************************************************/

// Set Style of Button
bool CRoundButton::SetRoundButtonStyle(CRoundButtonStyle* _ptRoundButtonStyle)
{
	// Check, if Button-Style is given
	if (_ptRoundButtonStyle == NULL)
		return false;

	// Set Pointer to ButtonStyle
	m_ptRoundButtonStyle = _ptRoundButtonStyle;

	// Redraw Button
	m_bRedraw = true;

	// All Done
	return false;
}

// Set Font of Button
bool CRoundButton::SetFont(LOGFONT* _ptLogFont)
{
	if (_ptLogFont == NULL)
		return false;

	// Delete Font, if already given
	if (m_tBtnFont.m_hObject != NULL)
		m_tBtnFont.DeleteObject();

	// Store Infos local
	memcpy(&m_tLogFont, _ptLogFont, sizeof(LOGFONT));

	// Create new Font
	m_tBtnFont.CreateFontIndirect(&m_tLogFont);

	// Button should be redrawn
	m_bRedraw = true;

	return true;
}

bool CRoundButton::SetFont(CFont* _pFont)
{
	if(_pFont == NULL)
		return FALSE;

	LOGFONT lf;

	_pFont->GetLogFont(&lf);

	SetFont(&lf);

	return TRUE;
}

void CRoundButton::SetFont(const CString& strFont,int nFontSize, int nWeight)
{
	LOGFONT lf;
	memset(&lf, 0, sizeof(LOGFONT));		// Zero out the structure.

	_tcscpy(lf.lfFaceName,strFont);			// Set Font Familly
	lf.lfHeight = nFontSize;				// Set Height
	lf.lfWeight = nWeight;					// Set Weight

	SetFont(&lf);
}

// Set Font of Button
bool CRoundButton::GetFont(LOGFONT* _ptLogFont)
{
	if (_ptLogFont == NULL)
		return false;

	// Store Infos local
	memcpy(_ptLogFont, &m_tLogFont, sizeof(LOGFONT));

	return true;
}

//! Set Color of Caption
bool CRoundButton::SetTextColor(tColorScheme* _ptTextColor)
{
	if (_ptTextColor == NULL)
		return false;

	// Store Infos locally
	memcpy(&m_tTextColor, _ptTextColor, sizeof(tColorScheme));

	// Button should be redrawn
	m_bRedraw = true;

	return true;
}

bool CRoundButton::SetTextColor(COLORREF crTextColor)
{
	// Store Infos locally
	m_tTextColor.m_tEnabled		= crTextColor;
	m_tTextColor.m_tHot			= crTextColor;
	m_tTextColor.m_tClicked		= crTextColor;
	m_tTextColor.m_tPressed		= crTextColor;
	m_tTextColor.m_tDisabled	= crTextColor;
	// Button should be redrawn
	m_bRedraw = true;

	return true;
}

//! Get Color of Caption
bool CRoundButton::GetTextColor(tColorScheme* _ptTextColor)
{
	if (_ptTextColor == NULL)
		return false;

	// Store Infos locally
	memcpy(_ptTextColor, &m_tTextColor, sizeof(tColorScheme));

	return true;
}

/************************************************************************/
/* Own Drawing-Functions                                                */
/************************************************************************/

//! Generate Bitmaps to hold Buttons
void CRoundButton::GenButtonBMPs(CDC* _pDC, CRect _rRect)
{
	if (m_tBmpBtn.m_hObject != NULL)
		m_tBmpBtn.DeleteObject();
	m_tBmpBtn.m_hObject = NULL;
	// Generate Bitmap
	if (m_tBmpBtn.CreateCompatibleBitmap(_pDC, _rRect.Width(), _rRect.Height() * BS_LAST_STATE) == FALSE)
	{
		m_rBtnSize = CRect(0, 0, 0, 0);
	}
	else
	{
		m_rBtnSize = _rRect;
	}
}

//! Draw Button-Face
void CRoundButton::DrawButtonFace(CDC* _pDC)
{
	// We need an attached style
	if (m_ptRoundButtonStyle == NULL)
		return;

	// Get Pointer to Bitmap of Masks
	CBitmap* pButtonMasks = m_ptRoundButtonStyle->GetButtonEdge(_pDC);

	// Create Memory-DC
	CDC SourceDC;
	SourceDC.CreateCompatibleDC(_pDC);

	// Select Working Objects into DCs
	HGDIOBJ hOldBmp1 = SourceDC.SelectObject(pButtonMasks);

	int nState;

	CSize tEdgeSize = m_ptRoundButtonStyle->GetEdgeSize();
	CSize tCorrectedEdgeSize;
	CSize tMaskSize = m_ptRoundButtonStyle->GetMaskSize();

	// Correct Edge-Size for smaller Buttons
	tCorrectedEdgeSize.cx = __min(tEdgeSize.cx, __min(m_rBtnSize.Width() / 2, m_rBtnSize.Height() / 2));
	tCorrectedEdgeSize.cy = tCorrectedEdgeSize.cx;

	for (nState = 0; nState < BS_LAST_STATE; nState++)
	{
		/************************************************************************/
		/* Draw Edges                                                           */
		/************************************************************************/
		// Left-Top
		_pDC->StretchBlt(
			0, 
			nState * m_rBtnSize.Height(), 
			tCorrectedEdgeSize.cx, 
			tCorrectedEdgeSize.cy, 
			&SourceDC, 
			0, 
			nState * tMaskSize.cy,
			tEdgeSize.cx,
			tEdgeSize.cy,
			SRCCOPY);
		// Left-Bottom
		_pDC->StretchBlt(
			0, 
			nState * m_rBtnSize.Height() + m_rBtnSize.Height() - tCorrectedEdgeSize.cy, 
			tCorrectedEdgeSize.cx, 
			tCorrectedEdgeSize.cy, 
			&SourceDC, 
			0, 
			nState * tMaskSize.cy + tMaskSize.cy - tEdgeSize.cy, 
			tEdgeSize.cx,
			tEdgeSize.cy,
			SRCCOPY);
		// Right-Top
		_pDC->StretchBlt(
			m_rBtnSize.Width() - tCorrectedEdgeSize.cx, 
			nState * m_rBtnSize.Height(), 
			tCorrectedEdgeSize.cx, 
			tCorrectedEdgeSize.cy, 
			&SourceDC, 
			tMaskSize.cx - tEdgeSize.cx, 
			nState * tMaskSize.cy, 
			tEdgeSize.cx,
			tEdgeSize.cy,
			SRCCOPY);
		// Right-Bottom
		_pDC->StretchBlt(
			m_rBtnSize.Width() - tCorrectedEdgeSize.cx, 
			nState * m_rBtnSize.Height() + m_rBtnSize.Height() - tCorrectedEdgeSize.cy, 
			tCorrectedEdgeSize.cx, 
			tCorrectedEdgeSize.cy, 
			&SourceDC, 
			tMaskSize.cx - tEdgeSize.cx, 
			nState * tMaskSize.cy + tMaskSize.cy - tEdgeSize.cy, 
			tEdgeSize.cx,
			tEdgeSize.cy,
			SRCCOPY);
		/************************************************************************/
		/* Draw Sides                                                           */
		/************************************************************************/
		// Top
		_pDC->StretchBlt(
			tCorrectedEdgeSize.cx, 
			nState * m_rBtnSize.Height(),
			m_rBtnSize.Width() - 2 * tCorrectedEdgeSize.cx,
			tCorrectedEdgeSize.cy,
			&SourceDC,
			tEdgeSize.cx,
			nState * tMaskSize.cy,
			1,
			tEdgeSize.cy,
			SRCCOPY);
		// Bottom
		_pDC->StretchBlt(
			tCorrectedEdgeSize.cx, 
			nState * m_rBtnSize.Height() + m_rBtnSize.Height() - tCorrectedEdgeSize.cy,
			m_rBtnSize.Width() - 2 * tCorrectedEdgeSize.cx,
			tCorrectedEdgeSize.cy,
			&SourceDC,
			tEdgeSize.cx,
			nState * tMaskSize.cy + tMaskSize.cy - tEdgeSize.cy,
			1,
			tEdgeSize.cy,
			SRCCOPY);
		// Left
		_pDC->StretchBlt(
			0, 
			nState * m_rBtnSize.Height() + tCorrectedEdgeSize.cy,
			tCorrectedEdgeSize.cx,
			m_rBtnSize.Height() - 2 * tCorrectedEdgeSize.cy,
			&SourceDC,
			0,
			nState * tMaskSize.cy + tEdgeSize.cy,
			tEdgeSize.cx,
			1,
			SRCCOPY);
		// Right
		_pDC->StretchBlt(
			m_rBtnSize.Width() - tCorrectedEdgeSize.cx, 
			nState * m_rBtnSize.Height() + tCorrectedEdgeSize.cy,
			tCorrectedEdgeSize.cx,
			m_rBtnSize.Height() - 2 * tCorrectedEdgeSize.cy,
			&SourceDC,
			tMaskSize.cx - tEdgeSize.cx,
			nState * tMaskSize.cy + tEdgeSize.cy,
			tEdgeSize.cx,
			1,
			SRCCOPY);
		/************************************************************************/
		/* Filling                                                              */
		/************************************************************************/
		_pDC->StretchBlt(
			tCorrectedEdgeSize.cx,
			nState * m_rBtnSize.Height() + tCorrectedEdgeSize.cy,
			m_rBtnSize.Width() - 2* tCorrectedEdgeSize.cx,
			m_rBtnSize.Height() - 2 * tCorrectedEdgeSize.cy,
			&SourceDC,
			tEdgeSize.cx,
			nState * tMaskSize.cy + tEdgeSize.cy,
			1,
			1,
			SRCCOPY);
	}

	// Select Old Objects into DCs
//	SourceDC.SelectObject(hOldBmp1);
}

void CRoundButton::DrawButtonIcon(CDC *_pDC, RECT* pRect)
{
	CRect	BtnRect;
	UINT	iFlag;

	BtnRect.CopyRect(pRect);

	if(m_iAlign == ST_ALIGN_HORIZ)
	{
		BtnRect.left = (BtnRect.Width() / m_iDivFactor_X);
		BtnRect.top += ((BtnRect.Height() - m_stIcon.dwHeight)/2);
	}
	else if(m_iAlign == ST_ALIGN_VERT)
	{
		BtnRect.left = ((BtnRect.Width() - m_stIcon.dwWidth) / 2);
		BtnRect.top  = (BtnRect.Height() / m_iDivFactor_Y);
	}

//	if(m_iBtnState == BS_ENABLED || m_iBtnState == BS_HOT || m_iBtnState == BS_PRESSED)
//		iFlag = DSS_NORMAL;
//	else
//		iFlag = DSS_DISABLED;
	iFlag = DSS_NORMAL;

	_pDC->DrawState(BtnRect.TopLeft(), BtnRect.Size(), m_stIcon.hIcon, iFlag, (CBrush*)NULL);
}
 
//! Draw Caption on Button
void CRoundButton::DrawButtonCaption(CDC *_pDC, CRect rect)
{
	// Select Transparency for Background
	int nOldBckMode = _pDC->SetBkMode(TRANSPARENT);

	// Get old Text-Color
	COLORREF tOldColor = _pDC->SetTextColor(RGB(0,0,0));

	// Select Font into DC
	HGDIOBJ hOldFont = _pDC->SelectObject(&m_tBtnFont);

	// Get Caption of Button
	CString sCaption;
	this->GetWindowText(sCaption);

	for (int nState = 0; nState < BS_LAST_STATE; nState++)
	{
		switch(nState)
		{
		case BS_ENABLED:
			_pDC->SetTextColor(m_tTextColor.m_tEnabled);
			break;
		case BS_CLICKED:
			_pDC->SetTextColor(m_tTextColor.m_tClicked);
			break;
		case BS_PRESSED:
			_pDC->SetTextColor(m_tTextColor.m_tPressed);
			break;
		case BS_HOT:
			_pDC->SetTextColor(m_tTextColor.m_tHot);
			break;
		case BS_DISABLED:
		default:
			_pDC->SetTextColor(m_tTextColor.m_tDisabled);
			break;
		}

		CRect rect(	m_rBtnSize.left,
					m_rBtnSize.Height() * nState + m_rBtnSize.top, 
					m_rBtnSize.right,
					m_rBtnSize.Height() * nState + m_rBtnSize.bottom );

		if(m_stIcon.hIcon != NULL && m_iAlign == ST_ALIGN_HORIZ)
			rect.left	+= m_iPosTxt_X;
		else if(m_stIcon.hIcon != NULL && m_iAlign == ST_ALIGN_VERT)
			rect.top	+= m_iPosTxt_Y;

		if( m_bMultiText )
		{
			_pDC->DrawText(sCaption, rect, DT_CENTER | DT_VCENTER);
		}
		else//PHIL
		{
			_pDC->DrawText(sCaption, rect, DT_LEFT | DT_VCENTER | DT_SINGLELINE);
		}
	}

	// Select Old Font back
//	_pDC->SelectObject(hOldFont);

	// Set old Background-Mode
	_pDC->SetBkMode(nOldBckMode);

	// Set old Text-Color
	_pDC->SetTextColor(tOldColor);
}

/************************************************************************/
/* Overwritten Functions for Init and Draw of Button                    */
/************************************************************************/

//! Presubclass-Window-Function
void CRoundButton::PreSubclassWindow()
{
#ifdef _DEBUG
	// We really should be only sub classing a button control
	TCHAR buffer[255];
	GetClassName (m_hWnd, buffer, sizeof(buffer) / sizeof(TCHAR));
	ASSERT (CString (buffer) == _T("Button"));
#endif

	// Check if it's a default button
	if (GetStyle() & 0x0FL)
		m_bDefaultButton = true;

	// Make the button owner-drawn
	ModifyStyle (0x0FL, BS_OWNERDRAW | BS_AUTOCHECKBOX | BS_MULTILINE, SWP_FRAMECHANGED);

	CButton::PreSubclassWindow();
}

//! Draw-Item-Function
/*! This Function is called each time, the Button needs a redraw
*/
void CRoundButton::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct)
{
	DrawButton(lpDrawItemStruct);
}

void CRoundButton::DrawButton(LPDRAWITEMSTRUCT lpDrawItemStruct)
{
	// Get DC of Item
	CDC* pDC = CDC::FromHandle(lpDrawItemStruct->hDC);
	ASSERT (pDC != NULL);

	// Should Buttons be generated?
	bool bGenerate = !m_rBtnSize.EqualRect(&lpDrawItemStruct->rcItem) || m_bRedraw;

	// If Rectangles of Button are not the same
	if (bGenerate)
	{
		// Generate Bitmap to hold Buttons
		GenButtonBMPs(pDC, lpDrawItemStruct->rcItem);

		// Redraw done
		m_bRedraw = false;
	}

	// Generate DC to draw in Memory
	CDC MemDC;
	MemDC.CreateCompatibleDC(pDC);

	HGDIOBJ hOldBmp = MemDC.SelectObject(m_tBmpBtn);

	CString sActualCaption;
	// Get actual caption
	GetWindowText(sActualCaption);

	// Check, if caption has changed
	if (sActualCaption != m_sOldCaption)
		bGenerate = true;

	// Store old caption
	m_sOldCaption = sActualCaption;

	CRect captionRect = lpDrawItemStruct->rcItem;
	// If Rectangles of Button are not the same
	if (bGenerate)
	{
		// Draw Buttons
		DrawButtonFace(&MemDC);
		// Draw Button-Caption
		DrawButtonCaption(&MemDC, captionRect);
		// Draw Button-Icon
	}

	m_iBtnState = BS_ENABLED;

	if (m_bIsHotButton && m_bMouseOnButton)
	{
		m_iBtnState = BS_HOT;
	}

	if ((lpDrawItemStruct->itemState & ODS_DISABLED) == ODS_DISABLED)
	{
		m_iBtnState = BS_DISABLED;
	}
	else
	{		
		if ((lpDrawItemStruct->itemState & ODS_SELECTED) == ODS_SELECTED)
		{
			// Mouse Down
			m_iBtnState = BS_PRESSED;
		}
		else
		{
			// Mouse Up
			if(m_bIsChecked)
			{
				m_iBtnState = BS_CLICKED;
			}
		}
	}
	
	// Copy correct Bitmap to Screen
	pDC->BitBlt(
		lpDrawItemStruct->rcItem.left, 
		lpDrawItemStruct->rcItem.top, 
		m_rBtnSize.Width(), 
		m_rBtnSize.Height(), 
		&MemDC, 
		0, 
		m_rBtnSize.Height() * m_iBtnState, 
		SRCCOPY);

	MemDC.SelectObject(hOldBmp);

	DrawButtonIcon(pDC, captionRect);
}

void CRoundButton::SetAlign(int iAlign)
{
	switch (iAlign)
	{    
		case ST_ALIGN_HORIZ:
		case ST_ALIGN_VERT:
			m_iAlign = iAlign;
			break;
	}
	Invalidate();
}

// 아이콘을 넣을 경우 
// 1. iIconInId : ICON ID
// 2. iAlign : 중간, 왼쪽
// 3. iDivFactor_X : 왼쪽일 경우 몇 등분의 첫번째에 아이콘이 위치할 것인지 결정 (값이 클 수록 왼쪽외곽에 가까워 진다.)
// 4. iDivFactor_Y : 중간일 경우 및 등분의 첫번째에 아이콘이 위치할 것인지 결정 (값이 클 수록 외곽에 가까워 진다.)
// 5. iPosTxt_X : Caption의 X 위치는 어느 정도인가? 버튼을 기준으로 설정.(값을 설정하지 않을 경우 중간으로 Caption이 들어간다.)
// 6. iPosTxt_Y : Caption의 Y 위치는 어느 정도인가? 버튼을 기준으로 설정.(값을 설정하지 않을 경우 중간으로 Caption이 들어간다.)
void CRoundButton::SetIcon(int iIconInId, int iAlign, int iDivFactor_X, int iDivFactor_Y, int iPosTxt_X, int iPosTxt_Y)
{
	HICON		hIcon;
	HINSTANCE	hInstResource;

	m_iDivFactor_X	= iDivFactor_X;
	m_iDivFactor_Y	= iDivFactor_Y;
	m_iPosTxt_X		= iPosTxt_X;
	m_iPosTxt_Y		= iPosTxt_Y;

	m_iAlign = iAlign;

	// Find correct resource handle
	hInstResource = AfxFindResourceHandle(MAKEINTRESOURCE(iIconInId), RT_GROUP_ICON);

	// Set icon when the mouse is IN the button
	hIcon = (HICON)::LoadImage(hInstResource, MAKEINTRESOURCE(iIconInId), IMAGE_ICON, 0, 0, 0);

	LowLevelSetIcon(hIcon);	
}

void CRoundButton::LowLevelSetIcon(HICON hIcon)
{
	BOOL		bRetValue;
	ICONINFO	IconInfo;

	if (m_stIcon.hIcon != NULL)	::DeleteObject(m_stIcon.hIcon);

	::ZeroMemory(&m_stIcon, sizeof(m_stIcon));

	if (hIcon != NULL)
	{
		m_stIcon.hIcon = hIcon;

		// Get icon dimension
		ZeroMemory(&IconInfo, sizeof(ICONINFO));
		bRetValue = ::GetIconInfo(hIcon, &IconInfo);
		if (bRetValue == FALSE)
		{
			if (m_stIcon.hIcon != NULL)	::DeleteObject(m_stIcon.hIcon);
			::ZeroMemory(&m_stIcon, sizeof(m_stIcon));
			return;
		} // if
		
		m_stIcon.dwWidth	= (DWORD)(IconInfo.xHotspot * 2);
		m_stIcon.dwHeight	= (DWORD)(IconInfo.yHotspot * 2);
		::DeleteObject(IconInfo.hbmMask);
		::DeleteObject(IconInfo.hbmColor);

	} // if

	RedrawWindow();
}


BEGIN_MESSAGE_MAP(CRoundButton, CButton)
	ON_WM_LBUTTONUP()
	ON_WM_MOUSEMOVE()
	ON_WM_CAPTURECHANGED()
END_MESSAGE_MAP()

LRESULT CRoundButton::DefWindowProc(UINT message, WPARAM wParam, LPARAM lParam) 
{
    switch( message )
    {
		// Up, Down, Dblclk 까지 모두 메시지를 보내지 않으면 버튼이 빠르게 동작하지 않는다.
		case WM_LBUTTONUP:
		{
			this->SendMessage(BM_SETSTATE, 0);
			// 부모에게 Click되었다고 알려준다.
			// 만약 부모에게 알려주지 않으면 Click이벤트가 발생하지 않는다.
			CWnd* pParent = GetParent();
			if( NULL != pParent )
			{
				if( m_bButtonLDown ) // YTH 수정 2010.03.30
				{
					m_bButtonLDown = false;
					::SendMessage(	pParent->m_hWnd, WM_COMMAND, (BN_CLICKED<<16) 
								| this->GetDlgCtrlID(), (LPARAM)this->m_hWnd );

				}
			}
			break;
		}
		case WM_LBUTTONDOWN:
			m_bButtonLDown = true; // YTH 수정 2010.03.30
			this->SendMessage(BM_SETSTATE, 1);
			break;
		case WM_LBUTTONDBLCLK:
			//m_bButtonLDown = false;
			this->SendMessage(BM_SETSTATE, 2);
			break;
    }

    return CButton::DefWindowProc( message, wParam, lParam );
}

void CRoundButton::OnLButtonUp(UINT nFlags, CPoint point)
{
	if (m_bIsCheckButton)
	{
		m_bIsChecked = !m_bIsChecked;
	}
	if (m_bIsRadioButton)
	{
		m_bIsChecked = true;
	}

	CButton::OnLButtonUp(nFlags, point);
}


void CRoundButton::OnMouseMove(UINT nFlags, CPoint point)
{
	CRect rcClient;

	// Get Rectangle of Client
	GetClientRect(rcClient);

	// Check, if Mouse is on Control
	if (rcClient.PtInRect(point))
	{
		// We only need to redraw, if the mouse enters
		bool bRedrawNeeded = !m_bMouseOnButton;

		// Mouse is on Control
		m_bMouseOnButton = true;

		// Set Capture to recognize, when the mouse leaves the control
		SetCapture();

		// Redraw Control, if Button is hot
		if (m_bIsHotButton && bRedrawNeeded)
		{
			SetCapture();
			Invalidate();
		}

		////////////////////////////////////////////////////////////
		// YTH 추가 2010.03.31  상위 윈도위로 이동 했을 경우 Release 하기 위해.
		POINT p2 = point;
		ClientToScreen(&p2);
		CWnd* wndUnderMouse = WindowFromPoint(p2);
		//		if (wndUnderMouse != this)
		if (wndUnderMouse && wndUnderMouse->m_hWnd != this->m_hWnd)
		{
			// Redraw only if mouse goes out
			if (m_bMouseOnButton == true)
			{
				m_bMouseOnButton = false;
				Invalidate();
			}
			// If user is NOT pressing left button then release capture!
			if (!(nFlags & MK_LBUTTON)) ReleaseCapture();
		}
		////////////////////////////////////////////////////////////
	}
	else
	{
		// We have lost the mouse-capture, so the mouse has left the buttons face
		m_bMouseOnButton = false;
		m_bButtonLDown = false; // YTH 수정 2010.03.30

		// Mouse has left the button
		ReleaseCapture();

		// Redraw Control, if Button is hot
		if (m_bIsHotButton)
			Invalidate();
	}

	CButton::OnMouseMove(nFlags, point);
}

void CRoundButton::OnCaptureChanged(CWnd *pWnd)
{
	// Check, if we lost the mouse-capture
	if (GetCapture() != this)
	{
		// We have lost the mouse-capture, so the mouse has left the buttons face
		m_bMouseOnButton = false;

		// Redraw Control, if Button is hot
		if (m_bIsHotButton)
			Invalidate();
	}

	CButton::OnCaptureChanged(pWnd);
}
void CRoundButton::SetMulitiText(bool bMultiText)
{
	m_bMultiText = bMultiText;
}
