// HumanFont.cpp: implementation of the CHumanFont class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "UserFont.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

#pragma warning( disable : 4996 )

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CUserFont::CUserFont(CFont* pFont, CString strFontName, int iSize, BOOL bBold)
{
	if(pFont == NULL)
		AfxMessageBox(L"pFont is NULL");

	memset(&m_LogFont, 0x00, sizeof(LOGFONT));
    m_LogFont.lfHeight = iSize;
	if(bBold == FALSE)	m_LogFont.lfWeight = 400;
	else				m_LogFont.lfWeight = 600;
    m_LogFont.lfCharSet = 129;
    m_LogFont.lfOutPrecision = 3;
    m_LogFont.lfClipPrecision = 3;
    m_LogFont.lfQuality = 0;
    m_LogFont.lfPitchAndFamily = 49;
	_tcscpy(m_LogFont.lfFaceName, strFontName);
	
    pFont->CreateFontIndirect(&m_LogFont);
}

CUserFont::~CUserFont()
{

}

void CUserFont::GetLogFont(LOGFONT* LogFont)
{
	LogFont = &m_LogFont;
}



	
