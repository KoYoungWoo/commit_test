#pragma once

#include "GdipWndExt.h"

// CFlatLabelExt

namespace UIExt
{

class CFlatLabelExt : public CGdipStatic
{
	DECLARE_DYNAMIC(CFlatLabelExt)

protected:
	COLORREF		m_dwTextColor;
	COLORREF		m_dwBodyColor;
	COLORREF		m_dwBorderColor;
	int				m_nBorderWidth;
	int				m_nFontHeight;
	BOOL			m_bIsBold;
	BOOL			m_bOnOffStyle;
	COLORREF		m_dwOnColor;
	COLORREF		m_dwOffColor;
	COLORREF		m_dwDisableColor;
	int				m_nOnOffSignal;		// 0: disable.  1: on,   2: off

	virtual void OnDraw(Gdiplus::Graphics& g, Rect rect) override;
	virtual void OnGdipEraseBkgnd( Graphics& g, Rect rect ) override;

public:
	void SetColor( COLORREF dwBodyColor, COLORREF dwTextColor, COLORREF dwBorderColor=-1 );
	void SetOnOffColor( COLORREF dwOnColor, COLORREF dwOffColor, COLORREF dwDisableColor);
	void SetBorderWidth( int nWidth );
	void SetBold( BOOL bBold );
	void SetFontHeight( int nHeight=0 );	// 0: variable, other: fixed
	void SetOnOffStyle( BOOL bOnOffStyle=TRUE );
	void SetOnOffSignal( int nSignal=0 );	// 0: disable.  1: on,   2: off
	CFlatLabelExt();
	virtual ~CFlatLabelExt();

protected:
	DECLARE_MESSAGE_MAP()
	virtual void PreSubclassWindow();
public:
	afx_msg void OnUpdateUIState(UINT /*nAction*/, UINT /*nUIElement*/);
};

} // namespace UIExt