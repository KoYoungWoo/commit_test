#include "stdafx.h"
#include "PLCReader.h"
#include "MxComp3.h"

CPLCReaderD::CPLCReaderD(void)
{
	m_pMxComp3 = NULL;
	m_pBuffer = NULL;
	m_nStartAddress = m_nEndAddress = 0;
}


CPLCReaderD::~CPLCReaderD(void)
{
	if (m_pBuffer)
		delete[] m_pBuffer;
	m_pBuffer = NULL;
}

void CPLCReaderD::SetAddress( int nStartAddress, int nEndAddress )
{
	if (nStartAddress >= nEndAddress)
		return;

	Lock lock(m_csBuffer);

	if (m_pBuffer)
		delete[] m_pBuffer;

	m_nStartAddress = nStartAddress;
	m_nEndAddress = nEndAddress;

	int nBufCount = m_nEndAddress - m_nStartAddress + 1;
	m_pBuffer = new SHORT[nBufCount];
	memset( m_pBuffer, 0, sizeof(SHORT)*nBufCount );
}

BOOL CPLCReaderD::Read()
{
	if (!m_pMxComp3)
		return FALSE;

	Lock lock(m_csBuffer);

	CString strAddress;
	strAddress.Format( _T("D%d"), m_nStartAddress );
	int nBufCount = m_nEndAddress - m_nStartAddress + 1;
	return m_pMxComp3->ReadDeviceBlock2( strAddress, nBufCount, m_pBuffer );
}

SHORT CPLCReaderD::GetValue( int nAddress )
{
	ASSERT (nAddress >= m_nStartAddress && nAddress <= m_nEndAddress);

	Lock lock(m_csBuffer);

	int nPos = nAddress-m_nStartAddress;  ///수정
	
	return m_pBuffer[nPos];
}

LONG CPLCReaderD::GetValue2( int nAddress )
{
	ASSERT (nAddress >= m_nStartAddress && nAddress <= m_nEndAddress);

	Lock lock(m_csBuffer);

	int nPos = nAddress-m_nStartAddress;  ///수정
	
	return *(LONG*)(m_pBuffer+nPos);     ///32비트로 받기 공부 
}

MADDRESS CPLCReaderD::GetValueM( int nAddress )
{
	ASSERT (nAddress >= m_nStartAddress && nAddress <= m_nEndAddress);

	Lock lock(m_csBuffer);

	int nPos = nAddress-m_nStartAddress;  ///수정
	SHORT sTemp = m_pBuffer[nPos];

	MADDRESS *pMAddress = (MADDRESS *)&sTemp;

	return *pMAddress;
}

CString CPLCReaderD::GetString( int nAddress, int nSize)
{
	ASSERT (nAddress >= m_nStartAddress && nAddress <= m_nEndAddress);
	ASSERT (nSize < 256 && nSize > 0);
	Lock lock(m_csBuffer);

	int nPos = nAddress-m_nStartAddress;  ///수정
	SHORT sTemp = m_pBuffer[nPos];

	TCHAR strTemp[256];
	for(int i=0;i<nSize;i++)
	{
		strTemp[i*2] = LOBYTE(m_pBuffer[nPos+i]);
		strTemp[i*2+1] = HIBYTE(m_pBuffer[nPos+i]);
	}
	CString strString;
	strString.Format(_T("%s"), strTemp);

	return strString;
}


CPLCReaderZR::CPLCReaderZR(void)
{
	m_pMxComp3 = NULL;
	m_pBuffer = NULL;
	m_nStartAddress = m_nEndAddress = 0;
}


CPLCReaderZR::~CPLCReaderZR(void)
{
	if (m_pBuffer)
		delete[] m_pBuffer;
	m_pBuffer = NULL;
}

void CPLCReaderZR::SetAddress( int nStartAddress, int nEndAddress )
{
	if (nStartAddress >= nEndAddress)
		return;

	Lock lock(m_csBuffer);

	if (m_pBuffer)
		delete[] m_pBuffer;

	m_nStartAddress = nStartAddress;
	m_nEndAddress = nEndAddress;

	int nBufCount = m_nEndAddress - m_nStartAddress + 1;
	m_pBuffer = new SHORT[nBufCount];
	memset( m_pBuffer, 0, sizeof(SHORT)*nBufCount );
}

BOOL CPLCReaderZR::Read()
{
	if (!m_pMxComp3)
		return FALSE;

	Lock lock(m_csBuffer);

	CString strAddress;
	strAddress.Format( _T("ZR%d"), m_nStartAddress );
	int nBufCount = m_nEndAddress - m_nStartAddress + 1;
	return m_pMxComp3->ReadDeviceBlock2( strAddress, nBufCount, m_pBuffer );
}

SHORT CPLCReaderZR::GetValue( int nAddress )
{
	ASSERT (nAddress >= m_nStartAddress && nAddress <= m_nEndAddress);

	Lock lock(m_csBuffer);

	int nPos = nAddress-m_nStartAddress;  ///수정
	
	return m_pBuffer[nPos];
}

LONG CPLCReaderZR::GetValue2( int nAddress )
{
	ASSERT (nAddress >= m_nStartAddress && nAddress <= m_nEndAddress);

	Lock lock(m_csBuffer);

	int nPos = nAddress-m_nStartAddress;  ///수정
	
	return *(LONG*)(m_pBuffer+nPos);     ///32비트로 받기 공부 
}

MADDRESS CPLCReaderZR::GetValueM( int nAddress )
{
	ASSERT (nAddress >= m_nStartAddress && nAddress <= m_nEndAddress);

	Lock lock(m_csBuffer);

	int nPos = nAddress-m_nStartAddress;  ///수정
	SHORT sTemp = m_pBuffer[nPos];

	MADDRESS *pMAddress = (MADDRESS *)&sTemp;

	return *pMAddress;
}

CString CPLCReaderZR::GetString( int nAddress, int nSize)
{
	ASSERT (nAddress >= m_nStartAddress && nAddress <= m_nEndAddress);
	ASSERT (nSize < 256 && nSize > 0);
	Lock lock(m_csBuffer);

	int nPos = nAddress-m_nStartAddress;  ///수정
	SHORT sTemp = m_pBuffer[nPos];

	TCHAR strTemp[256];
	for(int i=0;i<nSize;i++)
	{
		strTemp[i*2] = LOBYTE(m_pBuffer[nPos+i]);
		strTemp[i*2+1] = HIBYTE(m_pBuffer[nPos+i]);
	}
	CString strString;
	strString.Format(_T("%s"), strTemp);

	return strString;
}


CPLCReaderR::CPLCReaderR(void)
{
	m_pMxComp3 = NULL;
	m_pBuffer = NULL;
	m_nStartAddress = m_nEndAddress = 0;
}


CPLCReaderR::~CPLCReaderR(void)
{
	if (m_pBuffer)
		delete[] m_pBuffer;
	m_pBuffer = NULL;
}

void CPLCReaderR::SetAddress( int nStartAddress, int nEndAddress )
{
	if (nStartAddress >= nEndAddress)
		return;

	Lock lock(m_csBuffer);

	if (m_pBuffer)
		delete[] m_pBuffer;

	m_nStartAddress = nStartAddress;
	m_nEndAddress = nEndAddress;

	int nBufCount = m_nEndAddress - m_nStartAddress + 1;
	m_pBuffer = new SHORT[nBufCount];
	memset( m_pBuffer, 0, sizeof(SHORT)*nBufCount );
}

BOOL CPLCReaderR::Read()
{
	if (!m_pMxComp3)
		return FALSE;

	Lock lock(m_csBuffer);

	CString strAddress;
	strAddress.Format( _T("R%d"), m_nStartAddress );
	int nBufCount = m_nEndAddress - m_nStartAddress + 1;
	return m_pMxComp3->ReadDeviceBlock2( strAddress, nBufCount, m_pBuffer );
}

SHORT CPLCReaderR::GetValue( int nAddress )
{
	ASSERT (nAddress >= m_nStartAddress && nAddress <= m_nEndAddress);

	Lock lock(m_csBuffer);

	int nPos = nAddress-m_nStartAddress;  ///수정
	
	return m_pBuffer[nPos];
}

LONG CPLCReaderR::GetValue2( int nAddress )
{
	ASSERT (nAddress >= m_nStartAddress && nAddress <= m_nEndAddress);

	Lock lock(m_csBuffer);

	int nPos = nAddress-m_nStartAddress;  ///수정
	
	return *(LONG*)(m_pBuffer+nPos);     ///32비트로 받기 공부 
}

MADDRESS CPLCReaderR::GetValueM( int nAddress )
{
	ASSERT (nAddress >= m_nStartAddress && nAddress <= m_nEndAddress);

	Lock lock(m_csBuffer);

	int nPos = nAddress-m_nStartAddress;  ///수정
	SHORT sTemp = m_pBuffer[nPos];

	MADDRESS *pMAddress = (MADDRESS *)&sTemp;

	return *pMAddress;
}

CString CPLCReaderR::GetString( int nAddress, int nSize)
{
	ASSERT (nAddress >= m_nStartAddress && nAddress <= m_nEndAddress);
	ASSERT (nSize < 256 && nSize > 0);
	Lock lock(m_csBuffer);

	int nPos = nAddress-m_nStartAddress;  ///수정
	SHORT sTemp = m_pBuffer[nPos];

	TCHAR strTemp[256];
	for(int i=0;i<nSize;i++)
	{
		strTemp[i*2] = LOBYTE(m_pBuffer[nPos+i]);
		strTemp[i*2+1] = HIBYTE(m_pBuffer[nPos+i]);
	}
	CString strString;
	strString.Format(_T("%s"), strTemp);

	return strString;
}


CPLCReaderM::CPLCReaderM(void)
{
	m_pMxComp3 = NULL;
	m_pBuffer = NULL;
	m_nStartAddress = m_nEndAddress = 0;
}


CPLCReaderM::~CPLCReaderM(void)
{
	if (m_pBuffer)
		delete[] m_pBuffer;
	m_pBuffer = NULL;
}

void CPLCReaderM::SetAddress( int nStartAddress, int nEndAddress )
{
	if (nStartAddress >= nEndAddress)
		return;

	Lock lock(m_csBuffer);

	if (m_pBuffer)
		delete[] m_pBuffer;

	m_nStartAddress = (nStartAddress) / 16 * 16;
	m_nEndAddress = (nEndAddress + 15) / 16 * 16;

	int nBufCount = m_nEndAddress - m_nStartAddress;
	nBufCount = nBufCount/16 + 1;
	m_pBuffer = new SHORT[nBufCount];
	memset( m_pBuffer, 0, sizeof(SHORT)*nBufCount );
}

BOOL CPLCReaderM::Read()
{
	if (!m_pMxComp3)
		return FALSE;

	Lock lock(m_csBuffer);

	CString strAddress;
	strAddress.Format( _T("M%d"), m_nStartAddress );
	int nBufCount = m_nEndAddress - m_nStartAddress;
	nBufCount = nBufCount/16 + 1;
	return m_pMxComp3->ReadDeviceBlock2( strAddress, nBufCount, m_pBuffer );

}



BOOL CPLCReaderM::IsON( int nAddress )
{
	if(nAddress < m_nStartAddress || nAddress > m_nEndAddress) return FALSE;
	//ASSERT (nAddress >= m_nStartAddress && nAddress <= m_nEndAddress);

	Lock lock(m_csBuffer);

	int nV = nAddress-m_nStartAddress;
	int nPos = nV / 16;
	SHORT v = m_pBuffer[nPos];

	int nBit = nV % 16;
	if ( (v & (0x01<<nBit)) )
		return TRUE;
	return FALSE;
}

CPLCReaderL::CPLCReaderL(void)
{
	m_pMxComp3 = NULL;
	m_pBuffer = NULL;
	m_nStartAddress = m_nEndAddress = 0;
}


CPLCReaderL::~CPLCReaderL(void)
{
	if (m_pBuffer)
		delete[] m_pBuffer;
	m_pBuffer = NULL;
}

void CPLCReaderL::SetAddress( int nStartAddress, int nEndAddress )
{
	if (nStartAddress >= nEndAddress)
		return;

	Lock lock(m_csBuffer);

	if (m_pBuffer)
		delete[] m_pBuffer;

	m_nStartAddress = (nStartAddress) / 16 * 16;
	m_nEndAddress = (nEndAddress + 15) / 16 * 16;

	int nBufCount = m_nEndAddress - m_nStartAddress;
	nBufCount = nBufCount/16 + 1;
	m_pBuffer = new SHORT[nBufCount];
	memset( m_pBuffer, 0, sizeof(SHORT)*nBufCount );
}

BOOL CPLCReaderL::Read()
{
	if (!m_pMxComp3)
		return FALSE;

	Lock lock(m_csBuffer);

	CString strAddress;
	strAddress.Format( _T("L%d"), m_nStartAddress );
	int nBufCount = m_nEndAddress - m_nStartAddress;
	nBufCount = nBufCount/16 + 1;
	return m_pMxComp3->ReadDeviceBlock2( strAddress, nBufCount, m_pBuffer );
}

BOOL CPLCReaderL::IsON( int nAddress )
{
	if(nAddress < m_nStartAddress || nAddress > m_nEndAddress) return FALSE;
	//ASSERT (nAddress >= m_nStartAddress && nAddress <= m_nEndAddress);

	Lock lock(m_csBuffer);

	int nV = nAddress-m_nStartAddress;
	int nPos = nV / 16;
	SHORT v = m_pBuffer[nPos];

	int nBit = nV % 16;
	if ( (v & (0x01<<nBit)) )
		return TRUE;
	return FALSE;
}


CPLCReaderX::CPLCReaderX(void)
{
	m_pMxComp3 = NULL;
	m_pBuffer = NULL;
	m_nStartAddress = m_nEndAddress = 0;
}


CPLCReaderX::~CPLCReaderX(void)
{
	if (m_pBuffer)
		delete[] m_pBuffer;
	m_pBuffer = NULL;
}

void CPLCReaderX::SetAddressHex( int nStartAddress, int nEndAddress )
{
	if (nStartAddress >= nEndAddress)
		return;

	Lock lock(m_csBuffer);

	if (m_pBuffer)
		delete[] m_pBuffer;

	m_nStartAddress = (nStartAddress) / 16 * 16;
	m_nEndAddress = (nEndAddress + 15) / 16 * 16;

	int nBufCount = m_nEndAddress - m_nStartAddress;
	nBufCount = nBufCount/16 + 1;
	m_pBuffer = new SHORT[nBufCount];
	memset( m_pBuffer, 0, sizeof(SHORT)*nBufCount );
}

BOOL CPLCReaderX::Read()
{
	if (!m_pMxComp3)
		return FALSE;

	Lock lock(m_csBuffer);

	CString strAddress;
	strAddress.Format( _T("X%x"), m_nStartAddress );
	int nBufCount = m_nEndAddress - m_nStartAddress;
	nBufCount = nBufCount/16 + 1;
	return m_pMxComp3->ReadDeviceBlock2( strAddress, nBufCount, m_pBuffer );
}

BOOL CPLCReaderX::IsONHex( int nAddress )
{
	Lock lock(m_csBuffer);
	if(nAddress >= m_nStartAddress && nAddress <= m_nEndAddress)
	{
		int nV = nAddress-m_nStartAddress;
		int nPos = nV / 16;
		SHORT v = m_pBuffer[nPos];

		int nBit = nV % 16;
		if ( (v & (0x01<<nBit)) )
			return TRUE;
	}
	return FALSE;
}


CPLCReaderY::CPLCReaderY(void)
{
	m_pMxComp3 = NULL;
	m_pBuffer = NULL;
	m_nStartAddress = m_nEndAddress = 0;
}


CPLCReaderY::~CPLCReaderY(void)
{
	if (m_pBuffer)
		delete[] m_pBuffer;
	m_pBuffer = NULL;
}

void CPLCReaderY::SetAddressHex( int nStartAddress, int nEndAddress )
{
	if (nStartAddress >= nEndAddress)
		return;

	Lock lock(m_csBuffer);

	if (m_pBuffer)
		delete[] m_pBuffer;

	m_nStartAddress = (nStartAddress) / 16 * 16;
	m_nEndAddress = (nEndAddress + 15) / 16 * 16;

	int nBufCount = m_nEndAddress - m_nStartAddress;
	nBufCount = nBufCount/16 + 1;
	m_pBuffer = new SHORT[nBufCount];
	memset( m_pBuffer, 0, sizeof(SHORT)*nBufCount );
}

BOOL CPLCReaderY::Read()
{
	if (!m_pMxComp3)
		return FALSE;

	Lock lock(m_csBuffer);

	CString strAddress;
	strAddress.Format( _T("Y%x"), m_nStartAddress );
	int nBufCount = m_nEndAddress - m_nStartAddress;
	nBufCount = nBufCount/16 + 1;
	return m_pMxComp3->ReadDeviceBlock2( strAddress, nBufCount, m_pBuffer );
}

BOOL CPLCReaderY::IsONHex( int nAddress )
{
	Lock lock(m_csBuffer);

	if(nAddress >= m_nStartAddress && nAddress <= m_nEndAddress)
	{
		int nV = nAddress-m_nStartAddress;
		int nPos = nV / 16;
		SHORT v = m_pBuffer[nPos];

		int nBit = nV % 16;
		if ( (v & (0x01<<nBit)) )
			return TRUE;
	}
	return FALSE;
}