// Dlg_Auth.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "AMKOR_MMI.h"
#include "Dlg_Auth.h"
#include "Dlg_NumPad.h"
#include "afxdialogex.h"


// CDlg_Auth 대화 상자입니다.

IMPLEMENT_DYNAMIC(CDlg_Auth, CDialogEx)

CDlg_Auth::CDlg_Auth(CWnd* pParent /*=NULL*/)
	: CDialogEx(CDlg_Auth::IDD, pParent)
{
	m_nAuthNumber = 0; // 0-Operator, 1-Engineer, 2-Developer
}

CDlg_Auth::~CDlg_Auth()
{
}

void CDlg_Auth::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BTN_AUTH_1, m_btn_1);
	DDX_Control(pDX, IDC_BTN_AUTH_2, m_btn_2);
	DDX_Control(pDX, IDC_BTN_AUTH_3, m_btn_3);
	DDX_Control(pDX, IDC_BTN_AUTH_4, m_btn_4);
}


BEGIN_MESSAGE_MAP(CDlg_Auth, CDialogEx)
	ON_WM_ERASEBKGND()
	ON_WM_SIZE()
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BTN_AUTH_1, &CDlg_Auth::OnBnClickedBtnAuth1)
	ON_BN_CLICKED(IDC_BTN_AUTH_2, &CDlg_Auth::OnBnClickedBtnAuth2)
	ON_BN_CLICKED(IDC_BTN_AUTH_3, &CDlg_Auth::OnBnClickedBtnAuth3)
	ON_BN_CLICKED(IDC_BTN_AUTH_4, &CDlg_Auth::OnBnClickedBtnAuth4)
END_MESSAGE_MAP()


// CDlg_Auth 메시지 처리기입니다.


BOOL CDlg_Auth::OnEraseBkgnd(CDC* pDC)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	// 전체 배경색
	CRect rect;
    GetClientRect(&rect);
    CBrush myBrush(CR_MAIN_BKGND);	// dialog background color
    CBrush *pOld = pDC->SelectObject(&myBrush);
    BOOL bRes  = pDC->PatBlt(0, 0, rect.Width(), rect.Height(), PATCOPY);
    pDC->SelectObject(pOld);		// restore old brush
    return FALSE;     
	//return CDialogEx::OnEraseBkgnd(pDC);
}


void CDlg_Auth::OnSize(UINT nType, int cx, int cy)
{
	CDialogEx::OnSize(nType, cx, cy);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
}


void CDlg_Auth::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CDialogEx::OnTimer(nIDEvent);
}


BOOL CDlg_Auth::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.

	m_btn_1.SetFontBtn(CTRL_FONT, 26, FW_BOLD);
	m_btn_1.SetButtonColor(CR_OLIVE, CR_LIGHTGRAY);
	m_btn_1.SetTextColor(CR_BLACK);
	m_btn_1.SetIconPos(0);
	m_btn_1.SetIconID(IDI_DEVELOPER);
	m_btn_1.SetIconSize(64,64);
	m_btn_1.SetFlatType(TRUE);

	m_btn_2.SetFontBtn(CTRL_FONT, 26, FW_BOLD);
	m_btn_2.SetButtonColor(CR_OLIVE, CR_LIGHTGRAY);
	m_btn_2.SetTextColor(CR_BLACK);
	m_btn_2.SetIconPos(0);
	m_btn_2.SetIconID(IDI_ENGINEER);
	m_btn_2.SetIconSize(64,64);
	m_btn_2.SetFlatType(TRUE);

	m_btn_3.SetFontBtn(CTRL_FONT, 26, FW_BOLD);
	m_btn_3.SetButtonColor(CR_OLIVE, CR_LIGHTGRAY);
	m_btn_3.SetTextColor(CR_BLACK);
	m_btn_3.SetIconPos(0);
	m_btn_3.SetIconID(IDI_OPERATOR);
	m_btn_3.SetIconSize(64,64);
	m_btn_3.SetFlatType(TRUE);

	m_btn_4.SetFontBtn(CTRL_FONT, 26, FW_BOLD);
	m_btn_4.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_4.SetTextColor(CR_BLACK);
	m_btn_4.SetIconPos(0);
	m_btn_4.SetIconID(IDI_CLOSE);
	m_btn_4.SetIconSize(48,48);
	m_btn_4.SetFlatType(TRUE);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


BOOL CDlg_Auth::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message == WM_SYSKEYDOWN)
	{
		if(pMsg->wParam == VK_F4) 
			return TRUE;
	}

	if(pMsg->message == WM_KEYDOWN)
	{
		if( pMsg->wParam == VK_ESCAPE ||	
			pMsg->wParam == VK_RETURN ||	
			pMsg->wParam == VK_SPACE  ||
			pMsg->wParam == VK_CANCEL )     
			return TRUE;        
	}

	return CDialogEx::PreTranslateMessage(pMsg);
}


void CDlg_Auth::OnBnClickedBtnAuth1()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CDlg_NumPad dlg;
	dlg.m_strCaption = _T("CHANGE AUTH");
	dlg.m_bStringMode = TRUE;
	if(dlg.DoModal() == IDOK)
	{
		if(dlg.m_strValue.CompareNoCase(_T("0000")) == 0)
		{
			// LOG 남기기
			GetLog()->Debug( _T("CHANGE AUTH - DEVELOPER") );
			CLogList::Instance()->Add_LogData(eLog_DEBUG, _T("CHANGE AUTH - DEVELOPER") );

			m_nAuthNumber = 2;
			CDialogEx::OnOK();
		}
	}
}


void CDlg_Auth::OnBnClickedBtnAuth2()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CDlg_NumPad dlg;
	dlg.m_strCaption = _T("CHANGE AUTH");
	dlg.m_bStringMode = TRUE;
	if(dlg.DoModal() == IDOK)
	{
		if(dlg.m_strValue.CompareNoCase(_T("0000")) == 0)
		{
			// LOG 남기기
			GetLog()->Debug( _T("CHANGE AUTH - ENGINEER") );
			CLogList::Instance()->Add_LogData(eLog_DEBUG, _T("CHANGE AUTH - ENGINEER") );

			m_nAuthNumber = 1;
			CDialogEx::OnOK();
		}
	}
}


void CDlg_Auth::OnBnClickedBtnAuth3()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CDlg_NumPad dlg;
	dlg.m_strCaption = _T("CHANGE AUTH");
	dlg.m_bStringMode = TRUE;
	if(dlg.DoModal() == IDOK)
	{
		if(dlg.m_strValue.CompareNoCase(_T("0000")) == 0)
		{
			// LOG 남기기
			GetLog()->Debug( _T("CHANGE AUTH - OPERATOR") );
			CLogList::Instance()->Add_LogData(eLog_DEBUG, _T("CHANGE AUTH - OPERATOR") );

			m_nAuthNumber = 0;
			CDialogEx::OnOK();
		}
	}
}


void CDlg_Auth::OnBnClickedBtnAuth4()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CDialogEx::OnCancel();
}
