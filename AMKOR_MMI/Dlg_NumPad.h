#pragma once
#include "afxwin.h"
#include "UIExt/CxStatic.h"
#include "UIExt/IconButton.h"

// CDlg_NumPad 대화 상자입니다.

class CDlg_NumPad : public CDialogEx
{
	DECLARE_DYNAMIC(CDlg_NumPad)

public:
	CDlg_NumPad(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CDlg_NumPad();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DLG_NUMPAD };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);

	CString m_strCaption;
	CString m_strValue;
	BOOL m_bStringMode;
	BOOL m_bPassword;

	void SetNumeric(BOOL bNumeric);
	void SetPassword(BOOL bPassword);

	CxStatic m_st_1;
	CIconButton m_btn_1;
	CIconButton m_btn_2;
	CIconButton m_btn_3;
	CIconButton m_btn_4;
	CIconButton m_btn_5;
	CIconButton m_btn_6;
	CIconButton m_btn_7;
	CIconButton m_btn_8;
	CIconButton m_btn_9;
	CIconButton m_btn_10;
	CIconButton m_btn_11;
	CIconButton m_btn_12;
	CIconButton m_btn_13;
	CIconButton m_btn_14;
	CIconButton m_btn_15;
	CIconButton m_btn_16;

	afx_msg void OnBnClickedBtnNp1();
	afx_msg void OnBnClickedBtnNp2();
	afx_msg void OnBnClickedBtnNp3();
	afx_msg void OnBnClickedBtnNp4();
	afx_msg void OnBnClickedBtnNp5();
	afx_msg void OnBnClickedBtnNp6();
	afx_msg void OnBnClickedBtnNp7();
	afx_msg void OnBnClickedBtnNp8();
	afx_msg void OnBnClickedBtnNp9();
	afx_msg void OnBnClickedBtnNp10();
	afx_msg void OnBnClickedBtnNp11();
	afx_msg void OnBnClickedBtnNp12();
	afx_msg void OnBnClickedBtnNp13();
	afx_msg void OnBnClickedBtnNp14();
	afx_msg void OnBnClickedBtnNp15();
	afx_msg void OnBnClickedBtnNp16();
};
