#pragma once
#include "afxwin.h"
#include "UIExt/CxStatic.h"
#include "UIExt/IconButton.h"
#include "UIExt/XPGroupBox.h"
#include "UIExt\/BorderStyleEdit.h"
#include "GC/gridctrl.h"

// CDlg_TrayInfo 대화 상자입니다.

class CDlg_TrayInfo : public CDialogEx
{
	DECLARE_DYNAMIC(CDlg_TrayInfo)

public:
	CDlg_TrayInfo(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CDlg_TrayInfo();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DLG_TRAYINFO };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);

	void Cal_Area();
	void Cal_CtrlArea();
	void Cal_CtrlArea_Left();
	void Cal_CtrlArea_Right();

	bool Init_GC_1(int nRows, int nCols);
	afx_msg void OnGridClick_GC_1(NMHDR *pNotifyStruct, LRESULT* pResult);

	void SetTrayInfoListSettingValue();
	void Update_SelTrayIndex(int nTrayIndex);
	
	CRect m_rcLeft;
	CRect m_rcRight;

	BOOL m_bCtrl_FirstLoad;

	int m_nSelTrayIndex;
	int m_nCurPageNum;

	CXPGroupBox m_gb_1;
	CGridCtrl m_GC_1;
	CIconButton m_btn_1;
	CIconButton m_btn_2;
	CIconButton m_btn_3;
	CIconButton m_btn_4;
	CIconButton m_btn_9;
	CIconButton m_btn_10;

	CXPGroupBox m_gb_2;
	CxStatic m_st_1;
	CxStatic m_st_2;
	CxStatic m_st_3;
	CxStatic m_st_4;
	CxStatic m_st_5;
	CxStatic m_st_6;
	CxStatic m_st_7;
	CBorderStyleEdit m_edit_1;
	CBorderStyleEdit m_edit_2;
	CBorderStyleEdit m_edit_3;
	CBorderStyleEdit m_edit_4;
	CBorderStyleEdit m_edit_5;
	CBorderStyleEdit m_edit_6;
	CBorderStyleEdit m_edit_7;
	CIconButton m_btn_5;
	CIconButton m_btn_6;
	CIconButton m_btn_7;
	CIconButton m_btn_8;

	afx_msg void OnBnClickedBtnTi1();
	afx_msg void OnBnClickedBtnTi2();
	afx_msg void OnBnClickedBtnTi3();
	afx_msg void OnBnClickedBtnTi4();
	afx_msg void OnBnClickedBtnTi5();
	afx_msg void OnBnClickedBtnTi6();
	afx_msg void OnBnClickedBtnTi7();
	afx_msg void OnBnClickedBtnTi8();
	afx_msg void OnBnClickedBtnTi9();
	afx_msg void OnBnClickedBtnTi10();
};
