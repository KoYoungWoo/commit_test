// ClientSocket.cpp : implementation file
//

#include "stdafx.h"
#include "AMKOR_MMI.h"
#include "AMKOR_MMIDlg.h"
#include "ListenSocket.h"
#include "ClientSocket.h"

// CClientSocket
CClientSocket::CClientSocket()
{
	m_pListenSocket = NULL;
	m_strSendData = _T("");
}

CClientSocket::~CClientSocket()
{
}

// CClientSocket member functions

void CClientSocket::SetListenSocket(CAsyncSocket* pSocket)
{
	m_pListenSocket = pSocket;
}

void CClientSocket::OnClose(int nErrorCode)
{
	CSocket::OnClose(nErrorCode);

	CListenSocket* pServerSocket = (CListenSocket*)m_pListenSocket;
	pServerSocket->CloseClientSocket(this);
}

void CClientSocket::OnReceive(int nErrorCode)
{
	int nRead = 0;
	
	CHAR chBuff[1024];
	::ZeroMemory(chBuff, sizeof(chBuff));

	TCHAR Buff[1024*2];
	::ZeroMemory(Buff, sizeof(Buff));

	nRead = Receive((BYTE*)chBuff, sizeof(chBuff));
	CString strRcv,strIP,strLog,strCommand;
	CString strPartial[8];
	int nPartialCount = 0;
	CString strBuffer;
	UINT nPort;

	CAMKOR_MMIApp* pApp = (CAMKOR_MMIApp*)AfxGetApp();
	CAMKOR_MMIDlg* pMain = (CAMKOR_MMIDlg*)AfxGetMainWnd();
	CListenSocket* pServerSocket = (CListenSocket*)m_pListenSocket;

	if(nRead != SOCKET_ERROR)
	{
		pApp->MBCS2Unicode(chBuff,Buff);
		strBuffer.Format(L"%s",Buff);
		strRcv.Format(L"%s",Buff);

		// IP 가져오기
		GetPeerName(strIP, nPort);
		// Received Data Log 남기기	
		GetLog()->Debug(_T("[RCV:%s]%s"),strIP,Buff);

		// 패킷 사이즈가 넘 작으면...이상한 값 처리
		if(strBuffer.GetLength() <= 6)
		{
			CSocket::OnReceive(nErrorCode);
			return;
		}

		// Parsing Packet
		int nStartIndex = 0;
		int nPartialSize = 0;
		while(1)
		{
			nPartialSize = _wtoi(strBuffer.Mid(nStartIndex + 4,4));
			strPartial[nPartialCount] = strBuffer.Mid(nStartIndex, nPartialSize);
			nPartialCount ++;
			nStartIndex += nPartialSize;
			if(nStartIndex >= strBuffer.GetLength()-1) break;
			if(nPartialCount >= 3)
			{
				GetLog()->Debug(_T("Packet Parsing Count Over !!!!!-[%s]"), strBuffer);
				CLogList::Instance()->Add_LogData(eLog_DEBUG, _T("Packet Parsing Count Over !!!!!-[%s]"), strBuffer);
				
				break;
			}
		}

		for(int i=0;i<nPartialCount;i++)
		{
			strRcv = strPartial[i];
		
			strCommand = strRcv.Mid(2,2);

			if(strCommand == L"DC") //DEVICE COUNT 정보 요청
			{
				// 응답 체크 - 01DC0011|0|
				int nVisionNum = _wtoi(strRcv.Mid(0,2));
				int nTotalSize = _wtoi(strRcv.Mid(4,4));
				if(nTotalSize == 11) // 비전에서 정보 요청이 오면....
				{
					//CString strSend = _T("");
					//if(CDataManager::Instance()->m_bDryRun)
					//{
					//	strSend.Format(_T("01DC0025|158|160|019|4|4|"));
					//		
					//	GetLog()->DebugT((LPCTSTR)strSend);
					//	CLogList::Instance()->Add_LogData(eLog_DEBUG, strSend);

					//	int nRet = pMain->m_ListenSocket.SendData(pMain->m_dlg_GemScreen.m_strVision01IP, strSend);
					//}
					//else
					//{
					//	strSend = CDataManager::Instance()->Make_TcpIpString_DC(TPOS_COVERTRAYLOAD);
					//	if(strSend.IsEmpty() == FALSE)
					//	{
					//		GetLog()->DebugT((LPCTSTR)strSend);
					//		CLogList::Instance()->Add_LogData(eLog_DEBUG, strSend);

					//		int nRet = pMain->m_ListenSocket.SendData(pMain->m_dlg_GemScreen.m_strVision01IP, strSend); //for test 1번 vision
					//	}
					//	else
					//	{
					//		GetLog()->Debug(_T("Not Exist TrayPack !!!"));
					//		CLogList::Instance()->Add_LogData(eLog_DEBUG, _T("Not Exist TrayPack !!!"));
					//	}
					//}
				}
			}
			else if(strCommand == L"CT") // CAM TRIGGER REP ACK
			{
				GetLog()->Debug(_T("[RCV:%s]%s"), strIP, strRcv);
				CLogList::Instance()->Add_LogData(eLog_DEBUG, _T("[RCV:%s]%s"), strIP, strRcv);

				// 응답 체크
				int nTotalSize = _wtoi(strRcv.Mid(4,4));
				int nCamPos = _wtoi(strRcv.Mid(9,2));
				int nCamSubPos = _wtoi(strRcv.Mid(12,2));
				if(nTotalSize == 15) // HMS에서 잘 받았다고 응답이 오면...
				{
					pMain->Clear_CamTriggerBit(nCamPos,nCamSubPos);
				}
			}
		}
	}

	CSocket::OnReceive(nErrorCode);
}
