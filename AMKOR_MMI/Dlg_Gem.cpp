// Dlg_Gem.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "AMKOR_MMI.h"
#include "AMKOR_MMIDlg.h"
#include "Dlg_Gem.h"
#include "afxdialogex.h"

#define EVENT_COUNT	14
#define ZEBRA_COUNT	5
#define ZPL_COUNT	2
#define ALARM_COUNT 1
#define RECIPE_COUNT 2 //NVIDIA, ARUBA

WCHAR* EventCase[] = {_T("[1000] READY_TO_LOAD_REQUEST"), _T("[1100] MATERIAL_STATUS_REPORT"), _T("[1200] RFID_FULL_REPORT"), _T("[1300] READY_TO_LOAD_BARCODE_REQUEST"), _T("[1400] LOT_START_REQUEST"),
					  _T("[1500] BOX_PROC_RESULT_INFO"), _T("[1600] BOX_END_REQUEST"), _T("[1700] READY_TO_UNLOAD_REQUEST"), _T("[1800] LOT_END_REQUEST"), _T("[1900] ZEBRA_CODE_REQUEST"),
					  _T("[901] Run"), _T("[902] Stop"), _T("[903] Idle"), _T("[10] Process Recipe Selected"), };

short EventReportSVIDcnt[] = {1, 10, 1, 1, 5, 
							  7, 6, 3, 4, 1,
							  3, 3, 3, 2,};

long SVID_LIST_1000[] = { SVID_PORT, };
WCHAR* ITEM_NAME_1000[] = {_T("Port"), };
WCHAR* TEST_DATA_1000[] = {_T("LOAD"), };

long SVID_LIST_1100[] = { SVID_TRAY_STACKER, SVID_END_CAP, SVID_BANDING, SVID_HIC, SVID_MBB,
						  SVID_BOX, SVID_SILICAGEL, SVID_LABEL, SVID_RIBBON, SVID_BUBBLE_SHEET};
WCHAR* ITEM_NAME_1100[] = { _T("Tray Stacker"), _T("End Cap"), _T("Banding"), _T("HIC"), _T("MBB"),
							_T("Box"), _T("SilicaGel"), _T("Label"), _T("Ribbon"), _T("Bubble Sheet"), };
WCHAR* TEST_DATA_1100[] = { _T("T,T,T,T,T,T,T,T,T,T,T,T,T,T,T,T,T,T,T,T,T,T,T,T,T,T,T,T,T,T"), _T("T,T,T,T,T,T,T,T"), _T("T"), _T("T"), _T("T,T"), 
							_T("T"), _T("T"), _T("T,T,T,T"), _T("T,T,T,T"), _T("T"), };

long SVID_LIST_1200[] = { SVID_RFID_FULL_STATUS, };
WCHAR* ITEM_NAME_1200[] = {_T("Rfid_full_status"), };
WCHAR* TEST_DATA_1200[] = {_T("F"), };

long SVID_LIST_1300[] = { SVID_BARCODE, };
WCHAR* ITEM_NAME_1300[] = {_T("BARCODE"), };
WCHAR* TEST_DATA_1300[] = {_T("TEST_BARCODE"), };

long SVID_LIST_1400[] = { SVID_BARCODE, SVID_LOT_ID, SVID_LOT_QTY, SVID_TRAY_QTY, SVID_BUNDLE_TRAY_QTY,};
WCHAR* ITEM_NAME_1400[] = { _T("BARCODE"), _T("LOT ID"), _T("LOT qty"), _T("LOT Tray qty"), _T("BUNDLE Tray qty"), };
WCHAR* TEST_DATA_1400[] = { _T("TEST_BARCODE"), _T("TEST_LOT_ID"), _T("5000"), _T("10"), _T("10"), };

long SVID_LIST_1500[] = { SVID_LOT_ID, SVID_LOT_QTY, SVID_OPER_CODE, SVID_INNER_BOX_ID, SVID_INNER_BOX_DEVICE_QTY,
						  SVID_PARTIAL, SVID_FLAG };
WCHAR* ITEM_NAME_1500[] = { _T("LOT ID"), _T("LOT qty"), _T("OPER_CODE"), _T("Inner BOX ID"), _T("Inner BOX DEVICE qty"), 
							_T("Partial"), _T("FLAG"), };
WCHAR* TEST_DATA_1500[] = { _T("TEST_LOT_ID"), _T("5000"), _T("MBB_SEALING"), _T("001"), _T("500"),
							_T("N"), _T("Y"), };

long SVID_LIST_1600[] = { SVID_BARCODE, SVID_LOT_ID, SVID_LOT_QTY, SVID_INNER_BOX_ID, SVID_INNER_BOX_DEVICE_QTY,
						  SVID_PARTIAL, };
WCHAR* ITEM_NAME_1600[] = { _T("BARCODE"), _T("LOT ID"), _T("LOT qty"), _T("Inner BOX ID"), _T("Inner BOX DEVICE qty"), 
							_T("Partial"),  };
WCHAR* TEST_DATA_1600[] = { _T("TEST_BARCODE"), _T("TEST_LOT_ID"), _T("5000"), _T("001"), _T("500"),
							_T("N"), };

long SVID_LIST_1700[] = { SVID_BARCODE, SVID_LOT_ID, SVID_LOT_QTY,};
WCHAR* ITEM_NAME_1700[] = { _T("BARCODE"), _T("LOT ID"), _T("LOT QTY"), };
WCHAR* TEST_DATA_1700[] = { _T("TEST_BARCODE"), _T("TEST_LOT_ID"), _T("5000"), };

long SVID_LIST_1800[] = { SVID_BARCODE, SVID_LOT_ID, SVID_LOT_QTY, SVID_INNER_BOX_QTY,  };
WCHAR* ITEM_NAME_1800[] = { _T("BARCODE"), _T("LOT ID"), _T("LOT QTY"), _T("Inner BOX qty"), };
WCHAR* TEST_DATA_1800[] = { _T("TEST_BARCODE"), _T("TEST_LOT_ID"), _T("5000"), _T("10"), };

long   SVID_LIST_1900[] = { SVID_LOT_ID, };
WCHAR* ITEM_NAME_1900[] = { _T("LOT ID"), };
WCHAR* TEST_DATA_1900[] = { _T("TEST_LOT_ID"), };

long   SVID_LIST_901[] = { SVID_EQUIPMENT_STATUS, SVID_CONTROL_STATE, SVID_LOT_ID, };
WCHAR* ITEM_NAME_901[] = { _T("EQUIPMENT STATUS"), _T("3"), _T("LOT ID"),  };
WCHAR* TEST_DATA_901[] = { _T("RUN"), _T("OR"), _T("TEST_LOT_ID"), };

long   SVID_LIST_902[] = { SVID_EQUIPMENT_STATUS, SVID_CONTROL_STATE, SVID_LOT_ID, };
WCHAR* ITEM_NAME_902[] = { _T("EQUIPMENT STATUS"), _T("2"), _T("LOT ID"),  };
WCHAR* TEST_DATA_902[] = { _T("STOP"), _T("OR"), _T("TEST_LOT_ID"), };

long   SVID_LIST_903[] = { SVID_EQUIPMENT_STATUS, SVID_CONTROL_STATE, SVID_LOT_ID, };
WCHAR* ITEM_NAME_903[] = { _T("EQUIPMENT STATUS"), _T("3"), _T("LOT ID"),  };
WCHAR* TEST_DATA_903[] = { _T("IDLE"), _T("OR"), _T("TEST_LOT_ID"), };

//Process Recipe Selected
long   SVID_LIST_10[] = { SVID_CURRENT_RECIPE, SVID_PREVIOUS_RECIPE, };
WCHAR* ITEM_NAME_10[] = { _T("CURRENT RECIPE"), _T("PREVIOUS RECIPE"),  };
WCHAR* TEST_DATA_10[] = { _T("TEST_CURRENT_RECIPE"), _T("TEST_PREVIOUS_RECIPE"), };

WCHAR* ZEBRA[] = {_T("[#1] Tray"), _T("[#2] MBB(하)"), _T("[#3] MBB(상)"), _T("[#4] BOX(우)"), _T("[#5] BOX(좌)"),};
WCHAR* ZPL_GUBUN[]= {_T("NVIDIA Box#1 [Small]"), _T("NVIDIA Box#2 [Small]"),_T("A社 Box#1 [Big]"),_T("A社 Box#2 [Big]"),_T("A社 Box#3 [Big]"),
					 _T("A社 Box#4 [Big]"),_T("A社 Box#5 [Big]"),};
WCHAR* TEST_ZPL[] = {
//Box#1
L"^XA"                                                              
L"^MCY"                                                             
L"^XZ"                                                              
L"^XA"                                                               
L"^LH000,000^BY2,6,60^FS"                                            
L"^FO020,030^A0,40,40^FDNVIDIA Corporation^FS"                       
L"^FO020,070^A0,20,20^FDP/N: ASTU104-2228A-1CA1^FS"                  
L"^BY1,3,060^FO020,100^BCN,,N,N^FDASTU104-2228A-1CA1^FS"             
L"^FO330,070^A0,20,20^FD       MODEL#:^FS"                           
L"^BY1,3,060^FO300,100^BCN,,N,N^FD^FS"                               
L"^FO020,170^A0,20,20^FDLot No : PACKING LABEL531^FS"                
L"^FO020,200^BCN,,N,N^FDPACKING LABEL531^FS"                         
L"^FO020,270^A0,20,20^FDQ'TY:     40^FS"                             
L"^FO020,300^BCN,,N,N^FD     40^FS"                                  
L"^FO020,370^A0,20,20^FDKOREA^FS"                                    
L"^FO020,400^BCN,,N,N^FDKOREA^FS"                                    
L"^FO020,470^A0,20,20^FDRoHS Compliant^FS"                           
L"^FO020,500^BCN,,N,N^FDRoHS Compliant^FS"                           
L"^FO500,120^A0,20,20^FDPlant ID: B^FS"   
L"^FO550,150^BCN,,N,N^FDB^FS"                                                                         
L"^FO450,220^A0,20,20^FDDate Code: 1833^FS"                                                           
L"^FO550,250^BCN,,N,N^FD1833^FS"                                                                      
L"^FO450,320^A0,20,20^FDRev : A1 ^FS"                                                                 
L"^FO550,350^BCN,,N,N^FDA1 ^FS"                                                                       
L"^FO450,520^A0,20,20^FDTested in ^FS"                                                                
L"^FO600,550^BCN,,N,N^FD^FS"                                                                          
L" ^FO550,425^GB080,060,2^FS"                                                                         
L"^FO555,435^A0,60,60^FDHF^FS"                                                                        
L"^FO0600,0001^BQN,2,3  ^FDQA,ASTU104-2228A-1CA1|PACKING LABEL531|40^FS"                              
L"~DGEMARK,00792,011,"                                                                                
L"00000000000000000000000000000000000000000000000000003FFE0000000000"                                 
L"00000003FFFFE0000000000000001FFFFFFC000000000000007FFFFFFF00000000000001FFFFFFFFC0000000"           
L"000003FFFFFFFFE000000000000FFFFFFFFFF800000000001FFFFFFFFFFC00000000003FFFFFFFFFFE000000"           
L"00007FFFFFFFFFFF0000000000FFFFFFFFFFFF8000000001FFFFF00FFFFFC000000003FFFF0000FFFFE00000"           
L"0007FFFC00003FFFF00000000FFFF800000FFFF00000000FFFE0000003FFF80000001FFFC0000001FFFC0000"           
L"003FFF00000000FFFC0000003FFF000FF8007FFE0000007FFE007FFF003FFE0000007FFC00FFFFC01FFF0000"           
L"007FF803FFFFE01FF3000000FFF807FFFFF00FE1800000FFF00FFFFFF80FCC800000FFF00FFFFFFC079E0000"           
L"01FFE01FFF7FFC073F000001FFE03FF007FE027F800001FFE03FE003FE00FFC00001FFC03FC001FF01FFE000"              
L"01FFC07F8000FF03FFF00003FFC07F8000FF07FFF80003FFC07FFFFFFF0FFFFC0003FFC07FFFFFFF1FFFFE00"     
L"03FFC07FFFFFFFBFFFFF003FFFFE7FFFFFFF81FFC0001FFFFC7FFFFFFF01FFC0000FFFF87F00000001FFC000"     
L"07FFF07F00000001FFC00003FFE07F80000301FFC00001FFC03F80000603FFC00000FF803FC0000603FFC000"     
L"007F201FC0000C03FFC000003E601FE0001C07FF8000001CF00FF8003807FF80000089F007FC00F00FFF8000"     
L"00C3F803FFFFE00FFF00000067F801FFFFC01FFF0000007FFC007FFF801FFF0000003FFE001FFE003FFE0000"     
L"003FFF0003E0007FFE0000001FFF80000000FFFC0000001FFFC0000001FFF80000000FFFE0000007FFF80000"     
L"0007FFF800000FFFF000000007FFFE00003FFFE000000003FFFF8001FFFFE000000001FFFFFC1FFFFFC00000"     
L"0000FFFFFFFFFFFF80000000007FFFFFFFFFFF00000000003FFFFFFFFFFE00000000001FFFFFFFFFFC000000"     
L"000007FFFFFFFFF0000000000003FFFFFFFFE0000000000000FFFFFFFF800000000000003FFFFFFE00000000"     
L"0000000FFFFFF80000000000000001FFFFC000000000000000000FF800000000000000000000000000000000"     
L"0000000000000000000000"                                                                       
L"^FO300,300^XGEMARK,2,2^FS"                                                                    
L"^XZ",
//Box#2
L"^XA"                                                              
L"^MCY"                                                             
L"^XZ"                                                              
L"^XA"                                                               
L"^LH000,000^BY2,6,60^FS"                                            
L"^FO020,030^A0,40,40^FDNVIDIA Corporation^FS"                       
L"^FO020,070^A0,20,20^FDP/N: ASTU104-2228A-1CA1^FS"                  
L"^BY1,3,060^FO020,100^BCN,,N,N^FDASTU104-2228A-1CA1^FS"             
L"^FO330,070^A0,20,20^FD       MODEL#:^FS"                           
L"^BY1,3,060^FO300,100^BCN,,N,N^FD^FS"                               
L"^FO020,170^A0,20,20^FDLot No : PACKING LABEL531^FS"                
L"^FO020,200^BCN,,N,N^FDPACKING LABEL531^FS"                         
L"^FO020,270^A0,20,20^FDQ'TY:     20^FS"                             
L"^FO020,300^BCN,,N,N^FD     20^FS"                                  
L"^FO020,370^A0,20,20^FDKOREA^FS"                                    
L"^FO020,400^BCN,,N,N^FDKOREA^FS"                                    
L"^FO020,470^A0,20,20^FDRoHS Compliant^FS"                           
L"^FO020,500^BCN,,N,N^FDRoHS Compliant^FS"                           
L"^FO500,120^A0,20,20^FDPlant ID: B^FS"   
L"^FO550,150^BCN,,N,N^FDB^FS"                                                                         
L"^FO450,220^A0,20,20^FDDate Code: 1833^FS"                                                           
L"^FO550,250^BCN,,N,N^FD1833^FS"                                                                      
L"^FO450,320^A0,20,20^FDRev : A1 ^FS"                                                                 
L"^FO550,350^BCN,,N,N^FDA1 ^FS"                                                                       
L"^FO450,520^A0,20,20^FDTested in ^FS"                                                                
L"^FO600,550^BCN,,N,N^FD^FS"                                                                          
L" ^FO550,425^GB080,060,2^FS"                                                                         
L"^FO555,435^A0,60,60^FDHF^FS"                                                                        
L"^FO0600,0001^BQN,2,3  ^FDQA,ASTU104-2228A-1CA1|PACKING LABEL531|20^FS"                              
L"~DGEMARK,00792,011,"                                                                                
L"00000000000000000000000000000000000000000000000000003FFE0000000000"                                 
L"00000003FFFFE0000000000000001FFFFFFC000000000000007FFFFFFF00000000000001FFFFFFFFC0000000"           
L"000003FFFFFFFFE000000000000FFFFFFFFFF800000000001FFFFFFFFFFC00000000003FFFFFFFFFFE000000"          
L"00007FFFFFFFFFFF0000000000FFFFFFFFFFFF8000000001FFFFF00FFFFFC000000003FFFF0000FFFFE00000"          
L"0007FFFC00003FFFF00000000FFFF800000FFFF00000000FFFE0000003FFF80000001FFFC0000001FFFC0000"          
L"003FFF00000000FFFC0000003FFF000FF8007FFE0000007FFE007FFF003FFE0000007FFC00FFFFC01FFF0000"          
L"007FF803FFFFE01FF3000000FFF807FFFFF00FE1800000FFF00FFFFFF80FCC800000FFF00FFFFFFC079E0000"          
L"01FFE01FFF7FFC073F000001FFE03FF007FE027F800001FFE03FE003FE00FFC00001FFC03FC001FF01FFE000"             
L"01FFC07F8000FF03FFF00003FFC07F8000FF07FFF80003FFC07FFFFFFF0FFFFC0003FFC07FFFFFFF1FFFFE00"    
L"03FFC07FFFFFFFBFFFFF003FFFFE7FFFFFFF81FFC0001FFFFC7FFFFFFF01FFC0000FFFF87F00000001FFC000"    
L"07FFF07F00000001FFC00003FFE07F80000301FFC00001FFC03F80000603FFC00000FF803FC0000603FFC000"    
L"007F201FC0000C03FFC000003E601FE0001C07FF8000001CF00FF8003807FF80000089F007FC00F00FFF8000"    
L"00C3F803FFFFE00FFF00000067F801FFFFC01FFF0000007FFC007FFF801FFF0000003FFE001FFE003FFE0000"    
L"003FFF0003E0007FFE0000001FFF80000000FFFC0000001FFFC0000001FFF80000000FFFE0000007FFF80000"    
L"0007FFF800000FFFF000000007FFFE00003FFFE000000003FFFF8001FFFFE000000001FFFFFC1FFFFFC00000"    
L"0000FFFFFFFFFFFF80000000007FFFFFFFFFFF00000000003FFFFFFFFFFE00000000001FFFFFFFFFFC000000"    
L"000007FFFFFFFFF0000000000003FFFFFFFFE0000000000000FFFFFFFF800000000000003FFFFFFE00000000"     
L"0000000FFFFFF80000000000000001FFFFC000000000000000000FF800000000000000000000000000000000"     
L"0000000000000000000000"                                                                      
L"^FO300,300^XGEMARK,2,2^FS"                                                                    
L"^XZ",};

WCHAR* AlarmID[] = {_T("[2000]  F2000 0.LD.C/V스토퍼0 상/하   CYL에러"), };

WCHAR* RecipeID[] = { _T("NVIDIA"), _T("ARUBA"), };

// CDlg_Gem 대화 상자입니다.

IMPLEMENT_DYNAMIC(CDlg_Gem, CDialogEx)

CDlg_Gem::CDlg_Gem(CWnd* pParent /*=NULL*/)
	: CDialogEx(CDlg_Gem::IDD, pParent)
	, m_strInput(_T(""))
	, m_strSVID(_T(""))
	, m_strEventSVIDs(_T(""))
	, m_strEditZebra(_T(""))
{
	m_strEditXGem.Empty();

	m_bCtrl_FirstLoad = FALSE;
}

CDlg_Gem::~CDlg_Gem()
{
}

void CDlg_Gem::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BTN_GEM_1, m_btn_1);
	DDX_Control(pDX, IDC_BTN_GEM_2, m_btn_2);
	DDX_Control(pDX, IDC_BTN_GEM_3, m_btn_3);
	DDX_Control(pDX, IDC_BTN_GEM_4, m_btn_4);
	DDX_Control(pDX, IDC_BTN_GEM_5, m_btn_5);
	DDX_Control(pDX, IDC_BTN_GEM_6, m_btn_6);
	DDX_Control(pDX, IDC_BTN_GEM_7, m_btn_7);
	DDX_Control(pDX, IDC_GB_GEM_1, m_gb_1);
	DDX_Control(pDX, IDC_GB_GEM_2, m_gb_2);
	DDX_Control(pDX, IDC_GB_GEM_3, m_gb_3);
	DDX_Control(pDX, IDC_ST_GEM_1, m_st_1);
	DDX_Control(pDX, IDC_ST_GEM_2, m_st_2);
	DDX_Control(pDX, IDC_ST_GEM_3, m_st_3);
	DDX_Control(pDX, IDC_ST_GEM_4, m_st_4);
	DDX_Control(pDX, IDC_CBO_GEM_1, m_cbo_1);
	DDX_Control(pDX, IDC_CBO_GEM_2, m_cbo_2);
	DDX_Control(pDX, IDC_CBO_GEM_3, m_cbo_3);
	DDX_Control(pDX, IDC_EDIT_GEM_1, m_edit_1);
	DDX_Control(pDX, IDC_EDIT_GEM_2, m_edit_2);
	DDX_Control(pDX, IDC_EDIT_GEM_3, m_edit_3);
	DDX_Text(pDX, IDC_EDIT_GEM_3, m_strEditXGem);
	DDX_Control(pDX, IDC_EDIT_GEM_4, m_edit_4);
	DDX_Control(pDX, IDC_LIST_GEM_1, m_list_1);
	DDX_Control(pDX, IDC_GB_GEM_4, m_gb_4);
	DDX_Control(pDX, IDC_ST_GEM_5, m_st_5);
	DDX_Control(pDX, IDC_ST_GEM_6, m_st_6);
	DDX_Control(pDX, IDC_ST_GEM_7, m_st_7);
	DDX_Control(pDX, IDC_ST_GEM_8, m_st_8);
	DDX_Control(pDX, IDC_CBO_GEM_4, m_cbo_4);
	DDX_Control(pDX, IDC_CBO_GEM_5, m_cbo_5);
	DDX_Control(pDX, IDC_BTN_GEM_8, m_btn_8);
	DDX_Control(pDX, IDC_BTN_GEM_9, m_btn_9);
	DDX_Control(pDX, IDC_BTN_GEM_10, m_btn_10);
	DDX_Control(pDX, IDC_EDIT_GEM_5, m_edit_5);
	DDX_Control(pDX, IDC_EDIT_GEM_6, m_edit_6);
	DDX_Control(pDX, IDC_EXGEM3CTRL1, m_XGem300);
	DDX_LBString(pDX, IDC_LIST_GEM_1, m_strInput);
	DDX_Text(pDX, IDC_EDIT_GEM_1, m_strSVID);
	DDX_Text(pDX, IDC_EDIT_GEM_4, m_strEventSVIDs);
	DDX_Text(pDX, IDC_EDIT_GEM_2, m_strInput);
	DDX_Text(pDX, IDC_EDIT_GEM_6, m_strEditZebra);
}


BEGIN_MESSAGE_MAP(CDlg_Gem, CDialogEx)
	ON_WM_ERASEBKGND()
	ON_WM_SIZE()
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BTN_GEM_1, &CDlg_Gem::OnBnClickedBtnGem1)
	ON_BN_CLICKED(IDC_BTN_GEM_2, &CDlg_Gem::OnBnClickedBtnGem2)
	ON_BN_CLICKED(IDC_BTN_GEM_3, &CDlg_Gem::OnBnClickedBtnGem3)
	ON_BN_CLICKED(IDC_BTN_GEM_4, &CDlg_Gem::OnBnClickedBtnGem4)
	ON_BN_CLICKED(IDC_BTN_GEM_5, &CDlg_Gem::OnBnClickedBtnGem5)
	ON_BN_CLICKED(IDC_BTN_GEM_6, &CDlg_Gem::OnBnClickedBtnGem6)
	ON_BN_CLICKED(IDC_BTN_GEM_7, &CDlg_Gem::OnBnClickedBtnGem7)
	ON_BN_CLICKED(IDC_BTN_GEM_9, &CDlg_Gem::OnBnClickedBtnGem9)
	ON_BN_CLICKED(IDC_BTN_GEM_8, &CDlg_Gem::OnBnClickedBtnGem8)
	ON_BN_CLICKED(IDC_BTN_GEM_10, &CDlg_Gem::OnBnClickedBtnGem10)
	ON_CBN_CLOSEUP(IDC_CBO_GEM_1, &CDlg_Gem::OnCbnCloseupCboGem1)
END_MESSAGE_MAP()


// CDlg_Gem 메시지 처리기입니다.


BOOL CDlg_Gem::OnEraseBkgnd(CDC* pDC)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	// 전체 배경색
	CRect rect;
    GetClientRect(&rect);
    CBrush myBrush(CR_MAIN_BKGND);	// dialog background color
    CBrush *pOld = pDC->SelectObject(&myBrush);
    BOOL bRes  = pDC->PatBlt(0, 0, rect.Width(), rect.Height(), PATCOPY);
    pDC->SelectObject(pOld);		// restore old brush
    return FALSE;     
	//return CDialogEx::OnEraseBkgnd(pDC);
}


void CDlg_Gem::OnSize(UINT nType, int cx, int cy)
{
	CDialogEx::OnSize(nType, cx, cy);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	if(m_bCtrl_FirstLoad == TRUE) Cal_CtrlArea();
}


void CDlg_Gem::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CDialogEx::OnTimer(nIDEvent);
}


BOOL CDlg_Gem::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	Cal_CtrlArea();

//CComboBox m_cbo_1; //Event Combo
//CComboBox m_cbo_2; //ALID Combo
//CComboBox m_cbo_3; //RECIPE ID Combo
//CComboBox m_cbo_4; //Printer Combo
//CComboBox m_cbo_5; //ZPL Combo

	int nCnt = 0;
	//event combo 초기화
	m_cbo_1.AddString(_T("선택하세요."));
	while(nCnt < EVENT_COUNT)
	{
		m_cbo_1.AddString(EventCase[nCnt]);
		nCnt++;
	}
	m_cbo_1.SetCurSel(0);

	//Alarm combo 초기화
	m_cbo_2.AddString(_T("선택하세요."));
	nCnt = 0;
	while(nCnt < ALARM_COUNT)
	{
		m_cbo_2.AddString(AlarmID[nCnt]);
		nCnt++;
	}
	m_cbo_2.SetCurSel(0);

	//Recipe combo 초기화
	m_cbo_3.AddString(_T("선택하세요."));
	nCnt = 0;
	while(nCnt < RECIPE_COUNT)
	{
		m_cbo_3.AddString(RecipeID[nCnt]);
		nCnt++;
	}
	m_cbo_3.SetCurSel(0);

	//ZEBRA combo 초기화
	m_cbo_4.AddString(_T("선택하세요."));
	nCnt = 0;
	while(nCnt < ZEBRA_COUNT)
	{
		m_cbo_4.AddString(ZEBRA[nCnt]);
		nCnt++;
	}
	m_cbo_4.SetCurSel(0);

	//ZPL combo 초기화
	m_cbo_5.AddString(_T("선택하세요."));
	nCnt = 0;
	while(nCnt < ZPL_COUNT)
	{
		m_cbo_5.AddString(ZPL_GUBUN[nCnt]);
		nCnt++;
	}
	m_cbo_5.SetCurSel(0);

	////Label Type combo 초기화
	//m_ctlComboLabelType.AddString(_T("선택하세요."));
	//nCnt = 0;
	//while(nCnt < LABEL_TYPE_COUNT)
	//{
	//	m_ctlComboLabelType.AddString(LABEL_TYPE[nCnt]);
	//	nCnt++;
	//}
	//m_ctlComboLabelType.SetCurSel(0);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


BOOL CDlg_Gem::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message == WM_SYSKEYDOWN)
	{
		if(pMsg->wParam == VK_F4) 
			return TRUE;
	}

	if(pMsg->message == WM_KEYDOWN)
	{
		if( pMsg->wParam == VK_ESCAPE ||	
			pMsg->wParam == VK_RETURN ||	
			pMsg->wParam == VK_SPACE  ||
			pMsg->wParam == VK_CANCEL )     
			return TRUE;        
	}

	return CDialogEx::PreTranslateMessage(pMsg);
}

void CDlg_Gem::Cal_Area()
{
	CRect ClientRect;
	GetClientRect(&ClientRect);

	int n60pW = (int)((float)ClientRect.Width()*0.60f);

	m_rcLeft.SetRect(ClientRect.left, ClientRect.top, ClientRect.left+n60pW, ClientRect.bottom);
	m_rcRight.SetRect(ClientRect.left+n60pW+CTRL_MARGIN4, ClientRect.top, ClientRect.right, ClientRect.bottom);
}

void CDlg_Gem::Cal_CtrlArea()
{
	Cal_Area();

	Cal_CtrlArea_Left();
	Cal_CtrlArea_Right();

	m_bCtrl_FirstLoad = TRUE;
}

void CDlg_Gem::Cal_CtrlArea_Left()
{
	CRect CalRect;
	CRect rcGB[4], rcGBInside[4], rcDiv5H[5];
	int nTopHeight = 35;
	
	int nDiv4H = (int)((float)m_rcLeft.Height()/4.0f);

	rcGB[0].SetRect(m_rcLeft.left, m_rcLeft.top, m_rcLeft.right, m_rcLeft.top+nDiv4H);
	rcGB[1].SetRect(m_rcLeft.left, rcGB[0].bottom, m_rcLeft.right, rcGB[0].bottom+nDiv4H);
	rcGB[2].SetRect(m_rcLeft.left, rcGB[1].bottom, m_rcLeft.right, rcGB[1].bottom+nDiv4H);
	rcGB[3].SetRect(m_rcLeft.left, rcGB[2].bottom, m_rcLeft.right, m_rcLeft.bottom);

	CalRect.SetRect(rcGB[0].left, rcGB[0].top, rcGB[1].right, rcGB[1].bottom);
	m_edit_3.MoveWindow(&CalRect);

	CalRect.SetRect(rcGB[2].left, rcGB[2].top, rcGB[2].right, rcGB[2].top+30);
	m_edit_4.MoveWindow(&CalRect);
	CalRect.SetRect(rcGB[2].left, rcGB[2].top+30, rcGB[2].right, rcGB[2].bottom);
	m_list_1.MoveWindow(&CalRect);

	m_edit_6.MoveWindow(&rcGB[3]);
}

void CDlg_Gem::Cal_CtrlArea_Right()
{
	CRect CalRect;
	CRect rcTop, rcGB[4], rcGBInside[4], rcDiv5H[5];
	int nDiv5H, n25pW, n50pW;
	int nTopHeight = 35;
	
	rcTop.SetRect(m_rcRight.left, m_rcRight.top, m_rcRight.right, m_rcRight.top+nTopHeight);
	rcTop.DeflateRect(0,2,0,3);
	CalRect.SetRect(m_rcRight.left, m_rcRight.top+nTopHeight, m_rcRight.right, m_rcRight.bottom);

	int nDiv4H = (int)((float)CalRect.Height()/4.0f);

	rcGB[0].SetRect(CalRect.left, CalRect.top, CalRect.right, CalRect.top+nDiv4H);
	rcGB[1].SetRect(CalRect.left, rcGB[0].bottom, CalRect.right, rcGB[0].bottom+nDiv4H);
	rcGB[2].SetRect(CalRect.left, rcGB[1].bottom, CalRect.right, rcGB[1].bottom+nDiv4H);
	rcGB[3].SetRect(CalRect.left, rcGB[2].bottom, CalRect.right, CalRect.bottom);

	for(int i=0;i<4;i++)
	{
		rcGBInside[i] = rcGB[i];
		rcGBInside[i].DeflateRect(6,28,6,6);
	}

	// Top Button 처리
	CalRect.SetRect(rcTop.right-100, rcTop.top, rcTop.right, rcTop.bottom);
	m_btn_2.SetFontBtn(CTRL_FONT, 18, FW_NORMAL);
	m_btn_2.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_2.SetTextColor(CR_BLACK);
	m_btn_2.SetFlatType(TRUE);
	m_btn_2.MoveWindow(&CalRect);
	CalRect.SetRect(CalRect.left-100, rcTop.top, CalRect.left, rcTop.bottom);
	m_btn_1.SetFontBtn(CTRL_FONT, 18, FW_NORMAL);
	m_btn_1.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_1.SetTextColor(CR_BLACK);
	m_btn_1.SetFlatType(TRUE);
	m_btn_1.MoveWindow(&CalRect);

	// Event Report
	m_gb_1.SetXPGroupStyle(CXPGroupBox::XPGB_WINDOW_2);
	m_gb_1.SetBackgroundColor(CR_DARKGRAY, CR_GB_BKGND);
	m_gb_1.SetBorderColor(CR_BLACK);
	m_gb_1.SetCatptionTextColor(CR_BLACK);
	m_gb_1.MoveWindow(&rcGB[0]);

	nDiv5H = (int)((float)rcGBInside[0].Height()/5.0f);
	n25pW = (int)((float)rcGBInside[0].Width()*0.25f);
	n50pW = (int)((float)rcGBInside[0].Width()*0.50f);
	rcDiv5H[0].SetRect(rcGBInside[0].left, rcGBInside[0].top, rcGBInside[0].right, rcGBInside[0].top+nDiv5H);
	rcDiv5H[1].SetRect(rcGBInside[0].left, rcDiv5H[0].bottom, rcGBInside[0].right, rcDiv5H[0].bottom+nDiv5H);
	rcDiv5H[2].SetRect(rcGBInside[0].left, rcDiv5H[1].bottom, rcGBInside[0].right, rcDiv5H[1].bottom+nDiv5H);
	rcDiv5H[3].SetRect(rcGBInside[0].left, rcDiv5H[2].bottom, rcGBInside[0].right, rcDiv5H[2].bottom+nDiv5H);
	rcDiv5H[4].SetRect(rcGBInside[0].left, rcDiv5H[3].bottom, rcGBInside[0].right, rcGBInside[0].bottom);

	CalRect.SetRect(rcDiv5H[0].left, rcDiv5H[0].top, rcDiv5H[0].left+n25pW, rcDiv5H[0].bottom);
	m_st_1.SetBkColor(CR_DARKSLATEGRAY);
	m_st_1.SetTextColor(CR_WHITE);
	m_st_1.SetFont(CTRL_FONT,20,FW_NORMAL);
	m_st_1.MoveWindow(&CalRect);
	CalRect.SetRect(rcDiv5H[0].left+n25pW, rcDiv5H[0].top, rcDiv5H[0].right, rcDiv5H[0].bottom);
	m_cbo_1.MoveWindow(&CalRect);
	m_edit_1.MoveWindow(&rcDiv5H[1]);
	m_edit_2.MoveWindow(&rcDiv5H[2]);
	CalRect.SetRect(rcDiv5H[3].left, rcDiv5H[3].top, rcDiv5H[3].left+n50pW, rcDiv5H[3].bottom);
	m_btn_3.SetFontBtn(CTRL_FONT, 18, FW_NORMAL);
	m_btn_3.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_3.SetTextColor(CR_BLACK);
	m_btn_3.SetFlatType(TRUE);
	m_btn_3.MoveWindow(&CalRect);
	CalRect.SetRect(rcDiv5H[3].left+n50pW, rcDiv5H[3].top, rcDiv5H[3].right, rcDiv5H[3].bottom);
	m_btn_4.SetFontBtn(CTRL_FONT, 18, FW_NORMAL);
	m_btn_4.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_4.SetTextColor(CR_BLACK);
	m_btn_4.SetFlatType(TRUE);
	m_btn_4.MoveWindow(&CalRect);

	// Alarm Report
	m_gb_2.SetXPGroupStyle(CXPGroupBox::XPGB_WINDOW_2);
	m_gb_2.SetBackgroundColor(CR_DARKGRAY, CR_GB_BKGND);
	m_gb_2.SetBorderColor(CR_BLACK);
	m_gb_2.SetCatptionTextColor(CR_BLACK);
	m_gb_2.MoveWindow(&rcGB[1]);

	nDiv5H = (int)((float)rcGBInside[1].Height()/5.0f);
	n25pW = (int)((float)rcGBInside[1].Width()*0.25f);
	n50pW = (int)((float)rcGBInside[1].Width()*0.50f);
	rcDiv5H[0].SetRect(rcGBInside[1].left, rcGBInside[1].top, rcGBInside[1].right, rcGBInside[1].top+nDiv5H);
	rcDiv5H[1].SetRect(rcGBInside[1].left, rcDiv5H[0].bottom, rcGBInside[1].right, rcDiv5H[0].bottom+nDiv5H);
	rcDiv5H[2].SetRect(rcGBInside[1].left, rcDiv5H[1].bottom, rcGBInside[1].right, rcDiv5H[1].bottom+nDiv5H);
	rcDiv5H[3].SetRect(rcGBInside[1].left, rcDiv5H[2].bottom, rcGBInside[1].right, rcDiv5H[2].bottom+nDiv5H);
	rcDiv5H[4].SetRect(rcGBInside[1].left, rcDiv5H[3].bottom, rcGBInside[1].right, rcGBInside[1].bottom);

	CalRect.SetRect(rcDiv5H[0].left, rcDiv5H[0].top, rcDiv5H[0].left+n25pW, rcDiv5H[0].bottom);
	m_st_2.SetBkColor(CR_DARKSLATEGRAY);
	m_st_2.SetTextColor(CR_WHITE);
	m_st_2.SetFont(CTRL_FONT,20,FW_NORMAL);
	m_st_2.MoveWindow(&CalRect);
	CalRect.SetRect(rcDiv5H[0].left+n25pW, rcDiv5H[0].top, rcDiv5H[0].right, rcDiv5H[0].bottom);
	m_cbo_2.MoveWindow(&CalRect);
	CalRect.SetRect(rcDiv5H[1].left, rcDiv5H[1].top, rcDiv5H[1].left+n50pW, rcDiv5H[1].bottom);
	m_btn_5.SetFontBtn(CTRL_FONT, 18, FW_NORMAL);
	m_btn_5.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_5.SetTextColor(CR_BLACK);
	m_btn_5.SetFlatType(TRUE);
	m_btn_5.MoveWindow(&CalRect);
	CalRect.SetRect(rcDiv5H[1].left+n50pW, rcDiv5H[1].top, rcDiv5H[1].right, rcDiv5H[1].bottom);
	m_btn_6.SetFontBtn(CTRL_FONT, 18, FW_NORMAL);
	m_btn_6.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_6.SetTextColor(CR_BLACK);
	m_btn_6.SetFlatType(TRUE);
	m_btn_6.MoveWindow(&CalRect);

	// Recipe Report
	m_gb_3.SetXPGroupStyle(CXPGroupBox::XPGB_WINDOW_2);
	m_gb_3.SetBackgroundColor(CR_DARKGRAY, CR_GB_BKGND);
	m_gb_3.SetBorderColor(CR_BLACK);
	m_gb_3.SetCatptionTextColor(CR_BLACK);
	m_gb_3.MoveWindow(&rcGB[2]);

	nDiv5H = (int)((float)rcGBInside[2].Height()/5.0f);
	n25pW = (int)((float)rcGBInside[2].Width()*0.25f);
	n50pW = (int)((float)rcGBInside[2].Width()*0.50f);
	rcDiv5H[0].SetRect(rcGBInside[2].left, rcGBInside[2].top, rcGBInside[2].right, rcGBInside[2].top+nDiv5H);
	rcDiv5H[1].SetRect(rcGBInside[2].left, rcDiv5H[0].bottom, rcGBInside[2].right, rcDiv5H[0].bottom+nDiv5H);
	rcDiv5H[2].SetRect(rcGBInside[2].left, rcDiv5H[1].bottom, rcGBInside[2].right, rcDiv5H[1].bottom+nDiv5H);
	rcDiv5H[3].SetRect(rcGBInside[2].left, rcDiv5H[2].bottom, rcGBInside[2].right, rcDiv5H[2].bottom+nDiv5H);
	rcDiv5H[4].SetRect(rcGBInside[2].left, rcDiv5H[3].bottom, rcGBInside[2].right, rcGBInside[2].bottom);

	CalRect.SetRect(rcDiv5H[0].left, rcDiv5H[0].top, rcDiv5H[0].left+n25pW, rcDiv5H[0].bottom);
	m_st_3.SetBkColor(CR_DARKSLATEGRAY);
	m_st_3.SetTextColor(CR_WHITE);
	m_st_3.SetFont(CTRL_FONT,20,FW_NORMAL);
	m_st_3.MoveWindow(&CalRect);
	CalRect.SetRect(rcDiv5H[0].left+n25pW, rcDiv5H[0].top, rcDiv5H[0].right, rcDiv5H[0].bottom);
	m_cbo_3.MoveWindow(&CalRect);
	m_st_4.SetBkColor(CR_DARKSLATEGRAY);
	m_st_4.SetTextColor(CR_WHITE);
	m_st_4.SetFont(CTRL_FONT,20,FW_NORMAL);
	m_st_4.MoveWindow(&rcDiv5H[1]);
	CalRect.SetRect(rcDiv5H[2].left, rcDiv5H[2].top, rcDiv5H[2].left+n50pW, rcDiv5H[2].bottom);
	m_btn_7.SetFontBtn(CTRL_FONT, 18, FW_NORMAL);
	m_btn_7.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_7.SetTextColor(CR_BLACK);
	m_btn_7.SetFlatType(TRUE);
	m_btn_7.MoveWindow(&CalRect);

	// Zebra Printer Test
	m_gb_4.SetXPGroupStyle(CXPGroupBox::XPGB_WINDOW_2);
	m_gb_4.SetBackgroundColor(CR_DARKGRAY, CR_GB_BKGND);
	m_gb_4.SetBorderColor(CR_BLACK);
	m_gb_4.SetCatptionTextColor(CR_BLACK);
	m_gb_4.MoveWindow(&rcGB[3]);

	nDiv5H = (int)((float)rcGBInside[3].Height()/5.0f);
	n25pW = (int)((float)rcGBInside[3].Width()*0.25f);
	n50pW = (int)((float)rcGBInside[3].Width()*0.50f);
	rcDiv5H[0].SetRect(rcGBInside[3].left, rcGBInside[3].top, rcGBInside[3].right, rcGBInside[3].top+nDiv5H);
	rcDiv5H[1].SetRect(rcGBInside[3].left, rcDiv5H[0].bottom, rcGBInside[3].right, rcDiv5H[0].bottom+nDiv5H);
	rcDiv5H[2].SetRect(rcGBInside[3].left, rcDiv5H[1].bottom, rcGBInside[3].right, rcDiv5H[1].bottom+nDiv5H);
	rcDiv5H[3].SetRect(rcGBInside[3].left, rcDiv5H[2].bottom, rcGBInside[3].right, rcDiv5H[2].bottom+nDiv5H);
	rcDiv5H[4].SetRect(rcGBInside[3].left, rcDiv5H[3].bottom, rcGBInside[3].right, rcGBInside[3].bottom);

	CalRect.SetRect(rcDiv5H[0].left, rcDiv5H[0].top, rcDiv5H[0].left+n25pW, rcDiv5H[0].bottom);
	m_st_5.SetBkColor(CR_DARKSLATEGRAY);
	m_st_5.SetTextColor(CR_WHITE);
	m_st_5.SetFont(CTRL_FONT,20,FW_NORMAL);
	m_st_5.MoveWindow(&CalRect);
	CalRect.SetRect(rcDiv5H[0].left+n25pW, rcDiv5H[0].top, rcDiv5H[0].right, rcDiv5H[0].bottom);
	m_cbo_4.MoveWindow(&CalRect);
	CalRect.SetRect(rcDiv5H[1].left, rcDiv5H[1].top, rcDiv5H[1].left+n25pW, rcDiv5H[1].bottom);
	m_st_6.SetBkColor(CR_DARKSLATEGRAY);
	m_st_6.SetTextColor(CR_WHITE);
	m_st_6.SetFont(CTRL_FONT,20,FW_NORMAL);
	m_st_6.MoveWindow(&CalRect);
	CalRect.SetRect(rcDiv5H[1].right-n25pW, rcDiv5H[1].top, rcDiv5H[1].right, rcDiv5H[1].bottom);
	m_btn_8.SetFontBtn(CTRL_FONT, 18, FW_NORMAL);
	m_btn_8.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_8.SetTextColor(CR_BLACK);
	m_btn_8.SetFlatType(TRUE);
	m_btn_8.MoveWindow(&CalRect);
	CalRect.SetRect(rcDiv5H[1].left+n25pW, rcDiv5H[1].top, rcDiv5H[1].right-n25pW, rcDiv5H[1].bottom);
	m_cbo_5.MoveWindow(&CalRect);
	m_st_7.SetBkColor(CR_DARKSLATEGRAY);
	m_st_7.SetTextColor(CR_WHITE);
	m_st_7.SetFont(CTRL_FONT,20,FW_NORMAL);
	m_st_7.MoveWindow(&rcDiv5H[2]);
	CalRect.SetRect(rcDiv5H[3].left, rcDiv5H[3].top, rcDiv5H[3].left+n25pW, rcDiv5H[3].bottom);
	m_st_8.SetBkColor(CR_DARKSLATEGRAY);
	m_st_8.SetTextColor(CR_WHITE);
	m_st_8.SetFont(CTRL_FONT,20,FW_NORMAL);
	m_st_8.MoveWindow(&CalRect);
	CalRect.SetRect(CalRect.right, rcDiv5H[3].top, CalRect.right+n25pW, rcDiv5H[3].bottom);
	m_edit_5.MoveWindow(&CalRect);
	CalRect.SetRect(rcDiv5H[4].left, rcDiv5H[4].top, rcDiv5H[4].left+n50pW, rcDiv5H[4].bottom);
	m_btn_9.SetFontBtn(CTRL_FONT, 18, FW_NORMAL);
	m_btn_9.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_9.SetTextColor(CR_BLACK);
	m_btn_9.SetFlatType(TRUE);
	m_btn_9.MoveWindow(&CalRect);
	CalRect.SetRect(rcDiv5H[4].left+n50pW, rcDiv5H[4].top, rcDiv5H[4].right, rcDiv5H[4].bottom);
	m_btn_10.SetFontBtn(CTRL_FONT, 18, FW_NORMAL);
	m_btn_10.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_10.SetTextColor(CR_BLACK);
	m_btn_10.SetFlatType(TRUE);
	m_btn_10.MoveWindow(&CalRect);
}

BEGIN_EVENTSINK_MAP(CDlg_Gem, CDialogEx)
	ON_EVENT(CDlg_Gem, IDC_EXGEM3CTRL1, 27, CDlg_Gem::eXGEMStateEventExgem3ctrl1, VTS_I4)
	ON_EVENT(CDlg_Gem, IDC_EXGEM3CTRL1, 2, CDlg_Gem::eGEMCommStateChangedExgem3ctrl1, VTS_I4)
	ON_EVENT(CDlg_Gem, IDC_EXGEM3CTRL1, 3, CDlg_Gem::eGEMControlStateChangedExgem3ctrl1, VTS_I4)
	ON_EVENT(CDlg_Gem, IDC_EXGEM3CTRL1, 24, CDlg_Gem::eGEMTerminalMessageExgem3ctrl1, VTS_I4 VTS_BSTR)
	ON_EVENT(CDlg_Gem, IDC_EXGEM3CTRL1, 25, CDlg_Gem::eGEMTerminalMultiMessageExgem3ctrl1, VTS_I4 VTS_I4 VTS_PBSTR)
	ON_EVENT(CDlg_Gem, IDC_EXGEM3CTRL1, 6, CDlg_Gem::eGEMReqGetDateTimeExgem3ctrl1, VTS_I4)
	ON_EVENT(CDlg_Gem, IDC_EXGEM3CTRL1, 8, CDlg_Gem::eGEMReqDateTimeExgem3ctrl1, VTS_I4 VTS_BSTR)
	ON_EVENT(CDlg_Gem, IDC_EXGEM3CTRL1, 18, CDlg_Gem::eGEMReqPPListExgem3ctrl1, VTS_I4)
	ON_EVENT(CDlg_Gem, IDC_EXGEM3CTRL1, 10, CDlg_Gem::eGEMReqRemoteCommandExgem3ctrl1, VTS_I4 VTS_BSTR VTS_I4 VTS_PBSTR VTS_PBSTR)
END_EVENTSINK_MAP()


void CDlg_Gem::eXGEMStateEventExgem3ctrl1(long nState)
{
	if (nState == XGemState_Execute) {
		
		m_XGem300.GEMSetEstablish(1);  //bEnable : 0(Disabled), 1(Enabled)
		m_XGem300.GEMEQInitialized(1); /* ?  1 : CarrierID와 관련성이 없는Carrier, PJ, CJ 삭제  
									2 : PJ, CJ만 모두 삭제
									3 : 모든 Object 삭제
									4 : 기존 Data 정보로 Loading*/
	}
}

void CDlg_Gem::eGEMCommStateChangedExgem3ctrl1(long nState)
{
	long	nReturn = 0;
	CString strMsg = L"";

	if(nState == 5) //Communication State -1 : None 1 : Comm Disabled 2 : WaitCRFromHost 3 : WaitDelay 4 : WaitCRA 5 : Communicating
	 {
		
		nReturn = m_XGem300.GEMReqRemote();
		if( nReturn == 0 ) {
			AddGemLog(L"Send GEMReqRemote successfully");
		}
		else {
			strMsg.Format(L"Fail to GEMReqRemote (%d)", nReturn );
			AddGemLog(strMsg);
			GetLog()->Debug(L"(XGEM) %s",strMsg); //에러 log
		}
	 }
}

//S10F3 TERMINAL DISPLAY, SINGLE 수신시 처리
void CDlg_Gem::eGEMTerminalMessageExgem3ctrl1(long nTid, LPCTSTR sMsg)
{
	CString strLog;
	strLog = L"[IN][S10F3] TERMINAL DISPLAY, SINGLE";
	AddGemLog(strLog);

	CString strMsg;
	strMsg.Format(L"[%d] %s",nTid, sMsg);

	MessageBox(strMsg,L"Terminal Display (single)");
}

//S10F5  TERMINAL DISPLAY, MULTI-BLOCK 수신시 처리
void CDlg_Gem::eGEMTerminalMultiMessageExgem3ctrl1(long nTid, long nCount, BSTR* psMsg)
{
	CString strLog;
	strLog = L"[IN][S10F5] TERMINAL DISPLAY, MULTI-BLOCK";
	AddGemLog(strLog);

	int i=0;
	CString strTemp = L"";
	for( i = 0; i < nCount; i++) {
		strTemp += psMsg[i];
		strTemp += L"\r\n";
	}
	CString strMsg;
	strMsg.Format(L"[%d] %s", nTid, strTemp);
	
	MessageBox(strMsg,L"Terminal Display (multi-block)");
}

//S2F17 DATE AND TIME REQUEST 수신시 처리
void CDlg_Gem::eGEMReqGetDateTimeExgem3ctrl1(long nMsgId)
{
	CString strLog;
	strLog = L"[IN][S2F17] DATE AND TIME REQUEST";
	AddGemLog(strLog);

	CString strCurrentTime;
	CTime CurTime;
	
	CurTime =  CTime::GetCurrentTime(); 
	strCurrentTime.Format(L"%d%02d%02d%02d%02d%02d", CurTime.GetYear(), CurTime.GetMonth(), CurTime.GetDay(), 
														CurTime.GetHour(), CurTime.GetMinute(), CurTime.GetSecond());
	int nRet = m_XGem300.GEMRspGetDateTime(nMsgId, strCurrentTime);

	if(nRet == 0)
	{
		strLog = L"[OUT][S2F18]DATE AND TIME RESPONSE";
		AddGemLog(strLog);
	}
	else
	{
		strLog = L"[ERROR][OUT][S7F18] DATE AND TIME RESPONSE fail";
		AddGemLog(strLog);
		GetLog()->Debug(L"(XGEM) %s",strLog); //에러 log
	}
}

//S2F31(H->E) Date and Time Set Request (DTS) 수신시 처리
void CDlg_Gem::eGEMReqDateTimeExgem3ctrl1(long nMsgId, LPCTSTR sSystemTime)
{
	CString strLog;
	strLog = L"[IN][S2F31] Date and Time Set Request";
	AddGemLog(strLog);

	int nRet = m_XGem300.GEMRspDateTime(nMsgId, 0); //응답 및 TimeSync  - UAC 실행 수준 requireAdministrator 

	if(nRet == 0)
	{
		strLog = L"[OUT][S2F32] Date and Time Set";
		AddGemLog(strLog);
	}
	else
	{
		strLog = L"[ERROR][OUT][S7F20] Date and Time Set fail";
		AddGemLog(strLog);
		GetLog()->Debug(L"(XGEM) %s",strLog); //에러 log
	}
}

//S7F19 CURRENT EPPD REQUEST 수신시 처리
void CDlg_Gem::eGEMReqPPListExgem3ctrl1(long nMsgId)
{
	CString strLog;
	strLog = L"[IN][S7F19] CURRENT EPPD REQUEST";
	AddGemLog(strLog);

	CString strValue;
	BSTR bstr[RECIPE_COUNT];
	
	int i;

	for(i = 0; i < RECIPE_COUNT; i++)
	{
		strValue = RecipeID[i];
		bstr[i] = strValue.AllocSysString();
	}

	int nRet = m_XGem300.GEMRspPPList(nMsgId,RECIPE_COUNT, &bstr[0]);
	
	if(nRet == 0)
	{
		strLog = L"[OUT][S7F20] RECIPE LIST SEND";
		AddGemLog(strLog);
	}
	else
	{
		strLog = L"[ERROR][OUT][S7F20] RECIPE LIST SEND fail";
		AddGemLog(strLog);
		GetLog()->Debug(L"(XGEM) %s",strLog); //에러 log
	}
}

void CDlg_Gem::StartXGem300() //
{
	long		nReturn = 0;
	CString     strMsg;

	CString strPath = CAppSetting::Instance()->GetExecuteDirectory();
	strPath += _T("\\Setting\\");
	strPath += _T("AMKOR_TrayPacking.cfg");

	nReturn = m_XGem300.Initialize(strPath);

	if( nReturn == 0 ) 
	{
        AddGemLog(L"XGem initialized successfully");
    }
    else 
	{
		if(nReturn == -10012) ////EHI_READ_CONFIG -10012
		{
			strMsg.Format(L"Fail to initialize XGem (%d) - Fail to read config file. \r\n %s 파일을 읽기 실패하였습니다. \r\n"
				          L"cfg 파일의 경로와 item을 확인하기 바랍니다.", nReturn,strPath );
		}
		else
		{
			strMsg.Format(L"Fail to initialize XGem (%d)", nReturn ); 
		}
        AddGemLog(strMsg);
		GetLog()->Debug(L"(XGEM) %s",strMsg); //에러 log
		MessageBox(strMsg,L"SECS/GEM Driver Start Error!",MB_ICONSTOP);
		return;
    }

	nReturn = m_XGem300.Start();

	if( nReturn == 0 ) 
	{
		CDataManager::Instance()->m_stMCHINFO.m_stConnStatus_Info.m_bConnectStatus[CONN_MES] = TRUE;
		AfxGetMainWnd()->SendMessage(WM_UPDATE_UI_CONN, 0, 0);

		AddGemLog(L"XGem started successfully" );
    }
    else 
	{
		CDataManager::Instance()->m_stMCHINFO.m_stConnStatus_Info.m_bConnectStatus[CONN_MES] = FALSE;
		AfxGetMainWnd()->SendMessage(WM_UPDATE_UI_CONN, 0, 0);

		strMsg.Format(L"Fail to start XGem (%d)", nReturn );
        AddGemLog(strMsg);
		GetLog()->Debug(L"(XGEM) %s",strMsg); //에러 log
		MessageBox(strMsg,L"SECS/GEM Driver Start Error!",MB_ICONSTOP);
    }
}

void CDlg_Gem::StopXGem300()
{
	long		nReturn = 0;
	CString     strMsg;

	nReturn = m_XGem300.Stop();

	if( nReturn == 0 ) 
	{
        AddGemLog(L"XGem stopped successfully");
    }
    else 
	{
		strMsg.Format(L"Fail to stop XGem (%d)", nReturn);
        AddGemLog(strMsg);
		GetLog()->Debug(L"(XGEM) %s",strMsg); //에러 log
    }

	nReturn = m_XGem300.Close();

	if( nReturn == 0 ) 
	{
        AddGemLog(L"XGem closed successfully");
    }
    else 
	{
        strMsg.Format(L"Fail to close XGem (%d)", nReturn );
        AddGemLog(strMsg);
		GetLog()->Debug(L"(XGEM) %s",strMsg); //에러 log
    }

	CDataManager::Instance()->m_stMCHINFO.m_stConnStatus_Info.m_bConnectStatus[CONN_MES] = FALSE;
	AfxGetMainWnd()->SendMessage(WM_UPDATE_UI_CONN, 0, 0);
}

void CDlg_Gem::OnBnClickedBtnGem1() //"START"
{
	StartXGem300();
}


void CDlg_Gem::OnBnClickedBtnGem2() //"STOP"
{
	StopXGem300();
}


void CDlg_Gem::OnBnClickedBtnGem3() //"Add n Set SVID Value"
{
	int nSel = m_cbo_1.GetCurSel();
	if(nSel <= 0)
	{
		AfxMessageBox(L"Event를 선택하세요.");
		return;
	}

	int nCnt = m_list_1.GetCount();
	
	CString str;

	if(m_nInputCnt > 0)
	{
		UpdateData();

		if(nSel == 1) //
		{
			str.Format(_T("[%d %s]%s"), SVID_LIST_1000[nCnt], ITEM_NAME_1000[nCnt], m_strInput);
		}
		else if(nSel == 2) //
		{
			str.Format(_T("[%d %s]%s"), SVID_LIST_1100[nCnt], ITEM_NAME_1100[nCnt], m_strInput);
		}
		else if(nSel == 3) //
		{
			str.Format(_T("[%d %s]%s"), SVID_LIST_1200[nCnt], ITEM_NAME_1200[nCnt], m_strInput);
		}
		else if(nSel == 4) //
		{
			str.Format(_T("[%d %s]%s"), SVID_LIST_1300[nCnt], ITEM_NAME_1300[nCnt], m_strInput);
		}
		else if(nSel == 5) //
		{
			str.Format(_T("[%d %s]%s"), SVID_LIST_1400[nCnt], ITEM_NAME_1400[nCnt], m_strInput);
		}
		else if(nSel == 6) //
		{
			str.Format(_T("[%d %s]%s"), SVID_LIST_1500[nCnt], ITEM_NAME_1500[nCnt], m_strInput);
		}
		else if(nSel == 7) //
		{
			str.Format(_T("[%d %s]%s"), SVID_LIST_1600[nCnt], ITEM_NAME_1600[nCnt], m_strInput);
		}
		else if(nSel == 8) //
		{
			str.Format(_T("[%d %s]%s"), SVID_LIST_1700[nCnt], ITEM_NAME_1700[nCnt], m_strInput);
		}
		else if(nSel == 9) //
		{
			str.Format(_T("[%d %s]%s"), SVID_LIST_1800[nCnt], ITEM_NAME_1800[nCnt], m_strInput);
		}
		else if(nSel == 10) //
		{
			str.Format(_T("[%d %s]%s"), SVID_LIST_1900[nCnt], ITEM_NAME_1900[nCnt], m_strInput);
		}
		else if(nSel == 11) //
		{
			str.Format(_T("[%d %s]%s"), SVID_LIST_901[nCnt], ITEM_NAME_901[nCnt], m_strInput);
		}
		else if(nSel == 12) //
		{
			str.Format(_T("[%d %s]%s"), SVID_LIST_902[nCnt], ITEM_NAME_902[nCnt], m_strInput);
		}
		else if(nSel == 13) //
		{
			str.Format(_T("[%d %s]%s"), SVID_LIST_903[nCnt], ITEM_NAME_903[nCnt], m_strInput);
		}
		else if(nSel == 14) //
		{
			str.Format(_T("[%d %s]%s"), SVID_LIST_10[nCnt], ITEM_NAME_10[nCnt], m_strInput);
		}
		/////
		//////////////////////////////////////////////////////////////////////
	
		if(nCnt == 0)
		{
			m_list_1.InsertString(0,str);
		}
		else
		{
			m_list_1.AddString(str);
		}

		m_list_1.SetCurSel(nCnt);
		m_strInput.Empty();

		UpdateData(FALSE);

		m_edit_1.SetFocus();

		m_nInputCnt--;

		if(m_nInputCnt > 0)
		{
			if(nSel == 1) // (1000)
			{
				str.Format(_T("%d %s"), SVID_LIST_1000[nCnt+1], ITEM_NAME_1000[nCnt+1]);
				m_strInput = TEST_DATA_1000[nCnt+1];
			}
			else if(nSel == 2) // (1100)
			{
				str.Format(_T("%d %s"), SVID_LIST_1100[nCnt+1], ITEM_NAME_1100[nCnt+1]);
				m_strInput = TEST_DATA_1100[nCnt+1];
			}
			else if(nSel == 3) // (1200)
			{
				str.Format(_T("%d %s"), SVID_LIST_1200[nCnt+1], ITEM_NAME_1200[nCnt+1]);
				m_strInput = TEST_DATA_1200[nCnt+1];
			}
			else if(nSel == 4) // (1300)
			{
				str.Format(_T("%d %s"), SVID_LIST_1300[nCnt+1], ITEM_NAME_1300[nCnt+1]);
				m_strInput = TEST_DATA_1300[nCnt+1];
			}
			else if(nSel == 5) // (1400)
			{
				str.Format(_T("%d %s"), SVID_LIST_1400[nCnt+1], ITEM_NAME_1400[nCnt+1]);
				m_strInput = TEST_DATA_1400[nCnt+1];
			}
			else if(nSel == 6) // (1500)
			{
				str.Format(_T("%d %s"), SVID_LIST_1500[nCnt+1], ITEM_NAME_1500[nCnt+1]);
				m_strInput = TEST_DATA_1500[nCnt+1];
			}
			else if(nSel == 7) // (1600)
			{
				str.Format(_T("%d %s"), SVID_LIST_1600[nCnt+1], ITEM_NAME_1600[nCnt+1]);
				m_strInput = TEST_DATA_1600[nCnt+1];
			}
			else if(nSel == 8) // (1700)
			{
				str.Format(_T("%d %s"), SVID_LIST_1700[nCnt+1], ITEM_NAME_1700[nCnt+1]);
				m_strInput = TEST_DATA_1700[nCnt+1];
			}
			else if(nSel == 9) // (1800)
			{
				str.Format(_T("%d %s"), SVID_LIST_1800[nCnt+1], ITEM_NAME_1800[nCnt+1]);
				m_strInput = TEST_DATA_1800[nCnt+1];
			}
			else if(nSel == 10) // (1900)
			{
				str.Format(_T("%d %s"), SVID_LIST_1900[nCnt+1], ITEM_NAME_1900[nCnt+1]);
				m_strInput = TEST_DATA_1900[nCnt+1];
			}
			else if(nSel == 11) // (901)
			{
				str.Format(_T("%d %s"), SVID_LIST_901[nCnt+1], ITEM_NAME_901[nCnt+1]);
				m_strInput = TEST_DATA_901[nCnt+1];
			}
			else if(nSel == 12) // (902)
			{
				str.Format(_T("%d %s"), SVID_LIST_902[nCnt+1], ITEM_NAME_902[nCnt+1]);
				m_strInput = TEST_DATA_901[nCnt+1];
			}
			else if(nSel == 13) // (903)
			{
				str.Format(_T("%d %s"), SVID_LIST_903[nCnt+1], ITEM_NAME_903[nCnt+1]);
				m_strInput = TEST_DATA_901[nCnt+1];
			}
			else if(nSel == 14) // (10)
			{
				str.Format(_T("%d %s"), SVID_LIST_10[nCnt+1], ITEM_NAME_10[nCnt+1]);
				m_strInput = TEST_DATA_10[nCnt+1];
			}
		}
		//////////////////////////////////////////////////////////////////////////
		m_strSVID = str;
	}
	else
	{
		m_strSVID = _T("SVID 입력 완료");
	}

	UpdateData(FALSE);
}


void CDlg_Gem::OnBnClickedBtnGem4() //"Send Event (S6F11)"
{
	int nIdx = m_cbo_1.GetCurSel();
	if(nIdx <= 0)
	{
		AfxMessageBox(L"Event를 선택하세요.");
		return;
	}

	long nReturn = 0;
	CString strMsg = L"";

	CString strTemp;
	int nCnt = 0;  
	long nVID,nCEID;
	CString strValue;

	if(nIdx == 1) //READY_TO_LOAD_REQUEST (1000)
	{
		nCEID = 1000; //(1000)
		BSTR bstr;
		while(nCnt < EventReportSVIDcnt[0])
		{
			m_list_1.GetText(nCnt,strValue);
			//Set SVID Value
			nIdx = strValue.Find(_T("]"));
			strValue = strValue.Mid(nIdx+1);

			nVID = SVID_LIST_1000[nCnt];

			bstr = strValue.AllocSysString();
			nReturn = m_XGem300.GEMSetVariable(1, &nVID, &bstr);
			SysFreeString(bstr);

			if( nReturn != 0 )  
			{
				strMsg.Format(L"Fail to GEMSetVariable-%d CEID(%d) SVID[%d]", nReturn,nCEID,nVID );
				AddGemLog(strMsg);
			}
			nCnt++;
		}

	}
	else if(nIdx == 2) //MATERIAL_STATUS_REPORT (1100)
	{
		nCEID = 1100; 
		BSTR bstr;
		while(nCnt < EventReportSVIDcnt[1])
		{
			m_list_1.GetText(nCnt,strValue);
			//Set SVID Value
			nIdx = strValue.Find(_T("]"));
			strValue = strValue.Mid(nIdx+1);

			nVID = SVID_LIST_1100[nCnt];

			bstr = strValue.AllocSysString();
			nReturn = m_XGem300.GEMSetVariable(1, &nVID, &bstr);
			SysFreeString(bstr);

			if( nReturn != 0 )  
			{
				strMsg.Format(L"Fail to GEMSetVariable-%d CEID(%d) SVID[%d]", nReturn,nCEID,nVID );
				AddGemLog(strMsg);
			}
			nCnt++;
		}
	}
	else if(nIdx == 3) //RFID_FULL_REPORT (1200)
	{
		nCEID = 1200; 
		BSTR bstr;
		while(nCnt < EventReportSVIDcnt[2])
		{
			m_list_1.GetText(nCnt,strValue);
			//Set SVID Value
			nIdx = strValue.Find(_T("]"));
			strValue = strValue.Mid(nIdx+1);

			nVID = SVID_LIST_1200[nCnt];

			bstr = strValue.AllocSysString();
			nReturn = m_XGem300.GEMSetVariable(1, &nVID, &bstr);
			SysFreeString(bstr);

			if( nReturn != 0 )  
			{
				strMsg.Format(L"Fail to GEMSetVariable-%d CEID(%d) SVID[%d]", nReturn,nCEID,nVID );
				AddGemLog(strMsg);
			}
			nCnt++;
		}
	}
	else if(nIdx == 4) //READY_TO_LOAD_BARCODE_REQUEST (1300)
	{
		nCEID = 1300; 
		BSTR bstr;
		while(nCnt < EventReportSVIDcnt[3])
		{
			m_list_1.GetText(nCnt,strValue);
			//Set SVID Value
			nIdx = strValue.Find(_T("]"));
			strValue = strValue.Mid(nIdx+1);

			nVID = SVID_LIST_1300[nCnt];

			bstr = strValue.AllocSysString();
			nReturn = m_XGem300.GEMSetVariable(1, &nVID, &bstr);
			SysFreeString(bstr);

			if( nReturn != 0 )  
			{
				strMsg.Format(L"Fail to GEMSetVariable-%d CEID(%d) SVID[%d]", nReturn,nCEID,nVID );
				AddGemLog(strMsg);
			}
			nCnt++;
		}
	}
	else if(nIdx == 5) //LOT_START_REQUEST (1400)
	{
		nCEID = 1400; 
		BSTR bstr;
		while(nCnt < EventReportSVIDcnt[4])
		{
			m_list_1.GetText(nCnt,strValue);
			//Set SVID Value
			nIdx = strValue.Find(_T("]"));
			strValue = strValue.Mid(nIdx+1);

			nVID = SVID_LIST_1400[nCnt];

			bstr = strValue.AllocSysString();
			nReturn = m_XGem300.GEMSetVariable(1, &nVID, &bstr);
			SysFreeString(bstr);

			if( nReturn != 0 )  
			{
				strMsg.Format(L"Fail to GEMSetVariable-%d CEID(%d) SVID[%d]", nReturn,nCEID,nVID );
				AddGemLog(strMsg);
			}
			nCnt++;
		}
	}
	else if(nIdx == 6) //BOX_PROC_RESULT_INFO (1500)
	{
		nCEID = 1500; 
		BSTR bstr;
		while(nCnt < EventReportSVIDcnt[5])
		{
			m_list_1.GetText(nCnt,strValue);
			//Set SVID Value
			nIdx = strValue.Find(_T("]"));
			strValue = strValue.Mid(nIdx+1);

			nVID = SVID_LIST_1500[nCnt];

			bstr = strValue.AllocSysString();
			nReturn = m_XGem300.GEMSetVariable(1, &nVID, &bstr);
			SysFreeString(bstr);

			if( nReturn != 0 )  
			{
				strMsg.Format(L"Fail to GEMSetVariable-%d CEID(%d) SVID[%d]", nReturn,nCEID,nVID );
				AddGemLog(strMsg);
			}
			nCnt++;
		}
	}
	else if(nIdx == 7) // BOX_END_REQUEST (1600)
	{
		nCEID = 1600; //
		BSTR bstr;
		while(nCnt < EventReportSVIDcnt[6])
		{
			m_list_1.GetText(nCnt,strValue);
			//Set SVID Value
			nIdx = strValue.Find(_T("]"));
			strValue = strValue.Mid(nIdx+1);

			nVID = SVID_LIST_1600[nCnt];

			bstr = strValue.AllocSysString();
			nReturn = m_XGem300.GEMSetVariable(1, &nVID, &bstr);
			SysFreeString(bstr);

			if( nReturn != 0 )  
			{
				strMsg.Format(L"Fail to GEMSetVariable-%d CEID(%d) SVID[%d]", nReturn,nCEID,nVID );
				AddGemLog(strMsg);
			}
			nCnt++;
		}
	}
	else if(nIdx == 8) // READY_TO_UNLOAD_REQUEST (1700)
	{
		nCEID = 1700;
		BSTR bstr;
		while(nCnt < EventReportSVIDcnt[7])
		{
			m_list_1.GetText(nCnt,strValue);
			//Set SVID Value
			nIdx = strValue.Find(_T("]"));
			strValue = strValue.Mid(nIdx+1);

			nVID = SVID_LIST_1700[nCnt];

			bstr = strValue.AllocSysString();
			nReturn = m_XGem300.GEMSetVariable(1, &nVID, &bstr);
			SysFreeString(bstr);

			if( nReturn != 0 )  
			{
				strMsg.Format(L"Fail to GEMSetVariable-%d CEID(%d) SVID[%d]", nReturn,nCEID,nVID );
				AddGemLog(strMsg);
			}
			nCnt++;
		}
	}
	else if(nIdx == 9) // LOT_END_REQUEST (1800)
	{
		nCEID = 1800;
		BSTR bstr;
		while(nCnt < EventReportSVIDcnt[8])
		{
			m_list_1.GetText(nCnt,strValue);
			//Set SVID Value
			nIdx = strValue.Find(_T("]"));
			strValue = strValue.Mid(nIdx+1);

			nVID = SVID_LIST_1800[nCnt];

			bstr = strValue.AllocSysString();
			nReturn = m_XGem300.GEMSetVariable(1, &nVID, &bstr);
			SysFreeString(bstr);

			if( nReturn != 0 )  
			{
				strMsg.Format(L"Fail to GEMSetVariable-%d CEID(%d) SVID[%d]", nReturn,nCEID,nVID );
				AddGemLog(strMsg);
			}
			nCnt++;
		}
	}
	else if(nIdx == 10) // ZEBRA_CODE_REQUEST (1900)
	{
		nCEID = 1900;
		BSTR bstr;
		while(nCnt < EventReportSVIDcnt[9])
		{
			m_list_1.GetText(nCnt,strValue);
			//Set SVID Value
			nIdx = strValue.Find(_T("]"));
			strValue = strValue.Mid(nIdx+1);

			nVID = SVID_LIST_1900[nCnt];

			bstr = strValue.AllocSysString();
			nReturn = m_XGem300.GEMSetVariable(1, &nVID, &bstr);
			SysFreeString(bstr);

			if( nReturn != 0 )  
			{
				strMsg.Format(L"Fail to GEMSetVariable-%d CEID(%d) SVID[%d]", nReturn,nCEID,nVID );
				AddGemLog(strMsg);
			}
			nCnt++;
		}
	}
	else if(nIdx == 11) // RUN (901)
	{
		nCEID = 901;
		BSTR bstr;
		while(nCnt < EventReportSVIDcnt[10])
		{
			m_list_1.GetText(nCnt,strValue);
			//Set SVID Value
			nIdx = strValue.Find(_T("]"));
			strValue = strValue.Mid(nIdx+1);

			nVID = SVID_LIST_901[nCnt];

			bstr = strValue.AllocSysString();
			nReturn = m_XGem300.GEMSetVariable(1, &nVID, &bstr);
			SysFreeString(bstr);

			if( nReturn != 0 )  
			{
				strMsg.Format(L"Fail to GEMSetVariable-%d CEID(%d) SVID[%d]", nReturn,nCEID,nVID );
				AddGemLog(strMsg);
			}
			nCnt++;
		}
	}
	else if(nIdx == 12) // STOP (902)
	{
		nCEID = 902;
		BSTR bstr;
		while(nCnt < EventReportSVIDcnt[11])
		{
			m_list_1.GetText(nCnt,strValue);
			//Set SVID Value
			nIdx = strValue.Find(_T("]"));
			strValue = strValue.Mid(nIdx+1);

			nVID = SVID_LIST_902[nCnt];

			bstr = strValue.AllocSysString();
			nReturn = m_XGem300.GEMSetVariable(1, &nVID, &bstr);
			SysFreeString(bstr);

			if( nReturn != 0 )  
			{
				strMsg.Format(L"Fail to GEMSetVariable-%d CEID(%d) SVID[%d]", nReturn,nCEID,nVID );
				AddGemLog(strMsg);
			}
			nCnt++;
		}
	}
	else if(nIdx == 13) // IDLE (903)
	{
		nCEID = 903;
		BSTR bstr;
		while(nCnt < EventReportSVIDcnt[12])
		{
			m_list_1.GetText(nCnt,strValue);
			//Set SVID Value
			nIdx = strValue.Find(_T("]"));
			strValue = strValue.Mid(nIdx+1);

			nVID = SVID_LIST_903[nCnt];

			bstr = strValue.AllocSysString();
			nReturn = m_XGem300.GEMSetVariable(1, &nVID, &bstr);
			SysFreeString(bstr);

			if( nReturn != 0 )  
			{
				strMsg.Format(L"Fail to GEMSetVariable-%d CEID(%d) SVID[%d]", nReturn,nCEID,nVID );
				AddGemLog(strMsg);
			}
			nCnt++;
		}
	}
	else if(nIdx == 14) // Process Recipe Selected (10)
	{
		nCEID = 10;
		BSTR bstr;
		while(nCnt < EventReportSVIDcnt[13])
		{
			m_list_1.GetText(nCnt,strValue);
			//Set SVID Value
			nIdx = strValue.Find(_T("]"));
			strValue = strValue.Mid(nIdx+1);

			nVID = SVID_LIST_10[nCnt];

			bstr = strValue.AllocSysString();
			nReturn = m_XGem300.GEMSetVariable(1, &nVID, &bstr);
			SysFreeString(bstr);

			if( nReturn != 0 )  
			{
				strMsg.Format(L"Fail to GEMSetVariable-%d CEID(%d) SVID[%d]", nReturn,nCEID,nVID );
				AddGemLog(strMsg);
			}
			nCnt++;
		}
	}

	nReturn = m_XGem300.GEMSetEvent(nCEID);
	if( nReturn == 0 ) {
		strMsg.Format(L"[OUT][S6F11] (%d) - Send GEMSetEvent successfully", nCEID);
		AddGemLog(strMsg);
    }
    else {
		strMsg.Format(L"[ERROR][OUT][S6F11] (%d) Fail to GEMSetEvent (%d)", nCEID, nReturn);
        AddGemLog(strMsg);
    }
}


void CDlg_Gem::OnBnClickedBtnGem5() //"Set Alarm (S5F1)"
{
	int nIdx = m_cbo_2.GetCurSel();
	if(nIdx <= 0)
	{
		AfxMessageBox(L"Alarm을 선택하세요.");
		return;
	}
	int nALID = 0;
	switch(nIdx)
	{
		case 1:
			nALID = 2000; //test
			break;
		default:
			return;
	}

	int nRet = m_XGem300.GEMSetAlarm(nALID, 1); //state (0 : clear, 1: detect)
	CString strMsg;
	if(nRet == 0)
	{
		strMsg.Format(L"[OUT][S5F1] %d (Alarm Set)", nALID);
		AddGemLog(strMsg);
	}
	else
	{
		strMsg.Format(L"[ERROR][OUT][S5F1] %d (Alarm Set) fail", nALID);
		AddGemLog(strMsg);
	}
}


void CDlg_Gem::OnBnClickedBtnGem6() //"Clear Alarm (S5F1)"
{
	int nIdx = m_cbo_2.GetCurSel();
	if(nIdx <= 0)
	{
		AfxMessageBox(L"Alarm을 선택하세요.");
		return;
	}
	int nALID = 0;
	switch(nIdx)
	{
		case 1:
			nALID = 2000; //test
			break;
		default:
			return;
	}
	int nRet = m_XGem300.GEMSetAlarm(nALID, 0);
	CString strMsg;
	if(nRet == 0)
	{
		strMsg.Format(L"[OUT][S5F1] %d (Alarm Clear)", nALID);
		AddGemLog(strMsg);
	}
	else
	{
		strMsg.Format(L"[ERROR][OUT][S5F1] %d (Alarm Clear) fail", nALID);
		AddGemLog(strMsg);
	}
}


void CDlg_Gem::OnBnClickedBtnGem7() //"Recipe Load (S7F1)"
{
//for test
//ReadyToLoadReq();
//MaterialStatRpt(_T("T,T,T,T,T,T,T,T,T,T,T,T,T,T,T,T,T,T,T,T,T,T,T,T,T,T,T,T,T,T"), _T("T,T,T,T,T,T,T,T"), _T("T"), _T("T"), _T("T,T"), 
//								_T("T"), _T("T"), _T("T,T,T,T,T"), _T("T,T,T,T,T"), _T("T"));
//	RFID_FullRpt(L"T");
//	LotStartReq(L"TEST BARCODE",L"LOTID",5000,10, 10); 
//BoxProcResultInfoRpt(L"1", 200, L"3", L"4", 500, 
//								L"6", L"7"); //(1500)
	//BoxEndReq(L"1", L"2", 300, L"4", 500, 
	//				  L"6"); //(1600)
	//ReadyToUnloadReq(L"1",L"2",300);
	//LotEndReq(L"1",L"2",300,400);
	//ZebraCodeReq(L"1");
	//EquipStatRpt(L"IDLE");

	//AlarmReportSend(2000,1);
	
	
}

void CDlg_Gem::OnBnClickedBtnGem8() //"Sample Print"
{
	int nIdx = m_cbo_4.GetCurSel();
	if(nIdx <= 0)
	{
		AfxMessageBox(L"ZEBRA프린터를 선택하세요.");
		return;
	}
	
	CString strIP;
	switch (nIdx)
	{
		case 1:
			strIP = CAppSetting::Instance()->GetIP_ZPL_Printer(0).GetIPAddressString();
			break;
		case 2:
			strIP = CAppSetting::Instance()->GetIP_ZPL_Printer(1).GetIPAddressString();
			break;
		case 3:
			strIP = CAppSetting::Instance()->GetIP_ZPL_Printer(2).GetIPAddressString();
			break;
		case 4:
			strIP = CAppSetting::Instance()->GetIP_ZPL_Printer(3).GetIPAddressString();
			break;
		case 5:
			strIP = CAppSetting::Instance()->GetIP_ZPL_Printer(4).GetIPAddressString();
			break;
		default:
			break;
	}

//CAppSetting::Instance()->GetIP_ZPL_Printer(0).GetIPAddressString()
//CAppSetting::Instance()->GetPort_ZPL_Printer(0)

	////CString strZPL = L"^XA^LH0,0^PO^FS^FO010,030^A0,30,25^FDSUPPLIER:SK hynix^FS^PRC^FO250,010^FR^XGHALOGEN_PKT,1,1^FS^FO450,030^A0,30,30^FD^FS^FO010,060^A0,30,30^FDDEVICE     :H9CKNNN8KTARKR-NTH^FS^FO010,085^A0,35,35^FDLOT NO. :T3LCZ88D     ( 1 / 5 )^FS^BY2,2.5:1,60 ^FS^FO035,115^B3,N,40,Y,N,^FDT3LCZ88D     001^FS^FO010,180^A0,22,22^FDITEM NO :120266-069(A3-3ALSHH0F-TAI-5055)^FS^BY2,2.3:1,60 ^FS^FO035,200^B3,N,40,Y,N,^FD120266-069  960^FS^FO010,300^A0,24,24^FDDATE CODE:1524^FS^FO010,265^A0,35,35^FDQ'TY: 960 EA^FS^FO010,325^A0,24,24^FDPACK DATE:2015/06/13^FS^FO490,170^FR^XGON7PCSKH,1,1^FS^FO010,350^A0,20,20^FDASSEMBLED IN KOREA FROM WAFER FABRICATED IN KOREA^FS^FO253,265^A0,16,16^FDAdditional information^FS^FO253,280^B7N,5,2,6,1,N^FDSK hynix            H9CKNNN8KTARKR-NTH            KR_KR     960    1524   T3LCZ88D            001  ^FS^XZ";
	//nIdx = m_ctlComboZPL.GetCurSel();
	//if(nIdx <= 0)
	//{
	//	AfxMessageBox(L"ZPL을 선택하세요.");
	//	return;
	//}
	//CString strZPL = TEST_ZPL[nIdx-1];
	//if(m_strZEBRA_IP == ZEBRA_IP_5)
	//{
	//	CString str = ModifyLabelHome(strZPL);
	//	PrintLabel(str);
	//}
	//else{
	//	PrintLabel(strZPL);
	//}
}

void CDlg_Gem::OnBnClickedBtnGem9() //
{
	;
}


//

void CDlg_Gem::OnBnClickedBtnGem10() //
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	AlarmReportSend(2000,0);
}

void CDlg_Gem::AddGemLog(CString strEvent)
{
	SYSTEMTIME st;
	GetLocalTime(&st);
	CString strLog;
	strLog.Format( _T("%04d-%02d-%02d %02d:%02d:%02d %s\r\n"),
			st.wYear, st.wMonth, st.wDay, st.wHour, st.wMinute, st.wSecond, strEvent);

	m_strEditXGem += strLog;
	UpdateData(FALSE);

	m_edit_3.LineScroll(m_edit_3.GetLineCount());

	if(m_edit_3.GetLineCount() > 1000)
	{
		m_strEditXGem= L"이전 Event는 Log파일을 참조하세요.";
		m_strEditXGem += L"\r\n";
	}
}

void CDlg_Gem::AddZebraLog(CString strEvent)
{
	SYSTEMTIME st;
	GetLocalTime(&st);
	CString strLog;
	strLog.Format( _T("%04d-%02d-%02d %02d:%02d:%02d %s\r\n"),
			st.wYear, st.wMonth, st.wDay, st.wHour, st.wMinute, st.wSecond, strEvent);

	m_strEditZebra += strLog;
	UpdateData(FALSE);

	m_edit_6.LineScroll(m_edit_6.GetLineCount());

}

void CDlg_Gem::OnCbnCloseupCboGem1() //이벤트 콤보박스 선택
{
	int nIdx = m_cbo_1.GetCurSel();

	CString strTemp;
	int nCnt = 0;  
	m_strEventSVIDs.Empty();

	m_list_1.ResetContent();

	if(nIdx == 1) //READY_TO_LOAD_REQUEST
	{
		while(nCnt < EventReportSVIDcnt[0]) // 
		{
			strTemp.Format(_T("%d "),SVID_LIST_1000[nCnt]);
			m_strEventSVIDs.Append(strTemp);
			nCnt++;
		}

		strTemp.Format(_T("%d %s"),SVID_LIST_1000[0],ITEM_NAME_1000[0]);
		m_strSVID = strTemp;	

		m_nInputCnt = EventReportSVIDcnt[0];

		m_strInput = TEST_DATA_1000[0];
	}
	else if(nIdx == 2) //MATERIAL_STATUS_REPORT
	{
		while(nCnt < EventReportSVIDcnt[1]) // 
		{
			strTemp.Format(_T("%d "),SVID_LIST_1100[nCnt]);
			m_strEventSVIDs.Append(strTemp);
			nCnt++;
		}

		strTemp.Format(_T("%d %s"),SVID_LIST_1100[0],ITEM_NAME_1100[0]);
		m_strSVID = strTemp;	

		m_nInputCnt = EventReportSVIDcnt[1];

		m_strInput = TEST_DATA_1100[0];
	}
	else if(nIdx == 3) //RFID_FULL_REPORT
	{
		while(nCnt < EventReportSVIDcnt[2]) // 
		{
			strTemp.Format(_T("%d "),SVID_LIST_1200[nCnt]);
			m_strEventSVIDs.Append(strTemp);
			nCnt++;
		}

		strTemp.Format(_T("%d %s"),SVID_LIST_1200[0],ITEM_NAME_1200[0]);
		m_strSVID = strTemp;	

		m_nInputCnt = EventReportSVIDcnt[2];

		m_strInput = TEST_DATA_1200[0];
	}
	else if(nIdx == 4) //READY_TO_LOAD_BARCODE_REQUEST
	{
		while(nCnt < EventReportSVIDcnt[3]) // 
		{
			strTemp.Format(_T("%d "),SVID_LIST_1300[nCnt]);
			m_strEventSVIDs.Append(strTemp);
			nCnt++;
		}

		strTemp.Format(_T("%d %s"),SVID_LIST_1300[0],ITEM_NAME_1300[0]);
		m_strSVID = strTemp;	

		m_nInputCnt = EventReportSVIDcnt[3];

		m_strInput = TEST_DATA_1300[0];
	}
	else if(nIdx == 5) //LOT_START_REQUEST
	{
		while(nCnt < EventReportSVIDcnt[4]) // 
		{
			strTemp.Format(_T("%d "),SVID_LIST_1400[nCnt]);
			m_strEventSVIDs.Append(strTemp);
			nCnt++;
		}

		strTemp.Format(_T("%d %s"),SVID_LIST_1400[0],ITEM_NAME_1400[0]);
		m_strSVID = strTemp;	

		m_nInputCnt = EventReportSVIDcnt[4];

		m_strInput = TEST_DATA_1400[0];
	}
	else if(nIdx == 6) //CONTROL MODE (LOADER) - Off-Line
	{
		while(nCnt < EventReportSVIDcnt[5]) // 
		{
			strTemp.Format(_T("%d "),SVID_LIST_1500[nCnt]);
			m_strEventSVIDs.Append(strTemp);
			nCnt++;
		}

		strTemp.Format(_T("%d %s"),SVID_LIST_1500[0],ITEM_NAME_1500[0]);
		m_strSVID = strTemp;	

		m_nInputCnt = EventReportSVIDcnt[5];

		m_strInput = TEST_DATA_1500[0];
	}
	else if(nIdx == 7) //BOX_PROC_RESULT_INFO
	{
		while(nCnt < EventReportSVIDcnt[6]) // 
		{
			strTemp.Format(_T("%d "),SVID_LIST_1600[nCnt]);
			m_strEventSVIDs.Append(strTemp);
			nCnt++;
		}

		strTemp.Format(_T("%d %s"),SVID_LIST_1600[0],ITEM_NAME_1600[0]);
		m_strSVID = strTemp;	

		m_nInputCnt = EventReportSVIDcnt[6];

		m_strInput = TEST_DATA_1600[0];
	}
	else if(nIdx == 8) // READY_TO_UNLOAD_REQUEST
	{
		while(nCnt < EventReportSVIDcnt[7]) // 
		{
			strTemp.Format(_T("%d "),SVID_LIST_1700[nCnt]);
			m_strEventSVIDs.Append(strTemp);
			nCnt++;
		}

		strTemp.Format(_T("%d %s"),SVID_LIST_1700[0],ITEM_NAME_1700[0]);
		m_strSVID = strTemp;	

		m_nInputCnt = EventReportSVIDcnt[7];

		m_strInput = TEST_DATA_1700[0];
	}
	else if(nIdx == 9) // LOT_END_REQUEST
	{
		while(nCnt < EventReportSVIDcnt[8]) // 
		{
			strTemp.Format(_T("%d "),SVID_LIST_1800[nCnt]);
			m_strEventSVIDs.Append(strTemp);
			nCnt++;
		}

		strTemp.Format(_T("%d %s"),SVID_LIST_1800[0],ITEM_NAME_1800[0]);
		m_strSVID = strTemp;	

		m_nInputCnt = EventReportSVIDcnt[8];

		m_strInput = TEST_DATA_1800[0];
	}
	else if(nIdx == 10) // ZEBRA_CODE_REQUEST
	{
		while(nCnt < EventReportSVIDcnt[9]) // 
		{
			strTemp.Format(_T("%d "),SVID_LIST_1900[nCnt]);
			m_strEventSVIDs.Append(strTemp);
			nCnt++;
		}

		strTemp.Format(_T("%d %s"),SVID_LIST_1900[0],ITEM_NAME_1900[0]);
		m_strSVID = strTemp;	

		m_nInputCnt = EventReportSVIDcnt[9];

		m_strInput = TEST_DATA_1900[0];
	}
	else if(nIdx == 11) // RUN
	{
		while(nCnt < EventReportSVIDcnt[10]) // 
		{
			strTemp.Format(_T("%d "),SVID_LIST_901[nCnt]);
			m_strEventSVIDs.Append(strTemp);
			nCnt++;
		}

		strTemp.Format(_T("%d %s"),SVID_LIST_901[0],ITEM_NAME_901[0]);
		m_strSVID = strTemp;	

		m_nInputCnt = EventReportSVIDcnt[10];

		m_strInput = TEST_DATA_901[0];
	}
	else if(nIdx == 12) // STOP
	{
		while(nCnt < EventReportSVIDcnt[11]) // 
		{
			strTemp.Format(_T("%d "),SVID_LIST_902[nCnt]);
			m_strEventSVIDs.Append(strTemp);
			nCnt++;
		}

		strTemp.Format(_T("%d %s"),SVID_LIST_902[0],ITEM_NAME_902[0]);
		m_strSVID = strTemp;	

		m_nInputCnt = EventReportSVIDcnt[11];

		m_strInput = TEST_DATA_902[0];
	}
	else if(nIdx == 13) // IDLE
	{
		while(nCnt < EventReportSVIDcnt[12]) // 
		{
			strTemp.Format(_T("%d "),SVID_LIST_903[nCnt]);
			m_strEventSVIDs.Append(strTemp);
			nCnt++;
		}

		strTemp.Format(_T("%d %s"),SVID_LIST_903[0],ITEM_NAME_903[0]);
		m_strSVID = strTemp;	

		m_nInputCnt = EventReportSVIDcnt[12];

		m_strInput = TEST_DATA_903[0];
	}
	else if(nIdx == 14) // Process Recipe Selected
	{
		while(nCnt < EventReportSVIDcnt[13]) // 
		{
			strTemp.Format(_T("%d "),SVID_LIST_10[nCnt]);
			m_strEventSVIDs.Append(strTemp);
			nCnt++;
		}

		strTemp.Format(_T("%d %s"),SVID_LIST_10[0],ITEM_NAME_10[0]);
		m_strSVID = strTemp;	

		m_nInputCnt = EventReportSVIDcnt[13];

		m_strInput = TEST_DATA_10[0];
	}

	UpdateData(FALSE);
}
// S2F41 Host Command 수신시 처리
void CDlg_Gem::eGEMReqRemoteCommandExgem3ctrl1(long nMsgId, LPCTSTR sRcmd, long nCount, BSTR* psCpNames, BSTR* psCpVals)
{
	CString strRCMD;
	strRCMD.Format(L"%s",sRcmd);
	CString strName,strValue,strConfirmFlag,strMsg,strLog;
	strLog.Format(L"[IN] <%s> (S2F41)",strRCMD);
	AddGemLog(strLog);
	
	for(int i = 0; i < nCount; i++) 
	{	strName = psCpNames[i]; 
		strValue = psCpVals[i]; 
		strLog.Format(L"\t[Param] %s\r\n\t\t\t[Value] %s",strName,strValue);
		AddGemLog(strLog);
		if(i == 0)
		{
			strConfirmFlag = psCpVals[i]; 
		}
	}	
	if(strConfirmFlag == L"F")
	{
		strMsg.Format(L"호스트 커맨드 <%s> CONFIRM FLAG 'FALSE'!",strRCMD);
		MessageBox(strMsg,L"호스트 커맨드 CONFIRM ERROR",MB_ICONSTOP);
		GetLog()->Debug(L"(XGEM) %s",strMsg); //로그기록
		return;
	}

	if(strRCMD == L"READY_TO_LOAD_REPLY")
	{
		;
	}
	else if(strRCMD == L"READY_TO_LOAD_BARCODE_REPLY")
	{
		strLog.Format(L"LOT ID - %s\r\n", psCpVals[2]);
		strMsg += strLog;
		strLog.Format(L"LOT QTY (LOT device 총수량) - %s\r\n", psCpVals[3]); //LOT device 총수량
		strMsg += strLog;
		strLog.Format(L"BUNDLE qty (LOT 번들당 device 수량 ) - %s\r\n", psCpVals[4]); //LOT 번들당 device 수량 - 일단 1 Lot, 1 Bundle로 작업. (2018.09.10)협의 - 회의록은 내용누락. 신현대수석 구두제안만...
		strMsg += strLog;
		strLog.Format(L"Device Name - %s\r\n", psCpVals[5]); //
		strMsg += strLog;;
		strLog.Format(L"Inner Box Total qty (Inner Box 총수량) - %s\r\n", psCpVals[6]); //Inner Box 총수량
		strMsg += strLog;
		strLog.Format(L"Inner Box in qty (Inner Box당 full device 수량) - %s\r\n", psCpVals[7]); //Inner Box당 full device 수량
		strMsg += strLog;
		strLog.Format(L"Marking Info (Marking Vision OCR data/콤마 구분자) - %s\r\n", psCpVals[8]); //Marking Vision OCR data (콤마 구분자)
		strMsg += strLog;
		AfxMessageBox(strMsg);
	}
	else if(strRCMD == L"LOT_START_REPLY")
	{
		;
	}
	else if(strRCMD == L"BOX_END_REPLY")
	{
		;
	}
	else if(strRCMD == L"READY_TO_UNLOAD_REPLY")
	{
		;
	}
	else if(strRCMD == L"LOT_END_REPLY")
	{
		;
	}
	else if(strRCMD == L"ZEBRA_CODE_REPLY")
	{
		strMsg.Format(L"(ZPL) %s",psCpVals[2]);
		AfxMessageBox(strMsg);
	}

//Host Command Ack
//0 = Acknowledge, command has been performed
//1 = Command does not exist
//2 = Cannot perform now
//3 = At least one parameter is invalid
//4 = Acknowledge, command will be performed with completion signaled later by an event
//5 = Rejected, Already in Desired Condition
//6 = No such object exists
	long nHCAck = 0;
//전송할 psCpName/pnCpAck 쌍의 개수입니다. Command, command parameter/command parameter value가 유효하면 전송할 개수는 0(zero) 입니다.
	nCount=0;
	int nReturn = m_XGem300.GEMRspRemoteCommand(nMsgId,sRcmd, nHCAck,nCount,NULL,NULL);
	if(nReturn == 0)
	{
		strLog.Format(L"[OUT] <%s> nHCAck(%d) (S2F42)",strRCMD,nHCAck);
		AddGemLog(strLog);
	}
	else
	{
		strLog.Format(L"Fail to GEMRspRemoteCommand [%s] (%d)", strRCMD, nReturn);
		AddGemLog(strLog);
		GetLog()->Debug(L"(XGEM) %s",strLog); //에러 기록
	}
}

//////////////////ERS(Event Report Send) Functions
BOOL CDlg_Gem::ReadyToLoadReq(CString strPort) //CEID(1000) 
{
	long nReturn = 0;
	CString strMsg = L"";
	long nVID,nCEID;
	nCEID = 1000;  //
	BSTR bstr;
	int nCnt = 0;  
	//1. Port - 'LOAD' default 
	nVID = SVID_LIST_1000[nCnt];
	bstr = strPort.AllocSysString(); 
	nReturn = m_XGem300.GEMSetVariable(1, &nVID, &bstr);
	SysFreeString(bstr);
	if( nReturn != 0 )  
	{
		strMsg.Format(L"[ERROR] Fail to GEMSetVariable-%d CEID(%d) SVID[%d]", nReturn,nCEID,nVID );
		AddGemLog(strMsg);
		GetLog()->Debug(L"(XGEM) %s",strMsg); //Error log만.. GEM Driver 통신 log write
		return FALSE;
	}

	nReturn = m_XGem300.GEMSetEvent(nCEID);
	if( nReturn == 0 ) {
		strMsg.Format(L"[OUT][S6F11] (%d) - Send GEMSetEvent successfully", nCEID);
		AddGemLog(strMsg);
		return TRUE;
    }
    else {
		strMsg.Format(L"[ERROR][OUT][S6F11] (%d) Fail to GEMSetEvent (%d)", nCEID, nReturn);
        AddGemLog(strMsg);
		GetLog()->Debug(L"(XGEM) %s",strMsg);
		return FALSE;
    }
}

BOOL CDlg_Gem::MaterialStatRpt(CString strTrayStacker, CString strEndCap, CString strBanding, CString strHIC, CString strMBB, 
							   CString strBox, CString strSilicaGel, CString strLabel, CString strRibbon, CString strBubbleSheet) //(1100)
{
	long nReturn = 0;
	CString strMsg = L"";
	long nVID,nCEID;
	nCEID = 1100; //
	BSTR bstr;
	int nCnt = 0; 
	//1. TrayStacker - 30 position ',' 구분자  _T("T,T,T,T,T,T,T,T,T,T,T,T,T,T,T,T,T,T,T,T,T,T,T,T,T,T,T,T,T,T")
	nVID = SVID_LIST_1100[nCnt];
	bstr = strTrayStacker.AllocSysString(); 
	nReturn = m_XGem300.GEMSetVariable(1, &nVID, &bstr);
	SysFreeString(bstr);
	if( nReturn != 0 )  
	{
		strMsg.Format(L"(XGEM) [ERROR] Fail to GEMSetVariable-%d CEID(%d) SVID[%d]", nReturn,nCEID,nVID );
		AddGemLog(strMsg);
		GetLog()->Debug(L"(XGEM) %s",strMsg);
		return FALSE;
	}
	nCnt++;
	//2. EndCap - 8 position  _T("T,T,T,T,T,T,T,T")
	nVID = SVID_LIST_1100[nCnt];
	bstr = strEndCap.AllocSysString(); 
	nReturn = m_XGem300.GEMSetVariable(1, &nVID, &bstr);
	SysFreeString(bstr);
	if( nReturn != 0 )  
	{
		strMsg.Format(L"(XGEM) [ERROR] Fail to GEMSetVariable-%d CEID(%d) SVID[%d]", nReturn,nCEID,nVID );
		AddGemLog(strMsg);
		GetLog()->Debug(L"(XGEM) %s",strMsg);
		return FALSE;
	}
	nCnt++;
	//3. Banding - 1 position _T("T")
	nVID = SVID_LIST_1100[nCnt];
	bstr = strBanding.AllocSysString(); 
	nReturn = m_XGem300.GEMSetVariable(1, &nVID, &bstr);
	SysFreeString(bstr);
	if( nReturn != 0 )  
	{
		strMsg.Format(L"(XGEM) [ERROR] Fail to GEMSetVariable-%d CEID(%d) SVID[%d]", nReturn,nCEID,nVID );
		AddGemLog(strMsg);
		GetLog()->Debug(L"(XGEM) %s",strMsg);
		return FALSE;
	}
	nCnt++;
	//4. HIC - 1 position _T("T")
	nVID = SVID_LIST_1100[nCnt];
	bstr = strHIC.AllocSysString(); //
	nReturn = m_XGem300.GEMSetVariable(1, &nVID, &bstr);
	SysFreeString(bstr);
	if( nReturn != 0 )  
	{
		strMsg.Format(L"(XGEM) [ERROR] Fail to GEMSetVariable-%d CEID(%d) SVID[%d]", nReturn,nCEID,nVID );
		AddGemLog(strMsg);
		GetLog()->Debug(L"(XGEM) %s",strMsg);
		return FALSE;
	}
	nCnt++;
	//5. MBB - 2 position _T("T,T")
	nVID = SVID_LIST_1100[nCnt];
	bstr = strMBB.AllocSysString(); 
	nReturn = m_XGem300.GEMSetVariable(1, &nVID, &bstr);
	SysFreeString(bstr);
	if( nReturn != 0 )  
	{
		strMsg.Format(L"(XGEM) [ERROR] Fail to GEMSetVariable-%d CEID(%d) SVID[%d]", nReturn,nCEID,nVID );
		AddGemLog(strMsg);
		GetLog()->Debug(L"(XGEM) %s",strMsg);
		return FALSE;
	}
	nCnt++;
	//6. Box - 1 position _T("T")
	nVID = SVID_LIST_1100[nCnt];
	bstr = strBox.AllocSysString(); 
	nReturn = m_XGem300.GEMSetVariable(1, &nVID, &bstr);
	SysFreeString(bstr);
	if( nReturn != 0 )  
	{
		strMsg.Format(L"(XGEM) [ERROR] Fail to GEMSetVariable-%d CEID(%d) SVID[%d]", nReturn,nCEID,nVID );
		AddGemLog(strMsg);
		GetLog()->Debug(L"(XGEM) %s",strMsg);
		return FALSE;
	}
	nCnt++;
	//7. SilicaGel - 1 position _T("T")
	nVID = SVID_LIST_1100[nCnt];
	bstr = strSilicaGel.AllocSysString(); //1 position _T("T,T")
	nReturn = m_XGem300.GEMSetVariable(1, &nVID, &bstr);
	SysFreeString(bstr);
	if( nReturn != 0 )  
	{
		strMsg.Format(L"(XGEM) [ERROR] Fail to GEMSetVariable-%d CEID(%d) SVID[%d]", nReturn,nCEID,nVID );
		AddGemLog(strMsg);
		GetLog()->Debug(L"(XGEM) %s",strMsg);
		return FALSE;
	}
	nCnt++;
	//8. Label지 - 5 position _T("T","T","T","T","T")
	nVID = SVID_LIST_1100[nCnt];
	bstr = strLabel.AllocSysString(); //1 position _T("T,T")
	nReturn = m_XGem300.GEMSetVariable(1, &nVID, &bstr);
	SysFreeString(bstr);
	if( nReturn != 0 )  
	{
		strMsg.Format(L"(XGEM) [ERROR] Fail to GEMSetVariable-%d CEID(%d) SVID[%d]", nReturn,nCEID,nVID );
		AddGemLog(strMsg);
		GetLog()->Debug(L"(XGEM) %s",strMsg);
		return FALSE;
	}
	nCnt++;
	//9. Label프린터리본 - 5 position _T("T","T","T","T","T")
	nVID = SVID_LIST_1100[nCnt];
	bstr = strRibbon.AllocSysString(); //1 position _T("T,T")
	nReturn = m_XGem300.GEMSetVariable(1, &nVID, &bstr);
	SysFreeString(bstr);
	if( nReturn != 0 )  
	{
		strMsg.Format(L"(XGEM) [ERROR] Fail to GEMSetVariable-%d CEID(%d) SVID[%d]", nReturn,nCEID,nVID );
		AddGemLog(strMsg);
		GetLog()->Debug(L"(XGEM) %s",strMsg);
		return FALSE;
	}
	nCnt++;
	//10. BubbleSheet - 1 position _T("T")
	nVID = SVID_LIST_1100[nCnt];
	bstr = strBubbleSheet.AllocSysString(); //1 position _T("T,T")
	nReturn = m_XGem300.GEMSetVariable(1, &nVID, &bstr);
	SysFreeString(bstr);
	if( nReturn != 0 )  
	{
		strMsg.Format(L"(XGEM) [ERROR] Fail to GEMSetVariable-%d CEID(%d) SVID[%d]", nReturn,nCEID,nVID );
		AddGemLog(strMsg);
		GetLog()->Debug(L"(XGEM) %s",strMsg);
		return FALSE;
	}

	nReturn = m_XGem300.GEMSetEvent(nCEID);
	if( nReturn == 0 ) {
		strMsg.Format(L"[OUT][S6F11] (%d) - Send GEMSetEvent successfully", nCEID);
		AddGemLog(strMsg);
		return TRUE;
    }
    else {
		strMsg.Format(L"[ERROR][OUT][S6F11] (%d) Fail to GEMSetEvent (%d)", nCEID, nReturn);
        AddGemLog(strMsg);
		GetLog()->Debug(L"(XGEM) %s",strMsg);
		return FALSE;
    }
}

BOOL CDlg_Gem::RFID_FullRpt(CString strRfidFullStatus) //(1200)
{
	long nReturn = 0;
	CString strMsg = L"";
	long nVID,nCEID;
	nCEID = 1200; //
	BSTR bstr;
	int nCnt = 0;  
	//1. Rfid_full_status - T or F
	nVID = SVID_LIST_1200[nCnt];
	bstr = strRfidFullStatus.AllocSysString(); 
	nReturn = m_XGem300.GEMSetVariable(1, &nVID, &bstr);
	SysFreeString(bstr);
	if( nReturn != 0 )  
	{
		strMsg.Format(L"[ERROR] Fail to GEMSetVariable-%d CEID(%d) SVID[%d]", nReturn,nCEID,nVID );
		AddGemLog(strMsg);
		GetLog()->Debug(L"(XGEM) %s",strMsg); //Error log만.. GEM Driver 통신 log write
		return FALSE;
	}

	nReturn = m_XGem300.GEMSetEvent(nCEID);
	if( nReturn == 0 ) {
		strMsg.Format(L"[OUT][S6F11] (%d) - Send GEMSetEvent successfully", nCEID);
		AddGemLog(strMsg);
		return TRUE;
    }
    else {
		strMsg.Format(L"[ERROR][OUT][S6F11] (%d) Fail to GEMSetEvent (%d)", nCEID, nReturn);
        AddGemLog(strMsg);
		GetLog()->Debug(L"(XGEM) %s",strMsg);
		return FALSE;
    }
}

BOOL CDlg_Gem::ReadyToLoadBcrReq(CString strBarcode) //(1300)
{
	long nReturn = 0;
	CString strMsg = L"";
	long nVID,nCEID;
	nCEID = 1300; //
	BSTR bstr;
	int nCnt = 0;  
	//1. Barcode - 캐리어바코드 
	nVID = SVID_LIST_1300[nCnt];
	bstr = strBarcode.AllocSysString(); 
	nReturn = m_XGem300.GEMSetVariable(1, &nVID, &bstr);
	SysFreeString(bstr);
	if( nReturn != 0 )  
	{
		strMsg.Format(L"[ERROR] Fail to GEMSetVariable-%d CEID(%d) SVID[%d]", nReturn,nCEID,nVID );
		AddGemLog(strMsg);
		GetLog()->Debug(L"(XGEM) %s",strMsg); //Error log만.. GEM Driver 통신 log write
		return FALSE;
	}

	nReturn = m_XGem300.GEMSetEvent(nCEID);
	if( nReturn == 0 ) {
		strMsg.Format(L"[OUT][S6F11] (%d) - Send GEMSetEvent successfully", nCEID);
		AddGemLog(strMsg);
		return TRUE;
    }
    else {
		strMsg.Format(L"[ERROR][OUT][S6F11] (%d) Fail to GEMSetEvent (%d)", nCEID, nReturn);
        AddGemLog(strMsg);
		GetLog()->Debug(L"(XGEM) %s",strMsg);
		return FALSE;
    }
}

BOOL CDlg_Gem::LotStartReq(CString strBarcode,CString strLotID,int nDeviceQty,int nTrayQty, int nBundleTrayQty) //(1400)
{
	long nReturn = 0;
	CString strMsg = L"";
	long nVID,nCEID;
	nCEID = 1400; //
	BSTR bstr;
	int nCnt = 0;  
	//1. Barcode - 캐리어바코드 
	nVID = SVID_LIST_1400[nCnt];
	bstr = strBarcode.AllocSysString(); 
	nReturn = m_XGem300.GEMSetVariable(1, &nVID, &bstr);
	SysFreeString(bstr);
	if( nReturn != 0 )  
	{
		strMsg.Format(L"[ERROR] Fail to GEMSetVariable-%d CEID(%d) SVID[%d]", nReturn,nCEID,nVID );
		AddGemLog(strMsg);
		GetLog()->Debug(L"(XGEM) %s",strMsg); //Error log만.. GEM Driver 통신 log write
		return FALSE;
	}
	nCnt++;
	//2. LotID
	nVID = SVID_LIST_1400[nCnt];
	bstr = strLotID.AllocSysString(); 
	nReturn = m_XGem300.GEMSetVariable(1, &nVID, &bstr);
	SysFreeString(bstr);
	if( nReturn != 0 )  
	{
		strMsg.Format(L"[ERROR] Fail to GEMSetVariable-%d CEID(%d) SVID[%d]", nReturn,nCEID,nVID );
		AddGemLog(strMsg);
		GetLog()->Debug(L"(XGEM) %s",strMsg); //Error log만.. GEM Driver 통신 log write
		return FALSE;
	}
	nCnt++;
	//3. Lot Device Qty
	CString strValue;
	nVID = SVID_LIST_1400[nCnt];
	strValue.Format(L"%d",nDeviceQty);
	bstr = strValue.AllocSysString(); 
	nReturn = m_XGem300.GEMSetVariable(1, &nVID, &bstr);
	SysFreeString(bstr);
	if( nReturn != 0 )  
	{
		strMsg.Format(L"[ERROR] Fail to GEMSetVariable-%d CEID(%d) SVID[%d]", nReturn,nCEID,nVID );
		AddGemLog(strMsg);
		GetLog()->Debug(L"(XGEM) %s",strMsg); //Error log만.. GEM Driver 통신 log write
		return FALSE;
	}
	nCnt++;
	//4. Tray qty
	nVID = SVID_LIST_1400[nCnt];
	strValue.Format(L"%d",nTrayQty);
	bstr = strValue.AllocSysString(); 
	nReturn = m_XGem300.GEMSetVariable(1, &nVID, &bstr);
	SysFreeString(bstr);
	if( nReturn != 0 )  
	{
		strMsg.Format(L"[ERROR] Fail to GEMSetVariable-%d CEID(%d) SVID[%d]", nReturn,nCEID,nVID );
		AddGemLog(strMsg);
		GetLog()->Debug(L"(XGEM) %s",strMsg); //Error log만.. GEM Driver 통신 log write
		return FALSE;
	}
	nCnt++;
	//5. BUNDLE Tray qty
	nVID = SVID_LIST_1400[nCnt];
	strValue.Format(L"%d",nBundleTrayQty);
	bstr = strValue.AllocSysString(); 
	nReturn = m_XGem300.GEMSetVariable(1, &nVID, &bstr);
	SysFreeString(bstr);
	if( nReturn != 0 )  
	{
		strMsg.Format(L"[ERROR] Fail to GEMSetVariable-%d CEID(%d) SVID[%d]", nReturn,nCEID,nVID );
		AddGemLog(strMsg);
		GetLog()->Debug(L"(XGEM) %s",strMsg); //Error log만.. GEM Driver 통신 log write
		return FALSE;
	}


	nReturn = m_XGem300.GEMSetEvent(nCEID);
	if( nReturn == 0 ) {
		strMsg.Format(L"[OUT][S6F11] (%d) - Send GEMSetEvent successfully", nCEID);
		AddGemLog(strMsg);
		return TRUE;
    }
    else {
		strMsg.Format(L"[ERROR][OUT][S6F11] (%d) Fail to GEMSetEvent (%d)", nCEID, nReturn);
        AddGemLog(strMsg);
		GetLog()->Debug(L"(XGEM) %s",strMsg);
		return FALSE;
    }
}

BOOL CDlg_Gem::BoxProcResultInfoRpt(CString strLotID, int nLotQty, CString strOperCode, CString strInnerBoxID, int nBoxDeviceQty, 
									CString strPartial, CString strFlag) //(1500)
{
	long nReturn = 0;
	CString strMsg = L"";
	long nVID,nCEID;
	nCEID = 1500; //
	BSTR bstr;
	int nCnt = 0;  
	//1. LOT ID 
	nVID = SVID_LIST_1500[nCnt];
	bstr = strLotID.AllocSysString(); 
	nReturn = m_XGem300.GEMSetVariable(1, &nVID, &bstr);
	SysFreeString(bstr);
	if( nReturn != 0 )  
	{
		strMsg.Format(L"[ERROR] Fail to GEMSetVariable-%d CEID(%d) SVID[%d]", nReturn,nCEID,nVID );
		AddGemLog(strMsg);
		GetLog()->Debug(L"(XGEM) %s",strMsg); //Error log만.. GEM Driver 통신 log write
		return FALSE;
	}
	nCnt++;
	//2. LOT qty
	CString strValue;
	nVID = SVID_LIST_1500[nCnt];
	strValue.Format(L"%d",nLotQty);
	bstr = strValue.AllocSysString(); 
	nReturn = m_XGem300.GEMSetVariable(1, &nVID, &bstr);
	SysFreeString(bstr);
	if( nReturn != 0 )  
	{
		strMsg.Format(L"[ERROR] Fail to GEMSetVariable-%d CEID(%d) SVID[%d]", nReturn,nCEID,nVID );
		AddGemLog(strMsg);
		GetLog()->Debug(L"(XGEM) %s",strMsg); //Error log만.. GEM Driver 통신 log write
		return FALSE;
	}
	nCnt++;
	//3. OPER_CODE
	nVID = SVID_LIST_1500[nCnt];
	bstr = strOperCode.AllocSysString(); 
	nReturn = m_XGem300.GEMSetVariable(1, &nVID, &bstr);
	SysFreeString(bstr);
	if( nReturn != 0 )  
	{
		strMsg.Format(L"[ERROR] Fail to GEMSetVariable-%d CEID(%d) SVID[%d]", nReturn,nCEID,nVID );
		AddGemLog(strMsg);
		GetLog()->Debug(L"(XGEM) %s",strMsg); //Error log만.. GEM Driver 통신 log write
		return FALSE;
	}
	nCnt++;
	//4. Inner BOX ID
	nVID = SVID_LIST_1500[nCnt];
	bstr = strInnerBoxID.AllocSysString(); 
	nReturn = m_XGem300.GEMSetVariable(1, &nVID, &bstr);
	SysFreeString(bstr);
	if( nReturn != 0 )  
	{
		strMsg.Format(L"[ERROR] Fail to GEMSetVariable-%d CEID(%d) SVID[%d]", nReturn,nCEID,nVID );
		AddGemLog(strMsg);
		GetLog()->Debug(L"(XGEM) %s",strMsg); //Error log만.. GEM Driver 통신 log write
		return FALSE;
	}
	nCnt++;
	//5. Inner BOX DEVICE qty
	nVID = SVID_LIST_1500[nCnt];
	strValue.Format(L"%d",nBoxDeviceQty);
	bstr = strValue.AllocSysString(); 
	nReturn = m_XGem300.GEMSetVariable(1, &nVID, &bstr);
	SysFreeString(bstr);
	if( nReturn != 0 )  
	{
		strMsg.Format(L"[ERROR] Fail to GEMSetVariable-%d CEID(%d) SVID[%d]", nReturn,nCEID,nVID );
		AddGemLog(strMsg);
		GetLog()->Debug(L"(XGEM) %s",strMsg); //Error log만.. GEM Driver 통신 log write
		return FALSE;
	}
	nCnt++;
	//6. Partial
	nVID = SVID_LIST_1500[nCnt];
	bstr = strPartial.AllocSysString(); 
	nReturn = m_XGem300.GEMSetVariable(1, &nVID, &bstr);
	SysFreeString(bstr);
	if( nReturn != 0 )  
	{
		strMsg.Format(L"[ERROR] Fail to GEMSetVariable-%d CEID(%d) SVID[%d]", nReturn,nCEID,nVID );
		AddGemLog(strMsg);
		GetLog()->Debug(L"(XGEM) %s",strMsg); //Error log만.. GEM Driver 통신 log write
		return FALSE;
	}
	nCnt++;
	//7. FLAG 부분공정 작업 Y or N
	nVID = SVID_LIST_1500[nCnt];
	bstr = strFlag.AllocSysString(); 
	nReturn = m_XGem300.GEMSetVariable(1, &nVID, &bstr);
	SysFreeString(bstr);
	if( nReturn != 0 )  
	{
		strMsg.Format(L"[ERROR] Fail to GEMSetVariable-%d CEID(%d) SVID[%d]", nReturn,nCEID,nVID );
		AddGemLog(strMsg);
		GetLog()->Debug(L"(XGEM) %s",strMsg); //Error log만.. GEM Driver 통신 log write
		return FALSE;
	}

	nReturn = m_XGem300.GEMSetEvent(nCEID);
	if( nReturn == 0 ) {
		strMsg.Format(L"[OUT][S6F11] (%d) - Send GEMSetEvent successfully", nCEID);
		AddGemLog(strMsg);
		return TRUE;
    }
    else {
		strMsg.Format(L"[ERROR][OUT][S6F11] (%d) Fail to GEMSetEvent (%d)", nCEID, nReturn);
        AddGemLog(strMsg);
		GetLog()->Debug(L"(XGEM) %s",strMsg);
		return FALSE;
    }
}

BOOL CDlg_Gem::BoxEndReq(CString strBarcode, CString strLotID, int nLotQty, CString strInnerBoxID, int nBoxDeviceQty, CString strPartial) //(1600)
{
	long nReturn = 0;
	CString strMsg = L"";
	long nVID,nCEID;
	nCEID = 1600; //
	BSTR bstr;
	int nCnt = 0;  
	//1. Barcode
	nVID = SVID_LIST_1600[nCnt];
	bstr = strBarcode.AllocSysString(); 
	nReturn = m_XGem300.GEMSetVariable(1, &nVID, &bstr);
	SysFreeString(bstr);
	if( nReturn != 0 )  
	{
		strMsg.Format(L"[ERROR] Fail to GEMSetVariable-%d CEID(%d) SVID[%d]", nReturn,nCEID,nVID );
		AddGemLog(strMsg);
		GetLog()->Debug(L"(XGEM) %s",strMsg); //Error log만.. GEM Driver 통신 log write
		return FALSE;
	}
	nCnt++;
	//2. LOT ID
	nVID = SVID_LIST_1600[nCnt];
	bstr = strLotID.AllocSysString(); 
	nReturn = m_XGem300.GEMSetVariable(1, &nVID, &bstr);
	SysFreeString(bstr);
	if( nReturn != 0 )  
	{
		strMsg.Format(L"[ERROR] Fail to GEMSetVariable-%d CEID(%d) SVID[%d]", nReturn,nCEID,nVID );
		AddGemLog(strMsg);
		GetLog()->Debug(L"(XGEM) %s",strMsg); //Error log만.. GEM Driver 통신 log write
		return FALSE;
	}
	nCnt++;
	//3. LOT qty
	CString strValue;
	nVID = SVID_LIST_1600[nCnt];
	strValue.Format(L"%d",nLotQty);
	bstr = strValue.AllocSysString(); 
	nReturn = m_XGem300.GEMSetVariable(1, &nVID, &bstr);
	SysFreeString(bstr);
	if( nReturn != 0 )  
	{
		strMsg.Format(L"[ERROR] Fail to GEMSetVariable-%d CEID(%d) SVID[%d]", nReturn,nCEID,nVID );
		AddGemLog(strMsg);
		GetLog()->Debug(L"(XGEM) %s",strMsg); //Error log만.. GEM Driver 통신 log write
		return FALSE;
	}
	nCnt++;
	//4. Inner BOX ID
	nVID = SVID_LIST_1600[nCnt];
	bstr = strInnerBoxID.AllocSysString(); 
	nReturn = m_XGem300.GEMSetVariable(1, &nVID, &bstr);
	SysFreeString(bstr);
	if( nReturn != 0 )  
	{
		strMsg.Format(L"[ERROR] Fail to GEMSetVariable-%d CEID(%d) SVID[%d]", nReturn,nCEID,nVID );
		AddGemLog(strMsg);
		GetLog()->Debug(L"(XGEM) %s",strMsg); //Error log만.. GEM Driver 통신 log write
		return FALSE;
	}
	nCnt++;
	//5. Inner BOX DEVICE qty
	nVID = SVID_LIST_1600[nCnt];
	strValue.Format(L"%d",nBoxDeviceQty);
	bstr = strValue.AllocSysString(); 
	nReturn = m_XGem300.GEMSetVariable(1, &nVID, &bstr);
	SysFreeString(bstr);
	if( nReturn != 0 )  
	{
		strMsg.Format(L"[ERROR] Fail to GEMSetVariable-%d CEID(%d) SVID[%d]", nReturn,nCEID,nVID );
		AddGemLog(strMsg);
		GetLog()->Debug(L"(XGEM) %s",strMsg); //Error log만.. GEM Driver 통신 log write
		return FALSE;
	}
	nCnt++;
	//6. Partial
	nVID = SVID_LIST_1600[nCnt];
	bstr = strPartial.AllocSysString(); 
	nReturn = m_XGem300.GEMSetVariable(1, &nVID, &bstr);
	SysFreeString(bstr);
	if( nReturn != 0 )  
	{
		strMsg.Format(L"[ERROR] Fail to GEMSetVariable-%d CEID(%d) SVID[%d]", nReturn,nCEID,nVID );
		AddGemLog(strMsg);
		GetLog()->Debug(L"(XGEM) %s",strMsg); //Error log만.. GEM Driver 통신 log write
		return FALSE;
	}

	nReturn = m_XGem300.GEMSetEvent(nCEID);
	if( nReturn == 0 ) {
		strMsg.Format(L"[OUT][S6F11] (%d) - Send GEMSetEvent successfully", nCEID);
		AddGemLog(strMsg);
		return TRUE;
    }
    else {
		strMsg.Format(L"[ERROR][OUT][S6F11] (%d) Fail to GEMSetEvent (%d)", nCEID, nReturn);
        AddGemLog(strMsg);
		GetLog()->Debug(L"(XGEM) %s",strMsg);
		return FALSE;
    }
}

BOOL CDlg_Gem::ReadyToUnloadReq(CString strBarcode, CString strLotID, int nLotQty) //(1700)
{
	long nReturn = 0;
	CString strMsg = L"";
	long nVID,nCEID;
	nCEID = 1700; //
	BSTR bstr;
	int nCnt = 0;  
	//1. Barcode
	nVID = SVID_LIST_1700[nCnt];//
	bstr = strBarcode.AllocSysString(); 
	nReturn = m_XGem300.GEMSetVariable(1, &nVID, &bstr);
	SysFreeString(bstr);
	if( nReturn != 0 )  
	{
		strMsg.Format(L"[ERROR] Fail to GEMSetVariable-%d CEID(%d) SVID[%d]", nReturn,nCEID,nVID );
		AddGemLog(strMsg);
		GetLog()->Debug(L"(XGEM) %s",strMsg); //Error log만.. GEM Driver 통신 log write
		return FALSE;
	}
	nCnt++;
	//2. LOT ID
	nVID = SVID_LIST_1700[nCnt];//
	bstr = strLotID.AllocSysString(); 
	nReturn = m_XGem300.GEMSetVariable(1, &nVID, &bstr);
	SysFreeString(bstr);
	if( nReturn != 0 )  
	{
		strMsg.Format(L"[ERROR] Fail to GEMSetVariable-%d CEID(%d) SVID[%d]", nReturn,nCEID,nVID );
		AddGemLog(strMsg);
		GetLog()->Debug(L"(XGEM) %s",strMsg); //Error log만.. GEM Driver 통신 log write
		return FALSE;
	}
	nCnt++;
	//3. LOT qty
	CString strValue;
	nVID = SVID_LIST_1700[nCnt];//
	strValue.Format(L"%d",nLotQty);
	bstr = strValue.AllocSysString(); 
	nReturn = m_XGem300.GEMSetVariable(1, &nVID, &bstr);
	SysFreeString(bstr);
	if( nReturn != 0 )  
	{
		strMsg.Format(L"[ERROR] Fail to GEMSetVariable-%d CEID(%d) SVID[%d]", nReturn,nCEID,nVID );
		AddGemLog(strMsg);
		GetLog()->Debug(L"(XGEM) %s",strMsg); //Error log만.. GEM Driver 통신 log write
		return FALSE;
	}
	
	nReturn = m_XGem300.GEMSetEvent(nCEID);
	if( nReturn == 0 ) {
		strMsg.Format(L"[OUT][S6F11] (%d) - Send GEMSetEvent successfully", nCEID);
		AddGemLog(strMsg);
		return TRUE;
    }
    else {
		strMsg.Format(L"[ERROR][OUT][S6F11] (%d) Fail to GEMSetEvent (%d)", nCEID, nReturn);
        AddGemLog(strMsg);
		GetLog()->Debug(L"(XGEM) %s",strMsg);
		return FALSE;
    }
}

BOOL CDlg_Gem::LotEndReq(CString strBarcode, CString strLotID, int nLotQty, int nInnerBoxTotalQty) //(1800)
{
	long nReturn = 0;
	CString strMsg = L"";
	long nVID,nCEID;
	nCEID = 1800; //
	BSTR bstr;
	int nCnt = 0;  
	//1. Barcode
	nVID = SVID_LIST_1800[nCnt];//
	bstr = strBarcode.AllocSysString(); 
	nReturn = m_XGem300.GEMSetVariable(1, &nVID, &bstr);
	SysFreeString(bstr);
	if( nReturn != 0 )  
	{
		strMsg.Format(L"[ERROR] Fail to GEMSetVariable-%d CEID(%d) SVID[%d]", nReturn,nCEID,nVID );
		AddGemLog(strMsg);
		GetLog()->Debug(L"(XGEM) %s",strMsg); //Error log만.. GEM Driver 통신 log write
		return FALSE;
	}
	nCnt++;
	//2. LOT ID
	nVID = SVID_LIST_1800[nCnt];//
	bstr = strLotID.AllocSysString(); 
	nReturn = m_XGem300.GEMSetVariable(1, &nVID, &bstr);
	SysFreeString(bstr);
	if( nReturn != 0 )  
	{
		strMsg.Format(L"[ERROR] Fail to GEMSetVariable-%d CEID(%d) SVID[%d]", nReturn,nCEID,nVID );
		AddGemLog(strMsg);
		GetLog()->Debug(L"(XGEM) %s",strMsg); //Error log만.. GEM Driver 통신 log write
		return FALSE;
	}
	nCnt++;
	//3. LOT qty
	CString strValue;
	nVID = SVID_LIST_1800[nCnt];//
	strValue.Format(L"%d",nLotQty);
	bstr = strValue.AllocSysString(); 
	nReturn = m_XGem300.GEMSetVariable(1, &nVID, &bstr);
	SysFreeString(bstr);
	if( nReturn != 0 )  
	{
		strMsg.Format(L"[ERROR] Fail to GEMSetVariable-%d CEID(%d) SVID[%d]", nReturn,nCEID,nVID );
		AddGemLog(strMsg);
		GetLog()->Debug(L"(XGEM) %s",strMsg); //Error log만.. GEM Driver 통신 log write
		return FALSE;
	}
	nCnt++;
	//4. Inner BOX total qty
	nVID = SVID_LIST_1800[nCnt];//
	strValue.Format(L"%d",nInnerBoxTotalQty);
	bstr = strValue.AllocSysString(); 
	nReturn = m_XGem300.GEMSetVariable(1, &nVID, &bstr);
	SysFreeString(bstr);
	if( nReturn != 0 )  
	{
		strMsg.Format(L"[ERROR] Fail to GEMSetVariable-%d CEID(%d) SVID[%d]", nReturn,nCEID,nVID );
		AddGemLog(strMsg);
		GetLog()->Debug(L"(XGEM) %s",strMsg); //Error log만.. GEM Driver 통신 log write
		return FALSE;
	}
	
	nReturn = m_XGem300.GEMSetEvent(nCEID);
	if( nReturn == 0 ) {
		strMsg.Format(L"[OUT][S6F11] (%d) - Send GEMSetEvent successfully", nCEID);
		AddGemLog(strMsg);
		return TRUE;
    }
    else {
		strMsg.Format(L"[ERROR][OUT][S6F11] (%d) Fail to GEMSetEvent (%d)", nCEID, nReturn);
        AddGemLog(strMsg);
		GetLog()->Debug(L"(XGEM) %s",strMsg);
		return FALSE;
    }
}
			
BOOL CDlg_Gem::ZebraCodeReq(CString strLotID) //(1900)
{
	long nReturn = 0;
	CString strMsg = L"";
	long nVID,nCEID;
	nCEID = 1900;  //
	BSTR bstr;
	int nCnt = 0;  
	//1. Port - 'LOAD' default 
	nVID = SVID_LIST_1900[nCnt];
	bstr = strLotID.AllocSysString(); 
	nReturn = m_XGem300.GEMSetVariable(1, &nVID, &bstr);
	SysFreeString(bstr);
	if( nReturn != 0 )  
	{
		strMsg.Format(L"[ERROR] Fail to GEMSetVariable-%d CEID(%d) SVID[%d]", nReturn,nCEID,nVID );
		AddGemLog(strMsg);
		GetLog()->Debug(L"(XGEM) %s",strMsg); //Error log만.. GEM Driver 통신 log write
		return FALSE;
	}

	nReturn = m_XGem300.GEMSetEvent(nCEID);
	if( nReturn == 0 ) {
		strMsg.Format(L"[OUT][S6F11] (%d) - Send GEMSetEvent successfully", nCEID);
		AddGemLog(strMsg);
		return TRUE;
    }
    else {
		strMsg.Format(L"[ERROR][OUT][S6F11] (%d) Fail to GEMSetEvent (%d)", nCEID, nReturn);
        AddGemLog(strMsg);
		GetLog()->Debug(L"(XGEM) %s",strMsg);
		return FALSE;
    }
}

BOOL CDlg_Gem::EquipStatRpt(CString strEqStatus,CString strLotID) //CEID (901,902,903)
{
	long nReturn = 0;
	CString strMsg = L"";
	long nVID,nCEID;
	long nCount = 1;
	BSTR sControlState;
	nVID = SVID_CONTROL_STATE;
	nReturn = m_XGem300.GEMGetVariable(nCount, &nVID, &sControlState); //현재 control state "sControlState"에 Get

	if(nReturn != 0)
	{
		strMsg.Format(L"Fail to GEMGetVariable(%d) - SVID %d ", nReturn, nVID);
		GetLog()->Debug(strMsg.GetBuffer());
	}

	if(strEqStatus == L"RUN")
	{
		nCEID = 901;
	}
	else if(strEqStatus == L"STOP")
	{
		nCEID = 902;
	}
	else if(strEqStatus == L"IDLE")
	{
		nCEID = 903;
	}

	BSTR bstr;
	int nCnt = 0;  
	//1. EQUIPMENT_STATUS - RUN/STOP/IDLE
	nVID = SVID_LIST_901[nCnt];
	bstr = strEqStatus.AllocSysString(); 
	nReturn = m_XGem300.GEMSetVariable(1, &nVID, &bstr);
	SysFreeString(bstr);
	if( nReturn != 0 )  
	{
		strMsg.Format(L"[ERROR] Fail to GEMSetVariable-%d CEID(%d) SVID[%d]", nReturn,nCEID,nVID );
		AddGemLog(strMsg);
		GetLog()->Debug(L"(XGEM) %s",strMsg); //Error log만.. GEM Driver 통신 log write
		return FALSE;
	}
	nCnt++;
	//2. Control State - This status variable contains the code which identifies the current control state of the equipment.
	//1 : Equipment Off-Line//2 : Attempt On-Line//3 : Host Off-Line//4 : Local//5 : Remote
	nVID = SVID_LIST_901[nCnt];
	nReturn = m_XGem300.GEMSetVariable(1, &nVID, &sControlState); //
	SysFreeString(bstr);
	if( nReturn != 0 )  
	{
		strMsg.Format(L"[ERROR] Fail to GEMSetVariable-%d CEID(%d) SVID[%d]", nReturn,nCEID,nVID );
		AddGemLog(strMsg);
		GetLog()->Debug(L"(XGEM) %s",strMsg); //Error log만.. GEM Driver 통신 log write
		return FALSE;
	}
	nCnt++;
	//3. Lot ID
	nVID = SVID_LIST_901[nCnt];
	bstr = strLotID.AllocSysString(); 
	nReturn = m_XGem300.GEMSetVariable(1, &nVID, &bstr);
	SysFreeString(bstr);
	if( nReturn != 0 )  
	{
		strMsg.Format(L"[ERROR] Fail to GEMSetVariable-%d CEID(%d) SVID[%d]", nReturn,nCEID,nVID );
		AddGemLog(strMsg);
		GetLog()->Debug(L"(XGEM) %s",strMsg); //Error log만.. GEM Driver 통신 log write
		return FALSE;
	}

	nReturn = m_XGem300.GEMSetEvent(nCEID);
	if( nReturn == 0 ) {
		strMsg.Format(L"[OUT][S6F11] (%d) - Send GEMSetEvent successfully", nCEID);
		AddGemLog(strMsg);
		return TRUE;
    }
    else {
		strMsg.Format(L"[ERROR][OUT][S6F11] (%d) Fail to GEMSetEvent (%d)", nCEID, nReturn);
        AddGemLog(strMsg);
		GetLog()->Debug(L"(XGEM) %s",strMsg);
		return FALSE;
    }
}
////////////////////////////Control State/////////////////////////////////////////////////////////
/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Event Name		CEID		 RPTID								SVID				Memo
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
			CEID	Format			Data Name					SVID	Format	
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Offline		  1		  U4	  		Control State				4		ASCII		1 : Equipment Off-Line, 2 : Attempt On-Line, 3 : Host Off-Line, 4 : Local, 5 : Remote
									Previous Control State		34		U1										"
									Clock						1		U1			YYYYMMDDhhmmsscc
----------------------------      -----------------------------------------------------------------------------------------------------------------------------------------
Local		  2		  U4			Control State				4		ASCII	
							   4	Previous Control State		34		U1	
									Clock						1		U1	
----------------------------		-----------------------------------------------------------------------------------------------------------------------------------------
Remote		  3		  U4				Control State			4		ASCII	
									Previous Control State		34		U1	
									Clock						1		U1	
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

void CDlg_Gem::eGEMControlStateChangedExgem3ctrl1(long nState)
{
	long	nReturn = 0;
	CString strMsg = L"";

	//OL은 Auto Packing 장비 무의미. OR이어야 Host Command로 ZPL받아서 Label 출력.
	if(nState == 4) //Control State -1 : None 1 : Equipment Off-Line 2 : Attempt On-Line 3 : Host Off-Line 4 : Local 5 : Remote
	 {
		nReturn = m_XGem300.GEMReqRemote();
		if( nReturn == 0 ) {
			AddGemLog(L"Send GEMReqRemote successfully");
		}
		else {
			strMsg.Format(L"Fail to GEMReqRemote (%d)", nReturn );
			AddGemLog(strMsg);
			GetLog()->Debug(L"(XGEM) %s",strMsg);
		}
	 }
}

BOOL CDlg_Gem::ControlStateRpt() //CEID (1,2,3) - 호출 불필요. 주석 참조
{
	//XGem300 Driver에서 이벤트리포트 전송.  
	//XGem300 해당 Pre-Defined Event ->  "Use" option 적용.
	//(Pre-Defined). CEID 1: Offline,2: Local, 3:Remote / RPTID: 4 

	//-연관 method -
	//m_XGem300.GEMReqOffline(); //XGem Process의 Control State를 Offline으로 변경합니다
	                             //GEMReqOffline() 함수 호출 후 XGem Process의 Control State가 변경되면 
	                             //Host로 Offline 이벤트가 보고되며 XGem control에서 GEMControlStateChanged() 이벤트가 발생합니다
	//m_XGem300.GEMReqHostOffline(); //XGem Process의 Control State를 HostOffline 상태로 변경하고자 할 때 사용합니다. 
	                                 //Control State의 현재 상태가 AttemptOnline 일 경우 HostOffline 으로 변경할 수 없으며 그 이외의 상태에서는 변경이 가능합니다.
	                                 //GEMReqHostOffline() 함수 호출 후 XGem Process의 Control State가 변경되면 XGem control에서 GEMControlStateChanged() 이벤트가 발생됩니다.
	                                 //OnlineLocal or OnlineRemote 상태에서 HostOffline 상태로 변경되는 경우에는 Host로 Offline 이벤트를 보고 합니다
	//m_XGem300.GEMReqLocal();  //XGem Process의 Control State를 Online Local로 변경합니다.
								//GEMReqLocal() 함수 호출 후 XGem Process의 Control State가 변경되면 Host로 Online Local 이벤트가 보고되며
	                            //XGem control에서 GEMControlStateChanged() 이벤트가 발생합니다.
	//m_XGem300.GEMReqRemote(); //XGem Process의 Control State를 Online Remote로 변경합니다
	                            //GEMReqRemote() 함수 호출 후 XGem Process의 Control State가 변경되면 Host로 Online Remote 이벤트가 보고되며 
								//XGem control의 GEMControlStateChanged() 이벤트가 발생합니다
	return TRUE;
}

BOOL CDlg_Gem::RecipeSelectedRpt() // (10) To-Do
{
	return TRUE;
}

BOOL CDlg_Gem::AlarmReportSend(long nALID,long nState) //그룹추가 - XGEM config 에 Group구분 1~3 추가
{
	CString strMsg;
	long nCount = 1;
	long nArrALID[1]; long nArrALCD[1]; CString strArrALTEXT[1];
	nArrALID[0] = nALID;
	m_XGem300.GEMGetAlarmInfo(nCount, nArrALID, nArrALCD, (BSTR*)&strArrALTEXT);

	if(nState == 1)
		strMsg.Format(L"[OUT][S5F1] (%d) Alarm Set - %s", nALID, strArrALTEXT[0]);
	else
		strMsg.Format(L"[OUT][S5F1] (%d) Alarm Clear - %s", nALID, strArrALTEXT[0]);

	int nRet = m_XGem300.GEMSetAlarm(nALID, nState); //state (0 : clear, 1: detect)
	if(nRet == 0)
	{
		AddGemLog(strMsg);
		GetLog()->Debug(L"(XGEM) %s",strMsg); //Alarm은 전송 성공도 로그기록 - .
	}
	else
	{
		strMsg = L"[ERROR]" + strMsg;
		AddGemLog(strMsg);
		GetLog()->Debug(L"(XGEM) %s",strMsg);
	}
	return TRUE;
}
