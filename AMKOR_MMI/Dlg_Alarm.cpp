// Dlg_Alarm.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "AMKOR_MMI.h"
#include "AMKOR_MMIDlg.h"
#include "Dlg_Alarm.h"
#include "afxdialogex.h"


// CDlg_Alarm 대화 상자입니다.

IMPLEMENT_DYNAMIC(CDlg_Alarm, CDialogEx)

CDlg_Alarm::CDlg_Alarm(CWnd* pParent /*=NULL*/)
	: CDialogEx(CDlg_Alarm::IDD, pParent)
{
	m_bCtrl_FirstLoad = FALSE;

	m_SelAlarmInfo.Clear();
}

CDlg_Alarm::~CDlg_Alarm()
{
}

void CDlg_Alarm::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_GB_AL_1, m_gb_1);
	DDX_Control(pDX, IDC_GB_AL_2, m_gb_2);
	DDX_Control(pDX, IDC_GB_AL_3, m_gb_3);
	DDX_Control(pDX, IDC_GC_AL_1, m_GC_1);
	DDX_Control(pDX, IDC_GC_AL_2, m_GC_2);
	DDX_Control(pDX, IDC_BTN_AL_1, m_btn_1);
	DDX_Control(pDX, IDC_EDIT_AL_1, m_edit_1);
	DDX_Control(pDX, IDC_EDIT_AL_2, m_edit_2);
	DDX_Control(pDX, IDC_EDIT_AL_3, m_edit_3);
	DDX_Control(pDX, IDC_EDIT_AL_4, m_edit_4);
	DDX_Control(pDX, IDC_EDIT_AL_5, m_edit_5);
	DDX_Control(pDX, IDC_EDIT_AL_6, m_edit_6);
	DDX_Control(pDX, IDC_EDIT_AL_7, m_edit_7);
	DDX_Control(pDX, IDC_EDIT_AL_8, m_edit_8);
	DDX_Control(pDX, IDC_ST_AL_T1, m_st_T1);
	DDX_Control(pDX, IDC_ST_AL_T2, m_st_T2);
	DDX_Control(pDX, IDC_BTN_AL_2, m_btn_2);
	DDX_Control(pDX, IDC_BTN_AL_3, m_btn_3);
}


BEGIN_MESSAGE_MAP(CDlg_Alarm, CDialogEx)
	ON_WM_ERASEBKGND()
	ON_WM_SIZE()
	ON_WM_TIMER()
	ON_NOTIFY(NM_CLICK, IDC_GC_AL_1, OnGridClick_GC_1)
	ON_NOTIFY(NM_CLICK, IDC_GC_AL_2, OnGridClick_GC_2)
	ON_BN_CLICKED(IDC_BTN_AL_1, &CDlg_Alarm::OnBnClickedBtnAl1)
	ON_BN_CLICKED(IDC_BTN_AL_2, &CDlg_Alarm::OnBnClickedBtnAl2)
	ON_BN_CLICKED(IDC_BTN_AL_3, &CDlg_Alarm::OnBnClickedBtnAl3)
END_MESSAGE_MAP()


// CDlg_Alarm 메시지 처리기입니다.


BOOL CDlg_Alarm::OnEraseBkgnd(CDC* pDC)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	// 전체 배경색
	CRect rect;
    GetClientRect(&rect);
    CBrush myBrush(CR_MAIN_BKGND);	// dialog background color
    CBrush *pOld = pDC->SelectObject(&myBrush);
    BOOL bRes  = pDC->PatBlt(0, 0, rect.Width(), rect.Height(), PATCOPY);
    pDC->SelectObject(pOld);		// restore old brush
    return FALSE;     
	//return CDialogEx::OnEraseBkgnd(pDC);
}


void CDlg_Alarm::OnSize(UINT nType, int cx, int cy)
{
	CDialogEx::OnSize(nType, cx, cy);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	if(m_bCtrl_FirstLoad == TRUE) Cal_CtrlArea();
}


void CDlg_Alarm::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CDialogEx::OnTimer(nIDEvent);
}


BOOL CDlg_Alarm::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	Cal_CtrlArea();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


BOOL CDlg_Alarm::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message == WM_SYSKEYDOWN)
	{
		if(pMsg->wParam == VK_F4) 
			return TRUE;
	}

	if(pMsg->message == WM_KEYDOWN)
	{
		if( pMsg->wParam == VK_ESCAPE ||	
			pMsg->wParam == VK_RETURN ||	
			//pMsg->wParam == VK_SPACE  ||	// Grid Ctrl Text 입력시 사용하기때문에...
			pMsg->wParam == VK_CANCEL )     
			return TRUE;        
	}

	return CDialogEx::PreTranslateMessage(pMsg);
}

void CDlg_Alarm::Cal_Area()
{
	CRect ClientRect;
	GetClientRect(&ClientRect);

	int n20pH = (int)((float)ClientRect.Height()*0.2f);
	int n50pW = (int)((float)ClientRect.Width()*0.5f);

	m_rcTop.SetRect(ClientRect.left, ClientRect.top, ClientRect.right, ClientRect.top+n20pH);
	m_rcLeft.SetRect(ClientRect.left, ClientRect.top+n20pH, ClientRect.left+n50pW, ClientRect.bottom);
	m_rcRight.SetRect(ClientRect.left+n50pW, ClientRect.top+n20pH, ClientRect.right, ClientRect.bottom);
}

void CDlg_Alarm::Cal_CtrlArea()
{
	Cal_Area();

	Cal_CtrlArea_Top();
	Cal_CtrlArea_Left();
	Cal_CtrlArea_Right();

	Update_SelAlarmInfo();

	m_bCtrl_FirstLoad = TRUE;
}

void CDlg_Alarm::Cal_CtrlArea_Top()
{
	CRect CalRect;
	CRect rcGB, rcGBInside;

	rcGB = m_rcTop;
	rcGB.DeflateRect(4,4,4,4);
	rcGBInside = rcGB;
	rcGBInside.DeflateRect(6,28,6,6);

	m_gb_1.SetXPGroupStyle(CXPGroupBox::XPGB_WINDOW_2);
	m_gb_1.SetBackgroundColor(CR_DARKGRAY, CR_GB_BKGND);
	m_gb_1.SetBorderColor(CR_BLACK);
	m_gb_1.SetCatptionTextColor(CR_BLACK);
	m_gb_1.MoveWindow(&rcGB);
	m_GC_1.MoveWindow(&rcGBInside);
	Init_GC_1(4,4);
}

void CDlg_Alarm::Cal_CtrlArea_Left()
{
	CRect CalRect;
	CRect rcTop, rcGB, rcGBInside;

	rcGB = m_rcLeft;
	rcGB.DeflateRect(4,4,4,4);
	rcGBInside = rcGB;
	rcGBInside.DeflateRect(6,28,6,6);

	m_gb_2.SetXPGroupStyle(CXPGroupBox::XPGB_WINDOW_2);
	m_gb_2.SetBackgroundColor(CR_DARKGRAY, CR_GB_BKGND);
	m_gb_2.SetBorderColor(CR_BLACK);
	m_gb_2.SetCatptionTextColor(CR_BLACK);
	m_gb_2.MoveWindow(&rcGB);
	m_GC_2.MoveWindow(&rcGBInside);

	Init_GC_2(CAlarmListSetting::Instance()->m_mapAlarmList.size() + 1,2);
	Load_AlarmFileList();
}

void CDlg_Alarm::Cal_CtrlArea_Right()
{
	CRect CalRect;
	CRect rcGB, rcGBInside;

	rcGB = m_rcRight;
	rcGB.DeflateRect(4,4,4,100);
	rcGBInside = rcGB;
	rcGBInside.DeflateRect(6,28,6,6);

	m_gb_3.SetXPGroupStyle(CXPGroupBox::XPGB_WINDOW_2);
	m_gb_3.SetBackgroundColor(CR_DARKGRAY, CR_GB_BKGND);
	m_gb_3.SetBorderColor(CR_BLACK);
	m_gb_3.SetCatptionTextColor(CR_BLACK);
	m_gb_3.MoveWindow(&rcGB);

	CRect rcTop, rcMid, rcBottom;
	int n20pH = (int)((float)rcGBInside.Height()*0.2f);
	int n40pH = (int)((float)rcGBInside.Height()*0.4f);

	rcTop.SetRect(rcGBInside.left, rcGBInside.top, rcGBInside.right, rcGBInside.top+n20pH);
	rcMid.SetRect(rcGBInside.left, rcTop.bottom, rcGBInside.right, rcTop.bottom+n40pH);
	rcBottom.SetRect(rcGBInside.left, rcMid.bottom, rcGBInside.right, rcGBInside.bottom);

	int nTop_50pH = (int)((float)rcTop.Height()*0.5f);
	CalRect.SetRect(rcTop.left, rcTop.top, rcTop.right, rcTop.top+nTop_50pH);
	m_edit_1.MoveWindow(&CalRect);
	m_edit_1.SetFontStyle(CTRL_FONT, 26, FW_BOLD);
	m_edit_1.SetColor(CR_BLACK, CR_LAVENDER);
	m_edit_1.SetNumeric(FALSE);
	CalRect.SetRect(rcTop.left, rcTop.top+nTop_50pH, rcTop.right, rcTop.bottom);
	m_edit_2.MoveWindow(&CalRect);
	m_edit_2.SetFontStyle(CTRL_FONT, 26, FW_BOLD);
	m_edit_2.SetColor(CR_BLACK, CR_LAVENDER);
	m_edit_2.SetNumeric(FALSE);

	int nMid_25pH = (int)((float)rcMid.Height()*0.25f);
	CalRect.SetRect(rcMid.left, rcMid.top, rcMid.right-130, rcMid.top+nMid_25pH);
	m_st_T1.SetBkColor(CR_DARKORANGE);
	m_st_T1.SetTextColor(CR_WHITE);
	m_st_T1.SetFont(CTRL_FONT,24,FW_BOLD);
	m_st_T1.MoveWindow(&CalRect);
	CalRect.SetRect(rcMid.right-130, rcMid.top, rcMid.right, rcMid.top+nMid_25pH);
	m_btn_1.SetFontBtn(CTRL_FONT, 24, FW_BOLD);
	m_btn_1.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_1.SetTextColor(CR_BLACK);
	m_btn_1.SetIconPos(0);
	m_btn_1.SetIconID(IDI_SAVE);
	m_btn_1.SetIconSize(48,48);
	m_btn_1.SetFlatType(TRUE);
	m_btn_1.MoveWindow(&CalRect);
	CalRect.SetRect(rcMid.left, CalRect.bottom, rcMid.right, CalRect.bottom+nMid_25pH);
	m_edit_3.MoveWindow(&CalRect);
	m_edit_3.SetFontStyle(CTRL_FONT, 26, FW_BOLD);
	m_edit_3.SetColor(CR_BLACK, CR_LAVENDER);
	m_edit_3.SetNumeric(FALSE);
	CalRect.SetRect(rcMid.left, CalRect.bottom, rcMid.right, CalRect.bottom+nMid_25pH);
	m_edit_4.MoveWindow(&CalRect);
	m_edit_4.SetFontStyle(CTRL_FONT, 26, FW_BOLD);
	m_edit_4.SetColor(CR_BLACK, CR_LAVENDER);
	m_edit_4.SetNumeric(FALSE);
	CalRect.SetRect(rcMid.left, CalRect.bottom, rcMid.right, rcMid.bottom);
	m_edit_5.MoveWindow(&CalRect);
	m_edit_5.SetFontStyle(CTRL_FONT, 26, FW_BOLD);
	m_edit_5.SetColor(CR_BLACK, CR_LAVENDER);
	m_edit_5.SetNumeric(FALSE);

	int nBottom_25pH = (int)((float)rcBottom.Height()*0.25f);
	CalRect.SetRect(rcBottom.left, rcBottom.top, rcBottom.right-130, rcBottom.top+nBottom_25pH);
	m_st_T2.SetBkColor(CR_DODGERBLUE);
	m_st_T2.SetTextColor(CR_WHITE);
	m_st_T2.SetFont(CTRL_FONT,24,FW_BOLD);
	m_st_T2.MoveWindow(&CalRect);
	CalRect.SetRect(rcBottom.right-130, rcBottom.top, rcBottom.right, rcBottom.top+nBottom_25pH);
	m_btn_2.SetFontBtn(CTRL_FONT, 24, FW_BOLD);
	m_btn_2.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_2.SetTextColor(CR_BLACK);
	m_btn_2.SetIconPos(0);
	m_btn_2.SetIconID(IDI_SAVE);
	m_btn_2.SetIconSize(48,48);
	m_btn_2.SetFlatType(TRUE);
	m_btn_2.MoveWindow(&CalRect);
	CalRect.SetRect(rcBottom.left, CalRect.bottom, rcBottom.right, CalRect.bottom+nBottom_25pH);
	m_edit_6.MoveWindow(&CalRect);
	m_edit_6.SetFontStyle(CTRL_FONT, 26, FW_BOLD);
	m_edit_6.SetColor(CR_BLACK, CR_LAVENDER);
	m_edit_6.SetNumeric(FALSE);
	CalRect.SetRect(rcBottom.left, CalRect.bottom, rcBottom.right, CalRect.bottom+nBottom_25pH);
	m_edit_7.MoveWindow(&CalRect);
	m_edit_7.SetFontStyle(CTRL_FONT, 26, FW_BOLD);
	m_edit_7.SetColor(CR_BLACK, CR_LAVENDER);
	m_edit_7.SetNumeric(FALSE);
	CalRect.SetRect(rcBottom.left, CalRect.bottom, rcBottom.right, rcBottom.bottom);
	m_edit_8.MoveWindow(&CalRect);
	m_edit_8.SetFontStyle(CTRL_FONT, 26, FW_BOLD);
	m_edit_8.SetColor(CR_BLACK, CR_LAVENDER);
	m_edit_8.SetNumeric(FALSE);

	CalRect.SetRect(m_rcRight.right - 140, m_rcRight.bottom - 60, m_rcRight.right, m_rcRight.bottom);
	CalRect.DeflateRect(2,2);
	m_btn_3.SetFontBtn(CTRL_FONT, 24, FW_BOLD);
	m_btn_3.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_3.SetTextColor(CR_BLACK);
	m_btn_3.SetIconPos(0);
	m_btn_3.SetIconID(IDI_CLOSE);
	m_btn_3.SetIconSize(48,48);
	m_btn_3.SetFlatType(TRUE);
	m_btn_3.MoveWindow(&CalRect);
}

bool CDlg_Alarm::Init_GC_1(int nRows, int nCols)
{
	BOOL m_bEditable = FALSE;
    BOOL m_bListMode = TRUE;
    int m_nRows = nRows;
    int m_nCols = nCols;
    int m_nFixRows = 0;
    int m_nFixCols = 0;

    m_GC_1.SetEditable(m_bEditable);
    m_GC_1.SetListMode(m_bListMode);
    m_GC_1.EnableDragAndDrop(FALSE);
    m_GC_1.SetTextBkColor(RGB(0xFF, 0xFF, 0xE0));

    m_GC_1.SetRowCount(m_nRows);
    m_GC_1.SetColumnCount(m_nCols);
    m_GC_1.SetFixedRowCount(m_nFixRows);
    m_GC_1.SetFixedColumnCount(m_nFixCols);

	m_GC_1.EnableSelection(false);
	m_GC_1.SetSingleColSelection(true);
	m_GC_1.SetSingleRowSelection(true);
	m_GC_1.SetFixedColumnSelection(false);
    m_GC_1.SetFixedRowSelection(false);

	m_GC_1.SetRowResize(false);
	m_GC_1.SetColumnResize(false);

	CRect ClientRect;
	GetDlgItem(IDC_GC_AL_1)->GetWindowRect(&ClientRect);
	if(ClientRect.Width() < 4) return false;
	if(ClientRect.Height() < 4) return false;
	int nClientWidth = ClientRect.Width()-4;
	int nClientHeight = ClientRect.Height()-4;
	int n7pW = (int)(nClientWidth*0.07f);
	int n43pW = (int)(nClientWidth*0.43f);
	int nRemainW = nClientWidth - n7pW - n43pW - n7pW;
	//int nCellW = nClientWidth/m_nCols;
	int nCellH = nClientHeight/m_nRows;
	//int nLastCellW = nClientWidth-nCellW*(m_nCols-1);
	int nLastCellH = nClientHeight-nCellH*(m_nRows-1);
	//int nRemainW = nLastCellW - nCellW;
	int nRemainH = nLastCellH - nCellH;
	for (int i = 0; i < m_GC_1.GetRowCount(); i++) 
	{
		if(i == m_nRows-1)
		{
			m_GC_1.SetRowHeight(i,nCellH);
		}
		else 
		{
			if(nRemainH > 0)
			{
				m_GC_1.SetRowHeight(i,nCellH + 1);
				nRemainH --;
			}
			else
				m_GC_1.SetRowHeight(i,nCellH);
		}
	}
	for (int j = 0; j < m_GC_1.GetColumnCount(); j++)
	{
		if(j == 0) m_GC_1.SetColumnWidth(j,n7pW);
		else if(j == 1) m_GC_1.SetColumnWidth(j,n43pW);
		else if(j == 2) m_GC_1.SetColumnWidth(j,n7pW);
		else if(j == 3) m_GC_1.SetColumnWidth(j,nRemainW);
	}
    DWORD dwTextStyle = DT_CENTER|DT_VCENTER|DT_SINGLELINE;

    // fill rows/cols with text
    for (int row = 0; row < m_GC_1.GetRowCount(); row++) 
	{
        for (int col = 0; col < m_GC_1.GetColumnCount(); col++) 
		{ 
            GV_ITEM Item;
            Item.mask = GVIF_TEXT|GVIF_FORMAT|GVIF_BKCLR;
            Item.row = row;
            Item.col = col;
			Item.nFormat = dwTextStyle;
			Item.crBkClr = RGB(0xFF, 0xFF, 0xE0);

			m_GC_1.SetItemFgColour(row, col, RGB(0,0,0));
			m_GC_1.SetItemFont(row, col, CTRL_FONT, 18, FW_BOLD);
            m_GC_1.SetItem(&Item);  
        }
    }
       
    return TRUE;  // return TRUE  unless you set the focus to a control
}

void CDlg_Alarm::OnGridClick_GC_1(NMHDR *pNotifyStruct, LRESULT* pResult)
{
	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*) pNotifyStruct;

	int nSelRow = pItem->iRow;
	int nSelCol = pItem->iColumn;

	int nCode = 0;
	CString strText;
	if(nSelCol >= 0 && nSelCol <= 1)
	{
		strText = m_GC_1.GetItemText(nSelRow, 0);
		if(strText.IsEmpty() == FALSE)
		{
			nCode = (int)_wtoi(strText);
			CAlarmListSetting::Instance()->GetAlarmTableByCode( nCode, m_SelAlarmInfo.stAT);
			CAlarmListSetting::Instance()->GetAlarmSolutionByCode( nCode, m_SelAlarmInfo.stAS);
		}
		else
		{
			m_SelAlarmInfo.Clear();
		}
	}
	else if(nSelCol >= 2 && nSelCol <= 3)
	{
		strText = m_GC_1.GetItemText(nSelRow, 2);
		if(strText.IsEmpty() == FALSE)
		{
			nCode = (int)_wtoi(strText);
			CAlarmListSetting::Instance()->GetAlarmTableByCode( nCode, m_SelAlarmInfo.stAT);
			CAlarmListSetting::Instance()->GetAlarmSolutionByCode( nCode, m_SelAlarmInfo.stAS);
		}
		else
		{
			m_SelAlarmInfo.Clear();
		}
	}
	else
	{
		m_SelAlarmInfo.Clear();
	}

	Update_SelAlarmInfo();
}

bool CDlg_Alarm::Init_GC_2(int nRows, int nCols)
{
	BOOL m_bEditable = TRUE;
    BOOL m_bListMode = TRUE;
    int m_nRows = nRows;
    int m_nCols = nCols;
    int m_nFixRows = 1;
    int m_nFixCols = 1;

    m_GC_2.SetEditable(m_bEditable);
    m_GC_2.SetListMode(m_bListMode);
    m_GC_2.EnableDragAndDrop(FALSE);
    m_GC_2.SetTextBkColor(RGB(0xFF, 0xFF, 0xE0));

    m_GC_2.SetRowCount(m_nRows);
    m_GC_2.SetColumnCount(m_nCols);
    m_GC_2.SetFixedRowCount(m_nFixRows);
    m_GC_2.SetFixedColumnCount(m_nFixCols);

	m_GC_2.EnableSelection(false);
	m_GC_2.SetSingleColSelection(true);
	m_GC_2.SetSingleRowSelection(true);
	m_GC_2.SetFixedColumnSelection(false);
    m_GC_2.SetFixedRowSelection(false);

	m_GC_2.SetRowResize(false);
	m_GC_2.SetColumnResize(false);

	CRect ClientRect;
	GetDlgItem(IDC_GC_AL_2)->GetWindowRect(&ClientRect);
	if(ClientRect.Width() < 4) return false;
	if(ClientRect.Height() < 4) return false;
	int nClientWidth = ClientRect.Width()-4-18;
	int nClientHeight = ClientRect.Height()-4;
	int n12pW = (int)(nClientWidth*0.12f);
	int nRemainW = nClientWidth - n12pW;
	int nCellH = 36;
	for (int i = 0; i < m_GC_2.GetRowCount(); i++) 
	{
		m_GC_2.SetRowHeight(i,nCellH);
	}
	for (int j = 0; j < m_GC_2.GetColumnCount(); j++)
	{
		if(j == 0) m_GC_2.SetColumnWidth(j,n12pW);
		else if(j == 1) m_GC_2.SetColumnWidth(j,nRemainW);
	}
    DWORD dwTextStyle = DT_CENTER|DT_VCENTER|DT_SINGLELINE;

    // fill rows/cols with text
    for (int row = 0; row < m_GC_2.GetRowCount(); row++) 
	{
        for (int col = 0; col < m_GC_2.GetColumnCount(); col++) 
		{ 
            GV_ITEM Item;
            Item.mask = GVIF_TEXT|GVIF_FORMAT|GVIF_BKCLR;
            Item.row = row;
            Item.col = col;
			Item.nFormat = dwTextStyle;
			Item.crBkClr = RGB(0xFF, 0xFF, 0xE0);

			if(row == 0)
			{
				if(col == 0) Item.strText.Format(_T("CODE"));
				else if(col == 1) Item.strText.Format(_T("ERROR TEXT"));
				Item.crBkClr = CR_SLATEGRAY;
			}
			else
			{
			}

			if(col == 0) Item.crBkClr = CR_SLATEGRAY;
			m_GC_2.SetItemFgColour(row, col, RGB(0,0,0));
			m_GC_2.SetItemFont(row, col, CTRL_FONT, 18, FW_BOLD);
            m_GC_2.SetItem(&Item);  
        }
    }
       
    return TRUE;  // return TRUE  unless you set the focus to a control
}

void CDlg_Alarm::OnGridClick_GC_2(NMHDR *pNotifyStruct, LRESULT* pResult)
{
	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*) pNotifyStruct;

	int nSelRow = pItem->iRow;
	int nSelCol = pItem->iColumn;

	int nCode = 0;
	CString strText = m_GC_2.GetItemText(nSelRow, 0);
	if(strText.IsEmpty() == FALSE)
	{
		nCode = (int)_wtoi(strText);
		CAlarmListSetting::Instance()->GetAlarmTableByCode( nCode, m_SelAlarmInfo.stAT);
		CAlarmListSetting::Instance()->GetAlarmSolutionByCode( nCode, m_SelAlarmInfo.stAS);
	}
	else
	{
		m_SelAlarmInfo.Clear();
	}

	Update_SelAlarmInfo();
}

void CDlg_Alarm::Load_AlarmFileList()
{
	std::map<int,AlarmTable>::iterator iter;
	int nRowCount = 0;
	for(iter = CAlarmListSetting::Instance()->m_mapAlarmList.begin(); iter != CAlarmListSetting::Instance()->m_mapAlarmList.end(); ++iter)
	{
		for (int col = 0; col < m_GC_2.GetColumnCount(); col++) 
		{ 
            GV_ITEM Item;
            Item.mask = GVIF_TEXT|GVIF_FORMAT|GVIF_BKCLR;
            Item.row = nRowCount+1;
            Item.col = col;
			Item.crBkClr = RGB(0xFF, 0xFF, 0xE0);

			if(col == 0)
			{
				Item.nFormat = DT_RIGHT|DT_VCENTER|DT_SINGLELINE;
				Item.strText.Format(_T("%d"), iter->second.nCode);
				Item.crBkClr = CR_SLATEGRAY;
			}
			else if(col == 1)
			{
				Item.nFormat = DT_LEFT|DT_VCENTER|DT_SINGLELINE;
				Item.strText = iter->second.strText;
			}

			m_GC_2.SetItemFgColour(Item.row, Item.col, RGB(0,0,0));
            m_GC_2.SetItem(&Item);  
        }
		nRowCount++;
	}

	m_GC_2.Invalidate();
}

void CDlg_Alarm::Update_PLC_Value()
{
	SHORT sCode[8] = {0,};
	CString strErrorText;
	DWORD dwTextStyle = DT_CENTER|DT_VCENTER|DT_SINGLELINE;
	 GV_ITEM Item;
    Item.mask = GVIF_TEXT|GVIF_FORMAT|GVIF_BKCLR|GVIF_FGCLR;
	Item.nFormat = dwTextStyle;
	Item.crBkClr = RGB(0xFF, 0xFF, 0xE0);
	Item.crFgClr = RGB(0,0,0);

	if(CAppSetting::Instance()->GetGroupNumber() == 1)
	{
		sCode[0] = CDataManager::Instance()->m_PLCReaderD_Alarm_G1.GetValue(91);
		sCode[1] = CDataManager::Instance()->m_PLCReaderD_Alarm_G1.GetValue(92);
		sCode[2] = CDataManager::Instance()->m_PLCReaderD_Alarm_G1.GetValue(93);
		sCode[3] = CDataManager::Instance()->m_PLCReaderD_Alarm_G1.GetValue(94);
		sCode[4] = CDataManager::Instance()->m_PLCReaderD_Alarm_G1.GetValue(95);
		sCode[5] = CDataManager::Instance()->m_PLCReaderD_Alarm_G1.GetValue(96);
		sCode[6] = CDataManager::Instance()->m_PLCReaderD_Alarm_G1.GetValue(97);
		sCode[7] = CDataManager::Instance()->m_PLCReaderD_Alarm_G1.GetValue(98);
	}
	else if(CAppSetting::Instance()->GetGroupNumber() == 2)
	{
		sCode[0] = CDataManager::Instance()->m_PLCReaderD_Alarm_G2.GetValue(91);
		sCode[1] = CDataManager::Instance()->m_PLCReaderD_Alarm_G2.GetValue(92);
		sCode[2] = CDataManager::Instance()->m_PLCReaderD_Alarm_G2.GetValue(93);
		sCode[3] = CDataManager::Instance()->m_PLCReaderD_Alarm_G2.GetValue(94);
		sCode[4] = CDataManager::Instance()->m_PLCReaderD_Alarm_G2.GetValue(95);
		sCode[5] = CDataManager::Instance()->m_PLCReaderD_Alarm_G2.GetValue(96);
		sCode[6] = CDataManager::Instance()->m_PLCReaderD_Alarm_G2.GetValue(97);
		sCode[7] = CDataManager::Instance()->m_PLCReaderD_Alarm_G2.GetValue(98);
	}
	else if(CAppSetting::Instance()->GetGroupNumber() == 3)
	{
		sCode[0] = CDataManager::Instance()->m_PLCReaderD_Alarm_G3.GetValue(91);
		sCode[1] = CDataManager::Instance()->m_PLCReaderD_Alarm_G3.GetValue(92);
		sCode[2] = CDataManager::Instance()->m_PLCReaderD_Alarm_G3.GetValue(93);
		sCode[3] = CDataManager::Instance()->m_PLCReaderD_Alarm_G3.GetValue(94);
		sCode[4] = CDataManager::Instance()->m_PLCReaderD_Alarm_G3.GetValue(95);
		sCode[5] = CDataManager::Instance()->m_PLCReaderD_Alarm_G3.GetValue(96);
		sCode[6] = CDataManager::Instance()->m_PLCReaderD_Alarm_G3.GetValue(97);
		sCode[7] = CDataManager::Instance()->m_PLCReaderD_Alarm_G3.GetValue(98);
	}

	// ERROR 91 번지 처리
	if(sCode[0] != 0)
	{
		CAlarmListSetting::Instance()->GetErrorTextByCode((int)sCode, strErrorText);
		Item.row = 0;
		Item.col = 0;
		Item.strText.Format(_T("%d"), (int)sCode);
		m_GC_1.SetItem(&Item); 
		Item.row = 0;
		Item.col = 1;
		Item.strText = strErrorText;
		m_GC_1.SetItem(&Item); 
	}
	else
	{
		Item.row = 0;
		Item.col = 0;
		Item.strText.Format(_T(""));
		m_GC_1.SetItem(&Item); 
		Item.row = 0;
		Item.col = 1;
		Item.strText.Format(_T(""));
		m_GC_1.SetItem(&Item); 
	}

	// ERROR 92 번지 처리
	if(sCode[1] != 0)
	{
		CAlarmListSetting::Instance()->GetErrorTextByCode((int)sCode, strErrorText);
		Item.row = 1;
		Item.col = 0;
		Item.strText.Format(_T("%d"), (int)sCode);
		m_GC_1.SetItem(&Item); 
		Item.row = 1;
		Item.col = 1;
		Item.strText = strErrorText;
		m_GC_1.SetItem(&Item); 
	}
	else
	{
		Item.row = 1;
		Item.col = 0;
		Item.strText.Format(_T(""));
		m_GC_1.SetItem(&Item); 
		Item.row = 1;
		Item.col = 1;
		Item.strText.Format(_T(""));
		m_GC_1.SetItem(&Item); 
	}

	// ERROR 93 번지 처리
	if(sCode[2] != 0)
	{
		CAlarmListSetting::Instance()->GetErrorTextByCode((int)sCode, strErrorText);
		Item.row = 2;
		Item.col = 0;
		Item.strText.Format(_T("%d"), (int)sCode);
		m_GC_1.SetItem(&Item); 
		Item.row = 2;
		Item.col = 1;
		Item.strText = strErrorText;
		m_GC_1.SetItem(&Item); 
	}
	else
	{
		Item.row = 2;
		Item.col = 0;
		Item.strText.Format(_T(""));
		m_GC_1.SetItem(&Item); 
		Item.row = 2;
		Item.col = 1;
		Item.strText.Format(_T(""));
		m_GC_1.SetItem(&Item); 
	}

	// ERROR 94 번지 처리
	if(sCode[3] != 0)
	{
		CAlarmListSetting::Instance()->GetErrorTextByCode((int)sCode, strErrorText);
		Item.row = 3;
		Item.col = 0;
		Item.strText.Format(_T("%d"), (int)sCode);
		m_GC_1.SetItem(&Item); 
		Item.row = 3;
		Item.col = 1;
		Item.strText = strErrorText;
		m_GC_1.SetItem(&Item); 
	}
	else
	{
		Item.row = 3;
		Item.col = 0;
		Item.strText.Format(_T(""));
		m_GC_1.SetItem(&Item); 
		Item.row = 3;
		Item.col = 1;
		Item.strText.Format(_T(""));
		m_GC_1.SetItem(&Item); 
	}

	// ERROR 95 번지 처리
	if(sCode[4] != 0)
	{
		CAlarmListSetting::Instance()->GetErrorTextByCode((int)sCode, strErrorText);
		Item.row = 0;
		Item.col = 2;
		Item.strText.Format(_T("%d"), (int)sCode);
		m_GC_1.SetItem(&Item); 
		Item.row = 0;
		Item.col = 3;
		Item.strText = strErrorText;
		m_GC_1.SetItem(&Item); 
	}
	else
	{
		Item.row = 0;
		Item.col = 2;
		Item.strText.Format(_T(""));
		m_GC_1.SetItem(&Item); 
		Item.row = 0;
		Item.col = 3;
		Item.strText.Format(_T(""));
		m_GC_1.SetItem(&Item); 
	}

	// ERROR 96 번지 처리
	if(sCode[5] != 0)
	{
		CAlarmListSetting::Instance()->GetErrorTextByCode((int)sCode, strErrorText);
		Item.row = 1;
		Item.col = 2;
		Item.strText.Format(_T("%d"), (int)sCode);
		m_GC_1.SetItem(&Item); 
		Item.row = 1;
		Item.col = 3;
		Item.strText = strErrorText;
		m_GC_1.SetItem(&Item); 
	}
	else
	{
		Item.row = 1;
		Item.col = 2;
		Item.strText.Format(_T(""));
		m_GC_1.SetItem(&Item); 
		Item.row = 1;
		Item.col = 3;
		Item.strText.Format(_T(""));
		m_GC_1.SetItem(&Item); 
	}

	// ERROR 97 번지 처리
	if(sCode[6] != 0)
	{
		CAlarmListSetting::Instance()->GetErrorTextByCode((int)sCode, strErrorText);
		Item.row = 2;
		Item.col = 2;
		Item.strText.Format(_T("%d"), (int)sCode);
		m_GC_1.SetItem(&Item); 
		Item.row = 2;
		Item.col = 3;
		Item.strText = strErrorText;
		m_GC_1.SetItem(&Item); 
	}
	else
	{
		Item.row = 2;
		Item.col = 2;
		Item.strText.Format(_T(""));
		m_GC_1.SetItem(&Item); 
		Item.row = 2;
		Item.col = 3;
		Item.strText.Format(_T(""));
		m_GC_1.SetItem(&Item); 
	}

	// ERROR 98 번지 처리
	if(sCode[7] != 0)
	{
		CAlarmListSetting::Instance()->GetErrorTextByCode((int)sCode, strErrorText);
		Item.row = 3;
		Item.col = 2;
		Item.strText.Format(_T("%d"), (int)sCode);
		m_GC_1.SetItem(&Item); 
		Item.row = 3;
		Item.col = 3;
		Item.strText = strErrorText;
		m_GC_1.SetItem(&Item); 
	}
	else
	{
		Item.row = 3;
		Item.col = 2;
		Item.strText.Format(_T(""));
		m_GC_1.SetItem(&Item); 
		Item.row = 3;
		Item.col = 3;
		Item.strText.Format(_T(""));
		m_GC_1.SetItem(&Item); 
	}

	m_GC_1.Invalidate();
}

void CDlg_Alarm::Update_SelAlarmInfo()
{
	CString strTemp;
	strTemp.Format(_T("%05d"), m_SelAlarmInfo.stAT.nCode);
	m_edit_1.SetWindowText(strTemp);
	m_edit_2.SetWindowText(m_SelAlarmInfo.stAT.strText);
	m_edit_3.SetWindowText(m_SelAlarmInfo.stAS.strText_P1);
	m_edit_4.SetWindowText(m_SelAlarmInfo.stAS.strText_P2);
	m_edit_5.SetWindowText(m_SelAlarmInfo.stAS.strText_P3);
	m_edit_6.SetWindowText(m_SelAlarmInfo.stAS.strText_S1);
	m_edit_7.SetWindowText(m_SelAlarmInfo.stAS.strText_S2);
	m_edit_8.SetWindowText(m_SelAlarmInfo.stAS.strText_S3);
}

void CDlg_Alarm::OnBnClickedBtnAl1()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_btn_1.EnableWindow(FALSE);
	m_btn_2.EnableWindow(FALSE);
	m_btn_3.EnableWindow(FALSE);
	if(m_SelAlarmInfo.stAS.nCode != 0)
	{
		m_edit_3.GetWindowText(m_SelAlarmInfo.stAS.strText_P1);
		m_edit_4.GetWindowText(m_SelAlarmInfo.stAS.strText_P2);
		m_edit_5.GetWindowText(m_SelAlarmInfo.stAS.strText_P3);
		m_edit_6.GetWindowText(m_SelAlarmInfo.stAS.strText_S1);
		m_edit_7.GetWindowText(m_SelAlarmInfo.stAS.strText_S2);
		m_edit_8.GetWindowText(m_SelAlarmInfo.stAS.strText_S3);

		CAlarmListSetting::Instance()->SaveAlarmSolution(m_SelAlarmInfo.stAS);
	}
	m_btn_1.EnableWindow(TRUE);
	m_btn_2.EnableWindow(TRUE);
	m_btn_3.EnableWindow(TRUE);
}


void CDlg_Alarm::OnBnClickedBtnAl2()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_btn_1.EnableWindow(FALSE);
	m_btn_2.EnableWindow(FALSE);
	m_btn_3.EnableWindow(FALSE);
	if(m_SelAlarmInfo.stAS.nCode != 0)
	{
		m_edit_3.GetWindowText(m_SelAlarmInfo.stAS.strText_P1);
		m_edit_4.GetWindowText(m_SelAlarmInfo.stAS.strText_P2);
		m_edit_5.GetWindowText(m_SelAlarmInfo.stAS.strText_P3);
		m_edit_6.GetWindowText(m_SelAlarmInfo.stAS.strText_S1);
		m_edit_7.GetWindowText(m_SelAlarmInfo.stAS.strText_S2);
		m_edit_8.GetWindowText(m_SelAlarmInfo.stAS.strText_S3);

		CAlarmListSetting::Instance()->SaveAlarmSolution(m_SelAlarmInfo.stAS);
	}
	m_btn_1.EnableWindow(TRUE);
	m_btn_2.EnableWindow(TRUE);
	m_btn_3.EnableWindow(TRUE);
}


void CDlg_Alarm::OnBnClickedBtnAl3()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CAMKOR_MMIDlg *)AfxGetMainWnd())->SendMessage(WM_CHANGESCREEN, 0, 0);
}
