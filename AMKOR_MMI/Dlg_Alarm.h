#pragma once
#include "afxwin.h"
#include "UIExt/MatrixStatic.h"
#include "UIExt/CxStatic.h"
#include "UIExt/IconButton.h"
#include "UIExt/XPGroupBox.h"
#include "UIExt\/BorderStyleEdit.h"
#include "GC/gridctrl.h"

// CDlg_Alarm 대화 상자입니다.

struct AlarmInfo
{
	AlarmTable stAT;
	AlarmSolution stAS;

	void Clear()
	{
		stAT.nCode = 0;
		stAT.strText.Empty();

		stAS.nCode = 0;
		stAS.strText_P1.Empty();
		stAS.strText_P2.Empty();
		stAS.strText_P3.Empty();
		stAS.strText_S1.Empty();
		stAS.strText_S2.Empty();
		stAS.strText_S3.Empty();
	}
};

class CDlg_Alarm : public CDialogEx
{
	DECLARE_DYNAMIC(CDlg_Alarm)

public:
	CDlg_Alarm(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CDlg_Alarm();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DLG_ALARM };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);

	void Cal_Area();
	void Cal_CtrlArea();
	void Cal_CtrlArea_Top();
	void Cal_CtrlArea_Left();
	void Cal_CtrlArea_Right();

	bool Init_GC_1(int nRows, int nCols);
	afx_msg void OnGridClick_GC_1(NMHDR *pNotifyStruct, LRESULT* pResult);
	bool Init_GC_2(int nRows, int nCols);
	afx_msg void OnGridClick_GC_2(NMHDR *pNotifyStruct, LRESULT* pResult);

	void Load_AlarmFileList();
	void Update_PLC_Value();
	void Update_SelAlarmInfo();

	CRect m_rcTop;
	CRect m_rcLeft;
	CRect m_rcRight;

	BOOL m_bCtrl_FirstLoad;

	AlarmInfo m_SelAlarmInfo;

	CXPGroupBox m_gb_1;
	CXPGroupBox m_gb_2;
	CXPGroupBox m_gb_3;
	CGridCtrl m_GC_1;
	CGridCtrl m_GC_2;
	CIconButton m_btn_1;
	CIconButton m_btn_2;
	CIconButton m_btn_3;
	CBorderStyleEdit m_edit_1;
	CBorderStyleEdit m_edit_2;
	CBorderStyleEdit m_edit_3;
	CBorderStyleEdit m_edit_4;
	CBorderStyleEdit m_edit_5;
	CBorderStyleEdit m_edit_6;
	CBorderStyleEdit m_edit_7;
	CBorderStyleEdit m_edit_8;
	CxStatic m_st_T1;
	CxStatic m_st_T2;

	afx_msg void OnBnClickedBtnAl1();
	afx_msg void OnBnClickedBtnAl2();
	afx_msg void OnBnClickedBtnAl3();
};
