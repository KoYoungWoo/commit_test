// Dlg_Motor.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "AMKOR_MMI.h"
#include "AMKOR_MMIDlg.h"
#include "Dlg_Motor.h"
#include "Dlg_NumPad.h"
#include "Dlg_MotorList.h"
#include "afxdialogex.h"


// CDlg_Motor 대화 상자입니다.

IMPLEMENT_DYNAMIC(CDlg_Motor, CDialogEx)

CDlg_Motor::CDlg_Motor(CWnd* pParent /*=NULL*/)
	: CDialogEx(CDlg_Motor::IDD, pParent)
{
	m_bCtrl_FirstLoad = FALSE;
	m_nCurMotorNum = 0;
	m_nCurMotor_StartAddressD = 0;
	m_nCurMotor_StartAddressM = 0;
	m_nCurMotor_StartAddressL = 0;
	m_nCurSelMotorItemIndex = -1;
}

CDlg_Motor::~CDlg_Motor()
{
}

void CDlg_Motor::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_ST_MT_NAME, m_st_MotorName);
	DDX_Control(pDX, IDC_BTN_MT_1, m_btn_1);
	DDX_Control(pDX, IDC_BTN_MT_2, m_btn_2);
	DDX_Control(pDX, IDC_BTN_MT_3, m_btn_3);
	DDX_Control(pDX, IDC_GB_MT_1, m_gb_1);
	DDX_Control(pDX, IDC_GC_MT_1, m_GC_1);
	DDX_Control(pDX, IDC_GB_MT_2, m_gb_2);
	DDX_Control(pDX, IDC_ST_MT_T1, m_st_T1);
	DDX_Control(pDX, IDC_ST_MT_T2, m_st_T2);
	DDX_Control(pDX, IDC_ST_MT_T3, m_st_T3);
	DDX_Control(pDX, IDC_ST_MT_T4, m_st_T4);
	DDX_Control(pDX, IDC_ST_MT_T5, m_st_T5);
	DDX_Control(pDX, IDC_ST_MT_T6, m_st_T6);
	DDX_Control(pDX, IDC_ST_MT_T7, m_st_T7);
	DDX_Control(pDX, IDC_ST_MT_T8, m_st_T8);
	DDX_Control(pDX, IDC_GB_MT_3, m_gb_3);
	DDX_Control(pDX, IDC_GB_MT_4, m_gb_4);
	DDX_Control(pDX, IDC_ST_MT_T9, m_st_T9);
	DDX_Control(pDX, IDC_ST_MT_T10, m_st_T10);
	DDX_Control(pDX, IDC_ST_MT_T11, m_st_T11);
	DDX_Control(pDX, IDC_ST_MT_T12, m_st_T12);
	DDX_Control(pDX, IDC_ST_MT_T13, m_st_T13);
	DDX_Control(pDX, IDC_BTN_MT_4, m_btn_4);
	DDX_Control(pDX, IDC_BTN_MT_5, m_btn_5);
	DDX_Control(pDX, IDC_BTN_MT_6, m_btn_6);
	DDX_Control(pDX, IDC_BTN_MT_7, m_btn_7);
	DDX_Control(pDX, IDC_BTN_MT_8, m_btn_8);
	DDX_Control(pDX, IDC_BTN_MT_9, m_btn_9);
	DDX_Control(pDX, IDC_BTN_MT_10, m_btn_10);
	DDX_Control(pDX, IDC_BTN_MT_11, m_btn_11);
	DDX_Control(pDX, IDC_BTN_MT_12, m_btn_12);
	DDX_Control(pDX, IDC_BTN_MT_13, m_btn_13);
	DDX_Control(pDX, IDC_BTN_MT_14, m_btn_14);
	DDX_Control(pDX, IDC_BTN_MT_15, m_btn_15);
	DDX_Control(pDX, IDC_BTN_MT_16, m_btn_16);
	DDX_Control(pDX, IDC_BTN_MT_17, m_btn_17);
	DDX_Control(pDX, IDC_ST_MT_T14, m_st_T14);
	DDX_Control(pDX, IDC_ST_MT_T15, m_st_T15);
	DDX_Control(pDX, IDC_BTN_MT_18, m_btn_18);
}


BEGIN_MESSAGE_MAP(CDlg_Motor, CDialogEx)
	ON_WM_ERASEBKGND()
	ON_WM_SIZE()
	ON_WM_TIMER()
	ON_NOTIFY(NM_CLICK, IDC_GC_MT_1, OnGridClick_GC_1)
	ON_NOTIFY(GVN_ENDLABELEDIT, IDC_GC_MT_1, OnEndLabel_GC_1)
	ON_BN_CLICKED(IDC_BTN_MT_1, &CDlg_Motor::OnBnClickedBtnMt1)
	ON_BN_CLICKED(IDC_BTN_MT_2, &CDlg_Motor::OnBnClickedBtnMt2)
	ON_BN_CLICKED(IDC_BTN_MT_3, &CDlg_Motor::OnBnClickedBtnMt3)
	ON_BN_CLICKED(IDC_BTN_MT_4, &CDlg_Motor::OnBnClickedBtnMt4)
	ON_BN_CLICKED(IDC_BTN_MT_5, &CDlg_Motor::OnBnClickedBtnMt5)
	ON_BN_CLICKED(IDC_BTN_MT_6, &CDlg_Motor::OnBnClickedBtnMt6)
	ON_BN_CLICKED(IDC_BTN_MT_7, &CDlg_Motor::OnBnClickedBtnMt7)
	ON_BN_CLICKED(IDC_BTN_MT_8, &CDlg_Motor::OnBnClickedBtnMt8)
	ON_BN_CLICKED(IDC_BTN_MT_9, &CDlg_Motor::OnBnClickedBtnMt9)
	ON_BN_CLICKED(IDC_BTN_MT_10, &CDlg_Motor::OnBnClickedBtnMt10)
	ON_BN_CLICKED(IDC_BTN_MT_11, &CDlg_Motor::OnBnClickedBtnMt11)
	ON_BN_CLICKED(IDC_BTN_MT_12, &CDlg_Motor::OnBnClickedBtnMt12)
	ON_BN_CLICKED(IDC_BTN_MT_13, &CDlg_Motor::OnBnClickedBtnMt13)
	ON_BN_CLICKED(IDC_BTN_MT_14, &CDlg_Motor::OnBnClickedBtnMt14)
	ON_BN_CLICKED(IDC_BTN_MT_15, &CDlg_Motor::OnBnClickedBtnMt15)
	ON_BN_CLICKED(IDC_BTN_MT_16, &CDlg_Motor::OnBnClickedBtnMt16)
	ON_BN_CLICKED(IDC_BTN_MT_17, &CDlg_Motor::OnBnClickedBtnMt17)
	ON_BN_CLICKED(IDC_BTN_MT_18, &CDlg_Motor::OnBnClickedBtnMt18)
END_MESSAGE_MAP()


// CDlg_Motor 메시지 처리기입니다.


BOOL CDlg_Motor::OnEraseBkgnd(CDC* pDC)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	// 전체 배경색
	CRect rect;
    GetClientRect(&rect);
    CBrush myBrush(CR_MAIN_BKGND);	// dialog background color
    CBrush *pOld = pDC->SelectObject(&myBrush);
    BOOL bRes  = pDC->PatBlt(0, 0, rect.Width(), rect.Height(), PATCOPY);
    pDC->SelectObject(pOld);		// restore old brush
    return FALSE;     
	//return CDialogEx::OnEraseBkgnd(pDC);
}


void CDlg_Motor::OnSize(UINT nType, int cx, int cy)
{
	CDialogEx::OnSize(nType, cx, cy);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	if(m_bCtrl_FirstLoad == TRUE) Cal_CtrlArea();
}


void CDlg_Motor::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CDialogEx::OnTimer(nIDEvent);
}


BOOL CDlg_Motor::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	Cal_CtrlArea();
	
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


BOOL CDlg_Motor::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message == WM_SYSKEYDOWN)
	{
		if(pMsg->wParam == VK_F4) 
			return TRUE;
	}

	if(pMsg->message == WM_KEYDOWN)
	{
		if( pMsg->wParam == VK_ESCAPE ||	
			//pMsg->wParam == VK_RETURN ||	
			//pMsg->wParam == VK_SPACE  ||	// Grid Ctrl Text 입력시 사용하기때문에...
			pMsg->wParam == VK_CANCEL )     
			return TRUE;        
	}

	return CDialogEx::PreTranslateMessage(pMsg);
}

void CDlg_Motor::Cal_Area()
{
	CRect ClientRect;
	GetClientRect(&ClientRect);

	int n55pW = (int)((float)ClientRect.Width()*0.55f);

	m_rcLeft.SetRect(ClientRect.left, ClientRect.top, ClientRect.left+n55pW, ClientRect.bottom);
	m_rcRight.SetRect(ClientRect.left+n55pW+CTRL_MARGIN4, ClientRect.top, ClientRect.right, ClientRect.bottom);
}

void CDlg_Motor::Cal_CtrlArea()
{
	Cal_Area();

	Cal_CtrlArea_Left();
	Cal_CtrlArea_Right();

	m_bCtrl_FirstLoad = TRUE;
}

void CDlg_Motor::Cal_CtrlArea_Left()
{
	CRect CalRect;
	CRect rcTop, rcGB, rcGBInside;
	int nBtnH = 68;
	int nBtnW = 80;

	rcTop.SetRect(m_rcLeft.left, m_rcLeft.top, m_rcLeft.right, m_rcLeft.top+nBtnH);
	rcGB.SetRect(m_rcLeft.left, m_rcLeft.top+nBtnH+CTRL_MARGIN3, m_rcLeft.right, m_rcLeft.bottom);
	rcGBInside = rcGB;
	rcGBInside.DeflateRect(6,28,6,6);

	CalRect.SetRect(rcTop.right-nBtnW, rcTop.top, rcTop.right, rcTop.bottom);
	m_btn_6.SetFontBtn(CTRL_FONT, 20, FW_BOLD);
	m_btn_6.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_6.SetTextColor(CR_BLACK);
	m_btn_6.SetIconPos(2);
	m_btn_6.SetIconID(IDI_LIST);
	m_btn_6.SetIconSize(48,48);
	m_btn_6.SetFlatType(TRUE);
	m_btn_6.MoveWindow(&CalRect);

	CalRect.SetRect(CalRect.left-nBtnW, rcTop.top, CalRect.left, rcTop.bottom);
	m_btn_2.SetFontBtn(CTRL_FONT, 20, FW_BOLD);
	m_btn_2.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_2.SetTextColor(CR_BLACK);
	m_btn_2.SetIconPos(2);
	m_btn_2.SetIconID(IDI_NEXT);
	m_btn_2.SetIconSize(48,48);
	m_btn_2.SetFlatType(TRUE);
	m_btn_2.MoveWindow(&CalRect);

	CalRect.SetRect(CalRect.left-nBtnW, rcTop.top, CalRect.left, rcTop.bottom);
	m_btn_1.SetFontBtn(CTRL_FONT, 20, FW_BOLD);
	m_btn_1.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_1.SetTextColor(CR_BLACK);
	m_btn_1.SetIconPos(2);
	m_btn_1.SetIconID(IDI_PREV);
	m_btn_1.SetIconSize(48,48);
	m_btn_1.SetFlatType(TRUE);
	m_btn_1.MoveWindow(&CalRect);

	CalRect.SetRect(rcTop.left, rcTop.top, CalRect.left, rcTop.bottom);
	m_st_MotorName.SetBkColor(CR_WHITE);
	m_st_MotorName.SetTextColor(CR_DARKORANGE);
	m_st_MotorName.SetFont(CTRL_FONT,24,FW_BOLD);
	m_st_MotorName.MoveWindow(&CalRect);

	m_gb_1.SetXPGroupStyle(CXPGroupBox::XPGB_WINDOW_2);
	m_gb_1.SetBackgroundColor(CR_DARKGRAY, CR_GB_BKGND);
	m_gb_1.SetBorderColor(CR_BLACK);
	m_gb_1.SetCatptionTextColor(CR_BLACK);
	m_gb_1.MoveWindow(&rcGB);
	m_GC_1.MoveWindow(&rcGBInside);
	Init_GC_1(21,6);
	SetMotorSettingValue(m_nCurMotorNum);
}

void CDlg_Motor::Cal_CtrlArea_Right()
{
	CRect CalRect;
	CRect rcGb1, rcGB1Inside, rcDiv[2][4];
	CRect rcGb2, rcGB2Inside, rcDiv1[3];
	CRect rcGb3, rcGB3Inside, rcDiv2[5];
	CRect rcGb4, rcGB4Inside;

	int n20pH = (int)((float)m_rcRight.Height()*0.20f);
	int n15pH = (int)((float)m_rcRight.Height()*0.15f);
	int n45pH = (int)((float)m_rcRight.Height()*0.45f);
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	rcGb1.SetRect(m_rcRight.left, m_rcRight.top, m_rcRight.right, m_rcRight.top+n20pH);
	rcGB1Inside = rcGb1;
	rcGB1Inside.DeflateRect(6,28,6,6);
	int n50pH = (int)((float)rcGB1Inside.Height()*0.5f);
	int n25pW = (int)((float)rcGB1Inside.Width()*0.25f);

	rcDiv[0][0].SetRect(rcGB1Inside.left, rcGB1Inside.top, rcGB1Inside.left+n25pW, rcGB1Inside.top+n50pH);
	rcDiv[0][1].SetRect(rcDiv[0][0].right, rcGB1Inside.top, rcDiv[0][0].right+n25pW, rcGB1Inside.top+n50pH);
	rcDiv[0][2].SetRect(rcDiv[0][1].right, rcGB1Inside.top, rcDiv[0][1].right+n25pW, rcGB1Inside.top+n50pH);
	rcDiv[0][3].SetRect(rcDiv[0][2].right, rcGB1Inside.top, rcGB1Inside.right, rcGB1Inside.top+n50pH);
	rcDiv[1][0].SetRect(rcGB1Inside.left, rcGB1Inside.top+n50pH, rcGB1Inside.left+n25pW, rcGB1Inside.bottom);
	rcDiv[1][1].SetRect(rcDiv[1][0].right, rcGB1Inside.top+n50pH, rcDiv[1][0].right+n25pW, rcGB1Inside.bottom);
	rcDiv[1][2].SetRect(rcDiv[1][1].right, rcGB1Inside.top+n50pH, rcDiv[1][1].right+n25pW, rcGB1Inside.bottom);
	rcDiv[1][3].SetRect(rcDiv[1][2].right, rcGB1Inside.top+n50pH, rcGB1Inside.right, rcGB1Inside.bottom);
	rcDiv[0][0].DeflateRect(2,2);
	rcDiv[0][1].DeflateRect(2,2);
	rcDiv[0][2].DeflateRect(2,2);
	rcDiv[0][3].DeflateRect(2,2);
	rcDiv[1][0].DeflateRect(2,2);
	rcDiv[1][1].DeflateRect(2,2);
	rcDiv[1][2].DeflateRect(2,2);
	rcDiv[1][3].DeflateRect(2,2);

	m_gb_2.SetXPGroupStyle(CXPGroupBox::XPGB_WINDOW_2);
	m_gb_2.SetBackgroundColor(CR_DARKGRAY, CR_GB_BKGND);
	m_gb_2.SetBorderColor(CR_BLACK);
	m_gb_2.SetCatptionTextColor(CR_BLACK);
	m_gb_2.MoveWindow(&rcGb1);

	m_st_T1.SetBkColor(CR_LAVENDER);
	m_st_T1.SetTextColor(CR_BLACK);
	m_st_T1.SetFont(CTRL_FONT,24,FW_BOLD);
	m_st_T1.MoveWindow(&rcDiv[0][0]);
	m_st_T2.SetBkColor(CR_LAVENDER);
	m_st_T2.SetTextColor(CR_BLACK);
	m_st_T2.SetFont(CTRL_FONT,24,FW_BOLD);
	m_st_T2.MoveWindow(&rcDiv[0][1]);
	m_st_T3.SetBkColor(CR_LAVENDER);
	m_st_T3.SetTextColor(CR_BLACK);
	m_st_T3.SetFont(CTRL_FONT,24,FW_BOLD);
	m_st_T3.MoveWindow(&rcDiv[0][2]);
	m_st_T4.SetBkColor(CR_LAVENDER);
	m_st_T4.SetTextColor(CR_BLACK);
	m_st_T4.SetFont(CTRL_FONT,24,FW_BOLD);
	m_st_T4.MoveWindow(&rcDiv[0][3]);
	m_st_T5.SetBkColor(CR_LAVENDER);
	m_st_T5.SetTextColor(CR_BLACK);
	m_st_T5.SetFont(CTRL_FONT,24,FW_BOLD);
	m_st_T5.MoveWindow(&rcDiv[1][0]);
	m_st_T6.SetBkColor(CR_LAVENDER);
	m_st_T6.SetTextColor(CR_BLACK);
	m_st_T6.SetFont(CTRL_FONT,24,FW_BOLD);
	m_st_T6.MoveWindow(&rcDiv[1][1]);
	m_st_T7.SetBkColor(CR_LAVENDER);
	m_st_T7.SetTextColor(CR_BLACK);
	m_st_T7.SetFont(CTRL_FONT,24,FW_BOLD);
	m_st_T7.MoveWindow(&rcDiv[1][2]);
	m_st_T8.SetBkColor(CR_LAVENDER);
	m_st_T8.SetTextColor(CR_BLACK);
	m_st_T8.SetFont(CTRL_FONT,24,FW_BOLD);
	m_st_T8.MoveWindow(&rcDiv[1][3]);
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	rcGb2.SetRect(m_rcRight.left, rcGb1.bottom+10, m_rcRight.right, rcGb1.bottom+n15pH);
	rcGB2Inside = rcGb2;
	rcGB2Inside.DeflateRect(6,35,6,12);

	rcDiv1[0].SetRect(rcGB2Inside.left, rcGB2Inside.top, rcGB2Inside.left + 60, rcGB2Inside.bottom);
	rcDiv1[1].SetRect(rcGB2Inside.left + 60, rcGB2Inside.top, rcGB2Inside.right - 100, rcGB2Inside.bottom);
	rcDiv1[2].SetRect(rcGB2Inside.right - 100, rcGB2Inside.top, rcGB2Inside.right, rcGB2Inside.bottom);

	m_gb_3.SetXPGroupStyle(CXPGroupBox::XPGB_WINDOW_2);
	m_gb_3.SetBackgroundColor(CR_DARKGRAY, CR_GB_BKGND);
	m_gb_3.SetBorderColor(CR_BLACK);
	m_gb_3.SetCatptionTextColor(CR_BLACK);
	m_gb_3.MoveWindow(&rcGb2);

	m_st_T9.SetBkColor(CR_WHITE);
	m_st_T9.SetTextColor(CR_BLACK);
	m_st_T9.SetFont(CTRL_FONT,18,FW_BOLD);
	m_st_T9.MoveWindow(&rcDiv1[0]);
	m_st_T10.SetBkColor(CR_WHITE);
	m_st_T10.SetTextColor(CR_BLACK);
	m_st_T10.SetFont(CTRL_FONT,18,FW_BOLD);
	m_st_T10.MoveWindow(&rcDiv1[1]);
	m_btn_3.SetFontBtn(CTRL_FONT, 20, FW_BOLD);
	m_btn_3.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_3.SetTextColor(CR_BLACK);
	m_btn_3.SetIconPos(2);
	m_btn_3.SetIconID(IDI_WORK);
	m_btn_3.SetIconSize(48,48);
	m_btn_3.SetFlatType(TRUE);
	m_btn_3.MoveWindow(&rcDiv1[2]);
	m_nCurSelMotorItemIndex = -1;
	Update_SelItemIndex(m_nCurSelMotorItemIndex);
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	rcGb3.SetRect(m_rcRight.left, rcGb2.bottom+10, m_rcRight.right, rcGb2.bottom+n45pH);
	rcGB3Inside = rcGb3;
	rcGB3Inside.DeflateRect(6,28,6,6);
	int nGB3_20pH = (int)((float)rcGB3Inside.Height()*0.20f);

	rcDiv2[0].SetRect(rcGB3Inside.left, rcGB3Inside.top, rcGB3Inside.right, rcGB3Inside.top+nGB3_20pH);
	rcDiv2[1].SetRect(rcGB3Inside.left, rcDiv2[0].bottom, rcGB3Inside.right, rcDiv2[0].bottom+nGB3_20pH);
	rcDiv2[2].SetRect(rcGB3Inside.left, rcDiv2[1].bottom, rcGB3Inside.right, rcDiv2[1].bottom+nGB3_20pH);
	rcDiv2[3].SetRect(rcGB3Inside.left, rcDiv2[2].bottom, rcGB3Inside.right, rcDiv2[2].bottom+nGB3_20pH);
	rcDiv2[4].SetRect(rcGB3Inside.left, rcDiv2[3].bottom, rcGB3Inside.right, rcGB3Inside.bottom);

	rcDiv2[0].DeflateRect(0,12,0,12);
	rcDiv2[1].DeflateRect(0,4,0,4);
	rcDiv2[2].DeflateRect(0,12,0,12);
	rcDiv2[3].DeflateRect(0,12,0,12);
	rcDiv2[4].DeflateRect(0,4,0,4);

	m_gb_4.SetXPGroupStyle(CXPGroupBox::XPGB_WINDOW_2);
	m_gb_4.SetBackgroundColor(CR_DARKGRAY, CR_GB_BKGND);
	m_gb_4.SetBorderColor(CR_BLACK);
	m_gb_4.SetCatptionTextColor(CR_BLACK);
	m_gb_4.MoveWindow(&rcGb3);

	int nDiv2_50pW = (int)((float)rcDiv2[0].Width()*0.5f);
	CalRect.SetRect(rcDiv2[0].left, rcDiv2[0].top, rcDiv2[0].left+nDiv2_50pW-CTRL_MARGIN2, rcDiv2[0].bottom);
	m_st_T11.SetBkColor(CR_WHITE);
	m_st_T11.SetTextColor(CR_RED);
	m_st_T11.SetFont(CTRL_FONT,30,FW_BOLD);
	m_st_T11.MoveWindow(&CalRect);
	CalRect.SetRect(rcDiv2[0].left+nDiv2_50pW+CTRL_MARGIN2, rcDiv2[0].top, rcDiv2[0].right, rcDiv2[0].bottom);
	m_st_T12.SetBkColor(CR_WHITE);
	m_st_T12.SetTextColor(CR_RED);
	m_st_T12.SetFont(CTRL_FONT,30,FW_BOLD);
	m_st_T12.MoveWindow(&CalRect);

	CalRect.SetRect(rcDiv2[1].left, rcDiv2[1].top, rcDiv2[1].left+120, rcDiv2[1].bottom);
	m_btn_4.SetFontBtn(CTRL_FONT, 20, FW_BOLD);
	m_btn_4.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_4.SetTextColor(CR_BLACK);
	m_btn_4.SetFlatType(TRUE);
	m_btn_4.MoveWindow(&CalRect);
	CalRect.SetRect(CalRect.right, rcDiv2[1].top, CalRect.right+120, rcDiv2[1].bottom);
	m_btn_5.SetFontBtn(CTRL_FONT, 20, FW_BOLD);
	m_btn_5.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_5.SetTextColor(CR_BLACK);
	m_btn_5.SetFlatType(TRUE);
	m_btn_5.MoveWindow(&CalRect);
	CalRect.SetRect(CalRect.right, rcDiv2[1].top, CalRect.right+120, rcDiv2[1].bottom);
	m_btn_18.SetFontBtn(CTRL_FONT, 20, FW_BOLD);
	m_btn_18.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_18.SetTextColor(CR_BLACK);
	m_btn_18.SetFlatType(TRUE);
	m_btn_18.MoveWindow(&CalRect);
	if(m_nCurMotorNum >= 0 && m_nCurMotorNum <= 3) // Index X,Y,Z,T
	{
		m_btn_18.ShowWindow(SW_SHOW);
	}
	else m_btn_18.ShowWindow(SW_HIDE);

	CalRect.SetRect(rcDiv2[2].left, rcDiv2[2].top, rcDiv2[2].left+80, rcDiv2[2].bottom);
	m_btn_14.SetFontBtn(CTRL_FONT, 24, FW_BOLD);
	m_btn_14.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_14.SetTextColor(CR_BLACK);
	m_btn_14.SetFlatType(TRUE);
	m_btn_14.MoveWindow(&CalRect);
	CalRect.SetRect(CalRect.right, rcDiv2[2].top, rcDiv2[2].left+nDiv2_50pW, rcDiv2[2].bottom);
	m_st_T13.SetBkColor(CR_WHITE);
	m_st_T13.SetTextColor(CR_RED);
	m_st_T13.SetFont(CTRL_FONT,24,FW_BOLD);
	m_st_T13.MoveWindow(&CalRect);
	CalRect.SetRect(CalRect.right, rcDiv2[2].top, CalRect.right+80, rcDiv2[2].bottom);
	m_btn_15.SetFontBtn(CTRL_FONT, 24, FW_BOLD);
	m_btn_15.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_15.SetTextColor(CR_BLACK);
	m_btn_15.SetFlatType(TRUE);
	m_btn_15.MoveWindow(&CalRect);
	CalRect.SetRect(CalRect.right, rcDiv2[2].top, rcDiv2[2].right, rcDiv2[2].bottom);
	m_st_T14.SetBkColor(CR_WHITE);
	m_st_T14.SetTextColor(CR_RED);
	m_st_T14.SetFont(CTRL_FONT,24,FW_BOLD);
	m_st_T14.MoveWindow(&CalRect);

	CalRect.SetRect(rcDiv2[3].left, rcDiv2[3].top, rcDiv2[3].left+60, rcDiv2[3].bottom);
	m_btn_7.SetFontBtn(CTRL_FONT, 18, FW_BOLD);
	m_btn_7.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_7.SetTextColor(CR_BLACK);
	m_btn_7.SetFlatType(TRUE);
	m_btn_7.MoveWindow(&CalRect);
	CalRect.SetRect(CalRect.right, rcDiv2[3].top, CalRect.right+60, rcDiv2[3].bottom);
	m_btn_8.SetFontBtn(CTRL_FONT, 18, FW_BOLD);
	m_btn_8.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_8.SetTextColor(CR_BLACK);
	m_btn_8.SetFlatType(TRUE);
	m_btn_8.MoveWindow(&CalRect);
	CalRect.SetRect(CalRect.right, rcDiv2[3].top, CalRect.right+60, rcDiv2[3].bottom);
	m_btn_9.SetFontBtn(CTRL_FONT, 18, FW_BOLD);
	m_btn_9.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_9.SetTextColor(CR_BLACK);
	m_btn_9.SetFlatType(TRUE);
	m_btn_9.MoveWindow(&CalRect);
	CalRect.SetRect(CalRect.right, rcDiv2[3].top, CalRect.right+60, rcDiv2[3].bottom);
	m_btn_10.SetFontBtn(CTRL_FONT, 18, FW_BOLD);
	m_btn_10.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_10.SetTextColor(CR_BLACK);
	m_btn_10.SetFlatType(TRUE);
	m_btn_10.MoveWindow(&CalRect);
	CalRect.SetRect(CalRect.right, rcDiv2[3].top, rcDiv2[3].right-60, rcDiv2[3].bottom);
	m_st_T15.SetBkColor(CR_WHITE);
	m_st_T15.SetTextColor(CR_RED);
	m_st_T15.SetFont(CTRL_FONT,24,FW_BOLD);
	m_st_T15.MoveWindow(&CalRect);
	CalRect.SetRect(CalRect.right, rcDiv2[3].top, rcDiv2[3].right, rcDiv2[3].bottom);
	m_btn_11.SetFontBtn(CTRL_FONT, 18, FW_BOLD);
	m_btn_11.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_11.SetTextColor(CR_BLACK);
	m_btn_11.SetFlatType(TRUE);
	m_btn_11.MoveWindow(&CalRect);

	CalRect.SetRect(rcDiv2[4].left, rcDiv2[4].top, rcDiv2[4].left+120, rcDiv2[4].bottom);
	m_btn_12.SetFontBtn(CTRL_FONT, 24, FW_BOLD);
	m_btn_12.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_12.SetTextColor(CR_BLACK);
	m_btn_12.SetIconPos(0);
	m_btn_12.SetIconID(IDI_LEFT);
	m_btn_12.SetIconSize(48,48);
	m_btn_12.SetFlatType(TRUE);
	m_btn_12.MoveWindow(&CalRect);
	CalRect.SetRect(CalRect.right, rcDiv2[4].top, CalRect.right+120, rcDiv2[4].bottom);
	m_btn_13.SetFontBtn(CTRL_FONT, 24, FW_BOLD);
	m_btn_13.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_13.SetTextColor(CR_BLACK);
	m_btn_13.SetIconPos(0);
	m_btn_13.SetIconID(IDI_RIGHT);
	m_btn_13.SetIconSize(48,48);
	m_btn_13.SetFlatType(TRUE);
	m_btn_13.MoveWindow(&CalRect);
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	CalRect.SetRect(m_rcRight.left, m_rcRight.bottom - 60, m_rcRight.left + 160, m_rcRight.bottom);
	CalRect.DeflateRect(2,2);
	m_btn_16.SetFontBtn(CTRL_FONT, 24, FW_BOLD);
	m_btn_16.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_16.SetTextColor(CR_BLACK);
	m_btn_16.SetIconPos(0);
	m_btn_16.SetIconID(IDI_SAVE);
	m_btn_16.SetIconSize(48,48);
	m_btn_16.SetFlatType(TRUE);
	m_btn_16.MoveWindow(&CalRect);

	CalRect.SetRect(m_rcRight.right - 140, m_rcRight.bottom - 60, m_rcRight.right, m_rcRight.bottom);
	CalRect.DeflateRect(2,2);
	m_btn_17.SetFontBtn(CTRL_FONT, 24, FW_BOLD);
	m_btn_17.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_17.SetTextColor(CR_BLACK);
	m_btn_17.SetIconPos(0);
	m_btn_17.SetIconID(IDI_CLOSE);
	m_btn_17.SetIconSize(48,48);
	m_btn_17.SetFlatType(TRUE);
	m_btn_17.MoveWindow(&CalRect);
}

bool CDlg_Motor::Init_GC_1(int nRows, int nCols)
{
	BOOL m_bEditable = TRUE;
    BOOL m_bListMode = TRUE;
    int m_nRows = nRows;
    int m_nCols = nCols;
    int m_nFixRows = 1;
    int m_nFixCols = 1;

    m_GC_1.SetEditable(m_bEditable);
    m_GC_1.SetListMode(m_bListMode);
    m_GC_1.EnableDragAndDrop(FALSE);
    m_GC_1.SetTextBkColor(RGB(0xFF, 0xFF, 0xE0));

    m_GC_1.SetRowCount(m_nRows);
    m_GC_1.SetColumnCount(m_nCols);
    m_GC_1.SetFixedRowCount(m_nFixRows);
    m_GC_1.SetFixedColumnCount(m_nFixCols);

	m_GC_1.EnableSelection(false);
	m_GC_1.SetSingleColSelection(true);
	m_GC_1.SetSingleRowSelection(true);
	m_GC_1.SetFixedColumnSelection(false);
    m_GC_1.SetFixedRowSelection(false);

	m_GC_1.SetRowResize(false);
	m_GC_1.SetColumnResize(false);

	CRect ClientRect;
	GetDlgItem(IDC_GC_MT_1)->GetWindowRect(&ClientRect);
	if(ClientRect.Width() < 4) return false;
	if(ClientRect.Height() < 4) return false;
	int nClientWidth = ClientRect.Width()-4;
	int nClientHeight = ClientRect.Height()-4;
	int n08pW = (int)(nClientWidth*0.08f);
	int n40pW = (int)(nClientWidth*0.4f);
	int n13pW = (int)(nClientWidth*0.13f);
	int nRemainW = nClientWidth - n08pW - n40pW - n13pW*3;
	//int nCellW = nClientWidth/m_nCols;
	int nCellH = nClientHeight/m_nRows;
	//int nLastCellW = nClientWidth-nCellW*(m_nCols-1);
	int nLastCellH = nClientHeight-nCellH*(m_nRows-1);
	//int nRemainW = nLastCellW - nCellW;
	int nRemainH = nLastCellH - nCellH;
	for (int i = 0; i < m_GC_1.GetRowCount(); i++) 
	{
		if(i == m_nRows-1)
		{
				m_GC_1.SetRowHeight(i,nCellH);
		}
		else 
		{
			if(nRemainH > 0)
			{
				m_GC_1.SetRowHeight(i,nCellH + 1);
				nRemainH --;
			}
			else
				m_GC_1.SetRowHeight(i,nCellH);
		}
	}
	for (int j = 0; j < m_GC_1.GetColumnCount(); j++)
	{
		if(j == 0) m_GC_1.SetColumnWidth(j,n08pW);
		else if(j == 1) m_GC_1.SetColumnWidth(j,n40pW);
		else if(j == 2) m_GC_1.SetColumnWidth(j,n13pW);
		else if(j == 3) m_GC_1.SetColumnWidth(j,n13pW);
		else if(j == 4) m_GC_1.SetColumnWidth(j,n13pW);
		else if(j == 5) m_GC_1.SetColumnWidth(j,nRemainW);
	}
    DWORD dwTextStyle = DT_CENTER|DT_VCENTER|DT_SINGLELINE;

    // fill rows/cols with text
    for (int row = 0; row < m_GC_1.GetRowCount(); row++) 
	{
        for (int col = 0; col < m_GC_1.GetColumnCount(); col++) 
		{ 
            GV_ITEM Item;
            Item.mask = GVIF_TEXT|GVIF_FORMAT;
            Item.row = row;
            Item.col = col;
			Item.nFormat = dwTextStyle;

			if(row == 0)
			{
				if(col == 0) Item.strText.Format(_T("#"));
				else if(col == 1) Item.strText.Format(_T("NAME"));
				else if(col == 2) Item.strText.Format(_T("SPEED"));
				else if(col == 3) Item.strText.Format(_T("POS"));
				else if(col == 4) Item.strText.Format(_T("ADDSPD"));
				else if(col == 5) Item.strText.Format(_T("ADDPOS"));
			}
			else
			{
			}

			if(row == 0 || col == 0) m_GC_1.SetItemBkColour(row, col, CR_SLATEGRAY);
			m_GC_1.SetItemFgColour(row, col, RGB(0,0,0));
			m_GC_1.SetItemFont(row, col, CTRL_FONT, 18, FW_BOLD);
            m_GC_1.SetItem(&Item);  
        }
    }
       
    return TRUE;  // return TRUE  unless you set the focus to a control
}

void CDlg_Motor::OnGridClick_GC_1(NMHDR *pNotifyStruct, LRESULT* /*pResult*/)
{
	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*) pNotifyStruct;

	int nSelRow = pItem->iRow;
	int nSelCol = pItem->iColumn;

	CString strText = m_GC_1.GetItemText(nSelRow, 1);
	if(strText.IsEmpty() == FALSE)
	{
		m_nCurSelMotorItemIndex = nSelRow - 1;
		Update_SelItemIndex(m_nCurSelMotorItemIndex);
	}
	else
	{
		m_nCurSelMotorItemIndex = -1;
		Update_SelItemIndex(m_nCurSelMotorItemIndex);
	}

	if(nSelCol == 2) // SPD
	{
		CDlg_NumPad dlg;
		dlg.m_strCaption = _T("Change Motor Speed Value");
		if(dlg.DoModal() == IDOK)
		{
			int nPLCNum = CAppSetting::Instance()->GetGroupNumber();
			int nCalAddress = m_nCurMotor_StartAddressD + 20+(nSelRow-1)*4;
			double dValue = _wtof(dlg.m_strValue);
			CDataManager::Instance()->PLC_Write_D2(nPLCNum, nCalAddress, (int)dValue);
		}
	}
	if(nSelCol == 3) // POS
	{
		CDlg_NumPad dlg;
		dlg.m_strCaption = _T("Change Motor Position Value");
		if(dlg.DoModal() == IDOK)
		{
			int nPLCNum = CAppSetting::Instance()->GetGroupNumber();
			int nCalAddress = m_nCurMotor_StartAddressD + 22+(nSelRow-1)*4;
			double dValue = _wtof(dlg.m_strValue)*1000.0f;
			CDataManager::Instance()->PLC_Write_D2(nPLCNum, nCalAddress, (int)dValue);
		}
	}
	if(nSelCol == 4) // SPD ADDRESS
	{
		CDlg_NumPad dlg;
		dlg.m_strCaption = _T("Change Motor Speed Address");
		if(dlg.DoModal() == IDOK)
		{
			int nAddress = _wtoi(dlg.m_strValue);
			CMotorManualSetting::Instance()->SetMotorItemAddress_Spd(m_nCurMotorNum, m_nCurSelMotorItemIndex, nAddress);
		}
	}
	if(nSelCol == 5) // POS ADDRESS
	{
		CDlg_NumPad dlg;
		dlg.m_strCaption = _T("Change Motor Position Address");
		if(dlg.DoModal() == IDOK)
		{
			int nAddress = _wtoi(dlg.m_strValue);
			CMotorManualSetting::Instance()->SetMotorItemAddress_Pos(m_nCurMotorNum, m_nCurSelMotorItemIndex, nAddress);
		}
	}
}

void CDlg_Motor::OnEndLabel_GC_1(NMHDR *pNotifyStruct, LRESULT* pResult)
{
	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*) pNotifyStruct;

	int nSelRow = pItem->iRow;
	int nSelCol = pItem->iColumn;
	if(nSelCol == 1)
	{
		CMotorManualSetting::Instance()->SetMotorItemText(m_nCurMotorNum, nSelRow-1, m_GC_1.GetItemText(nSelRow, nSelCol));
	}
}

void CDlg_Motor::SetMotorSettingValue(int nMotorIndex)
{
	stMotorManual_Info stMMI = CMotorManualSetting::Instance()->GetMotorManualInfo();
	if(stMMI.m_nMotorCount <= 0) return;
	if(m_nCurMotorNum > stMMI.m_nMotorCount-1) return;

	// MOTOR NAME UPDATE
	CString strMotorName;
	strMotorName.Format(_T(" [%02d] %s"), m_nCurMotorNum+1, stMMI.m_vecMotorInfo[m_nCurMotorNum].m_strMotorName);
	m_st_MotorName.SetWindowText(strMotorName);
	// MOTOR START ADDRESS GET
	m_nCurMotor_StartAddressD = stMMI.m_vecMotorInfo[m_nCurMotorNum].m_nStartAddressD;
	m_nCurMotor_StartAddressM = stMMI.m_vecMotorInfo[m_nCurMotorNum].m_nStartAddressM;
	m_nCurMotor_StartAddressL = stMMI.m_vecMotorInfo[m_nCurMotorNum].m_nStartAddressL;

	// 
	DWORD dwTextStyle = DT_CENTER|DT_VCENTER|DT_SINGLELINE;
    for (int row = 0; row < m_GC_1.GetRowCount(); row++) 
	{
        for (int col = 0; col < m_GC_1.GetColumnCount(); col++) 
		{ 
            GV_ITEM Item;
            Item.mask = GVIF_TEXT|GVIF_FORMAT;
            Item.row = row;
            Item.col = col;
			Item.nFormat = dwTextStyle;

			if(row == 0)
			{
				if(col == 0) Item.strText.Format(_T("#"));
				else if(col == 1) Item.strText.Format(_T("NAME"));
				else if(col == 2) Item.strText.Format(_T("SPEED"));
				else if(col == 3) Item.strText.Format(_T("POS"));
				else if(col == 4) Item.strText.Format(_T("ADDSPD"));
				else if(col == 5) Item.strText.Format(_T("ADDPOS"));
			}
			else
			{
				if(col == 0)
				{
					Item.strText.Format(_T("%d"), row);
				}
				else if(col == 1)
				{
					Item.strText = stMMI.m_vecMotorInfo[m_nCurMotorNum].m_stMotorItem[row-1].m_strItemName;
				}
				else if(col == 2)
				{
					Item.strText.Format(_T("%d"), (int)stMMI.m_vecMotorInfo[m_nCurMotorNum].m_stMotorItem[row-1].m_dItemSpeed);
				}
				else if(col == 3)
				{
					Item.strText.Format(_T("%.3f"), stMMI.m_vecMotorInfo[m_nCurMotorNum].m_stMotorItem[row-1].m_dItemPostion);
				}
				else if(col == 4)
				{
					Item.strText.Format(_T("%d"), (int)stMMI.m_vecMotorInfo[m_nCurMotorNum].m_stMotorItem[row-1].m_nAddress_Spd);
				}
				else if(col == 5)
				{
					Item.strText.Format(_T("%d"), (int)stMMI.m_vecMotorInfo[m_nCurMotorNum].m_stMotorItem[row-1].m_nAddress_Pos);
				}
			}

			if(row == 0 || col == 0) m_GC_1.SetItemBkColour(row, col, CR_SLATEGRAY);
			m_GC_1.SetItemFgColour(row, col, RGB(0,0,0));
            m_GC_1.SetItem(&Item);  
        }
    }

	m_GC_1.Invalidate();
}

void CDlg_Motor::Update_SelItemIndex(int nItemIndex)
{
	if(nItemIndex < 0)
	{
		m_st_T9.SetWindowText(_T(""));
		m_st_T10.SetWindowText(_T(""));

		m_btn_3.EnableWindow(FALSE);
	}
	else
	{
		m_st_T9.SetWindowText(m_GC_1.GetItemText(nItemIndex+1, 0));
		m_st_T10.SetWindowText(m_GC_1.GetItemText(nItemIndex+1, 1));

		// 데이터가 있더라도 스피드가 0 이면 비활성
		double dValuePos = _wtof(m_GC_1.GetItemText(nItemIndex+1, 2));
		double dValueSpd = _wtof(m_GC_1.GetItemText(nItemIndex+1, 3));
		if(dValueSpd != 0.0f) m_btn_3.EnableWindow(TRUE);
		else m_btn_3.EnableWindow(FALSE);
	}
}

void CDlg_Motor::Update_PLC_Value()
{
	float fValue;
	float fPos, fSpeed;
	int nAddress_Pos, nAddress_Spd;
	SHORT sValue;
	int nPLCNum = CAppSetting::Instance()->GetGroupNumber();
	stMotorManual_Info stMMI = CMotorManualSetting::Instance()->GetMotorManualInfo();
	if(stMMI.m_nMotorCount <= 0) return;
	
	MADDRESS stMAddress = CDataManager::Instance()->m_PLCReaderD_Motor.GetValueM(m_nCurMotor_StartAddressD+7);
	if(stMAddress.m_xBit_0 == 0) // CCW LIMIT = LIMIT -
	{
		m_st_T2.SetBkColor(CR_LAVENDER);
		m_st_T2.SetTextColor(CR_BLACK);
	}
	else
	{
		m_st_T2.SetBkColor(CR_RED);
		m_st_T2.SetTextColor(CR_WHITE);
	}
	if(stMAddress.m_xBit_1 == 0) // CW LIMIT = LIMIT +
	{
		m_st_T1.SetBkColor(CR_LAVENDER);
		m_st_T1.SetTextColor(CR_BLACK);
	}
	else
	{
		m_st_T1.SetBkColor(CR_RED);
		m_st_T1.SetTextColor(CR_WHITE);
	}
	if(stMAddress.m_xBit_6 == 0) // HOME
	{
		m_st_T5.SetBkColor(CR_LAVENDER);
		m_st_T5.SetTextColor(CR_BLACK);
	}
	else
	{
		m_st_T5.SetBkColor(CR_RED);
		m_st_T5.SetTextColor(CR_WHITE);
	}
	// MOTOR CURRENT POSITION VALUE
	fValue = (float)CDataManager::Instance()->m_PLCReaderD_Motor.GetValue2(m_nCurMotor_StartAddressD+0)/1000.0f;
	CString strValue;
	strValue.Format(_T("%.3f mm "), fValue);
	m_st_T11.SetWindowText(strValue);
	// MOTOR CURRENT SPEED VALUE
	strValue.Format(_T("%05d  %05d mm/min "), 
		CDataManager::Instance()->m_PLCReaderD_Motor.GetValue(m_nCurMotor_StartAddressD+5),
		CDataManager::Instance()->m_PLCReaderD_Motor.GetValue(m_nCurMotor_StartAddressD+2) );
	m_st_T12.SetWindowText(strValue);
	// MOTOR ERROR CODE VALUE
	sValue = CDataManager::Instance()->m_PLCReaderD_Motor.GetValue(m_nCurMotor_StartAddressD+6);
	if(sValue)
	{
		m_st_T4.SetBkColor(CR_RED);
		m_st_T4.SetTextColor(CR_WHITE);

		strValue.Format(_T("%d"), sValue);
		m_st_T8.SetWindowText(strValue);
	}
	else
	{
		m_st_T4.SetBkColor(CR_LAVENDER);
		m_st_T4.SetTextColor(CR_BLACK);

		strValue.Format(_T("%d"), sValue);
		m_st_T8.SetWindowText(strValue);
	}
	// ACCEL SPEED
	sValue = CDataManager::Instance()->m_PLCReaderD_Motor.GetValue(m_nCurMotor_StartAddressD+12);
	if(sValue)
	{
		strValue.Format(_T("%d"), sValue);
		m_st_T13.SetWindowText(strValue);
	}
	// DECEL SPEED
	sValue = CDataManager::Instance()->m_PLCReaderD_Motor.GetValue(m_nCurMotor_StartAddressD+14);
	if(sValue)
	{
		strValue.Format(_T("%d"), sValue);
		m_st_T14.SetWindowText(strValue);
	}
	// JOG SPEED
	sValue = CDataManager::Instance()->m_PLCReaderD_Motor.GetValue(m_nCurMotor_StartAddressD+18)/10;
	if(sValue)
	{
		strValue.Format(_T("%d"), sValue);
		m_st_T15.SetWindowText(strValue);
	}
	// HOME BUTTON LAMP CHECK
	int nHomeMAddressBit = 45 + m_nCurMotor_StartAddressM;
	if(CDataManager::Instance()->PLC_Read_M2(nPLCNum, nHomeMAddressBit))
	{
		m_btn_4.SetButtonColor(CR_LIMEGREEN, CR_LIGHTGRAY);
		m_btn_4.SetTextColor(CR_WHITE);
		m_btn_4.Invalidate();
	}
	else
	{
		m_btn_4.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
		m_btn_4.SetTextColor(CR_BLACK);
		m_btn_4.Invalidate();
	}
	// SERVO BUTTON LAMP CHECK
	int nServoMAddressBit = 43 + m_nCurMotor_StartAddressM;
	if(CDataManager::Instance()->PLC_Read_M2(nPLCNum, nServoMAddressBit))
	{
		m_btn_5.SetButtonColor(CR_LIMEGREEN, CR_LIGHTGRAY);
		m_btn_5.SetTextColor(CR_WHITE);
		m_btn_5.Invalidate();
	}
	else
	{
		m_btn_5.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
		m_btn_5.SetTextColor(CR_BLACK);
		m_btn_5.Invalidate();
	}
	// << BUTTON LAMP CHECK
	int nPrevMAddressBit = 41 + m_nCurMotor_StartAddressM;
	if(CDataManager::Instance()->PLC_Read_M2(nPLCNum, nPrevMAddressBit))
	{
		m_btn_12.SetButtonColor(CR_LIMEGREEN, CR_LIGHTGRAY);
		m_btn_12.SetTextColor(CR_WHITE);
		m_btn_12.Invalidate();
	}
	else
	{
		m_btn_12.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
		m_btn_12.SetTextColor(CR_BLACK);
		m_btn_12.Invalidate();
	}
	// >> BUTTON LAMP CHECK
	int nNextMAddressBit = 42 + m_nCurMotor_StartAddressM;
	if(CDataManager::Instance()->PLC_Read_M2(nPLCNum, nNextMAddressBit))
	{
		m_btn_13.SetButtonColor(CR_LIMEGREEN, CR_LIGHTGRAY);
		m_btn_13.SetTextColor(CR_WHITE);
		m_btn_13.Invalidate();
	}
	else
	{
		m_btn_13.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
		m_btn_13.SetTextColor(CR_BLACK);
		m_btn_13.Invalidate();
	}
	// MOTOR ITEM #1
	if(CDataManager::Instance()->m_PLCReaderL_Motor.IsON(3001 + m_nCurMotor_StartAddressL))
		m_GC_1.SetItemBkColour(1,1,CR_LIGHTGREEN);
	else
		m_GC_1.SetItemBkColour(1,1,RGB(0xFF, 0xFF, 0xE0));
	nAddress_Spd = stMMI.m_vecMotorInfo[m_nCurMotorNum].m_stMotorItem[0].m_nAddress_Spd;
	fSpeed = (float)CDataManager::Instance()->m_PLCReaderD_Motor.GetValue2(nAddress_Spd)/10;
	nAddress_Pos = stMMI.m_vecMotorInfo[m_nCurMotorNum].m_stMotorItem[0].m_nAddress_Pos;
	fPos = (float)CDataManager::Instance()->m_PLCReaderD_Motor.GetValue2(nAddress_Pos)/1000.0f;
	CMotorManualSetting::Instance()->SetMotorItemValue(m_nCurMotorNum, 0, fPos, fSpeed);
	// MOTOR ITEM #2
	if(CDataManager::Instance()->m_PLCReaderL_Motor.IsON(3002 + m_nCurMotor_StartAddressL))
		m_GC_1.SetItemBkColour(2,1,CR_LIGHTGREEN);
	else
		m_GC_1.SetItemBkColour(2,1,RGB(0xFF, 0xFF, 0xE0));
	nAddress_Spd = stMMI.m_vecMotorInfo[m_nCurMotorNum].m_stMotorItem[1].m_nAddress_Spd;
	fSpeed = (float)CDataManager::Instance()->m_PLCReaderD_Motor.GetValue2(nAddress_Spd)/10;
	nAddress_Pos = stMMI.m_vecMotorInfo[m_nCurMotorNum].m_stMotorItem[1].m_nAddress_Pos;
	fPos = (float)CDataManager::Instance()->m_PLCReaderD_Motor.GetValue2(nAddress_Pos)/1000.0f;
	CMotorManualSetting::Instance()->SetMotorItemValue(m_nCurMotorNum, 1, fPos, fSpeed);
	// MOTOR ITEM #3
	if(CDataManager::Instance()->m_PLCReaderL_Motor.IsON(3003 + m_nCurMotor_StartAddressL))
		m_GC_1.SetItemBkColour(3,1,CR_LIGHTGREEN);
	else
		m_GC_1.SetItemBkColour(3,1,RGB(0xFF, 0xFF, 0xE0));
	nAddress_Spd = stMMI.m_vecMotorInfo[m_nCurMotorNum].m_stMotorItem[2].m_nAddress_Spd;
	fSpeed = (float)CDataManager::Instance()->m_PLCReaderD_Motor.GetValue2(nAddress_Spd)/10;
	nAddress_Pos = stMMI.m_vecMotorInfo[m_nCurMotorNum].m_stMotorItem[2].m_nAddress_Pos;
	fPos = (float)CDataManager::Instance()->m_PLCReaderD_Motor.GetValue2(nAddress_Pos)/1000.0f;
	CMotorManualSetting::Instance()->SetMotorItemValue(m_nCurMotorNum, 2, fPos, fSpeed);
	// MOTOR ITEM #4
	if(CDataManager::Instance()->m_PLCReaderL_Motor.IsON(3004 + m_nCurMotor_StartAddressL))
		m_GC_1.SetItemBkColour(4,1,CR_LIGHTGREEN);
	else
		m_GC_1.SetItemBkColour(4,1,RGB(0xFF, 0xFF, 0xE0));
	nAddress_Spd = stMMI.m_vecMotorInfo[m_nCurMotorNum].m_stMotorItem[3].m_nAddress_Spd;
	fSpeed = (float)CDataManager::Instance()->m_PLCReaderD_Motor.GetValue2(nAddress_Spd)/10;
	nAddress_Pos = stMMI.m_vecMotorInfo[m_nCurMotorNum].m_stMotorItem[3].m_nAddress_Pos;
	fPos = (float)CDataManager::Instance()->m_PLCReaderD_Motor.GetValue2(nAddress_Pos)/1000.0f;
	CMotorManualSetting::Instance()->SetMotorItemValue(m_nCurMotorNum, 3, fPos, fSpeed);
	// MOTOR ITEM #5
	if(CDataManager::Instance()->m_PLCReaderL_Motor.IsON(3005 + m_nCurMotor_StartAddressL))
		m_GC_1.SetItemBkColour(5,1,CR_LIGHTGREEN);
	else
		m_GC_1.SetItemBkColour(5,1,RGB(0xFF, 0xFF, 0xE0));
	nAddress_Spd = stMMI.m_vecMotorInfo[m_nCurMotorNum].m_stMotorItem[4].m_nAddress_Spd;
	fSpeed = (float)CDataManager::Instance()->m_PLCReaderD_Motor.GetValue2(nAddress_Spd)/10;
	nAddress_Pos = stMMI.m_vecMotorInfo[m_nCurMotorNum].m_stMotorItem[4].m_nAddress_Pos;
	fPos = (float)CDataManager::Instance()->m_PLCReaderD_Motor.GetValue2(nAddress_Pos)/1000.0f;
	CMotorManualSetting::Instance()->SetMotorItemValue(m_nCurMotorNum, 4, fPos, fSpeed);
	// MOTOR ITEM #6
	if(CDataManager::Instance()->m_PLCReaderL_Motor.IsON(3006 + m_nCurMotor_StartAddressL))
		m_GC_1.SetItemBkColour(6,1,CR_LIGHTGREEN);
	else
		m_GC_1.SetItemBkColour(6,1,RGB(0xFF, 0xFF, 0xE0));
	nAddress_Spd = stMMI.m_vecMotorInfo[m_nCurMotorNum].m_stMotorItem[5].m_nAddress_Spd;
	fSpeed = (float)CDataManager::Instance()->m_PLCReaderD_Motor.GetValue2(nAddress_Spd)/10;
	nAddress_Pos = stMMI.m_vecMotorInfo[m_nCurMotorNum].m_stMotorItem[5].m_nAddress_Pos;
	fPos = (float)CDataManager::Instance()->m_PLCReaderD_Motor.GetValue2(nAddress_Pos)/1000.0f;
	CMotorManualSetting::Instance()->SetMotorItemValue(m_nCurMotorNum, 5, fPos, fSpeed);
	// MOTOR ITEM #7
	if(CDataManager::Instance()->m_PLCReaderL_Motor.IsON(3007 + m_nCurMotor_StartAddressL))
		m_GC_1.SetItemBkColour(7,1,CR_LIGHTGREEN);
	else
		m_GC_1.SetItemBkColour(7,1,RGB(0xFF, 0xFF, 0xE0));
	nAddress_Spd = stMMI.m_vecMotorInfo[m_nCurMotorNum].m_stMotorItem[6].m_nAddress_Spd;
	fSpeed = (float)CDataManager::Instance()->m_PLCReaderD_Motor.GetValue2(nAddress_Spd)/10;
	nAddress_Pos = stMMI.m_vecMotorInfo[m_nCurMotorNum].m_stMotorItem[6].m_nAddress_Pos;
	fPos = (float)CDataManager::Instance()->m_PLCReaderD_Motor.GetValue2(nAddress_Pos)/1000.0f;
	CMotorManualSetting::Instance()->SetMotorItemValue(m_nCurMotorNum, 6, fPos, fSpeed);
	// MOTOR ITEM #8
	if(CDataManager::Instance()->m_PLCReaderL_Motor.IsON(3008 + m_nCurMotor_StartAddressL))
		m_GC_1.SetItemBkColour(8,1,CR_LIGHTGREEN);
	else
		m_GC_1.SetItemBkColour(8,1,RGB(0xFF, 0xFF, 0xE0));
	nAddress_Spd = stMMI.m_vecMotorInfo[m_nCurMotorNum].m_stMotorItem[7].m_nAddress_Spd;
	fSpeed = (float)CDataManager::Instance()->m_PLCReaderD_Motor.GetValue2(nAddress_Spd)/10;
	nAddress_Pos = stMMI.m_vecMotorInfo[m_nCurMotorNum].m_stMotorItem[7].m_nAddress_Pos;
	fPos = (float)CDataManager::Instance()->m_PLCReaderD_Motor.GetValue2(nAddress_Pos)/1000.0f;
	CMotorManualSetting::Instance()->SetMotorItemValue(m_nCurMotorNum, 7, fPos, fSpeed);
	// MOTOR ITEM #9
	if(CDataManager::Instance()->m_PLCReaderL_Motor.IsON(3009 + m_nCurMotor_StartAddressL))
		m_GC_1.SetItemBkColour(9,1,CR_LIGHTGREEN);
	else
		m_GC_1.SetItemBkColour(9,1,RGB(0xFF, 0xFF, 0xE0));
	nAddress_Spd = stMMI.m_vecMotorInfo[m_nCurMotorNum].m_stMotorItem[8].m_nAddress_Spd;
	fSpeed = (float)CDataManager::Instance()->m_PLCReaderD_Motor.GetValue2(nAddress_Spd)/10;
	nAddress_Pos = stMMI.m_vecMotorInfo[m_nCurMotorNum].m_stMotorItem[8].m_nAddress_Pos;
	fPos = (float)CDataManager::Instance()->m_PLCReaderD_Motor.GetValue2(nAddress_Pos)/1000.0f;
	CMotorManualSetting::Instance()->SetMotorItemValue(m_nCurMotorNum, 8, fPos, fSpeed);
	// MOTOR ITEM #10
	if(CDataManager::Instance()->m_PLCReaderL_Motor.IsON(3010 + m_nCurMotor_StartAddressL))
		m_GC_1.SetItemBkColour(10,1,CR_LIGHTGREEN);
	else
		m_GC_1.SetItemBkColour(10,1,RGB(0xFF, 0xFF, 0xE0));
	nAddress_Spd = stMMI.m_vecMotorInfo[m_nCurMotorNum].m_stMotorItem[9].m_nAddress_Spd;
	fSpeed = (float)CDataManager::Instance()->m_PLCReaderD_Motor.GetValue2(nAddress_Spd)/10;
	nAddress_Pos = stMMI.m_vecMotorInfo[m_nCurMotorNum].m_stMotorItem[9].m_nAddress_Pos;
	fPos = (float)CDataManager::Instance()->m_PLCReaderD_Motor.GetValue2(nAddress_Pos)/1000.0f;
	CMotorManualSetting::Instance()->SetMotorItemValue(m_nCurMotorNum, 9, fPos, fSpeed);

	//////////////////////////////////////////////////////////////////////////////////////////////
	// MOTOR ITEM #11
	nAddress_Spd = stMMI.m_vecMotorInfo[m_nCurMotorNum].m_stMotorItem[10].m_nAddress_Spd;
	fSpeed = (float)CDataManager::Instance()->m_PLCReaderD_Motor.GetValue2(nAddress_Spd)/10;
	nAddress_Pos = stMMI.m_vecMotorInfo[m_nCurMotorNum].m_stMotorItem[10].m_nAddress_Pos;
	fPos = (float)CDataManager::Instance()->m_PLCReaderD_Motor.GetValue2(nAddress_Pos)/1000.0f;
	CMotorManualSetting::Instance()->SetMotorItemValue(m_nCurMotorNum, 10, fPos, fSpeed);
	// MOTOR ITEM #12
	nAddress_Spd = stMMI.m_vecMotorInfo[m_nCurMotorNum].m_stMotorItem[11].m_nAddress_Spd;
	fSpeed = (float)CDataManager::Instance()->m_PLCReaderD_Motor.GetValue2(nAddress_Spd)/10;
	nAddress_Pos = stMMI.m_vecMotorInfo[m_nCurMotorNum].m_stMotorItem[11].m_nAddress_Pos;
	fPos = (float)CDataManager::Instance()->m_PLCReaderD_Motor.GetValue2(nAddress_Pos)/1000.0f;
	CMotorManualSetting::Instance()->SetMotorItemValue(m_nCurMotorNum, 11, fPos, fSpeed);
	// MOTOR ITEM #13
	nAddress_Spd = stMMI.m_vecMotorInfo[m_nCurMotorNum].m_stMotorItem[12].m_nAddress_Spd;
	fSpeed = (float)CDataManager::Instance()->m_PLCReaderD_Motor.GetValue2(nAddress_Spd)/10;
	nAddress_Pos = stMMI.m_vecMotorInfo[m_nCurMotorNum].m_stMotorItem[12].m_nAddress_Pos;
	fPos = (float)CDataManager::Instance()->m_PLCReaderD_Motor.GetValue2(nAddress_Pos)/1000.0f;
	CMotorManualSetting::Instance()->SetMotorItemValue(m_nCurMotorNum, 12, fPos, fSpeed);
	// MOTOR ITEM #14
	nAddress_Spd = stMMI.m_vecMotorInfo[m_nCurMotorNum].m_stMotorItem[13].m_nAddress_Spd;
	fSpeed = (float)CDataManager::Instance()->m_PLCReaderD_Motor.GetValue2(nAddress_Spd)/10;
	nAddress_Pos = stMMI.m_vecMotorInfo[m_nCurMotorNum].m_stMotorItem[13].m_nAddress_Pos;
	fPos = (float)CDataManager::Instance()->m_PLCReaderD_Motor.GetValue2(nAddress_Pos)/1000.0f;
	CMotorManualSetting::Instance()->SetMotorItemValue(m_nCurMotorNum, 13, fPos, fSpeed);
	// MOTOR ITEM #15
	nAddress_Spd = stMMI.m_vecMotorInfo[m_nCurMotorNum].m_stMotorItem[14].m_nAddress_Spd;
	fSpeed = (float)CDataManager::Instance()->m_PLCReaderD_Motor.GetValue2(nAddress_Spd)/10;
	nAddress_Pos = stMMI.m_vecMotorInfo[m_nCurMotorNum].m_stMotorItem[14].m_nAddress_Pos;
	fPos = (float)CDataManager::Instance()->m_PLCReaderD_Motor.GetValue2(nAddress_Pos)/1000.0f;
	CMotorManualSetting::Instance()->SetMotorItemValue(m_nCurMotorNum, 14, fPos, fSpeed);
	// MOTOR ITEM #16
	nAddress_Spd = stMMI.m_vecMotorInfo[m_nCurMotorNum].m_stMotorItem[15].m_nAddress_Spd;
	fSpeed = (float)CDataManager::Instance()->m_PLCReaderD_Motor.GetValue2(nAddress_Spd)/10;
	nAddress_Pos = stMMI.m_vecMotorInfo[m_nCurMotorNum].m_stMotorItem[15].m_nAddress_Pos;
	fPos = (float)CDataManager::Instance()->m_PLCReaderD_Motor.GetValue2(nAddress_Pos)/1000.0f;
	CMotorManualSetting::Instance()->SetMotorItemValue(m_nCurMotorNum, 15, fPos, fSpeed);
	// MOTOR ITEM #17
	nAddress_Spd = stMMI.m_vecMotorInfo[m_nCurMotorNum].m_stMotorItem[16].m_nAddress_Spd;
	fSpeed = (float)CDataManager::Instance()->m_PLCReaderD_Motor.GetValue2(nAddress_Spd)/10;
	nAddress_Pos = stMMI.m_vecMotorInfo[m_nCurMotorNum].m_stMotorItem[16].m_nAddress_Pos;
	fPos = (float)CDataManager::Instance()->m_PLCReaderD_Motor.GetValue2(nAddress_Pos)/1000.0f;
	CMotorManualSetting::Instance()->SetMotorItemValue(m_nCurMotorNum, 16, fPos, fSpeed);
	// MOTOR ITEM #18
	nAddress_Spd = stMMI.m_vecMotorInfo[m_nCurMotorNum].m_stMotorItem[17].m_nAddress_Spd;
	fSpeed = (float)CDataManager::Instance()->m_PLCReaderD_Motor.GetValue2(nAddress_Spd)/10;
	nAddress_Pos = stMMI.m_vecMotorInfo[m_nCurMotorNum].m_stMotorItem[17].m_nAddress_Pos;
	fPos = (float)CDataManager::Instance()->m_PLCReaderD_Motor.GetValue2(nAddress_Pos)/1000.0f;
	CMotorManualSetting::Instance()->SetMotorItemValue(m_nCurMotorNum, 17, fPos, fSpeed);
	// MOTOR ITEM #19
	nAddress_Spd = stMMI.m_vecMotorInfo[m_nCurMotorNum].m_stMotorItem[18].m_nAddress_Spd;
	fSpeed = (float)CDataManager::Instance()->m_PLCReaderD_Motor.GetValue2(nAddress_Spd)/10;
	nAddress_Pos = stMMI.m_vecMotorInfo[m_nCurMotorNum].m_stMotorItem[18].m_nAddress_Pos;
	fPos = (float)CDataManager::Instance()->m_PLCReaderD_Motor.GetValue2(nAddress_Pos)/1000.0f;
	CMotorManualSetting::Instance()->SetMotorItemValue(m_nCurMotorNum, 18, fPos, fSpeed);
	// MOTOR ITEM #20
	nAddress_Spd = stMMI.m_vecMotorInfo[m_nCurMotorNum].m_stMotorItem[19].m_nAddress_Spd;
	fSpeed = (float)CDataManager::Instance()->m_PLCReaderD_Motor.GetValue2(nAddress_Spd)/10;
	nAddress_Pos = stMMI.m_vecMotorInfo[m_nCurMotorNum].m_stMotorItem[19].m_nAddress_Pos;
	fPos = (float)CDataManager::Instance()->m_PLCReaderD_Motor.GetValue2(nAddress_Pos)/1000.0f;
	CMotorManualSetting::Instance()->SetMotorItemValue(m_nCurMotorNum, 19, fPos, fSpeed);

	SetMotorSettingValue(m_nCurMotorNum);
}

void CDlg_Motor::Update_UI()
{
	if(CAppSetting::Instance()->GetGroupNumber() == 1) 
	{
		if(m_nCurMotorNum >= 0 && m_nCurMotorNum <= 3) // Index X,Y,Z,T
		{
			m_btn_18.ShowWindow(SW_SHOW);
		}
		else m_btn_18.ShowWindow(SW_HIDE);
	}
	else m_btn_18.ShowWindow(SW_HIDE);
}

void CDlg_Motor::OnBnClickedBtnMt1()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	stMotorManual_Info stMMI = CMotorManualSetting::Instance()->GetMotorManualInfo();

	if(stMMI.m_nMotorCount <= 0) m_nCurMotorNum = 0;
	m_nCurMotorNum --;
	if(m_nCurMotorNum < 0) m_nCurMotorNum = 0;
	if(m_nCurMotorNum > stMMI.m_nMotorCount-1) m_nCurMotorNum = stMMI.m_nMotorCount-1;

	Update_UI();

	SetMotorSettingValue(m_nCurMotorNum);

	Update_SelItemIndex(m_nCurSelMotorItemIndex);
}


void CDlg_Motor::OnBnClickedBtnMt2()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	stMotorManual_Info stMMI = CMotorManualSetting::Instance()->GetMotorManualInfo();

	if(stMMI.m_nMotorCount <= 0) return;
	m_nCurMotorNum ++;
	if(m_nCurMotorNum > stMMI.m_nMotorCount-1) m_nCurMotorNum = stMMI.m_nMotorCount-1;

	Update_UI();

	SetMotorSettingValue(m_nCurMotorNum);

	Update_SelItemIndex(m_nCurSelMotorItemIndex);
}

// MOTOR ITEM 선택된거 MOVE 버튼 처리
void CDlg_Motor::OnBnClickedBtnMt3()
{
}


void CDlg_Motor::OnBnClickedBtnMt4()
{
}


void CDlg_Motor::OnBnClickedBtnMt5()
{
}


void CDlg_Motor::OnBnClickedBtnMt6()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CDlg_MotorList dlg;
	dlg.m_strCaption = _T("MOTOR LIST DIALOG");
	if(dlg.DoModal() == IDOK)
	{
		if(dlg.m_nSelMotorIndex != -1)
		{
			m_nCurMotorNum = dlg.m_nSelMotorIndex;

			Update_UI();

			SetMotorSettingValue(m_nCurMotorNum);

			m_nCurSelMotorItemIndex = -1;
			Update_SelItemIndex(m_nCurSelMotorItemIndex);
		}
	}
}


void CDlg_Motor::OnBnClickedBtnMt7()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	int nPLCNum = CAppSetting::Instance()->GetGroupNumber();
	int nCalAddress = m_nCurMotor_StartAddressD + 18;
	SHORT sValue = 1;
	CDataManager::Instance()->PLC_Write_D(nPLCNum, nCalAddress, (SHORT)sValue*10);
}


void CDlg_Motor::OnBnClickedBtnMt8()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	int nPLCNum = CAppSetting::Instance()->GetGroupNumber();
	int nCalAddress = m_nCurMotor_StartAddressD + 18;
	SHORT sValue = 10;
	CDataManager::Instance()->PLC_Write_D(nPLCNum, nCalAddress, (SHORT)sValue*10);
}


void CDlg_Motor::OnBnClickedBtnMt9()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	int nPLCNum = CAppSetting::Instance()->GetGroupNumber();
	int nCalAddress = m_nCurMotor_StartAddressD + 18;
	SHORT sValue = 100;
	CDataManager::Instance()->PLC_Write_D(nPLCNum, nCalAddress, (SHORT)sValue*10);
}


void CDlg_Motor::OnBnClickedBtnMt10()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	int nPLCNum = CAppSetting::Instance()->GetGroupNumber();
	int nCalAddress = m_nCurMotor_StartAddressD + 18;
	SHORT sValue = 1000;
	CDataManager::Instance()->PLC_Write_D(nPLCNum, nCalAddress, (SHORT)sValue*10);
}


void CDlg_Motor::OnBnClickedBtnMt11()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CDlg_NumPad dlg;
	dlg.m_strCaption = _T("Change JOG Value");
	if(dlg.DoModal() == IDOK)
	{
		int nPLCNum = CAppSetting::Instance()->GetGroupNumber();
		int nCalAddress = m_nCurMotor_StartAddressD + 18;
		SHORT sValue = _wtoi(dlg.m_strValue)*10;
		CDataManager::Instance()->PLC_Write_D(nPLCNum, nCalAddress, (SHORT)sValue);
	}
}


void CDlg_Motor::OnBnClickedBtnMt12()
{
}


void CDlg_Motor::OnBnClickedBtnMt13()
{
}


void CDlg_Motor::OnBnClickedBtnMt14()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CDlg_NumPad dlg;
	dlg.m_strCaption = _T("Change Accel Value");
	if(dlg.DoModal() == IDOK)
	{
		int nPLCNum = CAppSetting::Instance()->GetGroupNumber();
		int nCalAddress = m_nCurMotor_StartAddressD + 12;
		SHORT sValue = _wtoi(dlg.m_strValue);
		CDataManager::Instance()->PLC_Write_D(nPLCNum, nCalAddress, (SHORT)sValue);
	}
}


void CDlg_Motor::OnBnClickedBtnMt15()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CDlg_NumPad dlg;
	dlg.m_strCaption = _T("Change Decel Value");
	if(dlg.DoModal() == IDOK)
	{
		int nPLCNum = CAppSetting::Instance()->GetGroupNumber();
		int nCalAddress = m_nCurMotor_StartAddressD + 14;
		SHORT sValue = _wtoi(dlg.m_strValue);
		CDataManager::Instance()->PLC_Write_D(nPLCNum, nCalAddress, (SHORT)sValue);
	}
}


void CDlg_Motor::OnBnClickedBtnMt16()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_btn_16.EnableWindow(FALSE);
	CMotorManualSetting::Instance()->Save_MotorSetting();
	m_btn_16.EnableWindow(TRUE);
}


void CDlg_Motor::OnBnClickedBtnMt17()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CAMKOR_MMIDlg *)AfxGetMainWnd())->SendMessage(WM_CHANGESCREEN, 0, 0);
}

void CDlg_Motor::OnBnClickedBtnMt18()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CAMKOR_MMIDlg *)AfxGetMainWnd())->SendMessage(WM_CHANGESCREEN, 100, 0);
}

BOOL CDlg_Motor::OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	NMHDR *pNMHDR = (NMHDR*)lParam;	

	int nPLCNum = CAppSetting::Instance()->GetGroupNumber();
	if( pNMHDR->code == TCN_SELCHANGE ) // Button Pressed
	{
		if(wParam == IDC_BTN_MT_3) // MOVE
		{
			if(m_nCurSelMotorItemIndex >= 0)
			{
				int nMotorStartLAddressBit = 1001 + m_nCurMotor_StartAddressL;
				int nCalAddress = nMotorStartLAddressBit + m_nCurSelMotorItemIndex;

				CDataManager::Instance()->PLC_Write_L(nPLCNum, nCalAddress, TRUE);
			}
		}
		if(wParam == IDC_BTN_MT_4) // HOME
		{
			int nStartMAddressBit = 40 + m_nCurMotor_StartAddressM;
			CDataManager::Instance()->PLC_Write_M(nPLCNum, nStartMAddressBit, TRUE);
		}
		if(wParam == IDC_BTN_MT_5) // SERVO ON/OFF
		{
			int nStartMAddressBit = 43 + m_nCurMotor_StartAddressM;
			if(CDataManager::Instance()->PLC_Read_M2(nPLCNum, nStartMAddressBit))
				CDataManager::Instance()->PLC_Write_M(nPLCNum, nStartMAddressBit, FALSE);
			else
				CDataManager::Instance()->PLC_Write_M(nPLCNum, nStartMAddressBit, TRUE);
		}
		if(wParam == IDC_BTN_MT_12) // << 
		{
			int nStartMAddressBit = 41 + m_nCurMotor_StartAddressM;
			CDataManager::Instance()->PLC_Write_M(nPLCNum, nStartMAddressBit, TRUE);
		}
		if(wParam == IDC_BTN_MT_13) // >>
		{
			int nStartMAddressBit = 42 + m_nCurMotor_StartAddressM;
			CDataManager::Instance()->PLC_Write_M(nPLCNum, nStartMAddressBit, TRUE);
		}
	}
	if( pNMHDR->code == TCN_SELCHANGING ) // Button Released
	{
		if(wParam == IDC_BTN_MT_3) // MOVE
		{
			if(m_nCurSelMotorItemIndex >= 0)
			{
				int nMotorStartLAddressBit = 1001 + m_nCurMotor_StartAddressL;
				int nCalAddress = nMotorStartLAddressBit + m_nCurSelMotorItemIndex;

				CDataManager::Instance()->PLC_Write_L(nPLCNum, nCalAddress, FALSE);
			}
		}
		if(wParam == IDC_BTN_MT_4) // HOME
		{
			int nStartMAddressBit = 40 + m_nCurMotor_StartAddressM;
			CDataManager::Instance()->PLC_Write_M(nPLCNum, nStartMAddressBit, FALSE);
		}
		if(wParam == IDC_BTN_MT_5) // SERVO ON/OFF
		{
			//int nStartMAddressBit = 43 + m_nCurMotor_StartAddressM;
			//CDataManager::Instance()->PLC_Write_M(nPLCNum, nStartMAddressBit, FALSE);
		}
		if(wParam == IDC_BTN_MT_12) // << 
		{
			int nStartMAddressBit = 41 + m_nCurMotor_StartAddressM;
			CDataManager::Instance()->PLC_Write_M(nPLCNum, nStartMAddressBit, FALSE);
		}
		if(wParam == IDC_BTN_MT_13) // >>
		{
			int nStartMAddressBit = 42 + m_nCurMotor_StartAddressM;
			CDataManager::Instance()->PLC_Write_M(nPLCNum, nStartMAddressBit, FALSE);
		}
	}

	return CDialogEx::OnNotify(wParam, lParam, pResult);
}
