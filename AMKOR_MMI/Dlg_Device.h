#pragma once
#include "afxwin.h"
#include "UIExt/CxStatic.h"
#include "UIExt/IconButton.h"
#include "UIExt/XPGroupBox.h"
#include "UIExt\/BorderStyleEdit.h"
#include "GC/gridctrl.h"

// CDlg_Device 대화 상자입니다.

class CDlg_Device : public CDialogEx
{
	DECLARE_DYNAMIC(CDlg_Device)

public:
	CDlg_Device(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CDlg_Device();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DLG_DEVICE };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);

	void Cal_Area();
	void Cal_CtrlArea();
	void Cal_CtrlArea_Left();
	void Cal_CtrlArea_Right();

	bool Init_GC_1(int nRows, int nCols);
	afx_msg void OnGridClick_GC_1(NMHDR *pNotifyStruct, LRESULT* pResult);

	void SetDeviceListSettingValue();
	void Update_SelDeviceIndex(int nDeviceIndex);
	void Update_PLC_Value();

	CRect m_rcLeft;
	CRect m_rcRight;

	BOOL m_bCtrl_FirstLoad;

	int m_nSelDeviceIndex;
	int m_nCurPageNum;

	CXPGroupBox m_gb_1;
	CGridCtrl m_GC_1;
	CIconButton m_btn_1;
	CIconButton m_btn_2;
	CIconButton m_btn_3;
	CIconButton m_btn_4;
	CIconButton m_btn_5;
	CIconButton m_btn_6;

	CXPGroupBox m_gb_2;
	CxStatic m_st_1;
	CxStatic m_st_2;
	CxStatic m_st_3;
	CxStatic m_st_4;
	CxStatic m_st_5;
	CxStatic m_st_6;
	CxStatic m_st_7;
	CxStatic m_st_8;
	CBorderStyleEdit m_edit_1;
	CBorderStyleEdit m_edit_2;
	CBorderStyleEdit m_edit_3;
	CBorderStyleEdit m_edit_4;
	CBorderStyleEdit m_edit_5;
	CIconButton m_btn_7;
	CIconButton m_btn_8;
	CIconButton m_btn_9;
	CIconButton m_btn_V1;
	CIconButton m_btn_V2;
	CIconButton m_btn_V3;
	CIconButton m_btn_V4;
	CIconButton m_btn_V5;
	CIconButton m_btn_V6;
	CIconButton m_btn_V7;
	CIconButton m_btn_V8;
	CIconButton m_btn_V9;
	CIconButton m_btn_V10;
	CIconButton m_btn_V11;
	CIconButton m_btn_V12;
	CIconButton m_btn_V13;
	CIconButton m_btn_V14;
	CIconButton m_btn_V15;

	afx_msg void OnBnClickedBtnDv1();
	afx_msg void OnBnClickedBtnDv2();
	afx_msg void OnBnClickedBtnDv3();
	afx_msg void OnBnClickedBtnDv4();
	afx_msg void OnBnClickedBtnDv5();
	afx_msg void OnBnClickedBtnDv6();
	afx_msg void OnBnClickedBtnDv7();
	afx_msg void OnBnClickedBtnDv8();
	afx_msg void OnBnClickedBtnDv9();
	afx_msg void OnBnClickedBtnDvV1();
	afx_msg void OnBnClickedBtnDvV2();
	afx_msg void OnBnClickedBtnDvV3();
	afx_msg void OnBnClickedBtnDvV4();
	afx_msg void OnBnClickedBtnDvV5();
	afx_msg void OnBnClickedBtnDvV6();
	afx_msg void OnBnClickedBtnDvV7();
	afx_msg void OnBnClickedBtnDvV8();
	afx_msg void OnBnClickedBtnDvV9();
	afx_msg void OnBnClickedBtnDvV10();
	afx_msg void OnBnClickedBtnDvV11();
	afx_msg void OnBnClickedBtnDvV12();
	afx_msg void OnBnClickedBtnDvV13();
	afx_msg void OnBnClickedBtnDvV14();
	afx_msg void OnBnClickedBtnDvV15();
};
