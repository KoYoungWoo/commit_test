
// stdafx.h : 자주 사용하지만 자주 변경되지는 않는
// 표준 시스템 포함 파일 및 프로젝트 관련 포함 파일이 
// 들어 있는 포함 파일입니다.

#pragma once

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN            // 거의 사용되지 않는 내용은 Windows 헤더에서 제외합니다.
#endif

#include "targetver.h"

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS      // 일부 CString 생성자는 명시적으로 선언됩니다.

// MFC의 공통 부분과 무시 가능한 경고 메시지에 대한 숨기기를 해제합니다.
#define _AFX_ALL_WARNINGS

#include <afxwin.h>         // MFC 핵심 및 표준 구성 요소입니다.
#include <afxext.h>         // MFC 확장입니다.


#include <afxdisp.h>        // MFC 자동화 클래스입니다.



#ifndef _AFX_NO_OLE_SUPPORT
#include <afxdtctl.h>           // Internet Explorer 4 공용 컨트롤에 대한 MFC 지원입니다.
#endif
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>             // Windows 공용 컨트롤에 대한 MFC 지원입니다.
#endif // _AFX_NO_AFXCMN_SUPPORT

#include <afxcontrolbars.h>     // MFC의 리본 및 컨트롤 막대 지원

#include <atlcoll.h>
#include <vector>
#include <algorithm>
#include <tchar.h>
#include <map>
#include <hash_map>

#include <afxsock.h>            // MFC 소켓 확장. 

// ADO 클래스를 사용하기 위한 선언
#pragma warning(push)
#pragma warning(disable:4146)
#import "c:\program files\common files\system\ado\msado15.dll" rename ("EOF","adoEOF") no_namespace
#pragma warning(pop) 

// User Include
#include "ADO/ADOClass.h"
#include "AppSetting.h"
#include "DeviceTraySetting.h"
#include "MotorManualSetting.h"
#include "IOListSetting.h"
#include "AlarmListSetting.h"
#include "ColorDefine.h"
#include "PLC_define.h"
#include "LOG/Log.h"
#include "LOG/LogList.h"
#include "DataManager.h"
#include "UIExt/UserFont.h"
#include "Global.h"
#include "Define.h"

// User Define
#define CTRL_MARGIN		2
#define CTRL_MARGIN2	CTRL_MARGIN*2
#define CTRL_MARGIN3	CTRL_MARGIN*3
#define CTRL_MARGIN4	CTRL_MARGIN*4
#define CTRL_FONT		_T("맑은고딕")

//////////////////////////////////////////////////////////////////////
// User MSG
#define WM_CHANGESCREEN					(WM_USER+1000)
#define WM_UPDATESCREEN					(WM_USER+1001)
#define WM_UPDATE_UI_CONN				(WM_USER+1002)
#define WM_ALL_DATA_CLEAR				(WM_USER+1003)
#define WM_UPDATE_LOTLIST				(WM_USER+1004)
#define WM_ZEBRA_AUTOSEND				(WM_USER+1005)
#define WM_CLEAR_ZEBRA_AUTOSEND			(WM_USER+1006)
#define WM_RESIZE_MAT_ALARM_DLG			(WM_USER+1007)
#define WM_RESIZE_EQUIPSTATUS_DLG		(WM_USER+1008)

#ifdef _UNICODE
#if defined _M_IX86
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='x86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_X64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='amd64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#else
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif
#endif


