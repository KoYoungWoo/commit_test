
// AMKOR_MMIDlg.cpp : 구현 파일
//

#include "stdafx.h"
#include "AMKOR_MMI.h"
#include "AMKOR_MMIDlg.h"
#include "Dlg_NumPad.h"
#include "Dlg_Auth.h"
#include "Dlg_TrayStacker.h"
#include "afxdialogex.h"
#include "ClientSocket.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


#define	TM_CLOCK			100
#define TM_UPDATE_MOTOR		200
#define TM_UPDATE_MANUAL	300
#define TM_UPDATE_DEVICE	400
#define TM_UPDATE_IO		500
#define TM_UPDATE_ALARM		600

// CAMKOR_MMIDlg 대화 상자



CAMKOR_MMIDlg::CAMKOR_MMIDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CAMKOR_MMIDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);

	m_bCtrl_FirstLoad = FALSE;

	m_nCurScreen = 0;
	m_nCurSubScreen = 0;

	// 년월일시 저장 - 파일 제거 날짜 파악
	m_nYear = 0;
	m_nMonth = 0;
	m_nDay = 0;
	m_nHour = 0;

	// 권한 번호
	m_nAuthNumber = 2;

	// CCTV Trigger 관련
	for(int i=0;i<7;i++)
	{
		ZeroMemory(&m_stSendCamTrigger[i],sizeof(stSendCamTrigger));
	}

	// MES Alarm Check
	for(int i=0;i<8;i++)
	{
		m_sAlarmCode_G1[i] = 0;
		m_sAlarmCode_G2[i] = 0;
		m_sAlarmCode_G3[i] = 0;
	}
}

void CAMKOR_MMIDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_ST_M_DATE, m_st_Date);
	DDX_Control(pDX, IDC_ST_M_TIME, m_st_Time);
	DDX_Control(pDX, IDC_ST_M_ERROR_COLOR, m_st_ErrorColor);
	DDX_Control(pDX, IDC_BTN_ERRORCODE, m_btn_ErrorCode);
	DDX_Control(pDX, IDC_BTN_ERRORTEXT, m_btn_ErrorText);
	DDX_Control(pDX, IDC_BTN_M_EXIT, m_btn_Exit);
	DDX_Control(pDX, IDC_BTN_M_1, m_btn_1);
	DDX_Control(pDX, IDC_BTN_M_2, m_btn_2);
	DDX_Control(pDX, IDC_BTN_M_3, m_btn_3);
	DDX_Control(pDX, IDC_BTN_M_4, m_btn_4);
	DDX_Control(pDX, IDC_BTN_M_5, m_btn_5);
	DDX_Control(pDX, IDC_BTN_M_DEVICE, m_btn_DeviceName);
	DDX_Control(pDX, IDC_GB_1, m_gb_1);
	DDX_Control(pDX, IDC_GB_2, m_gb_2);
	DDX_Control(pDX, IDC_ST_M_1, m_st_T1);
	DDX_Control(pDX, IDC_ST_M_2, m_st_T2);
	DDX_Control(pDX, IDC_ST_M_3, m_st_T3);
	DDX_Control(pDX, IDC_ST_M_4, m_st_T4);
	DDX_Control(pDX, IDC_ST_M_V1, m_st_V1);
	DDX_Control(pDX, IDC_ST_M_V2, m_st_V2);
	DDX_Control(pDX, IDC_ST_M_V3, m_st_V3);
	DDX_Control(pDX, IDC_ST_M_V4, m_st_V4);
	DDX_Control(pDX, IDC_BTN_M_START, m_btn_Start);
	DDX_Control(pDX, IDC_BTN_M_STOP, m_btn_Stop);
	DDX_Control(pDX, IDC_BTN_M_RESET, m_btn_Reset);
	DDX_Control(pDX, IDC_BTN_M_SYSINFO, m_btn_SystemInfo);
	DDX_Control(pDX, IDC_BTN_M_MODEL, m_btn_ModelName);
	DDX_Control(pDX, IDC_BTN_M_AUTH, m_btn_Auth);
	DDX_Control(pDX, IDC_BTN_M_GEM, m_btn_Gem);
	DDX_Control(pDX, IDC_BTN_M_BUZZEROFF, m_btn_BuzOff);
	DDX_Control(pDX, IDC_BTN_M_HOME, m_btn_Home);
	DDX_Control(pDX, IDC_BTN_M_RESETCOUNT, m_btn_ResetCount);
	DDX_Control(pDX, IDC_BTN_M_6, m_btn_6);
	DDX_Control(pDX, IDC_BTN_M_AUTOMODE, m_btn_AutoMode);
	DDX_Control(pDX, IDC_BTN_M_MANUALMODE, m_btn_ManualMode);
	DDX_Control(pDX, IDC_BTN_M_CONN_1, m_btn_Conn_1);
	DDX_Control(pDX, IDC_BTN_M_CONN_2, m_btn_Conn_2);
	DDX_Control(pDX, IDC_BTN_M_CONN_3, m_btn_Conn_3);
	DDX_Control(pDX, IDC_BTN_M_CONN_4, m_btn_Conn_4);
	DDX_Control(pDX, IDC_BTN_M_CONN_5, m_btn_Conn_5);
	DDX_Control(pDX, IDC_BTN_M_CONN_6, m_btn_Conn_6);
	DDX_Control(pDX, IDC_BTN_M_CONN_7, m_btn_Conn_7);
}

BEGIN_MESSAGE_MAP(CAMKOR_MMIDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_DESTROY()
	ON_WM_ERASEBKGND()
	ON_WM_SIZE()
	ON_WM_TIMER()
	ON_MESSAGE(WM_UPDATE_UI_CONN, &CAMKOR_MMIDlg::OnConnUpdate)
	ON_BN_CLICKED(IDC_BTN_M_1, &CAMKOR_MMIDlg::OnBnClickedBtnM1)
	ON_BN_CLICKED(IDC_BTN_M_2, &CAMKOR_MMIDlg::OnBnClickedBtnM2)
	ON_BN_CLICKED(IDC_BTN_M_3, &CAMKOR_MMIDlg::OnBnClickedBtnM3)
	ON_BN_CLICKED(IDC_BTN_M_4, &CAMKOR_MMIDlg::OnBnClickedBtnM4)
	ON_BN_CLICKED(IDC_BTN_M_5, &CAMKOR_MMIDlg::OnBnClickedBtnM5)
	ON_MESSAGE(WM_CHANGESCREEN, &CAMKOR_MMIDlg::OnScreenChange)
	ON_BN_CLICKED(IDC_BTN_M_EXIT, &CAMKOR_MMIDlg::OnBnClickedBtnMExit)
	ON_BN_CLICKED(IDC_BTN_M_AUTH, &CAMKOR_MMIDlg::OnBnClickedBtnMAuth)
	ON_BN_CLICKED(IDC_BTN_M_SYSINFO, &CAMKOR_MMIDlg::OnBnClickedBtnMSysinfo)
	ON_BN_CLICKED(IDC_BTN_M_GEM, &CAMKOR_MMIDlg::OnBnClickedBtnMGem)
	ON_BN_CLICKED(IDC_BTN_M_BUZZEROFF, &CAMKOR_MMIDlg::OnBnClickedBtnMBuzzeroff)
	ON_BN_CLICKED(IDC_BTN_M_HOME, &CAMKOR_MMIDlg::OnBnClickedBtnMHome)
	ON_BN_CLICKED(IDC_BTN_M_RESETCOUNT, &CAMKOR_MMIDlg::OnBnClickedBtnMResetcount)
	ON_BN_CLICKED(IDC_BTN_M_6, &CAMKOR_MMIDlg::OnBnClickedBtnM6)
	ON_BN_CLICKED(IDC_BTN_M_AUTOMODE, &CAMKOR_MMIDlg::OnBnClickedBtnMAutomode)
	ON_BN_CLICKED(IDC_BTN_M_MANUALMODE, &CAMKOR_MMIDlg::OnBnClickedBtnMManualmode)
	ON_BN_CLICKED(IDC_BTN_M_CONN_1, &CAMKOR_MMIDlg::OnBnClickedBtnMConn1)
	ON_BN_CLICKED(IDC_BTN_M_CONN_2, &CAMKOR_MMIDlg::OnBnClickedBtnMConn2)
	ON_BN_CLICKED(IDC_BTN_M_CONN_3, &CAMKOR_MMIDlg::OnBnClickedBtnMConn3)
	ON_BN_CLICKED(IDC_BTN_ERRORCODE, &CAMKOR_MMIDlg::OnBnClickedBtnErrorcode)
	ON_BN_CLICKED(IDC_BTN_ERRORTEXT, &CAMKOR_MMIDlg::OnBnClickedBtnErrortext)
	ON_WM_CLOSE()
END_MESSAGE_MAP()


// CAMKOR_MMIDlg 메시지 처리기

BOOL CAMKOR_MMIDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 이 대화 상자의 아이콘을 설정합니다. 응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

#ifdef _DEBUG
	this->MoveWindow(0, 0, 1280, 1024);
#else
	this->MoveWindow(0, 0, ::GetSystemMetrics(SM_CXSCREEN), ::GetSystemMetrics(SM_CYSCREEN));       ////전체화면 
#endif

	// TODO: 여기에 추가 초기화 작업을 추가합니다.
	Cal_CtrlArea();
	// ADO DB Setting
	SetADOSetting();
	// Listen Socket Server Start
	SocketServerStart();
	// Connect Socket Setting
	m_ZPLPrinterSocket[0].SetConnectInfo(CAppSetting::Instance()->GetIP_ZPL_Printer(0).GetIPAddressString(),CAppSetting::Instance()->GetPort_ZPL_Printer(0));	// Tray
	m_ZPLPrinterSocket[1].SetConnectInfo(CAppSetting::Instance()->GetIP_ZPL_Printer(1).GetIPAddressString(),CAppSetting::Instance()->GetPort_ZPL_Printer(1));	// MBB dn
	m_ZPLPrinterSocket[2].SetConnectInfo(CAppSetting::Instance()->GetIP_ZPL_Printer(2).GetIPAddressString(),CAppSetting::Instance()->GetPort_ZPL_Printer(2));	// MBB up
	m_ZPLPrinterSocket[3].SetConnectInfo(CAppSetting::Instance()->GetIP_ZPL_Printer(3).GetIPAddressString(),CAppSetting::Instance()->GetPort_ZPL_Printer(3));	// BOX right
	m_ZPLPrinterSocket[4].SetConnectInfo(CAppSetting::Instance()->GetIP_ZPL_Printer(4).GetIPAddressString(),CAppSetting::Instance()->GetPort_ZPL_Printer(4));	// BOX left

	// PLC CONNECT LOGICAL NUMBER
	if(CDataManager::Instance()->OpenDevice_PLC_1(CAppSetting::Instance()->GetLogicalStationNumber(1)))
	{
		CDataManager::Instance()->SetConnect(CONN_PLC_1, TRUE);

		// LOG 남기기
		GetLog()->Debug( _T("G1 PLC CONNECT OK.....") );
		CLogList::Instance()->Add_LogData(eLog_DEBUG, _T("G1 PLC CONNECT OK.....") );
	}
	else
	{
		CDataManager::Instance()->SetConnect(CONN_PLC_1, FALSE);

		// LOG 남기기
		GetLog()->Debug( _T("G1 PLC CONNECT FAIL.....") );
		CLogList::Instance()->Add_LogData(eLog_DEBUG, _T("G1 PLC CONNECT FAIL.....") );
	}

	if(CDataManager::Instance()->OpenDevice_PLC_2(CAppSetting::Instance()->GetLogicalStationNumber(2)))
	{
		CDataManager::Instance()->SetConnect(CONN_PLC_2, TRUE);

		// LOG 남기기
		GetLog()->Debug( _T("G2 PLC CONNECT OK.....") );
		CLogList::Instance()->Add_LogData(eLog_DEBUG, _T("G2 PLC CONNECT OK.....") );
	}
	else
	{
		CDataManager::Instance()->SetConnect(CONN_PLC_2, FALSE);

		// LOG 남기기
		GetLog()->Debug( _T("G2 PLC CONNECT FAIL.....") );
		CLogList::Instance()->Add_LogData(eLog_DEBUG, _T("G2 PLC CONNECT FAIL.....") );
	}

	if(CDataManager::Instance()->OpenDevice_PLC_3(CAppSetting::Instance()->GetLogicalStationNumber(3)))
	{
		CDataManager::Instance()->SetConnect(CONN_PLC_3, TRUE);

		// LOG 남기기
		GetLog()->Debug( _T("G3 PLC CONNECT OK.....") );
		CLogList::Instance()->Add_LogData(eLog_DEBUG, _T("G3 PLC CONNECT OK.....") );
	}
	else
	{
		CDataManager::Instance()->SetConnect(CONN_PLC_3, FALSE);

		// LOG 남기기
		GetLog()->Debug( _T("G3 PLC CONNECT FAIL.....") );
		CLogList::Instance()->Add_LogData(eLog_DEBUG, _T("G3 PLC CONNECT FAIL.....") );
	}

	UpdateViewConnStatus();

	if(CAppSetting::Instance()->GetGroupNumber() == 1) 
	{
		CDataManager::Instance()->m_PLCReaderX.SetPLCModule(&CDataManager::Instance()->m_MxComp3_PLC_1);
		CDataManager::Instance()->m_PLCReaderX.SetAddressHex(CIOListSetting::Instance()->m_stIOInfo.m_nStartAddress_Input, CIOListSetting::Instance()->m_stIOInfo.m_nEndAddress_Input);
		CDataManager::Instance()->m_PLCReaderY.SetPLCModule(&CDataManager::Instance()->m_MxComp3_PLC_1);
		CDataManager::Instance()->m_PLCReaderY.SetAddressHex(CIOListSetting::Instance()->m_stIOInfo.m_nStartAddress_Output, CIOListSetting::Instance()->m_stIOInfo.m_nEndAddress_Output);
		CDataManager::Instance()->m_PLCReaderL_Manual.SetPLCModule(&CDataManager::Instance()->m_MxComp3_PLC_1);
		CDataManager::Instance()->m_PLCReaderL_Manual.SetAddress(CMotorManualSetting::Instance()->GetManualStartLAddress(), CMotorManualSetting::Instance()->GetManualEndLAddress());
		CDataManager::Instance()->m_PLCReaderL_Motor.SetPLCModule(&CDataManager::Instance()->m_MxComp3_PLC_1);
		CDataManager::Instance()->m_PLCReaderL_Motor.SetAddress(CMotorManualSetting::Instance()->GetMotorStartLAddress(), CMotorManualSetting::Instance()->GetMotorEndLAddress());
		CDataManager::Instance()->m_PLCReaderD_Motor.SetPLCModule(&CDataManager::Instance()->m_MxComp3_PLC_1);
		CDataManager::Instance()->m_PLCReaderD_Motor.SetAddress(CMotorManualSetting::Instance()->GetMotorStartDAddress(), CMotorManualSetting::Instance()->GetMotorEndDAddress()); 
		CDataManager::Instance()->m_PLCReaderD_Alarm_G1.SetPLCModule(&CDataManager::Instance()->m_MxComp3_PLC_1);
		CDataManager::Instance()->m_PLCReaderD_Alarm_G1.SetAddress(91, 99);
		CDataManager::Instance()->m_PLCReaderD_Alarm_G2.SetPLCModule(&CDataManager::Instance()->m_MxComp3_PLC_2);
		CDataManager::Instance()->m_PLCReaderD_Alarm_G2.SetAddress(91, 99);
		CDataManager::Instance()->m_PLCReaderD_Alarm_G3.SetPLCModule(&CDataManager::Instance()->m_MxComp3_PLC_3);
		CDataManager::Instance()->m_PLCReaderD_Alarm_G3.SetAddress(91, 99);
		// 접속시 최초 한번 가져오고 이후는 필요시 가져오거나 쓰는 작업 - 트레이 및 레시피 정보
		CDataManager::Instance()->m_PLCReaderZR.SetPLCModule(&CDataManager::Instance()->m_MxComp3_PLC_1);
		CDataManager::Instance()->m_PLCReaderZR.SetAddress(20000, 21999);
		if(CDataManager::Instance()->IsConnected(CONN_PLC_1)) CDataManager::Instance()->m_PLCReaderZR.Read();
		// 자재 정보 포지션별
		CDataManager::Instance()->m_PLCReaderR_G1.SetPLCModule(&CDataManager::Instance()->m_MxComp3_PLC_1);
		CDataManager::Instance()->m_PLCReaderR_G1.SetAddress(100, 899);
		CDataManager::Instance()->m_PLCReaderR_G2.SetPLCModule(&CDataManager::Instance()->m_MxComp3_PLC_2);
		CDataManager::Instance()->m_PLCReaderR_G2.SetAddress(100, 899);
		CDataManager::Instance()->m_PLCReaderR_G3.SetPLCModule(&CDataManager::Instance()->m_MxComp3_PLC_3);
		CDataManager::Instance()->m_PLCReaderR_G3.SetAddress(100, 899);
	}
	else if(CAppSetting::Instance()->GetGroupNumber() == 2) 
	{
		CDataManager::Instance()->m_PLCReaderX.SetPLCModule(&CDataManager::Instance()->m_MxComp3_PLC_2);
		CDataManager::Instance()->m_PLCReaderX.SetAddressHex(CIOListSetting::Instance()->m_stIOInfo.m_nStartAddress_Input, CIOListSetting::Instance()->m_stIOInfo.m_nEndAddress_Input);
		CDataManager::Instance()->m_PLCReaderY.SetPLCModule(&CDataManager::Instance()->m_MxComp3_PLC_2);
		CDataManager::Instance()->m_PLCReaderY.SetAddressHex(CIOListSetting::Instance()->m_stIOInfo.m_nStartAddress_Output, CIOListSetting::Instance()->m_stIOInfo.m_nEndAddress_Output);
		CDataManager::Instance()->m_PLCReaderL_Manual.SetPLCModule(&CDataManager::Instance()->m_MxComp3_PLC_2);
		CDataManager::Instance()->m_PLCReaderL_Manual.SetAddress(CMotorManualSetting::Instance()->GetManualStartLAddress(), CMotorManualSetting::Instance()->GetManualEndLAddress());
		CDataManager::Instance()->m_PLCReaderL_Motor.SetPLCModule(&CDataManager::Instance()->m_MxComp3_PLC_2);
		CDataManager::Instance()->m_PLCReaderL_Motor.SetAddress(CMotorManualSetting::Instance()->GetMotorStartLAddress(), CMotorManualSetting::Instance()->GetMotorEndLAddress());
		CDataManager::Instance()->m_PLCReaderD_Motor.SetPLCModule(&CDataManager::Instance()->m_MxComp3_PLC_2);
		CDataManager::Instance()->m_PLCReaderD_Motor.SetAddress(CMotorManualSetting::Instance()->GetMotorStartDAddress(), CMotorManualSetting::Instance()->GetMotorEndDAddress()); 
		CDataManager::Instance()->m_PLCReaderD_Alarm_G2.SetPLCModule(&CDataManager::Instance()->m_MxComp3_PLC_2);
		CDataManager::Instance()->m_PLCReaderD_Alarm_G2.SetAddress(91, 99);
		// 접속시 최초 한번 가져오고 이후는 필요시 가져오거나 쓰는 작업 - 트레이 및 레시피 정보
		CDataManager::Instance()->m_PLCReaderZR.SetPLCModule(&CDataManager::Instance()->m_MxComp3_PLC_1);
		CDataManager::Instance()->m_PLCReaderZR.SetAddress(20000, 21999);
		if(CDataManager::Instance()->IsConnected(CONN_PLC_1)) CDataManager::Instance()->m_PLCReaderZR.Read();
		// 자재 정보 포지션별
		CDataManager::Instance()->m_PLCReaderR_G2.SetPLCModule(&CDataManager::Instance()->m_MxComp3_PLC_2);
		CDataManager::Instance()->m_PLCReaderR_G2.SetAddress(100, 899);
	}
	else if(CAppSetting::Instance()->GetGroupNumber() == 3) 
	{
		CDataManager::Instance()->m_PLCReaderX.SetPLCModule(&CDataManager::Instance()->m_MxComp3_PLC_3);
		CDataManager::Instance()->m_PLCReaderX.SetAddressHex(CIOListSetting::Instance()->m_stIOInfo.m_nStartAddress_Input, CIOListSetting::Instance()->m_stIOInfo.m_nEndAddress_Input);
		CDataManager::Instance()->m_PLCReaderY.SetPLCModule(&CDataManager::Instance()->m_MxComp3_PLC_3);
		CDataManager::Instance()->m_PLCReaderY.SetAddressHex(CIOListSetting::Instance()->m_stIOInfo.m_nStartAddress_Output, CIOListSetting::Instance()->m_stIOInfo.m_nEndAddress_Output);
		CDataManager::Instance()->m_PLCReaderL_Manual.SetPLCModule(&CDataManager::Instance()->m_MxComp3_PLC_3);
		CDataManager::Instance()->m_PLCReaderL_Manual.SetAddress(CMotorManualSetting::Instance()->GetManualStartLAddress(), CMotorManualSetting::Instance()->GetManualEndLAddress());
		CDataManager::Instance()->m_PLCReaderL_Motor.SetPLCModule(&CDataManager::Instance()->m_MxComp3_PLC_3);
		CDataManager::Instance()->m_PLCReaderL_Motor.SetAddress(CMotorManualSetting::Instance()->GetMotorStartLAddress(), CMotorManualSetting::Instance()->GetMotorEndLAddress());
		CDataManager::Instance()->m_PLCReaderD_Motor.SetPLCModule(&CDataManager::Instance()->m_MxComp3_PLC_3);
		CDataManager::Instance()->m_PLCReaderD_Motor.SetAddress(CMotorManualSetting::Instance()->GetMotorStartDAddress(), CMotorManualSetting::Instance()->GetMotorEndDAddress()); 
		CDataManager::Instance()->m_PLCReaderD_Alarm_G3.SetPLCModule(&CDataManager::Instance()->m_MxComp3_PLC_3);
		CDataManager::Instance()->m_PLCReaderD_Alarm_G3.SetAddress(91, 99);
		// 접속시 최초 한번 가져오고 이후는 필요시 가져오거나 쓰는 작업 - 트레이 및 레시피 정보
		CDataManager::Instance()->m_PLCReaderZR.SetPLCModule(&CDataManager::Instance()->m_MxComp3_PLC_1);
		CDataManager::Instance()->m_PLCReaderZR.SetAddress(20000, 21999);
		if(CDataManager::Instance()->IsConnected(CONN_PLC_1)) CDataManager::Instance()->m_PLCReaderZR.Read();
		// 자재 정보 포지션별
		CDataManager::Instance()->m_PLCReaderR_G3.SetPLCModule(&CDataManager::Instance()->m_MxComp3_PLC_3);
		CDataManager::Instance()->m_PLCReaderR_G3.SetAddress(100, 899);
	}
	// 자재 위치별 프로세스 처리 - 읽기쓰기
	CDataManager::Instance()->m_PLCReaderD_Process_G1.SetPLCModule(&CDataManager::Instance()->m_MxComp3_PLC_1);
	CDataManager::Instance()->m_PLCReaderD_Process_G1.SetAddress(30001, 30499);
	CDataManager::Instance()->m_PLCReaderD_Process_G2.SetPLCModule(&CDataManager::Instance()->m_MxComp3_PLC_2);
	CDataManager::Instance()->m_PLCReaderD_Process_G2.SetAddress(30100, 30499);
	CDataManager::Instance()->m_PLCReaderD_Process_G3.SetPLCModule(&CDataManager::Instance()->m_MxComp3_PLC_3);
	CDataManager::Instance()->m_PLCReaderD_Process_G3.SetAddress(30100, 30499);

	//
	DB_GET_WORKINFO();

	StartPLCReadThread();

	SetTimer(TM_CLOCK, 1000, NULL);

	if(CAppSetting::Instance()->GetGroupNumber() == 1) m_dlg_Gem.StartXGem300();

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

void CAMKOR_MMIDlg::Cal_Area()
{
	int nFrameGap = 5;
	CRect ClientRect;
	GetClientRect(&ClientRect);

	ClientRect.DeflateRect(nFrameGap,nFrameGap);

	int nTopH = 140;
	int nBottomH = 100;
	int nMLeftW = 200;
	
	// 타이틀 위쪽 전체 영역
	m_rcTop.SetRect(ClientRect.left,ClientRect.top,ClientRect.right,ClientRect.top+nTopH);

	// 아래 메뉴 영역
	m_rcBottom.SetRect(ClientRect.left,ClientRect.bottom-nBottomH,ClientRect.right,ClientRect.bottom);

	// 중간 영역
	m_rcMiddle.SetRect(ClientRect.left,ClientRect.top+nTopH,ClientRect.right,ClientRect.bottom-nBottomH);
	m_rcMiddle.DeflateRect(0,nFrameGap,0,nFrameGap);

	// 중간 왼쪽 영역
	m_rcMidLeft.SetRect(m_rcMiddle.left,m_rcMiddle.top,m_rcMiddle.left+nMLeftW,m_rcMiddle.bottom);

	// 중간 오른쪽 영역
	m_rcMidRight.SetRect(m_rcMiddle.left+nMLeftW+nFrameGap,m_rcMiddle.top,m_rcMiddle.right,m_rcMiddle.bottom);

	// 메뉴에 있는 다이얼로그 영역
	m_rcMenuDlgArea.SetRect(m_rcMiddle.left,m_rcMiddle.top,m_rcMiddle.right,m_rcBottom.bottom);
}

void CAMKOR_MMIDlg::Cal_CtrlArea()
{
	Cal_Area();

	Cal_CtrlArea_Top();
	Cal_CtrlArea_Bottom();
	Cal_CtrlArea_MidLeft();
	Cal_CtrlArea_MidRight();

	m_bCtrl_FirstLoad = TRUE;
}

void CAMKOR_MMIDlg::Cal_CtrlArea_Top()
{
	CRect CalRect;
	int n50pH = (int)((float)m_rcTop.Height()*0.5f);
	int nTimeWidth = 176;
	CRect rcDataTime, rcDate, rcTime;
	rcDataTime.SetRect(m_rcTop.left, m_rcTop.top, m_rcTop.left + nTimeWidth, m_rcTop.top+n50pH);
	rcDate.SetRect(rcDataTime.left, rcDataTime.top, rcDataTime.right, rcDataTime.top+rcDataTime.Height()/2);
	rcTime.SetRect(rcDataTime.left, rcDataTime.top+rcDataTime.Height()/2, rcDataTime.right, rcDataTime.bottom);

	m_st_Date.MoveWindow(&rcDate);
	m_st_Date.SetNumberOfLines(1);
	m_st_Date.SetXCharsPerLine(10);
	m_st_Date.SetSize(CMatrixStatic::SMALL);
	m_st_Date.SetDisplayColors(RGB(0, 0, 0), RGB(50, 255, 50), RGB(0, 0, 0));
	m_st_Date.SetText(_T("2018-08-10"));

	m_st_Time.MoveWindow(&rcTime);
	m_st_Time.SetNumberOfLines(1);
	m_st_Time.SetXCharsPerLine(10);
	m_st_Time.SetSize(CMatrixStatic::SMALL);
	m_st_Time.SetDisplayColors(RGB(0, 0, 0), RGB(50, 255, 50), RGB(0, 0, 0));
	m_st_Time.SetText(_T(" 00;00;00 "));

	CalRect.SetRect(rcDataTime.right, rcDataTime.top, rcDataTime.right+650, rcDataTime.bottom);
	m_btn_ModelName.SetFontBtn(CTRL_FONT, 36, FW_BOLD);
	m_btn_ModelName.SetButtonColor(CR_DARKSLATEGRAY3, CR_BUTTON_SUB_D);
	m_btn_ModelName.SetTextColor(CR_WHITE);
	m_btn_ModelName.SetFlatType(TRUE);
	m_btn_ModelName.MoveWindow(&CalRect);
	m_btn_ModelName.SetWindowText(_T("AMKOR    HPT-i200"));

	CalRect.SetRect(CalRect.right, CalRect.top, m_rcTop.right-70, CalRect.bottom);
	m_btn_DeviceName.SetFontBtn(CTRL_FONT, 24, FW_BOLD);
	m_btn_DeviceName.SetButtonColor(CR_DARKSLATEGRAY, CR_BUTTON_SUB_D);
	m_btn_DeviceName.SetTextColor(CR_LIGHTGRAY);
	m_btn_DeviceName.SetFlatType(TRUE);
	m_btn_DeviceName.MoveWindow(&CalRect);
	
	CalRect.SetRect(m_rcTop.right-70, CalRect.top, m_rcTop.right, CalRect.bottom);
	m_btn_Exit.SetFontBtn(CTRL_FONT, 20, FW_BOLD);
	m_btn_Exit.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_Exit.SetTextColor(CR_BLACK);
	m_btn_Exit.SetIconPos(2);
	m_btn_Exit.SetIconID(IDI_EXIT);
	m_btn_Exit.SetIconSize(48,48);
	m_btn_Exit.SetFlatType(TRUE);
	m_btn_Exit.MoveWindow(&CalRect);

	CalRect.SetRect(m_rcTop.left, m_rcTop.top+n50pH+CTRL_MARGIN, m_rcTop.left+100, m_rcTop.bottom);
	m_st_ErrorColor.SetBkColor(CR_RED);
	m_st_ErrorColor.SetTextColor(CR_WHITE);
	m_st_ErrorColor.SetFont(CTRL_FONT,24,FW_BOLD);
	m_st_ErrorColor.MoveWindow(&CalRect);

	CalRect.SetRect(CalRect.right, m_rcTop.top+n50pH+CTRL_MARGIN, CalRect.right+150, m_rcTop.bottom);
	m_btn_ErrorCode.SetFontBtn(CTRL_FONT, 24, FW_BOLD);
	m_btn_ErrorCode.SetButtonColor(CR_WHITE, CR_BUTTON_SUB_D);
	m_btn_ErrorCode.SetTextColor(CR_RED);
	m_btn_ErrorCode.SetFlatType(TRUE);
	m_btn_ErrorCode.MoveWindow(&CalRect);

	CalRect.SetRect(CalRect.right, m_rcTop.top+n50pH+CTRL_MARGIN, m_rcTop.right-190, m_rcTop.bottom);
	m_btn_ErrorText.SetFontBtn(CTRL_FONT, 24, FW_BOLD);
	m_btn_ErrorText.SetButtonColor(CR_WHITE, CR_BUTTON_SUB_D);
	m_btn_ErrorText.SetTextColor(CR_RED);
	m_btn_ErrorText.SetFlatType(TRUE);
	m_btn_ErrorText.MoveWindow(&CalRect);

	CalRect.SetRect(CalRect.right, m_rcTop.top+n50pH+CTRL_MARGIN, m_rcTop.right, m_rcTop.bottom);
	m_btn_Auth.SetFontBtn(CTRL_FONT, 24, FW_BOLD);
	m_btn_Auth.SetButtonColor(CR_OLIVE, CR_BUTTON_SUB_D);
	m_btn_Auth.SetTextColor(CR_BLACK);
	m_btn_Auth.SetIconPos(0);
	m_btn_Auth.SetIconID(IDI_AUTH);
	m_btn_Auth.SetIconSize(48,48);
	m_btn_Auth.SetFlatType(TRUE);
	m_btn_Auth.MoveWindow(&CalRect);
	if(m_nAuthNumber == 0) m_btn_Auth.SetWindowText(_T("OPERATOR"));
	if(m_nAuthNumber == 1) m_btn_Auth.SetWindowText(_T("ENGINEER"));
	if(m_nAuthNumber == 2) m_btn_Auth.SetWindowText(_T("DEVELOPER"));

	///////////////////////   GetCompile Time   /////////////////////////////////////////////////////

	CString strMonth[]={L"Jan", L"Feb", L"Mar", L"Apr", L"May", L"Jun", L"Jul", L"Aug", L"Sep", L"Oct", L"Nov", L"Dec"};

	CString date,time;
	date.Format(L"%s", _T(__DATE__));
	time.Format(L"%s", _T(__TIME__));
	for (int i = 0; i < 12; i++){
		if (date.Left(3) == strMonth[i]){
			CTime tmCompile( _ttoi(date.Right(4)), i + 1, _ttoi(date.Mid(4,2)), _ttoi(time.Left(2)), _ttoi(time.Mid(3,2)), _ttoi(time.Right(2)) );

			CString strCompileTime;
			strCompileTime = tmCompile.Format("%Y-%m-%d %H:%M:%S");
			CString strText;
			strText.Format(_T("G%d MMI Version [%s]"), CAppSetting::Instance()->GetGroupNumber(), strCompileTime);
			m_btn_DeviceName.SetWindowText(strText);
		}
	}
}

void CAMKOR_MMIDlg::Cal_CtrlArea_Bottom()
{
	int nBtnWidth = 120;
	CRect CalRect;

	CalRect.SetRect(m_rcBottom.left, m_rcBottom.top, m_rcBottom.left+nBtnWidth, m_rcBottom.bottom);
	m_btn_AutoMode.SetFontBtn(CTRL_FONT, 24, FW_BOLD);
	m_btn_AutoMode.SetButtonColor(CR_SLATEGRAY, CR_LIMEGREEN);
	m_btn_AutoMode.SetTextColor(CR_BLACK);
	m_btn_AutoMode.SetOnOffStyle();
	m_btn_AutoMode.SetOnOff(FALSE);
	m_btn_AutoMode.MoveWindow(&CalRect);

	CalRect.SetRect(CalRect.right, m_rcBottom.top, CalRect.right+nBtnWidth, m_rcBottom.bottom);
	m_btn_ManualMode.SetFontBtn(CTRL_FONT, 24, FW_BOLD);
	m_btn_ManualMode.SetButtonColor(CR_SLATEGRAY, CR_LIMEGREEN);
	m_btn_ManualMode.SetTextColor(CR_BLACK);
	m_btn_ManualMode.SetOnOffStyle();
	m_btn_ManualMode.SetOnOff(FALSE);
	m_btn_ManualMode.MoveWindow(&CalRect);

	CalRect.SetRect(CalRect.right, m_rcBottom.top, CalRect.right+nBtnWidth, m_rcBottom.bottom);
	m_btn_1.SetFontBtn(CTRL_FONT, 24, FW_BOLD);
	m_btn_1.SetButtonColor(CR_SLATEGRAY, CR_LIGHTSTEELBLUE);
	m_btn_1.SetTextColor(CR_BLACK);
	m_btn_1.MoveWindow(&CalRect);

	CalRect.SetRect(CalRect.right, m_rcBottom.top, CalRect.right+nBtnWidth, m_rcBottom.bottom);
	m_btn_2.SetFontBtn(CTRL_FONT, 24, FW_BOLD);
	m_btn_2.SetButtonColor(CR_SLATEGRAY, CR_LIGHTSTEELBLUE);
	m_btn_2.SetTextColor(CR_BLACK);
	m_btn_2.MoveWindow(&CalRect);

	CalRect.SetRect(CalRect.right, m_rcBottom.top, CalRect.right+nBtnWidth, m_rcBottom.bottom);
	m_btn_3.SetFontBtn(CTRL_FONT, 24, FW_BOLD);
	m_btn_3.SetButtonColor(CR_SLATEGRAY, CR_LIGHTSTEELBLUE);
	m_btn_3.SetTextColor(CR_BLACK);
	m_btn_3.MoveWindow(&CalRect);

	CalRect.SetRect(CalRect.right, m_rcBottom.top, CalRect.right+nBtnWidth, m_rcBottom.bottom);
	m_btn_4.SetFontBtn(CTRL_FONT, 24, FW_BOLD);
	m_btn_4.SetButtonColor(CR_SLATEGRAY, CR_LIGHTSTEELBLUE);
	m_btn_4.SetTextColor(CR_BLACK);
	m_btn_4.MoveWindow(&CalRect);

	CalRect.SetRect(CalRect.right, m_rcBottom.top, CalRect.right+nBtnWidth, m_rcBottom.bottom);
	m_btn_5.SetFontBtn(CTRL_FONT, 24, FW_BOLD);
	m_btn_5.SetButtonColor(CR_SLATEGRAY, CR_LIGHTSTEELBLUE);
	m_btn_5.SetTextColor(CR_BLACK);
	m_btn_5.MoveWindow(&CalRect);

	CalRect.SetRect(CalRect.right, m_rcBottom.top, CalRect.right+nBtnWidth, m_rcBottom.bottom);
	m_btn_6.SetFontBtn(CTRL_FONT, 24, FW_BOLD);
	m_btn_6.SetButtonColor(CR_SLATEGRAY, CR_LIGHTSTEELBLUE);
	m_btn_6.SetTextColor(CR_BLACK);
	m_btn_6.MoveWindow(&CalRect);

	if(m_bCtrl_FirstLoad == FALSE) m_dlg_Motor.Create(CDlg_Motor::IDD, this);
	m_dlg_Motor.MoveWindow(&m_rcMenuDlgArea);
	m_dlg_Motor.ShowWindow(SW_HIDE);

	if(m_bCtrl_FirstLoad == FALSE) m_dlg_MotorStacker.Create(CDlg_MotorStacker::IDD, this);
	m_dlg_MotorStacker.MoveWindow(&m_rcMenuDlgArea);
	m_dlg_MotorStacker.ShowWindow(SW_HIDE);

	if(m_bCtrl_FirstLoad == FALSE) m_dlg_Manual.Create(CDlg_Manual::IDD, this);
	m_dlg_Manual.MoveWindow(&m_rcMenuDlgArea);
	m_dlg_Manual.ShowWindow(SW_HIDE);

	if(m_bCtrl_FirstLoad == FALSE) m_dlg_Device.Create(CDlg_Device::IDD, this);
	m_dlg_Device.MoveWindow(&m_rcMenuDlgArea);
	m_dlg_Device.ShowWindow(SW_HIDE);

	if(m_bCtrl_FirstLoad == FALSE) m_dlg_IO.Create(CDlg_IO::IDD, this);
	m_dlg_IO.MoveWindow(&m_rcMenuDlgArea);
	m_dlg_IO.ShowWindow(SW_HIDE);

	if(m_bCtrl_FirstLoad == FALSE) m_dlg_Alarm.Create(CDlg_Alarm::IDD, this);
	m_dlg_Alarm.MoveWindow(&m_rcMenuDlgArea);
	m_dlg_Alarm.ShowWindow(SW_HIDE);

	if(m_bCtrl_FirstLoad == FALSE) m_dlg_Config.Create(CDlg_Config::IDD, this);
	m_dlg_Config.MoveWindow(&m_rcMenuDlgArea);
	m_dlg_Config.ShowWindow(SW_HIDE);
}

void CAMKOR_MMIDlg::Cal_CtrlArea_MidLeft()
{
	CRect CalRect;
	CRect rcUp, rcDn;
	CRect rcUpGBInside, rcDnGBInside;
	CRect rcUpDiv[10], rcDnDiv[5];

	int n50pH = (int)((float)m_rcMidLeft.Height()*0.5f);
	// Product Count Area
	rcUp.SetRect(m_rcMidLeft.left, m_rcMidLeft.top, m_rcMidLeft.right, m_rcMidLeft.top+n50pH);
	rcUpGBInside = rcUp;
	rcUpGBInside.DeflateRect(6,28,6,6);
	int n10pH = (int)((float)rcUpGBInside.Height()*0.1f);
	rcUpDiv[0].SetRect(rcUpGBInside.left, rcUpGBInside.top, rcUpGBInside.right, rcUpGBInside.top+n10pH);
	rcUpDiv[1].SetRect(rcUpGBInside.left, rcUpDiv[0].bottom, rcUpGBInside.right, rcUpDiv[0].bottom+n10pH);
	rcUpDiv[2].SetRect(rcUpGBInside.left, rcUpDiv[1].bottom, rcUpGBInside.right, rcUpDiv[1].bottom+n10pH);
	rcUpDiv[3].SetRect(rcUpGBInside.left, rcUpDiv[2].bottom, rcUpGBInside.right, rcUpDiv[2].bottom+n10pH);
	rcUpDiv[4].SetRect(rcUpGBInside.left, rcUpDiv[3].bottom, rcUpGBInside.right, rcUpDiv[3].bottom+n10pH);
	rcUpDiv[5].SetRect(rcUpGBInside.left, rcUpDiv[4].bottom, rcUpGBInside.right, rcUpDiv[4].bottom+n10pH);
	rcUpDiv[6].SetRect(rcUpGBInside.left, rcUpDiv[5].bottom, rcUpGBInside.right, rcUpDiv[5].bottom+n10pH);
	rcUpDiv[7].SetRect(rcUpGBInside.left, rcUpDiv[6].bottom, rcUpGBInside.right, rcUpDiv[6].bottom+n10pH);
	rcUpDiv[8].SetRect(rcUpGBInside.left, rcUpDiv[7].bottom, rcUpGBInside.right, rcUpDiv[7].bottom+n10pH);
	rcUpDiv[9].SetRect(rcUpGBInside.left, rcUpDiv[8].bottom, rcUpGBInside.right, rcUpGBInside.bottom);

	// Equip Status Area
	rcDn.SetRect(m_rcMidLeft.left, m_rcMidLeft.top+n50pH+CTRL_MARGIN2, m_rcMidLeft.right, m_rcMidLeft.bottom);
	rcDnGBInside = rcDn;
	rcDnGBInside.DeflateRect(6,28,6,6);
	int n20pH = (int)((float)rcDnGBInside.Height()*0.2f);
	rcDnDiv[0].SetRect(rcDnGBInside.left, rcDnGBInside.top, rcDnGBInside.right, rcDnGBInside.top+n20pH);
	rcDnDiv[1].SetRect(rcDnGBInside.left, rcDnDiv[0].bottom, rcDnGBInside.right, rcDnDiv[0].bottom+n20pH);
	rcDnDiv[2].SetRect(rcDnGBInside.left, rcDnDiv[1].bottom, rcDnGBInside.right, rcDnDiv[1].bottom+n20pH);
	rcDnDiv[3].SetRect(rcDnGBInside.left, rcDnDiv[2].bottom, rcDnGBInside.right, rcDnDiv[2].bottom+n20pH);
	rcDnDiv[4].SetRect(rcDnGBInside.left, rcDnDiv[3].bottom, rcDnGBInside.right, rcDnGBInside.bottom);


	m_gb_1.SetXPGroupStyle(CXPGroupBox::XPGB_WINDOW_2);
	m_gb_1.SetBackgroundColor(CR_DARKGRAY, CR_GB_BKGND);
	m_gb_1.SetBorderColor(CR_BLACK);
	m_gb_1.SetCatptionTextColor(CR_BLACK);
	m_gb_1.MoveWindow(&rcUp);

	m_st_T1.SetBkColor(CR_DIMGRAY);
	m_st_T1.SetTextColor(CR_WHITE);
	m_st_T1.SetFont(CTRL_FONT,20,FW_BOLD);
	m_st_T1.MoveWindow(&rcUpDiv[0]);

	m_st_V1.SetBkColor(CR_WHITE);
	m_st_V1.SetTextColor(CR_BLACK);
	m_st_V1.SetFont(CTRL_FONT,20,FW_BOLD);
	m_st_V1.MoveWindow(&rcUpDiv[1]);

	m_st_T2.SetBkColor(CR_DIMGRAY);
	m_st_T2.SetTextColor(CR_WHITE);
	m_st_T2.SetFont(CTRL_FONT,20,FW_BOLD);
	m_st_T2.MoveWindow(&rcUpDiv[2]);

	m_st_V2.SetBkColor(CR_WHITE);
	m_st_V2.SetTextColor(CR_BLACK);
	m_st_V2.SetFont(CTRL_FONT,20,FW_BOLD);
	m_st_V2.MoveWindow(&rcUpDiv[3]);

	m_st_T3.SetBkColor(CR_DIMGRAY);
	m_st_T3.SetTextColor(CR_WHITE);
	m_st_T3.SetFont(CTRL_FONT,20,FW_BOLD);
	m_st_T3.MoveWindow(&rcUpDiv[4]);

	m_st_V3.SetBkColor(CR_WHITE);
	m_st_V3.SetTextColor(CR_BLACK);
	m_st_V3.SetFont(CTRL_FONT,20,FW_BOLD);
	m_st_V3.MoveWindow(&rcUpDiv[5]);

	m_st_T4.SetBkColor(CR_DIMGRAY);
	m_st_T4.SetTextColor(CR_WHITE);
	m_st_T4.SetFont(CTRL_FONT,20,FW_BOLD);
	m_st_T4.MoveWindow(&rcUpDiv[6]);

	m_st_V4.SetBkColor(CR_WHITE);
	m_st_V4.SetTextColor(CR_BLACK);
	m_st_V4.SetFont(CTRL_FONT,20,FW_BOLD);
	m_st_V4.MoveWindow(&rcUpDiv[7]);

	m_btn_ResetCount.SetFontBtn(CTRL_FONT, 20, FW_BOLD);
	m_btn_ResetCount.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_ResetCount.SetTextColor(CR_BLACK);
	m_btn_ResetCount.MoveWindow(&rcUpDiv[9]);

	m_gb_2.SetXPGroupStyle(CXPGroupBox::XPGB_WINDOW_2);
	m_gb_2.SetBackgroundColor(CR_DARKGRAY, CR_GB_BKGND);
	m_gb_2.SetBorderColor(CR_BLACK);
	m_gb_2.SetCatptionTextColor(CR_BLACK);
	m_gb_2.MoveWindow(&rcDn);

	m_btn_Start.SetFontBtn(CTRL_FONT, 24, FW_BOLD);
	m_btn_Start.SetButtonColor(CR_DARKGRAY, CR_LIMEGREEN);
	m_btn_Start.SetTextColor(CR_BLACK);
	m_btn_Start.SetIconPos(0);
	m_btn_Start.SetIconID(IDI_START);
	m_btn_Start.SetIconSize(64,64);
	m_btn_Start.SetOnOffStyle();
	m_btn_Start.SetOnOff(FALSE);
	m_btn_Start.MoveWindow(&rcDnDiv[0]);
	m_btn_Stop.SetFontBtn(CTRL_FONT, 24, FW_BOLD);
	m_btn_Stop.SetButtonColor(CR_DARKGRAY, CR_ORANGERED);
	m_btn_Stop.SetTextColor(CR_BLACK);
	m_btn_Stop.SetIconPos(0);
	m_btn_Stop.SetIconID(IDI_STOP);
	m_btn_Stop.SetIconSize(64,64);
	m_btn_Stop.SetOnOffStyle();
	m_btn_Stop.SetOnOff(FALSE);
	m_btn_Stop.MoveWindow(&rcDnDiv[1]);
	m_btn_Reset.SetFontBtn(CTRL_FONT, 24, FW_BOLD);
	m_btn_Reset.SetButtonColor(CR_DARKGRAY, CR_BLUE);
	m_btn_Reset.SetTextColor(CR_BLACK);
	m_btn_Reset.SetIconPos(0);
	m_btn_Reset.SetIconID(IDI_RESET);
	m_btn_Reset.SetIconSize(64,64);
	m_btn_Reset.SetOnOffStyle();
	m_btn_Reset.SetOnOff(FALSE);
	m_btn_Reset.MoveWindow(&rcDnDiv[2]);
	m_btn_BuzOff.SetFontBtn(CTRL_FONT, 24, FW_BOLD);
	m_btn_BuzOff.SetButtonColor(CR_DARKGRAY, CR_LIGHTGRAY);
	m_btn_BuzOff.SetTextColor(CR_BLACK);
	m_btn_BuzOff.SetIconPos(0);
	m_btn_BuzOff.SetIconID(IDI_BUZ_OFF);
	m_btn_BuzOff.SetIconSize(64,64);
	m_btn_BuzOff.SetOnOffStyle();
	m_btn_BuzOff.SetOnOff(FALSE);
	m_btn_BuzOff.MoveWindow(&rcDnDiv[3]);
	m_btn_Home.SetFontBtn(CTRL_FONT, 24, FW_BOLD);
	m_btn_Home.SetButtonColor(CR_DARKGRAY, CR_LIGHTGRAY);
	m_btn_Home.SetTextColor(CR_BLACK);
	m_btn_Home.SetIconPos(0);
	m_btn_Home.SetIconID(IDI_HOME);
	m_btn_Home.SetIconSize(64,64);
	m_btn_Home.MoveWindow(&rcDnDiv[4]);
}

void CAMKOR_MMIDlg::Cal_CtrlArea_MidRight()
{
	CRect CalRect;
	CRect rcTabArea, rcScreenArea, rcConnArea;
	int nTabHeight = 40;
	int nBtnWidth = 140;
	// Button Tab Area
	rcTabArea.SetRect(m_rcMidRight.left, m_rcMidRight.top, m_rcMidRight.right, m_rcMidRight.top+nTabHeight);

	CalRect.SetRect(rcTabArea.left, rcTabArea.top, rcTabArea.left+nBtnWidth, rcTabArea.bottom);
	m_btn_SystemInfo.SetFontBtn(CTRL_FONT, 22, FW_BOLD);
	m_btn_SystemInfo.SetButtonColor(CR_SLATEGRAY, CR_LIGHTSTEELBLUE);
	m_btn_SystemInfo.SetTextColor(CR_BLACK);
	m_btn_SystemInfo.SetOnOffStyle();
	m_btn_SystemInfo.MoveWindow(&CalRect);
	CalRect.SetRect(CalRect.right, rcTabArea.top, CalRect.right+nBtnWidth, rcTabArea.bottom);
	m_btn_Gem.SetFontBtn(CTRL_FONT, 22, FW_BOLD);
	m_btn_Gem.SetButtonColor(CR_SLATEGRAY, CR_LIGHTSTEELBLUE);
	m_btn_Gem.SetTextColor(CR_BLACK);
	m_btn_Gem.SetOnOffStyle();
	m_btn_Gem.MoveWindow(&CalRect);
	if(CAppSetting::Instance()->GetGroupNumber() == 1) m_btn_Gem.ShowWindow(SW_SHOW);
	else m_btn_Gem.ShowWindow(SW_HIDE);

	// Screen Area
	rcScreenArea.SetRect(m_rcMidRight.left, m_rcMidRight.top+nTabHeight, m_rcMidRight.right, m_rcMidRight.bottom);
	if(m_bCtrl_FirstLoad == FALSE) m_dlg_MainSystemInfo.Create(CDlg_Main_SystemInfo::IDD, this);
	m_dlg_MainSystemInfo.MoveWindow(&rcScreenArea);
	m_dlg_MainSystemInfo.ShowWindow(SW_HIDE);

	if(m_bCtrl_FirstLoad == FALSE) m_dlg_Gem.Create(CDlg_Gem::IDD, this);
	m_dlg_Gem.MoveWindow(&rcScreenArea);
	m_dlg_Gem.ShowWindow(SW_HIDE);

	ChangeSubScreen(m_nCurSubScreen);

	// Connection View Area
	m_rcConnBtn[0].SetRect(m_rcMidRight.right-34, m_rcMidRight.top+2, m_rcMidRight.right, m_rcMidRight.top+32);
	m_rcConnBtn[1].SetRect(m_rcConnBtn[0].left-34, m_rcMidRight.top+2, m_rcConnBtn[0].left, m_rcMidRight.top+32);
	m_rcConnBtn[2].SetRect(m_rcConnBtn[1].left-34, m_rcMidRight.top+2, m_rcConnBtn[1].left, m_rcMidRight.top+32);
	m_rcConnBtn[3].SetRect(m_rcConnBtn[2].left-34, m_rcMidRight.top+2, m_rcConnBtn[2].left, m_rcMidRight.top+32);
	m_rcConnBtn[4].SetRect(m_rcConnBtn[3].left-34, m_rcMidRight.top+2, m_rcConnBtn[3].left, m_rcMidRight.top+32);
	m_rcConnBtn[5].SetRect(m_rcConnBtn[4].left-34, m_rcMidRight.top+2, m_rcConnBtn[4].left, m_rcMidRight.top+32);
	m_rcConnBtn[6].SetRect(m_rcConnBtn[5].left-34, m_rcMidRight.top+2, m_rcConnBtn[5].left, m_rcMidRight.top+32);

	m_btn_Conn_1.SetFontBtn(CTRL_FONT, 14, FW_NORMAL);
	m_btn_Conn_1.SetButtonColor(CR_SLATEGRAY, CR_LIMEGREEN);
	m_btn_Conn_1.SetTextColor(CR_BLACK);
	m_btn_Conn_1.MoveWindow(&m_rcConnBtn[0]);
	m_btn_Conn_1.ShowWindow(SW_HIDE);

	m_btn_Conn_2.SetFontBtn(CTRL_FONT, 14, FW_NORMAL);
	m_btn_Conn_2.SetButtonColor(CR_SLATEGRAY, CR_LIMEGREEN);
	m_btn_Conn_2.SetTextColor(CR_BLACK);
	m_btn_Conn_2.MoveWindow(&m_rcConnBtn[1]);
	m_btn_Conn_2.ShowWindow(SW_HIDE);

	m_btn_Conn_3.SetFontBtn(CTRL_FONT, 14, FW_NORMAL);
	m_btn_Conn_3.SetButtonColor(CR_SLATEGRAY, CR_LIMEGREEN);
	m_btn_Conn_3.SetTextColor(CR_BLACK);
	m_btn_Conn_3.MoveWindow(&m_rcConnBtn[2]);
	m_btn_Conn_3.ShowWindow(SW_HIDE);

	m_btn_Conn_4.SetFontBtn(CTRL_FONT, 14, FW_NORMAL);
	m_btn_Conn_4.SetButtonColor(CR_SLATEGRAY, CR_LIMEGREEN);
	m_btn_Conn_4.SetTextColor(CR_BLACK);
	m_btn_Conn_4.MoveWindow(&m_rcConnBtn[3]);
	m_btn_Conn_4.ShowWindow(SW_HIDE);

	m_btn_Conn_5.SetFontBtn(CTRL_FONT, 14, FW_NORMAL);
	m_btn_Conn_5.SetButtonColor(CR_SLATEGRAY, CR_LIMEGREEN);
	m_btn_Conn_5.SetTextColor(CR_BLACK);
	m_btn_Conn_5.MoveWindow(&m_rcConnBtn[4]);
	m_btn_Conn_5.ShowWindow(SW_HIDE);

	m_btn_Conn_6.SetFontBtn(CTRL_FONT, 14, FW_NORMAL);
	m_btn_Conn_6.SetButtonColor(CR_SLATEGRAY, CR_LIMEGREEN);
	m_btn_Conn_6.SetTextColor(CR_BLACK);
	m_btn_Conn_6.MoveWindow(&m_rcConnBtn[5]);
	m_btn_Conn_6.ShowWindow(SW_HIDE);

	m_btn_Conn_7.SetFontBtn(CTRL_FONT, 14, FW_NORMAL);
	m_btn_Conn_7.SetButtonColor(CR_SLATEGRAY, CR_LIMEGREEN);
	m_btn_Conn_7.SetTextColor(CR_BLACK);
	m_btn_Conn_7.MoveWindow(&m_rcConnBtn[6]);
	m_btn_Conn_7.ShowWindow(SW_HIDE);

	SetViewConnButton();
}

void CAMKOR_MMIDlg::ChangeScreen(int nCurScreen)
{
	if(nCurScreen == 0)
	{
		KillTimer(TM_UPDATE_MOTOR);
		KillTimer(TM_UPDATE_MANUAL);
		KillTimer(TM_UPDATE_DEVICE);
		KillTimer(TM_UPDATE_IO);
		KillTimer(TM_UPDATE_ALARM);

		m_dlg_Motor.ShowWindow(SW_HIDE);
		m_dlg_MotorStacker.ShowWindow(SW_HIDE);
		m_dlg_Manual.ShowWindow(SW_HIDE);
		m_dlg_Device.ShowWindow(SW_HIDE);
		m_dlg_IO.ShowWindow(SW_HIDE);
		m_dlg_Alarm.ShowWindow(SW_HIDE);
		m_dlg_Config.ShowWindow(SW_HIDE);

		ShowMainScreen(TRUE);
	}
	if(nCurScreen == 1)
	{
		KillTimer(TM_UPDATE_MANUAL);
		KillTimer(TM_UPDATE_DEVICE);
		KillTimer(TM_UPDATE_IO);
		KillTimer(TM_UPDATE_ALARM);

		m_dlg_MotorStacker.ShowWindow(SW_HIDE);
		m_dlg_Manual.ShowWindow(SW_HIDE);
		m_dlg_Device.ShowWindow(SW_HIDE);
		m_dlg_IO.ShowWindow(SW_HIDE);
		m_dlg_Alarm.ShowWindow(SW_HIDE);
		m_dlg_Config.ShowWindow(SW_HIDE);

		ShowMainScreen(FALSE);

		m_dlg_Motor.ShowWindow(SW_SHOW);

		SetTimer(TM_UPDATE_MOTOR, 500, 0);
	}
	if(nCurScreen == 2)
	{
		KillTimer(TM_UPDATE_MOTOR);
		KillTimer(TM_UPDATE_DEVICE);
		KillTimer(TM_UPDATE_IO);
		KillTimer(TM_UPDATE_ALARM);

		m_dlg_Motor.ShowWindow(SW_HIDE);
		m_dlg_MotorStacker.ShowWindow(SW_HIDE);
		m_dlg_Device.ShowWindow(SW_HIDE);
		m_dlg_IO.ShowWindow(SW_HIDE);
		m_dlg_Alarm.ShowWindow(SW_HIDE);
		m_dlg_Config.ShowWindow(SW_HIDE);

		ShowMainScreen(FALSE);

		m_dlg_Manual.ShowWindow(SW_SHOW);

		SetTimer(TM_UPDATE_MANUAL, 500, 0);
	}
	if(nCurScreen == 3)
	{
		KillTimer(TM_UPDATE_MOTOR);
		KillTimer(TM_UPDATE_MANUAL);
		KillTimer(TM_UPDATE_IO);
		KillTimer(TM_UPDATE_ALARM);

		m_dlg_Motor.ShowWindow(SW_HIDE);
		m_dlg_MotorStacker.ShowWindow(SW_HIDE);
		m_dlg_IO.ShowWindow(SW_HIDE);
		m_dlg_Alarm.ShowWindow(SW_HIDE);
		m_dlg_Config.ShowWindow(SW_HIDE);

		ShowMainScreen(FALSE);

		m_dlg_Device.ShowWindow(SW_SHOW);

		SetTimer(TM_UPDATE_DEVICE, 500, 0);
	}
	if(nCurScreen == 4)
	{
		KillTimer(TM_UPDATE_MOTOR);
		KillTimer(TM_UPDATE_MANUAL);
		KillTimer(TM_UPDATE_DEVICE);
		KillTimer(TM_UPDATE_ALARM);

		m_dlg_Motor.ShowWindow(SW_HIDE);
		m_dlg_MotorStacker.ShowWindow(SW_HIDE);
		m_dlg_Manual.ShowWindow(SW_HIDE);
		m_dlg_Device.ShowWindow(SW_HIDE);
		m_dlg_Alarm.ShowWindow(SW_HIDE);
		m_dlg_Config.ShowWindow(SW_HIDE);

		ShowMainScreen(FALSE);

		m_dlg_IO.ShowWindow(SW_SHOW);
		m_dlg_IO.Update_IOList_Input();
		m_dlg_IO.Update_IOList_Output();

		SetTimer(TM_UPDATE_IO, 500, 0);
	}
	if(nCurScreen == 5)
	{
		KillTimer(TM_UPDATE_MOTOR);
		KillTimer(TM_UPDATE_MANUAL);
		KillTimer(TM_UPDATE_DEVICE);
		KillTimer(TM_UPDATE_IO);

		m_dlg_Motor.ShowWindow(SW_HIDE);
		m_dlg_MotorStacker.ShowWindow(SW_HIDE);
		m_dlg_Manual.ShowWindow(SW_HIDE);
		m_dlg_Device.ShowWindow(SW_HIDE);
		m_dlg_IO.ShowWindow(SW_HIDE);
		m_dlg_Config.ShowWindow(SW_HIDE);

		ShowMainScreen(FALSE);

		m_dlg_Alarm.ShowWindow(SW_SHOW);

		SetTimer(TM_UPDATE_ALARM, 500, 0);
	}
	if(nCurScreen == 6)
	{
		KillTimer(TM_UPDATE_MOTOR);
		KillTimer(TM_UPDATE_MANUAL);
		KillTimer(TM_UPDATE_DEVICE);
		KillTimer(TM_UPDATE_IO);
		KillTimer(TM_UPDATE_ALARM);

		m_dlg_Motor.ShowWindow(SW_HIDE);
		m_dlg_MotorStacker.ShowWindow(SW_HIDE);
		m_dlg_Manual.ShowWindow(SW_HIDE);
		m_dlg_Device.ShowWindow(SW_HIDE);
		m_dlg_IO.ShowWindow(SW_HIDE);
		m_dlg_Alarm.ShowWindow(SW_HIDE);

		ShowMainScreen(FALSE);

		m_dlg_Config.ShowWindow(SW_SHOW);
	}
	if(nCurScreen == 100)
	{
		KillTimer(TM_UPDATE_MANUAL);
		KillTimer(TM_UPDATE_DEVICE);
		KillTimer(TM_UPDATE_IO);
		KillTimer(TM_UPDATE_ALARM);

		m_dlg_Motor.ShowWindow(SW_HIDE);
		m_dlg_Manual.ShowWindow(SW_HIDE);
		m_dlg_Device.ShowWindow(SW_HIDE);
		m_dlg_IO.ShowWindow(SW_HIDE);
		m_dlg_Alarm.ShowWindow(SW_HIDE);
		m_dlg_Config.ShowWindow(SW_HIDE);

		ShowMainScreen(FALSE);

		m_dlg_MotorStacker.ShowWindow(SW_SHOW);

		SetTimer(TM_UPDATE_MOTOR, 500, 0);
	}
}

void CAMKOR_MMIDlg::ShowMainScreen(BOOL bShow)
{
	if(bShow)
	{
		m_gb_1.ShowWindow(SW_SHOW);

		m_st_T1.ShowWindow(SW_SHOW);
		m_st_V1.ShowWindow(SW_SHOW);
		m_st_T2.ShowWindow(SW_SHOW);
		m_st_V2.ShowWindow(SW_SHOW);
		m_st_T3.ShowWindow(SW_SHOW);
		m_st_V3.ShowWindow(SW_SHOW);
		m_st_T4.ShowWindow(SW_SHOW);
		m_st_V4.ShowWindow(SW_SHOW);
		m_btn_ResetCount.ShowWindow(SW_SHOW);

		m_gb_2.ShowWindow(SW_SHOW);

		m_btn_Start.ShowWindow(SW_SHOW);
		m_btn_Stop.ShowWindow(SW_SHOW);
		m_btn_Reset.ShowWindow(SW_SHOW);
		m_btn_BuzOff.ShowWindow(SW_SHOW);
		m_btn_Home.ShowWindow(SW_SHOW);

		m_btn_SystemInfo.ShowWindow(SW_SHOW);
		if(CAppSetting::Instance()->GetGroupNumber() == 1) m_btn_Gem.ShowWindow(SW_SHOW);
		ChangeSubScreen(m_nCurSubScreen);

		m_btn_AutoMode.ShowWindow(SW_SHOW);
		m_btn_ManualMode.ShowWindow(SW_SHOW);
		m_btn_1.ShowWindow(SW_SHOW);
		m_btn_2.ShowWindow(SW_SHOW);
		m_btn_3.ShowWindow(SW_SHOW);
		m_btn_4.ShowWindow(SW_SHOW);
		m_btn_5.ShowWindow(SW_SHOW);
		m_btn_6.ShowWindow(SW_SHOW);

		m_btn_Conn_1.ShowWindow(SW_SHOW);
		m_btn_Conn_2.ShowWindow(SW_SHOW);
		m_btn_Conn_3.ShowWindow(SW_SHOW);
		m_btn_Conn_4.ShowWindow(SW_SHOW);
		m_btn_Conn_5.ShowWindow(SW_SHOW);
		m_btn_Conn_6.ShowWindow(SW_SHOW);
		m_btn_Conn_7.ShowWindow(SW_SHOW);
		SetViewConnButton();
	}
	else
	{
		m_gb_1.ShowWindow(SW_HIDE);

		m_st_T1.ShowWindow(SW_HIDE);
		m_st_V1.ShowWindow(SW_HIDE);
		m_st_T2.ShowWindow(SW_HIDE);
		m_st_V2.ShowWindow(SW_HIDE);
		m_st_T3.ShowWindow(SW_HIDE);
		m_st_V3.ShowWindow(SW_HIDE);
		m_st_T4.ShowWindow(SW_HIDE);
		m_st_V4.ShowWindow(SW_HIDE);
		m_btn_ResetCount.ShowWindow(SW_HIDE);

		m_gb_2.ShowWindow(SW_HIDE);

		m_btn_Start.ShowWindow(SW_HIDE);
		m_btn_Stop.ShowWindow(SW_HIDE);
		m_btn_Reset.ShowWindow(SW_HIDE);
		m_btn_BuzOff.ShowWindow(SW_HIDE);
		m_btn_Home.ShowWindow(SW_HIDE);

		m_btn_SystemInfo.ShowWindow(SW_HIDE);
		m_btn_Gem.ShowWindow(SW_HIDE);
		m_dlg_MainSystemInfo.ShowWindow(SW_HIDE);
		m_dlg_Gem.ShowWindow(SW_HIDE);

		m_btn_AutoMode.ShowWindow(SW_HIDE);
		m_btn_ManualMode.ShowWindow(SW_HIDE);
		m_btn_1.ShowWindow(SW_HIDE);
		m_btn_2.ShowWindow(SW_HIDE);
		m_btn_3.ShowWindow(SW_HIDE);
		m_btn_4.ShowWindow(SW_HIDE);
		m_btn_5.ShowWindow(SW_HIDE);
		m_btn_6.ShowWindow(SW_HIDE);

		m_btn_Conn_1.ShowWindow(SW_HIDE);
		m_btn_Conn_2.ShowWindow(SW_HIDE);
		m_btn_Conn_3.ShowWindow(SW_HIDE);
		m_btn_Conn_4.ShowWindow(SW_HIDE);
		m_btn_Conn_5.ShowWindow(SW_HIDE);
		m_btn_Conn_6.ShowWindow(SW_HIDE);
		m_btn_Conn_7.ShowWindow(SW_HIDE);
	}
}

void CAMKOR_MMIDlg::ChangeSubScreen(int nCurSubScreen)
{
	if(nCurSubScreen == 0)
	{
		m_dlg_MainSystemInfo.ShowWindow(SW_SHOW);
		m_dlg_Gem.ShowWindow(SW_HIDE);

		m_btn_SystemInfo.SetOnOff(TRUE);
		m_btn_SystemInfo.Invalidate();
		m_btn_Gem.SetOnOff(FALSE);
		m_btn_Gem.Invalidate();
	}
	if(nCurSubScreen == 1)
	{
		m_dlg_MainSystemInfo.ShowWindow(SW_HIDE);
		m_dlg_Gem.ShowWindow(SW_SHOW);

		m_btn_SystemInfo.SetOnOff(FALSE);
		m_btn_SystemInfo.Invalidate();
		m_btn_Gem.SetOnOff(TRUE);
		m_btn_Gem.Invalidate();
	}
}

void CAMKOR_MMIDlg::SetViewConnButton()
{
	if(CAppSetting::Instance()->GetGroupNumber() == 1) 
	{
		m_btn_Conn_1.MoveWindow(&m_rcConnBtn[0]); // PLC1
		m_btn_Conn_1.ShowWindow(SW_SHOW);
		m_btn_Conn_2.MoveWindow(&m_rcConnBtn[1]); // PLC2
		m_btn_Conn_2.ShowWindow(SW_SHOW);
		m_btn_Conn_3.MoveWindow(&m_rcConnBtn[2]); // PLC3
		m_btn_Conn_3.ShowWindow(SW_SHOW);
		m_btn_Conn_4.MoveWindow(&m_rcConnBtn[3]); // HMS1
		m_btn_Conn_4.ShowWindow(SW_SHOW);
		m_btn_Conn_5.MoveWindow(&m_rcConnBtn[4]); // HMS2
		m_btn_Conn_5.ShowWindow(SW_SHOW);
		m_btn_Conn_6.MoveWindow(&m_rcConnBtn[5]); // VIS
		m_btn_Conn_6.ShowWindow(SW_SHOW);
		m_btn_Conn_7.MoveWindow(&m_rcConnBtn[6]); // GEM
		m_btn_Conn_7.ShowWindow(SW_SHOW);
	}
	else if(CAppSetting::Instance()->GetGroupNumber() == 2) 
	{
		m_btn_Conn_1.MoveWindow(&m_rcConnBtn[0]); // PLC1
		m_btn_Conn_1.ShowWindow(SW_SHOW);
		m_btn_Conn_2.MoveWindow(&m_rcConnBtn[1]); // PLC2
		m_btn_Conn_2.ShowWindow(SW_SHOW);
		m_btn_Conn_3.MoveWindow(&m_rcConnBtn[2]); // PLC3
		m_btn_Conn_3.ShowWindow(SW_SHOW);
	}
	else if(CAppSetting::Instance()->GetGroupNumber() == 3) 
	{
		m_btn_Conn_1.MoveWindow(&m_rcConnBtn[0]); // PLC1
		m_btn_Conn_1.ShowWindow(SW_SHOW);
		m_btn_Conn_2.MoveWindow(&m_rcConnBtn[1]); // PLC2
		m_btn_Conn_2.ShowWindow(SW_SHOW);
		m_btn_Conn_3.MoveWindow(&m_rcConnBtn[2]); // PLC3
		m_btn_Conn_3.ShowWindow(SW_SHOW);
	}
}

void CAMKOR_MMIDlg::UpdateViewConnStatus()
{
	if(CAppSetting::Instance()->GetGroupNumber() == 1) 
	{
		if(CDataManager::Instance()->IsConnected(CONN_PLC_1)) m_btn_Conn_1.SetButtonColor(CR_LIMEGREEN, CR_SLATEGRAY);
		else m_btn_Conn_1.SetButtonColor(CR_SLATEGRAY, CR_LIMEGREEN);
		m_btn_Conn_1.Invalidate();
		if(CDataManager::Instance()->IsConnected(CONN_PLC_2)) m_btn_Conn_2.SetButtonColor(CR_LIMEGREEN, CR_SLATEGRAY);
		else m_btn_Conn_2.SetButtonColor(CR_SLATEGRAY, CR_LIMEGREEN);
		m_btn_Conn_2.Invalidate();
		if(CDataManager::Instance()->IsConnected(CONN_PLC_3)) m_btn_Conn_3.SetButtonColor(CR_LIMEGREEN, CR_SLATEGRAY);
		else m_btn_Conn_3.SetButtonColor(CR_SLATEGRAY, CR_LIMEGREEN);
		m_btn_Conn_3.Invalidate();
		if(CDataManager::Instance()->IsConnected(CONN_HMS_G1)) m_btn_Conn_4.SetButtonColor(CR_LIMEGREEN, CR_SLATEGRAY);
		else m_btn_Conn_4.SetButtonColor(CR_SLATEGRAY, CR_LIMEGREEN);
		m_btn_Conn_4.Invalidate();
		if(CDataManager::Instance()->IsConnected(CONN_HMS_G2)) m_btn_Conn_5.SetButtonColor(CR_LIMEGREEN, CR_SLATEGRAY);
		else m_btn_Conn_5.SetButtonColor(CR_SLATEGRAY, CR_LIMEGREEN);
		m_btn_Conn_5.Invalidate();
		if(CDataManager::Instance()->IsConnected(CONN_VISION)) m_btn_Conn_6.SetButtonColor(CR_LIMEGREEN, CR_SLATEGRAY);
		else m_btn_Conn_6.SetButtonColor(CR_SLATEGRAY, CR_LIMEGREEN);
		m_btn_Conn_6.Invalidate();
		if(CDataManager::Instance()->IsConnected(CONN_MES)) m_btn_Conn_7.SetButtonColor(CR_LIMEGREEN, CR_SLATEGRAY);
		else m_btn_Conn_7.SetButtonColor(CR_SLATEGRAY, CR_LIMEGREEN);
		m_btn_Conn_7.Invalidate();
	}
	else if(CAppSetting::Instance()->GetGroupNumber() == 2) 
	{
		if(CDataManager::Instance()->IsConnected(CONN_PLC_1)) m_btn_Conn_1.SetButtonColor(CR_LIMEGREEN, CR_SLATEGRAY);
		else m_btn_Conn_1.SetButtonColor(CR_SLATEGRAY, CR_LIMEGREEN);
		m_btn_Conn_1.Invalidate();
		if(CDataManager::Instance()->IsConnected(CONN_PLC_2)) m_btn_Conn_2.SetButtonColor(CR_LIMEGREEN, CR_SLATEGRAY);
		else m_btn_Conn_2.SetButtonColor(CR_SLATEGRAY, CR_LIMEGREEN);
		m_btn_Conn_2.Invalidate();
		if(CDataManager::Instance()->IsConnected(CONN_PLC_3)) m_btn_Conn_3.SetButtonColor(CR_LIMEGREEN, CR_SLATEGRAY);
		else m_btn_Conn_3.SetButtonColor(CR_SLATEGRAY, CR_LIMEGREEN);
		m_btn_Conn_3.Invalidate();
	}
	else if(CAppSetting::Instance()->GetGroupNumber() == 3) 
	{
		if(CDataManager::Instance()->IsConnected(CONN_PLC_1)) m_btn_Conn_1.SetButtonColor(CR_LIMEGREEN, CR_SLATEGRAY);
		else m_btn_Conn_1.SetButtonColor(CR_SLATEGRAY, CR_LIMEGREEN);
		m_btn_Conn_1.Invalidate();
		if(CDataManager::Instance()->IsConnected(CONN_PLC_2)) m_btn_Conn_2.SetButtonColor(CR_LIMEGREEN, CR_SLATEGRAY);
		else m_btn_Conn_2.SetButtonColor(CR_SLATEGRAY, CR_LIMEGREEN);
		m_btn_Conn_2.Invalidate();
		if(CDataManager::Instance()->IsConnected(CONN_PLC_3)) m_btn_Conn_3.SetButtonColor(CR_LIMEGREEN, CR_SLATEGRAY);
		else m_btn_Conn_3.SetButtonColor(CR_SLATEGRAY, CR_LIMEGREEN);
		m_btn_Conn_3.Invalidate();
	}
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다. 문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CAMKOR_MMIDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트입니다.

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CAMKOR_MMIDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CAMKOR_MMIDlg::OnDestroy()
{
	CDialogEx::OnDestroy();

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	SocketServerStop();
	StopPLCReadThread();
}


BOOL CAMKOR_MMIDlg::OnEraseBkgnd(CDC* pDC)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	// 전체 배경색
	CRect rect;
    GetClientRect(&rect);
    CBrush myBrush(CR_MAIN_BKGND);	// dialog background color
    CBrush *pOld = pDC->SelectObject(&myBrush);
    BOOL bRes  = pDC->PatBlt(0, 0, rect.Width(), rect.Height(), PATCOPY);
    pDC->SelectObject(pOld);		// restore old brush
    return FALSE;     
	//return CDialogEx::OnEraseBkgnd(pDC);
}


void CAMKOR_MMIDlg::OnSize(UINT nType, int cx, int cy)
{
	CDialogEx::OnSize(nType, cx, cy);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	if(m_bCtrl_FirstLoad) Cal_CtrlArea();
}


void CAMKOR_MMIDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	CTime   CurTime;
	CString strTemp;
	if(nIDEvent == TM_CLOCK)
	{
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		// 현재 시스템 시각을 구한다.
		CurTime = CTime::GetCurrentTime();

		strTemp.Format(L"%04d-%02d-%02d", CurTime.GetYear(), CurTime.GetMonth(), CurTime.GetDay());
		m_st_Date.SetText(strTemp);

		strTemp.Format(L" %02d;%02d;%02d ", CurTime.GetHour(), CurTime.GetMinute(), CurTime.GetSecond());
		m_st_Time.SetText(strTemp);	
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		// CLOCK 관련 설정
		int nYear, nMonth, nDay, nHour;
		nYear = CurTime.GetYear();  
		nMonth = CurTime.GetMonth();  
		nDay = CurTime.GetDay();
		nHour = CurTime.GetHour();

		// 아직 날짜를 못받았다면 ...
		if(m_nYear == 0)
		{
			m_nYear = nYear;
			m_nMonth = nMonth;
			m_nDay = nDay;
			m_nHour = nHour;
		}
		else
		{
			if(m_nHour == 23 && nHour == 0) // 하루 기준을 넘어갔다면
			{
				// 로그리스트 리셋
				CString strLogPath;
				strLogPath.Format(_T("%s\\LOG\\DEBUG"), CMotorManualSetting::Instance()->GetExecuteDirectory());
				RemoveFolderFileByElapsedDays(strLogPath, 31*6); // 6개월 지난 파일 제거
			}

			m_nYear = nYear;
			m_nMonth = nMonth;
			m_nDay = nDay;
			m_nHour = nHour;
		}
		// Update Tray Pos Info
		CDataManager::Instance()->Update_TrayPackPosInfo();
		CDataManager::Instance()->UpdateLotInfo();
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		// DAddress Parse
		if(CAppSetting::Instance()->GetGroupNumber() == 1) Parse_DAddress_G1();
		else if(CAppSetting::Instance()->GetGroupNumber() == 2) Parse_DAddress_G2();
		else if(CAppSetting::Instance()->GetGroupNumber() == 3) Parse_DAddress_G3();
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		// UPDATE MAIN UI
		if(m_nCurScreen == 0)
		{
			Update_UI();
			// UPDATE SUBSCREEN UI
			if(m_nCurSubScreen == 0)
			{
				m_dlg_MainSystemInfo.Update_UI();
				m_dlg_MainSystemInfo.Update_LogList();
			}
		}
	}
	if(nIDEvent == TM_UPDATE_MOTOR)
	{
		if(CAppSetting::Instance()->GetGroupNumber() == 1)
		{
			if(CDataManager::Instance()->IsConnected(CONN_PLC_1))
			{
				if(m_nCurScreen == 1) m_dlg_Motor.Update_PLC_Value();
				if(m_nCurScreen == 100) m_dlg_MotorStacker.Update_PLC_Value();
			}
		}
		else if(CAppSetting::Instance()->GetGroupNumber() == 2)
		{
			if(CDataManager::Instance()->IsConnected(CONN_PLC_2))
			{
				if(m_nCurScreen == 1) m_dlg_Motor.Update_PLC_Value();
				if(m_nCurScreen == 100) m_dlg_MotorStacker.Update_PLC_Value();
			}
		}
		else if(CAppSetting::Instance()->GetGroupNumber() == 3)
		{
			if(CDataManager::Instance()->IsConnected(CONN_PLC_3))
			{
				if(m_nCurScreen == 1) m_dlg_Motor.Update_PLC_Value();
				if(m_nCurScreen == 100) m_dlg_MotorStacker.Update_PLC_Value();
			}
		}
	}
	if(nIDEvent == TM_UPDATE_MANUAL)
	{
		if(CAppSetting::Instance()->GetGroupNumber() == 1)
		{
			if(CDataManager::Instance()->IsConnected(CONN_PLC_1))
				m_dlg_Manual.Update_PLC_Value();
		}
		else if(CAppSetting::Instance()->GetGroupNumber() == 2)
		{
			if(CDataManager::Instance()->IsConnected(CONN_PLC_2))
				m_dlg_Manual.Update_PLC_Value();
		}
		else if(CAppSetting::Instance()->GetGroupNumber() == 3)
		{
			if(CDataManager::Instance()->IsConnected(CONN_PLC_3))
				m_dlg_Manual.Update_PLC_Value();
		}
	}
	if(nIDEvent == TM_UPDATE_DEVICE)
	{
		if(CAppSetting::Instance()->GetGroupNumber() == 1)
		{
			if(CDataManager::Instance()->IsConnected(CONN_PLC_1))
				m_dlg_Device.Update_PLC_Value();
		}
		else if(CAppSetting::Instance()->GetGroupNumber() == 2)
		{
			if(CDataManager::Instance()->IsConnected(CONN_PLC_2))
				m_dlg_Device.Update_PLC_Value();
		}
		else if(CAppSetting::Instance()->GetGroupNumber() == 3)
		{
			if(CDataManager::Instance()->IsConnected(CONN_PLC_3))
				m_dlg_Device.Update_PLC_Value();
		}
	}
	if(nIDEvent == TM_UPDATE_IO)
	{
		if(CAppSetting::Instance()->GetGroupNumber() == 1)
		{
			if(CDataManager::Instance()->IsConnected(CONN_PLC_1))
				m_dlg_IO.Update_PLC_Value();
		}
		else if(CAppSetting::Instance()->GetGroupNumber() == 2)
		{
			if(CDataManager::Instance()->IsConnected(CONN_PLC_2))
				m_dlg_IO.Update_PLC_Value();
		}
		else if(CAppSetting::Instance()->GetGroupNumber() == 3)
		{
			if(CDataManager::Instance()->IsConnected(CONN_PLC_3))
				m_dlg_IO.Update_PLC_Value();
		}
	}
	if(nIDEvent == TM_UPDATE_ALARM)
	{
		if(CAppSetting::Instance()->GetGroupNumber() == 1)
		{
			if(CDataManager::Instance()->IsConnected(CONN_PLC_1))
				m_dlg_Alarm.Update_PLC_Value();
		}
		else if(CAppSetting::Instance()->GetGroupNumber() == 2)
		{
			if(CDataManager::Instance()->IsConnected(CONN_PLC_2))
				m_dlg_Alarm.Update_PLC_Value();
		}
		else if(CAppSetting::Instance()->GetGroupNumber() == 3)
		{
			if(CDataManager::Instance()->IsConnected(CONN_PLC_3))
				m_dlg_Alarm.Update_PLC_Value();
		}
	}

	CDialogEx::OnTimer(nIDEvent);
}

// MES Process 체크
void CAMKOR_MMIDlg::MES_LotProcessCheck()
{
	for(int i=0;i<TOTAL_LOT_COUNT;i++)
	{
		if(CDataManager::Instance()->m_arrayLOTINFO[i].m_bLive)
		{
			if(CDataManager::Instance()->m_arrayLOTINFO[i].m_nCurProcess == MES_ZEBRA_CODE_REQ)
			{
				if(CDataManager::Instance()->m_arrayLOTINFO[i].m_bSendMESMsg == FALSE)
				{
					// MES Zebra Req

					CDataManager::Instance()->m_arrayLOTINFO[i].m_bSendMESMsg = TRUE;
				}
				else
				{
					if(CDataManager::Instance()->m_arrayLOTINFO[i].m_bRecvMESMsg == TRUE)
					{
						CDataManager::Instance()->m_arrayLOTINFO[i].m_nCurProcess = MES_LOT_START_REQ;
						CDataManager::Instance()->m_arrayLOTINFO[i].m_bSendMESMsg = FALSE;
						CDataManager::Instance()->m_arrayLOTINFO[i].m_bRecvMESMsg = FALSE;
					}
				}
			}
			else if(CDataManager::Instance()->m_arrayLOTINFO[i].m_nCurProcess == MES_LOT_START_REQ)
			{
				if(CDataManager::Instance()->m_arrayLOTINFO[i].m_bSendMESMsg == FALSE)
				{
					// MES Lot Start Req

					CDataManager::Instance()->m_arrayLOTINFO[i].m_bSendMESMsg = TRUE;
				}
				else
				{
					if(CDataManager::Instance()->m_arrayLOTINFO[i].m_bRecvMESMsg == TRUE)
					{
						CDataManager::Instance()->m_arrayLOTINFO[i].m_nCurProcess = MES_LOT_START_END;
						CDataManager::Instance()->m_arrayLOTINFO[i].m_bSendMESMsg = FALSE;
						CDataManager::Instance()->m_arrayLOTINFO[i].m_bRecvMESMsg = FALSE;

						// PLC 에게 MES 완료 비트를 준다..

					}
				}
			}
			else if(CDataManager::Instance()->m_arrayLOTINFO[i].m_nCurProcess == MES_READY_TO_UNLOAD_REQ)
			{
				if(CDataManager::Instance()->m_arrayLOTINFO[i].m_bSendMESMsg == FALSE)
				{
					// MES Ready to Unload Req

					CDataManager::Instance()->m_arrayLOTINFO[i].m_bSendMESMsg = TRUE;
				}
				else
				{
					if(CDataManager::Instance()->m_arrayLOTINFO[i].m_bRecvMESMsg == TRUE)
					{
						CDataManager::Instance()->m_arrayLOTINFO[i].m_nCurProcess = MES_LOTEND_REQ;
						CDataManager::Instance()->m_arrayLOTINFO[i].m_bSendMESMsg = FALSE;
						CDataManager::Instance()->m_arrayLOTINFO[i].m_bRecvMESMsg = FALSE;
					}
				}
			}
			else if(CDataManager::Instance()->m_arrayLOTINFO[i].m_nCurProcess == MES_LOTEND_REQ)
			{
				if(CDataManager::Instance()->m_arrayLOTINFO[i].m_bSendMESMsg == FALSE)
				{
					// MES LotEnd Req

					CDataManager::Instance()->m_arrayLOTINFO[i].m_bSendMESMsg = TRUE;
				}
				else
				{
					if(CDataManager::Instance()->m_arrayLOTINFO[i].m_bRecvMESMsg == TRUE)
					{
						CDataManager::Instance()->m_arrayLOTINFO[i].m_nCurProcess = MES_MAX;
						CDataManager::Instance()->m_arrayLOTINFO[i].m_bSendMESMsg = FALSE;
						CDataManager::Instance()->m_arrayLOTINFO[i].m_bRecvMESMsg = FALSE;
					}
				}
			}
		}
	}
}

void CAMKOR_MMIDlg::MES_AlarmCheck()
{
	//m_sAlarmCode_G1[i] = 0;
	//m_sAlarmCode_G2[i] = 0;
	//m_sAlarmCode_G3[i] = 0;
	SHORT sAlarmCode = 0;
	if(CAppSetting::Instance()->GetGroupNumber() == 1)
	{
		if(CDataManager::Instance()->IsConnected(CONN_PLC_1))
		{
			for(int i=0;i<8;i++)
			{
				sAlarmCode = CDataManager::Instance()->m_PLCReaderD_Alarm_G1.GetValue(91+i);
				if(sAlarmCode > 0)
				{
					if(m_sAlarmCode_G1[i] == 0) // 알람 발생함 MES Alarm Set
					{
						m_dlg_Gem.AlarmReportSend(sAlarmCode, 1);
						m_sAlarmCode_G1[i] = sAlarmCode;
					}
				}
				else
				{
					if(m_sAlarmCode_G1[i] > 0) // 알람 없어짐 MES Alarm Clear
					{
						m_dlg_Gem.AlarmReportSend(m_sAlarmCode_G1[i], 0);
						m_sAlarmCode_G1[i] = 0;
					}
				}
			}
		}
		if(CDataManager::Instance()->IsConnected(CONN_PLC_2))
		{
			for(int i=0;i<8;i++)
			{
				sAlarmCode = CDataManager::Instance()->m_PLCReaderD_Alarm_G2.GetValue(91+i);
				if(sAlarmCode > 0)
				{
					if(m_sAlarmCode_G2[i] == 0) // 알람 발생함 MES Alarm Set
					{
						m_dlg_Gem.AlarmReportSend(sAlarmCode, 1);
						m_sAlarmCode_G2[i] = sAlarmCode;
					}
				}
				else
				{
					if(m_sAlarmCode_G2[i] > 0) // 알람 없어짐 MES Alarm Clear
					{
						m_dlg_Gem.AlarmReportSend(m_sAlarmCode_G2[i], 0);
						m_sAlarmCode_G2[i] = 0;
					}
				}
			}
		}
		if(CDataManager::Instance()->IsConnected(CONN_PLC_3))
		{
			for(int i=0;i<8;i++)
			{
				sAlarmCode = CDataManager::Instance()->m_PLCReaderD_Alarm_G3.GetValue(91+i);
				if(sAlarmCode > 0)
				{
					if(m_sAlarmCode_G3[i] == 0) // 알람 발생함 MES Alarm Set
					{
						m_dlg_Gem.AlarmReportSend(sAlarmCode, 1);
						m_sAlarmCode_G3[i] = sAlarmCode;
					}
				}
				else
				{
					if(m_sAlarmCode_G3[i] > 0) // 알람 없어짐 MES Alarm Clear
					{
						m_dlg_Gem.AlarmReportSend(m_sAlarmCode_G3[i], 0);
						m_sAlarmCode_G3[i] = 0;
					}
				}
			}
		}
	}
}

void CAMKOR_MMIDlg::Parse_DAddress_G1()
{
	int nPLCNum = CAppSetting::Instance()->GetGroupNumber();
	MADDRESS mAddressR, mAddressW;

	if(CDataManager::Instance()->IsConnected(CONN_PLC_1))
	{
		// Check Alarm - D 99 번지에 값이 있다면 알람 메세지 처리
		SHORT sAlarmCode = 0;
		sAlarmCode = CDataManager::Instance()->m_PLCReaderD_Alarm_G1.GetValue(99);
		if(sAlarmCode > 0)
		{
			CString strTemp;
			strTemp.Format(_T("%05d"), sAlarmCode);
			m_btn_ErrorCode.SetWindowText(strTemp);
			int nCode = _wtoi(strTemp);
			CAlarmListSetting::Instance()->GetErrorTextByCode(nCode, strTemp);
			m_btn_ErrorText.SetWindowText(strTemp);
		}
		else
		{
			m_btn_ErrorCode.SetWindowText(_T(""));
			m_btn_ErrorText.SetWindowText(_T(""));
		}
		// Check Alarm - D91~98 값을 가져온다. 값 비교후 알람 발생 및 알람 해제 MES 보고한다.
		MES_AlarmCheck();
		// 포지션별 체크 - Mapping & BCR Read
		mAddressR = CDataManager::Instance()->m_PLCReaderD_Process_G1.GetValueM(30100);
		mAddressW = CDataManager::Instance()->m_PLCReaderD_Process_G1.GetValueM(30120);
		if(mAddressR.m_xBit_1 == 1 && mAddressW.m_xBit_1 == 0) // BCR_Read Data보고
		{
			if(CDataManager::Instance()->m_MES_RFID.m_bLive == FALSE)
			{
				CString strRFID = CDataManager::Instance()->m_PLCReaderD_Process_G1.GetString(30105, 10);
				strRFID.Trim();
				wcscpy(CDataManager::Instance()->m_MES_RFID.m_strRFID, strRFID.GetBuffer());
				CDataManager::Instance()->m_MES_RFID.m_bLive = TRUE;
				// MES Send 처리 ///////
				// SendMES_ReadyToLoadBarcode(strRFID);
				CDataManager::Instance()->m_MES_RFID.m_bSend = TRUE;
				////////////////////////

				// MES Recv 처리 ///////
				if(strRFID == _T("RT0500001"))
				{
					wcscpy(CDataManager::Instance()->m_MES_RFID.m_strLOTID, _T("ARUBA_TESTLOT_01"));
					CDataManager::Instance()->m_MES_RFID.m_nDeviceIndex = 1;
					CDataManager::Instance()->m_MES_RFID.m_nTotalDeviceCount = 14*40;
					CDataManager::Instance()->m_MES_RFID.m_nBundleDeviceCount = 14*40;
					CDataManager::Instance()->m_MES_RFID.m_bRecv = TRUE;
				}
				else if(strRFID == _T("RT0500083"))
				{
					wcscpy(CDataManager::Instance()->m_MES_RFID.m_strLOTID, _T("ARUBA_TESTLOT_02"));
					CDataManager::Instance()->m_MES_RFID.m_nDeviceIndex = 5;
					CDataManager::Instance()->m_MES_RFID.m_nTotalDeviceCount = 14*40;
					CDataManager::Instance()->m_MES_RFID.m_nBundleDeviceCount = 14*40;
					CDataManager::Instance()->m_MES_RFID.m_bRecv = TRUE;
				}
				else if(strRFID == _T("RT0500078"))
				{
					wcscpy(CDataManager::Instance()->m_MES_RFID.m_strLOTID, _T("STD_TESTLOT_01"));
					CDataManager::Instance()->m_MES_RFID.m_nDeviceIndex = 2;
					CDataManager::Instance()->m_MES_RFID.m_nTotalDeviceCount = 14*40;
					CDataManager::Instance()->m_MES_RFID.m_nBundleDeviceCount = 14*40;
					CDataManager::Instance()->m_MES_RFID.m_bRecv = TRUE;
				}
				else if(strRFID == _T("RT0500085"))
				{
					wcscpy(CDataManager::Instance()->m_MES_RFID.m_strLOTID, _T("NVIDIA_TESTLOT_01"));
					CDataManager::Instance()->m_MES_RFID.m_nDeviceIndex = 3;
					CDataManager::Instance()->m_MES_RFID.m_nTotalDeviceCount = 8*12;
					CDataManager::Instance()->m_MES_RFID.m_nBundleDeviceCount = 8*12;
					CDataManager::Instance()->m_MES_RFID.m_bRecv = TRUE;
				}
				else if(strRFID == _T("RT0500076"))
				{
					wcscpy(CDataManager::Instance()->m_MES_RFID.m_strLOTID, _T("NVIDIA_TESTLOT_02"));
					CDataManager::Instance()->m_MES_RFID.m_nDeviceIndex = 6;
					CDataManager::Instance()->m_MES_RFID.m_nTotalDeviceCount = 14*12;
					CDataManager::Instance()->m_MES_RFID.m_nBundleDeviceCount = 14*12;
					CDataManager::Instance()->m_MES_RFID.m_bRecv = TRUE;
				}
				else if(strRFID == _T("RT0500084"))
				{
					wcscpy(CDataManager::Instance()->m_MES_RFID.m_strLOTID, _T("NVIDIA_TESTLOT_03"));
					CDataManager::Instance()->m_MES_RFID.m_nDeviceIndex = 7;
					CDataManager::Instance()->m_MES_RFID.m_nTotalDeviceCount = 14*12;
					CDataManager::Instance()->m_MES_RFID.m_nBundleDeviceCount = 14*12;
					CDataManager::Instance()->m_MES_RFID.m_bRecv = TRUE;
				}
				else if(strRFID == _T("RT0500051"))
				{
					wcscpy(CDataManager::Instance()->m_MES_RFID.m_strLOTID, _T("NVIDIA_TESTLOT_04"));
					CDataManager::Instance()->m_MES_RFID.m_nDeviceIndex = 8;
					CDataManager::Instance()->m_MES_RFID.m_nTotalDeviceCount = 11*12;
					CDataManager::Instance()->m_MES_RFID.m_nBundleDeviceCount = 11*12;
					CDataManager::Instance()->m_MES_RFID.m_bRecv = TRUE;
				}
				else if(strRFID == _T("RT0500003"))
				{
					wcscpy(CDataManager::Instance()->m_MES_RFID.m_strLOTID, _T("AUTO_TESTLOT_01"));
					CDataManager::Instance()->m_MES_RFID.m_nDeviceIndex = 4;
					CDataManager::Instance()->m_MES_RFID.m_nTotalDeviceCount = 11*12;
					CDataManager::Instance()->m_MES_RFID.m_nBundleDeviceCount = 11*12;
					CDataManager::Instance()->m_MES_RFID.m_bRecv = TRUE;
				}				
				////////////////////////
			}
			else
			{
				if(CDataManager::Instance()->m_MES_RFID.m_bSend && CDataManager::Instance()->m_MES_RFID.m_bRecv)
				{
					// Device Index번호
					CDataManager::Instance()->PLC_Write_D(nPLCNum, 30122, CDataManager::Instance()->m_MES_RFID.m_nDeviceIndex);
					// LOT 총 Device갯수 (칩)
					CDataManager::Instance()->PLC_Write_D(nPLCNum, 30123, CDataManager::Instance()->m_MES_RFID.m_nTotalDeviceCount);
					// Bundle 총 Device갯수 (칩)
					CDataManager::Instance()->PLC_Write_D(nPLCNum, 30124, CDataManager::Instance()->m_MES_RFID.m_nBundleDeviceCount);	
					// LOT ID 입력
					CDataManager::Instance()->PLC_Write_D2(nPLCNum, 30130, CDataManager::Instance()->m_MES_RFID.m_strLOTID, 20);

					// 값 넣은 후 완료 비트 살린다.
					mAddressW.m_xBit_1 = 1;
					CDataManager::Instance()->PLC_Write_D(nPLCNum, 30120, (SHORT*)&mAddressW, 1);

					// MES 구조체 -> LOT 등록
					if(CDataManager::Instance()->IsExistLotInfo(CDataManager::Instance()->m_MES_RFID.m_strLOTID, CDataManager::Instance()->m_MES_RFID.m_strRFID) == FALSE)
					{
						CDataManager::Instance()->AddLotInfo(CDataManager::Instance()->m_MES_RFID);
					}
					else
					{
						// LOG 생성
						GetLog()->Debug(_T("Loading RFID BCR Read : Already Exist Lot[%s],Rfid[%s]"), CDataManager::Instance()->m_MES_RFID.m_strLOTID, CDataManager::Instance()->m_MES_RFID.m_strRFID);
						CLogList::Instance()->Add_LogData(eLog_DEBUG, _T("Loading RFID BCR Read : Already Exist Lot[%s],Rfid[%s]"), CDataManager::Instance()->m_MES_RFID.m_strLOTID, CDataManager::Instance()->m_MES_RFID.m_strRFID);
					}

					// MES 구조체 리셋
					ZeroMemory(&CDataManager::Instance()->m_MES_RFID,sizeof(stMESInfo_READY_TO_LOAD_BARCODE));
				}
			}
		}
		// 포지션별 체크 - 1G 라벨
		mAddressR = CDataManager::Instance()->m_PLCReaderD_Process_G1.GetValueM(30300);
		mAddressW = CDataManager::Instance()->m_PLCReaderD_Process_G1.GetValueM(30320);
		if(mAddressR.m_xBit_1 == 1 && mAddressW.m_xBit_1 == 0) // 라벨 출력 요청
		{
			// 임시 트레이 라벨
			m_dlg_Config.TestZPLPrint(CDataManager::Instance()->m_arrayTrayPackInfo[4].m_nDeviceIndex, 1);

			// 값 넣은 후 완료 비트 살린다.
			mAddressW.m_xBit_1 = 1;
			CDataManager::Instance()->PLC_Write_D(1, 30320, (SHORT*)&mAddressW, 1);
		}
		// CCTV 체크 - CHIP COUNT 캡쳐 요청 비트
		mAddressR = CDataManager::Instance()->m_PLCReaderD_Process_G1.GetValueM(30200);
		mAddressW = CDataManager::Instance()->m_PLCReaderD_Process_G1.GetValueM(30220);
		if(mAddressR.m_xBit_2 == 1 && mAddressW.m_xBit_2 == 0)
		{
			if(m_stSendCamTrigger[0].m_bLive == FALSE)
			{
				// Send Cam Trigger
				if(CDataManager::Instance()->IsConnected(CONN_HMS_G1) == TRUE)
				{
					if(Send_CamTrigger(3,0) == TRUE)
					{
						m_stSendCamTrigger[0].m_bLive = TRUE;
						m_stSendCamTrigger[0].m_nPos = 3;
						m_stSendCamTrigger[0].m_nSubPos = 0;
						m_stSendCamTrigger[0].m_nWaitTime = 0;
						m_stSendCamTrigger[0].m_nRetryCount = 0;
					}
					else
					{
						// LOG 생성
						GetLog()->Debug(_T("Send_CamTrigger FALSE [CHIP COUNT]"));
						CLogList::Instance()->Add_LogData(eLog_DEBUG, _T("Send_CamTrigger FALSE [CHIP COUNT]"));

						Clear_CamTriggerBit(3,0);
					}
				}
			}
			else
			{
				// LOG 생성
				GetLog()->Debug(_T("Send_CamTrigger m_bLive = TRUE [CHIP COUNT]"));
				CLogList::Instance()->Add_LogData(eLog_DEBUG, _T("Send_CamTrigger m_bLive = TRUE [CHIP COUNT]"));
			}
		}
		// CCTV 체크 - TRAY COVER 캡쳐 요청 비트
		mAddressR = CDataManager::Instance()->m_PLCReaderD_Process_G1.GetValueM(30200);
		mAddressW = CDataManager::Instance()->m_PLCReaderD_Process_G1.GetValueM(30220);
		if(mAddressR.m_xBit_4 == 1 && mAddressW.m_xBit_4 == 0)
		{
			if(m_stSendCamTrigger[1].m_bLive == FALSE)
			{
				// Send Cam Trigger
				if(CDataManager::Instance()->IsConnected(CONN_HMS_G1) == TRUE)
				{
					if(Send_CamTrigger(3,1) == TRUE)
					{
						m_stSendCamTrigger[1].m_bLive = TRUE;
						m_stSendCamTrigger[1].m_nPos = 3;
						m_stSendCamTrigger[1].m_nSubPos = 1;
						m_stSendCamTrigger[1].m_nWaitTime = 0;
						m_stSendCamTrigger[1].m_nRetryCount = 0;
					}
					else
					{
						// LOG 생성
						GetLog()->Debug(_T("Send_CamTrigger FALSE [TRAY COVER]"));
						CLogList::Instance()->Add_LogData(eLog_DEBUG, _T("Send_CamTrigger FALSE [TRAY COVER]"));

						Clear_CamTriggerBit(3,1);
					}
				}
			}
			else
			{
				// LOG 생성
				GetLog()->Debug(_T("Send_CamTrigger m_bLive = TRUE [TRAY COVER]"));
				CLogList::Instance()->Add_LogData(eLog_DEBUG, _T("Send_CamTrigger m_bLive = TRUE [TRAY COVER]"));
			}
		}
		// CCTV 체크 - BANDING 캡쳐 요청 비트
		mAddressR = CDataManager::Instance()->m_PLCReaderD_Process_G1.GetValueM(30350);
		mAddressW = CDataManager::Instance()->m_PLCReaderD_Process_G1.GetValueM(30370);
		if(mAddressR.m_xBit_2 == 1 && mAddressW.m_xBit_2 == 0)
		{
			if(m_stSendCamTrigger[2].m_bLive == FALSE)
			{
				// Send Cam Trigger
				if(CDataManager::Instance()->IsConnected(CONN_HMS_G1) == TRUE)
				{
					if(Send_CamTrigger(6,0) == TRUE)
					{
						m_stSendCamTrigger[2].m_bLive = TRUE;
						m_stSendCamTrigger[2].m_nPos = 6;
						m_stSendCamTrigger[2].m_nSubPos = 0;
						m_stSendCamTrigger[2].m_nWaitTime = 0;
						m_stSendCamTrigger[2].m_nRetryCount = 0;
					}
					else
					{
						// LOG 생성
						GetLog()->Debug(_T("Send_CamTrigger FALSE [BANDING]"));
						CLogList::Instance()->Add_LogData(eLog_DEBUG, _T("Send_CamTrigger FALSE [BANDING]"));

						Clear_CamTriggerBit(6,0);
					}
				}
			}
			else
			{
				// LOG 생성
				GetLog()->Debug(_T("Send_CamTrigger m_bLive = TRUE [BANDING]"));
				CLogList::Instance()->Add_LogData(eLog_DEBUG, _T("Send_CamTrigger m_bLive = TRUE [BANDING]"));
			}
		}
		// CCTV 체크 - HIC & DSC 요청 비트
		mAddressR = CDataManager::Instance()->m_PLCReaderD_Process_G1.GetValueM(30450);
		mAddressW = CDataManager::Instance()->m_PLCReaderD_Process_G1.GetValueM(30470);
		if(mAddressR.m_xBit_2 == 1 && mAddressW.m_xBit_2 == 0)
		{
			if(m_stSendCamTrigger[3].m_bLive == FALSE)
			{
				// Send Cam Trigger
				if(CDataManager::Instance()->IsConnected(CONN_HMS_G2) == TRUE)
				{
					if(Send_CamTrigger(8,0) == TRUE)
					{
						m_stSendCamTrigger[3].m_bLive = TRUE;
						m_stSendCamTrigger[3].m_nPos = 8;
						m_stSendCamTrigger[3].m_nSubPos = 0;
						m_stSendCamTrigger[3].m_nWaitTime = 0;
						m_stSendCamTrigger[3].m_nRetryCount = 0;
					}
					else
					{
						// LOG 생성
						GetLog()->Debug(_T("Send_CamTrigger FALSE [HIC/DSC]"));
						CLogList::Instance()->Add_LogData(eLog_DEBUG, _T("Send_CamTrigger FALSE [HIC/DSC]"));

						Clear_CamTriggerBit(8,0);
					}
				}
			}
			else
			{
				// LOG 생성
				GetLog()->Debug(_T("Send_CamTrigger m_bLive = TRUE [HIC/DSC]"));
				CLogList::Instance()->Add_LogData(eLog_DEBUG, _T("Send_CamTrigger m_bLive = TRUE [HIC/DSC]"));
			}
		}
	}
	else
	{
		m_btn_ErrorCode.SetWindowText(_T("00000"));
		m_btn_ErrorText.SetWindowText(_T("PLC DISCONNECTED"));
	}

	if(CDataManager::Instance()->IsConnected(CONN_PLC_2))
	{
		// CCTV 체크 - SEALING 요청 비트
		mAddressR = CDataManager::Instance()->m_PLCReaderD_Process_G2.GetValueM(30150);
		mAddressW = CDataManager::Instance()->m_PLCReaderD_Process_G2.GetValueM(30170);
		if(mAddressR.m_xBit_2 == 1 && mAddressW.m_xBit_2 == 0)
		{
			if(m_stSendCamTrigger[4].m_bLive == FALSE)
			{
				// Send Cam Trigger
				if(CDataManager::Instance()->IsConnected(CONN_HMS_G2) == TRUE)
				{
					if(Send_CamTrigger(10,0) == TRUE)
					{
						m_stSendCamTrigger[4].m_bLive = TRUE;
						m_stSendCamTrigger[4].m_nPos = 10;
						m_stSendCamTrigger[4].m_nSubPos = 0;
						m_stSendCamTrigger[4].m_nWaitTime = 0;
						m_stSendCamTrigger[4].m_nRetryCount = 0;
					}
					else
					{
						// LOG 생성
						GetLog()->Debug(_T("Send_CamTrigger FALSE [SEALING]"));
						CLogList::Instance()->Add_LogData(eLog_DEBUG, _T("Send_CamTrigger FALSE [SEALING]"));

						Clear_CamTriggerBit(10,0);
					}
				}
			}
			else
			{
				// LOG 생성
				GetLog()->Debug(_T("Send_CamTrigger m_bLive = TRUE [SEALING]"));
				CLogList::Instance()->Add_LogData(eLog_DEBUG, _T("Send_CamTrigger m_bLive = TRUE [SEALING]"));
			}
		}
	}
}

void CAMKOR_MMIDlg::Parse_DAddress_G2()
{
	if(CDataManager::Instance()->IsConnected(CONN_PLC_2))
	{
		// Check Alarm - D 91 번지에 값이 있다면 알람 메세지 처리
		SHORT sAlarmCode = 0;
		sAlarmCode = CDataManager::Instance()->m_PLCReaderD_Alarm_G2.GetValue(99);
		if(sAlarmCode > 0)
		{
			CString strTemp;
			strTemp.Format(_T("%05d"), sAlarmCode);
			m_btn_ErrorCode.SetWindowText(strTemp);
			int nCode = _wtoi(strTemp);
			CAlarmListSetting::Instance()->GetErrorTextByCode(nCode, strTemp);
			m_btn_ErrorText.SetWindowText(strTemp);
		}
		else
		{
			m_btn_ErrorCode.SetWindowText(_T(""));
			m_btn_ErrorText.SetWindowText(_T(""));
		}
		MADDRESS mAddressR, mAddressW;
		// 포지션별 체크 - 2G 라벨
		mAddressR = CDataManager::Instance()->m_PLCReaderD_Process_G2.GetValueM(30100);
		mAddressW = CDataManager::Instance()->m_PLCReaderD_Process_G2.GetValueM(30120);
		if(mAddressR.m_xBit_5 == 1 && mAddressW.m_xBit_5 == 0) // DN 라벨 출력 요청
		{
			// 임시 트레이 라벨
			m_dlg_Config.TestZPLPrint(CDataManager::Instance()->m_arrayTrayPackInfo[0].m_nDeviceIndex, 2);

			// 값 넣은 후 완료 비트 살린다.
			mAddressW.m_xBit_5 = 1;
			CDataManager::Instance()->PLC_Write_D(2, 30120, (SHORT*)&mAddressW, 1);
		}
		if(mAddressR.m_xBit_6 == 1 && mAddressW.m_xBit_6 == 0) // UP 라벨 출력 요청
		{
			// 임시 트레이 라벨
			m_dlg_Config.TestZPLPrint(CDataManager::Instance()->m_arrayTrayPackInfo[0].m_nDeviceIndex, 3);

			// 값 넣은 후 완료 비트 살린다.
			mAddressW.m_xBit_6 = 1;
			CDataManager::Instance()->PLC_Write_D(2, 30120, (SHORT*)&mAddressW, 1);
		}
	}
	else
	{
		m_btn_ErrorCode.SetWindowText(_T("00000"));
		m_btn_ErrorText.SetWindowText(_T("PLC DISCONNECTED"));
	}
}

void CAMKOR_MMIDlg::Parse_DAddress_G3()
{
	if(CDataManager::Instance()->IsConnected(CONN_PLC_3))
	{
		// Check Alarm - D 91 번지에 값이 있다면 알람 메세지 처리
		SHORT sAlarmCode = 0;
		sAlarmCode = CDataManager::Instance()->m_PLCReaderD_Alarm_G3.GetValue(99);
		if(sAlarmCode > 0)
		{
			CString strTemp;
			strTemp.Format(_T("%05d"), sAlarmCode);
			m_btn_ErrorCode.SetWindowText(strTemp);
			int nCode = _wtoi(strTemp);
			CAlarmListSetting::Instance()->GetErrorTextByCode(nCode, strTemp);
			m_btn_ErrorText.SetWindowText(strTemp);
		}
		else
		{
			m_btn_ErrorCode.SetWindowText(_T(""));
			m_btn_ErrorText.SetWindowText(_T(""));
		}
		MADDRESS mAddressR, mAddressW;
		// 포지션별 체크 - 3G 라벨
		mAddressR = CDataManager::Instance()->m_PLCReaderD_Process_G3.GetValueM(30250);
		mAddressW = CDataManager::Instance()->m_PLCReaderD_Process_G3.GetValueM(30270);
		if(mAddressR.m_xBit_5 == 1 && mAddressW.m_xBit_5 == 0) // RIGHT 라벨 출력 요청
		{
			// 임시 트레이 라벨
			m_dlg_Config.TestZPLPrint(CDataManager::Instance()->m_arrayTrayPackInfo[3].m_nDeviceIndex, 4);
			//m_dlg_Config.TestZPLPrint(3, 4);

			// 값 넣은 후 완료 비트 살린다.
			mAddressW.m_xBit_5 = 1;
			CDataManager::Instance()->PLC_Write_D(3, 30270, (SHORT*)&mAddressW, 1);
		}
		if(mAddressR.m_xBit_6 == 1 && mAddressW.m_xBit_6 == 0) // LEFT 라벨 출력 요청
		{
			// 임시 트레이 라벨
			m_dlg_Config.TestZPLPrint(CDataManager::Instance()->m_arrayTrayPackInfo[3].m_nDeviceIndex, 5);
			//m_dlg_Config.TestZPLPrint(3, 5);

			// 값 넣은 후 완료 비트 살린다.
			mAddressW.m_xBit_6 = 1;
			CDataManager::Instance()->PLC_Write_D(3, 30270, (SHORT*)&mAddressW, 1);
		}
	}
	else
	{
		m_btn_ErrorCode.SetWindowText(_T("00000"));
		m_btn_ErrorText.SetWindowText(_T("PLC DISCONNECTED"));
	}
}

void CAMKOR_MMIDlg::Update_UI()
{
	int nPLCNum = CAppSetting::Instance()->GetGroupNumber();
	// START BTN LAMP CHECK
	int nLAddress = 15;
	if(CDataManager::Instance()->PLC_Read_L2(nPLCNum, nLAddress))
	{
		if(m_btn_Start.GetOnOff() == FALSE) { m_btn_Start.SetOnOff(TRUE); m_btn_Start.Invalidate(); }
	}
	else 
	{
		if(m_btn_Start.GetOnOff() == TRUE) { m_btn_Start.SetOnOff(FALSE); m_btn_Start.Invalidate(); }
	}
	// STOP BTN LAMP CHECK
	nLAddress = 16;
	if(CDataManager::Instance()->PLC_Read_L2(nPLCNum, nLAddress))
	{
		if(m_btn_Stop.GetOnOff() == FALSE) { m_btn_Stop.SetOnOff(TRUE); m_btn_Stop.Invalidate(); }
	}
	else 
	{
		if(m_btn_Stop.GetOnOff() == TRUE) { m_btn_Stop.SetOnOff(FALSE); m_btn_Stop.Invalidate(); }
	}
	// RESET BTN LAMP CHECK
	nLAddress = 3;
	if(CDataManager::Instance()->PLC_Read_L2(nPLCNum, nLAddress))
	{
		if(m_btn_Reset.GetOnOff() == FALSE) { m_btn_Reset.SetOnOff(TRUE); m_btn_Reset.Invalidate(); }
	}
	else 
	{
		if(m_btn_Reset.GetOnOff() == TRUE) { m_btn_Reset.SetOnOff(FALSE); m_btn_Reset.Invalidate(); }
	}
	// BUZZER BTN LAMP CHECK
	nLAddress = 50;
	if(CDataManager::Instance()->PLC_Read_L2(nPLCNum, nLAddress))
	{
		if(m_btn_BuzOff.GetOnOff() == FALSE) { m_btn_BuzOff.SetOnOff(TRUE); m_btn_BuzOff.Invalidate(); }
	}
	else 
	{
		if(m_btn_BuzOff.GetOnOff() == TRUE) { m_btn_BuzOff.SetOnOff(FALSE); m_btn_BuzOff.Invalidate(); }
	}
	// AUTO MODE BTN LAMP CHECK
	int nMAddressBit = 10;
	if(CDataManager::Instance()->PLC_Read_M2(nPLCNum, nMAddressBit))
	{
		if(m_btn_AutoMode.GetOnOff() == FALSE) { m_btn_AutoMode.SetOnOff(TRUE); m_btn_AutoMode.Invalidate(); }
	}
	else 
	{
		if(m_btn_AutoMode.GetOnOff() == TRUE) { m_btn_AutoMode.SetOnOff(FALSE); m_btn_AutoMode.Invalidate(); }
	}
	// MANUAL MODE BTN LAMP CHECK
	nMAddressBit = 11;
	if(CDataManager::Instance()->PLC_Read_M2(nPLCNum, nMAddressBit))
	{
		if(m_btn_ManualMode.GetOnOff() == FALSE) { m_btn_ManualMode.SetOnOff(TRUE); m_btn_ManualMode.Invalidate(); }
	}
	else 
	{
		if(m_btn_ManualMode.GetOnOff() == TRUE) { m_btn_ManualMode.SetOnOff(FALSE); m_btn_ManualMode.Invalidate(); }
	}

	Update_UI_WorkInfo();
}

void CAMKOR_MMIDlg::Update_UI_WorkInfo()
{
	CString strValue;
	int nGroupNum = CAppSetting::Instance()->GetGroupNumber();
 
	if(nGroupNum == 1)
	{
		strValue.Format(_T("%d"), CDataManager::Instance()->m_stMCHINFO.m_stWorkInfo.m_nTotal_BOX_INPUT);
		m_st_V1.SetWindowText(strValue);
		strValue.Format(_T("%d"), CDataManager::Instance()->m_stMCHINFO.m_stWorkInfo.m_nTotal_BOX_OUTPUT);
		m_st_V2.SetWindowText(strValue);
		strValue.Format(_T("%d"), CDataManager::Instance()->m_stMCHINFO.m_stWorkInfo.m_nTotal_BOX_DELETE);
		m_st_V3.SetWindowText(strValue);
		if(CDataManager::Instance()->m_stMCHINFO.m_stWorkInfo.m_nTotal_BOX_INPUT > 0)
			strValue.Format(_T("%.2f"), 100.0f*((float)CDataManager::Instance()->m_stMCHINFO.m_stWorkInfo.m_nTotal_BOX_OUTPUT/(float)CDataManager::Instance()->m_stMCHINFO.m_stWorkInfo.m_nTotal_BOX_INPUT));
		else
			strValue.Format(_T("0.00"));
		m_st_V4.SetWindowText(strValue);
	}
}

BOOL CAMKOR_MMIDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message == WM_SYSKEYDOWN)
	{
		if(pMsg->wParam == VK_F4) 
			return TRUE;
	}

	if(pMsg->message == WM_KEYDOWN)
	{
		if( pMsg->wParam == VK_ESCAPE ||	
			pMsg->wParam == VK_RETURN ||	
			pMsg->wParam == VK_SPACE  ||	
			pMsg->wParam == VK_CANCEL )     
			return TRUE;        
	} 

	return CDialogEx::PreTranslateMessage(pMsg);
}

BOOL CAMKOR_MMIDlg::OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	NMHDR *pNMHDR = (NMHDR*)lParam;	

	int nLAddress = 0;

	if( pNMHDR->code == TCN_SELCHANGE ) // Button Pressed
	{
		int nPLCNum = CAppSetting::Instance()->GetGroupNumber();
		switch(wParam)
		{
		case IDC_BTN_M_START: nLAddress = 5; CDataManager::Instance()->PLC_Write_L(nPLCNum, nLAddress, TRUE); break;
		case IDC_BTN_M_STOP: nLAddress = 6; CDataManager::Instance()->PLC_Write_L(nPLCNum, nLAddress, TRUE); break;
		case IDC_BTN_M_RESET: nLAddress = 3; CDataManager::Instance()->PLC_Write_L(nPLCNum, nLAddress, TRUE); break;
		//case IDC_BTN_M_BUZZEROFF: nLAddress = 50; CDataManager::Instance()->PLC_Write_L(nPLCNum, nLAddress, TRUE); break;
		case IDC_BTN_M_HOME: nLAddress = 4; CDataManager::Instance()->PLC_Write_L(nPLCNum, nLAddress, TRUE); break;
		case IDC_BTN_M_AUTOMODE: nLAddress = 0; CDataManager::Instance()->PLC_Write_L(nPLCNum, nLAddress, TRUE); break;
		case IDC_BTN_M_MANUALMODE: nLAddress = 1; CDataManager::Instance()->PLC_Write_L(nPLCNum, nLAddress, TRUE); break;
		}
	}
	if( pNMHDR->code == TCN_SELCHANGING ) // Button Released
	{
		int nPLCNum = CAppSetting::Instance()->GetGroupNumber();
		switch(wParam)
		{
		case IDC_BTN_M_START: nLAddress = 5; CDataManager::Instance()->PLC_Write_L(nPLCNum, nLAddress, FALSE); break;
		case IDC_BTN_M_STOP: nLAddress = 6; CDataManager::Instance()->PLC_Write_L(nPLCNum, nLAddress, FALSE); break;
		case IDC_BTN_M_RESET: nLAddress = 3; CDataManager::Instance()->PLC_Write_L(nPLCNum, nLAddress, FALSE); break;
		//case IDC_BTN_M_BUZZEROFF: nLAddress = 50; CDataManager::Instance()->PLC_Write_L(nPLCNum, nLAddress, FALSE); break;
		case IDC_BTN_M_HOME: nLAddress = 4; CDataManager::Instance()->PLC_Write_L(nPLCNum, nLAddress, FALSE); break;
		case IDC_BTN_M_AUTOMODE: nLAddress = 0; CDataManager::Instance()->PLC_Write_L(nPLCNum, nLAddress, FALSE); break;
		case IDC_BTN_M_MANUALMODE: nLAddress = 1; CDataManager::Instance()->PLC_Write_L(nPLCNum, nLAddress, FALSE); break;
		}
	}

	return CDialogEx::OnNotify(wParam, lParam, pResult);
}

BOOL CAMKOR_MMIDlg::StartPLCReadThread()
{
	if (m_hPLCReadThread)
		return FALSE;

	m_bTerminatePLCReadThread = FALSE;

	unsigned int nThreadID = 0;
	m_hPLCReadThread = (HANDLE)::_beginthreadex(0, 0, _OnPLCReadThread, (void*)this, 0, &nThreadID);

	return m_hPLCReadThread == NULL ? FALSE : TRUE;
}

void CAMKOR_MMIDlg::StopPLCReadThread()
{
	if (!m_hPLCReadThread)
		return;

	m_bTerminatePLCReadThread = TRUE;
	DWORD dwResult;
	dwResult = ::WaitForSingleObject(m_hPLCReadThread, 1000);
	if (dwResult != WAIT_OBJECT_0)
	{
		::TerminateThread(m_hPLCReadThread, 0);
	}

	::CloseHandle(m_hPLCReadThread);
	m_hPLCReadThread = NULL;
}

unsigned int __stdcall CAMKOR_MMIDlg::_OnPLCReadThread(void* pParam)
{
	CAMKOR_MMIDlg* pThis = (CAMKOR_MMIDlg*)pParam;
	pThis->OnPLCReadThread();
	TRACE( _T("PLCReadThread terminated...\n") );
	_endthreadex(0);
	return 0;
}

void CAMKOR_MMIDlg::OnPLCReadThread()
{
	BOOL bRet1 = TRUE;
	BOOL bRet2 = TRUE;
	BOOL bRet3 = TRUE;
	while (!m_bTerminatePLCReadThread)
	{
		if(CAppSetting::Instance()->GetGroupNumber() == 1)
		{
			if (CDataManager::Instance()->IsConnected(CONN_PLC_1))
			{
				bRet1 = CDataManager::Instance()->m_PLCReaderD_Alarm_G1.Read();
				bRet1 = CDataManager::Instance()->m_PLCReaderR_G1.Read();
				bRet1 = CDataManager::Instance()->m_PLCReaderD_Process_G1.Read();

				if(m_nCurScreen == 0) // Main Screen
				{
				}
				else if(m_nCurScreen == 1) // Motor
				{ 
					bRet1 = CDataManager::Instance()->m_PLCReaderD_Motor.Read();
					bRet1 = CDataManager::Instance()->m_PLCReaderL_Motor.Read();
				}
				else if(m_nCurScreen == 2) // Manual
				{ 
					bRet1 = CDataManager::Instance()->m_PLCReaderL_Manual.Read(); 
				}
				else if(m_nCurScreen == 3) // Device
				{ 
				}
				else if(m_nCurScreen == 4) // IO
				{
					bRet1 = CDataManager::Instance()->m_PLCReaderX.Read();
					bRet1 = CDataManager::Instance()->m_PLCReaderY.Read();
				}
				else if(m_nCurScreen == 5) // Alarm
				{ 
				}
				else if(m_nCurScreen == 6) // Config
				{ 
				}

				if(bRet1 == FALSE)
				{
					CDataManager::Instance()->SetConnect(CONN_PLC_1, FALSE);
					SendMessage(WM_UPDATE_UI_CONN, 0, 0);
				}
			}
			if (CDataManager::Instance()->IsConnected(CONN_PLC_2))
			{
				bRet2 = CDataManager::Instance()->m_PLCReaderD_Alarm_G2.Read();
				bRet2 = CDataManager::Instance()->m_PLCReaderR_G2.Read();
				bRet2 = CDataManager::Instance()->m_PLCReaderD_Process_G2.Read();
				if(bRet2 == FALSE)
				{
					CDataManager::Instance()->SetConnect(CONN_PLC_2, FALSE);
					SendMessage(WM_UPDATE_UI_CONN, 0, 0);
				}
			}
			if (CDataManager::Instance()->IsConnected(CONN_PLC_3))
			{
				bRet3 = CDataManager::Instance()->m_PLCReaderD_Alarm_G3.Read();
				bRet3 = CDataManager::Instance()->m_PLCReaderR_G3.Read();
				bRet3 = CDataManager::Instance()->m_PLCReaderD_Process_G3.Read();
				if(bRet3 == FALSE)
				{
					CDataManager::Instance()->SetConnect(CONN_PLC_3, FALSE);
					SendMessage(WM_UPDATE_UI_CONN, 0, 0);
				}
			}
		}
		else if(CAppSetting::Instance()->GetGroupNumber() == 2)
		{
			if (CDataManager::Instance()->IsConnected(CONN_PLC_2))
			{
				bRet2 = CDataManager::Instance()->m_PLCReaderD_Alarm_G2.Read();
				bRet2 = CDataManager::Instance()->m_PLCReaderR_G2.Read();
				bRet2 = CDataManager::Instance()->m_PLCReaderD_Process_G2.Read();

				if(m_nCurScreen == 0) // Main Screen
				{
				}
				else if(m_nCurScreen == 1) // Motor
				{ 
					bRet2 = CDataManager::Instance()->m_PLCReaderD_Motor.Read();
					bRet2 = CDataManager::Instance()->m_PLCReaderL_Motor.Read();
				}
				else if(m_nCurScreen == 2) // Manual
				{ 
					bRet2 = CDataManager::Instance()->m_PLCReaderL_Manual.Read(); 
				}
				else if(m_nCurScreen == 3) // Device
				{ 
				}
				else if(m_nCurScreen == 4) // IO
				{
					bRet2 = CDataManager::Instance()->m_PLCReaderX.Read();
					bRet2 = CDataManager::Instance()->m_PLCReaderY.Read();
				}
				else if(m_nCurScreen == 5) // Alarm
				{ 
				}
				else if(m_nCurScreen == 6) // Config
				{ 
				}

				if(bRet2 == FALSE)
				{
					CDataManager::Instance()->SetConnect(CONN_PLC_2, FALSE);
					SendMessage(WM_UPDATE_UI_CONN, 0, 0);
				}
			}
			if (CDataManager::Instance()->IsConnected(CONN_PLC_1))
			{
				bRet1 = CDataManager::Instance()->m_PLCReaderD_Process_G1.Read();
				if(bRet1 == FALSE)
				{
					CDataManager::Instance()->SetConnect(CONN_PLC_1, FALSE);
					SendMessage(WM_UPDATE_UI_CONN, 0, 0);
				}
			}
			if (CDataManager::Instance()->IsConnected(CONN_PLC_3))
			{
				bRet3 = CDataManager::Instance()->m_PLCReaderD_Process_G3.Read();
				if(bRet3 == FALSE)
				{
					CDataManager::Instance()->SetConnect(CONN_PLC_3, FALSE);
					SendMessage(WM_UPDATE_UI_CONN, 0, 0);
				}
			}
		}
		else if(CAppSetting::Instance()->GetGroupNumber() == 3)
		{
			if (CDataManager::Instance()->IsConnected(CONN_PLC_3))
			{
				bRet3 = CDataManager::Instance()->m_PLCReaderD_Alarm_G3.Read();
				bRet3 = CDataManager::Instance()->m_PLCReaderR_G3.Read();
				bRet3 = CDataManager::Instance()->m_PLCReaderD_Process_G3.Read();

				if(m_nCurScreen == 0) // Main Screen
				{
				}
				else if(m_nCurScreen == 1) // Motor
				{ 
					bRet3 = CDataManager::Instance()->m_PLCReaderD_Motor.Read();
					bRet3 = CDataManager::Instance()->m_PLCReaderL_Motor.Read();
				}
				else if(m_nCurScreen == 2) // Manual
				{ 
					bRet3 = CDataManager::Instance()->m_PLCReaderL_Manual.Read(); 
				}
				else if(m_nCurScreen == 3) // Device
				{ 
				}
				else if(m_nCurScreen == 4) // IO
				{
					bRet3 = CDataManager::Instance()->m_PLCReaderX.Read();
					bRet3 = CDataManager::Instance()->m_PLCReaderY.Read();
				}
				else if(m_nCurScreen == 5) // Alarm
				{ 
				}
				else if(m_nCurScreen == 6) // Config
				{ 
				}

				if(bRet3 == FALSE)
				{
					CDataManager::Instance()->SetConnect(CONN_PLC_3, FALSE);
					SendMessage(WM_UPDATE_UI_CONN, 0, 0);
				}
			}
			if (CDataManager::Instance()->IsConnected(CONN_PLC_1))
			{
				bRet1 = CDataManager::Instance()->m_PLCReaderD_Process_G1.Read();
				if(bRet1 == FALSE)
				{
					CDataManager::Instance()->SetConnect(CONN_PLC_1, FALSE);
					SendMessage(WM_UPDATE_UI_CONN, 0, 0);
				}
			}
			if (CDataManager::Instance()->IsConnected(CONN_PLC_2))
			{
				bRet2 = CDataManager::Instance()->m_PLCReaderD_Process_G2.Read();
				if(bRet2 == FALSE)
				{
					CDataManager::Instance()->SetConnect(CONN_PLC_2, FALSE);
					SendMessage(WM_UPDATE_UI_CONN, 0, 0);
				}
			}
		}

		Sleep(100);
	}
}

BOOL CAMKOR_MMIDlg::Send_CamTrigger(int nPos, int nSubPos)
{
	if(nPos < 1 || nPos > 16 ) return FALSE;

	//LOT_INFO stLotInfo;
	int nBoxNum = 0;
	int nRet = 0;
	SYSTEMTIME st;
	GetLocalTime(&st);
	CString strTriggerCreateTime, strDate, strTime;
	strTriggerCreateTime.Format(_T("%04d%02d%02d%02d%02d%02d"), st.wYear, st.wMonth, st.wDay, st.wHour, st.wMinute, st.wSecond);
	strDate.Format(_T("%04d%02d%02d"), st.wYear, st.wMonth, st.wDay);
	strTime.Format(_T("%02d%02d%02d"), st.wHour, st.wMinute, st.wSecond);

	CString strSend, strSize;
	strSend.Format(_T("01CT0074|0|%s|%s|%s|%d|%02d|%02d|%s|"), _T("20181029"), _T("000000"), _T("TEST_LOT"), nBoxNum, nPos, nSubPos, strTriggerCreateTime);
	strSize.Format(_T("01CT%04d"), strSend.GetLength());
	strSend.Replace(_T("01CT0074"), strSize);
		
	if(nPos == 3 || nPos == 6) nRet = m_ListenSocket.SendData(_T("127.0.0.1"), strSend);
	else nRet = m_ListenSocket.SendData(_T("192.168.0.92"), strSend);

	// LOG 남기기
	GetLog()->DebugT(strSend);
	CLogList::Instance()->Add_LogData(eLog_DEBUG, strSend);

	//if(CDataManager::Instance()->GetLotInfoByPos(nPos, stLotInfo, nBoxNum) == TRUE)
	//{
	//	// Cam Trigger Format 
	//	// Create Date 8
	//	// Create Time 6
	//	// LotId 25
	//	// Box Num 1
	//	// Pos	2
	//	// nSubPos 2
	//	// Trigger Send Time 14
	//	CString strSend, strSize;
	//	strSend.Format(_T("01CT0074|0|%s|%s|%s|%d|%02d|%02d|%s|"), stLotInfo.m_strCreateDate, stLotInfo.m_strCreateTime, stLotInfo.m_strLOTID, nBoxNum, nPos, nSubPos, strTriggerCreateTime);
	//	strSize.Format(_T("01CT%04d"), strSend.GetLength());
	//	strSend.Replace(_T("01CT0074"), strSize);
	//	
	//	//int nRet = m_ListenSocket.SendData(m_dlg_GemScreen.m_strHMSIP, strSend);

	//	// LOG 남기기
	//	GetLog()->DebugT(strSend);
	//	CLogList::Instance()->Add_LogData(eLog_DEBUG, strSend);
	//}
	//else
	//{
	//	// LOG 남기기
	//	GetLog()->Debug( _T("Send_CamTrigger -> [%s] 위치에 자재가 존재하지 않습니다."), STR_POS_NAME[nPos-1] );
	//	CLogList::Instance()->Add_LogData(eLog_DEBUG, _T("Send_CamTrigger -> [%s] 위치에 자재가 존재하지 않습니다."), STR_POS_NAME[nPos-1] );

	//	return FALSE;
	//}

	return TRUE;
}

// CCTV Trigger Bit Clear
BOOL CAMKOR_MMIDlg::Clear_CamTriggerBit(int nPos, int nSubPos)
{
	int nPLCNum = CAppSetting::Instance()->GetGroupNumber();
	MADDRESS mAddressR, mAddressW;
	if(nPLCNum == 1)
	{
		if(nPos == 3)
		{
			if(nSubPos == 0)
			{
				if(CDataManager::Instance()->IsConnected(CONN_PLC_1))
				{
					// CCTV 체크 - CHIP COUNT 캡쳐 요청 비트
					mAddressR = CDataManager::Instance()->m_PLCReaderD_Process_G1.GetValueM(30200);
					mAddressW = CDataManager::Instance()->m_PLCReaderD_Process_G1.GetValueM(30220);

					// 값 넣은 후 완료 비트 살린다.
					mAddressW.m_xBit_2 = 1;
					CDataManager::Instance()->PLC_Write_D(1, 30220, (SHORT*)&mAddressW, 1);

					m_stSendCamTrigger[0].m_bLive = FALSE;
					m_stSendCamTrigger[0].m_nWaitTime = 0;
					m_stSendCamTrigger[0].m_nRetryCount = 0;
					m_stSendCamTrigger[0].m_nPos = 0;
					m_stSendCamTrigger[0].m_nSubPos = 0;

					return TRUE;
				}
			}
			else if(nSubPos == 1)
			{
				if(CDataManager::Instance()->IsConnected(CONN_PLC_1))
				{
					// CCTV 체크 - TRAY COVER 캡쳐 요청 비트
					mAddressR = CDataManager::Instance()->m_PLCReaderD_Process_G1.GetValueM(30200);
					mAddressW = CDataManager::Instance()->m_PLCReaderD_Process_G1.GetValueM(30220);

					// 값 넣은 후 완료 비트 살린다.
					mAddressW.m_xBit_4 = 1;
					CDataManager::Instance()->PLC_Write_D(1, 30220, (SHORT*)&mAddressW, 1);

					m_stSendCamTrigger[1].m_bLive = FALSE;
					m_stSendCamTrigger[1].m_nWaitTime = 0;
					m_stSendCamTrigger[1].m_nRetryCount = 0;
					m_stSendCamTrigger[1].m_nPos = 0;
					m_stSendCamTrigger[1].m_nSubPos = 0;

					return TRUE;
				}
			}
		}
		if(nPos == 6)
		{
			if(nSubPos == 0)
			{
				if(CDataManager::Instance()->IsConnected(CONN_PLC_1))
				{
					// CCTV 체크 - BANDING 캡쳐 요청 비트
					mAddressR = CDataManager::Instance()->m_PLCReaderD_Process_G1.GetValueM(30350);
					mAddressW = CDataManager::Instance()->m_PLCReaderD_Process_G1.GetValueM(30370);

					// 값 넣은 후 완료 비트 살린다.
					mAddressW.m_xBit_2 = 1;
					CDataManager::Instance()->PLC_Write_D(1, 30370, (SHORT*)&mAddressW, 1);

					m_stSendCamTrigger[2].m_bLive = FALSE;
					m_stSendCamTrigger[2].m_nWaitTime = 0;
					m_stSendCamTrigger[2].m_nRetryCount = 0;
					m_stSendCamTrigger[2].m_nPos = 0;
					m_stSendCamTrigger[2].m_nSubPos = 0;

					return TRUE;
				}
			}
		}
		if(nPos == 8)
		{
			if(nSubPos == 0)
			{
				if(CDataManager::Instance()->IsConnected(CONN_PLC_1))
				{
					// CCTV 체크 - BANDING 캡쳐 요청 비트
					mAddressR = CDataManager::Instance()->m_PLCReaderD_Process_G1.GetValueM(30450);
					mAddressW = CDataManager::Instance()->m_PLCReaderD_Process_G1.GetValueM(30470);

					// 값 넣은 후 완료 비트 살린다.
					mAddressW.m_xBit_2 = 1;
					CDataManager::Instance()->PLC_Write_D(1, 30470, (SHORT*)&mAddressW, 1);

					m_stSendCamTrigger[3].m_bLive = FALSE;
					m_stSendCamTrigger[3].m_nWaitTime = 0;
					m_stSendCamTrigger[3].m_nRetryCount = 0;
					m_stSendCamTrigger[3].m_nPos = 0;
					m_stSendCamTrigger[3].m_nSubPos = 0;

					return TRUE;
				}
			}
		}
		if(nPos == 10)
		{
			if(nSubPos == 0)
			{
				if(CDataManager::Instance()->IsConnected(CONN_PLC_2))
				{
					// CCTV 체크 - SEALING 캡쳐 요청 비트
					mAddressR = CDataManager::Instance()->m_PLCReaderD_Process_G2.GetValueM(30150);
					mAddressW = CDataManager::Instance()->m_PLCReaderD_Process_G2.GetValueM(30170);

					// 값 넣은 후 완료 비트 살린다.
					mAddressW.m_xBit_2 = 1;
					CDataManager::Instance()->PLC_Write_D(2, 30170, (SHORT*)&mAddressW, 1);

					m_stSendCamTrigger[4].m_bLive = FALSE;
					m_stSendCamTrigger[4].m_nWaitTime = 0;
					m_stSendCamTrigger[4].m_nRetryCount = 0;
					m_stSendCamTrigger[4].m_nPos = 0;
					m_stSendCamTrigger[4].m_nSubPos = 0;

					return TRUE;
				}
			}
		}
	}
	else if(nPLCNum == 2)
	{
	}
	else if(nPLCNum == 3)
	{
	}

	return FALSE;
}

BOOL CAMKOR_MMIDlg::RemoveFolderFile(LPCTSTR strPath)
{
	if(strPath == NULL) return FALSE;

	BOOL bReturnVal = FALSE;
	CString strNextDirPath = _T("");
	CString strRoot = _T("");

	// 해당 폴더의 모든 파일을 검사한다.
	strRoot.Format(_T("%s\\*.*"), strPath);
	CFileFind find;
	bReturnVal = find.FindFile(strRoot);
	if(bReturnVal == FALSE) // 폴더내 파일이 없다면...
	{
		return FALSE;
	}
	while(bReturnVal)
	{
		bReturnVal = find.FindNextFile();
		if(find.IsDots() == FALSE)
		{
			// 폴더인 경우 재귀호출로 처리..
			if(find.IsDirectory())
			{
				RemoveFolderFile(find.GetFilePath());
			}
			// 파일일 경우 삭제하자..
			else
			{
				bReturnVal = DeleteFile(find.GetFilePath());
			}
		}
	}
	find.Close();

	// 다시 자신 폴더를 제거하자..
	bReturnVal = RemoveDirectory(strPath);
	return bReturnVal;
}

BOOL CAMKOR_MMIDlg::RemoveFolderFileByElapsedDays(LPCTSTR strPath, int nDays)
{
	if(nDays <= 0) return FALSE;
	if(strPath == NULL) return FALSE;

	BOOL bReturnVal = FALSE;
	CString strNextDirPath = _T("");
	CString strRoot = _T("");

	CTime ctTime_Cur = CTime::GetCurrentTime();
	CTime ctTime_FolderFile;
	CTimeSpan ctTimeSpan;
	BOOL bDeleteFolderFile = FALSE;

	// 해당 폴더의 모든 파일을 검사한다.
	strRoot.Format(_T("%s\\*.*"), strPath);
	CFileFind find;
	bReturnVal = find.FindFile(strRoot);
	if(bReturnVal == FALSE) // 폴더내 파일이 없다면...
	{
		return FALSE;
	}
	while(bReturnVal)
	{
		bReturnVal = find.FindNextFile();
		if(find.IsDots() == FALSE)
		{
			find.GetCreationTime(ctTime_FolderFile);
			ctTimeSpan = ctTime_Cur - ctTime_FolderFile;
			if(ctTimeSpan.GetDays() > nDays)
			{
				// 폴더인 경우 재귀호출로 처리..
				if(find.IsDirectory())
				{
					RemoveFolderFile(find.GetFilePath());
				}
				// 파일일 경우 삭제하자..
				else
				{
					bReturnVal = DeleteFile(find.GetFilePath());
				}
			}
		}
	}
	find.Close();

	return TRUE;
}

void CAMKOR_MMIDlg::SocketServerStart()
{
	CString strLog = L"";
	int nRet = -1;
	
	if(m_ListenSocket.Create(4999, SOCK_STREAM)) //2015-11-18
	{
		if(!m_ListenSocket.Listen()) //2015-11-18
		{		
			nRet = GetLastError();
			strLog.Format(_T("ERROR: Listen() return FALSE : %d"), GetLastError());
		}
		else
		{
			strLog = L"Socket Server Start....";
		}
	}
	else
	{
		strLog.Format(_T("ERROR: Create() return FALSE : %d"), GetLastError());
	}

	GetLog()->DebugT(strLog);
	CLogList::Instance()->Add_LogData(eLog_DEBUG, strLog);
}

void CAMKOR_MMIDlg::SocketServerStop()
{
	POSITION pos;
	pos = m_ListenSocket.m_ptrClientSocketList.GetHeadPosition();
	CClientSocket* pClient = NULL;

	while(pos != NULL)
	{
		pClient = (CClientSocket*)m_ListenSocket.m_ptrClientSocketList.GetNext(pos);
		if(pClient != NULL)
		{
			pClient->ShutDown();
			pClient->Close();

			delete pClient;
		}
	}

	m_ListenSocket.ShutDown();
	m_ListenSocket.Close();

	GetLog()->DebugT(_T("Socket Server Stop...."));
	CLogList::Instance()->Add_LogData(eLog_DEBUG, _T("Socket Server Stop...."));
}

void CAMKOR_MMIDlg::OnBnClickedBtnM1()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	ChangeScreen(1);
	m_nCurScreen = 1;

	// LOG 남기기
	GetLog()->Debug( _T("MOTOR BTN PRESSED") );
	CLogList::Instance()->Add_LogData(eLog_DEBUG, _T("MOTOR BTN PRESSED") );
}


void CAMKOR_MMIDlg::OnBnClickedBtnM2()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	ChangeScreen(2);
	m_nCurScreen = 2;

	// LOG 남기기
	GetLog()->Debug( _T("MANUAL BTN PRESSED") );
	CLogList::Instance()->Add_LogData(eLog_DEBUG, _T("MANUAL BTN PRESSED") );
}


void CAMKOR_MMIDlg::OnBnClickedBtnM3()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	ChangeScreen(3);
	m_nCurScreen = 3;

	// LOG 남기기
	GetLog()->Debug( _T("DEVICE BTN PRESSED") );
	CLogList::Instance()->Add_LogData(eLog_DEBUG, _T("DEVICE BTN PRESSED") );
}


void CAMKOR_MMIDlg::OnBnClickedBtnM4()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	ChangeScreen(4);
	m_nCurScreen = 4;

	// LOG 남기기
	GetLog()->Debug( _T("I/O BTN PRESSED") );
	CLogList::Instance()->Add_LogData(eLog_DEBUG, _T("I/O BTN PRESSED") );
}


void CAMKOR_MMIDlg::OnBnClickedBtnM5()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	ChangeScreen(5);
	m_nCurScreen = 5;

	// LOG 남기기
	GetLog()->Debug( _T("ERROR BTN PRESSED") );
	CLogList::Instance()->Add_LogData(eLog_DEBUG, _T("ERROR BTN PRESSED") );
}

void CAMKOR_MMIDlg::OnBnClickedBtnM6()
{
	/*CDlg_TrayStacker dlg;
	if(dlg.DoModal() == IDOK)
	{
	}*/

	ChangeScreen(6);
	m_nCurScreen = 6;

	// LOG 남기기
	GetLog()->Debug( _T("CONFIG BTN PRESSED") );
	CLogList::Instance()->Add_LogData(eLog_DEBUG, _T("CONFIG BTN PRESSED") );
}

LRESULT CAMKOR_MMIDlg::OnScreenChange(WPARAM wParam, LPARAM lParam)
{
	int nScreenNum = (int)wParam;

	if(m_nCurScreen != nScreenNum)
	{
		ChangeScreen(nScreenNum);
		m_nCurScreen = nScreenNum;
	}

	return 0;
}

LRESULT CAMKOR_MMIDlg::OnConnUpdate(WPARAM wParam, LPARAM lParam)
{
	UpdateViewConnStatus();
	return 0;
}

void CAMKOR_MMIDlg::OnBnClickedBtnMExit()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CDlg_NumPad dlg;
	dlg.m_strCaption = _T("EXIT PROGRAM");
	dlg.m_bStringMode = TRUE;
	dlg.m_bPassword = TRUE;
	if(dlg.DoModal() == IDOK)
	{
		if(dlg.m_strValue.CompareNoCase(_T("1234")) == 0)
		{
			CDialogEx::OnOK();
		}
	}
}


void CAMKOR_MMIDlg::OnBnClickedBtnMAuth()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CDlg_Auth dlg;
	if(dlg.DoModal() == IDOK)
	{
		m_nAuthNumber = dlg.m_nAuthNumber;
		if(m_nAuthNumber == 0) m_btn_Auth.SetWindowText(_T("OPERATOR"));
		if(m_nAuthNumber == 1) m_btn_Auth.SetWindowText(_T("ENGINEER"));
		if(m_nAuthNumber == 2) m_btn_Auth.SetWindowText(_T("DEVELOPER"));
		m_btn_Auth.Invalidate();
	}
}


void CAMKOR_MMIDlg::OnBnClickedBtnMSysinfo()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_nCurSubScreen = 0;
	ChangeSubScreen(m_nCurSubScreen);
}


void CAMKOR_MMIDlg::OnBnClickedBtnMGem()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_nCurSubScreen = 1;
	ChangeSubScreen(m_nCurSubScreen);
}


void CAMKOR_MMIDlg::OnBnClickedBtnMBuzzeroff()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	int nPLCNum = CAppSetting::Instance()->GetGroupNumber();
	int nLAddress = 50;
	if(CDataManager::Instance()->PLC_Read_L2(nPLCNum, nLAddress))
	{
		CDataManager::Instance()->PLC_Write_L(nPLCNum, nLAddress, FALSE);
	}
	else
	{
		CDataManager::Instance()->PLC_Write_L(nPLCNum, nLAddress, TRUE);
	}
}


void CAMKOR_MMIDlg::OnBnClickedBtnMHome()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CAMKOR_MMIDlg::OnBnClickedBtnMAutomode()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}


void CAMKOR_MMIDlg::OnBnClickedBtnMManualmode()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}


void CAMKOR_MMIDlg::OnBnClickedBtnMResetcount()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CDlg_NumPad dlg;
	dlg.m_bStringMode = TRUE;
	if(dlg.DoModal() == IDOK)
	{
		if(dlg.m_strValue.CompareNoCase(_T("0000")) == 0)
		{
			ZeroMemory(&CDataManager::Instance()->m_stMCHINFO.m_stWorkInfo, sizeof(WORK_INFO));
			DB_UPDATE_WORKINFO();
			Update_UI_WorkInfo();
		}
	}
}

// UNICODE
CString CAMKOR_MMIDlg::GetExecuteDirectory()
{
	CString strFolderPath;

	// 현재 프로그램의 실행 파일이 있는 폴더명을 추출함
	::GetModuleFileName(NULL, strFolderPath.GetBuffer(MAX_PATH), MAX_PATH);
	strFolderPath.ReleaseBuffer();
	if (strFolderPath.Find('\\') != -1)
	{
		for (int i = strFolderPath.GetLength() - 1; i >= 0; i--) 
		{
			TCHAR ch = strFolderPath[i];
			strFolderPath.Delete(i);
			if (ch == '\\') break; 
		}
	}
	return strFolderPath;
}

CStringA CAMKOR_MMIDlg::GetExecuteDirectoryA()
{
	CStringA strFolderPath;

	// 현재 프로그램의 실행 파일이 있는 폴더명을 추출함
	::GetModuleFileNameA(NULL, strFolderPath.GetBuffer(MAX_PATH), MAX_PATH);
	strFolderPath.ReleaseBuffer();
	if (strFolderPath.Find('\\') != -1)
	{
		for (int i = strFolderPath.GetLength() - 1; i >= 0; i--) 
		{
			CHAR ch = strFolderPath[i];
			strFolderPath.Delete(i);
			if (ch == '\\') break; 
		}
	}
	return strFolderPath;
}

void CAMKOR_MMIDlg::SetADOSetting()
{
	CStringA strPath = GetExecuteDirectoryA();
	CStringA strDBPath;

	strDBPath.Empty();
	strDBPath = strPath;
	strDBPath += "\\Data\\";
	strDBPath += "WorkInfo.mdb";
	m_strConnect_WorkInfo.Format("Driver={Microsoft Access Driver (*.mdb)};Dbq=%s;DefaultDir=;Uid=Admin;Pwd=;", strDBPath);
}

BOOL CAMKOR_MMIDlg::DB_GET_WORKINFO()
{
	CADOClass	m_ADOClass;

	if (FALSE == m_ADOClass.Connect( m_strConnect_WorkInfo ))
	{
		// LOG 남기기
		GetLog()->Debug( _T("ADODB WORKINFO 연결에 실패하였습니다!") );
		CLogList::Instance()->Add_LogData(eLog_DEBUG, _T("ADODB WORKINFO 연결에 실패하였습니다!") );
		return FALSE;
	}

	CStringA varTemp;
	int nRSCount = 0;
	BOOL bTempBool = FALSE;
	CStringA strSQL;

	try
	{
		strSQL.Format("SELECT * FROM [WORKINFO] WHERE YEAR(CREATE) = YEAR(Date()) AND MONTH(CREATE) = MONTH(Date()) AND DAY(CREATE) = DAY(Date())");
		m_ADOClass.ExecuteQryRs( strSQL );

		nRSCount = m_ADOClass.GetRecordCount();

		// 해당 날짜 데이터가 없다면 새로 생성한다.
		if (nRSCount == 0)
		{
			strSQL.Empty();
			varTemp = "INSERT INTO [WORKINFO] (LOT_IN, LOT_OUT, LOT_DEL, BOX_IN, BOX_OUT, BOX_DEL, CREATE, LAST) ";
			strSQL += varTemp;

			varTemp.Format("VALUES (0,0,0,0,0,0,Now(),Now())");
			strSQL += varTemp;

			bTempBool = m_ADOClass.ExecuteQryRs( strSQL );
			if(!bTempBool)
			{
				// LOG 남기기
				GetLog()->Debug( _T("ADODB DB_GET_WORKINFO QUERY 실패!") );
				CLogList::Instance()->Add_LogData(eLog_DEBUG, _T("ADODB DB_GET_WORKINFO QUERY 실패!") );
			}

			m_ADOClass.DisConnect();
			return bTempBool;
		}
		else
		{
			m_ADOClass.GetRsDataInt(L"LOT_IN", CDataManager::Instance()->m_stMCHINFO.m_stWorkInfo.m_nTotal_LOT_INPUT);
			m_ADOClass.GetRsDataInt(L"LOT_OUT", CDataManager::Instance()->m_stMCHINFO.m_stWorkInfo.m_nTotal_LOT_OUTPUT);
			m_ADOClass.GetRsDataInt(L"BOX_IN", CDataManager::Instance()->m_stMCHINFO.m_stWorkInfo.m_nTotal_BOX_INPUT);
			m_ADOClass.GetRsDataInt(L"BOX_OUT", CDataManager::Instance()->m_stMCHINFO.m_stWorkInfo.m_nTotal_BOX_OUTPUT);
			m_ADOClass.GetRsDataInt(L"BOX_DEL", CDataManager::Instance()->m_stMCHINFO.m_stWorkInfo.m_nTotal_BOX_DELETE);
		}

		m_ADOClass.DisConnect();
	}
	catch(_com_error &e)
	{
		m_ADOClass.DisConnect();

		_bstr_t btSource(e.Source());
		_bstr_t btDescription(e.Description());
		CString strMsg;
		strMsg.Format(_T("DATE= %s, %08lx= %s, Source= %s, Description= %s")
			,CTime::GetCurrentTime().Format(L"%Y-%m-%d %H:%M:%S")
			,e.Error(),e.ErrorMessage()
			,(LPCSTR)btSource
			,(LPCSTR)btDescription);
		// LOG 남기기
		GetLog()->Debug(_T("%s"), strMsg);
		CLogList::Instance()->Add_LogData(eLog_DEBUG, _T("%s"), strMsg);

		return FALSE;

	}
	return TRUE;
}

BOOL CAMKOR_MMIDlg::DB_UPDATE_WORKINFO()
{
	CADOClass	m_ADOClass;

	if (FALSE == m_ADOClass.Connect( m_strConnect_WorkInfo ))
	{
		// LOG 남기기
		GetLog()->Debug( _T("ADODB WORKINFO 연결에 실패하였습니다!") );
		CLogList::Instance()->Add_LogData(eLog_DEBUG, _T("ADODB WORKINFO 연결에 실패하였습니다!") );
		return FALSE;
	}

	CStringA varTemp;
	int nRSCount = 0;
	BOOL bTempBool = FALSE;
	CStringA strSQL;

	try
	{
		strSQL.Format("UPDATE [WORKINFO] SET ");
		varTemp.Format("LOT_IN = %d, ", CDataManager::Instance()->m_stMCHINFO.m_stWorkInfo.m_nTotal_LOT_INPUT);
		strSQL += varTemp;
		varTemp.Format("LOT_OUT = %d, ", CDataManager::Instance()->m_stMCHINFO.m_stWorkInfo.m_nTotal_LOT_OUTPUT);
		strSQL += varTemp;
		varTemp.Format("BOX_IN = %d, ", CDataManager::Instance()->m_stMCHINFO.m_stWorkInfo.m_nTotal_BOX_INPUT);
		strSQL += varTemp;
		varTemp.Format("BOX_OUT = %d, ", CDataManager::Instance()->m_stMCHINFO.m_stWorkInfo.m_nTotal_BOX_OUTPUT);
		strSQL += varTemp;
		varTemp.Format("BOX_DEL = %d, ", CDataManager::Instance()->m_stMCHINFO.m_stWorkInfo.m_nTotal_BOX_DELETE);
		strSQL += varTemp;
		varTemp.Format("LAST = Now() WHERE YEAR(CREATE) = YEAR(Date()) AND MONTH(CREATE) = MONTH(Date()) AND DAY(CREATE) = DAY(Date())");
		strSQL += varTemp;

		bTempBool = m_ADOClass.ExecuteQryRs( strSQL );
		if(!bTempBool)
		{
			// LOG 남기기
			GetLog()->Debug( _T("ADODB DB_UPDATE_WORKINFO QUERY 실패!") );
			CLogList::Instance()->Add_LogData(eLog_DEBUG, _T("ADODB DB_UPDATE_WORKINFO QUERY 실패!") );
		}

		m_ADOClass.DisConnect();
	}
	catch(_com_error &e)
	{
		m_ADOClass.DisConnect();

		_bstr_t btSource(e.Source());
		_bstr_t btDescription(e.Description());
		CString strMsg;
		strMsg.Format(_T("DATE= %s, %08lx= %s, Source= %s, Description= %s")
			,CTime::GetCurrentTime().Format(L"%Y-%m-%d %H:%M:%S")
			,e.Error(),e.ErrorMessage()
			,(LPCSTR)btSource
			,(LPCSTR)btDescription);
		// LOG 남기기
		GetLog()->Debug(_T("%s"), strMsg);
		CLogList::Instance()->Add_LogData(eLog_DEBUG, _T("%s"), strMsg);

		return FALSE;

	}
	return bTempBool;
}

BOOL CAMKOR_MMIDlg::DB_CREATE_LOTINFO(stLotInfo &LotInfo)
{
	CADOClass	m_ADOClass;

	if (FALSE == m_ADOClass.Connect( m_strConnect_WorkInfo ))
	{
		// LOG 남기기
		GetLog()->Debug( _T("ADODB WORKINFO 연결에 실패하였습니다!") );
		CLogList::Instance()->Add_LogData(eLog_DEBUG, _T("ADODB WORKINFO 연결에 실패하였습니다!") );
		return FALSE;
	}

	CStringA varTemp;
	int nRSCount = 0;
	BOOL bTempBool = FALSE;
	CStringA strSQL;

	try
	{
		strSQL.Empty();
		varTemp = "INSERT INTO [LOTINFO] (LOTID, RECIPE_INDEX, TOTAL_DEVICE_COUNT, BOX_IN_DEVICE_QTY, TOTAL_BOX_COUNT, MAKE_BOX_COUNT,";
		strSQL += varTemp;
		varTemp = "OUTPUT_BOX_COUNT, DELETE_BOX_COUNT, CUR_MAKED_BOXID, IS_MAKE_LAST_BOXID, CUR_PROCESS, INPUT_BUNDLE_COUNT,";
		strSQL += varTemp;
		varTemp = "BUNDLE_RFID_1, BUNDLE_RFID_2, BUNDLE_RFID_3, BUNDLE_RFID_4, BUNDLE_RFID_5, BUNDLE_RFID_6, BUNDLE_RFID_7, BUNDLE_RFID_8, BUNDLE_RFID_9, BUNDLE_RFID_10,";
		strSQL += varTemp;
		varTemp = "BUNDLE_DEVICE_1, BUNDLE_DEVICE_2, BUNDLE_DEVICE_3, BUNDLE_DEVICE_4, BUNDLE_DEVICE_5, BUNDLE_DEVICE_6, BUNDLE_DEVICE_7, BUNDLE_DEVICE_8, BUNDLE_DEVICE_9, BUNDLE_DEVICE_10,";
		strSQL += varTemp;
		varTemp = "CHIP_OCR_1, CHIP_OCR_2, CHIP_OCR_3, CHIP_OCR_4, CHIP_OCR_5, IS_COMPLETE, IS_FORCE_DELETED, CREATE, LAST)";
		strSQL += varTemp;
		varTemp.Format("VALUES (");
		strSQL += varTemp;
		varTemp.Format("%s, %d, %d,", LotInfo.m_strLOTID, LotInfo.m_nDeviceIndex, LotInfo.m_nTotalDeviceCount);
		strSQL += varTemp;
		varTemp.Format("%d, %d, %d,", LotInfo.m_nBoxInDeviceQty, LotInfo.m_nTotalBoxCount, LotInfo.m_nMakeBoxCount);
		strSQL += varTemp;
		varTemp.Format("%d, %d, %d,", LotInfo.m_nOutputBoxCount, LotInfo.m_nDeleteBoxCount, LotInfo.m_nCurMakeBoxID);
		strSQL += varTemp;
		varTemp.Format("%d, %d, %d,", LotInfo.m_bLastBoxIDMake, LotInfo.m_nCurProcess, LotInfo.m_nInputBundleCount);
		strSQL += varTemp;
		varTemp.Format("%s, %s, %s, %s, %s,", LotInfo.m_strRFID[0], LotInfo.m_strRFID[1], LotInfo.m_strRFID[2], LotInfo.m_strRFID[3], LotInfo.m_strRFID[4]);
		strSQL += varTemp;
		varTemp.Format("%s, %s, %s, %s, %s,", LotInfo.m_strRFID[5], LotInfo.m_strRFID[6], LotInfo.m_strRFID[7], LotInfo.m_strRFID[8], LotInfo.m_strRFID[9]);
		strSQL += varTemp;
		varTemp.Format("%d, %d, %d, %d, %d,", LotInfo.m_nBundleDeviceCount[0], LotInfo.m_nBundleDeviceCount[1], LotInfo.m_nBundleDeviceCount[2], LotInfo.m_nBundleDeviceCount[3], LotInfo.m_nBundleDeviceCount[4]);
		strSQL += varTemp;
		varTemp.Format("%d, %d, %d, %d, %d,", LotInfo.m_nBundleDeviceCount[5], LotInfo.m_nBundleDeviceCount[6], LotInfo.m_nBundleDeviceCount[7], LotInfo.m_nBundleDeviceCount[8], LotInfo.m_nBundleDeviceCount[9]);
		strSQL += varTemp;
		varTemp.Format("%s, %s, %s, %s, %s,", LotInfo.m_strChipOCR[0], LotInfo.m_strChipOCR[1], LotInfo.m_strChipOCR[2], LotInfo.m_strChipOCR[3], LotInfo.m_strChipOCR[4]);
		strSQL += varTemp;
		varTemp.Format("0, 0, Now(),Now())");
		strSQL += varTemp;

		bTempBool = m_ADOClass.ExecuteQryRs( strSQL );
		if(!bTempBool)
		{
			// LOG 남기기
			GetLog()->Debug( _T("ADODB DB_CREATE_LOTINFO QUERY 실패!") );
			CLogList::Instance()->Add_LogData(eLog_DEBUG, _T("ADODB DB_CREATE_LOTINFO QUERY 실패!") );
		}

		m_ADOClass.DisConnect();
	}
	catch(_com_error &e)
	{
		m_ADOClass.DisConnect();

		_bstr_t btSource(e.Source());
		_bstr_t btDescription(e.Description());
		CString strMsg;
		strMsg.Format(_T("DATE= %s, %08lx= %s, Source= %s, Description= %s")
			,CTime::GetCurrentTime().Format(L"%Y-%m-%d %H:%M:%S")
			,e.Error(),e.ErrorMessage()
			,(LPCSTR)btSource
			,(LPCSTR)btDescription);
		// LOG 남기기
		GetLog()->Debug(_T("%s"), strMsg);
		CLogList::Instance()->Add_LogData(eLog_DEBUG, _T("%s"), strMsg);

		return FALSE;

	}
	return bTempBool;
}

BOOL CAMKOR_MMIDlg::DB_UPDATE_LOTINFO(stLotInfo &LotInfo)
{
	CADOClass	m_ADOClass;

	if (FALSE == m_ADOClass.Connect( m_strConnect_WorkInfo ))
	{
		// LOG 남기기
		GetLog()->Debug( _T("ADODB WORKINFO 연결에 실패하였습니다!") );
		CLogList::Instance()->Add_LogData(eLog_DEBUG, _T("ADODB WORKINFO 연결에 실패하였습니다!") );
		return FALSE;
	}

	CStringA varTemp;
	int nRSCount = 0;
	BOOL bTempBool = FALSE;
	CStringA strSQL;

	try
	{
		strSQL.Format("UPDATE [LOTINFO] SET ");
		varTemp.Format("MAKE_BOX_COUNT = %d, ", LotInfo.m_nMakeBoxCount);
		strSQL += varTemp;
		varTemp.Format("OUTPUT_BOX_COUNT = %d, ", LotInfo.m_nOutputBoxCount);
		strSQL += varTemp;
		varTemp.Format("DELETE_BOX_COUNT = %d, ", LotInfo.m_nDeleteBoxCount);
		strSQL += varTemp;
		varTemp.Format("CUR_MAKED_BOXID = %d, ", LotInfo.m_nCurMakeBoxID);
		strSQL += varTemp;
		varTemp.Format("IS_MAKE_LAST_BOXID = %d, ", LotInfo.m_bLastBoxIDMake);
		strSQL += varTemp;
		varTemp.Format("MAKE_BOX_COUNT = %d, ", LotInfo.m_nMakeBoxCount);
		strSQL += varTemp;
		varTemp.Format("OUTPUT_BOX_COUNT = %d, ", LotInfo.m_nOutputBoxCount);
		strSQL += varTemp;
		varTemp.Format("DELETE_BOX_COUNT = %d, ", LotInfo.m_nDeleteBoxCount);
		strSQL += varTemp;
		varTemp.Format("CUR_MAKED_BOXID = %d, ", LotInfo.m_nCurMakeBoxID);
		strSQL += varTemp;
		varTemp.Format("IS_MAKE_LAST_BOXID = %d, ", LotInfo.m_bLastBoxIDMake);
		strSQL += varTemp;
		varTemp.Format("CUR_PROCESS = %d, ", LotInfo.m_nCurProcess);
		strSQL += varTemp;
		varTemp.Format("INPUT_BUNDLE_COUNT = %d, ", LotInfo.m_nInputBundleCount);
		strSQL += varTemp;
		varTemp.Format("BUNDLE_RFID_1 = %s, BUNDLE_RFID_2 = %s, BUNDLE_RFID_3 = %s, BUNDLE_RFID_4 = %s, BUNDLE_RFID_5 = %s, ", LotInfo.m_strRFID[0],LotInfo.m_strRFID[1],LotInfo.m_strRFID[2],LotInfo.m_strRFID[3],LotInfo.m_strRFID[4]);
		strSQL += varTemp;
		varTemp.Format("BUNDLE_RFID_6 = %s, BUNDLE_RFID_7 = %s, BUNDLE_RFID_8 = %s, BUNDLE_RFID_9 = %s, BUNDLE_RFID_10 = %s, ", LotInfo.m_strRFID[5],LotInfo.m_strRFID[6],LotInfo.m_strRFID[7],LotInfo.m_strRFID[8],LotInfo.m_strRFID[9]);
		strSQL += varTemp;
		varTemp.Format("BUNDLE_DEVICE_1 = %d, BUNDLE_DEVICE_2 = %d, BUNDLE_DEVICE_3 = %d, BUNDLE_DEVICE_4 = %d, BUNDLE_DEVICE_5 = %d, ", LotInfo.m_nBundleDeviceCount[0],LotInfo.m_nBundleDeviceCount[1],LotInfo.m_nBundleDeviceCount[2],LotInfo.m_nBundleDeviceCount[3],LotInfo.m_nBundleDeviceCount[4]);
		strSQL += varTemp;
		varTemp.Format("BUNDLE_DEVICE_6 = %d, BUNDLE_DEVICE_7 = %d, BUNDLE_DEVICE_8 = %d, BUNDLE_DEVICE_9 = %d, BUNDLE_DEVICE_10 = %d, ", LotInfo.m_nBundleDeviceCount[5],LotInfo.m_nBundleDeviceCount[6],LotInfo.m_nBundleDeviceCount[7],LotInfo.m_nBundleDeviceCount[8],LotInfo.m_nBundleDeviceCount[9]);
		strSQL += varTemp;
		varTemp.Format("IS_COMPLETE = %d, IS_FORCE_DELETED = %d,", LotInfo.m_bComplete, LotInfo.m_bForceDelete);
		strSQL += varTemp;
		varTemp.Format("LAST = Now() WHERE LOTID = %s", LotInfo.m_strLOTID);
		strSQL += varTemp;

		bTempBool = m_ADOClass.ExecuteQryRs( strSQL );
		if(!bTempBool)
		{
			// LOG 남기기
			GetLog()->Debug( _T("ADODB DB_UPDATE_LOTINFO QUERY 실패!") );
			CLogList::Instance()->Add_LogData(eLog_DEBUG, _T("ADODB DB_UPDATE_LOTINFO QUERY 실패!") );
		}

		m_ADOClass.DisConnect();
	}
	catch(_com_error &e)
	{
		m_ADOClass.DisConnect();

		_bstr_t btSource(e.Source());
		_bstr_t btDescription(e.Description());
		CString strMsg;
		strMsg.Format(_T("DATE= %s, %08lx= %s, Source= %s, Description= %s")
			,CTime::GetCurrentTime().Format(L"%Y-%m-%d %H:%M:%S")
			,e.Error(),e.ErrorMessage()
			,(LPCSTR)btSource
			,(LPCSTR)btDescription);
		// LOG 남기기
		GetLog()->Debug(_T("%s"), strMsg);
		CLogList::Instance()->Add_LogData(eLog_DEBUG, _T("%s"), strMsg);

		return FALSE;

	}
	return bTempBool;
}

BOOL CAMKOR_MMIDlg::DB_UPDATE_LOTINFO_BOX(stLotInfo &LotInfo)
{
	CADOClass	m_ADOClass;

	if (FALSE == m_ADOClass.Connect( m_strConnect_WorkInfo ))
	{
		// LOG 남기기
		GetLog()->Debug( _T("ADODB WORKINFO 연결에 실패하였습니다!") );
		CLogList::Instance()->Add_LogData(eLog_DEBUG, _T("ADODB WORKINFO 연결에 실패하였습니다!") );
		return FALSE;
	}

	CStringA varTemp;
	int nRSCount = 0;
	BOOL bTempBool = FALSE;
	CStringA strSQL;

	try
	{
		strSQL.Format("UPDATE [LOTINFO] SET ");
		varTemp.Format("MAKE_BOX_COUNT = %d, ", LotInfo.m_nMakeBoxCount);
		strSQL += varTemp;
		varTemp.Format("OUTPUT_BOX_COUNT = %d, ", LotInfo.m_nOutputBoxCount);
		strSQL += varTemp;
		varTemp.Format("DELETE_BOX_COUNT = %d, ", LotInfo.m_nDeleteBoxCount);
		strSQL += varTemp;
		varTemp.Format("CUR_MAKED_BOXID = %d, ", LotInfo.m_nCurMakeBoxID);
		strSQL += varTemp;
		varTemp.Format("IS_MAKE_LAST_BOXID = %d, ", LotInfo.m_bLastBoxIDMake);
		strSQL += varTemp;
		varTemp.Format("LAST = Now() WHERE LOTID = %s", LotInfo.m_strLOTID);
		strSQL += varTemp;

		bTempBool = m_ADOClass.ExecuteQryRs( strSQL );
		if(!bTempBool)
		{
			// LOG 남기기
			GetLog()->Debug( _T("ADODB DB_UPDATE_LOTINFO_BOX QUERY 실패!") );
			CLogList::Instance()->Add_LogData(eLog_DEBUG, _T("ADODB DB_UPDATE_LOTINFO_BOX QUERY 실패!") );
		}

		m_ADOClass.DisConnect();
	}
	catch(_com_error &e)
	{
		m_ADOClass.DisConnect();

		_bstr_t btSource(e.Source());
		_bstr_t btDescription(e.Description());
		CString strMsg;
		strMsg.Format(_T("DATE= %s, %08lx= %s, Source= %s, Description= %s")
			,CTime::GetCurrentTime().Format(L"%Y-%m-%d %H:%M:%S")
			,e.Error(),e.ErrorMessage()
			,(LPCSTR)btSource
			,(LPCSTR)btDescription);
		// LOG 남기기
		GetLog()->Debug(_T("%s"), strMsg);
		CLogList::Instance()->Add_LogData(eLog_DEBUG, _T("%s"), strMsg);

		return FALSE;

	}
	return bTempBool;
}

BOOL CAMKOR_MMIDlg::DB_UPDATE_LOTINFO_PROCESS(stLotInfo &LotInfo)
{
	CADOClass	m_ADOClass;

	if (FALSE == m_ADOClass.Connect( m_strConnect_WorkInfo ))
	{
		// LOG 남기기
		GetLog()->Debug( _T("ADODB WORKINFO 연결에 실패하였습니다!") );
		CLogList::Instance()->Add_LogData(eLog_DEBUG, _T("ADODB WORKINFO 연결에 실패하였습니다!") );
		return FALSE;
	}

	CStringA varTemp;
	int nRSCount = 0;
	BOOL bTempBool = FALSE;
	CStringA strSQL;

	try
	{
		strSQL.Format("UPDATE [LOTINFO] SET ");
		varTemp.Format("CUR_PROCESS = %d, ", LotInfo.m_nCurProcess);
		strSQL += varTemp;
		varTemp.Format("LAST = Now() WHERE LOTID = %s", LotInfo.m_strLOTID);
		strSQL += varTemp;

		bTempBool = m_ADOClass.ExecuteQryRs( strSQL );
		if(!bTempBool)
		{
			// LOG 남기기
			GetLog()->Debug( _T("ADODB DB_UPDATE_LOTINFO_PROCESS QUERY 실패!") );
			CLogList::Instance()->Add_LogData(eLog_DEBUG, _T("ADODB DB_UPDATE_LOTINFO_PROCESS QUERY 실패!") );
		}

		m_ADOClass.DisConnect();
	}
	catch(_com_error &e)
	{
		m_ADOClass.DisConnect();

		_bstr_t btSource(e.Source());
		_bstr_t btDescription(e.Description());
		CString strMsg;
		strMsg.Format(_T("DATE= %s, %08lx= %s, Source= %s, Description= %s")
			,CTime::GetCurrentTime().Format(L"%Y-%m-%d %H:%M:%S")
			,e.Error(),e.ErrorMessage()
			,(LPCSTR)btSource
			,(LPCSTR)btDescription);
		// LOG 남기기
		GetLog()->Debug(_T("%s"), strMsg);
		CLogList::Instance()->Add_LogData(eLog_DEBUG, _T("%s"), strMsg);

		return FALSE;

	}
	return bTempBool;
}

BOOL CAMKOR_MMIDlg::DB_UPDATE_LOTINFO_BUNDLE(stLotInfo &LotInfo)
{
	CADOClass	m_ADOClass;

	if (FALSE == m_ADOClass.Connect( m_strConnect_WorkInfo ))
	{
		// LOG 남기기
		GetLog()->Debug( _T("ADODB WORKINFO 연결에 실패하였습니다!") );
		CLogList::Instance()->Add_LogData(eLog_DEBUG, _T("ADODB WORKINFO 연결에 실패하였습니다!") );
		return FALSE;
	}

	CStringA varTemp;
	int nRSCount = 0;
	BOOL bTempBool = FALSE;
	CStringA strSQL;

	try
	{
		strSQL.Format("UPDATE [LOTINFO] SET ");
		varTemp.Format("INPUT_BUNDLE_COUNT = %d, ", LotInfo.m_nInputBundleCount);
		strSQL += varTemp;
		varTemp.Format("BUNDLE_RFID_1 = %s, BUNDLE_RFID_2 = %s, BUNDLE_RFID_3 = %s, BUNDLE_RFID_4 = %s, BUNDLE_RFID_5 = %s, ", LotInfo.m_strRFID[0],LotInfo.m_strRFID[1],LotInfo.m_strRFID[2],LotInfo.m_strRFID[3],LotInfo.m_strRFID[4]);
		strSQL += varTemp;
		varTemp.Format("BUNDLE_RFID_6 = %s, BUNDLE_RFID_7 = %s, BUNDLE_RFID_8 = %s, BUNDLE_RFID_9 = %s, BUNDLE_RFID_10 = %s, ", LotInfo.m_strRFID[5],LotInfo.m_strRFID[6],LotInfo.m_strRFID[7],LotInfo.m_strRFID[8],LotInfo.m_strRFID[9]);
		strSQL += varTemp;
		varTemp.Format("BUNDLE_DEVICE_1 = %d, BUNDLE_DEVICE_2 = %d, BUNDLE_DEVICE_3 = %d, BUNDLE_DEVICE_4 = %d, BUNDLE_DEVICE_5 = %d, ", LotInfo.m_nBundleDeviceCount[0],LotInfo.m_nBundleDeviceCount[1],LotInfo.m_nBundleDeviceCount[2],LotInfo.m_nBundleDeviceCount[3],LotInfo.m_nBundleDeviceCount[4]);
		strSQL += varTemp;
		varTemp.Format("BUNDLE_DEVICE_6 = %d, BUNDLE_DEVICE_7 = %d, BUNDLE_DEVICE_8 = %d, BUNDLE_DEVICE_9 = %d, BUNDLE_DEVICE_10 = %d, ", LotInfo.m_nBundleDeviceCount[5],LotInfo.m_nBundleDeviceCount[6],LotInfo.m_nBundleDeviceCount[7],LotInfo.m_nBundleDeviceCount[8],LotInfo.m_nBundleDeviceCount[9]);
		strSQL += varTemp;
		varTemp.Format("LAST = Now() WHERE LOTID = %s", LotInfo.m_strLOTID);
		strSQL += varTemp;

		bTempBool = m_ADOClass.ExecuteQryRs( strSQL );
		if(!bTempBool)
		{
			// LOG 남기기
			GetLog()->Debug( _T("ADODB DB_UPDATE_LOTINFO_BUNDLE QUERY 실패!") );
			CLogList::Instance()->Add_LogData(eLog_DEBUG, _T("ADODB DB_UPDATE_LOTINFO_BUNDLE QUERY 실패!") );
		}

		m_ADOClass.DisConnect();
	}
	catch(_com_error &e)
	{
		m_ADOClass.DisConnect();

		_bstr_t btSource(e.Source());
		_bstr_t btDescription(e.Description());
		CString strMsg;
		strMsg.Format(_T("DATE= %s, %08lx= %s, Source= %s, Description= %s")
			,CTime::GetCurrentTime().Format(L"%Y-%m-%d %H:%M:%S")
			,e.Error(),e.ErrorMessage()
			,(LPCSTR)btSource
			,(LPCSTR)btDescription);
		// LOG 남기기
		GetLog()->Debug(_T("%s"), strMsg);
		CLogList::Instance()->Add_LogData(eLog_DEBUG, _T("%s"), strMsg);

		return FALSE;

	}
	return bTempBool;
}

BOOL CAMKOR_MMIDlg::DB_UPDATE_LOTINFO_LAST(stLotInfo &LotInfo)
{
	CADOClass	m_ADOClass;

	if (FALSE == m_ADOClass.Connect( m_strConnect_WorkInfo ))
	{
		// LOG 남기기
		GetLog()->Debug( _T("ADODB WORKINFO 연결에 실패하였습니다!") );
		CLogList::Instance()->Add_LogData(eLog_DEBUG, _T("ADODB WORKINFO 연결에 실패하였습니다!") );
		return FALSE;
	}

	CStringA varTemp;
	int nRSCount = 0;
	BOOL bTempBool = FALSE;
	CStringA strSQL;

	try
	{
		strSQL.Format("UPDATE [LOTINFO] SET ");
		varTemp.Format("IS_COMPLETE = %d, IS_FORCE_DELETED = %d,", LotInfo.m_bComplete, LotInfo.m_bForceDelete);
		strSQL += varTemp;
		varTemp.Format("LAST = Now() WHERE LOTID = %s", LotInfo.m_strLOTID);
		strSQL += varTemp;

		bTempBool = m_ADOClass.ExecuteQryRs( strSQL );
		if(!bTempBool)
		{
			// LOG 남기기
			GetLog()->Debug( _T("ADODB DB_UPDATE_LOTINFO_LAST QUERY 실패!") );
			CLogList::Instance()->Add_LogData(eLog_DEBUG, _T("ADODB DB_UPDATE_LOTINFO_LAST QUERY 실패!") );
		}

		m_ADOClass.DisConnect();
	}
	catch(_com_error &e)
	{
		m_ADOClass.DisConnect();

		_bstr_t btSource(e.Source());
		_bstr_t btDescription(e.Description());
		CString strMsg;
		strMsg.Format(_T("DATE= %s, %08lx= %s, Source= %s, Description= %s")
			,CTime::GetCurrentTime().Format(L"%Y-%m-%d %H:%M:%S")
			,e.Error(),e.ErrorMessage()
			,(LPCSTR)btSource
			,(LPCSTR)btDescription);
		// LOG 남기기
		GetLog()->Debug(_T("%s"), strMsg);
		CLogList::Instance()->Add_LogData(eLog_DEBUG, _T("%s"), strMsg);

		return FALSE;

	}
	return bTempBool;
}

void CAMKOR_MMIDlg::OnBnClickedBtnMConn1()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(CDataManager::Instance()->IsConnected(CONN_PLC_1)) return;

	if( !CDataManager::Instance()->CloseDevice_PLC_1() )
	{
		// LOG 남기기
		GetLog()->Debug( _T("G1 PLC Close Fail.....") );
		CLogList::Instance()->Add_LogData(eLog_DEBUG, _T("G1 PLC Close Fail.....") );
	}
	else
	{
		// LOG 남기기
		GetLog()->Debug( _T("G1 PLC Close OK.....") );
		CLogList::Instance()->Add_LogData(eLog_DEBUG, _T("G1 PLC Close OK.....") );
	}

	if(!CDataManager::Instance()->OpenDevice_PLC_1(CAppSetting::Instance()->GetLogicalStationNumber(1)))
	{
		// LOG 남기기
		GetLog()->Debug( _T("G1 PLC CONNECT FAIL.....") );
		CLogList::Instance()->Add_LogData(eLog_DEBUG, _T("G1 PLC CONNECT FAIL.....") );
	}
	else
	{
		CDataManager::Instance()->SetConnect(CONN_PLC_1, TRUE);

		// LOG 남기기
		GetLog()->Debug( _T("G1 PLC CONNECT OK.....") );
		CLogList::Instance()->Add_LogData(eLog_DEBUG, _T("G1 PLC CONNECT OK.....") );
	}

	SendMessage(WM_UPDATE_UI_CONN, 0, 0);
}


void CAMKOR_MMIDlg::OnBnClickedBtnMConn2()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(CDataManager::Instance()->IsConnected(CONN_PLC_2)) return;

	if( !CDataManager::Instance()->CloseDevice_PLC_2() )
	{
		// LOG 남기기
		GetLog()->Debug( _T("G2 PLC Close Fail.....") );
		CLogList::Instance()->Add_LogData(eLog_DEBUG, _T("G2 PLC Close Fail.....") );
	}
	else
	{
		// LOG 남기기
		GetLog()->Debug( _T("G2 PLC Close OK.....") );
		CLogList::Instance()->Add_LogData(eLog_DEBUG, _T("G2 PLC Close OK.....") );
	}

	if(!CDataManager::Instance()->OpenDevice_PLC_2(CAppSetting::Instance()->GetLogicalStationNumber(2)))
	{
		// LOG 남기기
		GetLog()->Debug( _T("G2 PLC CONNECT FAIL.....") );
		CLogList::Instance()->Add_LogData(eLog_DEBUG, _T("G2 PLC CONNECT FAIL.....") );
	}
	else
	{
		CDataManager::Instance()->SetConnect(CONN_PLC_2, TRUE);

		// LOG 남기기
		GetLog()->Debug( _T("G2 PLC CONNECT OK.....") );
		CLogList::Instance()->Add_LogData(eLog_DEBUG, _T("G2 PLC CONNECT OK.....") );
	}

	SendMessage(WM_UPDATE_UI_CONN, 0, 0);
}


void CAMKOR_MMIDlg::OnBnClickedBtnMConn3()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(CDataManager::Instance()->IsConnected(CONN_PLC_3)) return;

	if( !CDataManager::Instance()->CloseDevice_PLC_3() )
	{
		// LOG 남기기
		GetLog()->Debug( _T("G3 PLC Close Fail.....") );
		CLogList::Instance()->Add_LogData(eLog_DEBUG, _T("G3 PLC Close Fail.....") );
	}
	else
	{
		// LOG 남기기
		GetLog()->Debug( _T("G3 PLC Close OK.....") );
		CLogList::Instance()->Add_LogData(eLog_DEBUG, _T("G3 PLC Close OK.....") );
	}

	if(!CDataManager::Instance()->OpenDevice_PLC_3(CAppSetting::Instance()->GetLogicalStationNumber(3)))
	{
		// LOG 남기기
		GetLog()->Debug( _T("G3 PLC CONNECT FAIL.....") );
		CLogList::Instance()->Add_LogData(eLog_DEBUG, _T("G3 PLC CONNECT FAIL.....") );
	}
	else
	{
		CDataManager::Instance()->SetConnect(CONN_PLC_3, TRUE);

		// LOG 남기기
		GetLog()->Debug( _T("G3 PLC CONNECT OK.....") );
		CLogList::Instance()->Add_LogData(eLog_DEBUG, _T("G3 PLC CONNECT OK.....") );
	}

	SendMessage(WM_UPDATE_UI_CONN, 0, 0);
}


void CAMKOR_MMIDlg::OnBnClickedBtnErrorcode()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	SHORT sCode = 0;
	if(CAppSetting::Instance()->GetGroupNumber() == 1) sCode = CDataManager::Instance()->m_PLCReaderD_Alarm_G1.GetValue(99);
	else if(CAppSetting::Instance()->GetGroupNumber() == 2) sCode = CDataManager::Instance()->m_PLCReaderD_Alarm_G2.GetValue(99);
	else if(CAppSetting::Instance()->GetGroupNumber() == 3) sCode = CDataManager::Instance()->m_PLCReaderD_Alarm_G3.GetValue(99);

	if(sCode != 0)
	{
		ChangeScreen(5);
		m_nCurScreen = 5;

		// LOG 남기기
		GetLog()->Debug( _T("MAIN ERROR CODE BTN PRESSED") );
		CLogList::Instance()->Add_LogData(eLog_DEBUG, _T("MAIN ERROR CODE BTN PRESSED") );
	}
}


void CAMKOR_MMIDlg::OnBnClickedBtnErrortext()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	SHORT sCode = 0;
	if(CAppSetting::Instance()->GetGroupNumber() == 1) sCode = CDataManager::Instance()->m_PLCReaderD_Alarm_G1.GetValue(99);
	else if(CAppSetting::Instance()->GetGroupNumber() == 2) sCode = CDataManager::Instance()->m_PLCReaderD_Alarm_G2.GetValue(99);
	else if(CAppSetting::Instance()->GetGroupNumber() == 3) sCode = CDataManager::Instance()->m_PLCReaderD_Alarm_G3.GetValue(99);

	if(sCode != 0)
	{
		ChangeScreen(5);
		m_nCurScreen = 5;

		// LOG 남기기
		GetLog()->Debug( _T("MAIN ERROR TEXT BTN PRESSED") );
		CLogList::Instance()->Add_LogData(eLog_DEBUG, _T("MAIN ERROR TEXT BTN PRESSED") );
	}
}

void CAMKOR_MMIDlg::OnClose()
{
	if(CAppSetting::Instance()->GetGroupNumber() == 1) m_dlg_Gem.StopXGem300();
	CDialogEx::OnClose();
}
