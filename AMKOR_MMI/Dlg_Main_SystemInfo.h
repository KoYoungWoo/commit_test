#pragma once
#include "afxwin.h"
#include "UIExt/MatrixStatic.h"
#include "UIExt/CxStatic.h"
#include "UIExt/IconButton.h"
#include "UIExt/XPGroupBox.h"
#include "UIExt/MultilineList.h"
#include "UIExt/CxListBox.h"
#include "GC/gridctrl.h"
#include "afxcmn.h"

// CDlg_Main_SystemInfo 대화 상자입니다.

class CDlg_Main_SystemInfo : public CDialogEx
{
	DECLARE_DYNAMIC(CDlg_Main_SystemInfo)

public:
	CDlg_Main_SystemInfo(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CDlg_Main_SystemInfo();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DLG_MAIN_SYSTEMINFO };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);

	void Cal_Area();
	void Cal_CtrlArea();
	void Cal_CtrlArea_Top();
	void Cal_CtrlArea_Mid();
	void Cal_CtrlArea_Bottom();

	bool Init_GC_1(int nRows, int nCols);
	afx_msg void OnGridClick_GC_1(NMHDR *pNotifyStruct, LRESULT* pResult);
	bool Init_GC_2(int nRows, int nCols);
	afx_msg void OnGridClick_GC_2(NMHDR *pNotifyStruct, LRESULT* pResult);

	void Update_TrayPosInfo();
	void Update_LotListInfo();

	void Update_UI();
	void Update_LogList();

	BOOL m_bCtrl_FirstLoad;

	CRect m_rcTop;
	CRect m_rcMid;
	CRect m_rcBottom;

	CXPGroupBox m_gb_1;
	CXPGroupBox m_gb_2;
	CGridCtrl m_GC_1;
	CGridCtrl m_GC_2;
	CMultilineList m_list_1;
	CxListBox m_list_LogView;
	CXPGroupBox m_gb_3;
};
