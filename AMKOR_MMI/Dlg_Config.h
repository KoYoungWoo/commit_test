#pragma once
#include "afxwin.h"
#include "UIExt/CxStatic.h"
#include "UIExt/IconButton.h"
#include "UIExt/XPGroupBox.h"
#include "UIExt/BorderStyleEdit.h"
#include "afxcmn.h"

// CDlg_Config 대화 상자입니다.

class CDlg_Config : public CDialogEx
{
	DECLARE_DYNAMIC(CDlg_Config)

public:
	CDlg_Config(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CDlg_Config();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DLG_CONFIG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);

	void Cal_Area();
	void Cal_CtrlArea();
	void Cal_CtrlArea_Div1();
	void Cal_CtrlArea_Div8();

	void SetAppSettingValue();

	BOOL m_bCtrl_FirstLoad;
	CRect m_rcDiv8[8];

	CXPGroupBox m_gb_1;
	CxStatic m_st_1;
	CxStatic m_st_2;
	CxStatic m_st_3;
	CxStatic m_st_4;
	CxStatic m_st_5;
	CIPAddressCtrl m_ip_1;
	CIPAddressCtrl m_ip_2;
	CIPAddressCtrl m_ip_3;
	CIPAddressCtrl m_ip_4;
	CIPAddressCtrl m_ip_5;
	CBorderStyleEdit m_edit_1;
	CBorderStyleEdit m_edit_2;
	CBorderStyleEdit m_edit_3;
	CBorderStyleEdit m_edit_4;
	CBorderStyleEdit m_edit_5;
	CIconButton m_btn_1;
	CIconButton m_btn_2;
	CIconButton m_btn_3;
	CIconButton m_btn_4;
	CIconButton m_btn_5;
	CIconButton m_btn_6;
	CIconButton m_btn_7;
	CIconButton m_btn_8;
	CIconButton m_btn_9;
	CIconButton m_btn_10;

	CIconButton m_btn_11;
	CIconButton m_btn_12;

	afx_msg void OnBnClickedBtnCf1();
	afx_msg void OnBnClickedBtnCf2();
	afx_msg void OnBnClickedBtnCf3();
	afx_msg void OnBnClickedBtnCf4();
	afx_msg void OnBnClickedBtnCf5();
	afx_msg void OnBnClickedBtnCf6();
	afx_msg void OnBnClickedBtnCf7();
	afx_msg void OnBnClickedBtnCf8();
	afx_msg void OnBnClickedBtnCf9();
	afx_msg void OnBnClickedBtnCf10();
	afx_msg void OnBnClickedBtnCf11();
	afx_msg void OnBnClickedBtnCf12();

	void TestZPLPrint(int nDeviceNum, int nPrinterNum);
	void LabelPrint(int nDeviceNum, int nPrinterNum, CString strOrigianlZPL);
	CString AdjustLabelHome(CString strZPL,CString strOffset);
	CString StdLabelShift(CString strZPL);
};
