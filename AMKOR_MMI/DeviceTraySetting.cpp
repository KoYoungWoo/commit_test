// DeviceTraySetting.cpp: implementation of the CAppSetting class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "DeviceTraySetting.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// CDeviceTraySettingGC
class CDeviceTraySettingGC
{
public:
	CDeviceTraySettingGC() {}
	~CDeviceTraySettingGC() { delete CDeviceTraySetting::m_pThis; CDeviceTraySetting::m_pThis = NULL; }
};
CDeviceTraySettingGC s_GC;

//////////////////////////////////////////////////////////////////////////
// CDeviceTraySetting
CDeviceTraySetting* CDeviceTraySetting::m_pThis = NULL;
CDeviceTraySetting::CDeviceTraySetting()
{
	m_stDTInfo.m_vecDeviceInfo.clear();
	m_stDTInfo.m_vecTrayInfo.clear();
}

CDeviceTraySetting::~CDeviceTraySetting()
{
}

CDeviceTraySetting* CDeviceTraySetting::Instance()
{
	if( !m_pThis )
		m_pThis = new CDeviceTraySetting;
	return m_pThis;
}

// 현재 실행화일 경로 가져오자.
CString CDeviceTraySetting::GetExecuteDirectory()
{
	CString strFolderPath;

	// 현재 프로그램의 실행 파일이 있는 폴더명을 추출함
	::GetModuleFileName(NULL, strFolderPath.GetBuffer(MAX_PATH), MAX_PATH);
	strFolderPath.ReleaseBuffer();
	if (strFolderPath.Find('\\') != -1)
	{
		for (int i = strFolderPath.GetLength() - 1; i >= 0; i--) 
		{
			TCHAR ch = strFolderPath[i];
			strFolderPath.Delete(i);
			if (ch == '\\') break; 
		}
	}
	return strFolderPath;
}

void CDeviceTraySetting::GetFileList()
{
	m_vecFileList.clear();

	CFileFind fileFind;
	CString strPath = GetExecuteDirectory();
	strPath += _T("\\*.ini");

	// 이곳에서 파일 리스트를 검색할 경로를 지정
	BOOL result = fileFind.FindFile(strPath);
	if (result)
	{
		while(fileFind.FindNextFile())
		{
			m_vecFileList.push_back(fileFind.GetFileName());
		}
		//  while문에서 다음 값이 널일 경우 마지막 파일일 경우 다음 파일이 없기 때문에
		// 마지막 파일명은 출력이 안되므로 while문 종료후에 현재 파일 명을 출력한다
		m_vecFileList.push_back(fileFind.GetFileName());
	}
	else
	{
		GetLog()->Debug(_T("CMotorDeviceSetting - ini 파일을 찾을수 없습니다..."));
	}
 
	// 파일 리스트를 닫는다.
	fileFind.Close();
}

BOOL CDeviceTraySetting::Load_DeviceSetting()
{
	m_stDTInfo.m_vecDeviceInfo.clear();
	
	CString strPath = GetExecuteDirectory();
	strPath += _T("\\Setting\\");
	strPath += _T("Device.ini");

	TCHAR temp[MAX_PATH];
	CString strMain, strSub;

	::GetPrivateProfileString( _T("DEVICE_COUNT"), _T("COUNT"), _T("0"), temp, MAX_PATH, strPath );
	m_stDTInfo.m_nDeviceCount = _wtoi(temp);

	m_stDTInfo.m_vecDeviceInfo.resize(m_stDTInfo.m_nDeviceCount);

	for(int i=0;i<m_stDTInfo.m_nDeviceCount;i++)
	{
		strMain.Format(_T("DEVICE_%03d"),i+1);

		::GetPrivateProfileString( strMain, _T("NAME"), _T(""), temp, MAX_PATH, strPath );
		m_stDTInfo.m_vecDeviceInfo[i].m_strDeviceName.Format(_T("%s"),temp);

		::GetPrivateProfileString( strMain, _T("CUSTOMER"), _T(""), temp, MAX_PATH, strPath );
		m_stDTInfo.m_vecDeviceInfo[i].m_strCustomer.Format(_T("%s"),temp);

		::GetPrivateProfileString( strMain, _T("TRAY_INDEX"), _T("0"), temp, MAX_PATH, strPath );
		m_stDTInfo.m_vecDeviceInfo[i].m_nTrayIndex = _wtoi(temp);

		::GetPrivateProfileString( strMain, _T("TRAY_DIVIDE"), _T("0"), temp, MAX_PATH, strPath );
		m_stDTInfo.m_vecDeviceInfo[i].m_nTrayDivideCount = _wtoi(temp);

		::GetPrivateProfileString( strMain, _T("BAND_VCOUNT"), _T("0"), temp, MAX_PATH, strPath );
		m_stDTInfo.m_vecDeviceInfo[i].m_nBand_VCount = _wtoi(temp);

		::GetPrivateProfileString( strMain, _T("BAND_HCOUNT"), _T("0"), temp, MAX_PATH, strPath );
		m_stDTInfo.m_vecDeviceInfo[i].m_nBand_HCount = _wtoi(temp);

		::GetPrivateProfileString( strMain, _T("BOTTOMTRAY"), _T("0"), temp, MAX_PATH, strPath );
		m_stDTInfo.m_vecDeviceInfo[i].m_nBottomTray = _wtoi(temp);

		::GetPrivateProfileString( strMain, _T("COVERTRAY"), _T("0"), temp, MAX_PATH, strPath );
		m_stDTInfo.m_vecDeviceInfo[i].m_nCoverTray = _wtoi(temp);

		::GetPrivateProfileString( strMain, _T("TRAYROTATE"), _T("0"), temp, MAX_PATH, strPath );
		m_stDTInfo.m_vecDeviceInfo[i].m_nTrayRotate = _wtoi(temp);

		::GetPrivateProfileString( strMain, _T("ENDCAP"), _T("0"), temp, MAX_PATH, strPath );
		m_stDTInfo.m_vecDeviceInfo[i].m_nEndCap = _wtoi(temp);

		::GetPrivateProfileString( strMain, _T("USE_HICDSC"), _T("0"), temp, MAX_PATH, strPath );
		m_stDTInfo.m_vecDeviceInfo[i].m_nUseHIC_DSC = _wtoi(temp);

		::GetPrivateProfileString( strMain, _T("MBB_TYPE"), _T("0"), temp, MAX_PATH, strPath );
		m_stDTInfo.m_vecDeviceInfo[i].m_nMBB_Type = _wtoi(temp);

		::GetPrivateProfileString( strMain, _T("BOX_TYPE"), _T("0"), temp, MAX_PATH, strPath );
		m_stDTInfo.m_vecDeviceInfo[i].m_nBOX_Type = _wtoi(temp);

		::GetPrivateProfileString( strMain, _T("BOXROTATE"), _T("0"), temp, MAX_PATH, strPath );
		m_stDTInfo.m_vecDeviceInfo[i].m_nBoxRotate = _wtoi(temp);

		::GetPrivateProfileString( strMain, _T("BUBBLESHEET"), _T("0"), temp, MAX_PATH, strPath );
		m_stDTInfo.m_vecDeviceInfo[i].m_nBubbleSheet = _wtoi(temp);

		::GetPrivateProfileString( strMain, _T("BOXLABEL_POS"), _T("0"), temp, MAX_PATH, strPath );
		m_stDTInfo.m_vecDeviceInfo[i].m_nBoxLabelPos = _wtoi(temp);

		::GetPrivateProfileString( strMain, _T("USE_BOX"), _T("0"), temp, MAX_PATH, strPath );
		m_stDTInfo.m_vecDeviceInfo[i].m_nUseBox = _wtoi(temp);

		::GetPrivateProfileString( strMain, _T("LABELSIZE"), _T("0"), temp, MAX_PATH, strPath );
		m_stDTInfo.m_vecDeviceInfo[i].m_nLabelSize = _wtoi(temp);

		::GetPrivateProfileString( strMain, _T("SILROTATE"), _T("0"), temp, MAX_PATH, strPath );
		m_stDTInfo.m_vecDeviceInfo[i].m_nSilRotate = _wtoi(temp);

		::GetPrivateProfileString( strMain, _T("USE_INK"), _T("0"), temp, MAX_PATH, strPath );
		m_stDTInfo.m_vecDeviceInfo[i].m_nUseInk = _wtoi(temp);
	}

	return TRUE;
}

BOOL CDeviceTraySetting::Save_DeviceSetting()
{
	// 저장전 백업해둔다.
	BackupFile_DeviceSetting();

	CString strPath = GetExecuteDirectory();
	strPath += _T("\\Setting\\");
	strPath += _T("Device.ini");

	CString strTemp;
	CString strMain, strSub;

	strTemp.Format(_T("%d"), m_stDTInfo.m_nDeviceCount);
	::WritePrivateProfileString( _T("DEVICE_COUNT"), _T("COUNT"), strTemp, strPath );

	for(size_t i=0;i<m_stDTInfo.m_vecDeviceInfo.size();i++)
	{
		strMain.Format(_T("DEVICE_%03d"),i+1);

		::WritePrivateProfileString( strMain, _T("NAME"), m_stDTInfo.m_vecDeviceInfo[i].m_strDeviceName, strPath );

		::WritePrivateProfileString( strMain, _T("CUSTOMER"), m_stDTInfo.m_vecDeviceInfo[i].m_strCustomer, strPath );

		strTemp.Format(_T("%d"), m_stDTInfo.m_vecDeviceInfo[i].m_nTrayIndex);
		::WritePrivateProfileString( strMain, _T("TRAY_INDEX"), strTemp, strPath );

		strTemp.Format(_T("%d"), m_stDTInfo.m_vecDeviceInfo[i].m_nTrayDivideCount);
		::WritePrivateProfileString( strMain, _T("TRAY_DIVIDE"), strTemp, strPath );

		strTemp.Format(_T("%d"), m_stDTInfo.m_vecDeviceInfo[i].m_nBand_VCount);
		::WritePrivateProfileString( strMain, _T("BAND_VCOUNT"), strTemp, strPath );

		strTemp.Format(_T("%d"), m_stDTInfo.m_vecDeviceInfo[i].m_nBand_HCount);
		::WritePrivateProfileString( strMain, _T("BAND_HCOUNT"), strTemp, strPath );

		strTemp.Format(_T("%d"), m_stDTInfo.m_vecDeviceInfo[i].m_nBottomTray);
		::WritePrivateProfileString( strMain, _T("BOTTOMTRAY"), strTemp, strPath );

		strTemp.Format(_T("%d"), m_stDTInfo.m_vecDeviceInfo[i].m_nCoverTray);
		::WritePrivateProfileString( strMain, _T("COVERTRAY"), strTemp, strPath );

		strTemp.Format(_T("%d"), m_stDTInfo.m_vecDeviceInfo[i].m_nTrayRotate);
		::WritePrivateProfileString( strMain, _T("TRAYROTATE"), strTemp, strPath );

		strTemp.Format(_T("%d"), m_stDTInfo.m_vecDeviceInfo[i].m_nEndCap);
		::WritePrivateProfileString( strMain, _T("ENDCAP"), strTemp, strPath );

		strTemp.Format(_T("%d"), m_stDTInfo.m_vecDeviceInfo[i].m_nUseHIC_DSC);
		::WritePrivateProfileString( strMain, _T("USE_HICDSC"), strTemp, strPath );

		strTemp.Format(_T("%d"), m_stDTInfo.m_vecDeviceInfo[i].m_nMBB_Type);
		::WritePrivateProfileString( strMain, _T("MBB_TYPE"), strTemp, strPath );

		strTemp.Format(_T("%d"), m_stDTInfo.m_vecDeviceInfo[i].m_nBOX_Type);
		::WritePrivateProfileString( strMain, _T("BOX_TYPE"), strTemp, strPath );

		strTemp.Format(_T("%d"), m_stDTInfo.m_vecDeviceInfo[i].m_nBoxRotate);
		::WritePrivateProfileString( strMain, _T("BOXROTATE"), strTemp, strPath );

		strTemp.Format(_T("%d"), m_stDTInfo.m_vecDeviceInfo[i].m_nBubbleSheet);
		::WritePrivateProfileString( strMain, _T("BUBBLESHEET"), strTemp, strPath );

		strTemp.Format(_T("%d"), m_stDTInfo.m_vecDeviceInfo[i].m_nBoxLabelPos);
		::WritePrivateProfileString( strMain, _T("BOXLABEL_POS"), strTemp, strPath );

		strTemp.Format(_T("%d"), m_stDTInfo.m_vecDeviceInfo[i].m_nUseBox);
		::WritePrivateProfileString( strMain, _T("USE_BOX"), strTemp, strPath );

		strTemp.Format(_T("%d"), m_stDTInfo.m_vecDeviceInfo[i].m_nLabelSize);
		::WritePrivateProfileString( strMain, _T("LABELSIZE"), strTemp, strPath );

		strTemp.Format(_T("%d"), m_stDTInfo.m_vecDeviceInfo[i].m_nSilRotate);
		::WritePrivateProfileString( strMain, _T("SILROTATE"), strTemp, strPath );

		strTemp.Format(_T("%d"), m_stDTInfo.m_vecDeviceInfo[i].m_nUseInk);
		::WritePrivateProfileString( strMain, _T("USE_INK"), strTemp, strPath );
	}

	return TRUE;
}

void CDeviceTraySetting::BackupFile_DeviceSetting()
{
	CString strPath = GetExecuteDirectory();
	strPath += _T("\\Setting\\");
	strPath += _T("Device.ini");

	CTime CurTime = CTime::GetCurrentTime();
	CString strTemp;
	strTemp.Format(_T("Device_%04d%02d%02d_%02d%02d%02d.ini"), CurTime.GetYear(), CurTime.GetMonth(), CurTime.GetDay(), CurTime.GetHour(), CurTime.GetMinute(), CurTime.GetSecond());

	CString strBkPath = GetExecuteDirectory();
	strBkPath += _T("\\Setting\\BackUp\\");
	strBkPath += strTemp;

	::CopyFile(strPath.GetBuffer(), strBkPath.GetBuffer(), FALSE);
}

void CDeviceTraySetting::AddDeviceInfo()
{
	m_stDTInfo.m_nDeviceCount ++;
	if(m_stDTInfo.m_nDeviceCount > (int)m_stDTInfo.m_vecDeviceInfo.size())
	{
		stDeviceInfo stDI;
		stDI.Clear();
		stDI.m_strDeviceName.Format(_T("Device #%02d"),m_stDTInfo.m_nDeviceCount);
		stDI.m_strCustomer.Format(_T("Customer #%02d"),m_stDTInfo.m_nDeviceCount);
		m_stDTInfo.m_vecDeviceInfo.push_back(stDI);
	}
	else
	{
		m_stDTInfo.m_vecDeviceInfo[m_stDTInfo.m_nDeviceCount - 1].Clear();
		m_stDTInfo.m_vecDeviceInfo[m_stDTInfo.m_nDeviceCount - 1].m_strDeviceName.Format(_T("Device #%02d"),m_stDTInfo.m_nDeviceCount);
		m_stDTInfo.m_vecDeviceInfo[m_stDTInfo.m_nDeviceCount - 1].m_strCustomer.Format(_T("Customer #%02d"),m_stDTInfo.m_nDeviceCount);
	}
}

void CDeviceTraySetting::DelDeviceInfo()
{
	m_stDTInfo.m_vecDeviceInfo[m_stDTInfo.m_nDeviceCount - 1].Clear();
	m_stDTInfo.m_nDeviceCount --;
}

void CDeviceTraySetting::SetDeviceTrayIndex(int nDeviceIndex, int nTrayIndex)
{
	if(nDeviceIndex > m_stDTInfo.m_nDeviceCount-1) return;

	m_stDTInfo.m_vecDeviceInfo[nDeviceIndex].m_nTrayIndex = nTrayIndex;
}

void CDeviceTraySetting::SetDeviceInfo(int nDeviceIndex, int nTrayIndex, int nDivide, int nBandV, int nBandH,
		int nApron, int nCover, int nTRotate, int nEndCap, int nUseHICDSC, int nMBBType, int nBOXType,
		int nBRotate, int nUseBubble, int nBoxLabelPos, int nUseBox, int nLabelSize, int nSilRotate, int nUseInk)
{
	if(nDeviceIndex > m_stDTInfo.m_nDeviceCount-1) return;

	m_stDTInfo.m_vecDeviceInfo[nDeviceIndex].m_nTrayIndex = nTrayIndex;
	m_stDTInfo.m_vecDeviceInfo[nDeviceIndex].m_nTrayDivideCount = nDivide;
	m_stDTInfo.m_vecDeviceInfo[nDeviceIndex].m_nBand_VCount = nBandV;
	m_stDTInfo.m_vecDeviceInfo[nDeviceIndex].m_nBand_HCount = nBandH;
	m_stDTInfo.m_vecDeviceInfo[nDeviceIndex].m_nBottomTray = nApron;
	m_stDTInfo.m_vecDeviceInfo[nDeviceIndex].m_nCoverTray = nCover;
	m_stDTInfo.m_vecDeviceInfo[nDeviceIndex].m_nTrayRotate = nTRotate;
	m_stDTInfo.m_vecDeviceInfo[nDeviceIndex].m_nEndCap = nEndCap;
	m_stDTInfo.m_vecDeviceInfo[nDeviceIndex].m_nUseHIC_DSC = nUseHICDSC;
	m_stDTInfo.m_vecDeviceInfo[nDeviceIndex].m_nMBB_Type = nMBBType;
	m_stDTInfo.m_vecDeviceInfo[nDeviceIndex].m_nBOX_Type = nBOXType;
	m_stDTInfo.m_vecDeviceInfo[nDeviceIndex].m_nBoxRotate = nBRotate;
	m_stDTInfo.m_vecDeviceInfo[nDeviceIndex].m_nBubbleSheet = nUseBubble;
	m_stDTInfo.m_vecDeviceInfo[nDeviceIndex].m_nBoxLabelPos = nBoxLabelPos;
	m_stDTInfo.m_vecDeviceInfo[nDeviceIndex].m_nUseBox = nUseBox;
	m_stDTInfo.m_vecDeviceInfo[nDeviceIndex].m_nLabelSize = nLabelSize;
	m_stDTInfo.m_vecDeviceInfo[nDeviceIndex].m_nSilRotate = nSilRotate;
	m_stDTInfo.m_vecDeviceInfo[nDeviceIndex].m_nUseInk = nUseInk;
}

void CDeviceTraySetting::SetDeviceInfo2(int nDeviceIndex, LPCTSTR strName, LPCTSTR strCustomer, int nTrayIndex, int nDivide,
		int nBandV, int nBandH, int nApron, int nCover, int nTRotate, int nEndCap, int nUseHICDSC, int nMBBType, int nBOXType,
		int nBRotate, int nUseBubble, int nBoxLabelPos, int nUseBox, int nLabelSize, int nSilRotate, int nUseInk)
{
	if(nDeviceIndex > m_stDTInfo.m_nDeviceCount-1) return;

	m_stDTInfo.m_vecDeviceInfo[nDeviceIndex].Clear();
	m_stDTInfo.m_vecDeviceInfo[nDeviceIndex].m_strDeviceName = strName;
	m_stDTInfo.m_vecDeviceInfo[nDeviceIndex].m_strCustomer = strCustomer;
	m_stDTInfo.m_vecDeviceInfo[nDeviceIndex].m_nTrayIndex = nTrayIndex;
	m_stDTInfo.m_vecDeviceInfo[nDeviceIndex].m_nTrayDivideCount = nDivide;
	m_stDTInfo.m_vecDeviceInfo[nDeviceIndex].m_nBand_VCount = nBandV;
	m_stDTInfo.m_vecDeviceInfo[nDeviceIndex].m_nBand_HCount = nBandH;
	m_stDTInfo.m_vecDeviceInfo[nDeviceIndex].m_nBottomTray = nApron;
	m_stDTInfo.m_vecDeviceInfo[nDeviceIndex].m_nCoverTray = nCover;
	m_stDTInfo.m_vecDeviceInfo[nDeviceIndex].m_nTrayRotate = nTRotate;
	m_stDTInfo.m_vecDeviceInfo[nDeviceIndex].m_nEndCap = nEndCap;
	m_stDTInfo.m_vecDeviceInfo[nDeviceIndex].m_nUseHIC_DSC = nUseHICDSC;
	m_stDTInfo.m_vecDeviceInfo[nDeviceIndex].m_nMBB_Type = nMBBType;
	m_stDTInfo.m_vecDeviceInfo[nDeviceIndex].m_nBOX_Type = nBOXType;
	m_stDTInfo.m_vecDeviceInfo[nDeviceIndex].m_nBoxRotate = nBRotate;
	m_stDTInfo.m_vecDeviceInfo[nDeviceIndex].m_nBubbleSheet = nUseBubble;
	m_stDTInfo.m_vecDeviceInfo[nDeviceIndex].m_nBoxLabelPos = nBoxLabelPos;
	m_stDTInfo.m_vecDeviceInfo[nDeviceIndex].m_nUseBox = nUseBox;
	m_stDTInfo.m_vecDeviceInfo[nDeviceIndex].m_nLabelSize = nLabelSize;
	m_stDTInfo.m_vecDeviceInfo[nDeviceIndex].m_nSilRotate = nSilRotate;
	m_stDTInfo.m_vecDeviceInfo[nDeviceIndex].m_nUseInk = nUseInk;
}

BOOL CDeviceTraySetting::Load_TrayInfoSetting()
{
	m_stDTInfo.m_vecTrayInfo.clear();
	
	CString strPath = GetExecuteDirectory();
	strPath += _T("\\Setting\\");
	strPath += _T("TrayIndex.ini");

	TCHAR temp[MAX_PATH];
	CString strMain, strSub;

	::GetPrivateProfileString( _T("TRAY_INDEX_COUNT"), _T("COUNT"), _T("0"), temp, MAX_PATH, strPath );
	m_stDTInfo.m_nTrayIndexCount = _wtoi(temp);

	m_stDTInfo.m_vecTrayInfo.resize(m_stDTInfo.m_nTrayIndexCount);

	for(int i=0;i<m_stDTInfo.m_nTrayIndexCount;i++)
	{
		strMain.Format(_T("TRAY_INFO_%02d"),i+1);

		::GetPrivateProfileString( strMain, _T("TRAY_SIZE"), _T(""), temp, MAX_PATH, strPath );
		m_stDTInfo.m_vecTrayInfo[i].m_strTraySize.Format(_T("%s"),temp);

		::GetPrivateProfileString( strMain, _T("TRAY_TOTAL_HEIGHT"), _T("0.0"), temp, MAX_PATH, strPath );
		m_stDTInfo.m_vecTrayInfo[i].m_fTotalHeight = _wtof(temp);

		::GetPrivateProfileString( strMain, _T("TRAY_OVERLAP_HEIGHT"), _T("0.0"), temp, MAX_PATH, strPath );
		m_stDTInfo.m_vecTrayInfo[i].m_fOverlapHeight = _wtof(temp);

		m_stDTInfo.m_vecTrayInfo[i].m_fHeight = m_stDTInfo.m_vecTrayInfo[i].m_fTotalHeight - m_stDTInfo.m_vecTrayInfo[i].m_fOverlapHeight;

		::GetPrivateProfileString( strMain, _T("TRAY_MAT_ROW"), _T("0"), temp, MAX_PATH, strPath );
		m_stDTInfo.m_vecTrayInfo[i].m_nTrayMatrixRow = _wtoi(temp);

		::GetPrivateProfileString( strMain, _T("TRAY_MAT_COL"), _T("0"), temp, MAX_PATH, strPath );
		m_stDTInfo.m_vecTrayInfo[i].m_nTrayMatrixCol = _wtoi(temp);

		::GetPrivateProfileString( strMain, _T("TRAY_SIZE_X"), _T("0.0"), temp, MAX_PATH, strPath );
		m_stDTInfo.m_vecTrayInfo[i].m_fTraySizeX = _wtof(temp);

		::GetPrivateProfileString( strMain, _T("TRAY_SIZE_Y"), _T("0.0"), temp, MAX_PATH, strPath );
		m_stDTInfo.m_vecTrayInfo[i].m_fTraySizeY = _wtof(temp);
	}

	return TRUE;
}

BOOL CDeviceTraySetting::Save_TrayInfoSetting()
{
	// 저장전 백업해둔다.
	BackupFile_TrayInfoSetting();

	CString strPath = GetExecuteDirectory();
	strPath += _T("\\Setting\\");
	strPath += _T("TrayIndex.ini");

	CString strTemp;
	CString strMain, strSub;

	strTemp.Format(_T("%d"), m_stDTInfo.m_nTrayIndexCount);
	::WritePrivateProfileString( _T("TRAY_INDEX_COUNT"), _T("COUNT"), strTemp, strPath );

	for(size_t i=0;i<m_stDTInfo.m_vecTrayInfo.size();i++)
	{
		strMain.Format(_T("TRAY_INFO_%02d"),i+1);

		::WritePrivateProfileString( strMain, _T("TRAY_SIZE"), m_stDTInfo.m_vecTrayInfo[i].m_strTraySize, strPath );

		strTemp.Format(_T("%f"), m_stDTInfo.m_vecTrayInfo[i].m_fTotalHeight);
		::WritePrivateProfileString( strMain, _T("TRAY_TOTAL_HEIGHT"), strTemp, strPath );

		strTemp.Format(_T("%f"), m_stDTInfo.m_vecTrayInfo[i].m_fOverlapHeight);
		::WritePrivateProfileString( strMain, _T("TRAY_OVERLAP_HEIGHT"), strTemp, strPath );

		strTemp.Format(_T("%d"), m_stDTInfo.m_vecTrayInfo[i].m_nTrayMatrixRow);
		::WritePrivateProfileString( strMain, _T("TRAY_MAT_ROW"), strTemp, strPath );

		strTemp.Format(_T("%d"), m_stDTInfo.m_vecTrayInfo[i].m_nTrayMatrixCol);
		::WritePrivateProfileString( strMain, _T("TRAY_MAT_COL"), strTemp, strPath );

		strTemp.Format(_T("%f"), m_stDTInfo.m_vecTrayInfo[i].m_fTraySizeX);
		::WritePrivateProfileString( strMain, _T("TRAY_SIZE_X"), strTemp, strPath );

		strTemp.Format(_T("%f"), m_stDTInfo.m_vecTrayInfo[i].m_fTraySizeY);
		::WritePrivateProfileString( strMain, _T("TRAY_SIZE_Y"), strTemp, strPath );
	}
	
	return TRUE;
}

void CDeviceTraySetting::BackupFile_TrayInfoSetting()
{
	CString strPath = GetExecuteDirectory();
	strPath += _T("\\Setting\\");
	strPath += _T("TrayIndex.ini");

	CTime CurTime = CTime::GetCurrentTime();
	CString strTemp;
	strTemp.Format(_T("TrayIndex_%04d%02d%02d_%02d%02d%02d.ini"), CurTime.GetYear(), CurTime.GetMonth(), CurTime.GetDay(), CurTime.GetHour(), CurTime.GetMinute(), CurTime.GetSecond());

	CString strBkPath = GetExecuteDirectory();
	strBkPath += _T("\\Setting\\BackUp\\");
	strBkPath += strTemp;

	::CopyFile(strPath.GetBuffer(), strBkPath.GetBuffer(), FALSE);
}

void CDeviceTraySetting::AddTrayInfo()
{
	m_stDTInfo.m_nTrayIndexCount ++;
	if(m_stDTInfo.m_nTrayIndexCount > (int)m_stDTInfo.m_vecTrayInfo.size())
	{
		stTrayInfo stTI;
		stTI.Clear();
		stTI.m_strTraySize.Format(_T("TrayInfo #%02d"),m_stDTInfo.m_nTrayIndexCount);
		m_stDTInfo.m_vecTrayInfo.push_back(stTI);
	}
	else
	{
		m_stDTInfo.m_vecTrayInfo[m_stDTInfo.m_nTrayIndexCount - 1].Clear();
		m_stDTInfo.m_vecTrayInfo[m_stDTInfo.m_nTrayIndexCount - 1].m_strTraySize.Format(_T("TrayInfo #%02d"),m_stDTInfo.m_nTrayIndexCount);
	}
}

void CDeviceTraySetting::DelTrayInfo()
{
	m_stDTInfo.m_vecTrayInfo[m_stDTInfo.m_nTrayIndexCount - 1].Clear();
	m_stDTInfo.m_nTrayIndexCount --;
}

void CDeviceTraySetting::SetTrayInfo(int nTrayIndex, LPCTSTR strText, double dTotalH, double dOverlapH, int nMatRow, int nMatCol, double dSizeX, double dSizeY)
{
	if(nTrayIndex > m_stDTInfo.m_nTrayIndexCount-1) return;

	m_stDTInfo.m_vecTrayInfo[nTrayIndex].m_strTraySize = strText;
	m_stDTInfo.m_vecTrayInfo[nTrayIndex].m_fTotalHeight = dTotalH;
	m_stDTInfo.m_vecTrayInfo[nTrayIndex].m_fOverlapHeight = dOverlapH;
	m_stDTInfo.m_vecTrayInfo[nTrayIndex].m_nTrayMatrixRow = nMatRow;
	m_stDTInfo.m_vecTrayInfo[nTrayIndex].m_fHeight = dTotalH - dOverlapH;
	m_stDTInfo.m_vecTrayInfo[nTrayIndex].m_nTrayMatrixCol = nMatCol;
	m_stDTInfo.m_vecTrayInfo[nTrayIndex].m_fTraySizeX = dSizeX;
	m_stDTInfo.m_vecTrayInfo[nTrayIndex].m_fTraySizeY = dSizeY;
}