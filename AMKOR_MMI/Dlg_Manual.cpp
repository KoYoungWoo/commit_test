// Dlg_Manual.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "AMKOR_MMI.h"
#include "Dlg_Manual.h"
#include "AMKOR_MMIDlg.h"
#include "afxdialogex.h"


// CDlg_Manual 대화 상자입니다.

IMPLEMENT_DYNAMIC(CDlg_Manual, CDialogEx)

CDlg_Manual::CDlg_Manual(CWnd* pParent /*=NULL*/)
	: CDialogEx(CDlg_Manual::IDD, pParent)
{
	m_bCtrl_FirstLoad = FALSE;

	m_nCurGroupPageNum = 0;
	m_nSelGroupIndex = 0;
	m_nSelItemIndex = 0;
	m_nCurItemPageNum = 0;
}

CDlg_Manual::~CDlg_Manual()
{
}

void CDlg_Manual::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_GB_MA_1, m_gb_1);
	DDX_Control(pDX, IDC_GB_MA_2, m_gb_2);
	DDX_Control(pDX, IDC_GB_MA_3, m_gb_3);
	DDX_Control(pDX, IDC_GC_MA_1, m_GC_1);
	DDX_Control(pDX, IDC_GC_MA_2, m_GC_2);
	DDX_Control(pDX, IDC_BTN_MA_1, m_btn_1);
	DDX_Control(pDX, IDC_BTN_MA_2, m_btn_2);
	DDX_Control(pDX, IDC_BTN_MA_3, m_btn_3);
	DDX_Control(pDX, IDC_BTN_MA_4, m_btn_4);
	DDX_Control(pDX, IDC_ST_MA_T1, m_st_T1);
	DDX_Control(pDX, IDC_ST_MA_T2, m_st_T2);
	DDX_Control(pDX, IDC_ST_MA_T3, m_st_T3);
	DDX_Control(pDX, IDC_ST_MA_T4, m_st_T4);
	DDX_Control(pDX, IDC_EDIT_MA_1, m_edit_1);
	DDX_Control(pDX, IDC_EDIT_MA_2, m_edit_2);
	DDX_Control(pDX, IDC_EDIT_MA_3, m_edit_3);
	DDX_Control(pDX, IDC_EDIT_MA_4, m_edit_4);
	DDX_Control(pDX, IDC_BTN_MA_5, m_btn_5);
	DDX_Control(pDX, IDC_BTN_MA_6, m_btn_6);
	DDX_Control(pDX, IDC_BTN_MA_7, m_btn_7);
	DDX_Control(pDX, IDC_BTN_MA_8, m_btn_8);
	DDX_Control(pDX, IDC_BTN_MA_9, m_btn_9);
	DDX_Control(pDX, IDC_BTN_MA_10, m_btn_10);
	DDX_Control(pDX, IDC_BTN_MA_11, m_btn_11);
	DDX_Control(pDX, IDC_BTN_MA_12, m_btn_12);
}


BEGIN_MESSAGE_MAP(CDlg_Manual, CDialogEx)
	ON_WM_ERASEBKGND()
	ON_WM_SIZE()
	ON_WM_TIMER()
	ON_NOTIFY(NM_CLICK, IDC_GC_MA_1, OnGridClick_GC_1)
	ON_NOTIFY(NM_CLICK, IDC_GC_MA_2, OnGridClick_GC_2)
	ON_BN_CLICKED(IDC_BTN_MA_1, &CDlg_Manual::OnBnClickedBtnMa1)
	ON_BN_CLICKED(IDC_BTN_MA_2, &CDlg_Manual::OnBnClickedBtnMa2)
	ON_BN_CLICKED(IDC_BTN_MA_3, &CDlg_Manual::OnBnClickedBtnMa3)
	ON_BN_CLICKED(IDC_BTN_MA_4, &CDlg_Manual::OnBnClickedBtnMa4)
	ON_BN_CLICKED(IDC_BTN_MA_5, &CDlg_Manual::OnBnClickedBtnMa5)
	ON_BN_CLICKED(IDC_BTN_MA_6, &CDlg_Manual::OnBnClickedBtnMa6)
	ON_BN_CLICKED(IDC_BTN_MA_7, &CDlg_Manual::OnBnClickedBtnMa7)
	ON_BN_CLICKED(IDC_BTN_MA_8, &CDlg_Manual::OnBnClickedBtnMa8)
	ON_BN_CLICKED(IDC_BTN_MA_9, &CDlg_Manual::OnBnClickedBtnMa9)
	ON_BN_CLICKED(IDC_BTN_MA_10, &CDlg_Manual::OnBnClickedBtnMa10)
	ON_BN_CLICKED(IDC_BTN_MA_11, &CDlg_Manual::OnBnClickedBtnMa11)
	ON_BN_CLICKED(IDC_BTN_MA_12, &CDlg_Manual::OnBnClickedBtnMa12)
END_MESSAGE_MAP()


// CDlg_Manual 메시지 처리기입니다.


BOOL CDlg_Manual::OnEraseBkgnd(CDC* pDC)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	// 전체 배경색
	CRect rect;
    GetClientRect(&rect);
    CBrush myBrush(CR_MAIN_BKGND);	// dialog background color
    CBrush *pOld = pDC->SelectObject(&myBrush);
    BOOL bRes  = pDC->PatBlt(0, 0, rect.Width(), rect.Height(), PATCOPY);
    pDC->SelectObject(pOld);		// restore old brush
    return FALSE;     
	//return CDialogEx::OnEraseBkgnd(pDC);
}


void CDlg_Manual::OnSize(UINT nType, int cx, int cy)
{
	CDialogEx::OnSize(nType, cx, cy);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	if(m_bCtrl_FirstLoad == TRUE) Cal_CtrlArea();
}


void CDlg_Manual::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CDialogEx::OnTimer(nIDEvent);
}


BOOL CDlg_Manual::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	Cal_CtrlArea();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


BOOL CDlg_Manual::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message == WM_SYSKEYDOWN)
	{
		if(pMsg->wParam == VK_F4) 
			return TRUE;
	}

	if(pMsg->message == WM_KEYDOWN)
	{
		if( pMsg->wParam == VK_ESCAPE ||	
			pMsg->wParam == VK_RETURN ||	
			//pMsg->wParam == VK_SPACE  ||	// Grid Ctrl Text 입력시 사용하기때문에...
			pMsg->wParam == VK_CANCEL )     
			return TRUE;        
	}

	return CDialogEx::PreTranslateMessage(pMsg);
}

void CDlg_Manual::Cal_Area()
{
	CRect ClientRect;
	GetClientRect(&ClientRect);

	int n25pW = (int)((float)ClientRect.Width()*0.25f);
	int n45pW = (int)((float)ClientRect.Width()*0.45f);

	m_rcLeft.SetRect(ClientRect.left, ClientRect.top, ClientRect.left+n25pW, ClientRect.bottom);
	m_rcMid.SetRect(m_rcLeft.right+CTRL_MARGIN4, ClientRect.top, m_rcLeft.right+n45pW, ClientRect.bottom);
	m_rcRight.SetRect(m_rcMid.right+CTRL_MARGIN4, ClientRect.top, ClientRect.right, ClientRect.bottom);
}

void CDlg_Manual::Cal_CtrlArea()
{
	Cal_Area();

	Cal_CtrlArea_Left();
	Cal_CtrlArea_Mid();
	Cal_CtrlArea_Right();

	SetManualGroupSettingValue();

	m_bCtrl_FirstLoad = TRUE;
}

void CDlg_Manual::Cal_CtrlArea_Left()
{
	CRect CalRect;
	CRect rcTop, rcBottom, rcGB, rcGBInside;

	rcGB = m_rcLeft;
	rcGB.DeflateRect(4,4,4,4);
	rcGBInside = rcGB;
	rcGBInside.DeflateRect(6,28,6,6);
	rcTop.SetRect(rcGBInside.left, rcGBInside.top, rcGBInside.right, rcGBInside.bottom - 50);
	rcBottom.SetRect(rcGBInside.left, rcGBInside.bottom - 50, rcGBInside.right, rcGBInside.bottom);
	int nDiv4W = (int)((float)rcBottom.Width()/4.0f);

	m_gb_1.SetXPGroupStyle(CXPGroupBox::XPGB_WINDOW_2);
	m_gb_1.SetBackgroundColor(CR_DARKGRAY, CR_GB_BKGND);
	m_gb_1.SetBorderColor(CR_BLACK);
	m_gb_1.SetCatptionTextColor(CR_BLACK);
	m_gb_1.MoveWindow(&rcGB);
	m_GC_1.MoveWindow(&rcTop);
	Init_GC_1(21, 2);

	CalRect.SetRect(rcBottom.left, rcBottom.top, rcBottom.left+nDiv4W, rcBottom.bottom);
	m_btn_7.SetFontBtn(CTRL_FONT, 24, FW_BOLD);
	m_btn_7.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_7.SetTextColor(CR_BLACK);
	m_btn_7.SetIconPos(0);
	m_btn_7.SetIconID(IDI_PREV);
	m_btn_7.SetIconSize(48,48);
	m_btn_7.SetFlatType(TRUE);
	m_btn_7.MoveWindow(&CalRect);
	CalRect.SetRect(CalRect.right, rcBottom.top, CalRect.right+nDiv4W, rcBottom.bottom);
	m_btn_8.SetFontBtn(CTRL_FONT, 24, FW_BOLD);
	m_btn_8.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_8.SetTextColor(CR_BLACK);
	m_btn_8.SetIconPos(0);
	m_btn_8.SetIconID(IDI_NEXT);
	m_btn_8.SetIconSize(48,48);
	m_btn_8.SetFlatType(TRUE);
	m_btn_8.MoveWindow(&CalRect);
	CalRect.SetRect(CalRect.right, rcBottom.top, CalRect.right+nDiv4W, rcBottom.bottom);
	m_btn_9.SetFontBtn(CTRL_FONT, 24, FW_BOLD);
	m_btn_9.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_9.SetTextColor(CR_BLACK);
	m_btn_9.SetIconPos(0);
	m_btn_9.SetIconID(IDI_PLUS);
	m_btn_9.SetIconSize(48,48);
	m_btn_9.SetFlatType(TRUE);
	m_btn_9.MoveWindow(&CalRect);
	CalRect.SetRect(CalRect.right, rcBottom.top, rcBottom.right, rcBottom.bottom);
	m_btn_10.SetFontBtn(CTRL_FONT, 24, FW_BOLD);
	m_btn_10.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_10.SetTextColor(CR_BLACK);
	m_btn_10.SetIconPos(0);
	m_btn_10.SetIconID(IDI_MINUS);
	m_btn_10.SetIconSize(48,48);
	m_btn_10.SetFlatType(TRUE);
	m_btn_10.MoveWindow(&CalRect);
}

void CDlg_Manual::Cal_CtrlArea_Mid()
{
	CRect CalRect;
	CRect rcTop, rcBottom, rcGB, rcGBInside;

	rcGB = m_rcMid;
	rcGB.DeflateRect(4,4,4,4);
	rcGBInside = rcGB;
	rcGBInside.DeflateRect(6,28,6,6);
	rcTop.SetRect(rcGBInside.left, rcGBInside.top, rcGBInside.right, rcGBInside.bottom - 50);
	rcBottom.SetRect(rcGBInside.left, rcGBInside.bottom - 50, rcGBInside.right, rcGBInside.bottom);

	m_gb_2.SetXPGroupStyle(CXPGroupBox::XPGB_WINDOW_2);
	m_gb_2.SetBackgroundColor(CR_DARKGRAY, CR_GB_BKGND);
	m_gb_2.SetBorderColor(CR_BLACK);
	m_gb_2.SetCatptionTextColor(CR_BLACK);
	m_gb_2.MoveWindow(&rcGB);
	m_GC_2.MoveWindow(&rcTop);
	Init_GC_2(21,4);

	CalRect.SetRect(rcBottom.left, rcBottom.top, rcBottom.left+100, rcBottom.bottom);
	m_btn_11.SetFontBtn(CTRL_FONT, 24, FW_BOLD);
	m_btn_11.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_11.SetTextColor(CR_BLACK);
	m_btn_11.SetIconPos(0);
	m_btn_11.SetIconID(IDI_PREV);
	m_btn_11.SetIconSize(48,48);
	m_btn_11.SetFlatType(TRUE);
	m_btn_11.MoveWindow(&CalRect);
	CalRect.SetRect(CalRect.right, rcBottom.top, CalRect.right+100, rcBottom.bottom);
	m_btn_12.SetFontBtn(CTRL_FONT, 24, FW_BOLD);
	m_btn_12.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_12.SetTextColor(CR_BLACK);
	m_btn_12.SetIconPos(0);
	m_btn_12.SetIconID(IDI_NEXT);
	m_btn_12.SetIconSize(48,48);
	m_btn_12.SetFlatType(TRUE);
	m_btn_12.MoveWindow(&CalRect);
}

void CDlg_Manual::Cal_CtrlArea_Right()
{
	CRect CalRect;
	CRect rcTop, rcGB, rcGBInside;

	rcGB = m_rcRight;
	rcGB.DeflateRect(4,4,4,350);
	rcGBInside = rcGB;
	rcGBInside.DeflateRect(6,28,6,6);

	m_gb_3.SetXPGroupStyle(CXPGroupBox::XPGB_WINDOW_2);
	m_gb_3.SetBackgroundColor(CR_DARKGRAY, CR_GB_BKGND);
	m_gb_3.SetBorderColor(CR_BLACK);
	m_gb_3.SetCatptionTextColor(CR_BLACK);
	m_gb_3.MoveWindow(&rcGB);

	int nDiv12pH = (int)((float)rcGBInside.Height()/12.0f);
	CRect rcDivH[12];

	rcDivH[0].SetRect(rcGBInside.left, rcGBInside.top, rcGBInside.right, rcGBInside.top+nDiv12pH);
	rcDivH[1].SetRect(rcGBInside.left, rcDivH[0].bottom, rcGBInside.right, rcDivH[0].bottom+nDiv12pH);
	rcDivH[2].SetRect(rcGBInside.left, rcDivH[1].bottom, rcGBInside.right, rcDivH[1].bottom+nDiv12pH);
	rcDivH[3].SetRect(rcGBInside.left, rcDivH[2].bottom, rcGBInside.right, rcDivH[2].bottom+nDiv12pH);
	rcDivH[4].SetRect(rcGBInside.left, rcDivH[3].bottom, rcGBInside.right, rcDivH[3].bottom+nDiv12pH);
	rcDivH[5].SetRect(rcGBInside.left, rcDivH[4].bottom, rcGBInside.right, rcDivH[4].bottom+nDiv12pH);
	rcDivH[6].SetRect(rcGBInside.left, rcDivH[5].bottom, rcGBInside.right, rcDivH[5].bottom+nDiv12pH);
	rcDivH[7].SetRect(rcGBInside.left, rcDivH[6].bottom, rcGBInside.right, rcDivH[6].bottom+nDiv12pH);
	rcDivH[8].SetRect(rcGBInside.left, rcDivH[7].bottom, rcGBInside.right, rcDivH[7].bottom+nDiv12pH);
	rcDivH[9].SetRect(rcGBInside.left, rcDivH[8].bottom, rcGBInside.right, rcDivH[8].bottom+nDiv12pH);
	rcDivH[10].SetRect(rcGBInside.left, rcDivH[9].bottom, rcGBInside.right, rcDivH[9].bottom+nDiv12pH);
	rcDivH[11].SetRect(rcGBInside.left, rcDivH[10].bottom, rcGBInside.right, rcGBInside.bottom);

	m_st_T1.SetBkColor(CR_DARKSLATEGRAY);
	m_st_T1.SetTextColor(CR_WHITE);
	m_st_T1.SetFont(CTRL_FONT,22,FW_BOLD);
	m_st_T1.MoveWindow(&rcDivH[0]);

	CalRect.SetRect(rcDivH[1].left, rcDivH[1].top, rcDivH[1].right-40, rcDivH[1].bottom);
	m_edit_1.MoveWindow(&CalRect);
	m_edit_1.SetFontStyle(CTRL_FONT, 22, FW_BOLD);
	m_edit_1.SetColor(CR_BLACK, CR_WHITE);
	m_edit_1.SetNumeric(FALSE);
	CalRect.SetRect(rcDivH[1].right-40, rcDivH[1].top, rcDivH[1].right, rcDivH[1].bottom);
	m_btn_6.SetFontBtn(CTRL_FONT, 24, FW_BOLD);
	m_btn_6.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_6.SetTextColor(CR_BLACK);
	m_btn_6.SetIconPos(0);
	m_btn_6.SetIconID(IDI_MODIFY);
	m_btn_6.SetIconSize(32,32);
	m_btn_6.SetFlatType(TRUE);
	m_btn_6.MoveWindow(&CalRect);

	m_st_T2.SetBkColor(CR_DARKSLATEGRAY);
	m_st_T2.SetTextColor(CR_WHITE);
	m_st_T2.SetFont(CTRL_FONT,22,FW_BOLD);
	m_st_T2.MoveWindow(&rcDivH[2]);
	m_edit_2.MoveWindow(&rcDivH[3]);
	m_edit_2.SetFontStyle(CTRL_FONT, 22, FW_BOLD);
	m_edit_2.SetColor(CR_BLACK, CR_WHITE);
	m_edit_2.SetNumeric(FALSE);

	m_st_T3.SetBkColor(CR_DARKSLATEGRAY);
	m_st_T3.SetTextColor(CR_WHITE);
	m_st_T3.SetFont(CTRL_FONT,22,FW_BOLD);
	m_st_T3.MoveWindow(&rcDivH[4]);
	m_edit_3.MoveWindow(&rcDivH[5]);
	m_edit_3.SetFontStyle(CTRL_FONT, 22, FW_BOLD);
	m_edit_3.SetColor(CR_BLACK, CR_WHITE);
	m_edit_3.SetNumeric(TRUE);
	m_edit_3.SetFloatStyle(FALSE);

	m_st_T4.SetBkColor(CR_DARKSLATEGRAY);
	m_st_T4.SetTextColor(CR_WHITE);
	m_st_T4.SetFont(CTRL_FONT,22,FW_BOLD);
	m_st_T4.MoveWindow(&rcDivH[6]);
	m_edit_4.MoveWindow(&rcDivH[7]);
	m_edit_4.SetFontStyle(CTRL_FONT, 22, FW_BOLD);
	m_edit_4.SetColor(CR_BLACK, CR_WHITE);
	m_edit_4.SetNumeric(TRUE);
	m_edit_4.SetFloatStyle(FALSE);

	int nDiv8_50pW = (int)((float)rcDivH[8].Width()*0.5f);
	CalRect.SetRect(rcDivH[8].left, rcDivH[8].top, rcDivH[8].left+nDiv8_50pW, rcDivH[9].bottom);
	m_btn_1.SetFontBtn(CTRL_FONT, 24, FW_BOLD);
	m_btn_1.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_1.SetTextColor(CR_BLACK);
	m_btn_1.SetIconPos(0);
	m_btn_1.SetIconID(IDI_MODIFY);
	m_btn_1.SetIconSize(48,48);
	m_btn_1.SetFlatType(TRUE);
	m_btn_1.MoveWindow(&CalRect);
	m_btn_1.EnableWindow(FALSE);
	CalRect.SetRect(rcDivH[8].left+nDiv8_50pW, rcDivH[8].top, rcDivH[8].right, rcDivH[9].bottom);
	m_btn_2.SetFontBtn(CTRL_FONT, 24, FW_BOLD);
	m_btn_2.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_2.SetTextColor(CR_BLACK);
	m_btn_2.SetIconPos(0);
	m_btn_2.SetIconID(IDI_ERASE);
	m_btn_2.SetIconSize(48,48);
	m_btn_2.SetFlatType(TRUE);
	m_btn_2.MoveWindow(&CalRect);
	m_btn_2.EnableWindow(FALSE);

	CalRect.SetRect(rcDivH[10].left, rcDivH[10].top, rcDivH[10].right, rcDivH[11].bottom);
	m_btn_3.SetFontBtn(CTRL_FONT, 24, FW_BOLD);
	m_btn_3.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_3.SetTextColor(CR_BLACK);
	m_btn_3.SetIconPos(0);
	m_btn_3.SetIconID(IDI_WORK);
	m_btn_3.SetIconSize(48,48);
	m_btn_3.SetFlatType(TRUE);
	m_btn_3.MoveWindow(&CalRect);
	m_btn_3.EnableWindow(FALSE);

	CalRect.SetRect(m_rcRight.left, m_rcRight.bottom - 60, m_rcRight.left + 120, m_rcRight.bottom);
	CalRect.DeflateRect(2,2);
	m_btn_4.SetFontBtn(CTRL_FONT, 24, FW_BOLD);
	m_btn_4.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_4.SetTextColor(CR_BLACK);
	m_btn_4.SetIconPos(0);
	m_btn_4.SetIconID(IDI_SAVE);
	m_btn_4.SetIconSize(48,48);
	m_btn_4.SetFlatType(TRUE);
	m_btn_4.MoveWindow(&CalRect);

	CalRect.SetRect(m_rcRight.right - 140, m_rcRight.bottom - 60, m_rcRight.right, m_rcRight.bottom);
	CalRect.DeflateRect(2,2);
	m_btn_5.SetFontBtn(CTRL_FONT, 24, FW_BOLD);
	m_btn_5.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_5.SetTextColor(CR_BLACK);
	m_btn_5.SetIconPos(0);
	m_btn_5.SetIconID(IDI_CLOSE);
	m_btn_5.SetIconSize(48,48);
	m_btn_5.SetFlatType(TRUE);
	m_btn_5.MoveWindow(&CalRect);
}

bool CDlg_Manual::Init_GC_1(int nRows, int nCols)
{
	BOOL m_bEditable = FALSE;
    BOOL m_bListMode = TRUE;
    int m_nRows = nRows;
    int m_nCols = nCols;
    int m_nFixRows = 1;
    int m_nFixCols = 1;

    m_GC_1.SetEditable(m_bEditable);
    m_GC_1.SetListMode(m_bListMode);
    m_GC_1.EnableDragAndDrop(FALSE);
    m_GC_1.SetTextBkColor(RGB(0xFF, 0xFF, 0xE0));

    m_GC_1.SetRowCount(m_nRows);
    m_GC_1.SetColumnCount(m_nCols);
    m_GC_1.SetFixedRowCount(m_nFixRows);
    m_GC_1.SetFixedColumnCount(m_nFixCols);

	m_GC_1.EnableSelection(false);
	m_GC_1.SetSingleColSelection(true);
	m_GC_1.SetSingleRowSelection(true);
	m_GC_1.SetFixedColumnSelection(false);
    m_GC_1.SetFixedRowSelection(false);

	m_GC_1.SetRowResize(false);
	m_GC_1.SetColumnResize(false);

	CRect ClientRect;
	GetDlgItem(IDC_GC_MA_1)->GetWindowRect(&ClientRect);
	if(ClientRect.Width() < 4) return false;
	if(ClientRect.Height() < 4) return false;
	int nClientWidth = ClientRect.Width()-4;
	int nClientHeight = ClientRect.Height()-4;
	int n10pW = (int)(nClientWidth*0.1f);
	int nRemainW = nClientWidth - n10pW;
	//int nCellW = nClientWidth/m_nCols;
	int nCellH = nClientHeight/m_nRows;
	//int nLastCellW = nClientWidth-nCellW*(m_nCols-1);
	int nLastCellH = nClientHeight-nCellH*(m_nRows-1);
	//int nRemainW = nLastCellW - nCellW;
	int nRemainH = nLastCellH - nCellH;
	for (int i = 0; i < m_GC_1.GetRowCount(); i++) 
	{
		if(i == m_nRows-1)
		{
				m_GC_1.SetRowHeight(i,nCellH);
		}
		else 
		{
			if(nRemainH > 0)
			{
				m_GC_1.SetRowHeight(i,nCellH + 1);
				nRemainH --;
			}
			else
				m_GC_1.SetRowHeight(i,nCellH);
		}
	}
	for (int j = 0; j < m_GC_1.GetColumnCount(); j++)
	{
		if(j == 0) m_GC_1.SetColumnWidth(j,n10pW);
		else if(j == 1) m_GC_1.SetColumnWidth(j,nRemainW);
	}
    DWORD dwTextStyle = DT_CENTER|DT_VCENTER|DT_SINGLELINE;

    // fill rows/cols with text
    for (int row = 0; row < m_GC_1.GetRowCount(); row++) 
	{
        for (int col = 0; col < m_GC_1.GetColumnCount(); col++) 
		{ 
            GV_ITEM Item;
            Item.mask = GVIF_TEXT|GVIF_FORMAT|GVIF_BKCLR;
            Item.row = row;
            Item.col = col;
			Item.nFormat = dwTextStyle;
			Item.crBkClr = RGB(0xFF, 0xFF, 0xE0);

			if(row == 0)
			{
				if(col == 0) Item.strText.Format(_T("#"));
				else if(col == 1) Item.strText.Format(_T("NAME"));
				Item.crBkClr = CR_SLATEGRAY;
			}
			else
			{
			}

			if(col == 0) Item.crBkClr = CR_SLATEGRAY;
			m_GC_1.SetItemFgColour(row, col, RGB(0,0,0));
			m_GC_1.SetItemFont(row, col, CTRL_FONT, 18, FW_BOLD);
            m_GC_1.SetItem(&Item);  
        }
    }
       
    return TRUE;  // return TRUE  unless you set the focus to a control
}

void CDlg_Manual::OnGridClick_GC_1(NMHDR *pNotifyStruct, LRESULT* pResult)
{
	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*) pNotifyStruct;

	int nSelRow = pItem->iRow;
	int nSelCol = pItem->iColumn;

	if(nSelRow > 0) nSelRow += m_nCurGroupPageNum*20;

	if(m_nSelGroupIndex != nSelRow)
	{
		stMotorManual_Info stMMI = CMotorManualSetting::Instance()->GetMotorManualInfo();
		if(nSelRow > stMMI.m_nManualCount)
		{
			m_nSelGroupIndex = 0;
			m_nSelItemIndex = 0;
			m_nCurItemPageNum = 0;

			Init_GC_2(21,4);
			Update_SelItemIndex(m_nSelItemIndex);
			return;
		}

		m_nSelGroupIndex = nSelRow;
		
		Init_GC_2(21,4);
		if(nSelRow != 0) SetManualItemSettingValue(m_nSelGroupIndex);

		m_nSelItemIndex = 0;
		m_nCurItemPageNum = 0;
		Update_SelItemIndex(m_nSelItemIndex + m_nCurItemPageNum*20);
	}
}

bool CDlg_Manual::Init_GC_2(int nRows, int nCols)
{
	BOOL m_bEditable = FALSE;
    BOOL m_bListMode = TRUE;
    int m_nRows = nRows;
    int m_nCols = nCols;
    int m_nFixRows = 1;
    int m_nFixCols = 1;

    m_GC_2.SetEditable(m_bEditable);
    m_GC_2.SetListMode(m_bListMode);
    m_GC_2.EnableDragAndDrop(FALSE);
    m_GC_2.SetTextBkColor(RGB(0xFF, 0xFF, 0xE0));

    m_GC_2.SetRowCount(m_nRows);
    m_GC_2.SetColumnCount(m_nCols);
    m_GC_2.SetFixedRowCount(m_nFixRows);
    m_GC_2.SetFixedColumnCount(m_nFixCols);

	m_GC_2.EnableSelection(false);
	m_GC_2.SetSingleColSelection(true);
	m_GC_2.SetSingleRowSelection(true);
	m_GC_2.SetFixedColumnSelection(false);
    m_GC_2.SetFixedRowSelection(false);

	m_GC_2.SetRowResize(false);
	m_GC_2.SetColumnResize(false);

	CRect ClientRect;
	GetDlgItem(IDC_GC_MA_2)->GetWindowRect(&ClientRect);
	if(ClientRect.Width() < 4) return false;
	if(ClientRect.Height() < 4) return false;
	int nClientWidth = ClientRect.Width()-4;
	int nClientHeight = ClientRect.Height()-4;
	int n10pW = (int)(nClientWidth*0.1f);
	int n60pW = (int)(nClientWidth*0.6f);
	int n15pW = (int)(nClientWidth*0.15f);
	int nRemainW = nClientWidth - n10pW - n60pW - n15pW;
	//int nCellW = nClientWidth/m_nCols;
	int nCellH = nClientHeight/m_nRows;
	//int nLastCellW = nClientWidth-nCellW*(m_nCols-1);
	int nLastCellH = nClientHeight-nCellH*(m_nRows-1);
	//int nRemainW = nLastCellW - nCellW;
	int nRemainH = nLastCellH - nCellH;
	for (int i = 0; i < m_GC_2.GetRowCount(); i++) 
	{
		if(i == m_nRows-1)
		{
				m_GC_2.SetRowHeight(i,nCellH);
		}
		else 
		{
			if(nRemainH > 0)
			{
				m_GC_2.SetRowHeight(i,nCellH + 1);
				nRemainH --;
			}
			else
				m_GC_2.SetRowHeight(i,nCellH);
		}
	}
	for (int j = 0; j < m_GC_2.GetColumnCount(); j++)
	{
		if(j == 0) m_GC_2.SetColumnWidth(j,n10pW);
		else if(j == 1) m_GC_2.SetColumnWidth(j,n60pW);
		else if(j == 2) m_GC_2.SetColumnWidth(j,n15pW);
		else if(j == 3) m_GC_2.SetColumnWidth(j,nRemainW);
	}
    DWORD dwTextStyle = DT_CENTER|DT_VCENTER|DT_SINGLELINE;

    // fill rows/cols with text
    for (int row = 0; row < m_GC_2.GetRowCount(); row++) 
	{
        for (int col = 0; col < m_GC_2.GetColumnCount(); col++) 
		{ 
            GV_ITEM Item;
            Item.mask = GVIF_TEXT|GVIF_FORMAT|GVIF_BKCLR;
            Item.row = row;
            Item.col = col;
			Item.nFormat = dwTextStyle;
			Item.crBkClr = RGB(0xFF, 0xFF, 0xE0);

			if(row == 0)
			{
				if(col == 0) Item.strText.Format(_T("#"));
				else if(col == 1) Item.strText.Format(_T("NAME"));
				else if(col == 2) Item.strText.Format(_T("BIT/LAMP"));
				else if(col == 3) Item.strText.Format(_T("STATUS"));
				Item.crBkClr = CR_SLATEGRAY;
			}
			else
			{
			}

			if(col == 0) Item.crBkClr = CR_SLATEGRAY;
			m_GC_2.SetItemFgColour(row, col, RGB(0,0,0));
			m_GC_2.SetItemFont(row, col, CTRL_FONT, 18, FW_BOLD);
            m_GC_2.SetItem(&Item);  
        }
    }
       
    return TRUE;  // return TRUE  unless you set the focus to a control
}

void CDlg_Manual::OnGridClick_GC_2(NMHDR *pNotifyStruct, LRESULT* pResult)
{
	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*) pNotifyStruct;

	int nSelRow = pItem->iRow;
	int nSelCol = pItem->iColumn;

	m_nSelItemIndex = nSelRow;

	Update_SelItemIndex(m_nSelItemIndex + m_nCurItemPageNum*20);
}

void CDlg_Manual::SetManualGroupSettingValue()
{
	stMotorManual_Info stMMI = CMotorManualSetting::Instance()->GetMotorManualInfo();
	if(stMMI.m_nManualCount <= 0) return;
	if(m_nSelGroupIndex > stMMI.m_nManualCount) return;

	// 
	DWORD dwTextStyle = DT_CENTER|DT_VCENTER|DT_SINGLELINE;
    for (int row = 0; row < m_GC_1.GetRowCount(); row++) 
	{
        for (int col = 0; col < m_GC_1.GetColumnCount(); col++) 
		{ 
            GV_ITEM Item;
            Item.mask = GVIF_TEXT|GVIF_FORMAT|GVIF_BKCLR;
            Item.row = row;
            Item.col = col;
			Item.nFormat = dwTextStyle;
			Item.crBkClr = RGB(0xFF, 0xFF, 0xE0);

			if(row == 0)
			{
				if(col == 0) Item.strText.Format(_T("#"));
				else if(col == 1) Item.strText.Format(_T("NAME"));
				Item.crBkClr = CR_SLATEGRAY;
			}
			else
			{
				if(col == 0)
				{
					Item.strText.Format(_T("%d"), row + m_nCurGroupPageNum*20);
					Item.crBkClr = CR_SLATEGRAY;
				}
				else if(col == 1)
				{
					if(row + m_nCurGroupPageNum*20 <= stMMI.m_nManualCount)
					{
						Item.strText = stMMI.m_vecManualInfo[row + m_nCurGroupPageNum*20 - 1].m_strManualName;
						Item.crBkClr = CR_GAINSBORO;
					}
				}
			}

			m_GC_1.SetItemFgColour(row, col, RGB(0,0,0));
            m_GC_1.SetItem(&Item);  
        }
    }

	m_GC_1.Invalidate();
}

void CDlg_Manual::SetManualItemSettingValue(int nGroupIndex)
{
	stMotorManual_Info stMMI = CMotorManualSetting::Instance()->GetMotorManualInfo();
	if(nGroupIndex > stMMI.m_nManualCount) return;

	DWORD dwTextStyle = DT_CENTER|DT_VCENTER|DT_SINGLELINE;
    for (int row = 0; row < m_GC_2.GetRowCount(); row++) 
	{
        for (int col = 0; col < m_GC_2.GetColumnCount(); col++) 
		{ 
            GV_ITEM Item;
            Item.mask = GVIF_TEXT|GVIF_FORMAT|GVIF_BKCLR;
            Item.row = row;
            Item.col = col;
			Item.nFormat = dwTextStyle;
			Item.crBkClr = RGB(0xFF, 0xFF, 0xE0);
			Item.strText.Empty();

			if(row == 0)
			{
				if(col == 0) Item.strText.Format(_T("#"));
				else if(col == 1) Item.strText.Format(_T("NAME"));
				else if(col == 2) Item.strText.Format(_T("BIT/LAMP"));
				else if(col == 3) Item.strText.Format(_T("STATUS"));
				Item.crBkClr = CR_SLATEGRAY;
			}
			else
			{
				if(col == 0)
				{
					Item.strText.Format(_T("%d"), row+m_nCurItemPageNum*20);
					Item.crBkClr = CR_SLATEGRAY;
				}
				else if(col == 1)
				{
					if(m_nCurItemPageNum != 2)
					{
						Item.strText = stMMI.m_vecManualInfo[m_nSelGroupIndex-1].m_stManualItem[row+m_nCurItemPageNum*20-1].m_strItemName;
					}
				}
				else if(col == 2)
				{
					if(m_nCurItemPageNum != 2)
					{
						Item.strText.Format(_T("%d/%d"), stMMI.m_vecManualInfo[m_nSelGroupIndex-1].m_stManualItem[row+m_nCurItemPageNum*20-1].m_nAddress_Bit, stMMI.m_vecManualInfo[m_nSelGroupIndex-1].m_stManualItem[row+m_nCurItemPageNum*20-1].m_nAddress_Lamp);
					}
				}
				else if(col == 3)
				{
					if(m_nCurItemPageNum != 2)
					{
						if(stMMI.m_vecManualInfo[m_nSelGroupIndex-1].m_stManualItem[row+m_nCurItemPageNum*20-1].m_nLampOnOff)
						{
							Item.crBkClr = CR_BLUE;
						}
					}
				}
			}

			m_GC_2.SetItemFgColour(row, col, RGB(0,0,0));
            m_GC_2.SetItem(&Item);  
        }
    }

	m_GC_2.Invalidate();
}

void CDlg_Manual::Update_SelItemIndex(int nItemIndex)
{
	if(m_nSelGroupIndex == 0 || nItemIndex == 0 || nItemIndex > 50)
	{
		m_edit_1.SetWindowText(_T(""));
		m_edit_2.SetWindowText(_T(""));
		m_edit_3.SetWindowText(_T(""));
		m_edit_4.SetWindowText(_T(""));

		m_btn_1.EnableWindow(FALSE);
		m_btn_2.EnableWindow(FALSE);
		m_btn_3.EnableWindow(FALSE);
	}
	else
	{
		CString strTemp;
		stMotorManual_Info stMMI = CMotorManualSetting::Instance()->GetMotorManualInfo();
		m_edit_1.SetWindowText(stMMI.m_vecManualInfo[m_nSelGroupIndex-1].m_strManualName);
		m_edit_2.SetWindowText(stMMI.m_vecManualInfo[m_nSelGroupIndex-1].m_stManualItem[nItemIndex-1].m_strItemName);
		strTemp.Format(_T("%d"), stMMI.m_vecManualInfo[m_nSelGroupIndex-1].m_stManualItem[nItemIndex-1].m_nAddress_Bit);
		m_edit_3.SetWindowText(strTemp);
		strTemp.Format(_T("%d"), stMMI.m_vecManualInfo[m_nSelGroupIndex-1].m_stManualItem[nItemIndex-1].m_nAddress_Lamp);
		m_edit_4.SetWindowText(strTemp);

		m_btn_1.EnableWindow(FALSE);
		m_btn_2.EnableWindow(FALSE);
		if(stMMI.m_vecManualInfo[m_nSelGroupIndex-1].m_stManualItem[nItemIndex-1].m_nAddress_Bit == 0 || stMMI.m_vecManualInfo[m_nSelGroupIndex-1].m_stManualItem[nItemIndex-1].m_nAddress_Lamp == 0)
		{
			m_btn_2.EnableWindow(FALSE);
			m_btn_3.EnableWindow(FALSE);
		}
		else
		{
			m_btn_2.EnableWindow(TRUE);
			m_btn_3.EnableWindow(TRUE);
		}
	}
}

void CDlg_Manual::Update_PLC_Value()
{
	stMotorManual_Info stMMI = CMotorManualSetting::Instance()->GetMotorManualInfo();
	if(m_nSelGroupIndex == 0) return;
	if(m_nSelGroupIndex > stMMI.m_nManualCount) return;

	int nOnOff = 0;
	for(int i=0;i<50;i++)
	{
		nOnOff = 0;
		if(stMMI.m_vecManualInfo[m_nSelGroupIndex-1].m_stManualItem[i].m_nAddress_Lamp > 0)
		{
			nOnOff = (int)CDataManager::Instance()->m_PLCReaderL_Manual.IsON(stMMI.m_vecManualInfo[m_nSelGroupIndex-1].m_stManualItem[i].m_nAddress_Lamp);
		}
		CMotorManualSetting::Instance()->SetManualItemStatus(m_nSelGroupIndex,i+1,nOnOff);
	}

	SetManualItemSettingValue(m_nSelGroupIndex);
}

void CDlg_Manual::OnBnClickedBtnMa1()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString strText, strTemp;
	m_edit_2.GetWindowText(strText);
	m_edit_3.GetWindowText(strTemp);
	int nBitAddress = _wtoi(strTemp);
	m_edit_4.GetWindowText(strTemp);
	int nLampAddress = _wtoi(strTemp);
	int nCalItemIndex = m_nSelItemIndex + m_nCurItemPageNum*20;
	CMotorManualSetting::Instance()->SetManualItemModify(m_nSelGroupIndex, nCalItemIndex, strText, nBitAddress, nLampAddress);

	SetManualItemSettingValue(m_nSelGroupIndex);

	m_btn_1.EnableWindow(FALSE);
}



BOOL CDlg_Manual::OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	NMHDR *pNMHDR = (NMHDR*)lParam;	

	int nPLCNum = CAppSetting::Instance()->GetGroupNumber();
	if( pNMHDR->code == TCN_SELCHANGE )
	{
		if(wParam == IDC_BTN_MA_3) // ACTIVATE
		{
			stMotorManual_Info stMMI = CMotorManualSetting::Instance()->GetMotorManualInfo();
			int nCalItemIndex = m_nSelItemIndex + m_nCurItemPageNum*20;
			int nStartLAddressBit = stMMI.m_vecManualInfo[m_nSelGroupIndex-1].m_stManualItem[nCalItemIndex-1].m_nAddress_Bit;
			CDataManager::Instance()->PLC_Write_L(nPLCNum, nStartLAddressBit, TRUE);
		}
	}
	if( pNMHDR->code == TCN_SELCHANGING )
	{
		if(wParam == IDC_BTN_MA_3) // ACTIVATE
		{
			stMotorManual_Info stMMI = CMotorManualSetting::Instance()->GetMotorManualInfo();
			int nCalItemIndex = m_nSelItemIndex + m_nCurItemPageNum*20;
			int nStartLAddressBit = stMMI.m_vecManualInfo[m_nSelGroupIndex-1].m_stManualItem[nCalItemIndex-1].m_nAddress_Bit;
			CDataManager::Instance()->PLC_Write_L(nPLCNum, nStartLAddressBit, FALSE);
		}
	}

	if( pNMHDR->code == WM_EDIT_MODIFIED )
	{
		if(wParam == IDC_EDIT_MA_2 || wParam == IDC_EDIT_MA_3 || wParam == IDC_EDIT_MA_4) 
		{
			m_btn_1.EnableWindow(TRUE);
		}
	}

	return CDialogEx::OnNotify(wParam, lParam, pResult);
}


void CDlg_Manual::OnBnClickedBtnMa2()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_edit_2.SetWindowText(_T(""));
	m_edit_3.SetWindowText(_T(""));
	m_edit_4.SetWindowText(_T(""));
	int nCalItemIndex = m_nSelItemIndex + m_nCurItemPageNum*20;
	CMotorManualSetting::Instance()->SetManualItemModify(m_nSelGroupIndex, nCalItemIndex, _T(""), 0, 0);

	SetManualItemSettingValue(m_nSelGroupIndex);

	m_btn_2.EnableWindow(FALSE);
}


void CDlg_Manual::OnBnClickedBtnMa3()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}


void CDlg_Manual::OnBnClickedBtnMa4()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_btn_4.EnableWindow(FALSE);
	CMotorManualSetting::Instance()->Save_ManualSetting();
	m_btn_4.EnableWindow(TRUE);
}


void CDlg_Manual::OnBnClickedBtnMa5()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CAMKOR_MMIDlg *)AfxGetMainWnd())->SendMessage(WM_CHANGESCREEN, 0, 0);
}


void CDlg_Manual::OnBnClickedBtnMa6()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	stMotorManual_Info stMMI = CMotorManualSetting::Instance()->GetMotorManualInfo();
	if(m_nSelGroupIndex == 0) return;
	if(m_nSelGroupIndex > stMMI.m_nManualCount) return;

	CString strText;
	m_edit_1.GetWindowText(strText);
	CMotorManualSetting::Instance()->SetManualNameModify(m_nSelGroupIndex, strText);

	SetManualGroupSettingValue();
}


void CDlg_Manual::OnBnClickedBtnMa7()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_nCurGroupPageNum --;
	if(m_nCurGroupPageNum < 0) m_nCurGroupPageNum = 0;
	else SetManualGroupSettingValue();
}


void CDlg_Manual::OnBnClickedBtnMa8()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	stMotorManual_Info stMMI = CMotorManualSetting::Instance()->GetMotorManualInfo();
	m_nCurGroupPageNum ++;
	int nMaxPage = stMMI.m_nManualCount/20;
	if(stMMI.m_nManualCount%20 > 0) nMaxPage ++;
	if(m_nCurGroupPageNum > nMaxPage - 1) m_nCurGroupPageNum = nMaxPage - 1;
	else SetManualGroupSettingValue();
}


void CDlg_Manual::OnBnClickedBtnMa9()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CMotorManualSetting::Instance()->AddManualInfo();

	SetManualGroupSettingValue();
}


void CDlg_Manual::OnBnClickedBtnMa10()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CMotorManualSetting::Instance()->DelManualInfo();

	SetManualGroupSettingValue();
}


void CDlg_Manual::OnBnClickedBtnMa11()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_nCurItemPageNum --;
	if(m_nCurItemPageNum < 0) m_nCurItemPageNum = 0;
	else Update_SelItemIndex(m_nSelItemIndex + m_nCurItemPageNum*20);
}


void CDlg_Manual::OnBnClickedBtnMa12()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_nCurItemPageNum ++;
	if(m_nCurItemPageNum > 2) m_nCurItemPageNum = 2;
	else Update_SelItemIndex(m_nSelItemIndex + m_nCurItemPageNum*20);
}
