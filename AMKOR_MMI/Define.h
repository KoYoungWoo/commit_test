//////////////////////////////////////////////////////
// DEFINE.H

/*********************************************************/

/*********************************************************/

#define		RESULT_OK		0x00

#define		WM_TERMMSG_ACK		WM_USER+10000

#define HSMS			_T("HSMS")
#define HSMS_PORT		_T("PORT")
#define HSMS_DEVICEID	_T("DEVICEID")
#define HSMS_LINKTEST	_T("LINKTEST")
#define HSMS_RETRY		_T("RETRY")
#define HSMS_T3			_T("T3")
#define HSMS_T5			_T("T5")
#define HSMS_T6			_T("T6")
#define HSMS_T7			_T("T7")
#define HSMS_T8			_T("T8")

/////////////////////////////////////////////////////////////////////////////////////////
// FCB LOADER/UNLOADER
//SVID - status variable
#define SVID_PORT					1001
#define SVID_TRAY_STACKER			1101
#define SVID_END_CAP				1102
#define SVID_BANDING				1103
#define SVID_HIC					1104
#define SVID_MBB					1105
#define SVID_BOX					1106
#define SVID_SILICAGEL				1107
#define SVID_LABEL					1108
#define SVID_RIBBON					1109
#define SVID_BUBBLE_SHEET			1110
#define SVID_RFID_FULL_STATUS		1201
#define SVID_BARCODE				1301
#define SVID_LOT_ID					1401
#define SVID_LOT_QTY				1402
#define SVID_TRAY_QTY				1403
#define SVID_BUNDLE_TRAY_QTY		1404
#define SVID_OPER_CODE				1501
#define SVID_INNER_BOX_ID			1502
#define SVID_INNER_BOX_DEVICE_QTY	1503
#define SVID_PARTIAL				1504
#define SVID_FLAG					1505
#define SVID_INNER_BOX_QTY			1801
#define SVID_EQUIPMENT_STATUS		810
//#define SVID_CONTROL_STATUS			820
#define SVID_CURRENT_RECIPE			2000
#define SVID_PREVIOUS_RECIPE		2001
#define SVID_CONTROL_STATE			4

//CEID

//ECID 
#define ECID_PORT					3001
#define ECID_DEVICEID				3002
#define ECID_T3						3003
#define ECID_T5						3005
#define ECID_T6						3006
#define ECID_T7						3007
#define ECID_T8						3008
#define ECID_LINKTEST				3009
#define ECID_RETRY					3010

#define LOCAL_HOST					L"127.0.0.1"

#define ZEBRA_PORT					9100
#define ZEBRA_IP_1					L"192.168.0.11"
#define ZEBRA_IP_2					L"192.168.0.12"
#define ZEBRA_IP_3					L"192.168.0.13"
#define ZEBRA_IP_4					L"192.168.0.14"
#define ZEBRA_IP_5					L"192.168.0.15"

//Device/프린터별 (Label Home) 
#define LH_ARUBA_TRAY				L"^LH0,30"
#define LH_ARUBA_MBB				L"^LH125,115"
#define LH_ARUBA_BOX				L"^LH450,120"

#define LH_STD_TRAY					L"^LH0,0"
#define LH_STD_MBB					L"^LH0,30" //5번째 프린터 192.168.0.15 offset 추가
#define LH_STD_BOX					L"^LH315,30"

#define LH_NVIDIA_ALL				L"LH30,10"

//STD Device/Tray (Label Shift)
#define LS_STD_01					L"^LS210"
#define LS_STD_02					L"^LS100"
#define LS_STD_CLEAR				L"^LS0"
