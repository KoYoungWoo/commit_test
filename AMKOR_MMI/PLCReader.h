#pragma once

class CMxComp3;
class CPLCReader
{
protected:
	CRITICAL_SECTION m_csBuffer;
	class Lock
	{
	protected:
		CRITICAL_SECTION& m_cs;
	public:
		Lock(CRITICAL_SECTION& cs) : m_cs(cs) { ::EnterCriticalSection(&m_cs); }
		~Lock() { ::LeaveCriticalSection(&m_cs); }
	};
public:
	CPLCReader() { ::InitializeCriticalSection(&m_csBuffer); }
	virtual ~CPLCReader() { ::DeleteCriticalSection(&m_csBuffer); }
};
class CPLCReaderD : public CPLCReader
{
protected:
	SHORT*	m_pBuffer;
	int		m_nStartAddress;
	int		m_nEndAddress;
	CMxComp3*	m_pMxComp3;

public:
	CPLCReaderD(void);
	virtual ~CPLCReaderD(void);

	void SetPLCModule(CMxComp3* pMxComp3) { m_pMxComp3 = pMxComp3; }
	void SetAddress( int nStartAddress, int nEndAddress );

	BOOL Read();

	SHORT GetValue( int nAddress );
	LONG GetValue2( int nAddress );
	MADDRESS GetValueM( int nAddress );

	CString GetString( int nAddress, int nSize);
};

class CPLCReaderZR : public CPLCReader
{
protected:
	SHORT*	m_pBuffer;
	int		m_nStartAddress;
	int		m_nEndAddress;
	CMxComp3*	m_pMxComp3;

public:
	CPLCReaderZR(void);
	virtual ~CPLCReaderZR(void);

	void SetPLCModule(CMxComp3* pMxComp3) { m_pMxComp3 = pMxComp3; }
	void SetAddress( int nStartAddress, int nEndAddress );
	int nGetAddress_Start() { return m_nStartAddress; };
	int nGetAddress_End() { return m_nEndAddress; };

	BOOL Read();

	SHORT GetValue( int nAddress );
	LONG GetValue2( int nAddress );
	MADDRESS GetValueM( int nAddress );

	CString GetString( int nAddress, int nSize);
};

class CPLCReaderR : public CPLCReader
{
protected:
	SHORT*	m_pBuffer;
	int		m_nStartAddress;
	int		m_nEndAddress;
	CMxComp3*	m_pMxComp3;

public:
	CPLCReaderR(void);
	virtual ~CPLCReaderR(void);

	void SetPLCModule(CMxComp3* pMxComp3) { m_pMxComp3 = pMxComp3; }
	void SetAddress( int nStartAddress, int nEndAddress );

	BOOL Read();

	SHORT GetValue( int nAddress );
	LONG GetValue2( int nAddress );
	MADDRESS GetValueM( int nAddress );

	CString GetString( int nAddress, int nSize);
};

class CPLCReaderM : public CPLCReader
{
protected:
	SHORT*	m_pBuffer;
	int		m_nStartAddress;
	int		m_nEndAddress;
	CMxComp3*	m_pMxComp3;
public:
	CPLCReaderM(void);
	virtual ~CPLCReaderM(void);

	void SetPLCModule(CMxComp3* pMxComp3) { m_pMxComp3 = pMxComp3; }
	void SetAddress( int nStartAddress, int nEndAddress );

	BOOL Read();

	BOOL IsON( int nAddress );
};

class CPLCReaderL : public CPLCReader
{
protected:
	SHORT*	m_pBuffer;
	int		m_nStartAddress;
	int		m_nEndAddress;
	CMxComp3*	m_pMxComp3;
public:
	CPLCReaderL(void);
	virtual ~CPLCReaderL(void);

	void SetPLCModule(CMxComp3* pMxComp3) { m_pMxComp3 = pMxComp3; }
	void SetAddress( int nStartAddress, int nEndAddress );

	BOOL Read();

	BOOL IsON( int nAddress );
};

class CPLCReaderX : public CPLCReader
{
protected:
	SHORT*	m_pBuffer;
	int		m_nStartAddress;
	int		m_nEndAddress;
	CMxComp3*	m_pMxComp3;
public:
	CPLCReaderX(void);
	virtual ~CPLCReaderX(void);

	void SetPLCModule(CMxComp3* pMxComp3) { m_pMxComp3 = pMxComp3; }
	void SetAddressHex( int nStartAddress, int nEndAddress );

	BOOL Read();

	BOOL IsONHex( int nAddress );
};

class CPLCReaderY : public CPLCReader
{
protected:
	SHORT*	m_pBuffer;
	int		m_nStartAddress;
	int		m_nEndAddress;
	CMxComp3*	m_pMxComp3;
public:
	CPLCReaderY(void);
	virtual ~CPLCReaderY(void);

	void SetPLCModule(CMxComp3* pMxComp3) { m_pMxComp3 = pMxComp3; }
	void SetAddressHex( int nStartAddress, int nEndAddress );

	BOOL Read();

	BOOL IsONHex( int nAddress );
};
