// Dlg_MotorList.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "AMKOR_MMI.h"
#include "Dlg_MotorList.h"
#include "afxdialogex.h"


// CDlg_MotorList 대화 상자입니다.

IMPLEMENT_DYNAMIC(CDlg_MotorList, CDialogEx)

CDlg_MotorList::CDlg_MotorList(CWnd* pParent /*=NULL*/)
	: CDialogEx(CDlg_MotorList::IDD, pParent)
{
	m_bCtrl_FirstLoad = FALSE;
	m_nSelMotorIndex = -1;
	m_nCurMotorPage = 0;
}

CDlg_MotorList::~CDlg_MotorList()
{
}

void CDlg_MotorList::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_GC_ML_1, m_GC_1);
	DDX_Control(pDX, IDC_BTN_ML_1, m_btn_1);
	DDX_Control(pDX, IDC_BTN_ML_2, m_btn_2);
	DDX_Control(pDX, IDC_BTN_ML_3, m_btn_3);
	DDX_Control(pDX, IDC_BTN_ML_4, m_btn_4);
	DDX_Control(pDX, IDC_BTN_ML_5, m_btn_5);
	DDX_Control(pDX, IDC_BTN_ML_6, m_btn_6);
}


BEGIN_MESSAGE_MAP(CDlg_MotorList, CDialogEx)
	ON_WM_ERASEBKGND()
	ON_WM_SIZE()
	ON_WM_TIMER()
	ON_NOTIFY(NM_CLICK, IDC_GC_ML_1, OnGridClick_GC_1)
	ON_BN_CLICKED(IDC_BTN_ML_1, &CDlg_MotorList::OnBnClickedBtnMl1)
	ON_BN_CLICKED(IDC_BTN_ML_2, &CDlg_MotorList::OnBnClickedBtnMl2)
	ON_BN_CLICKED(IDC_BTN_ML_3, &CDlg_MotorList::OnBnClickedBtnMl3)
	ON_BN_CLICKED(IDC_BTN_ML_4, &CDlg_MotorList::OnBnClickedBtnMl4)
	ON_BN_CLICKED(IDC_BTN_ML_5, &CDlg_MotorList::OnBnClickedBtnMl5)
	ON_BN_CLICKED(IDC_BTN_ML_6, &CDlg_MotorList::OnBnClickedBtnMl6)
END_MESSAGE_MAP()


// CDlg_MotorList 메시지 처리기입니다.


BOOL CDlg_MotorList::OnEraseBkgnd(CDC* pDC)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	// 전체 배경색
	CRect rect;
    GetClientRect(&rect);
    CBrush myBrush(CR_MAIN_BKGND);	// dialog background color
    CBrush *pOld = pDC->SelectObject(&myBrush);
    BOOL bRes  = pDC->PatBlt(0, 0, rect.Width(), rect.Height(), PATCOPY);
    pDC->SelectObject(pOld);		// restore old brush
    return FALSE;     
	//return CDialogEx::OnEraseBkgnd(pDC);
}


void CDlg_MotorList::OnSize(UINT nType, int cx, int cy)
{
	CDialogEx::OnSize(nType, cx, cy);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	if(m_bCtrl_FirstLoad == TRUE) Cal_CtrlArea();
}


void CDlg_MotorList::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CDialogEx::OnTimer(nIDEvent);
}


BOOL CDlg_MotorList::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	Cal_CtrlArea();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


BOOL CDlg_MotorList::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message == WM_SYSKEYDOWN)
	{
		if(pMsg->wParam == VK_F4) 
			return TRUE;
	}

	if(pMsg->message == WM_KEYDOWN)
	{
		if( pMsg->wParam == VK_ESCAPE ||	
			pMsg->wParam == VK_RETURN ||	
			pMsg->wParam == VK_SPACE  ||
			pMsg->wParam == VK_CANCEL )     
			return TRUE;        
	}

	return CDialogEx::PreTranslateMessage(pMsg);
}

void CDlg_MotorList::Cal_Area()
{
	CRect ClientRect, rcGCRect, rcBtnRect, rcDiv[6];
	GetClientRect(&ClientRect);
	CRect CalRect;

	SetWindowText(m_strCaption);

	ClientRect.DeflateRect(5,5,5,5);
	rcGCRect.SetRect(ClientRect.left, ClientRect.top, ClientRect.right, ClientRect.bottom-60);
	rcBtnRect.SetRect(ClientRect.left, ClientRect.bottom-60, ClientRect.right, ClientRect.bottom);
	m_GC_1.MoveWindow(&rcGCRect);
	Init_GC_1(16,2);
	Update_MotorList();

	int n6DivW = (int)((float)rcBtnRect.Width()/6.0f);
	rcDiv[0].SetRect(rcBtnRect.left, rcBtnRect.top, rcBtnRect.left+n6DivW, rcBtnRect.bottom);
	rcDiv[1].SetRect(rcDiv[0].right, rcBtnRect.top, rcDiv[0].right+n6DivW, rcBtnRect.bottom);
	rcDiv[2].SetRect(rcDiv[1].right, rcBtnRect.top, rcDiv[1].right+n6DivW, rcBtnRect.bottom);
	rcDiv[3].SetRect(rcDiv[2].right, rcBtnRect.top, rcDiv[2].right+n6DivW, rcBtnRect.bottom);
	rcDiv[4].SetRect(rcDiv[3].right, rcBtnRect.top, rcDiv[3].right+n6DivW, rcBtnRect.bottom);
	rcDiv[5].SetRect(rcDiv[4].right, rcBtnRect.top, rcBtnRect.right, rcBtnRect.bottom);

	m_btn_1.SetFontBtn(CTRL_FONT, 24, FW_BOLD);
	m_btn_1.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_1.SetTextColor(CR_BLACK);
	m_btn_1.SetIconPos(0);
	m_btn_1.SetIconID(IDI_PREV);
	m_btn_1.SetIconSize(48,48);
	m_btn_1.SetFlatType(TRUE);
	m_btn_1.MoveWindow(&rcDiv[0]);

	m_btn_2.SetFontBtn(CTRL_FONT, 24, FW_BOLD);
	m_btn_2.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_2.SetTextColor(CR_BLACK);
	m_btn_2.SetIconPos(0);
	m_btn_2.SetIconID(IDI_NEXT);
	m_btn_2.SetIconSize(48,48);
	m_btn_2.SetFlatType(TRUE);
	m_btn_2.MoveWindow(&rcDiv[1]);

	m_btn_3.SetFontBtn(CTRL_FONT, 24, FW_BOLD);
	m_btn_3.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_3.SetTextColor(CR_BLACK);
	m_btn_3.SetIconPos(0);
	m_btn_3.SetIconID(IDI_PLUS);
	m_btn_3.SetIconSize(48,48);
	m_btn_3.SetFlatType(TRUE);
	m_btn_3.MoveWindow(&rcDiv[2]);

	m_btn_4.SetFontBtn(CTRL_FONT, 24, FW_BOLD);
	m_btn_4.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_4.SetTextColor(CR_BLACK);
	m_btn_4.SetIconPos(0);
	m_btn_4.SetIconID(IDI_MINUS);
	m_btn_4.SetIconSize(48,48);
	m_btn_4.SetFlatType(TRUE);
	m_btn_4.MoveWindow(&rcDiv[3]);

	m_btn_5.SetFontBtn(CTRL_FONT, 20, FW_BOLD);
	m_btn_5.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_5.SetTextColor(CR_BLACK);
	//m_btn_5.SetIconPos(0);
	//m_btn_5.SetIconID(IDI_MINUS);
	//m_btn_5.SetIconSize(48,48);
	m_btn_5.SetFlatType(TRUE);
	m_btn_5.MoveWindow(&rcDiv[4]);

	m_btn_6.SetFontBtn(CTRL_FONT, 24, FW_BOLD);
	m_btn_6.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_6.SetTextColor(CR_BLACK);
	m_btn_6.SetIconPos(0);
	m_btn_6.SetIconID(IDI_CLOSE);
	m_btn_6.SetIconSize(48,48);
	m_btn_6.SetFlatType(TRUE);
	m_btn_6.MoveWindow(&rcDiv[5]);
}

void CDlg_MotorList::Cal_CtrlArea()
{
	Cal_Area();

	m_bCtrl_FirstLoad = TRUE;
}

bool CDlg_MotorList::Init_GC_1(int nRows, int nCols)
{
	BOOL m_bEditable = FALSE;
    BOOL m_bListMode = TRUE;
    int m_nRows = nRows;
    int m_nCols = nCols;
    int m_nFixRows = 1;
    int m_nFixCols = 1;

    m_GC_1.SetEditable(m_bEditable);
    m_GC_1.SetListMode(m_bListMode);
    m_GC_1.EnableDragAndDrop(FALSE);
    m_GC_1.SetTextBkColor(RGB(0xFF, 0xFF, 0xE0));

    m_GC_1.SetRowCount(m_nRows);
    m_GC_1.SetColumnCount(m_nCols);
    m_GC_1.SetFixedRowCount(m_nFixRows);
    m_GC_1.SetFixedColumnCount(m_nFixCols);

	m_GC_1.EnableSelection(false);
	m_GC_1.SetSingleColSelection(true);
	m_GC_1.SetSingleRowSelection(true);
	m_GC_1.SetFixedColumnSelection(false);
    m_GC_1.SetFixedRowSelection(false);

	m_GC_1.SetRowResize(false);
	m_GC_1.SetColumnResize(false);

	CRect ClientRect;
	GetDlgItem(IDC_GC_ML_1)->GetWindowRect(&ClientRect);
	if(ClientRect.Width() < 4) return false;
	if(ClientRect.Height() < 4) return false;
	int nClientWidth = ClientRect.Width()-4;
	int nClientHeight = ClientRect.Height()-4;
	int n15pW = (int)(nClientWidth*0.15f);
	int nRemainW = nClientWidth - n15pW;
	//int nCellW = nClientWidth/m_nCols;
	int nCellH = nClientHeight/m_nRows;
	//int nLastCellW = nClientWidth-nCellW*(m_nCols-1);
	int nLastCellH = nClientHeight-nCellH*(m_nRows-1);
	//int nRemainW = nLastCellW - nCellW;
	int nRemainH = nLastCellH - nCellH;
	for (int i = 0; i < m_GC_1.GetRowCount(); i++) 
	{
		if(i == m_nRows-1)
		{
				m_GC_1.SetRowHeight(i,nCellH);
		}
		else 
		{
			if(nRemainH > 0)
			{
				m_GC_1.SetRowHeight(i,nCellH + 1);
				nRemainH --;
			}
			else
				m_GC_1.SetRowHeight(i,nCellH);
		}
	}
	for (int j = 0; j < m_GC_1.GetColumnCount(); j++)
	{
		if(j == 0) m_GC_1.SetColumnWidth(j,n15pW);
		else if(j == 1) m_GC_1.SetColumnWidth(j,nRemainW);
	}
    DWORD dwTextStyle = DT_CENTER|DT_VCENTER|DT_SINGLELINE;

    // fill rows/cols with text
    for (int row = 0; row < m_GC_1.GetRowCount(); row++) 
	{
        for (int col = 0; col < m_GC_1.GetColumnCount(); col++) 
		{ 
            GV_ITEM Item;
            Item.mask = GVIF_TEXT|GVIF_FORMAT;
            Item.row = row;
            Item.col = col;
			Item.nFormat = dwTextStyle;

			if(row == 0)
			{
				if(col == 0) Item.strText.Format(_T("#"));
				else if(col == 1) Item.strText.Format(_T("MOTOR NAME"));
			}
			else
			{
				if(col == 0)
				{
					Item.strText.Format(_T("%d"), row);
				}
				else if(col == 1)
				{
					
				}
			}

			if(row == 0 || col == 0) m_GC_1.SetItemBkColour(row, col, CR_SLATEGRAY);
			m_GC_1.SetItemFgColour(row, col, RGB(0,0,0));
			m_GC_1.SetItemFont(row, col, CTRL_FONT, 18, FW_BOLD);
            m_GC_1.SetItem(&Item);  
        }
    }
       
    return TRUE;  // return TRUE  unless you set the focus to a control
}

void CDlg_MotorList::OnGridClick_GC_1(NMHDR *pNotifyStruct, LRESULT* /*pResult*/)
{
	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*) pNotifyStruct;

	int nSelRow = pItem->iRow;
	int nSelCol = pItem->iColumn;

	if(nSelRow > 0) m_nSelMotorIndex = nSelRow - 1 + m_nCurMotorPage*15;

	stMotorManual_Info stMMI = CMotorManualSetting::Instance()->GetMotorManualInfo();
	if(m_nSelMotorIndex > (int)stMMI.m_vecMotorInfo.size() - 1)
	{
		m_nSelMotorIndex = -1;
	}
}

void CDlg_MotorList::Update_MotorList()
{
	int nMotorIndex = 0;
	CString strIndex;
	stMotorManual_Info stMMI = CMotorManualSetting::Instance()->GetMotorManualInfo();
	for (int row = 1; row < m_GC_1.GetRowCount(); row++) 
	{
		nMotorIndex = row - 1 + m_nCurMotorPage*15;
		if(nMotorIndex < (int)stMMI.m_vecMotorInfo.size())
		{
			strIndex.Format(_T("%d"), nMotorIndex+1);
			m_GC_1.SetItemText(row, 0, strIndex);
			m_GC_1.SetItemText(row, 1, stMMI.m_vecMotorInfo[nMotorIndex].m_strMotorName);
		}
		else
		{
			m_GC_1.SetItemText(row, 0, _T(""));
			m_GC_1.SetItemText(row, 1, _T(""));
		}
    }
	m_GC_1.Invalidate();
}

void CDlg_MotorList::OnBnClickedBtnMl1()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	stMotorManual_Info stMMI = CMotorManualSetting::Instance()->GetMotorManualInfo();

	m_nCurMotorPage --;
	if(m_nCurMotorPage < 0) 
	{
		m_nCurMotorPage = 0;
	}
	else
	{
		Update_MotorList();
	}
}


void CDlg_MotorList::OnBnClickedBtnMl2()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	stMotorManual_Info stMMI = CMotorManualSetting::Instance()->GetMotorManualInfo();
	int nMaxPage = stMMI.m_nMotorCount/15;
	if(stMMI.m_nMotorCount%15 > 0) nMaxPage ++;

	m_nCurMotorPage ++;
	if(m_nCurMotorPage > nMaxPage - 1)
	{
		m_nCurMotorPage = nMaxPage - 1;
	}
	else
	{
		Update_MotorList();
	}
}


void CDlg_MotorList::OnBnClickedBtnMl3()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CMotorManualSetting::Instance()->AddMotorInfo(100,100,10);
	Update_MotorList();
}


void CDlg_MotorList::OnBnClickedBtnMl4()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CMotorManualSetting::Instance()->DelMotorInfo();
	Update_MotorList();
}


void CDlg_MotorList::OnBnClickedBtnMl5()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CDialogEx::OnOK();
}


void CDlg_MotorList::OnBnClickedBtnMl6()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CDialogEx::OnCancel();
}
