#include "StdAfx.h"
#include "profile.h"


//CString     g_inipath;

CString getprofilestring_g( LPCTSTR lpAppName, LPCTSTR lpKeyName, LPCTSTR lpDefault, LPCTSTR lpFileName )
{
	TCHAR szTmp[MAX_PATH];
	GetPrivateProfileString( lpAppName, lpKeyName, lpDefault, szTmp, MAX_PATH, lpFileName );	
	return szTmp;
}
BOOL setprofilestring_g( LPCTSTR lpAppName, LPCTSTR lpKeyName, LPCTSTR lpData, LPCTSTR lpFileName )
{
	return WritePrivateProfileString( lpAppName, lpKeyName, lpData, lpFileName );
}
int getprofileint_g( LPCTSTR lpAppName, LPCTSTR lpKeyName, int nValue, LPCTSTR lpFileName )
{
	return GetPrivateProfileInt(lpAppName, lpKeyName, nValue, lpFileName);
}
BOOL setprofileint_g( LPCTSTR lpAppName, LPCTSTR lpKeyName, int nValue, LPCTSTR lpFileName )
{
	TCHAR szTmp[MAX_PATH];
	_stprintf_s( szTmp, "%d", nValue );	
	return WritePrivateProfileString( lpAppName, lpKeyName, szTmp, lpFileName );	
}
BOOL getprofilestruct_g( LPCTSTR lpszsection, LPCTSTR lpszkey, LPVOID lpstruct, UINT usizestruct, LPCTSTR szfile )
{
	return GetPrivateProfileStruct( lpszsection, lpszkey, lpstruct, usizestruct, szfile );
}
BOOL setprofilestruct_g( LPCTSTR lpszsection, LPCTSTR lpszkey, LPVOID lpstruct, UINT usizestruct, LPCTSTR szfile )
{	
	return WritePrivateProfileStruct( lpszsection, lpszkey, lpstruct, usizestruct, szfile );
}
BOOL getprofilebinary_g(CWinApp *pApp, LPCTSTR lpszSection, LPCTSTR lpszEntry, LPBYTE* ppData, UINT* pBytes)
{
	return pApp->GetProfileBinary( lpszSection, lpszEntry, ppData, pBytes );
}
BOOL setprofilebinary_g(CWinApp *pApp, LPCTSTR lpszSection, LPCTSTR lpszEntry, LPBYTE pData, UINT nBytes)
{
	return pApp->WriteProfileBinary( lpszSection, lpszEntry, pData, nBytes );
}


BOOL setprofilerect_g(LPCTSTR lpszSection, LPCTSTR lpszEntry, const RECT Value, LPCTSTR lpFileName)
{
	CString str;
	str.Format(_T("%i,%i,%i,%i"), Value.left, Value.top, Value.right, Value.bottom);
	return WritePrivateProfileString(lpszSection, lpszEntry, str, lpFileName);
}
RECT getprofilerect_g(LPCTSTR lpszSection, LPCTSTR lpszEntry, const RECT Default, LPCTSTR lpFileName)
{
	TCHAR szTmp[MAX_PATH] = { 0,};
	CRect rtReturn = Default;
	GetPrivateProfileString(lpszSection, lpszEntry,  TEXT(""), szTmp, MAX_PATH,  lpFileName );
	if( strlen( szTmp ) ){
		_stscanf_s(szTmp, _T("%ld,%ld,%ld,%ld"), &rtReturn.left, &rtReturn.top, &rtReturn.right, &rtReturn.bottom);
	}		
	return rtReturn;
}
BOOL setprofilesize_g(LPCTSTR lpszSection, LPCTSTR lpszEntry, const SIZE Value, LPCTSTR lpFileName)
{
	CString str;
	str.Format(_T("%i,%i"), Value.cx, Value.cy);
	return WritePrivateProfileString(lpszSection, lpszEntry, str, lpFileName);
}
SIZE getprofilesize_g(LPCTSTR lpszSection, LPCTSTR lpszEntry, const SIZE Default, LPCTSTR lpFileName)
{
	TCHAR szTmp[MAX_PATH] = { 0,};
	SIZE sizeReturn = Default;
	GetPrivateProfileString(lpszSection, lpszEntry,  TEXT(""), szTmp, MAX_PATH,  lpFileName );

	if( strlen( szTmp ) ){
		_stscanf_s(szTmp, _T("%ld,%ld"), &sizeReturn.cx, sizeReturn.cy );
	}		
	return sizeReturn;
}

BOOL setprofiledouble_g(LPCTSTR lpszSection, LPCTSTR lpszEntry, const double Value, LPCTSTR lpFileName)
{
	LPBYTE pData = (LPBYTE) &Value;
	UINT nBytes = sizeof(double);
	/*
	LPTSTR lpsz = (LPTSTR)_alloca(sizeof(TCHAR) * (nBytes * 2 + 1));
	LONG i = 0;
	for (; i < (LONG)nBytes; i ++)
	{
		lpsz[i * 2] = (TCHAR)((pData[i] & 0x0F) + 'A'); 
		lpsz[i * 2 + 1] = (TCHAR)(((pData[i] >> 4) & 0x0F) + 'A'); 
	}
	lpsz[i * 2] = 0;
	*/
	CString temp;
	temp.Format(_T("%.3f"),Value);
	return WritePrivateProfileString(lpszSection, lpszEntry, temp, lpFileName);
}
double getprofiledouble_g(LPCTSTR lpszSection, LPCTSTR lpszEntry, const double Default, LPCTSTR lpFileName)
{
	double dbReturn = Default;
	TCHAR szTmp[MAX_PATH] = { 0,};
	GetPrivateProfileString(lpszSection, lpszEntry,  TEXT(""), szTmp, MAX_PATH,  lpFileName );
	/*if( strlen( szTmp ) ){
		LONG nLen = strlen( szTmp );

		LPBYTE pData = (LPBYTE)&dbReturn;
		LONG i;
		for (i = 0; i < nLen; i += 2){
			(pData)[i / 2] = (BYTE)(((szTmp[i + 1] - 'A') << 4) + (szTmp[i] - 'A'));
		}
	}		*/

	dbReturn = atof(szTmp);
	return dbReturn;
}

BOOL setprofiledword_g(LPCTSTR lpszSection, LPCTSTR lpszEntry, const DWORD Value, LPCTSTR lpFileName)
{
	TCHAR szTmp[MAX_PATH];
	_stprintf_s( szTmp, "%d", Value );	
	return WritePrivateProfileString( lpszSection, lpszEntry, szTmp, lpFileName );	
	
}
DWORD getprofiledword_g(LPCTSTR lpszSection, LPCTSTR lpszEntry, const DWORD Default, LPCTSTR lpFileName)
{
	return GetPrivateProfileInt(lpszSection, lpszEntry, Default, lpFileName);
}

BOOL setprofilecolor_g(LPCTSTR lpszSection, LPCTSTR lpszEntry, const COLORREF& Value, LPCTSTR lpFileName)
{
	return setprofiledword_g(lpszSection, lpszEntry, Value, lpFileName);	
}
COLORREF getprofilecolor_g(LPCTSTR lpszSection, LPCTSTR lpszEntry, const COLORREF& crDefault, LPCTSTR lpFileName)
{
	return getprofiledword_g(lpszSection, lpszEntry, crDefault, lpFileName);
}
