#pragma once

#include "MxComp3.h"
#include "PLCReader.h"

#define TOTAL_LOT_COUNT		16
#define CHILD_TP_COUNT		16
#define TOTAL_BUNDLE_COUNT	10

enum TRAYPOS
{
	TPOS_NOTHING = 0,
	TPOS_G1_POS1 = 1,				// RFID 읽기 / 트레이 수량 체크
	TPOS_G1_POS2 = 2,				// RF트레이 제거 / 트레이 소분
	TPOS_G1_POS3 = 3,				// 비전 Device 검사(BOX ID 결정) / BOTTOM TRAY 결합
	TPOS_G1_POS4 = 4,				// 비전 사면 들뜸 검사
	TPOS_G1_POS5 = 5,				// 트레이 라벨 투입
	TPOS_G1_POS6 = 6,				// 밴딩 가로 제어 / 밴딩 세로 제어
	TPOS_G1_POS7 = 7,				// HIC 투입
	TPOS_G1_POS8 = 8,				// DSC 투입
	TPOS_G2_POS1 = 9,				// MBB 실링전 대기
	TPOS_G2_POS2 = 10,				// MBB 실링 처리 / 실링 검사
	TPOS_G2_POS3 = 11,				// 실링후 배출 대기
	TPOS_G3_POS1 = 12,				// MBB 폴딩
	TPOS_G3_POS2 = 13,				// BOX 에 MBB 폴딩 투입 ( 박스접은후 투입 & BubbleSheet )
	TPOS_G3_POS3 = 14,				// 박스 덮기
	TPOS_G3_POS4 = 15,				// 박스 라벨 부착
	TPOS_G3_POS5 = 16,				// 박스 바코드 & 배출대기
	TPOS_MAX = 17,
};

enum OPERCODE
{
	OP_NOTHING = 0,
	OP_G101 = 1,		// Tray 투입
	OP_G102 = 2,		// C-tray 제거 및 Tray 소분
	OP_G103 = 3,		// Lot 및 PKG Validation
	OP_G104 = 4,		// A- Tray stack 및 자재 Tray stack
	OP_G105 = 5,		// Gap 검사 Vison
	OP_G106 = 6,		// Tray Label 출력 및 Loading
	OP_G201 = 7,		// Banding
	OP_G202 = 8,		// End cap 안착
	OP_G203 = 9,		// HIC, Desiccant
	OP_G204 = 10,		// Seal date marking & P-Label 부착
	OP_G205 = 11,		// MBB Sealing
	OP_G301 = 12,		// MBB Folding
	OP_G302 = 13,		// Bubble folding
	OP_G303 = 14,		// Inner box packing
	OP_G304 = 15,		// QA Seal
	OP_G305 = 16,		// Unloading

	OP_MAX = 17,
};

enum MES_PROCESS
{
	MES_NOTHING = 0,
	MES_READY_TO_LOAD_REQ = 1,
	MES_READY_TO_LOAD_RFID_REQ = 2,
	MES_ZEBRA_CODE_REQ = 3,
	MES_LOT_START_REQ = 4,
	MES_LOT_START_END = 5,
	MES_READY_TO_UNLOAD_REQ = 6,
	MES_LOTEND_REQ = 7,
	MES_MAX = 8,
};

enum CONNECT_TYPE
{
	CONN_PLC_1 = 0,
	CONN_PLC_2 = 1,
	CONN_PLC_3 = 2,
	CONN_MES = 3,
	CONN_VISION = 4,
	CONN_HMS_G1 = 5,
	CONN_HMS_G2 = 6,
	CONN_MAX = 7,
};

struct WORK_INFO{
	int m_nTotal_LOT_INPUT;							// 총 투입된 LOT 수량
	int m_nTotal_LOT_OUTPUT;						// 총 배출된 LOT 수량
	
	int m_nTotal_BOX_INPUT;							// 총 투입된 BOX 수량
	int m_nTotal_BOX_OUTPUT;						// 총 배출된 BOX 수량
	int m_nTotal_BOX_DELETE;						// 총 제거된 BOX 수량
};

// 장비내 부자재 상태 정보
struct MATSTATUS_INFO{
	// GROUP 1
	int m_nIDStacker[30];							// 커버 트레이 스태커 각 위치별 인덱스 정보
	int m_nCTStacker[30];							// 커버 트레이 스태커 각 위치별 유무
	int m_nStatus_RFTrayStacker;					// RF Tray Stacker Full Check
	int m_nStatus_Banding;							// 밴딩기 상태
	int m_nStatus_Label_TRAY;						// TRAY 라벨 용지 상태
	int m_nStatus_Label_MBB1;						// MBB 라벨 용지 상태
	int m_nStatus_Label_MBB2;						// MBB 라벨 용지 상태
	int m_nStatus_Label_BOX1;						// BOX 라벨 용지 상태
	int m_nStatus_Label_BOX2;						// BOX 라벨 용지 상태
	int m_nStatus_Ribbon_TRAY;						// TRAY RIBBON 상태
	int m_nStatus_Ribbon_MBB1;						// MBB RIBBON 상태
	int m_nStatus_Ribbon_MBB2;						// MBB RIBBON 상태
	int m_nStatus_Ribbon_BOX1;						// BOX RIBBON 상태
	int m_nStatus_Ribbon_BOX2;						// BOX RIBBON 상태

	int m_nStatus_EndCap[8];						// EndCap 유무
	int m_nStatus_HIC;								// HIC 상태
	int m_nStatus_SIL;								// 실리카겔 상태
	int m_nStatus_BubbleSheet;						// 에어캡 유무
	int m_nStatus_BubbleTape;						// 에어캡 TAPE 상태

	int m_nStatus_MBB_1;							// MBB 1번 상태
	int m_nStatus_MBB_2;							// MBB 2번 상태
	int m_nStatus_BOX;								// BOX 1번 상태

	int m_nStatus_BoxTape;							// BOX TAPE 상태

	int m_bFirstLoad;								// 첫 정보를 받았는지 여부
};

// 기타 주변 장치 연결 상태
struct CONNECT_STATUS_INFO
{
	BOOL m_bConnectStatus[CONN_MAX];

	CONNECT_STATUS_INFO::CONNECT_STATUS_INFO()
	{
		for(int i=0;i<CONN_MAX;i++) m_bConnectStatus[i] = FALSE;
	}
	void Clear()
	{
		for(int i=0;i<CONN_MAX;i++) m_bConnectStatus[i] = FALSE;
	}
};

// MCH 구조체
struct MCH_INFO{
	int m_nStatus;									// 가동정보	- Start, Stop
	int m_nAutoMode;								// AutoMode정보 - Manual, Auto
	int m_nDummyMode;								// 더미 모드

	WORK_INFO m_stWorkInfo;							// 작업 정보
	MATSTATUS_INFO m_stMatStatus_Info;				// 장비내 부자재 상태 정보
	CONNECT_STATUS_INFO m_stConnStatus_Info;		// 기타 주변 장치 연결 상태

	BOOL m_bCurAlarmSet;							// 현재 Alarm Set 상태
	BOOL m_bPrevAlarmSet;							// 이전 Alarm Set 상태
};

// 박스 공정 이동시 사용할 구조체
struct stBoxProcInfo{
	BOOL m_bFlag;
	int m_nOperCode;
	TCHAR m_strLOTID[25];
	int m_nLotDeviceQty;
	TCHAR m_strBoxID[3];
	int m_nBoxDeviceQty;
	int m_nBoxSplitTotalQty;
	BOOL m_bPartial;
};

// MMI TRAY INFO
struct stTrayPackInfo
{
	BOOL m_bLive;

	TCHAR m_strLOTID[20];							// LOT ID
	TCHAR m_strRFID[20];							// RFID-Barcode

	int m_nBoxNum;									// BOXNUM - 계산으로 도출함. 계산전 기본값 000
	int m_nDeviceIndex;								// 디바이스 레시피 인덱스
	int m_nTrayIndex;								// 트레이 인덱스 정보
	int m_nTrayCount;								// 투입된 Tray 개수 - 계산으로 도출함.(CoverTray 제외)

	int m_nCurPos;									// 현재 위치 TP_MAX 라면 배출 완료된 박스임
	BOOL m_bComplete;								// 정상 배출된 박스인지 여부
	BOOL m_bForceDelete;							// 강제 제거된 박스인지 여부
};

// INPUT 된 LOT 정보 관리 - 전산 공정 처리 위함.
struct stLotInfo
{
	BOOL m_bLive;
	BOOL m_bComplete;								// 정상 배출된 LOT인지 여부
	BOOL m_bForceDelete;							// 강제 제거된 LOT인지 여부
	
	TCHAR m_strLOTID[20];							// LOT ID
	TCHAR m_strRFID[TOTAL_BUNDLE_COUNT][20];		// RFID-Barcode

	int m_nDeviceIndex;								// 디바이스 레시피 인덱스
	int m_nInputBundleCount;						// 투입된 번들 수량
	int m_nTotalDeviceCount;						// LOT 총 chip 개수
	int m_nBundleDeviceCount[TOTAL_BUNDLE_COUNT];	// 번들 chip 개수
	int m_nBundleTrayCount[TOTAL_BUNDLE_COUNT];		// 번들 트레이 개수
	int m_nBoxInDeviceQty;							// 박스당 디바이스 개수
	
	int m_nCurMakeBoxID;							// 현재까지 만들어진 박스 번호
	BOOL m_bLastBoxIDMake;							// 마지막 박스 번호가 만들어졌는지 여부 판단
	int m_nTotalBoxCount;							// 총 박스 개수
	int m_nMakeBoxCount;							// 지금까지 만들어진 박스 개수
	int m_nOutputBoxCount;							// 최종 배출된 박스 개수
	int m_nDeleteBoxCount;							// 제거된 박스 개수

	int m_nCurProcess;								// 현재 진행된 프로세스
	BOOL m_bSendMESMsg;
	BOOL m_bRecvMESMsg;

	// 번들당 부모 자식 트레이 정보 관리
	stTrayPackInfo m_arrayBundleInfo[TOTAL_BUNDLE_COUNT];
	stTrayPackInfo m_arrayBOXInfo[TOTAL_BUNDLE_COUNT][CHILD_TP_COUNT];
	// Prinf 할 ZPL Code
	TCHAR m_strZplCode[TOTAL_BUNDLE_COUNT*CHILD_TP_COUNT][5000];
	// Vision 검사용 Chip OCR Info
	TCHAR m_strChipOCR[5][30];

	// 최초 투입된 LOT 시간 - 이후 같은 LOT이 투입되더라도 갱신 안됨
	TCHAR m_strCreateDate[9];						// CreateDate - ex 20170607
	TCHAR m_strCreateTime[7];						// CreateTime - ex 111059
};

// CCTV Trigger 정보 관련
struct stSendCamTrigger{
	BOOL m_bLive;
	int m_nPos;
	int m_nSubPos;
	int m_nWaitTime;
	int m_nRetryCount;
};

// 전산 프로세스 처리
struct stMESInfo_READY_TO_LOAD_BARCODE{
	BOOL m_bLive;
	//
	BOOL m_bSend;
	TCHAR m_strRFID[20];		// RFID-Barcode
	//
	BOOL m_bRecv;
	TCHAR m_strLOTID[20];		// LOT ID
	TCHAR m_strDEVICE[20];		// DEVICE ID
	TCHAR m_strChipOCR[5][30];	// CHIP MARK DATA
	int m_nTotalDeviceCount;	// LOT 총 chip 개수
	int m_nBundleDeviceCount;	// 번들 chip 개수
	int m_nTotalBoxCount;		// 총 박스 개수
	int m_nBoxInDeviceQty;		// 박스당 디바이스 개수
	int m_nDeviceIndex;			// DEVICE INDEX
};

class CDataManagerGC;
class CDataManager
{
public:
	friend class CDataManagerGC;
	static CDataManager* Instance();

public:

	static CDataManager* m_pThis;

	CDataManager(void);
	~CDataManager(void);

	void Reset_Data(); // 전체 정보 데이터 초기화

	BOOL CreateDevice_PLC_1() { return m_MxComp3_PLC_1.Create(); }	
	BOOL OpenDevice_PLC_1( int nLogicalStationNumber );
	BOOL CloseDevice_PLC_1() { return m_MxComp3_PLC_1.Close(); }	
	BOOL DestroyDevice_PLC_1() { return m_MxComp3_PLC_1.Destroy(); }

	BOOL CreateDevice_PLC_2() { return m_MxComp3_PLC_2.Create(); }	
	BOOL OpenDevice_PLC_2( int nLogicalStationNumber );
	BOOL CloseDevice_PLC_2() { return m_MxComp3_PLC_2.Close(); }	
	BOOL DestroyDevice_PLC_2() { return m_MxComp3_PLC_2.Destroy(); }

	BOOL CreateDevice_PLC_3() { return m_MxComp3_PLC_3.Create(); }	
	BOOL OpenDevice_PLC_3( int nLogicalStationNumber );
	BOOL CloseDevice_PLC_3() { return m_MxComp3_PLC_3.Close(); }	
	BOOL DestroyDevice_PLC_3() { return m_MxComp3_PLC_3.Destroy(); }

	// 현재 연결 상태 유무
	BOOL IsConnected(int nConnType) { return m_stMCHINFO.m_stConnStatus_Info.m_bConnectStatus[nConnType]; };

	// 연결 상태 설정
	void SetConnect(int nConnType, BOOL bConn) { m_stMCHINFO.m_stConnStatus_Info.m_bConnectStatus[nConnType] = bConn; }

	// PLC 연결상태 끊음
	void CloseDeviceAll_PLC();

	// PLC Data Update
	void Parse_PLC_Data();
	void Update_TrayPackPosInfo();

	// PLC READ/WRITE FUNC
	BOOL PLC_Read_D(int nPLCNum, int address, int count,SHORT* result);
	BOOL PLC_Read_D2(int nPLCNum, int address,DWORD* result);
	BOOL PLC_Write_D(int nPLCNum, int address, SHORT input);
	BOOL PLC_Write_D(int nPLCNum, int address, SHORT* sBuffer, int count);
	BOOL PLC_Write_D2(int nPLCNum, int address, INT input);
	BOOL PLC_Write_D2(int nPLCNum, int address, INT* inputBuffer, int count);
	BOOL PLC_Write_D2(int nPLCNum, int address, LPCTSTR strText, int count);
	BOOL PLC_Write_ZR(int nPLCNum, int address, SHORT input);
	BOOL PLC_Write_ZR(int nPLCNum, int address, SHORT* inputBuffer, int count);
	BOOL PLC_Write_ZR2(int nPLCNum, int address, INT input);
	BOOL PLC_Write_ZR2(int nPLCNum, int address, SHORT* inputBuffer, int count);
	BOOL PLC_Read_M(int nPLCNum, int address, int count, MADDRESS* result);
	int PLC_Read_M2(int nPLCNum, int address);
	BOOL PLC_Write_M(int nPLCNum, int address, bool input);
	BOOL PLC_Read_X(int nPLCNum, int address, int count, MADDRESS* result);
	BOOL PLC_Read_Y(int nPLCNum, int address, int count, MADDRESS* result);
	BOOL PLC_Write_Y(int nPLCNum, CString address_str, bool input);
	int PLC_Read_L2(int nPLCNum, int address);
	BOOL PLC_Write_L(int nPLCNum, int address, bool input);

	DWORD HexStrToInt(CString aStr);
	void IsOn16Bit(int nAddress ,int count , MADDRESS* val);

	// LOT 관리 함수
	BOOL IsExistLotInfo(LPCTSTR strLotID, LPCTSTR strRFID);
	BOOL IsExistLotInfoByLotID(LPCTSTR strLotID);
	BOOL IsExistLotInfoByRFID(LPCTSTR strRFID);
	int FindLotIndexByLotID(LPCTSTR strLotID);
	int FindLotIndexByPos(int nPos);
	int FindBundleIndexByRFID(int nFindLotIndex, LPCTSTR strRFID);
	int FindBundleIndexByPos(int nFindLotIndex, int nPos);
	int FindBoxIndexByBOXID(int nFindLotIndex, int nFindBundleIndex, int nBoxID);
	int FindBoxIndexByPos(int nFindLotIndex, int nFindBundleIndex, int nPos);
	int FindEmptyBoxIndex(int nFindLotIndex, int nFindBundleIndex);
	BOOL AddLotInfo(stMESInfo_READY_TO_LOAD_BARCODE stMESInfo);
	void UpdateLotInfo();
	void DeleteBoxInfo(int nPos);
	//void SaveLotInfoFile();
	//void LoadLotInfoFile();

public:
	// 장비 정보
	MCH_INFO m_stMCHINFO;
	// 전체 LOT 정보
	stLotInfo m_arrayLOTINFO[TOTAL_LOT_COUNT];
	// 포지션별 자재 정보 - R어드레스 정보 바탕
	stTrayPackInfo m_arrayTrayPackInfo[16];
	// MES 처리 프로세스 
	stMESInfo_READY_TO_LOAD_BARCODE m_MES_RFID;
	// 알람 발생 리스트 관리 - MES 용

	// PLC 연결 객체
	CMxComp3 m_MxComp3_PLC_1;
	CMxComp3 m_MxComp3_PLC_2;
	CMxComp3 m_MxComp3_PLC_3;

	CPLCReaderX	m_PLCReaderX;
	CPLCReaderY	m_PLCReaderY;
	CPLCReaderM	m_PLCReaderM;
	CPLCReaderL	m_PLCReaderL_Manual;
	CPLCReaderL	m_PLCReaderL_Motor;
	CPLCReaderD	m_PLCReaderD_Motor;
	CPLCReaderD	m_PLCReaderD_Alarm_G1;
	CPLCReaderD	m_PLCReaderD_Alarm_G2;
	CPLCReaderD	m_PLCReaderD_Alarm_G3;
	CPLCReaderZR m_PLCReaderZR;				// TrayInfo, DeviceInfo - 필요시만 사용
	CPLCReaderR m_PLCReaderR_G1;			// G1 자재 위치별 정보 - 읽기 전용
	CPLCReaderR m_PLCReaderR_G2;			// G2 자재 위치별 정보 - 읽기 전용
	CPLCReaderR m_PLCReaderR_G3;			// G3 자재 위치별 정보 - 읽기 전용
	CPLCReaderD	m_PLCReaderD_Process_G1;	// G1 자재 위치별 프로세스 처리 - 읽기쓰기
	CPLCReaderD	m_PLCReaderD_Process_G2;	// G2 자재 위치별 프로세스 처리 - 읽기쓰기
	CPLCReaderD	m_PLCReaderD_Process_G3;	// G3 자재 위치별 프로세스 처리 - 읽기쓰기
};

