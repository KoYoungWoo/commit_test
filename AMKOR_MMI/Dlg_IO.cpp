// Dlg_IO.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "AMKOR_MMI.h"
#include "AMKOR_MMIDlg.h"
#include "Dlg_IO.h"
#include "Dlg_NumPad.h"
#include "afxdialogex.h"


// CDlg_IO 대화 상자입니다.

IMPLEMENT_DYNAMIC(CDlg_IO, CDialogEx)

CDlg_IO::CDlg_IO(CWnd* pParent /*=NULL*/)
	: CDialogEx(CDlg_IO::IDD, pParent)
{
	m_bCtrl_FirstLoad = FALSE;
	m_nCurInputView = 0;
	m_nCurOutputView = 0;
}

CDlg_IO::~CDlg_IO()
{
}

void CDlg_IO::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_GB_IO_1, m_gb_1);
	DDX_Control(pDX, IDC_GB_IO_2, m_gb_2);
	DDX_Control(pDX, IDC_ST_IO_T1, m_st_T1);
	DDX_Control(pDX, IDC_ST_IO_T2, m_st_T2);
	DDX_Control(pDX, IDC_ST_IO_T3, m_st_T3);
	DDX_Control(pDX, IDC_ST_IO_T4, m_st_T4);
	DDX_Control(pDX, IDC_ST_IO_T5, m_st_T5);
	DDX_Control(pDX, IDC_ST_IO_T6, m_st_T6);
	DDX_Control(pDX, IDC_ST_IO_T7, m_st_T7);
	DDX_Control(pDX, IDC_ST_IO_T8, m_st_T8);
	DDX_Control(pDX, IDC_GC_IO_1, m_GC_1);
	DDX_Control(pDX, IDC_GC_IO_2, m_GC_2);
	DDX_Control(pDX, IDC_GC_IO_3, m_GC_3);
	DDX_Control(pDX, IDC_GC_IO_4, m_GC_4);
	DDX_Control(pDX, IDC_BTN_IO_1, m_btn_1);
	DDX_Control(pDX, IDC_BTN_IO_2, m_btn_2);
	DDX_Control(pDX, IDC_BTN_IO_3, m_btn_3);
	DDX_Control(pDX, IDC_BTN_IO_4, m_btn_4);
	DDX_Control(pDX, IDC_BTN_IO_5, m_btn_5);
	DDX_Control(pDX, IDC_BTN_IO_6, m_btn_6);
	DDX_Control(pDX, IDC_BTN_IO_7, m_btn_7);
}


BEGIN_MESSAGE_MAP(CDlg_IO, CDialogEx)
	ON_WM_ERASEBKGND()
	ON_WM_SIZE()
	ON_WM_TIMER()
	ON_NOTIFY(NM_CLICK, IDC_GC_IO_1, OnGridClick_GC_1)
	ON_NOTIFY(NM_CLICK, IDC_GC_IO_2, OnGridClick_GC_2)
	ON_NOTIFY(NM_CLICK, IDC_GC_IO_3, OnGridClick_GC_3)
	ON_NOTIFY(NM_CLICK, IDC_GC_IO_4, OnGridClick_GC_4)
	ON_NOTIFY(GVN_ENDLABELEDIT, IDC_GC_IO_1, OnEndLabel_GC_1)
	ON_NOTIFY(GVN_ENDLABELEDIT, IDC_GC_IO_2, OnEndLabel_GC_2)
	ON_NOTIFY(GVN_ENDLABELEDIT, IDC_GC_IO_3, OnEndLabel_GC_3)
	ON_NOTIFY(GVN_ENDLABELEDIT, IDC_GC_IO_4, OnEndLabel_GC_4)
	ON_BN_CLICKED(IDC_BTN_IO_1, &CDlg_IO::OnBnClickedBtnIo1)
	ON_BN_CLICKED(IDC_BTN_IO_2, &CDlg_IO::OnBnClickedBtnIo2)
	ON_BN_CLICKED(IDC_BTN_IO_3, &CDlg_IO::OnBnClickedBtnIo3)
	ON_BN_CLICKED(IDC_BTN_IO_4, &CDlg_IO::OnBnClickedBtnIo4)
	ON_BN_CLICKED(IDC_BTN_IO_5, &CDlg_IO::OnBnClickedBtnIo5)
	ON_BN_CLICKED(IDC_BTN_IO_6, &CDlg_IO::OnBnClickedBtnIo6)
	ON_BN_CLICKED(IDC_BTN_IO_7, &CDlg_IO::OnBnClickedBtnIo7)
END_MESSAGE_MAP()


// CDlg_IO 메시지 처리기입니다.


BOOL CDlg_IO::OnEraseBkgnd(CDC* pDC)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	// 전체 배경색
	CRect rect;
    GetClientRect(&rect);
    CBrush myBrush(CR_MAIN_BKGND);	// dialog background color
    CBrush *pOld = pDC->SelectObject(&myBrush);
    BOOL bRes  = pDC->PatBlt(0, 0, rect.Width(), rect.Height(), PATCOPY);
    pDC->SelectObject(pOld);		// restore old brush
    return FALSE;     
	//return CDialogEx::OnEraseBkgnd(pDC);
}


void CDlg_IO::OnSize(UINT nType, int cx, int cy)
{
	CDialogEx::OnSize(nType, cx, cy);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	if(m_bCtrl_FirstLoad == TRUE) Cal_CtrlArea();
}


void CDlg_IO::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CDialogEx::OnTimer(nIDEvent);
}


BOOL CDlg_IO::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	Cal_CtrlArea();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


BOOL CDlg_IO::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message == WM_SYSKEYDOWN)
	{
		if(pMsg->wParam == VK_F4) 
			return TRUE;
	}

	if(pMsg->message == WM_KEYDOWN)
	{
		if( pMsg->wParam == VK_ESCAPE ||	
			//pMsg->wParam == VK_RETURN ||	
			//pMsg->wParam == VK_SPACE  ||	// Grid Ctrl Text 입력시 사용하기때문에...
			pMsg->wParam == VK_CANCEL )     
			return TRUE;        
	}

	return CDialogEx::PreTranslateMessage(pMsg);
}

void CDlg_IO::Cal_Area()
{
	CRect ClientRect;
	GetClientRect(&ClientRect);

	int n50pW = (int)((float)ClientRect.Width()*0.50f);

	m_rcLeft.SetRect(ClientRect.left, ClientRect.top, ClientRect.left+n50pW-CTRL_MARGIN4, ClientRect.bottom);
	m_rcRight.SetRect(ClientRect.left+n50pW+CTRL_MARGIN4, ClientRect.top, ClientRect.right, ClientRect.bottom);
}

void CDlg_IO::Cal_CtrlArea()
{
	Cal_Area();

	Cal_CtrlArea_Left();
	Cal_CtrlArea_Right();

	m_bCtrl_FirstLoad = TRUE;
}

void CDlg_IO::Cal_CtrlArea_Left()
{
	CRect CalRect;
	CRect rcGBArea, rcBtnArea;
	CRect rcGBInside, rcGB_UP, rcGB_DN;
	int nBtnWH = 60;

	rcGBArea.SetRect(m_rcLeft.left, m_rcLeft.top, m_rcLeft.right, m_rcLeft.bottom - nBtnWH - CTRL_MARGIN4);
	rcBtnArea.SetRect(m_rcLeft.left, m_rcLeft.bottom - nBtnWH, m_rcLeft.right, m_rcLeft.bottom);
	rcBtnArea.DeflateRect(8,0,8,10);

	rcGBInside = rcGBArea;
	rcGBInside.DeflateRect(6,28,6,6);
	
	m_gb_1.SetXPGroupStyle(CXPGroupBox::XPGB_WINDOW_2);
	m_gb_1.SetBackgroundColor(CR_DARKGRAY, CR_GB_BKGND);
	m_gb_1.SetBorderColor(CR_BLACK);
	m_gb_1.SetCatptionTextColor(CR_BLACK);
	m_gb_1.MoveWindow(&m_rcLeft);

	int n50pH = (int)((float)rcGBInside.Height()*0.50f);
	rcGB_UP.SetRect(rcGBInside.left, rcGBInside.top, rcGBInside.right, rcGBInside.top+n50pH-CTRL_MARGIN2);
	rcGB_DN.SetRect(rcGBInside.left, rcGBInside.top+n50pH+CTRL_MARGIN2, rcGBInside.right, rcGBInside.bottom);
	int nTitleH = 25;
	int nNumW = 60;

	CalRect.SetRect(rcGB_UP.left, rcGB_UP.top, rcGB_UP.left+nNumW, rcGB_UP.top+nTitleH);
	m_st_T1.SetBkColor(CR_DODGERBLUE);
	m_st_T1.SetTextColor(CR_WHITE);
	m_st_T1.SetFont(CTRL_FONT,20,FW_BOLD);
	m_st_T1.MoveWindow(&CalRect);
	CalRect.SetRect(rcGB_UP.left+nNumW, rcGB_UP.top, rcGB_UP.right, rcGB_UP.top+nTitleH);
	m_st_T2.SetBkColor(CR_DODGERBLUE);
	m_st_T2.SetTextColor(CR_WHITE);
	m_st_T2.SetFont(CTRL_FONT,20,FW_BOLD);
	m_st_T2.MoveWindow(&CalRect);
	CalRect.SetRect(rcGB_UP.left, rcGB_UP.top+nTitleH, rcGB_UP.right, rcGB_UP.bottom);
	m_GC_1.MoveWindow(&CalRect);
	Init_GC_1(17,3);

	CalRect.SetRect(rcGB_DN.left, rcGB_DN.top, rcGB_DN.left+nNumW, rcGB_DN.top+nTitleH);
	m_st_T3.SetBkColor(CR_DODGERBLUE);
	m_st_T3.SetTextColor(CR_WHITE);
	m_st_T3.SetFont(CTRL_FONT,20,FW_BOLD);
	m_st_T3.MoveWindow(&CalRect);
	CalRect.SetRect(rcGB_DN.left+nNumW, rcGB_DN.top, rcGB_DN.right, rcGB_DN.top+nTitleH);
	m_st_T4.SetBkColor(CR_DODGERBLUE);
	m_st_T4.SetTextColor(CR_WHITE);
	m_st_T4.SetFont(CTRL_FONT,20,FW_BOLD);
	m_st_T4.MoveWindow(&CalRect);
	CalRect.SetRect(rcGB_DN.left, rcGB_DN.top+nTitleH, rcGB_DN.right, rcGB_DN.bottom);
	m_GC_2.MoveWindow(&CalRect);
	Init_GC_2(17,3);

	CalRect.SetRect(rcBtnArea.left, rcBtnArea.top, rcBtnArea.left + 140, rcBtnArea.bottom);
	m_btn_1.SetFontBtn(CTRL_FONT, 24, FW_BOLD);
	m_btn_1.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_1.SetTextColor(CR_BLACK);
	m_btn_1.SetIconPos(0);
	m_btn_1.SetIconSize(48,48);
	m_btn_1.SetIconID(IDI_PREV);
	m_btn_1.SetFlatType(TRUE);
	m_btn_1.MoveWindow(&CalRect);
	CalRect.SetRect(CalRect.right + CTRL_MARGIN4, rcBtnArea.top, CalRect.right + 140 + CTRL_MARGIN4, rcBtnArea.bottom);
	m_btn_2.SetFontBtn(CTRL_FONT, 24, FW_BOLD);
	m_btn_2.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_2.SetTextColor(CR_BLACK);
	m_btn_2.SetIconPos(0);
	m_btn_2.SetIconID(IDI_NEXT);
	m_btn_2.SetIconSize(48,48);
	m_btn_2.SetFlatType(TRUE);
	m_btn_2.MoveWindow(&CalRect);
	CalRect.SetRect(CalRect.right + CTRL_MARGIN4, rcBtnArea.top, CalRect.right + 140 + CTRL_MARGIN4, rcBtnArea.bottom);
	m_btn_5.SetFontBtn(CTRL_FONT, 24, FW_BOLD);
	m_btn_5.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_5.SetTextColor(CR_BLACK);
	m_btn_5.SetIconPos(0);
	m_btn_5.SetIconID(IDI_SAVE);
	m_btn_5.SetIconSize(48,48);
	m_btn_5.SetFlatType(TRUE);
	m_btn_5.MoveWindow(&CalRect);
}

void CDlg_IO::Cal_CtrlArea_Right()
{
	CRect CalRect;
	CRect rcGBArea, rcBtnArea;
	CRect rcGBInside, rcGB_UP, rcGB_DN;
	int nBtnWH = 60;

	rcGBArea.SetRect(m_rcRight.left, m_rcRight.top, m_rcRight.right, m_rcRight.bottom - nBtnWH - CTRL_MARGIN4);
	rcBtnArea.SetRect(m_rcRight.left, m_rcRight.bottom - nBtnWH, m_rcRight.right, m_rcRight.bottom);
	rcBtnArea.DeflateRect(8,0,8,10);

	rcGBInside = rcGBArea;
	rcGBInside.DeflateRect(6,28,6,6);
	
	m_gb_2.SetXPGroupStyle(CXPGroupBox::XPGB_WINDOW_2);
	m_gb_2.SetBackgroundColor(CR_DARKGRAY, CR_GB_BKGND);
	m_gb_2.SetBorderColor(CR_BLACK);
	m_gb_2.SetCatptionTextColor(CR_BLACK);
	m_gb_2.MoveWindow(&m_rcRight);

	int n50pH = (int)((float)rcGBInside.Height()*0.50f);
	rcGB_UP.SetRect(rcGBInside.left, rcGBInside.top, rcGBInside.right, rcGBInside.top+n50pH-CTRL_MARGIN2);
	rcGB_DN.SetRect(rcGBInside.left, rcGBInside.top+n50pH+CTRL_MARGIN2, rcGBInside.right, rcGBInside.bottom);
	int nTitleH = 25;
	int nNumW = 60;

	CalRect.SetRect(rcGB_UP.left, rcGB_UP.top, rcGB_UP.left+nNumW, rcGB_UP.top+nTitleH);
	m_st_T5.SetBkColor(CR_DARKORANGE);
	m_st_T5.SetTextColor(CR_WHITE);
	m_st_T5.SetFont(CTRL_FONT,20,FW_BOLD);
	m_st_T5.MoveWindow(&CalRect);
	CalRect.SetRect(rcGB_UP.left+nNumW, rcGB_UP.top, rcGB_UP.right, rcGB_UP.top+nTitleH);
	m_st_T6.SetBkColor(CR_DARKORANGE);
	m_st_T6.SetTextColor(CR_WHITE);
	m_st_T6.SetFont(CTRL_FONT,20,FW_BOLD);
	m_st_T6.MoveWindow(&CalRect);
	CalRect.SetRect(rcGB_UP.left, rcGB_UP.top+nTitleH, rcGB_UP.right, rcGB_UP.bottom);
	m_GC_3.MoveWindow(&CalRect);
	Init_GC_3(17,3);

	CalRect.SetRect(rcGB_DN.left, rcGB_DN.top, rcGB_DN.left+nNumW, rcGB_DN.top+nTitleH);
	m_st_T7.SetBkColor(CR_DARKORANGE);
	m_st_T7.SetTextColor(CR_WHITE);
	m_st_T7.SetFont(CTRL_FONT,20,FW_BOLD);
	m_st_T7.MoveWindow(&CalRect);
	CalRect.SetRect(rcGB_DN.left+nNumW, rcGB_DN.top, rcGB_DN.right, rcGB_DN.top+nTitleH);
	m_st_T8.SetBkColor(CR_DARKORANGE);
	m_st_T8.SetTextColor(CR_WHITE);
	m_st_T8.SetFont(CTRL_FONT,20,FW_BOLD);
	m_st_T8.MoveWindow(&CalRect);
	CalRect.SetRect(rcGB_DN.left, rcGB_DN.top+nTitleH, rcGB_DN.right, rcGB_DN.bottom);
	m_GC_4.MoveWindow(&CalRect);
	Init_GC_4(17,3);

	CalRect.SetRect(rcBtnArea.left, rcBtnArea.top, rcBtnArea.left + 140, rcBtnArea.bottom);
	m_btn_3.SetFontBtn(CTRL_FONT, 24, FW_BOLD);
	m_btn_3.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_3.SetTextColor(CR_BLACK);
	m_btn_3.SetIconPos(0);
	m_btn_3.SetIconID(IDI_PREV);
	m_btn_3.SetIconSize(48,48);
	m_btn_3.SetFlatType(TRUE);
	m_btn_3.MoveWindow(&CalRect);
	CalRect.SetRect(CalRect.right + CTRL_MARGIN4, rcBtnArea.top, CalRect.right + 140 + CTRL_MARGIN4, rcBtnArea.bottom);
	m_btn_4.SetFontBtn(CTRL_FONT, 24, FW_BOLD);
	m_btn_4.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_4.SetTextColor(CR_BLACK);
	m_btn_4.SetIconPos(0);
	m_btn_4.SetIconID(IDI_NEXT);
	m_btn_4.SetIconSize(48,48);
	m_btn_4.SetFlatType(TRUE);
	m_btn_4.MoveWindow(&CalRect);
	CalRect.SetRect(CalRect.right + CTRL_MARGIN4, rcBtnArea.top, CalRect.right + 140 + CTRL_MARGIN4, rcBtnArea.bottom);
	m_btn_6.SetFontBtn(CTRL_FONT, 24, FW_BOLD);
	m_btn_6.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_6.SetTextColor(CR_BLACK);
	m_btn_6.SetIconPos(0);
	m_btn_6.SetIconID(IDI_SAVE);
	m_btn_6.SetIconSize(48,48);
	m_btn_6.SetFlatType(TRUE);
	m_btn_6.MoveWindow(&CalRect);
	CalRect.SetRect(rcBtnArea.right - 140, rcBtnArea.top, rcBtnArea.right, rcBtnArea.bottom);
	m_btn_7.SetFontBtn(CTRL_FONT, 24, FW_BOLD);
	m_btn_7.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_7.SetTextColor(CR_BLACK);
	m_btn_7.SetIconPos(0);
	m_btn_7.SetIconID(IDI_CLOSE);
	m_btn_7.SetIconSize(48,48);
	m_btn_7.SetFlatType(TRUE);
	m_btn_7.MoveWindow(&CalRect);
}

bool CDlg_IO::Init_GC_1(int nRows, int nCols)
{
	BOOL m_bEditable = TRUE;
    BOOL m_bListMode = TRUE;
    int m_nRows = nRows;
    int m_nCols = nCols;
    int m_nFixRows = 1;
    int m_nFixCols = 1;

    m_GC_1.SetEditable(m_bEditable);
    m_GC_1.SetListMode(m_bListMode);
    m_GC_1.EnableDragAndDrop(FALSE);
    m_GC_1.SetTextBkColor(RGB(0xFF, 0xFF, 0xE0));

    m_GC_1.SetRowCount(m_nRows);
    m_GC_1.SetColumnCount(m_nCols);
    m_GC_1.SetFixedRowCount(m_nFixRows);
    m_GC_1.SetFixedColumnCount(m_nFixCols);

	m_GC_1.EnableSelection(false);
	m_GC_1.SetSingleColSelection(true);
	m_GC_1.SetSingleRowSelection(true);
	m_GC_1.SetFixedColumnSelection(false);
    m_GC_1.SetFixedRowSelection(false);

	m_GC_1.SetRowResize(false);
	m_GC_1.SetColumnResize(false);

	CRect ClientRect;
	GetDlgItem(IDC_GC_IO_1)->GetWindowRect(&ClientRect);
	if(ClientRect.Width() < 4) return false;
	if(ClientRect.Height() < 4) return false;
	int nClientWidth = ClientRect.Width()-4;
	int nClientHeight = ClientRect.Height()-4;
	int n15pW = (int)(nClientWidth*0.15f);
	int n65pW = (int)(nClientWidth*0.65f);
	int nRemainW = nClientWidth - n15pW - n65pW;
	//int nCellW = nClientWidth/m_nCols;
	int nCellH = nClientHeight/m_nRows;
	//int nLastCellW = nClientWidth-nCellW*(m_nCols-1);
	int nLastCellH = nClientHeight-nCellH*(m_nRows-1);
	//int nRemainW = nLastCellW - nCellW;
	int nRemainH = nLastCellH - nCellH;
	for (int i = 0; i < m_GC_1.GetRowCount(); i++) 
	{
		if(i == m_nRows-1)
		{
				m_GC_1.SetRowHeight(i,nCellH);
		}
		else 
		{
			if(nRemainH > 0)
			{
				m_GC_1.SetRowHeight(i,nCellH + 1);
				nRemainH --;
			}
			else
				m_GC_1.SetRowHeight(i,nCellH);
		}
	}
	for (int j = 0; j < m_GC_1.GetColumnCount(); j++)
	{
		if(j == 0) m_GC_1.SetColumnWidth(j,n15pW);
		else if(j == 1) m_GC_1.SetColumnWidth(j,n65pW);
		else if(j == 2) m_GC_1.SetColumnWidth(j,nRemainW);
	}
    DWORD dwTextStyle = DT_CENTER|DT_VCENTER|DT_SINGLELINE;

    // fill rows/cols with text
    for (int row = 0; row < m_GC_1.GetRowCount(); row++) 
	{
        for (int col = 0; col < m_GC_1.GetColumnCount(); col++) 
		{ 
            GV_ITEM Item;
            Item.mask = GVIF_TEXT|GVIF_FORMAT;
            Item.row = row;
            Item.col = col;
			Item.nFormat = dwTextStyle;

			if(row == 0)
			{
				if(col == 0) Item.strText.Format(_T("Address"));
				else if(col == 1) Item.strText.Format(_T("ITEM NAME"));
				else if(col == 2) Item.strText.Format(_T("STATUS"));
			}
			else
			{
				if(col == 0)
				{
					Item.strText.Format(_T("%d"), row);
				}
				else if(col == 1)
				{
					
				}
				else if(col == 2)
				{
					
				}
			}

			if(row == 0 || col == 0) m_GC_1.SetItemBkColour(row, col, CR_SLATEGRAY);
			m_GC_1.SetItemFgColour(row, col, RGB(0,0,0));
			m_GC_1.SetItemFont(row, col, CTRL_FONT, 18, FW_BOLD);
            m_GC_1.SetItem(&Item);  
        }
    }
       
    return TRUE;  // return TRUE  unless you set the focus to a control
}

void CDlg_IO::OnGridClick_GC_1(NMHDR *pNotifyStruct, LRESULT* /*pResult*/)
{
	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*) pNotifyStruct;

	int nSelCol = pItem->iColumn;
}

void CDlg_IO::OnEndLabel_GC_1(NMHDR *pNotifyStruct, LRESULT* pResult)
{
	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*) pNotifyStruct;

	int nSelRow = pItem->iRow;
	int nSelCol = pItem->iColumn;
}

bool CDlg_IO::Init_GC_2(int nRows, int nCols)
{
	BOOL m_bEditable = TRUE;
    BOOL m_bListMode = TRUE;
    int m_nRows = nRows;
    int m_nCols = nCols;
    int m_nFixRows = 1;
    int m_nFixCols = 1;

    m_GC_2.SetEditable(m_bEditable);
    m_GC_2.SetListMode(m_bListMode);
    m_GC_2.EnableDragAndDrop(FALSE);
    m_GC_2.SetTextBkColor(RGB(0xFF, 0xFF, 0xE0));

    m_GC_2.SetRowCount(m_nRows);
    m_GC_2.SetColumnCount(m_nCols);
    m_GC_2.SetFixedRowCount(m_nFixRows);
    m_GC_2.SetFixedColumnCount(m_nFixCols);

	m_GC_2.EnableSelection(false);
	m_GC_2.SetSingleColSelection(true);
	m_GC_2.SetSingleRowSelection(true);
	m_GC_2.SetFixedColumnSelection(false);
    m_GC_2.SetFixedRowSelection(false);

	m_GC_2.SetRowResize(false);
	m_GC_2.SetColumnResize(false);

	CRect ClientRect;
	GetDlgItem(IDC_GC_IO_2)->GetWindowRect(&ClientRect);
	if(ClientRect.Width() < 4) return false;
	if(ClientRect.Height() < 4) return false;
	int nClientWidth = ClientRect.Width()-4;
	int nClientHeight = ClientRect.Height()-4;
	int n15pW = (int)(nClientWidth*0.15f);
	int n65pW = (int)(nClientWidth*0.65f);
	int nRemainW = nClientWidth - n15pW - n65pW;
	//int nCellW = nClientWidth/m_nCols;
	int nCellH = nClientHeight/m_nRows;
	//int nLastCellW = nClientWidth-nCellW*(m_nCols-1);
	int nLastCellH = nClientHeight-nCellH*(m_nRows-1);
	//int nRemainW = nLastCellW - nCellW;
	int nRemainH = nLastCellH - nCellH;
	for (int i = 0; i < m_GC_1.GetRowCount(); i++) 
	{
		if(i == m_nRows-1)
		{
				m_GC_2.SetRowHeight(i,nCellH);
		}
		else 
		{
			if(nRemainH > 0)
			{
				m_GC_2.SetRowHeight(i,nCellH + 1);
				nRemainH --;
			}
			else
				m_GC_2.SetRowHeight(i,nCellH);
		}
	}
	for (int j = 0; j < m_GC_2.GetColumnCount(); j++)
	{
		if(j == 0) m_GC_2.SetColumnWidth(j,n15pW);
		else if(j == 1) m_GC_2.SetColumnWidth(j,n65pW);
		else if(j == 2) m_GC_2.SetColumnWidth(j,nRemainW);
	}
    DWORD dwTextStyle = DT_CENTER|DT_VCENTER|DT_SINGLELINE;

    // fill rows/cols with text
    for (int row = 0; row < m_GC_2.GetRowCount(); row++) 
	{
        for (int col = 0; col < m_GC_2.GetColumnCount(); col++) 
		{ 
            GV_ITEM Item;
            Item.mask = GVIF_TEXT|GVIF_FORMAT;
            Item.row = row;
            Item.col = col;
			Item.nFormat = dwTextStyle;

			if(row == 0)
			{
				if(col == 0) Item.strText.Format(_T("Address"));
				else if(col == 1) Item.strText.Format(_T("ITEM NAME"));
				else if(col == 2) Item.strText.Format(_T("STATUS"));
			}
			else
			{
				if(col == 0)
				{
					Item.strText.Format(_T("%d"), row);
				}
				else if(col == 1)
				{
					
				}
				else if(col == 2)
				{
					
				}
			}

			if(row == 0 || col == 0) m_GC_2.SetItemBkColour(row, col, CR_SLATEGRAY);
			m_GC_2.SetItemFgColour(row, col, RGB(0,0,0));
			m_GC_2.SetItemFont(row, col, CTRL_FONT, 18, FW_BOLD);
            m_GC_2.SetItem(&Item);  
        }
    }
       
    return TRUE;  // return TRUE  unless you set the focus to a control
}

void CDlg_IO::OnGridClick_GC_2(NMHDR *pNotifyStruct, LRESULT* /*pResult*/)
{
	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*) pNotifyStruct;

	int nSelCol = pItem->iColumn;
}

void CDlg_IO::OnEndLabel_GC_2(NMHDR *pNotifyStruct, LRESULT* pResult)
{
	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*) pNotifyStruct;

	int nSelRow = pItem->iRow;
	int nSelCol = pItem->iColumn;
}

bool CDlg_IO::Init_GC_3(int nRows, int nCols)
{
	BOOL m_bEditable = TRUE;
    BOOL m_bListMode = TRUE;
    int m_nRows = nRows;
    int m_nCols = nCols;
    int m_nFixRows = 1;
    int m_nFixCols = 1;

    m_GC_3.SetEditable(m_bEditable);
    m_GC_3.SetListMode(m_bListMode);
    m_GC_3.EnableDragAndDrop(FALSE);
    m_GC_3.SetTextBkColor(RGB(0xFF, 0xFF, 0xE0));

    m_GC_3.SetRowCount(m_nRows);
    m_GC_3.SetColumnCount(m_nCols);
    m_GC_3.SetFixedRowCount(m_nFixRows);
    m_GC_3.SetFixedColumnCount(m_nFixCols);

	m_GC_3.EnableSelection(false);
	m_GC_3.SetSingleColSelection(true);
	m_GC_3.SetSingleRowSelection(true);
	m_GC_3.SetFixedColumnSelection(false);
    m_GC_3.SetFixedRowSelection(false);

	m_GC_3.SetRowResize(false);
	m_GC_3.SetColumnResize(false);

	CRect ClientRect;
	GetDlgItem(IDC_GC_IO_3)->GetWindowRect(&ClientRect);
	if(ClientRect.Width() < 4) return false;
	if(ClientRect.Height() < 4) return false;
	int nClientWidth = ClientRect.Width()-4;
	int nClientHeight = ClientRect.Height()-4;
	int n15pW = (int)(nClientWidth*0.15f);
	int n65pW = (int)(nClientWidth*0.65f);
	int nRemainW = nClientWidth - n15pW - n65pW;
	//int nCellW = nClientWidth/m_nCols;
	int nCellH = nClientHeight/m_nRows;
	//int nLastCellW = nClientWidth-nCellW*(m_nCols-1);
	int nLastCellH = nClientHeight-nCellH*(m_nRows-1);
	//int nRemainW = nLastCellW - nCellW;
	int nRemainH = nLastCellH - nCellH;
	for (int i = 0; i < m_GC_3.GetRowCount(); i++) 
	{
		if(i == m_nRows-1)
		{
				m_GC_3.SetRowHeight(i,nCellH);
		}
		else 
		{
			if(nRemainH > 0)
			{
				m_GC_3.SetRowHeight(i,nCellH + 1);
				nRemainH --;
			}
			else
				m_GC_3.SetRowHeight(i,nCellH);
		}
	}
	for (int j = 0; j < m_GC_3.GetColumnCount(); j++)
	{
		if(j == 0) m_GC_3.SetColumnWidth(j,n15pW);
		else if(j == 1) m_GC_3.SetColumnWidth(j,n65pW);
		else if(j == 2) m_GC_3.SetColumnWidth(j,nRemainW);
	}
    DWORD dwTextStyle = DT_CENTER|DT_VCENTER|DT_SINGLELINE;

    // fill rows/cols with text
    for (int row = 0; row < m_GC_3.GetRowCount(); row++) 
	{
        for (int col = 0; col < m_GC_3.GetColumnCount(); col++) 
		{ 
            GV_ITEM Item;
            Item.mask = GVIF_TEXT|GVIF_FORMAT;
            Item.row = row;
            Item.col = col;
			Item.nFormat = dwTextStyle;

			if(row == 0)
			{
				if(col == 0) Item.strText.Format(_T("Address"));
				else if(col == 1) Item.strText.Format(_T("ITEM NAME"));
				else if(col == 2) Item.strText.Format(_T("STATUS"));
			}
			else
			{
				if(col == 0)
				{
					Item.strText.Format(_T("%d"), row);
				}
				else if(col == 1)
				{
					
				}
				else if(col == 2)
				{
					
				}
			}

			if(row == 0 || col == 0) m_GC_3.SetItemBkColour(row, col, CR_SLATEGRAY);
			m_GC_3.SetItemFgColour(row, col, RGB(0,0,0));
			m_GC_3.SetItemFont(row, col, CTRL_FONT, 18, FW_BOLD);
            m_GC_3.SetItem(&Item);  
        }
    }
       
    return TRUE;  // return TRUE  unless you set the focus to a control
}

void CDlg_IO::OnGridClick_GC_3(NMHDR *pNotifyStruct, LRESULT* /*pResult*/)
{
	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*) pNotifyStruct;

	int nSelRow = pItem->iRow;
	int nSelCol = pItem->iColumn;
	if(nSelCol == 2 && nSelRow > 0)
	{
		int nPLCNum = CAppSetting::Instance()->GetGroupNumber();
		CString strYAddress = m_GC_3.GetItemText(nSelRow, 0);

		CDlg_NumPad dlg;
		dlg.m_strCaption.Format(_T("Output Signal [%s]"), strYAddress);
		dlg.m_bStringMode = TRUE;
		if(dlg.DoModal() == IDOK)
		{
			if(dlg.m_strValue.CompareNoCase(_T("0000")) == 0)
			{
				int nStartAddress = CDataManager::Instance()->HexStrToInt(strYAddress);
				if(CDataManager::Instance()->m_PLCReaderY.IsONHex(nStartAddress) == TRUE)
				{
					CDataManager::Instance()->PLC_Write_Y(nPLCNum, strYAddress, FALSE);
				}
				else
				{
					CDataManager::Instance()->PLC_Write_Y(nPLCNum, strYAddress, TRUE);
				}
			}
		}
	}
}

void CDlg_IO::OnEndLabel_GC_3(NMHDR *pNotifyStruct, LRESULT* pResult)
{
	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*) pNotifyStruct;

	int nSelRow = pItem->iRow;
	int nSelCol = pItem->iColumn;
}

bool CDlg_IO::Init_GC_4(int nRows, int nCols)
{
	BOOL m_bEditable = TRUE;
    BOOL m_bListMode = TRUE;
    int m_nRows = nRows;
    int m_nCols = nCols;
    int m_nFixRows = 1;
    int m_nFixCols = 1;

    m_GC_4.SetEditable(m_bEditable);
    m_GC_4.SetListMode(m_bListMode);
    m_GC_4.EnableDragAndDrop(FALSE);
    m_GC_4.SetTextBkColor(RGB(0xFF, 0xFF, 0xE0));

    m_GC_4.SetRowCount(m_nRows);
    m_GC_4.SetColumnCount(m_nCols);
    m_GC_4.SetFixedRowCount(m_nFixRows);
    m_GC_4.SetFixedColumnCount(m_nFixCols);

	m_GC_4.EnableSelection(false);
	m_GC_4.SetSingleColSelection(true);
	m_GC_4.SetSingleRowSelection(true);
	m_GC_4.SetFixedColumnSelection(false);
    m_GC_4.SetFixedRowSelection(false);

	m_GC_4.SetRowResize(false);
	m_GC_4.SetColumnResize(false);

	CRect ClientRect;
	GetDlgItem(IDC_GC_IO_4)->GetWindowRect(&ClientRect);
	if(ClientRect.Width() < 4) return false;
	if(ClientRect.Height() < 4) return false;
	int nClientWidth = ClientRect.Width()-4;
	int nClientHeight = ClientRect.Height()-4;
	int n15pW = (int)(nClientWidth*0.15f);
	int n65pW = (int)(nClientWidth*0.65f);
	int nRemainW = nClientWidth - n15pW - n65pW;
	//int nCellW = nClientWidth/m_nCols;
	int nCellH = nClientHeight/m_nRows;
	//int nLastCellW = nClientWidth-nCellW*(m_nCols-1);
	int nLastCellH = nClientHeight-nCellH*(m_nRows-1);
	//int nRemainW = nLastCellW - nCellW;
	int nRemainH = nLastCellH - nCellH;
	for (int i = 0; i < m_GC_4.GetRowCount(); i++) 
	{
		if(i == m_nRows-1)
		{
				m_GC_4.SetRowHeight(i,nCellH);
		}
		else 
		{
			if(nRemainH > 0)
			{
				m_GC_4.SetRowHeight(i,nCellH + 1);
				nRemainH --;
			}
			else
				m_GC_4.SetRowHeight(i,nCellH);
		}
	}
	for (int j = 0; j < m_GC_4.GetColumnCount(); j++)
	{
		if(j == 0) m_GC_4.SetColumnWidth(j,n15pW);
		else if(j == 1) m_GC_4.SetColumnWidth(j,n65pW);
		else if(j == 2) m_GC_4.SetColumnWidth(j,nRemainW);
	}
    DWORD dwTextStyle = DT_CENTER|DT_VCENTER|DT_SINGLELINE;

    // fill rows/cols with text
    for (int row = 0; row < m_GC_4.GetRowCount(); row++) 
	{
        for (int col = 0; col < m_GC_4.GetColumnCount(); col++) 
		{ 
            GV_ITEM Item;
            Item.mask = GVIF_TEXT|GVIF_FORMAT;
            Item.row = row;
            Item.col = col;
			Item.nFormat = dwTextStyle;

			if(row == 0)
			{
				if(col == 0) Item.strText.Format(_T("Address"));
				else if(col == 1) Item.strText.Format(_T("ITEM NAME"));
				else if(col == 2) Item.strText.Format(_T("STATUS"));
			}
			else
			{
				if(col == 0)
				{
					Item.strText.Format(_T("%d"), row);
				}
				else if(col == 1)
				{
					
				}
				else if(col == 2)
				{
					
				}
			}

			if(row == 0 || col == 0) m_GC_4.SetItemBkColour(row, col, CR_SLATEGRAY);
			m_GC_4.SetItemFgColour(row, col, RGB(0,0,0));
			m_GC_4.SetItemFont(row, col, CTRL_FONT, 18, FW_BOLD);
            m_GC_4.SetItem(&Item);  
        }
    }
       
    return TRUE;  // return TRUE  unless you set the focus to a control
}

void CDlg_IO::OnGridClick_GC_4(NMHDR *pNotifyStruct, LRESULT* /*pResult*/)
{
	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*) pNotifyStruct;

	int nSelRow = pItem->iRow;
	int nSelCol = pItem->iColumn;
	if(nSelCol == 2 && nSelRow > 0)
	{
		int nPLCNum = CAppSetting::Instance()->GetGroupNumber();
		CString strYAddress = m_GC_4.GetItemText(nSelRow, 0);

		CDlg_NumPad dlg;
		dlg.m_strCaption.Format(_T("Output Signal [%s]"), strYAddress);
		dlg.m_bStringMode = TRUE;
		if(dlg.DoModal() == IDOK)
		{
			if(dlg.m_strValue.CompareNoCase(_T("0000")) == 0)
			{
				int nStartAddress = CDataManager::Instance()->HexStrToInt(strYAddress);
				if(CDataManager::Instance()->m_PLCReaderY.IsONHex(nStartAddress) == TRUE)
				{
					CDataManager::Instance()->PLC_Write_Y(nPLCNum, strYAddress, FALSE);
				}
				else
				{
					CDataManager::Instance()->PLC_Write_Y(nPLCNum, strYAddress, TRUE);
				}
			}
		}
	}
}

void CDlg_IO::OnEndLabel_GC_4(NMHDR *pNotifyStruct, LRESULT* pResult)
{
	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*) pNotifyStruct;

	int nSelRow = pItem->iRow;
	int nSelCol = pItem->iColumn;
}

void CDlg_IO::Update_IOList_Input()
{
	DWORD dwTextStyle = DT_LEFT|DT_VCENTER|DT_SINGLELINE;
	DWORD dwTextStyle1 = DT_CENTER|DT_VCENTER|DT_SINGLELINE;
	CString strHex;

	stIOData IOData1 = CIOListSetting::Instance()->m_stIOInfo.m_vecInput[m_nCurInputView*2 + 0];

    // fill rows/cols with text
    for (int row = 1; row < m_GC_1.GetRowCount(); row++) 
	{
        for (int col = 0; col < m_GC_1.GetColumnCount(); col++) 
		{ 
            GV_ITEM Item;
            Item.mask = GVIF_TEXT|GVIF_FORMAT|GVIF_BKCLR;
            Item.row = row;
            Item.col = col;
			Item.nFormat = dwTextStyle;
			Item.crBkClr = RGB(0xFF, 0xFF, 0xE0);

			if(col == 0)
			{
				Item.nFormat = dwTextStyle1;
				Item.crBkClr = CR_SLATEGRAY;
				Item.strText.Format(_T("X%X"), IOData1.m_nAddress + row - 1);
			}
			else if(col == 1)
			{
				Item.strText.Format(_T("%s"), IOData1.m_strText[row-1]);
			}
			else if(col == 2)
			{
				Item.strText.Format(_T(""));
			}

			m_GC_1.SetItemFgColour(row, col, RGB(0,0,0));
            m_GC_1.SetItem(&Item);  
        }
    }
	m_GC_1.Invalidate();

	stIOData IOData2 = CIOListSetting::Instance()->m_stIOInfo.m_vecInput[m_nCurInputView*2 + 1];

    // fill rows/cols with text
    for (int row = 1; row < m_GC_2.GetRowCount(); row++) 
	{
        for (int col = 0; col < m_GC_2.GetColumnCount(); col++) 
		{ 
            GV_ITEM Item;
            Item.mask = GVIF_TEXT|GVIF_FORMAT|GVIF_BKCLR;
            Item.row = row;
            Item.col = col;
			Item.nFormat = dwTextStyle;
			Item.crBkClr = RGB(0xFF, 0xFF, 0xE0);

			if(col == 0)
			{
				Item.nFormat = dwTextStyle1;
				Item.crBkClr = CR_SLATEGRAY;
				Item.strText.Format(_T("X%X"), IOData2.m_nAddress + row - 1);
			}
			else if(col == 1)
			{
				Item.strText.Format(_T("%s"), IOData2.m_strText[row-1]);
			}
			else if(col == 2)
			{
				Item.strText.Format(_T(""));
			}

			m_GC_2.SetItemFgColour(row, col, RGB(0,0,0));
            m_GC_2.SetItem(&Item);  
        }
    }
	m_GC_2.Invalidate();
}

void CDlg_IO::Update_IOList_Output()
{
	DWORD dwTextStyle = DT_LEFT|DT_VCENTER|DT_SINGLELINE;
	DWORD dwTextStyle1 = DT_CENTER|DT_VCENTER|DT_SINGLELINE;
	CString strHex;
	stIOData IOData1 = CIOListSetting::Instance()->m_stIOInfo.m_vecOutput[m_nCurOutputView*2 + 0];

    // fill rows/cols with text
    for (int row = 1; row < m_GC_3.GetRowCount(); row++) 
	{
        for (int col = 0; col < m_GC_3.GetColumnCount(); col++) 
		{ 
            GV_ITEM Item;
            Item.mask = GVIF_TEXT|GVIF_FORMAT|GVIF_BKCLR;
            Item.row = row;
            Item.col = col;
			Item.nFormat = dwTextStyle;
			Item.crBkClr = RGB(0xFF, 0xFF, 0xE0);

			if(col == 0)
			{
				Item.nFormat = dwTextStyle1;
				Item.crBkClr = CR_SLATEGRAY;
				Item.strText.Format(_T("Y%X"), IOData1.m_nAddress + row - 1);
			}
			else if(col == 1)
			{
				Item.strText.Format(_T("%s"), IOData1.m_strText[row-1]);
			}
			else if(col == 2)
			{
				Item.strText.Format(_T(""));
			}

			m_GC_3.SetItemFgColour(row, col, RGB(0,0,0));
            m_GC_3.SetItem(&Item);  
        }
    }
	m_GC_3.Invalidate();

	stIOData IOData2 = CIOListSetting::Instance()->m_stIOInfo.m_vecOutput[m_nCurOutputView*2 + 1];

    // fill rows/cols with text
    for (int row = 1; row < m_GC_4.GetRowCount(); row++) 
	{
        for (int col = 0; col < m_GC_4.GetColumnCount(); col++) 
		{ 
            GV_ITEM Item;
            Item.mask = GVIF_TEXT|GVIF_FORMAT|GVIF_BKCLR;
            Item.row = row;
            Item.col = col;
			Item.nFormat = dwTextStyle;
			Item.crBkClr = RGB(0xFF, 0xFF, 0xE0);

			if(col == 0)
			{
				Item.nFormat = dwTextStyle1;
				Item.crBkClr = CR_SLATEGRAY;
				Item.strText.Format(_T("Y%X"), IOData2.m_nAddress + row - 1);
			}
			else if(col == 1)
			{
				Item.strText.Format(_T("%s"), IOData2.m_strText[row-1]);
			}
			else if(col == 2)
			{
				Item.strText.Format(_T(""));
			}

			m_GC_4.SetItemFgColour(row, col, RGB(0,0,0));
            m_GC_4.SetItem(&Item);  
        }
    }
	m_GC_4.Invalidate();
}

void CDlg_IO::Update_PLC_Value()
{
	DWORD dwTextStyle = DT_LEFT|DT_VCENTER|DT_SINGLELINE;
	UINT uiMask = GVIF_TEXT|GVIF_FORMAT|GVIF_BKCLR;
	int nStartAddress;
	int nSize;
	
	nSize = (int)CIOListSetting::Instance()->m_stIOInfo.m_vecInput.size();

	if(nSize > m_nCurInputView*2 + 0)
	{
		stIOData IOData1 = CIOListSetting::Instance()->m_stIOInfo.m_vecInput[m_nCurInputView*2 + 0];
		nStartAddress = IOData1.m_nAddress;
		// fill rows/cols with text
		for (int row = 1; row < m_GC_1.GetRowCount(); row++) 
		{
			GV_ITEM Item;
			Item.mask = uiMask;
			Item.row = row;
			Item.col = 2;
			Item.nFormat = dwTextStyle;
			Item.crBkClr = RGB(0xFF, 0xFF, 0xE0);

			if(CDataManager::Instance()->m_PLCReaderX.IsONHex(nStartAddress + row - 1) == TRUE)
			{
				Item.crBkClr = CR_BLUE;
			}

			m_GC_1.SetItemFgColour(row, 2, RGB(0,0,0));
			m_GC_1.SetItem(&Item);  
		}
		m_GC_1.Invalidate();
	}
	if(nSize > m_nCurInputView*2 + 1)
	{
		stIOData IOData2 = CIOListSetting::Instance()->m_stIOInfo.m_vecInput[m_nCurInputView*2 + 1];
		nStartAddress = IOData2.m_nAddress;
		// fill rows/cols with text
		for (int row = 1; row < m_GC_2.GetRowCount(); row++) 
		{
			GV_ITEM Item;
			Item.mask = uiMask;
			Item.row = row;
			Item.col = 2;
			Item.nFormat = dwTextStyle;
			Item.crBkClr = RGB(0xFF, 0xFF, 0xE0);

			if(CDataManager::Instance()->m_PLCReaderX.IsONHex(nStartAddress + row - 1) == TRUE)
			{
				Item.crBkClr = CR_BLUE;
			}

			m_GC_2.SetItemFgColour(row, 2, RGB(0,0,0));
			m_GC_2.SetItem(&Item);  
		}
		m_GC_2.Invalidate();
	}

	nSize = (int)CIOListSetting::Instance()->m_stIOInfo.m_vecOutput.size();

	if(nSize > m_nCurOutputView*2 + 0)
	{
		stIOData IOData3 = CIOListSetting::Instance()->m_stIOInfo.m_vecOutput[m_nCurOutputView*2 + 0];
		nStartAddress = IOData3.m_nAddress;
		// fill rows/cols with text
		for (int row = 1; row < m_GC_3.GetRowCount(); row++) 
		{
			GV_ITEM Item;
			Item.mask = uiMask;
			Item.row = row;
			Item.col = 2;
			Item.nFormat = dwTextStyle;
			Item.crBkClr = RGB(0xFF, 0xFF, 0xE0);

			if(CDataManager::Instance()->m_PLCReaderY.IsONHex(nStartAddress + row - 1) == TRUE)
			{
				Item.crBkClr = CR_BLUE;
			}

			m_GC_3.SetItemFgColour(row, 2, RGB(0,0,0));
			m_GC_3.SetItem(&Item);  
		}
		m_GC_3.Invalidate();
	}
	if(nSize > m_nCurOutputView*2 + 1)
	{
		stIOData IOData4 = CIOListSetting::Instance()->m_stIOInfo.m_vecOutput[m_nCurOutputView*2 + 1];
		nStartAddress = IOData4.m_nAddress;
		// fill rows/cols with text
		for (int row = 1; row < m_GC_4.GetRowCount(); row++) 
		{
			GV_ITEM Item;
			Item.mask = uiMask;
			Item.row = row;
			Item.col = 2;
			Item.nFormat = dwTextStyle;
			Item.crBkClr = RGB(0xFF, 0xFF, 0xE0);

			if(CDataManager::Instance()->m_PLCReaderY.IsONHex(nStartAddress + row - 1) == TRUE)
			{
				Item.crBkClr = CR_BLUE;
			}

			m_GC_4.SetItemFgColour(row, 2, RGB(0,0,0));
			m_GC_4.SetItem(&Item);  
		}
		m_GC_4.Invalidate();
	}
}

void CDlg_IO::OnBnClickedBtnIo1()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_nCurInputView --;
	if(m_nCurInputView < 0) 
	{
		m_nCurInputView = 0;
		return;
	}

	Update_IOList_Input();

	CString strIndex;
	strIndex.Format(_T("%d"), m_nCurInputView*2 + 0);
	m_st_T1.SetWindowText(strIndex);
	strIndex.Format(_T("%d"), m_nCurInputView*2 + 1);
	m_st_T3.SetWindowText(strIndex);
}


void CDlg_IO::OnBnClickedBtnIo2()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_nCurInputView ++;
	int nMaxPage = (int)CIOListSetting::Instance()->m_stIOInfo.m_vecInput.size()/2;
	if((int)CIOListSetting::Instance()->m_stIOInfo.m_vecInput.size()%2 > 0) nMaxPage ++;
	if(m_nCurInputView > nMaxPage - 1)
	{
		m_nCurInputView = nMaxPage - 1;
		return;
	}

	Update_IOList_Input();

	CString strIndex;
	strIndex.Format(_T("%d"), m_nCurInputView*2 + 0);
	m_st_T1.SetWindowText(strIndex);
	strIndex.Format(_T("%d"), m_nCurInputView*2 + 1);
	m_st_T3.SetWindowText(strIndex);
}


void CDlg_IO::OnBnClickedBtnIo3()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_nCurOutputView --;
	if(m_nCurOutputView < 0) 
	{
		m_nCurOutputView = 0;
		return;
	}

	Update_IOList_Output();

	CString strIndex;
	strIndex.Format(_T("%d"), m_nCurOutputView*2 + 0);
	m_st_T5.SetWindowText(strIndex);
	strIndex.Format(_T("%d"), m_nCurOutputView*2 + 1);
	m_st_T7.SetWindowText(strIndex);
}


void CDlg_IO::OnBnClickedBtnIo4()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_nCurOutputView ++;
	int nMaxPage = (int)CIOListSetting::Instance()->m_stIOInfo.m_vecOutput.size()/2;
	if((int)CIOListSetting::Instance()->m_stIOInfo.m_vecOutput.size()%2 > 0) nMaxPage ++;
	if(m_nCurOutputView > nMaxPage - 1)
	{
		m_nCurOutputView = nMaxPage - 1;
		return;
	}

	Update_IOList_Output();

	CString strIndex;
	strIndex.Format(_T("%d"), m_nCurOutputView*2 + 0);
	m_st_T5.SetWindowText(strIndex);
	strIndex.Format(_T("%d"), m_nCurOutputView*2 + 1);
	m_st_T7.SetWindowText(strIndex);
}


void CDlg_IO::OnBnClickedBtnIo5()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_btn_5,EnableWindow(FALSE);
	for (int row = 1; row < m_GC_1.GetRowCount(); row++) 
	{
		CIOListSetting::Instance()->SetIOList_InputText_ByIndexBit(m_nCurInputView*2 + 0, row-1, m_GC_1.GetItemText(row,1));
		CIOListSetting::Instance()->SetIOList_InputText_ByIndexBit(m_nCurInputView*2 + 1, row-1, m_GC_2.GetItemText(row,1));
	}
	CIOListSetting::Instance()->SaveIOList_Input();
	m_btn_5,EnableWindow(TRUE);
}


void CDlg_IO::OnBnClickedBtnIo6()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_btn_6,EnableWindow(FALSE);
	for (int row = 1; row < m_GC_3.GetRowCount(); row++) 
	{
		CIOListSetting::Instance()->SetIOList_OutputText_ByIndexBit(m_nCurOutputView*2 + 0, row-1, m_GC_3.GetItemText(row,1));
		CIOListSetting::Instance()->SetIOList_OutputText_ByIndexBit(m_nCurOutputView*2 + 1, row-1, m_GC_4.GetItemText(row,1));
	}
	CIOListSetting::Instance()->SaveIOList_Output();
	m_btn_6,EnableWindow(TRUE);
}


void CDlg_IO::OnBnClickedBtnIo7()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CAMKOR_MMIDlg *)AfxGetMainWnd())->SendMessage(WM_CHANGESCREEN, 0, 0);
}
