#include "stdafx.h"
#include "AlarmListSetting.h"
#include "Log/CsvParser.h"

//////////////////////////////////////////////////////////////////////
// CAlarmListGC
class CAlarmListGC
{
public:
	CAlarmListGC() {}
	~CAlarmListGC() { delete CAlarmListSetting::m_pThis; CAlarmListSetting::m_pThis = NULL; }
};
CAlarmListGC s_GC;

//////////////////////////////////////////////////////////////////////////
// CAlarmList
CAlarmListSetting* CAlarmListSetting::m_pThis = NULL;
CAlarmListSetting::CAlarmListSetting()
{
}

CAlarmListSetting::~CAlarmListSetting()
{
	m_mapAlarmList.clear();
	m_mapAlarmSolution.clear();
}

CAlarmListSetting* CAlarmListSetting::Instance()
{
	if( !m_pThis )
		m_pThis = new CAlarmListSetting;
	return m_pThis;
}

// UNICODE
CString CAlarmListSetting::GetExecuteDirectory()
{
	CString strFolderPath;

	// 현재 프로그램의 실행 파일이 있는 폴더명을 추출함
	::GetModuleFileName(NULL, strFolderPath.GetBuffer(MAX_PATH), MAX_PATH);
	strFolderPath.ReleaseBuffer();
	if (strFolderPath.Find('\\') != -1)
	{
		for (int i = strFolderPath.GetLength() - 1; i >= 0; i--) 
		{
			TCHAR ch = strFolderPath[i];
			strFolderPath.Delete(i);
			if (ch == '\\') break; 
		}
	}
	return strFolderPath;
}

CStringA CAlarmListSetting::GetExecuteDirectoryA()
{
	CStringA strFolderPath;

	// 현재 프로그램의 실행 파일이 있는 폴더명을 추출함
	::GetModuleFileNameA(NULL, strFolderPath.GetBuffer(MAX_PATH), MAX_PATH);
	strFolderPath.ReleaseBuffer();
	if (strFolderPath.Find('\\') != -1)
	{
		for (int i = strFolderPath.GetLength() - 1; i >= 0; i--) 
		{
			CHAR ch = strFolderPath[i];
			strFolderPath.Delete(i);
			if (ch == '\\') break; 
		}
	}
	return strFolderPath;
}

int CAlarmListSetting::LoadAlarmList()
{
	LoadAlarmList_CSV();

	return 0;
}

int CAlarmListSetting::LoadAlarmList_CSV()
{
	CStringA strPath = GetExecuteDirectoryA();
	CStringA strFilePath = "\\Setting\\Alarm\\ALARM.csv";
	CStringA strFullPath;
	strFullPath = strPath + strFilePath;

	m_mapAlarmList.clear();

	cCsvTable table;

    if (!table.Load(strFullPath))
        return -1;
    
    table.AddAlias("code", 0);
    table.AddAlias("text", 1);

    while (table.Next())
    {
		AlarmTable stTemp;

        stTemp.nCode = table.AsInt("code");
        std::string text = table.AsString("text");
		
		int nLen = MultiByteToWideChar(CP_ACP, 0, &text[0], text.size(), NULL, NULL);
		std::wstring wtext(nLen,0);
		MultiByteToWideChar(CP_ACP, 0, &text[0], text.size(), &wtext[0], nLen);

		stTemp.strText.Format(_T("%s"),wtext.c_str());

		m_mapAlarmList.insert(std::pair<int,AlarmTable>(stTemp.nCode,stTemp));
    }

	return 0;
}

int CAlarmListSetting::LoadAlarmSolution(int nCode)
{
	AlarmSolution stAlarmSolution;
		
	CString strTemp;
	strTemp.Format(_T("Alarm%05d.ini"), nCode);
	CString strPath = GetExecuteDirectory();
	strPath += _T("\\Setting\\Alarm\\");
	strPath += strTemp;

	TCHAR temp[MAX_PATH];

	::GetPrivateProfileString( _T("ALARM_CODE"), _T("CODE"), _T("0"), temp, MAX_PATH, strPath );
	stAlarmSolution.nCode = _tcstoul( temp, NULL, 10 );

	::GetPrivateProfileString( _T("ALARM_PROBLEM"), _T("TEXT01"), _T(""), temp, MAX_PATH, strPath );
	stAlarmSolution.strText_P1 = temp;
	::GetPrivateProfileString( _T("ALARM_PROBLEM"), _T("TEXT02"), _T(""), temp, MAX_PATH, strPath );
	stAlarmSolution.strText_P2 = temp;
	::GetPrivateProfileString( _T("ALARM_PROBLEM"), _T("TEXT03"), _T(""), temp, MAX_PATH, strPath );
	stAlarmSolution.strText_P3 = temp;
	::GetPrivateProfileString( _T("ALARM_SOLUTION"), _T("TEXT01"), _T(""), temp, MAX_PATH, strPath );
	stAlarmSolution.strText_S1 = temp;
	::GetPrivateProfileString( _T("ALARM_SOLUTION"), _T("TEXT02"), _T(""), temp, MAX_PATH, strPath );
	stAlarmSolution.strText_S2 = temp;
	::GetPrivateProfileString( _T("ALARM_SOLUTION"), _T("TEXT03"), _T(""), temp, MAX_PATH, strPath );
	stAlarmSolution.strText_S3 = temp;


	m_mapAlarmSolution.insert(std::pair<int,AlarmSolution>(stAlarmSolution.nCode,stAlarmSolution));

	return TRUE;
}

int CAlarmListSetting::SaveAlarmSolution(AlarmSolution stAlarmSolution)
{
	CString strTemp;
	strTemp.Format(_T("Alarm%05d.ini"), stAlarmSolution.nCode);
	CString strPath = GetExecuteDirectory();
	strPath += _T("\\Setting\\Alarm\\");
	strPath += strTemp;

	strTemp.Format(_T("%d"), stAlarmSolution.nCode);
	::WritePrivateProfileString( _T("ALARM_CODE"), _T("CODE"), strTemp, strPath );

	::WritePrivateProfileString( _T("ALARM_PROBLEM"), _T("TEXT01"), stAlarmSolution.strText_P1, strPath );
	::WritePrivateProfileString( _T("ALARM_PROBLEM"), _T("TEXT02"), stAlarmSolution.strText_P2, strPath );
	::WritePrivateProfileString( _T("ALARM_PROBLEM"), _T("TEXT03"), stAlarmSolution.strText_P3, strPath );
	::WritePrivateProfileString( _T("ALARM_SOLUTION"), _T("TEXT01"), stAlarmSolution.strText_S1, strPath );
	::WritePrivateProfileString( _T("ALARM_SOLUTION"), _T("TEXT02"), stAlarmSolution.strText_S2, strPath );
	::WritePrivateProfileString( _T("ALARM_SOLUTION"), _T("TEXT03"), stAlarmSolution.strText_S3, strPath );

	stdext::hash_map<int,AlarmSolution>::iterator iter;

	iter = m_mapAlarmSolution.find(stAlarmSolution.nCode);
	if(iter == m_mapAlarmSolution.end()) // 해당 데이터가 없다면 추가한다..
	{
		m_mapAlarmSolution.insert(std::pair<int,AlarmSolution>(stAlarmSolution.nCode,stAlarmSolution));
	}

	return TRUE;
}

void CAlarmListSetting::GetErrorTextByCode(int nCode, CString& strErrorText)
{
	std::map<int,AlarmTable>::iterator iter;

	iter = m_mapAlarmList.find(nCode);
	if(iter == m_mapAlarmList.end()) // 해당 데이터가 없다면...
	{
		// Error Code 알려줘야함
		strErrorText = CString(_T("해당 코드의 에러내용이 없습니다..."));
		return;
	}

	strErrorText = iter->second.strText;
}

void CAlarmListSetting::GetAlarmTableByCode(int nCode, AlarmTable& stAlarmTable)
{
	std::map<int,AlarmTable>::iterator iter;

	iter = m_mapAlarmList.find(nCode);
	if(iter == m_mapAlarmList.end()) // 해당 데이터가 없다면...
	{
		stAlarmTable.nCode = 0; // 에러 코드없음
		stAlarmTable.strText = CString(_T("해당 코드의 에러내용이 없습니다..."));
	}
	else
	{
		stAlarmTable = iter->second;
	}
}

void CAlarmListSetting::GetAlarmSolutionByCode(int nCode, AlarmSolution& stAlarmSolution)
{
	stdext::hash_map<int,AlarmSolution>::iterator iter;

	iter = m_mapAlarmSolution.find(nCode);
	if(iter == m_mapAlarmSolution.end()) // 해당 데이터가 없다면...
	{
		stAlarmSolution.nCode = nCode; // 데이터가 없으므로 에러코드만 받아서 넘김.
		stAlarmSolution.strText_P1.Empty();
		stAlarmSolution.strText_P2.Empty();
		stAlarmSolution.strText_P3.Empty();
		stAlarmSolution.strText_S1.Empty();
		stAlarmSolution.strText_S2.Empty();
		stAlarmSolution.strText_S3.Empty();
	}
	else
	{
		stAlarmSolution = iter->second;
	}
}
