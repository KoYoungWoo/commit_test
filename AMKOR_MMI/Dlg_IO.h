#pragma once
#include "afxwin.h"
#include "UIExt/CxStatic.h"
#include "UIExt/IconButton.h"
#include "UIExt/XPGroupBox.h"
#include "GC/gridctrl.h"

// CDlg_IO 대화 상자입니다.

class CDlg_IO : public CDialogEx
{
	DECLARE_DYNAMIC(CDlg_IO)

public:
	CDlg_IO(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CDlg_IO();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DLG_IO };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);

	void Cal_Area();
	void Cal_CtrlArea();
	void Cal_CtrlArea_Left();
	void Cal_CtrlArea_Right();

	bool Init_GC_1(int nRows, int nCols);
	afx_msg void OnGridClick_GC_1(NMHDR *pNotifyStruct, LRESULT* pResult);
	afx_msg void OnEndLabel_GC_1(NMHDR *pNotifyStruct, LRESULT* pResult);
	bool Init_GC_2(int nRows, int nCols);
	afx_msg void OnGridClick_GC_2(NMHDR *pNotifyStruct, LRESULT* pResult);
	afx_msg void OnEndLabel_GC_2(NMHDR *pNotifyStruct, LRESULT* pResult);
	bool Init_GC_3(int nRows, int nCols);
	afx_msg void OnGridClick_GC_3(NMHDR *pNotifyStruct, LRESULT* pResult);
	afx_msg void OnEndLabel_GC_3(NMHDR *pNotifyStruct, LRESULT* pResult);
	bool Init_GC_4(int nRows, int nCols);
	afx_msg void OnGridClick_GC_4(NMHDR *pNotifyStruct, LRESULT* pResult);
	afx_msg void OnEndLabel_GC_4(NMHDR *pNotifyStruct, LRESULT* pResult);

	void Update_IOList_Input();
	void Update_IOList_Output();
	void Update_PLC_Value();

	CRect m_rcLeft;
	CRect m_rcRight;

	BOOL m_bCtrl_FirstLoad;
	int m_nCurInputView;
	int m_nCurOutputView;

	CXPGroupBox m_gb_1;
	CXPGroupBox m_gb_2;
	CxStatic m_st_T1;
	CxStatic m_st_T2;
	CxStatic m_st_T3;
	CxStatic m_st_T4;
	CxStatic m_st_T5;
	CxStatic m_st_T6;
	CxStatic m_st_T7;
	CxStatic m_st_T8;
	CGridCtrl m_GC_1;
	CGridCtrl m_GC_2;
	CGridCtrl m_GC_3;
	CGridCtrl m_GC_4;
	CIconButton m_btn_1;
	CIconButton m_btn_2;
	CIconButton m_btn_3;
	CIconButton m_btn_4;
	CIconButton m_btn_5;
	CIconButton m_btn_6;
	CIconButton m_btn_7;
	afx_msg void OnBnClickedBtnIo1();
	afx_msg void OnBnClickedBtnIo2();
	afx_msg void OnBnClickedBtnIo3();
	afx_msg void OnBnClickedBtnIo4();
	afx_msg void OnBnClickedBtnIo5();
	afx_msg void OnBnClickedBtnIo6();
	afx_msg void OnBnClickedBtnIo7();
};
