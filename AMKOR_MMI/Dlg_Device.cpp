// Dlg_Device.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "AMKOR_MMI.h"
#include "AMKOR_MMIDlg.h"
#include "Dlg_Device.h"
#include "Dlg_TrayInfo.h"
#include "afxdialogex.h"


// CDlg_Device 대화 상자입니다.

IMPLEMENT_DYNAMIC(CDlg_Device, CDialogEx)

CDlg_Device::CDlg_Device(CWnd* pParent /*=NULL*/)
	: CDialogEx(CDlg_Device::IDD, pParent)
{
	m_bCtrl_FirstLoad = FALSE;
	m_nSelDeviceIndex = 0;
	m_nCurPageNum = 0;
}

CDlg_Device::~CDlg_Device()
{
}

void CDlg_Device::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_GC_DV_1, m_GC_1);
	DDX_Control(pDX, IDC_GB_DV_1, m_gb_1);
	DDX_Control(pDX, IDC_BTN_DV_1, m_btn_1);
	DDX_Control(pDX, IDC_BTN_DV_2, m_btn_2);
	DDX_Control(pDX, IDC_BTN_DV_3, m_btn_3);
	DDX_Control(pDX, IDC_BTN_DV_4, m_btn_4);
	DDX_Control(pDX, IDC_BTN_DV_5, m_btn_5);
	DDX_Control(pDX, IDC_BTN_DV_6, m_btn_6);
	DDX_Control(pDX, IDC_BTN_DV_7, m_btn_7);
	DDX_Control(pDX, IDC_BTN_DV_8, m_btn_8);
	DDX_Control(pDX, IDC_BTN_DV_9, m_btn_9);
	DDX_Control(pDX, IDC_BTN_DV_V1, m_btn_V1);
	DDX_Control(pDX, IDC_GB_DV_2, m_gb_2);
	DDX_Control(pDX, IDC_BTN_DV_V2, m_btn_V2);
	DDX_Control(pDX, IDC_BTN_DV_V3, m_btn_V3);
	DDX_Control(pDX, IDC_BTN_DV_V4, m_btn_V4);
	DDX_Control(pDX, IDC_BTN_DV_V5, m_btn_V5);
	DDX_Control(pDX, IDC_BTN_DV_V6, m_btn_V6);
	DDX_Control(pDX, IDC_BTN_DV_V7, m_btn_V7);
	DDX_Control(pDX, IDC_BTN_DV_V8, m_btn_V8);
	DDX_Control(pDX, IDC_ST_DV_1, m_st_1);
	DDX_Control(pDX, IDC_ST_DV_2, m_st_2);
	DDX_Control(pDX, IDC_ST_DV_3, m_st_3);
	DDX_Control(pDX, IDC_ST_DV_4, m_st_4);
	DDX_Control(pDX, IDC_ST_DV_5, m_st_5);
	DDX_Control(pDX, IDC_ST_DV_6, m_st_6);
	DDX_Control(pDX, IDC_ST_DV_7, m_st_7);
	DDX_Control(pDX, IDC_EDIT_DV_1, m_edit_1);
	DDX_Control(pDX, IDC_EDIT_DV_2, m_edit_2);
	DDX_Control(pDX, IDC_EDIT_DV_3, m_edit_3);
	DDX_Control(pDX, IDC_EDIT_DV_4, m_edit_4);
	DDX_Control(pDX, IDC_BTN_DV_V9, m_btn_V9);
	DDX_Control(pDX, IDC_BTN_DV_V10, m_btn_V10);
	DDX_Control(pDX, IDC_BTN_DV_V11, m_btn_V11);
	DDX_Control(pDX, IDC_BTN_DV_V12, m_btn_V12);
	DDX_Control(pDX, IDC_BTN_DV_V13, m_btn_V13);
	DDX_Control(pDX, IDC_BTN_DV_V14, m_btn_V14);
	DDX_Control(pDX, IDC_ST_DV_8, m_st_8);
	DDX_Control(pDX, IDC_EDIT_DV_5, m_edit_5);
	DDX_Control(pDX, IDC_BTN_DV_V15, m_btn_V15);
}


BEGIN_MESSAGE_MAP(CDlg_Device, CDialogEx)
	ON_WM_ERASEBKGND()
	ON_WM_SIZE()
	ON_WM_TIMER()
	ON_NOTIFY(NM_CLICK, IDC_GC_DV_1, OnGridClick_GC_1)
	ON_BN_CLICKED(IDC_BTN_DV_1, &CDlg_Device::OnBnClickedBtnDv1)
	ON_BN_CLICKED(IDC_BTN_DV_2, &CDlg_Device::OnBnClickedBtnDv2)
	ON_BN_CLICKED(IDC_BTN_DV_3, &CDlg_Device::OnBnClickedBtnDv3)
	ON_BN_CLICKED(IDC_BTN_DV_4, &CDlg_Device::OnBnClickedBtnDv4)
	ON_BN_CLICKED(IDC_BTN_DV_5, &CDlg_Device::OnBnClickedBtnDv5)
	ON_BN_CLICKED(IDC_BTN_DV_6, &CDlg_Device::OnBnClickedBtnDv6)
	ON_BN_CLICKED(IDC_BTN_DV_7, &CDlg_Device::OnBnClickedBtnDv7)
	ON_BN_CLICKED(IDC_BTN_DV_8, &CDlg_Device::OnBnClickedBtnDv8)
	ON_BN_CLICKED(IDC_BTN_DV_9, &CDlg_Device::OnBnClickedBtnDv9)
	ON_BN_CLICKED(IDC_BTN_DV_V1, &CDlg_Device::OnBnClickedBtnDvV1)
	ON_BN_CLICKED(IDC_BTN_DV_V2, &CDlg_Device::OnBnClickedBtnDvV2)
	ON_BN_CLICKED(IDC_BTN_DV_V3, &CDlg_Device::OnBnClickedBtnDvV3)
	ON_BN_CLICKED(IDC_BTN_DV_V4, &CDlg_Device::OnBnClickedBtnDvV4)
	ON_BN_CLICKED(IDC_BTN_DV_V5, &CDlg_Device::OnBnClickedBtnDvV5)
	ON_BN_CLICKED(IDC_BTN_DV_V6, &CDlg_Device::OnBnClickedBtnDvV6)
	ON_BN_CLICKED(IDC_BTN_DV_V7, &CDlg_Device::OnBnClickedBtnDvV7)
	ON_BN_CLICKED(IDC_BTN_DV_V8, &CDlg_Device::OnBnClickedBtnDvV8)
	ON_BN_CLICKED(IDC_BTN_DV_V9, &CDlg_Device::OnBnClickedBtnDvV9)
	ON_BN_CLICKED(IDC_BTN_DV_V10, &CDlg_Device::OnBnClickedBtnDvV10)
	ON_BN_CLICKED(IDC_BTN_DV_V11, &CDlg_Device::OnBnClickedBtnDvV11)
	ON_BN_CLICKED(IDC_BTN_DV_V12, &CDlg_Device::OnBnClickedBtnDvV12)
	ON_BN_CLICKED(IDC_BTN_DV_V13, &CDlg_Device::OnBnClickedBtnDvV13)
	ON_BN_CLICKED(IDC_BTN_DV_V14, &CDlg_Device::OnBnClickedBtnDvV14)
	ON_BN_CLICKED(IDC_BTN_DV_V15, &CDlg_Device::OnBnClickedBtnDvV15)
END_MESSAGE_MAP()


// CDlg_Device 메시지 처리기입니다.


BOOL CDlg_Device::OnEraseBkgnd(CDC* pDC)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	// 전체 배경색
	CRect rect;
    GetClientRect(&rect);
    CBrush myBrush(CR_MAIN_BKGND);	// dialog background color
    CBrush *pOld = pDC->SelectObject(&myBrush);
    BOOL bRes  = pDC->PatBlt(0, 0, rect.Width(), rect.Height(), PATCOPY);
    pDC->SelectObject(pOld);		// restore old brush
    return FALSE;     
	//return CDialogEx::OnEraseBkgnd(pDC);
}


void CDlg_Device::OnSize(UINT nType, int cx, int cy)
{
	CDialogEx::OnSize(nType, cx, cy);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	if(m_bCtrl_FirstLoad == TRUE) Cal_CtrlArea();
}


void CDlg_Device::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CDialogEx::OnTimer(nIDEvent);
}


BOOL CDlg_Device::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	Cal_CtrlArea();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


BOOL CDlg_Device::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message == WM_SYSKEYDOWN)
	{
		if(pMsg->wParam == VK_F4) 
			return TRUE;
	}

	if(pMsg->message == WM_KEYDOWN)
	{
		if( pMsg->wParam == VK_ESCAPE ||	
			pMsg->wParam == VK_RETURN ||	
			pMsg->wParam == VK_SPACE  ||
			pMsg->wParam == VK_CANCEL )     
			return TRUE;        
	}

	return CDialogEx::PreTranslateMessage(pMsg);
}

void CDlg_Device::Cal_Area()
{
	CRect ClientRect;
	GetClientRect(&ClientRect);

	int n84pW = (int)((float)ClientRect.Width()*0.84f);

	m_rcLeft.SetRect(ClientRect.left, ClientRect.top, ClientRect.left+n84pW, ClientRect.bottom);
	m_rcRight.SetRect(ClientRect.left+n84pW+CTRL_MARGIN4, ClientRect.top, ClientRect.right, ClientRect.bottom);
}

void CDlg_Device::Cal_CtrlArea()
{
	Cal_Area();

	Cal_CtrlArea_Left();
	Cal_CtrlArea_Right();

	m_bCtrl_FirstLoad = TRUE;
}

void CDlg_Device::Cal_CtrlArea_Left()
{
	CRect CalRect;
	CRect rcBottom, rcGB, rcGBInside;
	int nBtnH = 60;
	int nBtnW = 60;

	rcBottom.SetRect(m_rcLeft.left, m_rcLeft.bottom - nBtnH, m_rcLeft.right, m_rcLeft.bottom);
	rcGB.SetRect(m_rcLeft.left, m_rcLeft.top, m_rcLeft.right, m_rcLeft.bottom - nBtnH);
	rcGBInside = rcGB;
	rcGBInside.DeflateRect(6,28,6,6);

	m_gb_1.SetXPGroupStyle(CXPGroupBox::XPGB_WINDOW_2);
	m_gb_1.SetBackgroundColor(CR_DARKGRAY, CR_GB_BKGND);
	m_gb_1.SetBorderColor(CR_BLACK);
	m_gb_1.SetCatptionTextColor(CR_BLACK);
	m_gb_1.MoveWindow(&rcGB);
	m_GC_1.MoveWindow(&rcGBInside);
	Init_GC_1(21,21);
	SetDeviceListSettingValue();

	CalRect.SetRect(rcBottom.left, rcBottom.top, rcBottom.left+nBtnW, rcBottom.bottom);
	m_btn_1.SetFontBtn(CTRL_FONT, 20, FW_BOLD);
	m_btn_1.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_1.SetTextColor(CR_BLACK);
	m_btn_1.SetIconPos(0);
	m_btn_1.SetIconID(IDI_PREV);
	m_btn_1.SetIconSize(48,48);
	m_btn_1.SetFlatType(TRUE);
	m_btn_1.MoveWindow(&CalRect);

	CalRect.SetRect(CalRect.right, rcBottom.top, CalRect.right+nBtnW, rcBottom.bottom);
	m_btn_2.SetFontBtn(CTRL_FONT, 20, FW_BOLD);
	m_btn_2.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_2.SetTextColor(CR_BLACK);
	m_btn_2.SetIconPos(0);
	m_btn_2.SetIconID(IDI_NEXT);
	m_btn_2.SetIconSize(48,48);
	m_btn_2.SetFlatType(TRUE);
	m_btn_2.MoveWindow(&CalRect);

	CalRect.SetRect(CalRect.right, rcBottom.top, CalRect.right+nBtnW, rcBottom.bottom);
	m_btn_3.SetFontBtn(CTRL_FONT, 20, FW_BOLD);
	m_btn_3.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_3.SetTextColor(CR_BLACK);
	m_btn_3.SetIconPos(0);
	m_btn_3.SetIconID(IDI_PLUS);
	m_btn_3.SetIconSize(48,48);
	m_btn_3.SetFlatType(TRUE);
	m_btn_3.MoveWindow(&CalRect);

	CalRect.SetRect(CalRect.right, rcBottom.top, CalRect.right+nBtnW, rcBottom.bottom);
	m_btn_4.SetFontBtn(CTRL_FONT, 20, FW_BOLD);
	m_btn_4.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_4.SetTextColor(CR_BLACK);
	m_btn_4.SetIconPos(0);
	m_btn_4.SetIconID(IDI_MINUS);
	m_btn_4.SetIconSize(48,48);
	m_btn_4.SetFlatType(TRUE);
	m_btn_4.MoveWindow(&CalRect);

	CalRect.SetRect(rcBottom.right-nBtnW, rcBottom.top, rcBottom.right, rcBottom.bottom);
	m_btn_6.SetFontBtn(CTRL_FONT, 20, FW_BOLD);
	m_btn_6.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_6.SetTextColor(CR_BLACK);
	m_btn_6.SetIconPos(0);
	m_btn_6.SetIconID(IDI_UPLOAD);
	m_btn_6.SetIconSize(48,48);
	m_btn_6.SetFlatType(TRUE);
	m_btn_6.MoveWindow(&CalRect);

	CalRect.SetRect(CalRect.left-nBtnW, rcBottom.top, CalRect.left, rcBottom.bottom);
	m_btn_5.SetFontBtn(CTRL_FONT, 20, FW_BOLD);
	m_btn_5.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_5.SetTextColor(CR_BLACK);
	m_btn_5.SetIconPos(0);
	m_btn_5.SetIconID(IDI_DOWNLOAD);
	m_btn_5.SetIconSize(48,48);
	m_btn_5.SetFlatType(TRUE);
	m_btn_5.MoveWindow(&CalRect);
}

void CDlg_Device::Cal_CtrlArea_Right()
{
	CRect CalRect;
	CRect rcBottom, rcGB, rcGBInside;
	CRect rcDiv[16];
	int nBtnH = 60;
	int nBtnW = 120;

	rcBottom.SetRect(m_rcRight.left, m_rcRight.bottom - nBtnH, m_rcRight.right, m_rcRight.bottom);
	rcGB.SetRect(m_rcRight.left, m_rcRight.top, m_rcRight.right, m_rcRight.bottom - nBtnH);
	rcGBInside = rcGB;
	rcGBInside.DeflateRect(6,28,6,6);
	int n11DivH = (int)((float)rcGBInside.Height()/11.0f);
	int n4DivW = (int)((float)rcGBInside.Width()/4.0f);
	int n50pW = (int)((float)rcGBInside.Width()/2.0f);
	rcDiv[0].SetRect(rcGBInside.left, rcGBInside.top, rcGBInside.right, rcGBInside.top+32);
	rcDiv[1].SetRect(rcGBInside.left, rcDiv[0].bottom, rcGBInside.right, rcDiv[0].bottom+32);
	rcDiv[2].SetRect(rcGBInside.left, rcDiv[1].bottom, rcGBInside.right, rcDiv[1].bottom+32);
	rcDiv[3].SetRect(rcGBInside.left, rcDiv[2].bottom, rcGBInside.right, rcDiv[2].bottom+32);
	rcDiv[4].SetRect(rcGBInside.left, rcDiv[3].bottom, rcGBInside.right, rcDiv[3].bottom+36);
	rcDiv[5].SetRect(rcGBInside.left, rcDiv[4].bottom, rcGBInside.right, rcDiv[4].bottom+36);
	rcDiv[6].SetRect(rcGBInside.left, rcDiv[5].bottom, rcGBInside.right, rcDiv[5].bottom+36);
	rcDiv[7].SetRect(rcGBInside.left, rcDiv[6].bottom, rcGBInside.right, rcDiv[6].bottom+36);
	rcDiv[8].SetRect(rcGBInside.left, rcDiv[7].bottom, rcGBInside.right, rcDiv[7].bottom+55);
	rcDiv[9].SetRect(rcGBInside.left, rcDiv[8].bottom, rcGBInside.right, rcDiv[8].bottom+55);
	rcDiv[10].SetRect(rcGBInside.left, rcDiv[9].bottom, rcGBInside.right, rcDiv[9].bottom+55);
	rcDiv[11].SetRect(rcGBInside.left, rcDiv[10].bottom, rcGBInside.right, rcDiv[10].bottom+55);
	rcDiv[12].SetRect(rcGBInside.left, rcDiv[11].bottom, rcGBInside.right, rcDiv[11].bottom+55);
	rcDiv[13].SetRect(rcGBInside.left, rcDiv[12].bottom, rcGBInside.right, rcDiv[12].bottom+55);
	rcDiv[14].SetRect(rcGBInside.left, rcDiv[13].bottom, rcGBInside.right, rcDiv[13].bottom+55);
	rcDiv[15].SetRect(rcGBInside.left, rcGBInside.bottom-60, rcGBInside.right, rcGBInside.bottom);

	m_gb_2.SetXPGroupStyle(CXPGroupBox::XPGB_WINDOW_2);
	m_gb_2.SetBackgroundColor(CR_DARKGRAY, CR_GB_BKGND);
	m_gb_2.SetBorderColor(CR_BLACK);
	m_gb_2.SetCatptionTextColor(CR_BLACK);
	m_gb_2.MoveWindow(&rcGB);

	m_st_1.SetBkColor(CR_DARKSLATEGRAY);
	m_st_1.SetTextColor(CR_WHITE);
	m_st_1.SetFont(CTRL_FONT,20,FW_BOLD);
	m_st_1.MoveWindow(&rcDiv[0]);

	m_edit_1.SetFontStyle(CTRL_FONT, 20, FW_BOLD);
	m_edit_1.SetColor(CR_BLACK, CR_WHITE);
	m_edit_1.SetNumeric(FALSE);
	m_edit_1.MoveWindow(&rcDiv[1]);

	m_st_8.SetBkColor(CR_DARKSLATEGRAY);
	m_st_8.SetTextColor(CR_WHITE);
	m_st_8.SetFont(CTRL_FONT,20,FW_BOLD);
	m_st_8.MoveWindow(&rcDiv[2]);

	m_edit_5.SetFontStyle(CTRL_FONT, 20, FW_BOLD);
	m_edit_5.SetColor(CR_BLACK, CR_WHITE);
	m_edit_5.SetNumeric(FALSE);
	m_edit_5.MoveWindow(&rcDiv[3]);

	CalRect.SetRect(rcDiv[4].left, rcDiv[4].top, rcDiv[4].right-60, rcDiv[4].bottom);
	m_st_2.SetBkColor(CR_DARKSLATEGRAY);
	m_st_2.SetTextColor(CR_WHITE);
	m_st_2.SetFont(CTRL_FONT,20,FW_BOLD);
	m_st_2.MoveWindow(&CalRect);

	CalRect.SetRect(rcDiv[4].right-60, rcDiv[4].top, rcDiv[4].right, rcDiv[4].bottom);
	m_btn_V1.SetFontBtn(CTRL_FONT, 20, FW_BOLD);
	m_btn_V1.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_V1.SetTextColor(CR_BLACK);
	m_btn_V1.SetFlatType(TRUE);
	m_btn_V1.MoveWindow(&CalRect);

	CalRect.SetRect(rcDiv[5].left, rcDiv[5].top, rcDiv[5].left+60, rcDiv[5].bottom);
	m_st_3.SetBkColor(CR_WHITE);
	m_st_3.SetTextColor(CR_BLACK);
	m_st_3.SetFont(CTRL_FONT,18,FW_BOLD);
	m_st_3.MoveWindow(&CalRect);

	CalRect.SetRect(rcDiv[5].left+60, rcDiv[5].top, rcDiv[5].right, rcDiv[5].bottom);
	m_st_4.SetBkColor(CR_WHITE);
	m_st_4.SetTextColor(CR_BLACK);
	m_st_4.SetFont(CTRL_FONT,18,FW_BOLD);
	m_st_4.MoveWindow(&CalRect);

	CalRect.SetRect(rcDiv[6].left, rcDiv[6].top, rcDiv[6].left+150, rcDiv[6].bottom);
	m_st_5.SetBkColor(CR_DARKSLATEGRAY);
	m_st_5.SetTextColor(CR_WHITE);
	m_st_5.SetFont(CTRL_FONT,16,FW_BOLD);
	m_st_5.MoveWindow(&CalRect);

	CalRect.SetRect(rcDiv[6].left+150, rcDiv[6].top, rcDiv[6].right, rcDiv[6].bottom);
	m_edit_2.SetFontStyle(CTRL_FONT, 20, FW_BOLD);
	m_edit_2.SetColor(CR_BLACK, CR_WHITE);
	m_edit_2.SetNumeric(FALSE);
	m_edit_2.MoveWindow(&CalRect);

	CalRect.SetRect(rcDiv[7].left, rcDiv[7].top, rcDiv[7].left+n4DivW, rcDiv[7].bottom);
	m_st_6.SetBkColor(CR_DARKSLATEGRAY);
	m_st_6.SetTextColor(CR_WHITE);
	m_st_6.SetFont(CTRL_FONT,16,FW_BOLD);
	m_st_6.MoveWindow(&CalRect);

	CalRect.SetRect(CalRect.right, rcDiv[7].top, CalRect.right+n4DivW, rcDiv[7].bottom);
	m_edit_3.SetFontStyle(CTRL_FONT, 20, FW_BOLD);
	m_edit_3.SetColor(CR_BLACK, CR_WHITE);
	m_edit_3.SetNumeric(FALSE);
	m_edit_3.MoveWindow(&CalRect);

	CalRect.SetRect(CalRect.right, rcDiv[7].top, CalRect.right+n4DivW, rcDiv[7].bottom);
	m_st_7.SetBkColor(CR_DARKSLATEGRAY);
	m_st_7.SetTextColor(CR_WHITE);
	m_st_7.SetFont(CTRL_FONT,16,FW_BOLD);
	m_st_7.MoveWindow(&CalRect);

	CalRect.SetRect(CalRect.right, rcDiv[7].top, rcDiv[7].right, rcDiv[7].bottom);
	m_edit_4.SetFontStyle(CTRL_FONT, 20, FW_BOLD);
	m_edit_4.SetColor(CR_BLACK, CR_WHITE);
	m_edit_4.SetNumeric(FALSE);
	m_edit_4.MoveWindow(&CalRect);

	CalRect.SetRect(rcDiv[8].left, rcDiv[8].top, rcDiv[8].left+n50pW, rcDiv[8].bottom);
	m_btn_V2.SetFontBtn(CTRL_FONT, 20, FW_BOLD);
	m_btn_V2.SetButtonColor(CR_LAVENDER, CR_LIGHTORANGE);
	m_btn_V2.SetTextColor(CR_BLACK);
	m_btn_V2.SetFlatType(TRUE);
	m_btn_V2.SetOnOffStyle();
	m_btn_V2.SetOnOff(FALSE);
	m_btn_V2.MoveWindow(&CalRect);

	CalRect.SetRect(rcDiv[8].left+n50pW, rcDiv[8].top, rcDiv[8].right, rcDiv[8].bottom);
	m_btn_V3.SetFontBtn(CTRL_FONT, 18, FW_BOLD);
	m_btn_V3.SetButtonColor(CR_LAVENDER, CR_LIGHTORANGE);
	m_btn_V3.SetTextColor(CR_BLACK);
	m_btn_V3.SetFlatType(TRUE);
	m_btn_V3.SetOnOffStyle();
	m_btn_V3.SetOnOff(FALSE);
	m_btn_V3.MoveWindow(&CalRect);

	CalRect.SetRect(rcDiv[9].left, rcDiv[9].top, rcDiv[9].left+n50pW, rcDiv[9].bottom);
	m_btn_V4.SetFontBtn(CTRL_FONT, 18, FW_BOLD);
	m_btn_V4.SetButtonColor(CR_LAVENDER, CR_LIGHTORANGE);
	m_btn_V4.SetTextColor(CR_BLACK);
	m_btn_V4.SetFlatType(TRUE);
	m_btn_V4.SetOnOffStyle();
	m_btn_V4.SetOnOff(FALSE);
	m_btn_V4.MoveWindow(&CalRect);

	CalRect.SetRect(rcDiv[9].left+n50pW, rcDiv[9].top, rcDiv[9].right, rcDiv[9].bottom);
	m_btn_V5.SetFontBtn(CTRL_FONT, 18, FW_BOLD);
	m_btn_V5.SetButtonColor(CR_LAVENDER, CR_LIGHTORANGE);
	m_btn_V5.SetTextColor(CR_BLACK);
	m_btn_V5.SetFlatType(TRUE);
	m_btn_V5.SetOnOffStyle();
	m_btn_V5.SetOnOff(FALSE);
	m_btn_V5.MoveWindow(&CalRect);

	CalRect.SetRect(rcDiv[10].left, rcDiv[10].top, rcDiv[10].left+n50pW, rcDiv[10].bottom);
	m_btn_V6.SetFontBtn(CTRL_FONT, 18, FW_BOLD);
	m_btn_V6.SetButtonColor(CR_LAVENDER, CR_LIGHTORANGE);
	m_btn_V6.SetTextColor(CR_BLACK);
	m_btn_V6.SetFlatType(TRUE);
	m_btn_V6.SetOnOffStyle();
	m_btn_V6.SetOnOff(FALSE);
	m_btn_V6.MoveWindow(&CalRect);

	CalRect.SetRect(rcDiv[10].left+n50pW, rcDiv[10].top, rcDiv[10].right, rcDiv[10].bottom);
	m_btn_V7.SetFontBtn(CTRL_FONT, 18, FW_BOLD);
	m_btn_V7.SetButtonColor(CR_LAVENDER, CR_LIGHTORANGE);
	m_btn_V7.SetTextColor(CR_BLACK);
	m_btn_V7.SetFlatType(TRUE);
	m_btn_V7.SetOnOffStyle();
	m_btn_V7.SetOnOff(FALSE);
	m_btn_V7.MoveWindow(&CalRect);

	CalRect.SetRect(rcDiv[11].left, rcDiv[11].top, rcDiv[11].left+n50pW, rcDiv[11].bottom);
	m_btn_V8.SetFontBtn(CTRL_FONT, 18, FW_BOLD);
	m_btn_V8.SetButtonColor(CR_LAVENDER, CR_LIGHTORANGE);
	m_btn_V8.SetTextColor(CR_BLACK);
	m_btn_V8.SetFlatType(TRUE);
	m_btn_V8.SetOnOffStyle();
	m_btn_V8.SetOnOff(FALSE);
	m_btn_V8.MoveWindow(&CalRect);

	CalRect.SetRect(rcDiv[11].left+n50pW, rcDiv[11].top, rcDiv[11].right, rcDiv[11].bottom);
	m_btn_V9.SetFontBtn(CTRL_FONT, 18, FW_BOLD);
	m_btn_V9.SetButtonColor(CR_LAVENDER, CR_LIGHTORANGE);
	m_btn_V9.SetTextColor(CR_BLACK);
	m_btn_V9.SetFlatType(TRUE);
	m_btn_V9.SetOnOffStyle();
	m_btn_V9.SetOnOff(FALSE);
	m_btn_V9.MoveWindow(&CalRect);

	CalRect.SetRect(rcDiv[12].left, rcDiv[12].top, rcDiv[12].left+n50pW, rcDiv[12].bottom);
	m_btn_V10.SetFontBtn(CTRL_FONT, 18, FW_BOLD);
	m_btn_V10.SetButtonColor(CR_LAVENDER, CR_LIGHTORANGE);
	m_btn_V10.SetTextColor(CR_BLACK);
	m_btn_V10.SetFlatType(TRUE);
	m_btn_V10.SetOnOffStyle();
	m_btn_V10.SetOnOff(FALSE);
	m_btn_V10.MoveWindow(&CalRect);

	CalRect.SetRect(rcDiv[12].left+n50pW, rcDiv[12].top, rcDiv[12].right, rcDiv[12].bottom);
	m_btn_V11.SetFontBtn(CTRL_FONT, 18, FW_BOLD);
	m_btn_V11.SetButtonColor(CR_LAVENDER, CR_LIGHTORANGE);
	m_btn_V11.SetTextColor(CR_BLACK);
	m_btn_V11.SetFlatType(TRUE);
	m_btn_V11.SetOnOffStyle();
	m_btn_V11.SetOnOff(FALSE);
	m_btn_V11.MoveWindow(&CalRect);

	CalRect.SetRect(rcDiv[13].left, rcDiv[13].top, rcDiv[13].left+n50pW, rcDiv[13].bottom);
	m_btn_V12.SetFontBtn(CTRL_FONT, 18, FW_BOLD);
	m_btn_V12.SetButtonColor(CR_LAVENDER, CR_LIGHTORANGE);
	m_btn_V12.SetTextColor(CR_BLACK);
	m_btn_V12.SetFlatType(TRUE);
	m_btn_V12.SetOnOffStyle();
	m_btn_V12.SetOnOff(FALSE);
	m_btn_V12.MoveWindow(&CalRect);

	CalRect.SetRect(rcDiv[13].left+n50pW, rcDiv[13].top, rcDiv[13].right, rcDiv[13].bottom);
	m_btn_V13.SetFontBtn(CTRL_FONT, 18, FW_BOLD);
	m_btn_V13.SetButtonColor(CR_LAVENDER, CR_LIGHTORANGE);
	m_btn_V13.SetTextColor(CR_BLACK);
	m_btn_V13.SetFlatType(TRUE);
	m_btn_V13.SetOnOffStyle();
	m_btn_V13.SetOnOff(FALSE);
	m_btn_V13.MoveWindow(&CalRect);

	CalRect.SetRect(rcDiv[14].left, rcDiv[14].top, rcDiv[14].left+n50pW, rcDiv[14].bottom);
	m_btn_V14.SetFontBtn(CTRL_FONT, 18, FW_BOLD);
	m_btn_V14.SetButtonColor(CR_LAVENDER, CR_LIGHTORANGE);
	m_btn_V14.SetTextColor(CR_BLACK);
	m_btn_V14.SetFlatType(TRUE);
	m_btn_V14.SetOnOffStyle();
	m_btn_V14.SetOnOff(FALSE);
	m_btn_V14.MoveWindow(&CalRect);

	CalRect.SetRect(rcDiv[14].left+n50pW, rcDiv[14].top, rcDiv[14].right, rcDiv[14].bottom);
	m_btn_V15.SetFontBtn(CTRL_FONT, 18, FW_BOLD);
	m_btn_V15.SetButtonColor(CR_LAVENDER, CR_LIGHTORANGE);
	m_btn_V15.SetTextColor(CR_BLACK);
	m_btn_V15.SetFlatType(TRUE);
	m_btn_V15.SetOnOffStyle();
	m_btn_V15.SetOnOff(FALSE);
	m_btn_V15.MoveWindow(&CalRect);

	m_btn_7.SetFontBtn(CTRL_FONT, 20, FW_BOLD);
	m_btn_7.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_7.SetTextColor(CR_BLACK);
	m_btn_7.SetIconPos(0);
	m_btn_7.SetIconID(IDI_MODIFY);
	m_btn_7.SetIconSize(48,48);
	m_btn_7.SetFlatType(TRUE);
	m_btn_7.MoveWindow(&rcDiv[15]);

	CalRect.SetRect(rcBottom.left, rcBottom.top, rcBottom.left+60, rcBottom.bottom);
	m_btn_8.SetFontBtn(CTRL_FONT, 20, FW_BOLD);
	m_btn_8.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_8.SetTextColor(CR_BLACK);
	m_btn_8.SetIconPos(0);
	m_btn_8.SetIconID(IDI_SAVE);
	m_btn_8.SetIconSize(48,48);
	m_btn_8.SetFlatType(TRUE);
	m_btn_8.MoveWindow(&CalRect);

	CalRect.SetRect(rcBottom.right-nBtnW, rcBottom.top, rcBottom.right, rcBottom.bottom);
	m_btn_9.SetFontBtn(CTRL_FONT, 20, FW_BOLD);
	m_btn_9.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_9.SetTextColor(CR_BLACK);
	m_btn_9.SetIconPos(0);
	m_btn_9.SetIconID(IDI_CLOSE);
	m_btn_9.SetIconSize(48,48);
	m_btn_9.SetFlatType(TRUE);
	m_btn_9.MoveWindow(&CalRect);
}

bool CDlg_Device::Init_GC_1(int nRows, int nCols)
{
	BOOL m_bEditable = FALSE;
    BOOL m_bListMode = TRUE;
    int m_nRows = nRows;
    int m_nCols = nCols;
    int m_nFixRows = 1;
    int m_nFixCols = 1;

    m_GC_1.SetEditable(m_bEditable);
    m_GC_1.SetListMode(m_bListMode);
    m_GC_1.EnableDragAndDrop(FALSE);
    m_GC_1.SetTextBkColor(RGB(0xFF, 0xFF, 0xE0));

    m_GC_1.SetRowCount(m_nRows);
    m_GC_1.SetColumnCount(m_nCols);
    m_GC_1.SetFixedRowCount(m_nFixRows);
    m_GC_1.SetFixedColumnCount(m_nFixCols);

	m_GC_1.EnableSelection(false);
	m_GC_1.SetSingleColSelection(true);
	m_GC_1.SetSingleRowSelection(true);
	m_GC_1.SetFixedColumnSelection(false);
    m_GC_1.SetFixedRowSelection(false);

	m_GC_1.SetRowResize(false);
	m_GC_1.SetColumnResize(false);

	CRect ClientRect;
	GetDlgItem(IDC_GC_DV_1)->GetWindowRect(&ClientRect);
	if(ClientRect.Width() < 4) return false;
	if(ClientRect.Height() < 4) return false;
	int nClientWidth = ClientRect.Width()-4;
	int nClientHeight = ClientRect.Height()-4;
	int n80pW = (int)(nClientWidth*0.8f);
	int nDiv19W = (int)((float)n80pW/19.0f);
	int n10pW = (int)(nClientWidth*0.1f);
	int nRemainW = nClientWidth - nDiv19W - n10pW*2 - nDiv19W*17;
	//int nCellW = nClientWidth/m_nCols;
	int nCellH = nClientHeight/m_nRows;
	//int nLastCellW = nClientWidth-nCellW*(m_nCols-1);
	int nLastCellH = nClientHeight-nCellH*(m_nRows-1);
	//int nRemainW = nLastCellW - nCellW;
	int nRemainH = nLastCellH - nCellH;
	for (int i = 0; i < m_GC_1.GetRowCount(); i++) 
	{
		if(i == m_nRows-1)
		{
				m_GC_1.SetRowHeight(i,nCellH);
		}
		else 
		{
			if(nRemainH > 0)
			{
				m_GC_1.SetRowHeight(i,nCellH + 1);
				nRemainH --;
			}
			else
				m_GC_1.SetRowHeight(i,nCellH);
		}
	}
	for (int j = 0; j < m_GC_1.GetColumnCount(); j++)
	{
		if(j == 0) m_GC_1.SetColumnWidth(j,nDiv19W);
		else if(j == 1) m_GC_1.SetColumnWidth(j,n10pW);
		else if(j == 2) m_GC_1.SetColumnWidth(j,n10pW);
		else if(j == 3) m_GC_1.SetColumnWidth(j,nDiv19W);
		else if(j == 4) m_GC_1.SetColumnWidth(j,nDiv19W);
		else if(j == 5) m_GC_1.SetColumnWidth(j,nDiv19W);
		else if(j == 6) m_GC_1.SetColumnWidth(j,nDiv19W);
		else if(j == 7) m_GC_1.SetColumnWidth(j,nDiv19W);
		else if(j == 8) m_GC_1.SetColumnWidth(j,nDiv19W);
		else if(j == 9) m_GC_1.SetColumnWidth(j,nDiv19W);
		else if(j == 10) m_GC_1.SetColumnWidth(j,nDiv19W);
		else if(j == 11) m_GC_1.SetColumnWidth(j,nDiv19W);
		else if(j == 12) m_GC_1.SetColumnWidth(j,nDiv19W);
		else if(j == 13) m_GC_1.SetColumnWidth(j,nDiv19W);
		else if(j == 14) m_GC_1.SetColumnWidth(j,nDiv19W);
		else if(j == 15) m_GC_1.SetColumnWidth(j,nDiv19W);
		else if(j == 16) m_GC_1.SetColumnWidth(j,nDiv19W);
		else if(j == 17) m_GC_1.SetColumnWidth(j,nDiv19W);
		else if(j == 18) m_GC_1.SetColumnWidth(j,nDiv19W);
		else if(j == 19) m_GC_1.SetColumnWidth(j,nDiv19W);
		else if(j == 20) m_GC_1.SetColumnWidth(j,nRemainW);
	}
    DWORD dwTextStyle = DT_CENTER|DT_VCENTER|DT_SINGLELINE;

    // fill rows/cols with text
    for (int row = 0; row < m_GC_1.GetRowCount(); row++) 
	{
        for (int col = 0; col < m_GC_1.GetColumnCount(); col++) 
		{ 
            GV_ITEM Item;
            Item.mask = GVIF_TEXT|GVIF_FORMAT;
            Item.row = row;
            Item.col = col;
			Item.nFormat = dwTextStyle;

			if(row == 0)
			{
				if(col == 0) Item.strText.Format(_T("#"));
				else if(col == 1) Item.strText.Format(_T("Device"));
				else if(col == 2) Item.strText.Format(_T("Customer"));
				else if(col == 3) Item.strText.Format(_T("Tray#"));
				else if(col == 4) Item.strText.Format(_T("Div"));
				else if(col == 5) Item.strText.Format(_T("BndV"));
				else if(col == 6) Item.strText.Format(_T("BndH"));
				else if(col == 7) Item.strText.Format(_T("Aprn"));
				else if(col == 8) Item.strText.Format(_T("Ccov"));
				else if(col == 9) Item.strText.Format(_T("TRot"));
				else if(col == 10) Item.strText.Format(_T("ECap"));
				else if(col == 11) Item.strText.Format(_T("HicD"));
				else if(col == 12) Item.strText.Format(_T("MTyp"));
				else if(col == 13) Item.strText.Format(_T("BTyp"));
				else if(col == 14) Item.strText.Format(_T("BRot"));
				else if(col == 15) Item.strText.Format(_T("BB"));
				else if(col == 16) Item.strText.Format(_T("BLP"));
				else if(col == 17) Item.strText.Format(_T("UseB"));
				else if(col == 18) Item.strText.Format(_T("LSiz"));
				else if(col == 19) Item.strText.Format(_T("SRot"));
				else if(col == 20) Item.strText.Format(_T("Ink"));
			}
			else
			{
			}

			if(row == 0 || col == 0) m_GC_1.SetItemBkColour(row, col, CR_SLATEGRAY);
			m_GC_1.SetItemFgColour(row, col, RGB(0,0,0));
			m_GC_1.SetItemFont(row, col, CTRL_FONT, 18, FW_BOLD);
            m_GC_1.SetItem(&Item);  
        }
    }
       
    return TRUE;  // return TRUE  unless you set the focus to a control
}

void CDlg_Device::OnGridClick_GC_1(NMHDR *pNotifyStruct, LRESULT* /*pResult*/)
{
	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*) pNotifyStruct;

	int nSelRow = pItem->iRow;
	int nSelCol = pItem->iColumn;

	if(nSelRow > 0) nSelRow += m_nCurPageNum*20;

	if(m_nSelDeviceIndex != nSelRow)
	{
		stDeviceTrayInfo stDTI = CDeviceTraySetting::Instance()->GetDeviceTrayInfo();
		if(nSelRow > stDTI.m_nDeviceCount)
		{
			m_nSelDeviceIndex = 0;
			Update_SelDeviceIndex(m_nSelDeviceIndex);
			return;
		}

		m_nSelDeviceIndex = nSelRow;
		Update_SelDeviceIndex(m_nSelDeviceIndex);
	}
}

void CDlg_Device::SetDeviceListSettingValue()
{
	stDeviceTrayInfo stDTI = CDeviceTraySetting::Instance()->GetDeviceTrayInfo();
	if(stDTI.m_nDeviceCount <= 0) return;
	if(m_nSelDeviceIndex > stDTI.m_nDeviceCount) return;

	// 
	DWORD dwTextStyle = DT_CENTER|DT_VCENTER|DT_SINGLELINE;
    for (int row = 0; row < m_GC_1.GetRowCount(); row++) 
	{
        for (int col = 0; col < m_GC_1.GetColumnCount(); col++) 
		{ 
            GV_ITEM Item;
            Item.mask = GVIF_TEXT|GVIF_FORMAT|GVIF_BKCLR;
            Item.row = row;
            Item.col = col;
			Item.nFormat = dwTextStyle;
			Item.crBkClr = RGB(0xFF, 0xFF, 0xE0);

			if(row == 0)
			{
				if(col == 0) Item.strText.Format(_T("#"));
				else if(col == 1) Item.strText.Format(_T("Device"));
				else if(col == 2) Item.strText.Format(_T("Customer"));
				else if(col == 3) Item.strText.Format(_T("Tray#"));
				else if(col == 4) Item.strText.Format(_T("Div"));
				else if(col == 5) Item.strText.Format(_T("BndV"));
				else if(col == 6) Item.strText.Format(_T("BndH"));
				else if(col == 7) Item.strText.Format(_T("Aprn"));
				else if(col == 8) Item.strText.Format(_T("Ccov"));
				else if(col == 9) Item.strText.Format(_T("TRot"));
				else if(col == 10) Item.strText.Format(_T("ECap"));
				else if(col == 11) Item.strText.Format(_T("HicD"));
				else if(col == 12) Item.strText.Format(_T("MTyp"));
				else if(col == 13) Item.strText.Format(_T("BTyp"));
				else if(col == 14) Item.strText.Format(_T("BRot"));
				else if(col == 15) Item.strText.Format(_T("BB"));
				else if(col == 16) Item.strText.Format(_T("BLP"));
				else if(col == 17) Item.strText.Format(_T("UseB"));
				else if(col == 18) Item.strText.Format(_T("LSiz"));
				else if(col == 19) Item.strText.Format(_T("SRot"));
				else if(col == 20) Item.strText.Format(_T("Ink"));
				Item.crBkClr = CR_SLATEGRAY;
			}
			else
			{
				if(col == 0)
				{
					Item.strText.Format(_T("%d"), row + m_nCurPageNum*20);
					Item.crBkClr = CR_SLATEGRAY;
				}
				else if(col == 1)
				{
					if(row + m_nCurPageNum*20 <= stDTI.m_nDeviceCount)
					{
						Item.strText = stDTI.m_vecDeviceInfo[row + m_nCurPageNum*20 - 1].m_strDeviceName;
						Item.crBkClr = CR_GAINSBORO;
					}
				}
				else if(col == 2)
				{
					if(row + m_nCurPageNum*20 <= stDTI.m_nDeviceCount)
					{
						Item.strText = stDTI.m_vecDeviceInfo[row + m_nCurPageNum*20 - 1].m_strCustomer;
						Item.crBkClr = CR_GAINSBORO;
					}
				}
				else if(col == 3)
				{
					if(row + m_nCurPageNum*20 <= stDTI.m_nDeviceCount)
					{
						Item.strText.Format(_T("%d"), stDTI.m_vecDeviceInfo[row + m_nCurPageNum*20 - 1].m_nTrayIndex);
						Item.crBkClr = CR_GAINSBORO;
					}
				}
				else if(col == 4)
				{
					if(row + m_nCurPageNum*20 <= stDTI.m_nDeviceCount)
					{
						Item.strText.Format(_T("%d"), stDTI.m_vecDeviceInfo[row + m_nCurPageNum*20 - 1].m_nTrayDivideCount);
						Item.crBkClr = CR_GAINSBORO;
					}
				}
				else if(col == 5)
				{
					if(row + m_nCurPageNum*20 <= stDTI.m_nDeviceCount)
					{
						Item.strText.Format(_T("%d"), stDTI.m_vecDeviceInfo[row + m_nCurPageNum*20 - 1].m_nBand_VCount);
						Item.crBkClr = CR_GAINSBORO;
					}
				}
				else if(col == 6)
				{
					if(row + m_nCurPageNum*20 <= stDTI.m_nDeviceCount)
					{
						Item.strText.Format(_T("%d"), stDTI.m_vecDeviceInfo[row + m_nCurPageNum*20 - 1].m_nBand_HCount);
						Item.crBkClr = CR_GAINSBORO;
					}
				}
				else if(col == 7)
				{
					if(row + m_nCurPageNum*20 <= stDTI.m_nDeviceCount)
					{
						Item.strText.Format(_T("%d"), stDTI.m_vecDeviceInfo[row + m_nCurPageNum*20 - 1].m_nBottomTray);
						Item.crBkClr = CR_GAINSBORO;
					}
				}
				else if(col == 8)
				{
					if(row + m_nCurPageNum*20 <= stDTI.m_nDeviceCount)
					{
						Item.strText.Format(_T("%d"), stDTI.m_vecDeviceInfo[row + m_nCurPageNum*20 - 1].m_nCoverTray);
						Item.crBkClr = CR_GAINSBORO;
					}
				}
				else if(col == 9)
				{
					if(row + m_nCurPageNum*20 <= stDTI.m_nDeviceCount)
					{
						Item.strText.Format(_T("%d"), stDTI.m_vecDeviceInfo[row + m_nCurPageNum*20 - 1].m_nTrayRotate);
						Item.crBkClr = CR_GAINSBORO;
					}
				}
				else if(col == 10)
				{
					if(row + m_nCurPageNum*20 <= stDTI.m_nDeviceCount)
					{
						Item.strText.Format(_T("%d"), stDTI.m_vecDeviceInfo[row + m_nCurPageNum*20 - 1].m_nEndCap);
						Item.crBkClr = CR_GAINSBORO;
					}
				}
				else if(col == 11)
				{
					if(row + m_nCurPageNum*20 <= stDTI.m_nDeviceCount)
					{
						Item.strText.Format(_T("%d"), stDTI.m_vecDeviceInfo[row + m_nCurPageNum*20 - 1].m_nUseHIC_DSC);
						Item.crBkClr = CR_GAINSBORO;
					}
				}
				else if(col == 12)
				{
					if(row + m_nCurPageNum*20 <= stDTI.m_nDeviceCount)
					{
						Item.strText.Format(_T("%d"), stDTI.m_vecDeviceInfo[row + m_nCurPageNum*20 - 1].m_nMBB_Type);
						Item.crBkClr = CR_GAINSBORO;
					}
				}
				else if(col == 13)
				{
					if(row + m_nCurPageNum*20 <= stDTI.m_nDeviceCount)
					{
						Item.strText.Format(_T("%d"), stDTI.m_vecDeviceInfo[row + m_nCurPageNum*20 - 1].m_nBOX_Type);
						Item.crBkClr = CR_GAINSBORO;
					}
				}
				else if(col == 14)
				{
					if(row + m_nCurPageNum*20 <= stDTI.m_nDeviceCount)
					{
						Item.strText.Format(_T("%d"), stDTI.m_vecDeviceInfo[row + m_nCurPageNum*20 - 1].m_nBoxRotate);
						Item.crBkClr = CR_GAINSBORO;
					}
				}
				else if(col == 15)
				{
					if(row + m_nCurPageNum*20 <= stDTI.m_nDeviceCount)
					{
						Item.strText.Format(_T("%d"), stDTI.m_vecDeviceInfo[row + m_nCurPageNum*20 - 1].m_nBubbleSheet);
						Item.crBkClr = CR_GAINSBORO;
					}
				}
				else if(col == 16)
				{
					if(row + m_nCurPageNum*20 <= stDTI.m_nDeviceCount)
					{
						Item.strText.Format(_T("%d"), stDTI.m_vecDeviceInfo[row + m_nCurPageNum*20 - 1].m_nBoxLabelPos);
						Item.crBkClr = CR_GAINSBORO;
					}
				}
				else if(col == 17)
				{
					if(row + m_nCurPageNum*20 <= stDTI.m_nDeviceCount)
					{
						Item.strText.Format(_T("%d"), stDTI.m_vecDeviceInfo[row + m_nCurPageNum*20 - 1].m_nUseBox);
						Item.crBkClr = CR_GAINSBORO;
					}
				}
				else if(col == 18)
				{
					if(row + m_nCurPageNum*20 <= stDTI.m_nDeviceCount)
					{
						Item.strText.Format(_T("%d"), stDTI.m_vecDeviceInfo[row + m_nCurPageNum*20 - 1].m_nLabelSize);
						Item.crBkClr = CR_GAINSBORO;
					}
				}
				else if(col == 19)
				{
					if(row + m_nCurPageNum*20 <= stDTI.m_nDeviceCount)
					{
						Item.strText.Format(_T("%d"), stDTI.m_vecDeviceInfo[row + m_nCurPageNum*20 - 1].m_nSilRotate);
						Item.crBkClr = CR_GAINSBORO;
					}
				}
				else if(col == 20)
				{
					if(row + m_nCurPageNum*20 <= stDTI.m_nDeviceCount)
					{
						Item.strText.Format(_T("%d"), stDTI.m_vecDeviceInfo[row + m_nCurPageNum*20 - 1].m_nUseInk);
						Item.crBkClr = CR_GAINSBORO;
					}
				}
			}

			m_GC_1.SetItemFgColour(row, col, RGB(0,0,0));
            m_GC_1.SetItem(&Item);  
        }
    }

	m_GC_1.Invalidate();
}

void CDlg_Device::Update_SelDeviceIndex(int nDeviceIndex)
{
	if(nDeviceIndex == 0)
	{
		m_edit_1.SetWindowText(_T(""));
		m_st_3.SetWindowText(_T(""));
		m_st_4.SetWindowText(_T(""));
		m_edit_2.SetWindowText(_T(""));
		m_edit_3.SetWindowText(_T(""));
		m_edit_4.SetWindowText(_T(""));
		m_edit_5.SetWindowText(_T(""));
		m_btn_V2.SetOnOff(FALSE);
		m_btn_V2.Invalidate();
		m_btn_V3.SetOnOff(FALSE);
		m_btn_V3.Invalidate();
		m_btn_V4.SetOnOff(FALSE);
		m_btn_V4.Invalidate();
		m_btn_V5.SetOnOff(FALSE);
		m_btn_V5.Invalidate();
		m_btn_V6.SetOnOff(FALSE);
		m_btn_V6.Invalidate();
		m_btn_V7.SetOnOff(FALSE);
		m_btn_V7.Invalidate();
		m_btn_V8.SetOnOff(FALSE);
		m_btn_V8.Invalidate();
		m_btn_V9.SetOnOff(FALSE);
		m_btn_V9.Invalidate();
		m_btn_V10.SetOnOff(FALSE);
		m_btn_V10.Invalidate();
		m_btn_V11.SetOnOff(FALSE);
		m_btn_V11.Invalidate();
		m_btn_V12.SetOnOff(FALSE);
		m_btn_V12.Invalidate();
		m_btn_V13.SetOnOff(FALSE);
		m_btn_V13.Invalidate();
		m_btn_V14.SetOnOff(FALSE);
		m_btn_V14.Invalidate();
		m_btn_V15.SetOnOff(FALSE);
		m_btn_V15.Invalidate();
	}
	else
	{
		CString strTemp;
		stDeviceTrayInfo stDTI = CDeviceTraySetting::Instance()->GetDeviceTrayInfo();

		m_edit_1.SetWindowText(stDTI.m_vecDeviceInfo[m_nSelDeviceIndex-1].m_strDeviceName);
		m_edit_5.SetWindowText(stDTI.m_vecDeviceInfo[m_nSelDeviceIndex-1].m_strCustomer);
		if(stDTI.m_vecDeviceInfo[m_nSelDeviceIndex-1].m_nTrayIndex > 0)
		{
			strTemp.Format(_T("%d"), stDTI.m_vecDeviceInfo[m_nSelDeviceIndex-1].m_nTrayIndex);
			m_st_3.SetWindowText(strTemp);
			strTemp.Format(_T("%s"), stDTI.m_vecTrayInfo[stDTI.m_vecDeviceInfo[m_nSelDeviceIndex-1].m_nTrayIndex - 1].m_strTraySize);
			m_st_4.SetWindowText(strTemp);
		}
		else
		{
			m_st_3.SetWindowText(_T(""));
			m_st_4.SetWindowText(_T(""));
		}
		strTemp.Format(_T("%d"), stDTI.m_vecDeviceInfo[m_nSelDeviceIndex-1].m_nTrayDivideCount);
		m_edit_2.SetWindowText(strTemp);
		strTemp.Format(_T("%d"), stDTI.m_vecDeviceInfo[m_nSelDeviceIndex-1].m_nBand_VCount);
		m_edit_3.SetWindowText(strTemp);
		strTemp.Format(_T("%d"), stDTI.m_vecDeviceInfo[m_nSelDeviceIndex-1].m_nBand_HCount);
		m_edit_4.SetWindowText(strTemp);
		m_btn_V2.SetOnOff(stDTI.m_vecDeviceInfo[m_nSelDeviceIndex-1].m_nBottomTray);
		m_btn_V2.Invalidate();
		m_btn_V3.SetOnOff(stDTI.m_vecDeviceInfo[m_nSelDeviceIndex-1].m_nCoverTray);
		m_btn_V3.Invalidate();
		m_btn_V4.SetOnOff(stDTI.m_vecDeviceInfo[m_nSelDeviceIndex-1].m_nTrayRotate);
		m_btn_V4.Invalidate();
		m_btn_V5.SetOnOff(stDTI.m_vecDeviceInfo[m_nSelDeviceIndex-1].m_nEndCap);
		m_btn_V5.Invalidate();
		m_btn_V6.SetOnOff(stDTI.m_vecDeviceInfo[m_nSelDeviceIndex-1].m_nUseHIC_DSC);
		m_btn_V6.Invalidate();
		m_btn_V7.SetOnOff(stDTI.m_vecDeviceInfo[m_nSelDeviceIndex-1].m_nMBB_Type);
		m_btn_V7.Invalidate();
		m_btn_V8.SetOnOff(stDTI.m_vecDeviceInfo[m_nSelDeviceIndex-1].m_nBOX_Type);
		m_btn_V8.Invalidate();
		m_btn_V9.SetOnOff(stDTI.m_vecDeviceInfo[m_nSelDeviceIndex-1].m_nBoxRotate);
		m_btn_V9.Invalidate();
		m_btn_V10.SetOnOff(stDTI.m_vecDeviceInfo[m_nSelDeviceIndex-1].m_nBubbleSheet);
		m_btn_V10.Invalidate();
		m_btn_V11.SetOnOff(stDTI.m_vecDeviceInfo[m_nSelDeviceIndex-1].m_nBoxLabelPos);
		m_btn_V11.Invalidate();
		m_btn_V12.SetOnOff(stDTI.m_vecDeviceInfo[m_nSelDeviceIndex-1].m_nUseBox);
		m_btn_V12.Invalidate();
		m_btn_V13.SetOnOff(stDTI.m_vecDeviceInfo[m_nSelDeviceIndex-1].m_nLabelSize);
		m_btn_V13.Invalidate();
		m_btn_V14.SetOnOff(stDTI.m_vecDeviceInfo[m_nSelDeviceIndex-1].m_nSilRotate);
		m_btn_V14.Invalidate();
		m_btn_V15.SetOnOff(stDTI.m_vecDeviceInfo[m_nSelDeviceIndex-1].m_nUseInk);
		m_btn_V15.Invalidate();
	}
}

void CDlg_Device::Update_PLC_Value()
{
}

void CDlg_Device::OnBnClickedBtnDv1()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_nCurPageNum --;
	if(m_nCurPageNum < 0) m_nCurPageNum = 0;
	else SetDeviceListSettingValue();
}


void CDlg_Device::OnBnClickedBtnDv2()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	stDeviceTrayInfo stDTI = CDeviceTraySetting::Instance()->GetDeviceTrayInfo();
	m_nCurPageNum ++;
	int nMaxPage = stDTI.m_nDeviceCount/20;
	if(stDTI.m_nDeviceCount%20 > 0) nMaxPage ++;
	if(m_nCurPageNum > nMaxPage - 1) m_nCurPageNum = nMaxPage - 1;
	else SetDeviceListSettingValue();
}


void CDlg_Device::OnBnClickedBtnDv3()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CDeviceTraySetting::Instance()->AddDeviceInfo();

	SetDeviceListSettingValue();
}


void CDlg_Device::OnBnClickedBtnDv4()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CDeviceTraySetting::Instance()->DelDeviceInfo();

	SetDeviceListSettingValue();
}


void CDlg_Device::OnBnClickedBtnDv5()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(CDataManager::Instance()->IsConnected(CONN_PLC_1)) CDataManager::Instance()->m_PLCReaderZR.Read();
	else return;

	m_btn_5.EnableWindow(FALSE);

	int nTrayIndex, nDivide, nBandV, nBandH;
	int nApron, nCover, nTRotate, nEndCap, nUseHICDSC, nMBBType, nBoxType;
	int nBRotate, nBubble, nBoxLabelPos, nUseBox, nLabelSize, nSRotate, nUseInk;
	stDeviceTrayInfo stDTI = CDeviceTraySetting::Instance()->GetDeviceTrayInfo();
	int nStartAddress;
	MADDRESS mBit;

	for(int i=0;i<stDTI.m_nDeviceCount;i++)
	{
		nStartAddress = CDataManager::Instance()->m_PLCReaderZR.nGetAddress_Start() + i*10;
		nTrayIndex = CDataManager::Instance()->m_PLCReaderZR.GetValue(nStartAddress+0);
		nDivide = CDataManager::Instance()->m_PLCReaderZR.GetValue(nStartAddress+1);
		nBandV = CDataManager::Instance()->m_PLCReaderZR.GetValue(nStartAddress+2);
		nBandH = CDataManager::Instance()->m_PLCReaderZR.GetValue(nStartAddress+3);

		mBit = CDataManager::Instance()->m_PLCReaderZR.GetValueM(nStartAddress+9);

		nApron = mBit.m_xBit_0;
		nCover = mBit.m_xBit_1;
		nTRotate = mBit.m_xBit_4;
		nEndCap = mBit.m_xBit_5;
		nUseHICDSC = mBit.m_xBit_6;
		nMBBType = mBit.m_xBit_7;
		nBoxType = mBit.m_xBit_8;
		nBRotate = mBit.m_xBit_9;
		nBubble = mBit.m_xBit_A;
		nBoxLabelPos = mBit.m_xBit_B;
		nUseBox = mBit.m_xBit_C;
		nUseInk = mBit.m_xBit_D;
		nLabelSize = mBit.m_xBit_E;
		nSRotate = mBit.m_xBit_F;

		CDeviceTraySetting::Instance()->SetDeviceInfo(i, nTrayIndex, nDivide, nBandV, nBandH, nApron, nCover, nTRotate, nEndCap,
			nUseHICDSC, nMBBType, nBoxType, nBRotate, nBubble, nBoxLabelPos, nUseBox, nLabelSize, nSRotate, nUseInk);
	}

	SetDeviceListSettingValue();

	m_btn_5.EnableWindow(TRUE);
}


void CDlg_Device::OnBnClickedBtnDv6()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(CDataManager::Instance()->IsConnected(CONN_PLC_1) == FALSE) return;

	m_btn_6.EnableWindow(FALSE);

	int nTrayIndex, nDivide, nBandV, nBandH;
	stDeviceTrayInfo stDTI = CDeviceTraySetting::Instance()->GetDeviceTrayInfo();
	int nStartAddress;
	for(int i=0;i<stDTI.m_nDeviceCount;i++)
	{
		nStartAddress = 20000 + i*10;

		nTrayIndex = stDTI.m_vecDeviceInfo[i].m_nTrayIndex;
		CDataManager::Instance()->PLC_Write_ZR(1, nStartAddress+0, (SHORT)nTrayIndex);

		nDivide = stDTI.m_vecDeviceInfo[i].m_nTrayDivideCount;
		CDataManager::Instance()->PLC_Write_ZR(1, nStartAddress+1, (SHORT)nDivide);

		nBandV = stDTI.m_vecDeviceInfo[i].m_nBand_VCount;
		CDataManager::Instance()->PLC_Write_ZR(1, nStartAddress+2, (SHORT)nBandV);

		nBandH = stDTI.m_vecDeviceInfo[i].m_nBand_HCount;
		CDataManager::Instance()->PLC_Write_ZR(1, nStartAddress+3, (SHORT)nBandH);

		MADDRESS mBit;
		mBit.m_xBit_0 = stDTI.m_vecDeviceInfo[i].m_nBottomTray;
		mBit.m_xBit_1 = stDTI.m_vecDeviceInfo[i].m_nCoverTray;
		mBit.m_xBit_4 = stDTI.m_vecDeviceInfo[i].m_nTrayRotate;
		mBit.m_xBit_5 = stDTI.m_vecDeviceInfo[i].m_nEndCap;
		mBit.m_xBit_6 = stDTI.m_vecDeviceInfo[i].m_nUseHIC_DSC;
		mBit.m_xBit_7 = stDTI.m_vecDeviceInfo[i].m_nMBB_Type;
		mBit.m_xBit_8 = stDTI.m_vecDeviceInfo[i].m_nBOX_Type;
		mBit.m_xBit_9 = stDTI.m_vecDeviceInfo[i].m_nBoxRotate;
		mBit.m_xBit_A = stDTI.m_vecDeviceInfo[i].m_nBubbleSheet;
		mBit.m_xBit_B = stDTI.m_vecDeviceInfo[i].m_nBoxLabelPos;
		mBit.m_xBit_C = stDTI.m_vecDeviceInfo[i].m_nUseBox;
		mBit.m_xBit_D = stDTI.m_vecDeviceInfo[i].m_nUseInk;
		mBit.m_xBit_E = stDTI.m_vecDeviceInfo[i].m_nLabelSize;
		mBit.m_xBit_F = stDTI.m_vecDeviceInfo[i].m_nSilRotate;
		CDataManager::Instance()->PLC_Write_ZR(1, nStartAddress+9, (SHORT*)&mBit, 1);
	}

	m_btn_6.EnableWindow(TRUE);
}


void CDlg_Device::OnBnClickedBtnDv7()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	stDeviceTrayInfo stDTI = CDeviceTraySetting::Instance()->GetDeviceTrayInfo();
	if(m_nSelDeviceIndex == 0) return;
	if(m_nSelDeviceIndex > stDTI.m_nDeviceCount) return;

	CString strName, strCustomer, strTemp;
	int nTrayIndex, nDivide, nBandV, nBandH;
	int nApron, nCover, nTRotate, nEndCap, nUseHICDSC, nMBBType, nBoxType;
	int nBRotate, nBubble, nBoxLabelPos, nUseBox, nLabelSize, nSRotate, nUseInk;
	
	m_edit_1.GetWindowText(strName);
	m_edit_5.GetWindowText(strCustomer);
	nTrayIndex = _wtoi(m_st_3.GetText());
	m_edit_2.GetWindowText(strTemp);
	nDivide = _wtoi(strTemp);
	m_edit_3.GetWindowText(strTemp);
	nBandV = _wtoi(strTemp);
	m_edit_4.GetWindowText(strTemp);
	nBandH = _wtoi(strTemp);
	nApron = (int)m_btn_V2.GetOnOff();
	nCover = (int)m_btn_V3.GetOnOff();
	nTRotate = (int)m_btn_V4.GetOnOff();
	nEndCap = (int)m_btn_V5.GetOnOff();
	nUseHICDSC = (int)m_btn_V6.GetOnOff();
	nMBBType = (int)m_btn_V7.GetOnOff();
	nBoxType = (int)m_btn_V8.GetOnOff();
	nBRotate = (int)m_btn_V9.GetOnOff();
	nBubble = (int)m_btn_V10.GetOnOff();
	nBoxLabelPos = (int)m_btn_V11.GetOnOff();
	nUseBox = (int)m_btn_V12.GetOnOff();
	nLabelSize = (int)m_btn_V13.GetOnOff();
	nSRotate = (int)m_btn_V14.GetOnOff();
	nUseInk = (int)m_btn_V15.GetOnOff();

	CDeviceTraySetting::Instance()->SetDeviceInfo2(m_nSelDeviceIndex - 1, strName, strCustomer, nTrayIndex, nDivide, nBandV, nBandH, nApron, nCover,
		nTRotate, nEndCap, nUseHICDSC, nMBBType, nBoxType, nBRotate, nBubble, nBoxLabelPos, nUseBox, nLabelSize, nSRotate, nUseInk);

	SetDeviceListSettingValue();
}


void CDlg_Device::OnBnClickedBtnDv8()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_btn_8.EnableWindow(FALSE);
	CDeviceTraySetting::Instance()->Save_DeviceSetting();
	m_btn_8.EnableWindow(TRUE);
}


void CDlg_Device::OnBnClickedBtnDv9()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CAMKOR_MMIDlg *)AfxGetMainWnd())->SendMessage(WM_CHANGESCREEN, 0, 0);
}


void CDlg_Device::OnBnClickedBtnDvV1()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CDlg_TrayInfo stTI;

	if(stTI.DoModal() == IDOK)
	{
		stDeviceTrayInfo stDTI = CDeviceTraySetting::Instance()->GetDeviceTrayInfo();

		CString strTemp;
		strTemp.Format(_T("%d"), stTI.m_nSelTrayIndex);
		m_st_3.SetWindowText(strTemp);

		strTemp.Format(_T("%s"), stDTI.m_vecTrayInfo[stTI.m_nSelTrayIndex-1].m_strTraySize);
		m_st_4.SetWindowText(strTemp);
	}
}


void CDlg_Device::OnBnClickedBtnDvV2()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(m_btn_V2.GetOnOff()) m_btn_V2.SetOnOff(FALSE);
	else m_btn_V2.SetOnOff(TRUE);
	m_btn_V2.Invalidate();
}


void CDlg_Device::OnBnClickedBtnDvV3()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(m_btn_V3.GetOnOff()) m_btn_V3.SetOnOff(FALSE);
	else m_btn_V3.SetOnOff(TRUE);
	m_btn_V3.Invalidate();
}


void CDlg_Device::OnBnClickedBtnDvV4()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(m_btn_V4.GetOnOff()) m_btn_V4.SetOnOff(FALSE);
	else m_btn_V4.SetOnOff(TRUE);
	m_btn_V4.Invalidate();
}


void CDlg_Device::OnBnClickedBtnDvV5()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(m_btn_V5.GetOnOff()) m_btn_V5.SetOnOff(FALSE);
	else m_btn_V5.SetOnOff(TRUE);
	m_btn_V5.Invalidate();
}


void CDlg_Device::OnBnClickedBtnDvV6()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(m_btn_V6.GetOnOff()) m_btn_V6.SetOnOff(FALSE);
	else m_btn_V6.SetOnOff(TRUE);
	m_btn_V6.Invalidate();
}


void CDlg_Device::OnBnClickedBtnDvV7()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(m_btn_V7.GetOnOff()) m_btn_V7.SetOnOff(FALSE);
	else m_btn_V7.SetOnOff(TRUE);
	m_btn_V7.Invalidate();
}


void CDlg_Device::OnBnClickedBtnDvV8()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(m_btn_V8.GetOnOff()) m_btn_V8.SetOnOff(FALSE);
	else m_btn_V8.SetOnOff(TRUE);
	m_btn_V8.Invalidate();
}


void CDlg_Device::OnBnClickedBtnDvV9()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(m_btn_V9.GetOnOff()) m_btn_V9.SetOnOff(FALSE);
	else m_btn_V9.SetOnOff(TRUE);
	m_btn_V9.Invalidate();
}


void CDlg_Device::OnBnClickedBtnDvV10()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(m_btn_V10.GetOnOff()) m_btn_V10.SetOnOff(FALSE);
	else m_btn_V10.SetOnOff(TRUE);
	m_btn_V10.Invalidate();
}


void CDlg_Device::OnBnClickedBtnDvV11()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(m_btn_V11.GetOnOff()) m_btn_V11.SetOnOff(FALSE);
	else m_btn_V11.SetOnOff(TRUE);
	m_btn_V11.Invalidate();
}


void CDlg_Device::OnBnClickedBtnDvV12()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(m_btn_V12.GetOnOff()) m_btn_V12.SetOnOff(FALSE);
	else m_btn_V12.SetOnOff(TRUE);
	m_btn_V12.Invalidate();
}


void CDlg_Device::OnBnClickedBtnDvV13()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(m_btn_V13.GetOnOff()) m_btn_V13.SetOnOff(FALSE);
	else m_btn_V13.SetOnOff(TRUE);
	m_btn_V13.Invalidate();
}


void CDlg_Device::OnBnClickedBtnDvV14()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(m_btn_V14.GetOnOff()) m_btn_V14.SetOnOff(FALSE);
	else m_btn_V14.SetOnOff(TRUE);
	m_btn_V14.Invalidate();
}


void CDlg_Device::OnBnClickedBtnDvV15()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(m_btn_V15.GetOnOff()) m_btn_V15.SetOnOff(FALSE);
	else m_btn_V15.SetOnOff(TRUE);
	m_btn_V15.Invalidate();
}
