#ifndef _PROFILE_H_
#define _PROFILE_H_

#define PRF_PATH				"../system.ini"/*레시피 파일명 (파일 open 시 수정, 삭제, new)*/

#define PRF_AXIS_NAME			"AXIS"
	#define PRF_AXIS_DIRECTION		"DIRECTION"
	#define PRF_AXIS_MOVE_POS		"MovePOS"
	#define PRF_AXIS_VELOCITY		"VELOCITY"
	#define PRF_AXIS_ACCEL			"ACCEL"
	#define PRF_AXIS_DECEL			"DECEL"

#define PRF_TRIGGER				"TRIGGER"
#define PRF_TRIGGER_START		"START"
#define PRF_TRIGGER_END			"END"
#define PRF_TRIGGER_PULSEWIDTH	"PULSEWIDTH"
#define PRF_TRIGGER_CYCLE		"CYCLE"
#define PRF_TRIGGER_REPEAT_START	"REPEAT_START"
#define PRF_TRIGGER_REPEAT_END		"REPEAT_END"
#define PRF_TRIGGER_REPEAT_VEL		"REPEAT_VELOCITY"
#define PRF_TRIGGER_REPEAT_ACC_DEC	"REPEAT_ACC_DEC"


extern CString getprofilestring_g( LPCTSTR lpAppName, LPCTSTR lpKeyName, LPCTSTR lpDefault, LPCTSTR lpFileName );
extern BOOL setprofilestring_g( LPCTSTR lpAppName, LPCTSTR lpKeyName, LPCTSTR lpData, LPCTSTR lpFileName );
extern int getprofileint_g( LPCTSTR lpAppName, LPCTSTR lpKeyName, int nValue, LPCTSTR lpFileName );
extern BOOL setprofileint_g( LPCTSTR lpAppName, LPCTSTR lpKeyName, int nValue, LPCTSTR lpFileName );
extern BOOL getprofilestruct_g( LPCTSTR lpszsection, LPCTSTR lpszkey, LPVOID lpstruct, UINT usizestruct, LPCTSTR szfile );
extern BOOL setprofilestruct_g( LPCTSTR lpszsection, LPCTSTR lpszkey, LPVOID lpstruct, UINT usizestruct, LPCTSTR szfile );
extern BOOL getprofilebinary_g( CWinApp *pApp, LPCTSTR lpszSection, LPCTSTR lpszEntry, LPBYTE* ppData, UINT* pBytes );
extern BOOL setprofilebinary_g( CWinApp *pApp, LPCTSTR lpszSection, LPCTSTR lpszEntry, LPBYTE pData, UINT nBytes );

extern BOOL setprofilerect_g(LPCTSTR lpszSection, LPCTSTR lpszEntry, const RECT Value, LPCTSTR lpFileName);
extern RECT getprofilerect_g(LPCTSTR lpszSection, LPCTSTR lpszEntry, const RECT Default, LPCTSTR lpFileName);

extern BOOL setprofilesize_g(LPCTSTR lpszSection, LPCTSTR lpszEntry, const SIZE Value, LPCTSTR lpFileName);
extern SIZE getprofilesize_g(LPCTSTR lpszSection, LPCTSTR lpszEntry, const SIZE Default, LPCTSTR lpFileName);

extern BOOL setprofiledouble_g(LPCTSTR lpszSection, LPCTSTR lpszEntry, const double Value, LPCTSTR lpFileName);
extern double getprofiledouble_g(LPCTSTR lpszSection, LPCTSTR lpszEntry, const double Default, LPCTSTR lpFileName);

extern BOOL setprofiledword_g(LPCTSTR lpszSection, LPCTSTR lpszEntry, const DWORD Value, LPCTSTR lpFileName);
extern DWORD getprofiledword_g(LPCTSTR lpszSection, LPCTSTR lpszEntry, const DWORD Default, LPCTSTR lpFileName);

extern BOOL setprofilecolor_g(LPCTSTR lpszSection, LPCTSTR lpszEntry, const COLORREF Value, LPCTSTR lpFileName);
extern COLORREF getprofilecolor_g(LPCTSTR lpszSection, LPCTSTR lpszEntry, const COLORREF crDefault, LPCTSTR lpFileName);

// BOOL WriteProfileRect(LPCTSTR lpszSection, LPCTSTR lpszEntry, const RECT& Value);
// RECT GetProfileRect(LPCTSTR lpszSection, LPCTSTR lpszEntry, const RECT& Default);
// 
// BOOL WriteProfileSize(LPCTSTR lpszSection, LPCTSTR lpszEntry, const SIZE& Value);
// SIZE GetProfileSize(LPCTSTR lpszSection, LPCTSTR lpszEntry, const SIZE& Default);
// 
// BOOL WriteProfileDouble(LPCTSTR lpszSection, LPCTSTR lpszEntry, const double& Value);
// double GetProfileDouble(LPCTSTR lpszSection, LPCTSTR lpszEntry, const double& Default);
// 
// BOOL WriteProfileDword(LPCTSTR lpszSection, LPCTSTR lpszEntry, const DWORD& Value);
// DWORD GetProfileDword(LPCTSTR lpszSection, LPCTSTR lpszEntry, const DWORD& Default);
// 
// BOOL WriteProfileColor(LPCTSTR lpszSection, LPCTSTR lpszEntry, const COLORREF& Value);
// COLORREF GetProfileColor(LPCTSTR lpszSection, LPCTSTR lpszEntry, const COLORREF& crDefault);	
// 



#endif