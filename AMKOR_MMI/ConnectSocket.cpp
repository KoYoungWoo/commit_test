// ConnectSocket.cpp : implementation file
//

#include "stdafx.h"
#include "AMKOR_MMI.h"
#include "AMKOR_MMIDlg.h"
#include "ConnectSocket.h"

// CConnectSocket

CConnectSocket::CConnectSocket()
{
	m_strIP.Empty();
	m_uiPort = 0;
}

CConnectSocket::~CConnectSocket()
{
}

// CConnectSocket member functions
void CConnectSocket::OnClose(int nErrorCode)
{
	CAMKOR_MMIDlg* pMain = (CAMKOR_MMIDlg*)AfxGetMainWnd();
}

void CConnectSocket::OnReceive(int nErrorCode)
{
	int nRead = 0;

	CHAR chBuff[1024];
	::ZeroMemory(chBuff, sizeof(chBuff));

	TCHAR Buff[1024*2];
	::ZeroMemory(Buff, sizeof(Buff));
	CAMKOR_MMIApp* pApp = (CAMKOR_MMIApp*)AfxGetApp();
	CAMKOR_MMIDlg* pMain = (CAMKOR_MMIDlg*)AfxGetMainWnd();

	nRead = Receive((BYTE*)chBuff, sizeof(chBuff));
	if(nRead >= 0)
	{
		if(strlen(chBuff) > 0) 
		{
			pApp->MBCS2Unicode(chBuff,Buff);
			GetLog()->Debug(_T("RCV PRINTER 01 MSG[%s]"),Buff);
		}
	}
	
	CSocket::OnReceive(nErrorCode);
}

void CConnectSocket::SetConnectInfo(CString strIP, UINT uiPort)
{
	m_strIP = strIP;
	m_uiPort = uiPort;
}

BOOL CConnectSocket::SendData(CString strData)
{
	if(m_strIP.IsEmpty()) return FALSE;
	if(m_uiPort == 0) return FALSE;
	if(Create() == FALSE) return FALSE;
	
	if(Connect(m_strIP, m_uiPort) == FALSE)
	{
		ShutDown(2);
		CancelBlockingCall();
		Close();
		
		GetLog()->Debug(_T("ERROR: Failed to connect %s"), m_strIP);
		CLogList::Instance()->Add_LogData(eLog_DEBUG, _T("ERROR: Failed to connect %s"), m_strIP);
		return FALSE;
	}

	char chSendPacket[1024*10];
	memset(chSendPacket,0x00,sizeof(chSendPacket));
	
	CAMKOR_MMIApp* pApp = (CAMKOR_MMIApp*)AfxGetApp();
	pApp->Unicode2MBCS(strData.GetBuffer(),chSendPacket);
	strData.ReleaseBuffer();

	int dwBytes = Send((LPVOID)chSendPacket,strlen(chSendPacket)+1);
	if(dwBytes != SOCKET_ERROR)
	{
		GetLog()->Debug(_T("(%s)[SEND] %s"), m_strIP, strData.Left(dwBytes));
	}
	else
	{
		GetLog()->Debug(_T("[ERROR](%s)[SEND] Err(%d) %s"), m_strIP, GetLastError(),strData);
	}
	ShutDown();
	CancelBlockingCall();
	Close();

	return TRUE;
}
