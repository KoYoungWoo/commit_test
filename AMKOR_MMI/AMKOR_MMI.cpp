
// AMKOR_MMI.cpp : 응용 프로그램에 대한 클래스 동작을 정의합니다.
//

#include "stdafx.h"
#include "AMKOR_MMI.h"
#include "AMKOR_MMIDlg.h"
#include "LOG/whdump.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CAMKOR_MMIApp

BEGIN_MESSAGE_MAP(CAMKOR_MMIApp, CWinApp)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
END_MESSAGE_MAP()


// CAMKOR_MMIApp 생성

CAMKOR_MMIApp::CAMKOR_MMIApp()
{
	// 다시 시작 관리자 지원
	m_dwRestartManagerSupportFlags = AFX_RESTART_MANAGER_SUPPORT_RESTART;

	// TODO: 여기에 생성 코드를 추가합니다.
	// InitInstance에 모든 중요한 초기화 작업을 배치합니다.
}


// 유일한 CAMKOR_MMIApp 개체입니다.

CAMKOR_MMIApp theApp;


// CAMKOR_MMIApp 초기화

BOOL CAMKOR_MMIApp::InitInstance()
{
	// 중복 실행 방지 //////////////////////////////////////////
	static HANDLE hMutex = NULL;

	hMutex = CreateMutex(NULL, TRUE, _T("AMKOR_MMI"));

	if (ERROR_ALREADY_EXISTS == GetLastError())
	{
		CloseHandle(hMutex);
		return FALSE;
	}
	if (hMutex)
		ReleaseMutex(hMutex);
	/////////////////////////////////////////////////////////////
	//MFC소켓 사용 위해 초기화
	if (!AfxSocketInit())
	{
		AfxMessageBox(_T("IDP_SOCKETS_INIT_FAILED"));
		return FALSE;
	}
	/////////////////////////////////////////////////////////////
	// 미니덤프 생성
	#ifdef _DEBUG
	//_CrtSetBreakAlloc(0x0060DF80);
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
	#else
	MiniDumper mDump(_T("MiniDump"));
	#endif
	/////////////////////////////////////////////////////////////

	// 응용 프로그램 매니페스트가 ComCtl32.dll 버전 6 이상을 사용하여 비주얼 스타일을
	// 사용하도록 지정하는 경우, Windows XP 상에서 반드시 InitCommonControlsEx()가 필요합니다.
	// InitCommonControlsEx()를 사용하지 않으면 창을 만들 수 없습니다.
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// 응용 프로그램에서 사용할 모든 공용 컨트롤 클래스를 포함하도록
	// 이 항목을 설정하십시오.
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinApp::InitInstance();

	AfxEnableControlContainer();

	// 대화 상자에 셸 트리 뷰 또는
	// 셸 목록 뷰 컨트롤이 포함되어 있는 경우 셸 관리자를 만듭니다.
	CShellManager *pShellManager = new CShellManager;

	// MFC 컨트롤의 테마를 사용하기 위해 "Windows 원형" 비주얼 관리자 활성화
	CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerWindows));

	// 표준 초기화
	// 이들 기능을 사용하지 않고 최종 실행 파일의 크기를 줄이려면
	// 아래에서 필요 없는 특정 초기화
	// 루틴을 제거해야 합니다.
	// 해당 설정이 저장된 레지스트리 키를 변경하십시오.
	// TODO: 이 문자열을 회사 또는 조직의 이름과 같은
	// 적절한 내용으로 수정해야 합니다.
	SetRegistryKey(_T("로컬 응용 프로그램 마법사에서 생성된 응용 프로그램"));

	/////////////////////////////////////////////////////////////
	// LOG 저장 폴더 지정
	CString strLogPath;
	strLogPath.Format(_T("%s\\LOG"), CMotorManualSetting::Instance()->GetExecuteDirectory());
	GetLog()->SetLogPath(strLogPath);
	/////////////////////////////////////////////////////////////
	// Pre Loading Setting File
	CAppSetting::Instance()->Load();
	CDeviceTraySetting::Instance()->Load_TrayInfoSetting();
	CDeviceTraySetting::Instance()->Load_DeviceSetting();
	CMotorManualSetting::Instance()->Load_MotorSetting();
	CMotorManualSetting::Instance()->Load_ManualSetting();
	CIOListSetting::Instance()->LoadIOList();
	CAlarmListSetting::Instance()->LoadAlarmList();
	/////////////////////////////////////////////////////////////
	// PLC CREATE
	CDataManager::Instance()->CreateDevice_PLC_1();
	CDataManager::Instance()->CreateDevice_PLC_2();
	CDataManager::Instance()->CreateDevice_PLC_3();
	/////////////////////////////////////////////////////////////
	CAMKOR_MMIDlg dlg;
	m_pMainWnd = &dlg;
	INT_PTR nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: 여기에 [확인]을 클릭하여 대화 상자가 없어질 때 처리할
		//  코드를 배치합니다.
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: 여기에 [취소]를 클릭하여 대화 상자가 없어질 때 처리할
		//  코드를 배치합니다.
	}
	else if (nResponse == -1)
	{
		TRACE(traceAppMsg, 0, "경고: 대화 상자를 만들지 못했으므로 응용 프로그램이 예기치 않게 종료됩니다.\n");
		TRACE(traceAppMsg, 0, "경고: 대화 상자에서 MFC 컨트롤을 사용하는 경우 #define _AFX_NO_MFC_CONTROLS_IN_DIALOGS를 수행할 수 없습니다.\n");
	}

	// 위에서 만든 셸 관리자를 삭제합니다.
	if (pShellManager != NULL)
	{
		delete pShellManager;
	}

	/////////////////////////////////////////////////////////////
	CDataManager::Instance()->CloseDevice_PLC_1();
	CDataManager::Instance()->DestroyDevice_PLC_1();
	CDataManager::Instance()->CloseDevice_PLC_2();
	CDataManager::Instance()->DestroyDevice_PLC_2();
	CDataManager::Instance()->CloseDevice_PLC_3();
	CDataManager::Instance()->DestroyDevice_PLC_3();
	/////////////////////////////////////////////////////////////

	// 대화 상자가 닫혔으므로 응용 프로그램의 메시지 펌프를 시작하지 않고  응용 프로그램을 끝낼 수 있도록 FALSE를
	// 반환합니다.
	return FALSE;
}

void CAMKOR_MMIApp::Unicode2MBCS(LPWSTR lpData,LPSTR lpRtd)
{
	memset(lpRtd,0x00,wcslen(lpData)*2+1);
	WideCharToMultiByte(949,0,lpData,-1,lpRtd,wcslen(lpData)*2,NULL,NULL);
	return;
}

void CAMKOR_MMIApp::MBCS2Unicode(LPCSTR lpData,LPWSTR ReturnData)
{
	memset(ReturnData,0,(strlen(lpData)/2)+5);
	MultiByteToWideChar(949,0,lpData,-1,ReturnData,strlen(lpData));
	return;
}