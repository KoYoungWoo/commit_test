#pragma once

// CConnectSocket command target

class CConnectSocket : public CSocket
{
public:
	CConnectSocket();
	virtual ~CConnectSocket();
	virtual void OnClose(int nErrorCode);
	virtual void OnReceive(int nErrorCode);

	void SetConnectInfo(CString strIP, UINT uiPort);
	BOOL SendData(CString strData);
private:
	CString m_strIP;
	UINT m_uiPort;
};

