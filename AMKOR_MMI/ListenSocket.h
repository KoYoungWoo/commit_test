#pragma once

// CListenSocket command target


class CListenSocket : public CAsyncSocket
{
public:
	CListenSocket();
	virtual ~CListenSocket();

	

	CPtrList		m_ptrClientSocketList;
	virtual void	OnAccept(int nErrorCode);
	void			CloseClientSocket(CSocket* pClient);
	
	void			SendDataAll(TCHAR* pszMessage);
	int				SendData(CString strIP, CString str);

	BOOL m_bDestroy;

};


