
// AMKOR_MMIDlg.h : 헤더 파일
//

#pragma once
#include "afxwin.h"
#include "UIExt/MatrixStatic.h"
#include "UIExt/CxStatic.h"
#include "UIExt/IconButton.h"
#include "UIExt/RoundButton.h"
#include "UIExt/XPGroupBox.h"

#include "Dlg_Main_SystemInfo.h"
#include "Dlg_Gem.h"
#include "Dlg_Motor.h"
#include "Dlg_MotorStacker.h"
#include "Dlg_Manual.h"
#include "Dlg_Device.h"
#include "Dlg_IO.h"
#include "Dlg_Alarm.h"
#include "Dlg_Config.h"

#include "ListenSocket.h"
#include "ConnectSocket.h"

// CAMKOR_MMIDlg 대화 상자
class CAMKOR_MMIDlg : public CDialogEx
{
// 생성입니다.
public:
	CAMKOR_MMIDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.

// 대화 상자 데이터입니다.
	enum { IDD = IDD_AMKOR_MMI_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.


// 구현입니다.
protected:
	HICON m_hIcon;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnDestroy();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	virtual BOOL PreTranslateMessage(MSG* pMsg);


	void Cal_Area();
	void Cal_CtrlArea();
	void Cal_CtrlArea_Top();
	void Cal_CtrlArea_Bottom();
	void Cal_CtrlArea_MidLeft();
	void Cal_CtrlArea_MidRight();

	CRect m_rcTop;
	CRect m_rcBottom;
	CRect m_rcMiddle;
	CRect m_rcMidLeft;
	CRect m_rcMidRight;
	CRect m_rcMenuDlgArea;
	CRect m_rcConnBtn[7];

	BOOL m_bCtrl_FirstLoad;

	int m_nCurScreen;	// 0: Main, 1: Device, 2: Motor, 3: Manual, 4: i/o, 5: error
	void ChangeScreen(int nCurScreen);
	void ShowMainScreen(BOOL bShow);
	int m_nCurSubScreen;	// 0: System, 1: Gem
	void ChangeSubScreen(int nCurSubScreen);
	void SetViewConnButton();
	void UpdateViewConnStatus();

	// 년월일시 저장 -> WORK INFO 리셋하기 위함
	int m_nYear, m_nMonth, m_nDay, m_nHour;

	// 권한 번호
	int m_nAuthNumber; // 0-Operator, 1-Engineer, 2-Developer

	// Top Area
	CMatrixStatic m_st_Date;
	CMatrixStatic m_st_Time;
	CxStatic m_st_ErrorColor;
	CIconButton m_btn_ErrorCode;
	CIconButton m_btn_ErrorText;
	CIconButton m_btn_Exit;
	CIconButton m_btn_DeviceName;
	CIconButton m_btn_ModelName;
	CIconButton m_btn_Auth;

	// Bottom Area
	CIconButton m_btn_AutoMode;
	CIconButton m_btn_ManualMode;
	CIconButton m_btn_1;
	CIconButton m_btn_2;
	CIconButton m_btn_3;
	CIconButton m_btn_4;
	CIconButton m_btn_5;
	CIconButton m_btn_6;

	// Mid Left Area
	CXPGroupBox m_gb_1;
	CxStatic m_st_T1;
	CxStatic m_st_T2;
	CxStatic m_st_T3;
	CxStatic m_st_T4;
	CxStatic m_st_V1;
	CxStatic m_st_V2;
	CxStatic m_st_V3;
	CxStatic m_st_V4;
	CIconButton m_btn_ResetCount;

	CXPGroupBox m_gb_2;
	CIconButton m_btn_Start;
	CIconButton m_btn_Stop;
	CIconButton m_btn_Reset;
	CIconButton m_btn_BuzOff;
	CIconButton m_btn_Home;

	// Mid Right Area
	CIconButton m_btn_SystemInfo;
	CIconButton m_btn_Gem;

	CIconButton m_btn_Conn_1;
	CIconButton m_btn_Conn_2;
	CIconButton m_btn_Conn_3;
	CIconButton m_btn_Conn_4;
	CIconButton m_btn_Conn_5;
	CIconButton m_btn_Conn_6;
	CIconButton m_btn_Conn_7;

	// Main Dlg List
	CDlg_Main_SystemInfo m_dlg_MainSystemInfo;
	CDlg_Gem m_dlg_Gem;

	// Menu Dlg List
	CDlg_Motor m_dlg_Motor;
	CDlg_MotorStacker m_dlg_MotorStacker;
	CDlg_Manual m_dlg_Manual;
	CDlg_Device m_dlg_Device;
	CDlg_IO m_dlg_IO;
	CDlg_Alarm m_dlg_Alarm;
	CDlg_Config m_dlg_Config;

	CListenSocket	m_ListenSocket;
	CConnectSocket  m_ZPLPrinterSocket[5];

	void SocketServerStart();
	void SocketServerStop();

	// ADO DB
	CStringA m_strConnect_WorkInfo;
	CString GetExecuteDirectory();
	CStringA GetExecuteDirectoryA();
	void SetADOSetting();
	BOOL DB_GET_WORKINFO();
	BOOL DB_UPDATE_WORKINFO();
	BOOL DB_CREATE_LOTINFO(stLotInfo &LotInfo);			// LOT 생성시 발생
	BOOL DB_UPDATE_LOTINFO(stLotInfo &LotInfo);			// LOT 업데이트시 발생
	BOOL DB_UPDATE_LOTINFO_BOX(stLotInfo &LotInfo);		// BOX 변동시 발생
	BOOL DB_UPDATE_LOTINFO_PROCESS(stLotInfo &LotInfo);	// 프로세스 변동시 발생
	BOOL DB_UPDATE_LOTINFO_BUNDLE(stLotInfo &LotInfo);	// 번들 추가시 발생
	BOOL DB_UPDATE_LOTINFO_LAST(stLotInfo &LotInfo);	// LOT 배출시 발생

	// MES Process 체크
	void MES_LotProcessCheck();
	// MES Alarm Check
	SHORT m_sAlarmCode_G1[8];
	SHORT m_sAlarmCode_G2[8];
	SHORT m_sAlarmCode_G3[8];
	void MES_AlarmCheck();
	// Address Parse
	void Parse_DAddress_G1();
	void Parse_DAddress_G2();
	void Parse_DAddress_G3();
	// Update Main UI
	void Update_UI();
	void Update_UI_WorkInfo();

public:
	///////////////////////////////////////////////////////////////////////////////////
	// PLC 관련 쓰레드 및 함수 ////////////////////////////////////////////////////////
	BOOL StartPLCReadThread();
	void StopPLCReadThread();
	static unsigned int __stdcall _OnPLCReadThread(void* pParam);

	volatile BOOL m_bTerminatePLCReadThread;
	HANDLE m_hPLCReadThread;

	void OnPLCReadThread();
	///////////////////////////////////////////////////////////////////////////////////

	// CAM CONTROL
	stSendCamTrigger m_stSendCamTrigger[7];
	BOOL Send_CamTrigger(int nPos, int nSubPos);
	BOOL Clear_CamTriggerBit(int nPos, int nSubPos);

	// 폴더 및 파일 제거
	BOOL RemoveFolderFile(LPCTSTR strPath);
	BOOL RemoveFolderFileByElapsedDays(LPCTSTR strPath, int nDays);

	afx_msg void OnBnClickedBtnM1();
	afx_msg void OnBnClickedBtnM2();
	afx_msg void OnBnClickedBtnM3();
	afx_msg void OnBnClickedBtnM4();
	afx_msg void OnBnClickedBtnM5();
	afx_msg void OnBnClickedBtnM6();

	afx_msg LRESULT OnScreenChange(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnConnUpdate(WPARAM wParam, LPARAM lParam);	
	afx_msg void OnBnClickedBtnMExit();
	afx_msg void OnBnClickedBtnMAuth();
	afx_msg void OnBnClickedBtnMSysinfo();
	afx_msg void OnBnClickedBtnMGem();
	afx_msg void OnBnClickedBtnMBuzzeroff();
	afx_msg void OnBnClickedBtnMHome();
	afx_msg void OnBnClickedBtnMResetcount();

	afx_msg void OnBnClickedBtnMAutomode();
	afx_msg void OnBnClickedBtnMManualmode();

	virtual BOOL OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult);

	afx_msg void OnBnClickedBtnMConn1();
	afx_msg void OnBnClickedBtnMConn2();
	afx_msg void OnBnClickedBtnMConn3();
	afx_msg void OnBnClickedBtnErrorcode();
	afx_msg void OnBnClickedBtnErrortext();
	afx_msg void OnClose();
};
