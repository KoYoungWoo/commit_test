#pragma once
#include "afxwin.h"
#include "UIExt/MatrixStatic.h"
#include "UIExt/CxStatic.h"
#include "UIExt/IconButton.h"
#include "UIExt/XPGroupBox.h"
#include "GC/gridctrl.h"

// CDlg_Motor 대화 상자입니다.

class CDlg_Motor : public CDialogEx
{
	DECLARE_DYNAMIC(CDlg_Motor)

public:
	CDlg_Motor(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CDlg_Motor();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DLG_MOTOR };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);

	void Cal_Area();
	void Cal_CtrlArea();
	void Cal_CtrlArea_Left();
	void Cal_CtrlArea_Right();

	bool Init_GC_1(int nRows, int nCols);
	afx_msg void OnGridClick_GC_1(NMHDR *pNotifyStruct, LRESULT* pResult);
	afx_msg void OnEndLabel_GC_1(NMHDR *pNotifyStruct, LRESULT* pResult);
	
	void SetMotorSettingValue(int nMotorIndex);
	void Update_SelItemIndex(int nItemIndex);
	void Update_PLC_Value();
	void Update_UI();

	CRect m_rcLeft;
	CRect m_rcRight;

	BOOL m_bCtrl_FirstLoad;

	int m_nCurMotorNum;
	int m_nCurMotor_StartAddressD;
	int m_nCurMotor_StartAddressM;
	int m_nCurMotor_StartAddressL;
	int m_nCurSelMotorItemIndex;

	// Left Area
	CxStatic m_st_MotorName;
	CIconButton m_btn_1;
	CIconButton m_btn_2;
	CIconButton m_btn_6;
	CXPGroupBox m_gb_1;
	CGridCtrl m_GC_1;
	// Right Area
	CXPGroupBox m_gb_2;
	CxStatic m_st_T1;
	CxStatic m_st_T2;
	CxStatic m_st_T3;
	CxStatic m_st_T4;
	CxStatic m_st_T5;
	CxStatic m_st_T6;
	CxStatic m_st_T7;
	CxStatic m_st_T8;

	CXPGroupBox m_gb_3;
	CxStatic m_st_T9;
	CxStatic m_st_T10;
	CIconButton m_btn_3;

	CXPGroupBox m_gb_4;
	CxStatic m_st_T11;
	CxStatic m_st_T12;
	CxStatic m_st_T13;
	CxStatic m_st_T14;
	CxStatic m_st_T15;
	CIconButton m_btn_4;
	CIconButton m_btn_5;
	CIconButton m_btn_18;
	CIconButton m_btn_7;
	CIconButton m_btn_8;
	CIconButton m_btn_9;
	CIconButton m_btn_10;
	CIconButton m_btn_11;
	CIconButton m_btn_12;
	CIconButton m_btn_13;
	CIconButton m_btn_14;
	CIconButton m_btn_15;

	CIconButton m_btn_16;
	CIconButton m_btn_17;

	afx_msg void OnBnClickedBtnMt1();
	afx_msg void OnBnClickedBtnMt2();
	afx_msg void OnBnClickedBtnMt3();
	afx_msg void OnBnClickedBtnMt4();
	afx_msg void OnBnClickedBtnMt5();
	afx_msg void OnBnClickedBtnMt6();
	afx_msg void OnBnClickedBtnMt7();
	afx_msg void OnBnClickedBtnMt8();
	afx_msg void OnBnClickedBtnMt9();
	afx_msg void OnBnClickedBtnMt10();
	afx_msg void OnBnClickedBtnMt11();
	afx_msg void OnBnClickedBtnMt12();
	afx_msg void OnBnClickedBtnMt13();
	afx_msg void OnBnClickedBtnMt14();
	afx_msg void OnBnClickedBtnMt15();
	afx_msg void OnBnClickedBtnMt16();
	afx_msg void OnBnClickedBtnMt17();
	afx_msg void OnBnClickedBtnMt18();
	virtual BOOL OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult);
};
