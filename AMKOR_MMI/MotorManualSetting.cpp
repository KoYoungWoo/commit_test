// MotorManualSetting.cpp: implementation of the CAppSetting class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "MotorManualSetting.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// CAppSettingGC
class CMotorManualSettingGC
{
public:
	CMotorManualSettingGC() {}
	~CMotorManualSettingGC() { delete CMotorManualSetting::m_pThis; CMotorManualSetting::m_pThis = NULL; }
};
CMotorManualSettingGC s_GC;

//////////////////////////////////////////////////////////////////////////
// CAppSetting
CMotorManualSetting* CMotorManualSetting::m_pThis = NULL;
CMotorManualSetting::CMotorManualSetting()
{
	m_stMMInfo.m_vecMotorInfo.clear();
	m_stMMInfo.m_vecManualInfo.clear();
}

CMotorManualSetting::~CMotorManualSetting()
{
}

CMotorManualSetting* CMotorManualSetting::Instance()
{
	if( !m_pThis )
		m_pThis = new CMotorManualSetting;
	return m_pThis;
}

// 현재 실행화일 경로 가져오자.
CString CMotorManualSetting::GetExecuteDirectory()
{
	CString strFolderPath;

	// 현재 프로그램의 실행 파일이 있는 폴더명을 추출함
	::GetModuleFileName(NULL, strFolderPath.GetBuffer(MAX_PATH), MAX_PATH);
	strFolderPath.ReleaseBuffer();
	if (strFolderPath.Find('\\') != -1)
	{
		for (int i = strFolderPath.GetLength() - 1; i >= 0; i--) 
		{
			TCHAR ch = strFolderPath[i];
			strFolderPath.Delete(i);
			if (ch == '\\') break; 
		}
	}
	return strFolderPath;
}

void CMotorManualSetting::GetFileList()
{
	m_vecFileList.clear();

	CFileFind fileFind;
	CString strPath = GetExecuteDirectory();
	strPath += _T("\\*.ini");

	// 이곳에서 파일 리스트를 검색할 경로를 지정
	BOOL result = fileFind.FindFile(strPath);
	if (result)
	{
		while(fileFind.FindNextFile())
		{
			m_vecFileList.push_back(fileFind.GetFileName());
		}
		//  while문에서 다음 값이 널일 경우 마지막 파일일 경우 다음 파일이 없기 때문에
		// 마지막 파일명은 출력이 안되므로 while문 종료후에 현재 파일 명을 출력한다
		m_vecFileList.push_back(fileFind.GetFileName());
	}
	else
	{
		GetLog()->Debug(_T("CMotorDeviceSetting - ini 파일을 찾을수 없습니다..."));
	}
 
	// 파일 리스트를 닫는다.
	fileFind.Close();
}

BOOL CMotorManualSetting::Load_MotorSetting()
{
	m_stMMInfo.m_vecMotorInfo.clear();
	
	CString strPath = GetExecuteDirectory();
	strPath += _T("\\Setting\\");
	strPath += _T("Motor.ini");

	TCHAR temp[MAX_PATH];
	CString strMain, strSub;
	CString strAddressDML;
	int nAddress = 0;

	::GetPrivateProfileString( _T("MOTOR_BASIC"), _T("MOTOR_COUNT"), _T("0"), temp, MAX_PATH, strPath );
	m_stMMInfo.m_nMotorCount = _tcstoul( temp, NULL, 10 );
	::GetPrivateProfileString( _T("MOTOR_BASIC"), _T("DADDRESS_START"), _T("0"), temp, MAX_PATH, strPath );
	m_stMMInfo.m_nMotorDAddress_Start = _tcstoul( temp, NULL, 10 );
	::GetPrivateProfileString( _T("MOTOR_BASIC"), _T("DADDRESS_END"), _T("0"), temp, MAX_PATH, strPath );
	m_stMMInfo.m_nMotorDAddress_End = _tcstoul( temp, NULL, 10 );
	::GetPrivateProfileString( _T("MOTOR_BASIC"), _T("LADDRESS_START"), _T("0"), temp, MAX_PATH, strPath );
	m_stMMInfo.m_nMotorLAddress_Start = _tcstoul( temp, NULL, 10 );
	::GetPrivateProfileString( _T("MOTOR_BASIC"), _T("LADDRESS_END"), _T("0"), temp, MAX_PATH, strPath );
	m_stMMInfo.m_nMotorLAddress_End = _tcstoul( temp, NULL, 10 );

	m_stMMInfo.m_vecMotorInfo.resize(m_stMMInfo.m_nMotorCount);

	for(int i=0;i<m_stMMInfo.m_nMotorCount;i++)
	{
		strSub.Format(_T("NAME%02d"),i+1);
		::GetPrivateProfileString( _T("MOTOR_NAME"), strSub, _T(""), temp, MAX_PATH, strPath );
		m_stMMInfo.m_vecMotorInfo[i].m_strMotorName.Format(_T("%s"),temp);

		strSub.Format(_T("ADDRESS%02d"),i+1);
		::GetPrivateProfileString( _T("MOTOR_ADDRESS_D"), strSub, _T("0"), temp, MAX_PATH, strPath );
		m_stMMInfo.m_vecMotorInfo[i].m_nStartAddressD = _wtoi(temp);

		strSub.Format(_T("ADDRESS%02d"),i+1);
		::GetPrivateProfileString( _T("MOTOR_ADDRESS_M"), strSub, _T("0"), temp, MAX_PATH, strPath );
		m_stMMInfo.m_vecMotorInfo[i].m_nStartAddressM = _wtoi(temp);

		strSub.Format(_T("ADDRESS%02d"),i+1);
		::GetPrivateProfileString( _T("MOTOR_ADDRESS_L"), strSub, _T("0"), temp, MAX_PATH, strPath );
		m_stMMInfo.m_vecMotorInfo[i].m_nStartAddressL = _wtoi(temp);


		for(int j=0;j<20;j++)
		{
			strMain.Format(_T("MOTORITEM_NAME_%02d"),i+1);
			strSub.Format(_T("NAME%02d"),j+1);
			::GetPrivateProfileString( strMain, strSub, _T(""), temp, MAX_PATH, strPath );
			m_stMMInfo.m_vecMotorInfo[i].m_stMotorItem[j].m_strItemName.Format(_T("%s"),temp);

			strMain.Format(_T("MOTORITEM_POS_%02d"),i+1);
			strSub.Format(_T("POS%02d"),j+1);
			::GetPrivateProfileString( strMain, strSub, _T("0.0"), temp, MAX_PATH, strPath );
			m_stMMInfo.m_vecMotorInfo[i].m_stMotorItem[j].m_dItemPostion = (double)_wtof(temp);

			strMain.Format(_T("MOTORITEM_SPEED_%02d"),i+1);
			strSub.Format(_T("SPEED%02d"),j+1);
			::GetPrivateProfileString( strMain, strSub, _T("0"), temp, MAX_PATH, strPath );
			m_stMMInfo.m_vecMotorInfo[i].m_stMotorItem[j].m_dItemSpeed = _tcstoul( temp, NULL, 10 );

			strMain.Format(_T("MOTORITEM_ADDRESS_POS_%02d"),i+1);
			strSub.Format(_T("ADDRESS%02d"),j+1);
			::GetPrivateProfileString( strMain, strSub, _T("0"), temp, MAX_PATH, strPath );
			nAddress = _tstoi( temp );
			if(nAddress != 0) m_stMMInfo.m_vecMotorInfo[i].m_stMotorItem[j].m_nAddress_Pos = _tstoi( temp );
			else m_stMMInfo.m_vecMotorInfo[i].m_stMotorItem[j].m_nAddress_Pos = m_stMMInfo.m_vecMotorInfo[i].m_nStartAddressD + 22 + j*4;

			strMain.Format(_T("MOTORITEM_ADDRESS_SPD_%02d"),i+1);
			strSub.Format(_T("ADDRESS%02d"),j+1);
			::GetPrivateProfileString( strMain, strSub, _T("0"), temp, MAX_PATH, strPath );
			nAddress = _tstoi( temp );
			if(nAddress != 0) m_stMMInfo.m_vecMotorInfo[i].m_stMotorItem[j].m_nAddress_Spd = _tstoi( temp );
			else m_stMMInfo.m_vecMotorInfo[i].m_stMotorItem[j].m_nAddress_Spd = m_stMMInfo.m_vecMotorInfo[i].m_nStartAddressD + 20 + j*4;
		}
	}

	return TRUE;
}

BOOL CMotorManualSetting::Save_MotorSetting()
{
	CString strPath = GetExecuteDirectory();
	strPath += _T("\\Setting\\");
	strPath += _T("Motor.ini");

	CString strTemp;
	CString strMain, strSub;

	strTemp.Format(_T("%d"), m_stMMInfo.m_nMotorCount);
	::WritePrivateProfileString( _T("MOTOR_BASIC"), _T("MOTOR_COUNT"), strTemp, strPath );
	strTemp.Format(_T("%d"), m_stMMInfo.m_nMotorDAddress_Start);
	::WritePrivateProfileString( _T("MOTOR_BASIC"), _T("DADDRESS_START"), strTemp, strPath );
	strTemp.Format(_T("%d"), m_stMMInfo.m_nMotorDAddress_End);
	::WritePrivateProfileString( _T("MOTOR_BASIC"), _T("DADDRESS_END"), strTemp, strPath );
	strTemp.Format(_T("%d"), m_stMMInfo.m_nMotorLAddress_Start);
	::WritePrivateProfileString( _T("MOTOR_BASIC"), _T("LADDRESS_START"), strTemp, strPath );
	strTemp.Format(_T("%d"), m_stMMInfo.m_nMotorLAddress_End);
	::WritePrivateProfileString( _T("MOTOR_BASIC"), _T("LADDRESS_END"), strTemp, strPath );

	for(size_t i=0;i<m_stMMInfo.m_vecMotorInfo.size();i++)
	{
		strSub.Format(_T("NAME%02d"),i+1);
		::WritePrivateProfileString( _T("MOTOR_NAME"), strSub, m_stMMInfo.m_vecMotorInfo[i].m_strMotorName, strPath );
	}
	for(size_t i=0;i<m_stMMInfo.m_vecMotorInfo.size();i++)
	{
		strSub.Format(_T("ADDRESS%02d"),i+1);
		strTemp.Format(_T("%d"), (int)m_stMMInfo.m_vecMotorInfo[i].m_nStartAddressD);
		::WritePrivateProfileString( _T("MOTOR_ADDRESS_D"), strSub, strTemp, strPath );
	}
	for(size_t i=0;i<m_stMMInfo.m_vecMotorInfo.size();i++)
	{
		strSub.Format(_T("ADDRESS%02d"),i+1);
		strTemp.Format(_T("%d"), (int)m_stMMInfo.m_vecMotorInfo[i].m_nStartAddressM);
		::WritePrivateProfileString( _T("MOTOR_ADDRESS_M"), strSub, strTemp, strPath );
	}
	for(size_t i=0;i<m_stMMInfo.m_vecMotorInfo.size();i++)
	{
		strSub.Format(_T("ADDRESS%02d"),i+1);
		strTemp.Format(_T("%d"), (int)m_stMMInfo.m_vecMotorInfo[i].m_nStartAddressL);
		::WritePrivateProfileString( _T("MOTOR_ADDRESS_L"), strSub, strTemp, strPath );
	}
	for(size_t i=0;i<m_stMMInfo.m_vecMotorInfo.size();i++)
	{
		for(size_t j=0;j<20;j++)
		{
			strMain.Format(_T("MOTORITEM_NAME_%02d"),i+1);
			strSub.Format(_T("NAME%02d"),j+1);
			::WritePrivateProfileString( strMain, strSub, m_stMMInfo.m_vecMotorInfo[i].m_stMotorItem[j].m_strItemName, strPath );
		}
		for(size_t j=0;j<20;j++)
		{
			strMain.Format(_T("MOTORITEM_POS_%02d"),i+1);
			strSub.Format(_T("POS%02d"),j+1);
			strTemp.Format(_T("%f"), m_stMMInfo.m_vecMotorInfo[i].m_stMotorItem[j].m_dItemPostion);
			::WritePrivateProfileString( strMain, strSub, strTemp, strPath );
		}
		for(size_t j=0;j<20;j++)
		{
			strMain.Format(_T("MOTORITEM_SPEED_%02d"),i+1);
			strSub.Format(_T("SPEED%02d"),j+1);
			strTemp.Format(_T("%d"), (int)m_stMMInfo.m_vecMotorInfo[i].m_stMotorItem[j].m_dItemPostion);
			::WritePrivateProfileString( strMain, strSub, strTemp, strPath );
		}
		for(size_t j=0;j<20;j++)
		{
			strMain.Format(_T("MOTORITEM_ADDRESS_POS_%02d"),i+1);
			strSub.Format(_T("ADDRESS%02d"),j+1);
			strTemp.Format(_T("%d"), (int)m_stMMInfo.m_vecMotorInfo[i].m_stMotorItem[j].m_nAddress_Pos);
			::WritePrivateProfileString( strMain, strSub, strTemp, strPath );
		}
		for(size_t j=0;j<20;j++)
		{
			strMain.Format(_T("MOTORITEM_ADDRESS_SPD_%02d"),i+1);
			strSub.Format(_T("ADDRESS%02d"),j+1);
			strTemp.Format(_T("%d"), (int)m_stMMInfo.m_vecMotorInfo[i].m_stMotorItem[j].m_nAddress_Spd);
			::WritePrivateProfileString( strMain, strSub, strTemp, strPath );
		}
	}

	return TRUE;
}

void CMotorManualSetting::SetMotorStartDAddress(int nStartAddress)
{
	m_stMMInfo.m_nMotorDAddress_Start = nStartAddress;
}

void CMotorManualSetting::SetMotorEndDAddress(int nEndAddress)
{
	m_stMMInfo.m_nMotorDAddress_End = nEndAddress;
}

int CMotorManualSetting::GetMotorStartDAddress()
{
	return m_stMMInfo.m_nMotorDAddress_Start;
}

int CMotorManualSetting::GetMotorEndDAddress()
{
	return m_stMMInfo.m_nMotorDAddress_End;
}

void CMotorManualSetting::SetMotorStartLAddress(int nStartAddress)
{
	m_stMMInfo.m_nMotorLAddress_Start = nStartAddress;
}

void CMotorManualSetting::SetMotorEndLAddress(int nEndAddress)
{
	m_stMMInfo.m_nMotorLAddress_End = nEndAddress;
}

int CMotorManualSetting::GetMotorStartLAddress()
{
	return m_stMMInfo.m_nMotorLAddress_Start;
}

int CMotorManualSetting::GetMotorEndLAddress()
{
	return m_stMMInfo.m_nMotorLAddress_End;
}

void CMotorManualSetting::AddMotorInfo(int nAddressD, int nAddressM, int nAddressL)
{
	m_stMMInfo.m_nMotorCount ++;
	if(m_stMMInfo.m_nMotorCount > (int)m_stMMInfo.m_vecMotorInfo.size())
	{
		stMotorInfo stMI;
		stMI.Clear();
		stMI.m_strMotorName.Format(_T("Motor #%02d"),m_stMMInfo.m_nMotorCount);
		stMI.m_nStartAddressD = nAddressD;
		stMI.m_nStartAddressM = nAddressM;
		stMI.m_nStartAddressL = nAddressL;
		m_stMMInfo.m_vecMotorInfo.push_back(stMI);
	}
	else
	{
		m_stMMInfo.m_vecMotorInfo[m_stMMInfo.m_nMotorCount - 1].Clear();
		m_stMMInfo.m_vecMotorInfo[m_stMMInfo.m_nMotorCount - 1].m_strMotorName.Format(_T("Motor #%02d"),m_stMMInfo.m_nMotorCount);
	}
}

void CMotorManualSetting::DelMotorInfo()
{
	m_stMMInfo.m_vecMotorInfo[m_stMMInfo.m_nMotorCount - 1].Clear();
	m_stMMInfo.m_nMotorCount --;
}

void CMotorManualSetting::SetMotorItemValue(int nMotorIndex, int nItemIndex, double dPos, double dSpd)
{
	if(nMotorIndex > m_stMMInfo.m_nMotorCount-1) return;
	if(nItemIndex < 0 || nItemIndex > 19) return;
	m_stMMInfo.m_vecMotorInfo[nMotorIndex].m_stMotorItem[nItemIndex].m_dItemPostion = dPos;
	m_stMMInfo.m_vecMotorInfo[nMotorIndex].m_stMotorItem[nItemIndex].m_dItemSpeed = dSpd;
}

void CMotorManualSetting::SetMotorItemText(int nMotorIndex, int nItemIndex, LPCTSTR strText)
{
	if(nMotorIndex > m_stMMInfo.m_nMotorCount-1) return;
	if(nItemIndex < 0 || nItemIndex > 19) return;
	m_stMMInfo.m_vecMotorInfo[nMotorIndex].m_stMotorItem[nItemIndex].m_strItemName = strText;
}

void CMotorManualSetting::SetMotorItemAddress_Spd(int nMotorIndex, int nItemIndex, int nAddress)
{
	if(nMotorIndex > m_stMMInfo.m_nMotorCount-1) return;
	if(nItemIndex < 0 || nItemIndex > 19) return;
	m_stMMInfo.m_vecMotorInfo[nMotorIndex].m_stMotorItem[nItemIndex].m_nAddress_Spd = nAddress;
}

void CMotorManualSetting::SetMotorItemAddress_Pos(int nMotorIndex, int nItemIndex, int nAddress)
{
	if(nMotorIndex > m_stMMInfo.m_nMotorCount-1) return;
	if(nItemIndex < 0 || nItemIndex > 19) return;
	m_stMMInfo.m_vecMotorInfo[nMotorIndex].m_stMotorItem[nItemIndex].m_nAddress_Pos = nAddress;
}

BOOL CMotorManualSetting::Load_ManualSetting()
{
	m_stMMInfo.m_vecManualInfo.clear();

	CString strPath = GetExecuteDirectory();
	strPath += _T("\\Setting\\");
	strPath += _T("Manual.ini");

	TCHAR temp[MAX_PATH];
	CString strMain, strSub;
	CString strAddress, strBit, strLamp;

	::GetPrivateProfileString( _T("MANUAL_ADDRESS"), _T("LAMP_START_ADDRESS"), _T("0"), temp, MAX_PATH, strPath );
	m_stMMInfo.m_nManualLampStartAddress = _tcstoul( temp, NULL, 10 );
	::GetPrivateProfileString( _T("MANUAL_ADDRESS"), _T("LAMP_END_ADDRESS"), _T("0"), temp, MAX_PATH, strPath );
	m_stMMInfo.m_nManualLampEndAddress = _tcstoul( temp, NULL, 10 );

	::GetPrivateProfileString( _T("MANUAL_BASIC"), _T("MANUAL_COUNT"), _T("0"), temp, MAX_PATH, strPath );
	m_stMMInfo.m_nManualCount = _tcstoul( temp, NULL, 10 );

	m_stMMInfo.m_vecManualInfo.resize(m_stMMInfo.m_nManualCount);

	for(size_t i=0;i<m_stMMInfo.m_vecManualInfo.size();i++)
	{
		strSub.Format(_T("NAME%02d"),i+1);
		::GetPrivateProfileString( _T("MANUAL_NAME"), strSub, _T(""), temp, MAX_PATH, strPath );
		m_stMMInfo.m_vecManualInfo[i].m_strManualName.Format(_T("%s"),temp);

		for(size_t j=0;j<50;j++)
		{
			strMain.Format(_T("MANUALITEM_NAME_%02d"),i+1);
			strSub.Format(_T("NAME%02d"),j+1);
			::GetPrivateProfileString( strMain, strSub, _T(""), temp, MAX_PATH, strPath );
			m_stMMInfo.m_vecManualInfo[i].m_stManualItem[j].m_strItemName.Format(_T("%s"),temp);

			strMain.Format(_T("MANUALITEM_ADDRESS_%02d"),i+1);
			strSub.Format(_T("ADDRESS%02d"),j+1);
			::GetPrivateProfileString( strMain, strSub, _T(""), temp, MAX_PATH, strPath );

			strAddress.Format(_T("%s"), temp);
			strBit = strAddress.Left(strAddress.Find(_T(",")));
			strLamp = strAddress.Right(strAddress.Find(_T(",")));

			m_stMMInfo.m_vecManualInfo[i].m_stManualItem[j].m_nAddress_Bit = _wtoi(strBit);
			m_stMMInfo.m_vecManualInfo[i].m_stManualItem[j].m_nAddress_Lamp = _wtoi(strLamp);
			m_stMMInfo.m_vecManualInfo[i].m_stManualItem[j].m_nLampOnOff = 0;
		}
	}

	return TRUE;
}

BOOL CMotorManualSetting::Save_ManualSetting()
{
	CString strPath = GetExecuteDirectory();
	strPath += _T("\\Setting\\");
	strPath += _T("Manual.ini");

	CString strTemp;
	CString strMain, strSub;

	strTemp.Format(_T("%d"), m_stMMInfo.m_nManualLampStartAddress);
	::WritePrivateProfileString( _T("MANUAL_ADDRESS"), _T("LAMP_START_ADDRESS"), strTemp, strPath );
	strTemp.Format(_T("%d"), m_stMMInfo.m_nManualLampEndAddress);
	::WritePrivateProfileString( _T("MANUAL_ADDRESS"), _T("LAMP_END_ADDRESS"), strTemp, strPath );

	strTemp.Format(_T("%d"), m_stMMInfo.m_nManualCount);
	::WritePrivateProfileString( _T("MANUAL_BASIC"), _T("MANUAL_COUNT"), strTemp, strPath );

	for(int i=0;i<m_stMMInfo.m_nManualCount;i++)
	{
		strSub.Format(_T("NAME%02d"),i+1);
		::WritePrivateProfileString( _T("MANUAL_NAME"), strSub, m_stMMInfo.m_vecManualInfo[i].m_strManualName, strPath );
	}
	for(int i=0;i<m_stMMInfo.m_nManualCount;i++)
	{
		for(int j=0;j<50;j++)
		{
			strMain.Format(_T("MANUALITEM_NAME_%02d"),i+1);
			strSub.Format(_T("NAME%02d"),j+1);
			::WritePrivateProfileString( strMain, strSub, m_stMMInfo.m_vecManualInfo[i].m_stManualItem[j].m_strItemName, strPath );
		}
		for(int j=0;j<50;j++)
		{
			strMain.Format(_T("MANUALITEM_ADDRESS_%02d"),i+1);
			strSub.Format(_T("ADDRESS%02d"),j+1);
			strTemp.Format(_T("%d,%d"), m_stMMInfo.m_vecManualInfo[i].m_stManualItem[j].m_nAddress_Bit, m_stMMInfo.m_vecManualInfo[i].m_stManualItem[j].m_nAddress_Lamp);
			::WritePrivateProfileString( strMain, strSub, strTemp, strPath );
		}
	}

	return TRUE;
}

void CMotorManualSetting::SetManualStartLAddress(int nStartAddress)
{
	m_stMMInfo.m_nManualLampStartAddress = nStartAddress;
}

void CMotorManualSetting::SetManualEndLAddress(int nEndAddress)
{
	m_stMMInfo.m_nManualLampEndAddress = nEndAddress;
}

int CMotorManualSetting::GetManualStartLAddress()
{
	return m_stMMInfo.m_nManualLampStartAddress;
}

int CMotorManualSetting::GetManualEndLAddress()
{
	return m_stMMInfo.m_nManualLampEndAddress;
}

void CMotorManualSetting::SetManualItemStatus(int nManualIndex, int nItemIndex, int nLampOnOff)
{
	if(nManualIndex == 0) return;
	if(nManualIndex > m_stMMInfo.m_nManualCount) return;
	if(nItemIndex <= 0 || nItemIndex > 50) return;

	m_stMMInfo.m_vecManualInfo[nManualIndex-1].m_stManualItem[nItemIndex-1].m_nLampOnOff = nLampOnOff;
}

void CMotorManualSetting::SetManualItemModify(int nManualIndex, int nItemIndex, LPCTSTR strText, int nBitAddress, int nLampAddress)
{
	if(nManualIndex == 0) return;
	if(nManualIndex > m_stMMInfo.m_nManualCount) return;
	if(nItemIndex <= 0 || nItemIndex > 50) return;

	m_stMMInfo.m_vecManualInfo[nManualIndex-1].m_stManualItem[nItemIndex-1].m_strItemName.Format(_T("%s"), strText);
	m_stMMInfo.m_vecManualInfo[nManualIndex-1].m_stManualItem[nItemIndex-1].m_nAddress_Bit = nBitAddress;
	m_stMMInfo.m_vecManualInfo[nManualIndex-1].m_stManualItem[nItemIndex-1].m_nAddress_Lamp = nLampAddress;
}

void CMotorManualSetting::AddManualInfo(LPCTSTR strManualName)
{
	stManualInfo stMI;
	stMI.Clear();
	stMI.m_strManualName = strManualName;

	m_stMMInfo.m_nManualCount ++;
	m_stMMInfo.m_vecManualInfo.push_back(stMI);
}

void CMotorManualSetting::AddManualInfo()
{
	m_stMMInfo.m_nManualCount ++;
	if(m_stMMInfo.m_nManualCount > (int)m_stMMInfo.m_vecManualInfo.size())
	{
		stManualInfo stMI;
		stMI.Clear();
		stMI.m_strManualName.Format(_T("ManualName #%02d"),m_stMMInfo.m_nManualCount);
		m_stMMInfo.m_vecManualInfo.push_back(stMI);
	}
	else
	{
		m_stMMInfo.m_vecManualInfo[m_stMMInfo.m_nManualCount - 1].Clear();
		m_stMMInfo.m_vecManualInfo[m_stMMInfo.m_nManualCount - 1].m_strManualName.Format(_T("ManualName #%02d"),m_stMMInfo.m_nManualCount);
	}
}

void CMotorManualSetting::DelManualInfo()
{
	m_stMMInfo.m_vecManualInfo[m_stMMInfo.m_nManualCount - 1].Clear();
	m_stMMInfo.m_nManualCount --;
}

void CMotorManualSetting::SetManualNameModify(int nManualIndex, LPCTSTR strManualName)
{
	if(nManualIndex == 0) return;
	if(nManualIndex > m_stMMInfo.m_nManualCount) return;

	m_stMMInfo.m_vecManualInfo[nManualIndex-1].m_strManualName = strManualName;
}
