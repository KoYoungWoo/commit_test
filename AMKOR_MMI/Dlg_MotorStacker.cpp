// Dlg_MotorStacker.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "AMKOR_MMI.h"
#include "AMKOR_MMIDlg.h"
#include "Dlg_MotorStacker.h"
#include "Dlg_NumPad.h"
#include "afxdialogex.h"


// CDlg_MotorStacker 대화 상자입니다.

IMPLEMENT_DYNAMIC(CDlg_MotorStacker, CDialogEx)

CDlg_MotorStacker::CDlg_MotorStacker(CWnd* pParent /*=NULL*/)
	: CDialogEx(CDlg_MotorStacker::IDD, pParent)
{
	m_bCtrl_FirstLoad = FALSE;
}

CDlg_MotorStacker::~CDlg_MotorStacker()
{
}

void CDlg_MotorStacker::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_ST_MS_1, m_st_1);
	DDX_Control(pDX, IDC_ST_MS_2, m_st_2);
	DDX_Control(pDX, IDC_ST_MS_3, m_st_3);
	DDX_Control(pDX, IDC_ST_MS_4, m_st_4);
	DDX_Control(pDX, IDC_ST_MS_5, m_st_5);
	DDX_Control(pDX, IDC_ST_MS_6, m_st_6);
	DDX_Control(pDX, IDC_ST_MS_7, m_st_7);
	DDX_Control(pDX, IDC_ST_MS_8, m_st_8);
	DDX_Control(pDX, IDC_ST_MS_9, m_st_9);
	DDX_Control(pDX, IDC_ST_MS_10, m_st_10);
	DDX_Control(pDX, IDC_ST_MS_11, m_st_11);
	DDX_Control(pDX, IDC_ST_MS_12, m_st_12);
	DDX_Control(pDX, IDC_ST_MS_13, m_st_13);
	DDX_Control(pDX, IDC_ST_MS_14, m_st_14);
	DDX_Control(pDX, IDC_ST_MS_15, m_st_15);
	DDX_Control(pDX, IDC_ST_MS_16, m_st_16);
	DDX_Control(pDX, IDC_ST_MS_17, m_st_17);
	DDX_Control(pDX, IDC_ST_MS_18, m_st_18);
	DDX_Control(pDX, IDC_ST_MS_19, m_st_19);
	DDX_Control(pDX, IDC_ST_MS_20, m_st_20);
	DDX_Control(pDX, IDC_ST_MS_21, m_st_21);
	DDX_Control(pDX, IDC_ST_MS_22, m_st_22);
	DDX_Control(pDX, IDC_ST_MS_23, m_st_23);
	DDX_Control(pDX, IDC_ST_MS_24, m_st_24);
	DDX_Control(pDX, IDC_BTN_MS_1, m_btn_1);
	DDX_Control(pDX, IDC_BTN_MS_2, m_btn_2);
	DDX_Control(pDX, IDC_BTN_MS_3, m_btn_3);
	DDX_Control(pDX, IDC_BTN_MS_4, m_btn_4);
	DDX_Control(pDX, IDC_BTN_MS_5, m_btn_5);
	DDX_Control(pDX, IDC_BTN_MS_6, m_btn_6);
	DDX_Control(pDX, IDC_BTN_MS_7, m_btn_7);
	DDX_Control(pDX, IDC_BTN_MS_8, m_btn_8);
	DDX_Control(pDX, IDC_BTN_MS_9, m_btn_9);
	DDX_Control(pDX, IDC_BTN_MS_10, m_btn_10);
	DDX_Control(pDX, IDC_BTN_MS_11, m_btn_11);
	DDX_Control(pDX, IDC_BTN_MS_12, m_btn_12);
	DDX_Control(pDX, IDC_BTN_MS_13, m_btn_13);
	DDX_Control(pDX, IDC_GB_MS_1, m_gb_1);
	DDX_Control(pDX, IDC_GB_MS_2, m_gb_2);
	DDX_Control(pDX, IDC_GB_MS_3, m_gb_3);
	DDX_Control(pDX, IDC_GB_MS_4, m_gb_4);
	DDX_Control(pDX, IDC_GC_MS_1, m_GC_1);
	DDX_Control(pDX, IDC_GC_MS_2, m_GC_2);
	DDX_Control(pDX, IDC_GC_MS_3, m_GC_3);
	DDX_Control(pDX, IDC_GC_MS_4, m_GC_4);
}


BEGIN_MESSAGE_MAP(CDlg_MotorStacker, CDialogEx)
	ON_WM_ERASEBKGND()
	ON_WM_SIZE()
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BTN_MS_1, &CDlg_MotorStacker::OnBnClickedBtnMs1)
	ON_BN_CLICKED(IDC_BTN_MS_2, &CDlg_MotorStacker::OnBnClickedBtnMs2)
	ON_BN_CLICKED(IDC_BTN_MS_3, &CDlg_MotorStacker::OnBnClickedBtnMs3)
	ON_BN_CLICKED(IDC_BTN_MS_4, &CDlg_MotorStacker::OnBnClickedBtnMs4)
	ON_BN_CLICKED(IDC_BTN_MS_5, &CDlg_MotorStacker::OnBnClickedBtnMs5)
	ON_BN_CLICKED(IDC_BTN_MS_6, &CDlg_MotorStacker::OnBnClickedBtnMs6)
	ON_BN_CLICKED(IDC_BTN_MS_7, &CDlg_MotorStacker::OnBnClickedBtnMs7)
	ON_BN_CLICKED(IDC_BTN_MS_8, &CDlg_MotorStacker::OnBnClickedBtnMs8)
	ON_BN_CLICKED(IDC_BTN_MS_9, &CDlg_MotorStacker::OnBnClickedBtnMs9)
	ON_BN_CLICKED(IDC_BTN_MS_10, &CDlg_MotorStacker::OnBnClickedBtnMs10)
	ON_BN_CLICKED(IDC_BTN_MS_11, &CDlg_MotorStacker::OnBnClickedBtnMs11)
	ON_BN_CLICKED(IDC_BTN_MS_12, &CDlg_MotorStacker::OnBnClickedBtnMs12)
	ON_BN_CLICKED(IDC_BTN_MS_13, &CDlg_MotorStacker::OnBnClickedBtnMs13)
	ON_NOTIFY(NM_CLICK, IDC_GC_MS_1, OnGridClick_GC_1)
	ON_NOTIFY(GVN_ENDLABELEDIT, IDC_GC_MS_1, OnEndLabel_GC_1)
	ON_NOTIFY(NM_CLICK, IDC_GC_MS_2, OnGridClick_GC_2)
	ON_NOTIFY(GVN_ENDLABELEDIT, IDC_GC_MS_2, OnEndLabel_GC_2)
	ON_NOTIFY(NM_CLICK, IDC_GC_MS_3, OnGridClick_GC_3)
	ON_NOTIFY(GVN_ENDLABELEDIT, IDC_GC_MS_3, OnEndLabel_GC_3)
	ON_NOTIFY(NM_CLICK, IDC_GC_MS_4, OnGridClick_GC_4)
	ON_NOTIFY(GVN_ENDLABELEDIT, IDC_GC_MS_4, OnEndLabel_GC_4)
END_MESSAGE_MAP()


// CDlg_MotorStacker 메시지 처리기입니다.


BOOL CDlg_MotorStacker::OnEraseBkgnd(CDC* pDC)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	// 전체 배경색
	CRect rect;
    GetClientRect(&rect);
    CBrush myBrush(CR_MAIN_BKGND);	// dialog background color
    CBrush *pOld = pDC->SelectObject(&myBrush);
    BOOL bRes  = pDC->PatBlt(0, 0, rect.Width(), rect.Height(), PATCOPY);
    pDC->SelectObject(pOld);		// restore old brush
    return FALSE;     
	//return CDialogEx::OnEraseBkgnd(pDC);
}


void CDlg_MotorStacker::OnSize(UINT nType, int cx, int cy)
{
	CDialogEx::OnSize(nType, cx, cy);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	if(m_bCtrl_FirstLoad == TRUE) Cal_CtrlArea();
}


void CDlg_MotorStacker::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CDialogEx::OnTimer(nIDEvent);
}


BOOL CDlg_MotorStacker::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	Cal_CtrlArea();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


BOOL CDlg_MotorStacker::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message == WM_SYSKEYDOWN)
	{
		if(pMsg->wParam == VK_F4) 
			return TRUE;
	}

	if(pMsg->message == WM_KEYDOWN)
	{
		if( pMsg->wParam == VK_ESCAPE ||	
			//pMsg->wParam == VK_RETURN ||	// Grid Ctrl Text 입력시 사용하기때문에...
			pMsg->wParam == VK_SPACE  ||
			pMsg->wParam == VK_CANCEL )     
			return TRUE;        
	}

	return CDialogEx::PreTranslateMessage(pMsg);
}

void CDlg_MotorStacker::Cal_Area()
{
	CRect ClientRect;
	GetClientRect(&ClientRect);

	int n35pH = (int)((float)ClientRect.Height()*0.35f);

	m_rcUp.SetRect(ClientRect.left, ClientRect.top, ClientRect.right, ClientRect.top+n35pH);
	m_rcUp.DeflateRect(5,5,5,5);
	m_rcDown.SetRect(ClientRect.left, ClientRect.top+n35pH+CTRL_MARGIN4, ClientRect.right, ClientRect.bottom);
	m_rcDown.DeflateRect(3,0,3,3);
}

void CDlg_MotorStacker::Cal_CtrlArea()
{
	Cal_Area();

	Cal_CtrlArea_Up();
	Cal_CtrlArea_Down();

	m_bCtrl_FirstLoad = TRUE;
}

void CDlg_MotorStacker::Cal_CtrlArea_Up()
{
	CRect CalRect;
	CRect rcDivW[4],rcDiv1[5],rcDiv2[5],rcDiv3[5],rcDiv4[5];
	int nDiv6H, nDiv3W;

	int nDiv4W = (int)((float)m_rcUp.Width()/4.0f);

	rcDivW[0].SetRect(m_rcUp.left, m_rcUp.top, m_rcUp.left+nDiv4W, m_rcUp.bottom);
	rcDivW[1].SetRect(rcDivW[0].right, m_rcUp.top, rcDivW[0].right+nDiv4W, m_rcUp.bottom);
	rcDivW[2].SetRect(rcDivW[1].right, m_rcUp.top, rcDivW[1].right+nDiv4W, m_rcUp.bottom);
	rcDivW[3].SetRect(rcDivW[2].right, m_rcUp.top, m_rcUp.right, m_rcUp.bottom);
	rcDivW[0].DeflateRect(1,1,1,1);
	rcDivW[1].DeflateRect(1,1,1,1);
	rcDivW[2].DeflateRect(1,1,1,1);
	rcDivW[3].DeflateRect(1,1,1,1);

	// Servo#1_Index_X
	nDiv6H = (int)((float)rcDivW[0].Height()/6.0f);

	rcDiv1[0].SetRect(rcDivW[0].left, rcDivW[0].top, rcDivW[0].right, rcDivW[0].top+nDiv6H);
	rcDiv1[1].SetRect(rcDivW[0].left, rcDiv1[0].bottom, rcDivW[0].right, rcDiv1[0].bottom+nDiv6H);
	rcDiv1[2].SetRect(rcDivW[0].left, rcDiv1[1].bottom, rcDivW[0].right, rcDiv1[1].bottom+nDiv6H);
	rcDiv1[3].SetRect(rcDivW[0].left, rcDiv1[2].bottom, rcDivW[0].right, rcDiv1[2].bottom+nDiv6H);
	rcDiv1[4].SetRect(rcDivW[0].left, rcDiv1[3].bottom, rcDivW[0].right, rcDivW[0].bottom);

	m_st_1.SetBkColor(CR_LIGHTGREEN);
	m_st_1.SetTextColor(CR_BLACK);
	m_st_1.SetFont(CTRL_FONT,20,FW_BOLD);
	m_st_1.MoveWindow(&rcDiv1[0]);

	nDiv3W = (int)((float)rcDiv1[1].Width()/3.0f);

	CalRect.SetRect(rcDiv1[1].left, rcDiv1[1].top, rcDiv1[1].left+nDiv3W, rcDiv1[1].bottom);
	m_st_5.SetBkColor(CR_LIGHTGRAY);
	m_st_5.SetTextColor(CR_BLACK);
	m_st_5.SetFont(CTRL_FONT,20,FW_BOLD);
	m_st_5.MoveWindow(&CalRect);
	CalRect.SetRect(CalRect.right, rcDiv1[1].top, CalRect.right+nDiv3W, rcDiv1[1].bottom);
	m_st_6.SetBkColor(CR_LIGHTGRAY);
	m_st_6.SetTextColor(CR_BLACK);
	m_st_6.SetFont(CTRL_FONT,20,FW_BOLD);
	m_st_6.MoveWindow(&CalRect);
	CalRect.SetRect(CalRect.right, rcDiv1[1].top, rcDiv1[1].right, rcDiv1[1].bottom);
	m_st_7.SetBkColor(CR_LIGHTGRAY);
	m_st_7.SetTextColor(CR_BLACK);
	m_st_7.SetFont(CTRL_FONT,20,FW_BOLD);
	m_st_7.MoveWindow(&CalRect);

	m_st_17.SetBkColor(CR_BLACK);
	m_st_17.SetTextColor(CR_LIGHTGREEN);
	m_st_17.SetFont(CTRL_FONT,20,FW_BOLD);
	m_st_17.MoveWindow(&rcDiv1[2]);

	m_st_21.SetBkColor(CR_WHITE);
	m_st_21.SetTextColor(CR_BLACK);
	m_st_21.SetFont(CTRL_FONT,20,FW_BOLD);
	m_st_21.MoveWindow(&rcDiv1[3]);

	nDiv3W = (int)((float)rcDiv1[4].Width()/3.0f);

	CalRect.SetRect(rcDiv1[4].left, rcDiv1[4].top, rcDiv1[4].left+nDiv3W, rcDiv1[4].bottom);
	m_btn_1.SetFontBtn(CTRL_FONT, 22, FW_BOLD);
	m_btn_1.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_1.SetTextColor(CR_BLACK);
	m_btn_1.SetFlatType(TRUE);
	m_btn_1.MoveWindow(&CalRect);
	CalRect.SetRect(CalRect.right, rcDiv1[4].top, CalRect.right+nDiv3W, rcDiv1[4].bottom);
	m_btn_2.SetFontBtn(CTRL_FONT, 22, FW_BOLD);
	m_btn_2.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_2.SetTextColor(CR_BLACK);
	m_btn_2.SetIconPos(2);
	m_btn_2.SetIconID(IDI_RIGHT);
	m_btn_2.SetIconSize(64,64);
	m_btn_2.SetFlatType(TRUE);
	m_btn_2.MoveWindow(&CalRect);
	CalRect.SetRect(CalRect.right, rcDiv1[4].top, rcDiv1[4].right, rcDiv1[4].bottom);
	m_btn_3.SetFontBtn(CTRL_FONT, 22, FW_BOLD);
	m_btn_3.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_3.SetTextColor(CR_BLACK);
	m_btn_3.SetIconPos(2);
	m_btn_3.SetIconID(IDI_LEFT);
	m_btn_3.SetIconSize(64,64);
	m_btn_3.SetFlatType(TRUE);
	m_btn_3.MoveWindow(&CalRect);

	// Servo#2_Index_Y
	nDiv6H = (int)((float)rcDivW[1].Height()/6.0f);

	rcDiv2[0].SetRect(rcDivW[1].left, rcDivW[1].top, rcDivW[1].right, rcDivW[1].top+nDiv6H);
	rcDiv2[1].SetRect(rcDivW[1].left, rcDiv2[0].bottom, rcDivW[1].right, rcDiv2[0].bottom+nDiv6H);
	rcDiv2[2].SetRect(rcDivW[1].left, rcDiv2[1].bottom, rcDivW[1].right, rcDiv2[1].bottom+nDiv6H);
	rcDiv2[3].SetRect(rcDivW[1].left, rcDiv2[2].bottom, rcDivW[1].right, rcDiv2[2].bottom+nDiv6H);
	rcDiv2[4].SetRect(rcDivW[1].left, rcDiv2[3].bottom, rcDivW[1].right, rcDivW[1].bottom);

	m_st_2.SetBkColor(CR_LIGHTGREEN);
	m_st_2.SetTextColor(CR_BLACK);
	m_st_2.SetFont(CTRL_FONT,20,FW_BOLD);
	m_st_2.MoveWindow(&rcDiv2[0]);

	nDiv3W = (int)((float)rcDiv2[1].Width()/3.0f);

	CalRect.SetRect(rcDiv2[1].left, rcDiv2[1].top, rcDiv2[1].left+nDiv3W, rcDiv2[1].bottom);
	m_st_8.SetBkColor(CR_LIGHTGRAY);
	m_st_8.SetTextColor(CR_BLACK);
	m_st_8.SetFont(CTRL_FONT,20,FW_BOLD);
	m_st_8.MoveWindow(&CalRect);
	CalRect.SetRect(CalRect.right, rcDiv2[1].top, CalRect.right+nDiv3W, rcDiv2[1].bottom);
	m_st_9.SetBkColor(CR_LIGHTGRAY);
	m_st_9.SetTextColor(CR_BLACK);
	m_st_9.SetFont(CTRL_FONT,20,FW_BOLD);
	m_st_9.MoveWindow(&CalRect);
	CalRect.SetRect(CalRect.right, rcDiv2[1].top, rcDiv2[1].right, rcDiv2[1].bottom);
	m_st_10.SetBkColor(CR_LIGHTGRAY);
	m_st_10.SetTextColor(CR_BLACK);
	m_st_10.SetFont(CTRL_FONT,20,FW_BOLD);
	m_st_10.MoveWindow(&CalRect);

	m_st_18.SetBkColor(CR_BLACK);
	m_st_18.SetTextColor(CR_LIGHTGREEN);
	m_st_18.SetFont(CTRL_FONT,20,FW_BOLD);
	m_st_18.MoveWindow(&rcDiv2[2]);

	m_st_22.SetBkColor(CR_WHITE);
	m_st_22.SetTextColor(CR_BLACK);
	m_st_22.SetFont(CTRL_FONT,20,FW_BOLD);
	m_st_22.MoveWindow(&rcDiv2[3]);

	nDiv3W = (int)((float)rcDiv2[4].Width()/3.0f);

	CalRect.SetRect(rcDiv2[4].left, rcDiv2[4].top, rcDiv2[4].left+nDiv3W, rcDiv2[4].bottom);
	m_btn_4.SetFontBtn(CTRL_FONT, 22, FW_BOLD);
	m_btn_4.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_4.SetTextColor(CR_BLACK);
	m_btn_4.SetFlatType(TRUE);
	m_btn_4.MoveWindow(&CalRect);
	CalRect.SetRect(CalRect.right, rcDiv2[4].top, CalRect.right+nDiv3W, rcDiv2[4].bottom);
	m_btn_5.SetFontBtn(CTRL_FONT, 22, FW_BOLD);
	m_btn_5.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_5.SetTextColor(CR_BLACK);
	m_btn_5.SetIconPos(2);
	m_btn_5.SetIconID(IDI_UP);
	m_btn_5.SetIconSize(64,64);
	m_btn_5.SetFlatType(TRUE);
	m_btn_5.MoveWindow(&CalRect);
	CalRect.SetRect(CalRect.right, rcDiv2[4].top, rcDiv2[4].right, rcDiv2[4].bottom);
	m_btn_6.SetFontBtn(CTRL_FONT, 22, FW_BOLD);
	m_btn_6.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_6.SetTextColor(CR_BLACK);
	m_btn_6.SetIconPos(2);
	m_btn_6.SetIconID(IDI_DOWN);
	m_btn_6.SetIconSize(64,64);
	m_btn_6.SetFlatType(TRUE);
	m_btn_6.MoveWindow(&CalRect);

	// Servo#3_Index_Z
	nDiv6H = (int)((float)rcDivW[2].Height()/6.0f);

	rcDiv3[0].SetRect(rcDivW[2].left, rcDivW[2].top, rcDivW[2].right, rcDivW[2].top+nDiv6H);
	rcDiv3[1].SetRect(rcDivW[2].left, rcDiv3[0].bottom, rcDivW[2].right, rcDiv3[0].bottom+nDiv6H);
	rcDiv3[2].SetRect(rcDivW[2].left, rcDiv3[1].bottom, rcDivW[2].right, rcDiv3[1].bottom+nDiv6H);
	rcDiv3[3].SetRect(rcDivW[2].left, rcDiv3[2].bottom, rcDivW[2].right, rcDiv3[2].bottom+nDiv6H);
	rcDiv3[4].SetRect(rcDivW[2].left, rcDiv3[3].bottom, rcDivW[2].right, rcDivW[2].bottom);

	m_st_3.SetBkColor(CR_LIGHTGREEN);
	m_st_3.SetTextColor(CR_BLACK);
	m_st_3.SetFont(CTRL_FONT,20,FW_BOLD);
	m_st_3.MoveWindow(&rcDiv3[0]);

	nDiv3W = (int)((float)rcDiv3[1].Width()/3.0f);

	CalRect.SetRect(rcDiv3[1].left, rcDiv3[1].top, rcDiv3[1].left+nDiv3W, rcDiv3[1].bottom);
	m_st_11.SetBkColor(CR_LIGHTGRAY);
	m_st_11.SetTextColor(CR_BLACK);
	m_st_11.SetFont(CTRL_FONT,20,FW_BOLD);
	m_st_11.MoveWindow(&CalRect);
	CalRect.SetRect(CalRect.right, rcDiv3[1].top, CalRect.right+nDiv3W, rcDiv3[1].bottom);
	m_st_12.SetBkColor(CR_LIGHTGRAY);
	m_st_12.SetTextColor(CR_BLACK);
	m_st_12.SetFont(CTRL_FONT,20,FW_BOLD);
	m_st_12.MoveWindow(&CalRect);
	CalRect.SetRect(CalRect.right, rcDiv3[1].top, rcDiv3[1].right, rcDiv3[1].bottom);
	m_st_13.SetBkColor(CR_LIGHTGRAY);
	m_st_13.SetTextColor(CR_BLACK);
	m_st_13.SetFont(CTRL_FONT,20,FW_BOLD);
	m_st_13.MoveWindow(&CalRect);

	m_st_19.SetBkColor(CR_BLACK);
	m_st_19.SetTextColor(CR_LIGHTGREEN);
	m_st_19.SetFont(CTRL_FONT,20,FW_BOLD);
	m_st_19.MoveWindow(&rcDiv3[2]);

	m_st_23.SetBkColor(CR_WHITE);
	m_st_23.SetTextColor(CR_BLACK);
	m_st_23.SetFont(CTRL_FONT,20,FW_BOLD);
	m_st_23.MoveWindow(&rcDiv3[3]);

	nDiv3W = (int)((float)rcDiv3[4].Width()/3.0f);

	CalRect.SetRect(rcDiv3[4].left, rcDiv3[4].top, rcDiv3[4].left+nDiv3W, rcDiv3[4].bottom);
	m_btn_7.SetFontBtn(CTRL_FONT, 22, FW_BOLD);
	m_btn_7.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_7.SetTextColor(CR_BLACK);
	m_btn_7.SetFlatType(TRUE);
	m_btn_7.MoveWindow(&CalRect);
	CalRect.SetRect(CalRect.right, rcDiv3[4].top, CalRect.right+nDiv3W, rcDiv3[4].bottom);
	m_btn_8.SetFontBtn(CTRL_FONT, 22, FW_BOLD);
	m_btn_8.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_8.SetTextColor(CR_BLACK);
	m_btn_8.SetIconPos(2);
	m_btn_8.SetIconID(IDI_DOWNLEFT);
	m_btn_8.SetIconSize(64,64);
	m_btn_8.SetFlatType(TRUE);
	m_btn_8.MoveWindow(&CalRect);
	CalRect.SetRect(CalRect.right, rcDiv3[4].top, rcDiv3[4].right, rcDiv3[4].bottom);
	m_btn_9.SetFontBtn(CTRL_FONT, 22, FW_BOLD);
	m_btn_9.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_9.SetTextColor(CR_BLACK);
	m_btn_9.SetIconPos(2);
	m_btn_9.SetIconID(IDI_UPRIGHT);
	m_btn_9.SetIconSize(64,64);
	m_btn_9.SetFlatType(TRUE);
	m_btn_9.MoveWindow(&CalRect);

	// Servo#4_Index_T
	nDiv6H = (int)((float)rcDivW[3].Height()/6.0f);

	rcDiv4[0].SetRect(rcDivW[3].left, rcDivW[3].top, rcDivW[3].right, rcDivW[3].top+nDiv6H);
	rcDiv4[1].SetRect(rcDivW[3].left, rcDiv4[0].bottom, rcDivW[3].right, rcDiv4[0].bottom+nDiv6H);
	rcDiv4[2].SetRect(rcDivW[3].left, rcDiv4[1].bottom, rcDivW[3].right, rcDiv4[1].bottom+nDiv6H);
	rcDiv4[3].SetRect(rcDivW[3].left, rcDiv4[2].bottom, rcDivW[3].right, rcDiv4[2].bottom+nDiv6H);
	rcDiv4[4].SetRect(rcDivW[3].left, rcDiv4[3].bottom, rcDivW[3].right, rcDivW[3].bottom);

	m_st_4.SetBkColor(CR_LIGHTGREEN);
	m_st_4.SetTextColor(CR_BLACK);
	m_st_4.SetFont(CTRL_FONT,20,FW_BOLD);
	m_st_4.MoveWindow(&rcDiv4[0]);

	nDiv3W = (int)((float)rcDiv4[1].Width()/3.0f);

	CalRect.SetRect(rcDiv4[1].left, rcDiv4[1].top, rcDiv4[1].left+nDiv3W, rcDiv4[1].bottom);
	m_st_14.SetBkColor(CR_LIGHTGRAY);
	m_st_14.SetTextColor(CR_BLACK);
	m_st_14.SetFont(CTRL_FONT,20,FW_BOLD);
	m_st_14.MoveWindow(&CalRect);
	CalRect.SetRect(CalRect.right, rcDiv4[1].top, CalRect.right+nDiv3W, rcDiv4[1].bottom);
	m_st_15.SetBkColor(CR_LIGHTGRAY);
	m_st_15.SetTextColor(CR_BLACK);
	m_st_15.SetFont(CTRL_FONT,20,FW_BOLD);
	m_st_15.MoveWindow(&CalRect);
	CalRect.SetRect(CalRect.right, rcDiv4[1].top, rcDiv4[1].right, rcDiv4[1].bottom);
	m_st_16.SetBkColor(CR_LIGHTGRAY);
	m_st_16.SetTextColor(CR_BLACK);
	m_st_16.SetFont(CTRL_FONT,20,FW_BOLD);
	m_st_16.MoveWindow(&CalRect);

	m_st_20.SetBkColor(CR_BLACK);
	m_st_20.SetTextColor(CR_LIGHTGREEN);
	m_st_20.SetFont(CTRL_FONT,20,FW_BOLD);
	m_st_20.MoveWindow(&rcDiv4[2]);

	m_st_24.SetBkColor(CR_WHITE);
	m_st_24.SetTextColor(CR_BLACK);
	m_st_24.SetFont(CTRL_FONT,20,FW_BOLD);
	m_st_24.MoveWindow(&rcDiv4[3]);

	nDiv3W = (int)((float)rcDiv4[4].Width()/3.0f);

	CalRect.SetRect(rcDiv4[4].left, rcDiv4[4].top, rcDiv4[4].left+nDiv3W, rcDiv4[4].bottom);
	m_btn_10.SetFontBtn(CTRL_FONT, 22, FW_BOLD);
	m_btn_10.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_10.SetTextColor(CR_BLACK);
	m_btn_10.SetFlatType(TRUE);
	m_btn_10.MoveWindow(&CalRect);
	CalRect.SetRect(CalRect.right, rcDiv4[4].top, CalRect.right+nDiv3W, rcDiv4[4].bottom);
	m_btn_11.SetFontBtn(CTRL_FONT, 22, FW_BOLD);
	m_btn_11.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_11.SetTextColor(CR_BLACK);
	m_btn_11.SetIconPos(2);
	m_btn_11.SetIconID(IDI_CW);
	m_btn_11.SetIconSize(64,64);
	m_btn_11.SetFlatType(TRUE);
	m_btn_11.MoveWindow(&CalRect);
	CalRect.SetRect(CalRect.right, rcDiv4[4].top, rcDiv4[4].right, rcDiv4[4].bottom);
	m_btn_12.SetFontBtn(CTRL_FONT, 22, FW_BOLD);
	m_btn_12.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_12.SetTextColor(CR_BLACK);
	m_btn_12.SetIconPos(2);
	m_btn_12.SetIconID(IDI_CCW);
	m_btn_12.SetIconSize(64,64);
	m_btn_12.SetFlatType(TRUE);
	m_btn_12.MoveWindow(&CalRect);
}

void CDlg_MotorStacker::Cal_CtrlArea_Down()
{
	CRect CalRect;
	CRect rcLeft, rcRight;
	CRect rcLeftUp, rcLeftDn;
	CRect rcRightUp, rcRightMid, rcRightDn;

	int n70pW = (int)((float)m_rcDown.Width()*0.70f);
	rcLeft.SetRect(m_rcDown.left, m_rcDown.top, m_rcDown.left+n70pW, m_rcDown.bottom);
	rcRight.SetRect(m_rcDown.left+n70pW+CTRL_MARGIN3, m_rcDown.top, m_rcDown.right, m_rcDown.bottom);

	int n50pH = (int)((float)rcLeft.Height()*0.50f);
	rcLeftUp.SetRect(rcLeft.left, rcLeft.top, rcLeft.right, rcLeft.top+n50pH);
	rcLeftDn.SetRect(rcLeft.left, rcLeft.top+n50pH+CTRL_MARGIN3, rcLeft.right, rcLeft.bottom);

	int n35pH = (int)((float)rcRight.Height()*0.35f);
	int n20pH = (int)((float)rcRight.Height()*0.20f);
	rcRightUp.SetRect(rcRight.left, rcRight.top, rcRight.right, rcRight.top+n35pH);
	rcRightMid.SetRect(rcRight.left, rcRightUp.bottom+CTRL_MARGIN3, rcRight.right, rcRightUp.bottom+n20pH);
	rcRightDn.SetRect(rcRight.left, rcRightMid.bottom+CTRL_MARGIN3, rcRight.right, rcRight.bottom);

	CalRect = rcLeftUp;
	CalRect.DeflateRect(6,28,6,6);
	m_gb_1.SetXPGroupStyle(CXPGroupBox::XPGB_WINDOW_2);
	m_gb_1.SetBackgroundColor(CR_DARKGRAY, CR_GB_BKGND);
	m_gb_1.SetBorderColor(CR_BLACK);
	m_gb_1.SetCatptionTextColor(CR_BLACK);
	m_gb_1.MoveWindow(&rcLeftUp);
	m_GC_1.MoveWindow(&CalRect);
	Init_GC_1(6,10);

	CalRect = rcLeftDn;
	CalRect.DeflateRect(6,28,6,6);
	m_gb_2.SetXPGroupStyle(CXPGroupBox::XPGB_WINDOW_2);
	m_gb_2.SetBackgroundColor(CR_DARKGRAY, CR_GB_BKGND);
	m_gb_2.SetBorderColor(CR_BLACK);
	m_gb_2.SetCatptionTextColor(CR_BLACK);
	m_gb_2.MoveWindow(&rcLeftDn);
	m_GC_2.MoveWindow(&CalRect);
	Init_GC_2(6,10);

	CalRect = rcRightUp;
	CalRect.DeflateRect(6,28,6,6);
	m_gb_3.SetXPGroupStyle(CXPGroupBox::XPGB_WINDOW_2);
	m_gb_3.SetBackgroundColor(CR_DARKGRAY, CR_GB_BKGND);
	m_gb_3.SetBorderColor(CR_BLACK);
	m_gb_3.SetCatptionTextColor(CR_BLACK);
	m_gb_3.MoveWindow(&rcRightUp);
	m_GC_3.MoveWindow(&CalRect);
	Init_GC_3(4,3);

	CalRect = rcRightMid;
	CalRect.DeflateRect(6,28,6,6);
	m_gb_4.SetXPGroupStyle(CXPGroupBox::XPGB_WINDOW_2);
	m_gb_4.SetBackgroundColor(CR_DARKGRAY, CR_GB_BKGND);
	m_gb_4.SetBorderColor(CR_BLACK);
	m_gb_4.SetCatptionTextColor(CR_BLACK);
	m_gb_4.MoveWindow(&rcRightMid);
	m_GC_4.MoveWindow(&CalRect);
	Init_GC_4(2,3);

	CalRect.SetRect(rcRightDn.right - 140, rcRightDn.bottom - 60, rcRightDn.right, rcRightDn.bottom);
	CalRect.DeflateRect(2,2);
	m_btn_13.SetFontBtn(CTRL_FONT, 24, FW_BOLD);
	m_btn_13.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_13.SetTextColor(CR_BLACK);
	m_btn_13.SetIconPos(0);
	m_btn_13.SetIconID(IDI_CLOSE);
	m_btn_13.SetIconSize(48,48);
	m_btn_13.SetFlatType(TRUE);
	m_btn_13.MoveWindow(&CalRect);
}

bool CDlg_MotorStacker::Init_GC_1(int nRows, int nCols)
{
	BOOL m_bEditable = FALSE;
    BOOL m_bListMode = TRUE;
    int m_nRows = nRows;
    int m_nCols = nCols;
    int m_nFixRows = 0;
    int m_nFixCols = 0;

    m_GC_1.SetEditable(m_bEditable);
    m_GC_1.SetListMode(m_bListMode);
    m_GC_1.EnableDragAndDrop(FALSE);
    m_GC_1.SetTextBkColor(RGB(0xFF, 0xFF, 0xE0));

    m_GC_1.SetRowCount(m_nRows);
    m_GC_1.SetColumnCount(m_nCols);
    m_GC_1.SetFixedRowCount(m_nFixRows);
    m_GC_1.SetFixedColumnCount(m_nFixCols);

	m_GC_1.EnableSelection(false);
	m_GC_1.SetSingleColSelection(true);
	m_GC_1.SetSingleRowSelection(true);
	m_GC_1.SetFixedColumnSelection(false);
    m_GC_1.SetFixedRowSelection(false);

	m_GC_1.SetRowResize(false);
	m_GC_1.SetColumnResize(false);

	CRect ClientRect;
	GetDlgItem(IDC_GC_MS_1)->GetWindowRect(&ClientRect);
	if(ClientRect.Width() < 4) return false;
	if(ClientRect.Height() < 4) return false;
	int nClientWidth = ClientRect.Width()-4;
	int nClientHeight = ClientRect.Height()-4;
	int nCellW = nClientWidth/m_nCols;
	int nCellH = nClientHeight/m_nRows;
	int nLastCellW = nClientWidth-nCellW*(m_nCols-1);
	int nLastCellH = nClientHeight-nCellH*(m_nRows-1);
	int nRemainW = nLastCellW - nCellW;
	int nRemainH = nLastCellH - nCellH;
	for (int i = 0; i < m_GC_1.GetRowCount(); i++) 
	{
		if(i == m_nRows-1)
		{
			m_GC_1.SetRowHeight(i,nCellH);
		}
		else 
		{
			if(nRemainH > 0)
			{
				m_GC_1.SetRowHeight(i,nCellH + 1);
				nRemainH --;
			}
			else
				m_GC_1.SetRowHeight(i,nCellH);
		}
	}
	for (int j = 0; j < m_GC_1.GetColumnCount(); j++)
	{
		if(j == m_nCols-1)
		{
			m_GC_1.SetColumnWidth(j,nCellW);
		}
		else 
		{
			if(nRemainW > 0)
			{
				m_GC_1.SetColumnWidth(j,nCellW + 1);
				nRemainW --;
			}
			else
				m_GC_1.SetColumnWidth(j,nCellW);
		}
	}
    DWORD dwTextStyle = DT_CENTER|DT_VCENTER|DT_SINGLELINE;

    // fill rows/cols with text
    for (int row = 0; row < m_GC_1.GetRowCount(); row++) 
	{
        for (int col = 0; col < m_GC_1.GetColumnCount(); col++) 
		{ 
            GV_ITEM Item;
            Item.mask = GVIF_TEXT|GVIF_FORMAT;
            Item.row = row;
            Item.col = col;
			Item.nFormat = dwTextStyle;

			if(row%2 == 0)
			{
				m_GC_1.SetItemBkColour(row, col, CR_SLATEGRAY);
				Item.strText.Format(_T("X%02d"), col+1 + (2-row/2)*10);
			}

			m_GC_1.SetItemFgColour(row, col, RGB(0,0,0));
			m_GC_1.SetItemFont(row, col, CTRL_FONT, 18, FW_BOLD);
            m_GC_1.SetItem(&Item);  
        }
    }
       
    return TRUE;  // return TRUE  unless you set the focus to a control
}

void CDlg_MotorStacker::OnGridClick_GC_1(NMHDR *pNotifyStruct, LRESULT* /*pResult*/)
{
	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*) pNotifyStruct;

	int nSelRow = pItem->iRow;
	int nSelCol = pItem->iColumn;

	if(nSelRow == 1 || nSelRow == 3 || nSelRow == 5) // POS
	{
		CDlg_NumPad dlg;
		dlg.m_strCaption = _T("Change Motor Position Value");
		if(dlg.DoModal() == IDOK)
		{
			int nPLCNum = CAppSetting::Instance()->GetGroupNumber();
			int nCalAddress = 3500 + nSelCol*2 + (2-nSelRow/2)*20;
			double dValue = _wtof(dlg.m_strValue)*1000.0f;
			CDataManager::Instance()->PLC_Write_D2(nPLCNum, nCalAddress, (int)dValue);
		}
	}
}

void CDlg_MotorStacker::OnEndLabel_GC_1(NMHDR *pNotifyStruct, LRESULT* pResult)
{
	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*) pNotifyStruct;

	int nSelRow = pItem->iRow;
	int nSelCol = pItem->iColumn;
}

bool CDlg_MotorStacker::Init_GC_2(int nRows, int nCols)
{
	BOOL m_bEditable = FALSE;
    BOOL m_bListMode = TRUE;
    int m_nRows = nRows;
    int m_nCols = nCols;
    int m_nFixRows = 0;
    int m_nFixCols = 0;

    m_GC_2.SetEditable(m_bEditable);
    m_GC_2.SetListMode(m_bListMode);
    m_GC_2.EnableDragAndDrop(FALSE);
    m_GC_2.SetTextBkColor(RGB(0xFF, 0xFF, 0xE0));

    m_GC_2.SetRowCount(m_nRows);
    m_GC_2.SetColumnCount(m_nCols);
    m_GC_2.SetFixedRowCount(m_nFixRows);
    m_GC_2.SetFixedColumnCount(m_nFixCols);

	m_GC_2.EnableSelection(false);
	m_GC_2.SetSingleColSelection(true);
	m_GC_2.SetSingleRowSelection(true);
	m_GC_2.SetFixedColumnSelection(false);
    m_GC_2.SetFixedRowSelection(false);

	m_GC_2.SetRowResize(false);
	m_GC_2.SetColumnResize(false);

	CRect ClientRect;
	GetDlgItem(IDC_GC_MS_2)->GetWindowRect(&ClientRect);
	if(ClientRect.Width() < 4) return false;
	if(ClientRect.Height() < 4) return false;
	int nClientWidth = ClientRect.Width()-4;
	int nClientHeight = ClientRect.Height()-4;
	int nCellW = nClientWidth/m_nCols;
	int nCellH = nClientHeight/m_nRows;
	int nLastCellW = nClientWidth-nCellW*(m_nCols-1);
	int nLastCellH = nClientHeight-nCellH*(m_nRows-1);
	int nRemainW = nLastCellW - nCellW;
	int nRemainH = nLastCellH - nCellH;
	for (int i = 0; i < m_GC_2.GetRowCount(); i++) 
	{
		if(i == m_nRows-1)
		{
			m_GC_2.SetRowHeight(i,nCellH);
		}
		else 
		{
			if(nRemainH > 0)
			{
				m_GC_2.SetRowHeight(i,nCellH + 1);
				nRemainH --;
			}
			else
				m_GC_2.SetRowHeight(i,nCellH);
		}
	}
	for (int j = 0; j < m_GC_2.GetColumnCount(); j++)
	{
		if(j == m_nCols-1)
		{
			m_GC_2.SetColumnWidth(j,nCellW);
		}
		else 
		{
			if(nRemainW > 0)
			{
				m_GC_2.SetColumnWidth(j,nCellW + 1);
				nRemainW --;
			}
			else
				m_GC_2.SetColumnWidth(j,nCellW);
		}
	}
    DWORD dwTextStyle = DT_CENTER|DT_VCENTER|DT_SINGLELINE;

    // fill rows/cols with text
    for (int row = 0; row < m_GC_2.GetRowCount(); row++) 
	{
        for (int col = 0; col < m_GC_2.GetColumnCount(); col++) 
		{ 
            GV_ITEM Item;
            Item.mask = GVIF_TEXT|GVIF_FORMAT;
            Item.row = row;
            Item.col = col;
			Item.nFormat = dwTextStyle;

			if(row%2 == 0)
			{
				Item.strText.Format(_T("Y%02d"), col+1 + (2-row/2)*10);
			}

			if(row%2 == 0) m_GC_2.SetItemBkColour(row, col, CR_SLATEGRAY);
			m_GC_2.SetItemFgColour(row, col, RGB(0,0,0));
			m_GC_2.SetItemFont(row, col, CTRL_FONT, 18, FW_BOLD);
            m_GC_2.SetItem(&Item);  
        }
    }
       
    return TRUE;  // return TRUE  unless you set the focus to a control
}

void CDlg_MotorStacker::OnGridClick_GC_2(NMHDR *pNotifyStruct, LRESULT* /*pResult*/)
{
	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*) pNotifyStruct;

	int nSelRow = pItem->iRow;
	int nSelCol = pItem->iColumn;

	if(nSelRow == 1 || nSelRow == 3 || nSelRow == 5) // POS
	{
		CDlg_NumPad dlg;
		dlg.m_strCaption = _T("Change Motor Position Value");
		if(dlg.DoModal() == IDOK)
		{
			int nPLCNum = CAppSetting::Instance()->GetGroupNumber();
			int nCalAddress = 3800 + nSelCol*2 + (2-nSelRow/2)*20;
			double dValue = _wtof(dlg.m_strValue)*1000.0f;
			CDataManager::Instance()->PLC_Write_D2(nPLCNum, nCalAddress, (int)dValue);
		}
	}
}

void CDlg_MotorStacker::OnEndLabel_GC_2(NMHDR *pNotifyStruct, LRESULT* pResult)
{
	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*) pNotifyStruct;

	int nSelRow = pItem->iRow;
	int nSelCol = pItem->iColumn;
}

bool CDlg_MotorStacker::Init_GC_3(int nRows, int nCols)
{
	BOOL m_bEditable = FALSE;
    BOOL m_bListMode = TRUE;
    int m_nRows = nRows;
    int m_nCols = nCols;
    int m_nFixRows = 0;
    int m_nFixCols = 0;

    m_GC_3.SetEditable(m_bEditable);
    m_GC_3.SetListMode(m_bListMode);
    m_GC_3.EnableDragAndDrop(FALSE);
    m_GC_3.SetTextBkColor(RGB(0xFF, 0xFF, 0xE0));

    m_GC_3.SetRowCount(m_nRows);
    m_GC_3.SetColumnCount(m_nCols);
    m_GC_3.SetFixedRowCount(m_nFixRows);
    m_GC_3.SetFixedColumnCount(m_nFixCols);

	m_GC_3.EnableSelection(false);
	m_GC_3.SetSingleColSelection(true);
	m_GC_3.SetSingleRowSelection(true);
	m_GC_3.SetFixedColumnSelection(false);
    m_GC_3.SetFixedRowSelection(false);

	m_GC_3.SetRowResize(false);
	m_GC_3.SetColumnResize(false);

	CRect ClientRect;
	GetDlgItem(IDC_GC_MS_3)->GetWindowRect(&ClientRect);
	if(ClientRect.Width() < 4) return false;
	if(ClientRect.Height() < 4) return false;
	int nClientWidth = ClientRect.Width()-4;
	int nClientHeight = ClientRect.Height()-4;
	int nCellW = nClientWidth/m_nCols;
	int nCellH = nClientHeight/m_nRows;
	int nLastCellW = nClientWidth-nCellW*(m_nCols-1);
	int nLastCellH = nClientHeight-nCellH*(m_nRows-1);
	int nRemainW = nLastCellW - nCellW;
	int nRemainH = nLastCellH - nCellH;
	for (int i = 0; i < m_GC_3.GetRowCount(); i++) 
	{
		if(i == m_nRows-1)
		{
			m_GC_3.SetRowHeight(i,nCellH);
		}
		else 
		{
			if(nRemainH > 0)
			{
				m_GC_3.SetRowHeight(i,nCellH + 1);
				nRemainH --;
			}
			else
				m_GC_3.SetRowHeight(i,nCellH);
		}
	}
	for (int j = 0; j < m_GC_3.GetColumnCount(); j++)
	{
		if(j == m_nCols-1)
		{
			m_GC_3.SetColumnWidth(j,nCellW);
		}
		else 
		{
			if(nRemainW > 0)
			{
				m_GC_3.SetColumnWidth(j,nCellW + 1);
				nRemainW --;
			}
			else
				m_GC_3.SetColumnWidth(j,nCellW);
		}
	}
    DWORD dwTextStyle = DT_CENTER|DT_VCENTER|DT_SINGLELINE;

    // fill rows/cols with text
    for (int row = 0; row < m_GC_3.GetRowCount(); row++) 
	{
        for (int col = 0; col < m_GC_3.GetColumnCount(); col++) 
		{ 
            GV_ITEM Item;
            Item.mask = GVIF_TEXT|GVIF_FORMAT;
            Item.row = row;
            Item.col = col;
			Item.nFormat = dwTextStyle;

			if(row == 0)
			{
				m_GC_3.SetItemBkColour(row, col, CR_SLATEGRAY);
				Item.strText.Format(_T("Z_Up%02d"), col+1);
			}
			if(row == 2)
			{
				m_GC_3.SetItemBkColour(row, col, CR_SLATEGRAY);
				Item.strText.Format(_T("Z_Down%02d"), col+1);
			}
			
			m_GC_3.SetItemFgColour(row, col, RGB(0,0,0));
			m_GC_3.SetItemFont(row, col, CTRL_FONT, 18, FW_BOLD);
            m_GC_3.SetItem(&Item);  
        }
    }
       
    return TRUE;  // return TRUE  unless you set the focus to a control
}

void CDlg_MotorStacker::OnGridClick_GC_3(NMHDR *pNotifyStruct, LRESULT* /*pResult*/)
{
	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*) pNotifyStruct;

	int nSelRow = pItem->iRow;
	int nSelCol = pItem->iColumn;

	if(nSelRow == 1) // POS
	{
		CDlg_NumPad dlg;
		dlg.m_strCaption = _T("Change Motor Position Value");
		if(dlg.DoModal() == IDOK)
		{
			int nPLCNum = CAppSetting::Instance()->GetGroupNumber();
			int nCalAddress = 260 + nSelCol*2;
			double dValue = _wtof(dlg.m_strValue)*1000.0f;
			CDataManager::Instance()->PLC_Write_D2(nPLCNum, nCalAddress, (int)dValue);
		}
	}
	if(nSelRow == 3) // POS
	{
		CDlg_NumPad dlg;
		dlg.m_strCaption = _T("Change Motor Position Value");
		if(dlg.DoModal() == IDOK)
		{
			int nPLCNum = CAppSetting::Instance()->GetGroupNumber();
			int nCalAddress = 250 + nSelCol*2;
			double dValue = _wtof(dlg.m_strValue)*1000.0f;
			CDataManager::Instance()->PLC_Write_D2(nPLCNum, nCalAddress, (int)dValue);
		}
	}
}

void CDlg_MotorStacker::OnEndLabel_GC_3(NMHDR *pNotifyStruct, LRESULT* pResult)
{
	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*) pNotifyStruct;

	int nSelRow = pItem->iRow;
	int nSelCol = pItem->iColumn;
}

bool CDlg_MotorStacker::Init_GC_4(int nRows, int nCols)
{
	BOOL m_bEditable = FALSE;
    BOOL m_bListMode = TRUE;
    int m_nRows = nRows;
    int m_nCols = nCols;
    int m_nFixRows = 0;
    int m_nFixCols = 0;

    m_GC_4.SetEditable(m_bEditable);
    m_GC_4.SetListMode(m_bListMode);
    m_GC_4.EnableDragAndDrop(FALSE);
    m_GC_4.SetTextBkColor(RGB(0xFF, 0xFF, 0xE0));

    m_GC_4.SetRowCount(m_nRows);
    m_GC_4.SetColumnCount(m_nCols);
    m_GC_4.SetFixedRowCount(m_nFixRows);
    m_GC_4.SetFixedColumnCount(m_nFixCols);

	m_GC_4.EnableSelection(false);
	m_GC_4.SetSingleColSelection(true);
	m_GC_4.SetSingleRowSelection(true);
	m_GC_4.SetFixedColumnSelection(false);
    m_GC_4.SetFixedRowSelection(false);

	m_GC_4.SetRowResize(false);
	m_GC_4.SetColumnResize(false);

	CRect ClientRect;
	GetDlgItem(IDC_GC_MS_4)->GetWindowRect(&ClientRect);
	if(ClientRect.Width() < 4) return false;
	if(ClientRect.Height() < 4) return false;
	int nClientWidth = ClientRect.Width()-4;
	int nClientHeight = ClientRect.Height()-4;
	int nCellW = nClientWidth/m_nCols;
	int nCellH = nClientHeight/m_nRows;
	int nLastCellW = nClientWidth-nCellW*(m_nCols-1);
	int nLastCellH = nClientHeight-nCellH*(m_nRows-1);
	int nRemainW = nLastCellW - nCellW;
	int nRemainH = nLastCellH - nCellH;
	for (int i = 0; i < m_GC_4.GetRowCount(); i++) 
	{
		if(i == m_nRows-1)
		{
			m_GC_4.SetRowHeight(i,nCellH);
		}
		else 
		{
			if(nRemainH > 0)
			{
				m_GC_4.SetRowHeight(i,nCellH + 1);
				nRemainH --;
			}
			else
				m_GC_4.SetRowHeight(i,nCellH);
		}
	}
	for (int j = 0; j < m_GC_4.GetColumnCount(); j++)
	{
		if(j == m_nCols-1)
		{
			m_GC_4.SetColumnWidth(j,nCellW);
		}
		else 
		{
			if(nRemainW > 0)
			{
				m_GC_4.SetColumnWidth(j,nCellW + 1);
				nRemainW --;
			}
			else
				m_GC_4.SetColumnWidth(j,nCellW);
		}
	}
    DWORD dwTextStyle = DT_CENTER|DT_VCENTER|DT_SINGLELINE;

    // fill rows/cols with text
    for (int row = 0; row < m_GC_4.GetRowCount(); row++) 
	{
        for (int col = 0; col < m_GC_4.GetColumnCount(); col++) 
		{ 
            GV_ITEM Item;
            Item.mask = GVIF_TEXT|GVIF_FORMAT;
            Item.row = row;
            Item.col = col;
			Item.nFormat = dwTextStyle;

			if(row == 0)
			{
				m_GC_4.SetItemBkColour(row, col, CR_SLATEGRAY);
				Item.strText.Format(_T("T%02d"), col+1);
			}

			m_GC_4.SetItemFgColour(row, col, RGB(0,0,0));
			m_GC_4.SetItemFont(row, col, CTRL_FONT, 18, FW_BOLD);
            m_GC_4.SetItem(&Item);  
        }
    }
       
    return TRUE;  // return TRUE  unless you set the focus to a control
}

void CDlg_MotorStacker::OnGridClick_GC_4(NMHDR *pNotifyStruct, LRESULT* /*pResult*/)
{
	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*) pNotifyStruct;

	int nSelRow = pItem->iRow;
	int nSelCol = pItem->iColumn;

	if(nSelRow == 1) // POS
	{
		CDlg_NumPad dlg;
		dlg.m_strCaption = _T("Change Motor Position Value");
		if(dlg.DoModal() == IDOK)
		{
			int nPLCNum = CAppSetting::Instance()->GetGroupNumber();
			int nCalAddress = 450 + nSelCol*2;
			double dValue = _wtof(dlg.m_strValue)*1000.0f;
			CDataManager::Instance()->PLC_Write_D2(nPLCNum, nCalAddress, (int)dValue);
		}
	}
}

void CDlg_MotorStacker::OnEndLabel_GC_4(NMHDR *pNotifyStruct, LRESULT* pResult)
{
	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*) pNotifyStruct;

	int nSelRow = pItem->iRow;
	int nSelCol = pItem->iColumn;
}

BOOL CDlg_MotorStacker::OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	NMHDR *pNMHDR = (NMHDR*)lParam;	

	int nPLCNum = CAppSetting::Instance()->GetGroupNumber();
	if( pNMHDR->code == TCN_SELCHANGE ) // Button Pressed
	{
		// INDEX_X
		if(wParam == IDC_BTN_MS_1) // SERVO ON/OFF
		{
			if(CDataManager::Instance()->PLC_Read_M2(nPLCNum, 143))
				CDataManager::Instance()->PLC_Write_M(nPLCNum, 143, FALSE);
			else
				CDataManager::Instance()->PLC_Write_M(nPLCNum, 143, TRUE);
		}
		if(wParam == IDC_BTN_MS_2) // BW
		{
			CDataManager::Instance()->PLC_Write_M(nPLCNum, 142, TRUE);
		}
		if(wParam == IDC_BTN_MS_3) // FW
		{
			CDataManager::Instance()->PLC_Write_M(nPLCNum, 141, TRUE);
		}
		// INDEX_Y
		if(wParam == IDC_BTN_MS_4) // SERVO ON/OFF
		{
			if(CDataManager::Instance()->PLC_Read_M2(nPLCNum, 243))
				CDataManager::Instance()->PLC_Write_M(nPLCNum, 243, FALSE);
			else
				CDataManager::Instance()->PLC_Write_M(nPLCNum, 243, TRUE);
		}
		if(wParam == IDC_BTN_MS_5) // UP
		{
			CDataManager::Instance()->PLC_Write_M(nPLCNum, 242, TRUE);
		}
		if(wParam == IDC_BTN_MS_6) // DN
		{
			CDataManager::Instance()->PLC_Write_M(nPLCNum, 241, TRUE);
		}
		// INDEX_Z
		if(wParam == IDC_BTN_MS_7) // SERVO ON/OFF
		{
			if(CDataManager::Instance()->PLC_Read_M2(nPLCNum, 343))
				CDataManager::Instance()->PLC_Write_M(nPLCNum, 343, FALSE);
			else
				CDataManager::Instance()->PLC_Write_M(nPLCNum, 343, TRUE);
		}
		if(wParam == IDC_BTN_MS_8) // BW
		{
			CDataManager::Instance()->PLC_Write_M(nPLCNum, 342, TRUE);
		}
		if(wParam == IDC_BTN_MS_9) // FW
		{
			CDataManager::Instance()->PLC_Write_M(nPLCNum, 341, TRUE);
		}
		// INDEX_T
		if(wParam == IDC_BTN_MS_10) // SERVO ON/OFF
		{
			if(CDataManager::Instance()->PLC_Read_M2(nPLCNum, 443))
				CDataManager::Instance()->PLC_Write_M(nPLCNum, 443, FALSE);
			else
				CDataManager::Instance()->PLC_Write_M(nPLCNum, 443, TRUE);
		}
		if(wParam == IDC_BTN_MS_11) // CCW
		{
			CDataManager::Instance()->PLC_Write_M(nPLCNum, 442, TRUE);
		}
		if(wParam == IDC_BTN_MS_12) // CW
		{
			CDataManager::Instance()->PLC_Write_M(nPLCNum, 441, TRUE);
		}
	}
	if( pNMHDR->code == TCN_SELCHANGING ) // Button Released
	{
		// INDEX_X
		if(wParam == IDC_BTN_MS_1) // SERVO ON/OFF
		{
		}
		if(wParam == IDC_BTN_MS_2) // BW 
		{
			CDataManager::Instance()->PLC_Write_M(nPLCNum, 142, FALSE);
		}
		if(wParam == IDC_BTN_MS_3) // FW
		{
			CDataManager::Instance()->PLC_Write_M(nPLCNum, 141, FALSE);
		}
		// INDEX_Y
		if(wParam == IDC_BTN_MS_4) // SERVO ON/OFF
		{
		}
		if(wParam == IDC_BTN_MS_5) // UP 
		{
			CDataManager::Instance()->PLC_Write_M(nPLCNum, 242, FALSE);
		}
		if(wParam == IDC_BTN_MS_6) // DN
		{
			CDataManager::Instance()->PLC_Write_M(nPLCNum, 241, FALSE);
		}
		// INDEX_Z
		if(wParam == IDC_BTN_MS_7) // SERVO ON/OFF
		{
		}
		if(wParam == IDC_BTN_MS_8) // BW 
		{
			CDataManager::Instance()->PLC_Write_M(nPLCNum, 342, FALSE);
		}
		if(wParam == IDC_BTN_MS_9) // FW
		{
			CDataManager::Instance()->PLC_Write_M(nPLCNum, 341, FALSE);
		}
		// INDEX_T
		if(wParam == IDC_BTN_MS_10) // SERVO ON/OFF
		{
		}
		if(wParam == IDC_BTN_MS_11) // CCW 
		{
			CDataManager::Instance()->PLC_Write_M(nPLCNum, 442, FALSE);
		}
		if(wParam == IDC_BTN_MS_12) // CW
		{
			CDataManager::Instance()->PLC_Write_M(nPLCNum, 441, FALSE);
		}
	}

	return CDialogEx::OnNotify(wParam, lParam, pResult);
}

void CDlg_MotorStacker::Update_PLC_Value()
{
	CString strValue;
	float fValue;
	int nServoMAddressBit, nPrevMAddressBit, nNextMAddressBit;
	SHORT sValue;
	stMotorManual_Info stMMI = CMotorManualSetting::Instance()->GetMotorManualInfo();
	MADDRESS stMAddress;
	int nPLCNum = CAppSetting::Instance()->GetGroupNumber();
	/////////////////////////////////////////////////////////////////////////////////////
	// INDEX _X /////////////////////////////////////////////////////////////////////////
	stMAddress = CDataManager::Instance()->m_PLCReaderD_Motor.GetValueM(107);
	if(stMAddress.m_xBit_0 == 0) // CCW LIMIT = LIMIT -
	{
		m_st_5.SetBkColor(CR_LAVENDER);
		m_st_5.SetTextColor(CR_BLACK);
	}
	else
	{
		m_st_5.SetBkColor(CR_RED);
		m_st_5.SetTextColor(CR_WHITE);
	}
	if(stMAddress.m_xBit_1 == 0) // CW LIMIT = LIMIT +
	{
		m_st_6.SetBkColor(CR_LAVENDER);
		m_st_6.SetTextColor(CR_BLACK);
	}
	else
	{
		m_st_6.SetBkColor(CR_RED);
		m_st_6.SetTextColor(CR_WHITE);
	}
	if(stMAddress.m_xBit_6 == 0) // HOME
	{
		m_st_7.SetBkColor(CR_LAVENDER);
		m_st_7.SetTextColor(CR_BLACK);
	}
	else
	{
		m_st_7.SetBkColor(CR_RED);
		m_st_7.SetTextColor(CR_WHITE);
	}
	// MOTOR CURRENT POSITION VALUE
	fValue = (float)CDataManager::Instance()->m_PLCReaderD_Motor.GetValue2(100)/1000.0f;
	strValue.Format(_T("%.3f mm "), fValue);
	m_st_17.SetWindowText(strValue);
	// JOG SPEED
	sValue = CDataManager::Instance()->m_PLCReaderD_Motor.GetValue(118)/10;
	if(sValue)
	{
		strValue.Format(_T("%d "), sValue);
		m_st_21.SetWindowText(strValue);
	}
	// SERVO BUTTON LAMP CHECK
	nServoMAddressBit = 143;
	if(CDataManager::Instance()->PLC_Read_M2(nPLCNum, nServoMAddressBit))
	{
		m_btn_1.SetButtonColor(CR_LIMEGREEN, CR_LIGHTGRAY);
		m_btn_1.SetTextColor(CR_WHITE);
		m_btn_1.Invalidate();
	}
	else
	{
		m_btn_1.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
		m_btn_1.SetTextColor(CR_BLACK);
		m_btn_1.Invalidate();
	}
	// BW BUTTON LAMP CHECK
	nPrevMAddressBit = 142;
	if(CDataManager::Instance()->PLC_Read_M2(nPLCNum, nPrevMAddressBit))
	{
		m_btn_2.SetButtonColor(CR_LIMEGREEN, CR_LIGHTGRAY);
		m_btn_2.SetTextColor(CR_WHITE);
		m_btn_2.Invalidate();
	}
	else
	{
		m_btn_2.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
		m_btn_2.SetTextColor(CR_BLACK);
		m_btn_2.Invalidate();
	}
	// FW BUTTON LAMP CHECK
	nNextMAddressBit = 141;
	if(CDataManager::Instance()->PLC_Read_M2(nPLCNum, nNextMAddressBit))
	{
		m_btn_3.SetButtonColor(CR_LIMEGREEN, CR_LIGHTGRAY);
		m_btn_3.SetTextColor(CR_WHITE);
		m_btn_3.Invalidate();
	}
	else
	{
		m_btn_3.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
		m_btn_3.SetTextColor(CR_BLACK);
		m_btn_3.Invalidate();
	}
	/////////////////////////////////////////////////////////////////////////////////////
	// INDEX _Y /////////////////////////////////////////////////////////////////////////
	stMAddress = CDataManager::Instance()->m_PLCReaderD_Motor.GetValueM(207);
	if(stMAddress.m_xBit_0 == 0) // CCW LIMIT = LIMIT -
	{
		m_st_8.SetBkColor(CR_LAVENDER);
		m_st_8.SetTextColor(CR_BLACK);
	}
	else
	{
		m_st_8.SetBkColor(CR_RED);
		m_st_8.SetTextColor(CR_WHITE);
	}
	if(stMAddress.m_xBit_1 == 0) // CW LIMIT = LIMIT +
	{
		m_st_9.SetBkColor(CR_LAVENDER);
		m_st_9.SetTextColor(CR_BLACK);
	}
	else
	{
		m_st_9.SetBkColor(CR_RED);
		m_st_9.SetTextColor(CR_WHITE);
	}
	if(stMAddress.m_xBit_6 == 0) // HOME
	{
		m_st_10.SetBkColor(CR_LAVENDER);
		m_st_10.SetTextColor(CR_BLACK);
	}
	else
	{
		m_st_10.SetBkColor(CR_RED);
		m_st_10.SetTextColor(CR_WHITE);
	}
	// MOTOR CURRENT POSITION VALUE
	fValue = (float)CDataManager::Instance()->m_PLCReaderD_Motor.GetValue2(200)/1000.0f;
	strValue.Format(_T("%.3f mm "), fValue);
	m_st_18.SetWindowText(strValue);
	// JOG SPEED
	sValue = CDataManager::Instance()->m_PLCReaderD_Motor.GetValue(218)/10;
	if(sValue)
	{
		strValue.Format(_T("%d "), sValue);
		m_st_22.SetWindowText(strValue);
	}
	// SERVO BUTTON LAMP CHECK
	nServoMAddressBit = 243;
	if(CDataManager::Instance()->PLC_Read_M2(nPLCNum, nServoMAddressBit))
	{
		m_btn_4.SetButtonColor(CR_LIMEGREEN, CR_LIGHTGRAY);
		m_btn_4.SetTextColor(CR_WHITE);
		m_btn_4.Invalidate();
	}
	else
	{
		m_btn_4.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
		m_btn_4.SetTextColor(CR_BLACK);
		m_btn_4.Invalidate();
	}
	// BW BUTTON LAMP CHECK
	nPrevMAddressBit = 242;
	if(CDataManager::Instance()->PLC_Read_M2(nPLCNum, nPrevMAddressBit))
	{
		m_btn_5.SetButtonColor(CR_LIMEGREEN, CR_LIGHTGRAY);
		m_btn_5.SetTextColor(CR_WHITE);
		m_btn_5.Invalidate();
	}
	else
	{
		m_btn_5.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
		m_btn_5.SetTextColor(CR_BLACK);
		m_btn_5.Invalidate();
	}
	// FW BUTTON LAMP CHECK
	nNextMAddressBit = 241;
	if(CDataManager::Instance()->PLC_Read_M2(nPLCNum, nNextMAddressBit))
	{
		m_btn_6.SetButtonColor(CR_LIMEGREEN, CR_LIGHTGRAY);
		m_btn_6.SetTextColor(CR_WHITE);
		m_btn_6.Invalidate();
	}
	else
	{
		m_btn_6.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
		m_btn_6.SetTextColor(CR_BLACK);
		m_btn_6.Invalidate();
	}
	/////////////////////////////////////////////////////////////////////////////////////
	// INDEX _Z /////////////////////////////////////////////////////////////////////////
	stMAddress = CDataManager::Instance()->m_PLCReaderD_Motor.GetValueM(307);
	if(stMAddress.m_xBit_0 == 0) // CCW LIMIT = LIMIT -
	{
		m_st_11.SetBkColor(CR_LAVENDER);
		m_st_11.SetTextColor(CR_BLACK);
	}
	else
	{
		m_st_11.SetBkColor(CR_RED);
		m_st_11.SetTextColor(CR_WHITE);
	}
	if(stMAddress.m_xBit_1 == 0) // CW LIMIT = LIMIT +
	{
		m_st_12.SetBkColor(CR_LAVENDER);
		m_st_12.SetTextColor(CR_BLACK);
	}
	else
	{
		m_st_12.SetBkColor(CR_RED);
		m_st_12.SetTextColor(CR_WHITE);
	}
	if(stMAddress.m_xBit_6 == 0) // HOME
	{
		m_st_13.SetBkColor(CR_LAVENDER);
		m_st_13.SetTextColor(CR_BLACK);
	}
	else
	{
		m_st_13.SetBkColor(CR_RED);
		m_st_13.SetTextColor(CR_WHITE);
	}
	// MOTOR CURRENT POSITION VALUE
	fValue = (float)CDataManager::Instance()->m_PLCReaderD_Motor.GetValue2(300)/1000.0f;
	strValue.Format(_T("%.3f mm "), fValue);
	m_st_19.SetWindowText(strValue);
	// JOG SPEED
	sValue = CDataManager::Instance()->m_PLCReaderD_Motor.GetValue(318)/10;
	if(sValue)
	{
		strValue.Format(_T("%d "), sValue);
		m_st_23.SetWindowText(strValue);
	}
	// SERVO BUTTON LAMP CHECK
	nServoMAddressBit = 343;
	if(CDataManager::Instance()->PLC_Read_M2(nPLCNum, nServoMAddressBit))
	{
		m_btn_7.SetButtonColor(CR_LIMEGREEN, CR_LIGHTGRAY);
		m_btn_7.SetTextColor(CR_WHITE);
		m_btn_7.Invalidate();
	}
	else
	{
		m_btn_7.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
		m_btn_7.SetTextColor(CR_BLACK);
		m_btn_7.Invalidate();
	}
	// BW BUTTON LAMP CHECK
	nPrevMAddressBit = 342;
	if(CDataManager::Instance()->PLC_Read_M2(nPLCNum, nPrevMAddressBit))
	{
		m_btn_8.SetButtonColor(CR_LIMEGREEN, CR_LIGHTGRAY);
		m_btn_8.SetTextColor(CR_WHITE);
		m_btn_8.Invalidate();
	}
	else
	{
		m_btn_8.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
		m_btn_8.SetTextColor(CR_BLACK);
		m_btn_8.Invalidate();
	}
	// FW BUTTON LAMP CHECK
	nNextMAddressBit = 341;
	if(CDataManager::Instance()->PLC_Read_M2(nPLCNum, nNextMAddressBit))
	{
		m_btn_9.SetButtonColor(CR_LIMEGREEN, CR_LIGHTGRAY);
		m_btn_9.SetTextColor(CR_WHITE);
		m_btn_9.Invalidate();
	}
	else
	{
		m_btn_9.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
		m_btn_9.SetTextColor(CR_BLACK);
		m_btn_9.Invalidate();
	}
	/////////////////////////////////////////////////////////////////////////////////////
	// INDEX _T /////////////////////////////////////////////////////////////////////////
	stMAddress = CDataManager::Instance()->m_PLCReaderD_Motor.GetValueM(407);
	if(stMAddress.m_xBit_0 == 0) // CCW LIMIT = LIMIT -
	{
		m_st_14.SetBkColor(CR_LAVENDER);
		m_st_14.SetTextColor(CR_BLACK);
	}
	else
	{
		m_st_14.SetBkColor(CR_RED);
		m_st_14.SetTextColor(CR_WHITE);
	}
	if(stMAddress.m_xBit_1 == 0) // CW LIMIT = LIMIT +
	{
		m_st_15.SetBkColor(CR_LAVENDER);
		m_st_15.SetTextColor(CR_BLACK);
	}
	else
	{
		m_st_15.SetBkColor(CR_RED);
		m_st_15.SetTextColor(CR_WHITE);
	}
	if(stMAddress.m_xBit_6 == 0) // HOME
	{
		m_st_16.SetBkColor(CR_LAVENDER);
		m_st_16.SetTextColor(CR_BLACK);
	}
	else
	{
		m_st_16.SetBkColor(CR_RED);
		m_st_16.SetTextColor(CR_WHITE);
	}
	// MOTOR CURRENT POSITION VALUE
	fValue = (float)CDataManager::Instance()->m_PLCReaderD_Motor.GetValue2(400)/1000.0f;
	strValue.Format(_T("%.3f mm "), fValue);
	m_st_20.SetWindowText(strValue);
	// JOG SPEED
	sValue = CDataManager::Instance()->m_PLCReaderD_Motor.GetValue(418)/10;
	if(sValue)
	{
		strValue.Format(_T("%d "), sValue);
		m_st_24.SetWindowText(strValue);
	}
	// SERVO BUTTON LAMP CHECK
	nServoMAddressBit = 443;
	if(CDataManager::Instance()->PLC_Read_M2(nPLCNum, nServoMAddressBit))
	{
		m_btn_10.SetButtonColor(CR_LIMEGREEN, CR_LIGHTGRAY);
		m_btn_10.SetTextColor(CR_WHITE);
		m_btn_10.Invalidate();
	}
	else
	{
		m_btn_10.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
		m_btn_10.SetTextColor(CR_BLACK);
		m_btn_10.Invalidate();
	}
	// BW BUTTON LAMP CHECK
	nPrevMAddressBit = 442;
	if(CDataManager::Instance()->PLC_Read_M2(nPLCNum, nPrevMAddressBit))
	{
		m_btn_11.SetButtonColor(CR_LIMEGREEN, CR_LIGHTGRAY);
		m_btn_11.SetTextColor(CR_WHITE);
		m_btn_11.Invalidate();
	}
	else
	{
		m_btn_11.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
		m_btn_11.SetTextColor(CR_BLACK);
		m_btn_11.Invalidate();
	}
	// FW BUTTON LAMP CHECK
	nNextMAddressBit = 441;
	if(CDataManager::Instance()->PLC_Read_M2(nPLCNum, nNextMAddressBit))
	{
		m_btn_12.SetButtonColor(CR_LIMEGREEN, CR_LIGHTGRAY);
		m_btn_12.SetTextColor(CR_WHITE);
		m_btn_12.Invalidate();
	}
	else
	{
		m_btn_12.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
		m_btn_12.SetTextColor(CR_BLACK);
		m_btn_12.Invalidate();
	}
	/////////////////////////////////////////////////////////////////////////////////////
	Update_PLC_Value_X();
	Update_PLC_Value_Y();
	Update_PLC_Value_Z();
	Update_PLC_Value_T();
}

void CDlg_MotorStacker::Update_PLC_Value_X()
{
	DWORD dwTextStyle = DT_CENTER|DT_VCENTER|DT_SINGLELINE;
	int nAddress_Pos = 0;
	float fValue = 0.0f;
    // fill rows/cols with text
    for (int row = 0; row < m_GC_1.GetRowCount(); row++) 
	{
        for (int col = 0; col < m_GC_1.GetColumnCount(); col++) 
		{ 
            GV_ITEM Item;
            Item.mask = GVIF_TEXT|GVIF_FORMAT;
            Item.row = row;
            Item.col = col;
			Item.nFormat = dwTextStyle;

			if(row%2 == 0)
			{
				m_GC_1.SetItemBkColour(row, col, CR_SLATEGRAY);
				Item.strText.Format(_T("X%02d"), col+1 + (2-row/2)*10);
			}
			else
			{
				nAddress_Pos = 3500 + col*2 + (2-row/2)*20;
				fValue = (float)CDataManager::Instance()->m_PLCReaderD_Motor.GetValue2(nAddress_Pos)/1000.0f;
				Item.strText.Format(_T("%.3f"), fValue);
			}

			m_GC_1.SetItemFgColour(row, col, RGB(0,0,0));
			m_GC_1.SetItemFont(row, col, CTRL_FONT, 18, FW_BOLD);
            m_GC_1.SetItem(&Item);  
        }
    }

	m_GC_1.Invalidate();
}

void CDlg_MotorStacker::Update_PLC_Value_Y()
{
	DWORD dwTextStyle = DT_CENTER|DT_VCENTER|DT_SINGLELINE;
	int nAddress_Pos = 0;
	float fValue = 0.0f;
    // fill rows/cols with text
    for (int row = 0; row < m_GC_2.GetRowCount(); row++) 
	{
        for (int col = 0; col < m_GC_2.GetColumnCount(); col++) 
		{ 
            GV_ITEM Item;
            Item.mask = GVIF_TEXT|GVIF_FORMAT;
            Item.row = row;
            Item.col = col;
			Item.nFormat = dwTextStyle;

			if(row%2 == 0)
			{
				m_GC_2.SetItemBkColour(row, col, CR_SLATEGRAY);
				Item.strText.Format(_T("Y%02d"), col+1 + (2-row/2)*10);
			}
			else
			{
				nAddress_Pos = 3800 + col*2 + (2-row/2)*20;
				fValue = (float)CDataManager::Instance()->m_PLCReaderD_Motor.GetValue2(nAddress_Pos)/1000.0f;
				Item.strText.Format(_T("%.3f"), fValue);
			}

			m_GC_2.SetItemFgColour(row, col, RGB(0,0,0));
			m_GC_2.SetItemFont(row, col, CTRL_FONT, 18, FW_BOLD);
            m_GC_2.SetItem(&Item);  
        }
    }

	m_GC_2.Invalidate();
}

void CDlg_MotorStacker::Update_PLC_Value_Z()
{
	DWORD dwTextStyle = DT_CENTER|DT_VCENTER|DT_SINGLELINE;
	int nAddress_Pos = 0;
	float fValue = 0.0f;
    // fill rows/cols with text
    for (int row = 0; row < m_GC_3.GetRowCount(); row++) 
	{
        for (int col = 0; col < m_GC_3.GetColumnCount(); col++) 
		{ 
            GV_ITEM Item;
            Item.mask = GVIF_TEXT|GVIF_FORMAT;
            Item.row = row;
            Item.col = col;
			Item.nFormat = dwTextStyle;

			if(row == 0)
			{
				m_GC_3.SetItemBkColour(row, col, CR_SLATEGRAY);
				Item.strText.Format(_T("Z_Up%02d"), col+1);
			}
			else if(row == 1)
			{
				nAddress_Pos = 260 + col*2;
				fValue = (float)CDataManager::Instance()->m_PLCReaderD_Motor.GetValue2(nAddress_Pos)/1000.0f;
				Item.strText.Format(_T("%.3f"), fValue);
			}
			else if(row == 2)
			{
				m_GC_3.SetItemBkColour(row, col, CR_SLATEGRAY);
				Item.strText.Format(_T("Z_Down%02d"), col+1);
			}
			else if(row == 3)
			{
				nAddress_Pos = 250 + col*2;
				fValue = (float)CDataManager::Instance()->m_PLCReaderD_Motor.GetValue2(nAddress_Pos)/1000.0f;
				Item.strText.Format(_T("%.3f"), fValue);
			}
			
			m_GC_3.SetItemFgColour(row, col, RGB(0,0,0));
			m_GC_3.SetItemFont(row, col, CTRL_FONT, 18, FW_BOLD);
            m_GC_3.SetItem(&Item);  
        }
    }

	m_GC_3.Invalidate();
}

void CDlg_MotorStacker::Update_PLC_Value_T()
{
	DWORD dwTextStyle = DT_CENTER|DT_VCENTER|DT_SINGLELINE;
	int nAddress_Pos = 0;
	float fValue = 0.0f;
    // fill rows/cols with text
    for (int row = 0; row < m_GC_4.GetRowCount(); row++) 
	{
        for (int col = 0; col < m_GC_4.GetColumnCount(); col++) 
		{ 
            GV_ITEM Item;
            Item.mask = GVIF_TEXT|GVIF_FORMAT;
            Item.row = row;
            Item.col = col;
			Item.nFormat = dwTextStyle;

			if(row == 0)
			{
				m_GC_4.SetItemBkColour(row, col, CR_SLATEGRAY);
				Item.strText.Format(_T("T%02d"), col+1);
			}
			else if(row == 1)
			{
				nAddress_Pos = 450 + col*2;
				fValue = (float)CDataManager::Instance()->m_PLCReaderD_Motor.GetValue2(nAddress_Pos)/1000.0f;
				Item.strText.Format(_T("%.3f"), fValue);
			}

			m_GC_4.SetItemFgColour(row, col, RGB(0,0,0));
			m_GC_4.SetItemFont(row, col, CTRL_FONT, 18, FW_BOLD);
            m_GC_4.SetItem(&Item);  
        }
    }

	m_GC_4.Invalidate();
}

void CDlg_MotorStacker::OnBnClickedBtnMs1()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}


void CDlg_MotorStacker::OnBnClickedBtnMs2()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}


void CDlg_MotorStacker::OnBnClickedBtnMs3()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}


void CDlg_MotorStacker::OnBnClickedBtnMs4()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}


void CDlg_MotorStacker::OnBnClickedBtnMs5()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}


void CDlg_MotorStacker::OnBnClickedBtnMs6()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}


void CDlg_MotorStacker::OnBnClickedBtnMs7()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}


void CDlg_MotorStacker::OnBnClickedBtnMs8()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}


void CDlg_MotorStacker::OnBnClickedBtnMs9()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}


void CDlg_MotorStacker::OnBnClickedBtnMs10()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}


void CDlg_MotorStacker::OnBnClickedBtnMs11()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}


void CDlg_MotorStacker::OnBnClickedBtnMs12()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}


void CDlg_MotorStacker::OnBnClickedBtnMs13()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CAMKOR_MMIDlg *)AfxGetMainWnd())->SendMessage(WM_CHANGESCREEN, 1, 0);
}
