#pragma once

struct stMotorItem
{
	CString m_strItemName;
	double	m_dItemPostion;
	double	m_dItemSpeed;
	int m_nAddress_Pos;
	int m_nAddress_Spd;

	void Clear()
	{
		m_strItemName.Empty();
		m_dItemPostion = 0.0f;
		m_dItemSpeed = 0.0f;
		m_nAddress_Pos = 0;
		m_nAddress_Spd = 0;
	}
};

struct stMotorInfo
{
	CString m_strMotorName;
	int m_nStartAddressD;
	int m_nStartAddressM;
	int m_nStartAddressL;
	stMotorItem m_stMotorItem[20];

	void Clear()
	{
		m_strMotorName.Empty();
		m_nStartAddressD = 0;
		m_nStartAddressM = 0;
		m_nStartAddressL = 0;
		for(int i=0;i<20;i++) m_stMotorItem[i].Clear();
	}
};

struct stManualItem
{
	CString m_strItemName;
	int m_nAddress_Bit;
	int m_nAddress_Lamp;
	int m_nLampOnOff;

	void Clear()
	{
		m_strItemName.Empty();
		m_nAddress_Bit = 0;
		m_nAddress_Lamp = 0;
		m_nLampOnOff = 0;
	}
};

struct stManualInfo
{
	CString m_strManualName;
	stManualItem m_stManualItem[50];

	void Clear()
	{
		m_strManualName.Empty();
		for(int i=0;i<50;i++) m_stManualItem[i].Clear();
	}
};

struct stMotorManual_Info
{
	CString m_strRecipeName;

	int m_nMotorCount;
	int m_nMotorDAddress_Start;
	int m_nMotorDAddress_End;
	int m_nMotorLAddress_Start;
	int m_nMotorLAddress_End;
	std::vector<stMotorInfo> m_vecMotorInfo;

	int m_nManualCount;
	int m_nManualLampStartAddress;
	int m_nManualLampEndAddress;
	std::vector<stManualInfo> m_vecManualInfo;
};

class CMotorManualSettingGC;
class CMotorManualSetting  
{
public:
	friend class CMotorManualSettingGC;
	static CMotorManualSetting* Instance();

	CString GetExecuteDirectory(); // 현재 실행화일 경로 가져오자.

	void GetFileList();
	std::vector<CString> m_vecFileList;
	CString m_strSelFileName; // 선택된 파일명..
	CString m_strMCHName; // 선택한 장비이름..

	stMotorManual_Info GetMotorManualInfo() { return m_stMMInfo; };

	BOOL Load_MotorSetting();
	BOOL Save_MotorSetting();

	void SetMotorStartDAddress(int nStartAddress);
	void SetMotorEndDAddress(int nEndAddress);
	int GetMotorStartDAddress();
	int GetMotorEndDAddress();
	void SetMotorStartLAddress(int nStartAddress);
	void SetMotorEndLAddress(int nEndAddress);
	int GetMotorStartLAddress();
	int GetMotorEndLAddress();
	void AddMotorInfo(int nAddressD, int nAddressM, int nAddressL);
	void DelMotorInfo();
	void SetMotorItemValue(int nMotorIndex, int nItemIndex, double dPos, double dSpd);
	void SetMotorItemText(int nMotorIndex, int nItemIndex, LPCTSTR strText);
	void SetMotorItemAddress_Spd(int nMotorIndex, int nItemIndex, int nAddress);
	void SetMotorItemAddress_Pos(int nMotorIndex, int nItemIndex, int nAddress);

	BOOL Load_ManualSetting();
	BOOL Save_ManualSetting();

	void SetManualStartLAddress(int nStartAddress);
	void SetManualEndLAddress(int nEndAddress);
	int GetManualStartLAddress();
	int GetManualEndLAddress();
	void SetManualItemStatus(int nManualIndex, int nItemIndex, int nLampOnOff);
	void SetManualItemModify(int nManualIndex, int nItemIndex, LPCTSTR strText, int nBitAddress, int nLampAddress);
	void AddManualInfo(LPCTSTR strManualName);
	void AddManualInfo();
	void DelManualInfo();
	void SetManualNameModify(int nManualIndex, LPCTSTR strManualName);

private:
	static CMotorManualSetting* m_pThis;
	CMotorManualSetting();
	~CMotorManualSetting();

	stMotorManual_Info	m_stMMInfo;
};
