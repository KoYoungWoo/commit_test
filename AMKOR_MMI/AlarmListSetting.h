#pragma once

struct AlarmTable
{
	int nCode;
	CString strText;
};

struct AlarmSolution
{
	int nCode;
	CString strText_P1;
	CString strText_P2;
	CString strText_P3;
	CString strText_S1;
	CString strText_S2;
	CString strText_S3;
};

class CAlarmListGC;
class CAlarmListSetting
{
public:
	friend class CAlarmListGC;
	static CAlarmListSetting* Instance();

	int LoadAlarmList();
	int LoadAlarmList_CSV();
	int LoadAlarmSolution(int nCode);
	int SaveAlarmSolution(AlarmSolution stAlarmSolution);

	void GetErrorTextByCode(int nCode, CString& strErrorText);
	void GetAlarmTableByCode(int nCode, AlarmTable& stAlarmTable);

	void GetAlarmSolutionByCode(int nCode, AlarmSolution& stAlarmSolution);
	
//#if _MSC_VER
//	stdext::hash_map<int, AlarmTable> m_mapAlarmList;
//#else
	std::map<int, AlarmTable> m_mapAlarmList;
//#endif

private:

#if _MSC_VER
	stdext::hash_map<int, AlarmSolution> m_mapAlarmSolution;
#else
	std::map<int, AlarmSolution> m_mapAlarmSolution;
#endif

	static CAlarmListSetting* m_pThis;
	CAlarmListSetting();
	~CAlarmListSetting();

	CString GetExecuteDirectory();
	CStringA GetExecuteDirectoryA();
};

