#include "stdafx.h"
#include "DataManager.h"
#include "Define.h"
#include "AMKOR_MMI.h"
#include "AMKOR_MMIDlg.h"

#define PLC_RETRY_COUNT 5

//////////////////////////////////////////////////////////////////////
// CAppSettingGC
class CDataManagerGC
{
public:
	CDataManagerGC() {}
	~CDataManagerGC() { delete CDataManager::m_pThis; CDataManager::m_pThis = NULL; }
};
CDataManagerGC s_DDMGC;


CDataManager* CDataManager::m_pThis = NULL;

CDataManager::CDataManager(void)
{
	// LOT 정보 구조체 초기화
	for(int i=0;i<TOTAL_LOT_COUNT;i++)
	{
		ZeroMemory(&m_arrayLOTINFO[i],sizeof(stLotInfo));
	}
	// 장비 정보 초기화
	ZeroMemory(&m_stMCHINFO,sizeof(MCH_INFO));
	// 포지션별 자재 정보 - R어드레스 정보 바탕
	for(int i=0;i<16;i++)
	{
		ZeroMemory(&m_arrayTrayPackInfo[i],sizeof(stTrayPackInfo));
	}
	// MES 처리 프로세스 
	ZeroMemory(&m_MES_RFID,sizeof(stMESInfo_READY_TO_LOAD_BARCODE));
}


CDataManager::~CDataManager(void)
{
}

CDataManager* CDataManager::Instance()
{
	if( !m_pThis )
		m_pThis = new CDataManager;
	return m_pThis;
}

void CDataManager::Reset_Data()
{
	// LOT 정보 구조체 초기화
	for(int i=0;i<TOTAL_LOT_COUNT;i++)
	{
		ZeroMemory(&m_arrayLOTINFO[i],sizeof(stLotInfo));
	}
	// 포지션별 자재 정보 - R어드레스 정보 바탕
	for(int i=0;i<16;i++)
	{
		ZeroMemory(&m_arrayTrayPackInfo[i],sizeof(stTrayPackInfo));
	}
	// MES 처리 프로세스 
	ZeroMemory(&m_MES_RFID,sizeof(stMESInfo_READY_TO_LOAD_BARCODE));
}

BOOL CDataManager::OpenDevice_PLC_1( int nLogicalStationNumber )
{ 
	if( m_MxComp3_PLC_1.Open( nLogicalStationNumber ) )
	{
		m_stMCHINFO.m_stConnStatus_Info.m_bConnectStatus[CONN_PLC_1] = TRUE;
		return TRUE;
	}

	return FALSE;
}

BOOL CDataManager::OpenDevice_PLC_2( int nLogicalStationNumber )
{ 
	if( m_MxComp3_PLC_2.Open( nLogicalStationNumber ) )
	{
		m_stMCHINFO.m_stConnStatus_Info.m_bConnectStatus[CONN_PLC_2] = TRUE;
		return TRUE;
	}

	return FALSE;
}

BOOL CDataManager::OpenDevice_PLC_3( int nLogicalStationNumber )
{ 
	if( m_MxComp3_PLC_3.Open( nLogicalStationNumber ) )
	{
		m_stMCHINFO.m_stConnStatus_Info.m_bConnectStatus[CONN_PLC_3] = TRUE;
		return TRUE;
	}

	return FALSE;
}

// PLC 연결상태 끊음
void CDataManager::CloseDeviceAll_PLC()
{
	if(m_stMCHINFO.m_stConnStatus_Info.m_bConnectStatus[CONN_PLC_1])
	{
		if( !CloseDevice_PLC_1() )
		{
			ATLTRACE( _T("[m_MxComp3_PLC_1] Close..Failed") );
		}
		else
			ATLTRACE( _T("[m_MxComp3_PLC_1] Close..OK") );
	
		if( !DestroyDevice_PLC_1() )
		{
			ATLTRACE( _T("[m_MxComp3_PLC_1] Destroy...Failed") );
		}
		else
			ATLTRACE( _T("[m_MxComp3_PLC_1] Destroy...OK") );
	}
	if(m_stMCHINFO.m_stConnStatus_Info.m_bConnectStatus[CONN_PLC_2])
	{
		if( !CloseDevice_PLC_2() )
		{
			ATLTRACE( _T("[m_MxComp3_PLC_2] Close..Failed") );
		}
		else
			ATLTRACE( _T("[m_MxComp3_PLC_2] Close..OK") );
	
		if( !DestroyDevice_PLC_2() )
		{
			ATLTRACE( _T("[m_MxComp3_PLC_2] Destroy...Failed") );
		}
		else
			ATLTRACE( _T("[m_MxComp3_PLC_2] Destroy...OK") );
	}
	if(m_stMCHINFO.m_stConnStatus_Info.m_bConnectStatus[CONN_PLC_3])
	{
		if( !CloseDevice_PLC_3() )
		{
			ATLTRACE( _T("[m_MxComp3_PLC_3] Close..Failed") );
		}
		else
			ATLTRACE( _T("[m_MxComp3_PLC_3] Close..OK") );
	
		if( !DestroyDevice_PLC_3() )
		{
			ATLTRACE( _T("[m_MxComp3_PLC_3] Destroy...Failed") );
		}
		else
			ATLTRACE( _T("[m_MxComp3_PLC_3] Destroy...OK") );
	}
}

// PLC Data Update
void CDataManager::Parse_PLC_Data()
{
	SYSTEMTIME stCurrentTime;
	GetLocalTime(&stCurrentTime);

	// Group1 장비 구동 상태 - 0:STOP, 1:RUN, 2:DOWN, 3:IDLE
	//m_stMCHINFO.m_nStatus = (int)m_PLC_DAddress_G1.m_stG1_MS.m_sMchStatus;

	// Group1 AUTO MODE : 0 - Manual, 1 - Auto
	//m_stMCHINFO.m_nAutoMode = (int)m_PLC_DAddress_G1.m_stG1_MS.m_sAutoMode;
}

void CDataManager::Update_TrayPackPosInfo()
{
	CString strTemp;
	int nDeviceIndex = 0;
	stDeviceTrayInfo stDTI = CDeviceTraySetting::Instance()->GetDeviceTrayInfo();

	if(CAppSetting::Instance()->GetGroupNumber() == 1) 
	{
		for(int i=0;i<8;i++)
		{
			m_arrayTrayPackInfo[i].m_nDeviceIndex = CDataManager::Instance()->m_PLCReaderR_G1.GetValue(106+i*100);
			if(m_arrayTrayPackInfo[i].m_nDeviceIndex == 0)
			{
				ZeroMemory(&m_arrayTrayPackInfo[i], sizeof(stTrayPackInfo));
			}
			else
			{
				// LOTID 읽어들이기
				strTemp = m_PLCReaderR_G1.GetString(160+i*100, 10);
				strTemp.Trim();
				if(strTemp.GetLength() <= 20)
				{
					wcscpy(m_arrayTrayPackInfo[i].m_strLOTID, strTemp.GetBuffer());
				}
				// RFID BCR 읽어들이기
				strTemp = m_PLCReaderR_G1.GetString(140+i*100, 5);
				strTemp.Trim();
				if(strTemp.GetLength() <= 20)
				{
					wcscpy(m_arrayTrayPackInfo[i].m_strRFID, strTemp.GetBuffer());
				}
				// BOXID
				if(i==0 || i==1) m_arrayTrayPackInfo[i].m_nBoxNum = 0;
				else m_arrayTrayPackInfo[i].m_nBoxNum = m_PLCReaderR_G1.GetValue(137+i*100);
				// Tray Index
				m_arrayTrayPackInfo[i].m_nTrayIndex = stDTI.m_vecDeviceInfo[m_arrayTrayPackInfo[i].m_nDeviceIndex-1].m_nTrayIndex;
				// Tray Count
				m_arrayTrayPackInfo[i].m_nTrayCount = CDataManager::Instance()->m_PLCReaderR_G1.GetValue(147+i*100);
			}
		}
		for(int i=0;i<3;i++)
		{
			m_arrayTrayPackInfo[i+8].m_nDeviceIndex = CDataManager::Instance()->m_PLCReaderR_G2.GetValue(106+i*100);
			if(m_arrayTrayPackInfo[i+8].m_nDeviceIndex == 0)
			{
				ZeroMemory(&m_arrayTrayPackInfo[i+8], sizeof(stTrayPackInfo));
			}
			else
			{
				// LOTID 읽어들이기
				strTemp = m_PLCReaderR_G2.GetString(160+i*100, 10);
				strTemp.Trim();
				if(strTemp.GetLength() <= 20)
				{
					wcscpy(m_arrayTrayPackInfo[i+8].m_strLOTID, strTemp.GetBuffer());
				}
				// RFID BCR 읽어들이기
				strTemp = m_PLCReaderR_G2.GetString(140+i*100, 5);
				strTemp.Trim();
				if(strTemp.GetLength() <= 20)
				{
					wcscpy(m_arrayTrayPackInfo[i+8].m_strRFID, strTemp.GetBuffer());
				}
				// BOXID
				m_arrayTrayPackInfo[i+8].m_nBoxNum = m_PLCReaderR_G2.GetValue(137+i*100);
				// Tray Index
				m_arrayTrayPackInfo[i+8].m_nTrayIndex = stDTI.m_vecDeviceInfo[m_arrayTrayPackInfo[i+8].m_nDeviceIndex-1].m_nTrayIndex;
				// Tray Count
				m_arrayTrayPackInfo[i+8].m_nTrayCount = CDataManager::Instance()->m_PLCReaderR_G2.GetValue(147+i*100);
			}
		}
		for(int i=0;i<5;i++)
		{
			m_arrayTrayPackInfo[i+11].m_nDeviceIndex = CDataManager::Instance()->m_PLCReaderR_G3.GetValue(106+i*100);
			if(m_arrayTrayPackInfo[i+11].m_nDeviceIndex == 0)
			{
				ZeroMemory(&m_arrayTrayPackInfo[i+11], sizeof(stTrayPackInfo));
			}
			else
			{
				// LOTID 읽어들이기
				strTemp = m_PLCReaderR_G3.GetString(160+i*100, 10);
				strTemp.Trim();
				if(strTemp.GetLength() <= 20)
				{
					wcscpy(m_arrayTrayPackInfo[i+11].m_strLOTID, strTemp.GetBuffer());
				}
				// RFID BCR 읽어들이기
				strTemp = m_PLCReaderR_G3.GetString(140+i*100, 5);
				strTemp.Trim();
				if(strTemp.GetLength() <= 20)
				{
					wcscpy(m_arrayTrayPackInfo[i+11].m_strRFID, strTemp.GetBuffer());
				}
				// BOXID
				m_arrayTrayPackInfo[i+11].m_nBoxNum = m_PLCReaderR_G3.GetValue(137+i*100);
				// Tray Index
				m_arrayTrayPackInfo[i+11].m_nTrayIndex = stDTI.m_vecDeviceInfo[m_arrayTrayPackInfo[i+11].m_nDeviceIndex-1].m_nTrayIndex;
				// Tray Count
				m_arrayTrayPackInfo[i+11].m_nTrayCount = CDataManager::Instance()->m_PLCReaderR_G3.GetValue(147+i*100);
			}
		}
	}
	else if(CAppSetting::Instance()->GetGroupNumber() == 2) 
	{
		for(int i=0;i<8;i++)
		{
			m_arrayTrayPackInfo[i].m_nDeviceIndex = CDataManager::Instance()->m_PLCReaderR_G2.GetValue(106+i*100);
			if(m_arrayTrayPackInfo[i].m_nDeviceIndex == 0)
			{
				ZeroMemory(&m_arrayTrayPackInfo[i], sizeof(stTrayPackInfo));
			}
			else
			{
				// LOTID 읽어들이기
				strTemp = m_PLCReaderR_G2.GetString(160+i*100, 10);
				strTemp.Trim();
				if(strTemp.GetLength() <= 20)
				{
					wcscpy(m_arrayTrayPackInfo[i].m_strLOTID, strTemp.GetBuffer());
				}
				// RFID BCR 읽어들이기
				strTemp = m_PLCReaderR_G2.GetString(140+i*100, 5);
				strTemp.Trim();
				if(strTemp.GetLength() <= 20)
				{
					wcscpy(m_arrayTrayPackInfo[i].m_strRFID, strTemp.GetBuffer());
				}
				// BOXID
				m_arrayTrayPackInfo[i].m_nBoxNum = m_PLCReaderR_G2.GetValue(137+i*100);
				// Tray Index
				m_arrayTrayPackInfo[i].m_nTrayIndex = stDTI.m_vecDeviceInfo[m_arrayTrayPackInfo[i].m_nDeviceIndex-1].m_nTrayIndex;
				// Tray Count
				m_arrayTrayPackInfo[i].m_nTrayCount = CDataManager::Instance()->m_PLCReaderR_G2.GetValue(147+i*100);
			}
		}
	}
	else if(CAppSetting::Instance()->GetGroupNumber() == 3) 
	{
		for(int i=0;i<8;i++)
		{
			m_arrayTrayPackInfo[i].m_nDeviceIndex = CDataManager::Instance()->m_PLCReaderR_G3.GetValue(106+i*100);
			if(m_arrayTrayPackInfo[i].m_nDeviceIndex == 0)
			{
				ZeroMemory(&m_arrayTrayPackInfo[i], sizeof(stTrayPackInfo));
			}
			else
			{
				// LOTID 읽어들이기
				strTemp = m_PLCReaderR_G3.GetString(160+i*100, 10);
				strTemp.Trim();
				if(strTemp.GetLength() <= 20)
				{
					wcscpy(m_arrayTrayPackInfo[i].m_strLOTID, strTemp.GetBuffer());
				}
				// RFID BCR 읽어들이기
				strTemp = m_PLCReaderR_G3.GetString(140+i*100, 5);
				strTemp.Trim();
				if(strTemp.GetLength() <= 20)
				{
					wcscpy(m_arrayTrayPackInfo[i].m_strRFID, strTemp.GetBuffer());
				}
				// BOXID
				m_arrayTrayPackInfo[i].m_nBoxNum = m_PLCReaderR_G3.GetValue(137+i*100);
				// Tray Index
				m_arrayTrayPackInfo[i].m_nTrayIndex = stDTI.m_vecDeviceInfo[m_arrayTrayPackInfo[i].m_nDeviceIndex-1].m_nTrayIndex;
				// Tray Count
				m_arrayTrayPackInfo[i].m_nTrayCount = CDataManager::Instance()->m_PLCReaderR_G3.GetValue(147+i*100);
			}
		}
	}
}

// PLC READ/WRITE FUNC
BOOL CDataManager::PLC_Read_D(int nPLCNum, int address, int count,SHORT* result)
{
	CString temp;
	BOOL bRet = FALSE;

	temp.Format(_T("D%d"),address);

	if(nPLCNum == 1)
	{
		if(IsConnected(CONN_PLC_1) == FALSE) return FALSE;
		bRet = m_MxComp3_PLC_1.ReadDeviceBlock2(temp,count,result);
	}
	else if(nPLCNum == 2)
	{
		if(IsConnected(CONN_PLC_2) == FALSE) return FALSE;
		bRet = m_MxComp3_PLC_2.ReadDeviceBlock2(temp,count,result);
	}
	else if(nPLCNum == 3)
	{
		if(IsConnected(CONN_PLC_3) == FALSE) return FALSE;
		bRet = m_MxComp3_PLC_3.ReadDeviceBlock2(temp,count,result);
	}
	
	return bRet;
}

BOOL CDataManager::PLC_Read_D2(int nPLCNum, int address,DWORD* result)
{
	CString temp;
	BOOL bRet = FALSE;

	temp.Format(_T("D%d"),address);

	if(nPLCNum == 1)
	{
		if(IsConnected(CONN_PLC_1) == FALSE) return FALSE;
		bRet = m_MxComp3_PLC_1.ReadDeviceBlock2(temp,2,(SHORT*)result);
	}
	else if(nPLCNum == 2)
	{
		if(IsConnected(CONN_PLC_2) == FALSE) return FALSE;
		bRet = m_MxComp3_PLC_2.ReadDeviceBlock2(temp,2,(SHORT*)result);
	}
	else if(nPLCNum == 3)
	{
		if(IsConnected(CONN_PLC_3) == FALSE) return FALSE;
		bRet = m_MxComp3_PLC_3.ReadDeviceBlock2(temp,2,(SHORT*)result);
	}

	return bRet;
}

BOOL CDataManager::PLC_Write_D(int nPLCNum, int address, SHORT input)
{
	CString temp;
	BOOL bRet = FALSE;

	temp.Format(_T("D%d"),address);

	if(nPLCNum == 1)
	{
		if(IsConnected(CONN_PLC_1) == FALSE) return FALSE;
		bRet = m_MxComp3_PLC_1.WriteDeviceBlock2(temp, 1, &input);
	}
	else if(nPLCNum == 2)
	{
		if(IsConnected(CONN_PLC_2) == FALSE) return FALSE;
		bRet = m_MxComp3_PLC_2.WriteDeviceBlock2(temp, 1, &input);
	}
	else if(nPLCNum == 3)
	{
		if(IsConnected(CONN_PLC_3) == FALSE) return FALSE;
		bRet = m_MxComp3_PLC_3.WriteDeviceBlock2(temp, 1, &input);
	}

	return bRet;
}

BOOL CDataManager::PLC_Write_D(int nPLCNum, int address, SHORT* sBuffer, int count)
{
	CString temp;
	BOOL bRet = FALSE;

	temp.Format(_T("D%d"),address);
	SHORT* item = sBuffer;

	if(nPLCNum == 1)
	{
		if(IsConnected(CONN_PLC_1) == FALSE) return FALSE;
		bRet = m_MxComp3_PLC_1.WriteDeviceBlock2(temp, count, item);
	}
	else if(nPLCNum == 2)
	{
		if(IsConnected(CONN_PLC_2) == FALSE) return FALSE;
		bRet = m_MxComp3_PLC_2.WriteDeviceBlock2(temp, count, item);
	}
	else if(nPLCNum == 3)
	{
		if(IsConnected(CONN_PLC_3) == FALSE) return FALSE;
		bRet = m_MxComp3_PLC_3.WriteDeviceBlock2(temp, count, item);
	}

	return bRet;
}

BOOL CDataManager::PLC_Write_D2(int nPLCNum, int address, INT input)
{
	CString temp;
	BOOL bRet = FALSE;

	temp.Format(_T("D%d"),address);
	SHORT* item = (SHORT*)&input;

	if(nPLCNum == 1)
	{
		if(IsConnected(CONN_PLC_1) == FALSE) return FALSE;
		bRet = m_MxComp3_PLC_1.WriteDeviceBlock2(temp, 2, item);
	}
	else if(nPLCNum == 2)
	{
		if(IsConnected(CONN_PLC_2) == FALSE) return FALSE;
		bRet = m_MxComp3_PLC_2.WriteDeviceBlock2(temp, 2, item);
	}
	else if(nPLCNum == 3)
	{
		if(IsConnected(CONN_PLC_3) == FALSE) return FALSE;
		bRet = m_MxComp3_PLC_3.WriteDeviceBlock2(temp, 2, item);
	}

	return bRet;
}

BOOL CDataManager::PLC_Write_D2(int nPLCNum, int address, INT* inputBuffer, int count)
{
	CString temp;
	BOOL bRet = FALSE;

	temp.Format(_T("D%d"),address);
	SHORT* item = (SHORT*)inputBuffer;

	if(nPLCNum == 1)
	{
		if(IsConnected(CONN_PLC_1) == FALSE) return FALSE;
		bRet = m_MxComp3_PLC_1.WriteDeviceBlock2(temp, 2*count, item);
	}
	else if(nPLCNum == 2)
	{
		if(IsConnected(CONN_PLC_2) == FALSE) return FALSE;
		bRet = m_MxComp3_PLC_2.WriteDeviceBlock2(temp, 2*count, item);
	}
	else if(nPLCNum == 3)
	{
		if(IsConnected(CONN_PLC_3) == FALSE) return FALSE;
		bRet = m_MxComp3_PLC_3.WriteDeviceBlock2(temp, 2*count, item);
	}

	return bRet;
}

BOOL CDataManager::PLC_Write_D2(int nPLCNum, int address, LPCTSTR strText, int count)
{
	CString temp;
	BOOL bRet = FALSE;

	temp.Format(_T("D%d"),address);

	CString strTemp;
	strTemp.Format(_T("%s"), strText);
	strTemp.Trim();
	int nTextLength = strTemp.GetLength();
	SHORT strString[10] = {0,};
	int nCount = nTextLength/2;
	if(nTextLength%2) nCount++;

	for(int i=0;i<nCount;i++)
	{
		if(i == nCount-1 && nTextLength%2) strString[i] = MAKEWORD(strTemp[i*2], 0x00);
		else strString[i] = MAKEWORD(strTemp[i*2], strTemp[i*2+1]);
	}

	if(nPLCNum == 1)
	{
		if(IsConnected(CONN_PLC_1) == FALSE) return FALSE;
		bRet = m_MxComp3_PLC_1.WriteDeviceBlock2(temp, nCount, strString);
	}
	else if(nPLCNum == 2)
	{
		if(IsConnected(CONN_PLC_2) == FALSE) return FALSE;
		bRet = m_MxComp3_PLC_2.WriteDeviceBlock2(temp, nCount, strString);
	}
	else if(nPLCNum == 3)
	{
		if(IsConnected(CONN_PLC_3) == FALSE) return FALSE;
		bRet = m_MxComp3_PLC_3.WriteDeviceBlock2(temp, nCount, strString);
	}

	return FALSE;
}

BOOL CDataManager::PLC_Write_ZR(int nPLCNum, int address, SHORT input)
{
	CString temp;
	BOOL bRet = FALSE;

	temp.Format(_T("ZR%d"),address);

	if(nPLCNum == 1)
	{
		if(IsConnected(CONN_PLC_1) == FALSE) return FALSE;
		bRet = m_MxComp3_PLC_1.WriteDeviceBlock2(temp, 1, &input);
	}
	else if(nPLCNum == 2)
	{
		if(IsConnected(CONN_PLC_2) == FALSE) return FALSE;
		bRet = m_MxComp3_PLC_2.WriteDeviceBlock2(temp, 1, &input);
	}
	else if(nPLCNum == 3)
	{
		if(IsConnected(CONN_PLC_3) == FALSE) return FALSE;
		bRet = m_MxComp3_PLC_3.WriteDeviceBlock2(temp, 1, &input);
	}

	return bRet;
}

BOOL CDataManager::PLC_Write_ZR(int nPLCNum, int address, SHORT* inputBuffer, int count)
{
	CString temp;
	BOOL bRet = FALSE;

	temp.Format(_T("ZR%d"),address);
	SHORT* item = (SHORT*)inputBuffer;

	if(nPLCNum == 1)
	{
		if(IsConnected(CONN_PLC_1) == FALSE) return FALSE;
		bRet = m_MxComp3_PLC_1.WriteDeviceBlock2(temp, count, item);
	}
	else if(nPLCNum == 2)
	{
		if(IsConnected(CONN_PLC_2) == FALSE) return FALSE;
		bRet = m_MxComp3_PLC_2.WriteDeviceBlock2(temp, count, item);
	}
	else if(nPLCNum == 3)
	{
		if(IsConnected(CONN_PLC_3) == FALSE) return FALSE;
		bRet = m_MxComp3_PLC_3.WriteDeviceBlock2(temp, count, item);
	}

	return bRet;
}

BOOL CDataManager::PLC_Write_ZR2(int nPLCNum, int address, INT input)
{
	CString temp;
	BOOL bRet = FALSE;

	temp.Format(_T("ZR%d"),address);
	SHORT* item = (SHORT*)&input;

	if(nPLCNum == 1)
	{
		if(IsConnected(CONN_PLC_1) == FALSE) return FALSE;
		bRet = m_MxComp3_PLC_1.WriteDeviceBlock2(temp, 2, item);
	}
	else if(nPLCNum == 2)
	{
		if(IsConnected(CONN_PLC_2) == FALSE) return FALSE;
		bRet = m_MxComp3_PLC_2.WriteDeviceBlock2(temp, 2, item);
	}
	else if(nPLCNum == 3)
	{
		if(IsConnected(CONN_PLC_3) == FALSE) return FALSE;
		bRet = m_MxComp3_PLC_3.WriteDeviceBlock2(temp, 2, item);
	}

	return bRet;
}

BOOL CDataManager::PLC_Write_ZR2(int nPLCNum, int address, SHORT* inputBuffer, int count)
{
	CString temp;
	BOOL bRet = FALSE;

	temp.Format(_T("ZR%d"),address);
	SHORT* item = (SHORT*)inputBuffer;

	if(nPLCNum == 1)
	{
		if(IsConnected(CONN_PLC_1) == FALSE) return FALSE;
		bRet = m_MxComp3_PLC_1.WriteDeviceBlock2(temp, 2*count, item);
	}
	else if(nPLCNum == 2)
	{
		if(IsConnected(CONN_PLC_2) == FALSE) return FALSE;
		bRet = m_MxComp3_PLC_2.WriteDeviceBlock2(temp, 2*count, item);
	}
	else if(nPLCNum == 3)
	{
		if(IsConnected(CONN_PLC_3) == FALSE) return FALSE;
		bRet = m_MxComp3_PLC_3.WriteDeviceBlock2(temp, 2*count, item);
	}

	return bRet;
}

BOOL CDataManager::PLC_Read_M(int nPLCNum, int address, int count, MADDRESS* result)
{
	CString temp;
	BOOL bRet = FALSE;

	temp.Format(_T("M%d"),address);

	if(nPLCNum == 1)
	{
		if(IsConnected(CONN_PLC_1) == FALSE) return FALSE;
		bRet = m_MxComp3_PLC_1.ReadDeviceBlock2(temp,count,(SHORT*)result);
	}
	else if(nPLCNum == 2)
	{
		if(IsConnected(CONN_PLC_2) == FALSE) return FALSE;
		bRet = m_MxComp3_PLC_2.ReadDeviceBlock2(temp,count,(SHORT*)result);
	}
	else if(nPLCNum == 3)
	{
		if(IsConnected(CONN_PLC_3) == FALSE) return FALSE;
		bRet = m_MxComp3_PLC_3.ReadDeviceBlock2(temp,count,(SHORT*)result);
	}

	return bRet;
}

int CDataManager::PLC_Read_M2(int nPLCNum, int address)
{
	int Returnvel = 0;
	CString temp;
	MADDRESS outputM ;
	int address_pos, address_bit;

	address_pos = (address/16)*16;
	address_bit = address%16;
	temp.Format(_T("M%d"),address_pos);

	if(nPLCNum == 1)
	{
		if(IsConnected(CONN_PLC_1) == FALSE) return Returnvel;
		if(m_MxComp3_PLC_1.ReadDeviceBlock2(temp,1,(SHORT*)&outputM) == FALSE) return Returnvel;
	}
	else if(nPLCNum == 2)
	{
		if(IsConnected(CONN_PLC_2) == FALSE) return Returnvel;
		if(m_MxComp3_PLC_2.ReadDeviceBlock2(temp,1,(SHORT*)&outputM) == FALSE) return Returnvel;
	}
	else if(nPLCNum == 3)
	{
		if(IsConnected(CONN_PLC_3) == FALSE) return Returnvel;
		if(m_MxComp3_PLC_3.ReadDeviceBlock2(temp,1,(SHORT*)&outputM) == FALSE) return Returnvel;
	}
	else return Returnvel;

	switch (address_bit)
	{
	case 0:	Returnvel = outputM.m_xBit_0; break;
	case 1:	Returnvel = outputM.m_xBit_1; break;
	case 2:	Returnvel = outputM.m_xBit_2; break;
	case 3:	Returnvel = outputM.m_xBit_3; break;
	case 4:	Returnvel = outputM.m_xBit_4; break;
	case 5:	Returnvel = outputM.m_xBit_5; break;
	case 6:	Returnvel = outputM.m_xBit_6; break;
	case 7:	Returnvel = outputM.m_xBit_7; break;
	case 8:	Returnvel = outputM.m_xBit_8; break;
	case 9:	Returnvel = outputM.m_xBit_9; break;
	case 10: Returnvel = outputM.m_xBit_A;	break;
	case 11: Returnvel = outputM.m_xBit_B;	break;
	case 12: Returnvel = outputM.m_xBit_C;	break;
	case 13: Returnvel = outputM.m_xBit_D;	break;
	case 14: Returnvel = outputM.m_xBit_E;	break;
	case 15: Returnvel = outputM.m_xBit_F;	break;
	default: break;
	}

	return Returnvel;
}

BOOL CDataManager::PLC_Write_M(int nPLCNum, int address, bool input)
{
	BOOL bRet = FALSE;
	int address16, bit;
	CString temp;
	SHORT outputM;

	address16 = (address/16)*16;
	bit = address%16;
	temp.Format(_T("M%d"),address16);

	if(nPLCNum == 1)
	{
		if(IsConnected(CONN_PLC_1) == FALSE) return FALSE;
		if(m_MxComp3_PLC_1.ReadDeviceBlock2(temp,1,&outputM) == FALSE) return FALSE;
	}
	else if(nPLCNum == 2)
	{
		if(IsConnected(CONN_PLC_2) == FALSE) return FALSE;
		if(m_MxComp3_PLC_2.ReadDeviceBlock2(temp,1,&outputM) == FALSE) return FALSE;
	}
	else if(nPLCNum == 3)
	{
		if(IsConnected(CONN_PLC_3) == FALSE) return FALSE;
		if(m_MxComp3_PLC_3.ReadDeviceBlock2(temp,1,&outputM) == FALSE) return FALSE;
	}

	SHORT bitMask = 1 << bit;
	if (input)
		outputM = (outputM & (~bitMask)) | bitMask;
	else
		outputM = (outputM & (~bitMask));

	if(nPLCNum == 1) bRet = m_MxComp3_PLC_1.WriteDeviceBlock2(temp, 1, &outputM);
	else if(nPLCNum == 2) bRet = m_MxComp3_PLC_2.WriteDeviceBlock2(temp, 1, &outputM);
	else if(nPLCNum == 3) bRet = m_MxComp3_PLC_3.WriteDeviceBlock2(temp, 1, &outputM);

	return bRet;
}

BOOL CDataManager::PLC_Read_X(int nPLCNum, int address, int count, MADDRESS* result)
{
	CString temp;
	BOOL bRet = FALSE;

	temp.Format(_T("X%d"),address);

	if(nPLCNum == 1)
	{
		if(IsConnected(CONN_PLC_1) == FALSE) return FALSE;
		bRet = m_MxComp3_PLC_1.ReadDeviceBlock2(temp,count,(SHORT*)result);
	}
	else if(nPLCNum == 2)
	{
		if(IsConnected(CONN_PLC_2) == FALSE) return FALSE;
		bRet = m_MxComp3_PLC_2.ReadDeviceBlock2(temp,count,(SHORT*)result);
	}
	else if(nPLCNum == 3)
	{
		if(IsConnected(CONN_PLC_3) == FALSE) return FALSE;
		bRet = m_MxComp3_PLC_3.ReadDeviceBlock2(temp,count,(SHORT*)result);
	}

	return bRet;
}

BOOL CDataManager::PLC_Read_Y(int nPLCNum, int address, int count, MADDRESS* result)
{
	CString temp;
	BOOL bRet = FALSE;

	temp.Format(_T("Y%d"),address);

	if(nPLCNum == 1)
	{
		if(IsConnected(CONN_PLC_1) == FALSE) return FALSE;
		bRet = m_MxComp3_PLC_1.ReadDeviceBlock2(temp,count,(SHORT*)result);
	}
	else if(nPLCNum == 2)
	{
		if(IsConnected(CONN_PLC_2) == FALSE) return FALSE;
		bRet = m_MxComp3_PLC_2.ReadDeviceBlock2(temp,count,(SHORT*)result);
	}
	else if(nPLCNum == 3)
	{
		if(IsConnected(CONN_PLC_3) == FALSE) return FALSE;
		bRet = m_MxComp3_PLC_3.ReadDeviceBlock2(temp,count,(SHORT*)result);
	}

	return bRet;
}

BOOL CDataManager::PLC_Write_Y(int nPLCNum, CString address_str, bool input)
{
	int address, address16, bit;
	CString temp;
	SHORT outputM;
	BOOL bRet = FALSE;

	address = HexStrToInt(address_str);
	address16 = (address/16)*16;
	bit = address%16;
	temp.Format(_T("Y%X"),address16);

	if(nPLCNum == 1)
	{
		if(IsConnected(CONN_PLC_1) == FALSE) return FALSE;
		if(m_MxComp3_PLC_1.ReadDeviceBlock2(temp,1,&outputM) == FALSE) return FALSE;
	}
	else if(nPLCNum == 2)
	{
		if(IsConnected(CONN_PLC_2) == FALSE) return FALSE;
		if(m_MxComp3_PLC_2.ReadDeviceBlock2(temp,1,&outputM) == FALSE) return FALSE;
	}
	else if(nPLCNum == 3)
	{
		if(IsConnected(CONN_PLC_3) == FALSE) return FALSE;
		if(m_MxComp3_PLC_3.ReadDeviceBlock2(temp,1,&outputM) == FALSE) return FALSE;
	}

	SHORT bitMask = 1 << bit;
	if (input)
		outputM = (outputM & (~bitMask)) | bitMask;
	else
		outputM = (outputM & (~bitMask));

	if(nPLCNum == 1) bRet = m_MxComp3_PLC_1.WriteDeviceBlock2(temp, 1, &outputM);
	else if(nPLCNum == 2) bRet = m_MxComp3_PLC_2.WriteDeviceBlock2(temp, 1, &outputM);
	else if(nPLCNum == 3) bRet = m_MxComp3_PLC_3.WriteDeviceBlock2(temp, 1, &outputM);

	return bRet;
}

int CDataManager::PLC_Read_L2(int nPLCNum, int address)
{
	int Returnvel = 0;
	CString temp;
	MADDRESS outputM;
	int address_pos, address_bit;

	address_pos = (address/16)*16;
	address_bit = address%16;
	temp.Format(_T("L%d"),address_pos);

	if(nPLCNum == 1)
	{
		if(IsConnected(CONN_PLC_1) == FALSE) return FALSE;
		if(m_MxComp3_PLC_1.ReadDeviceBlock2(temp,1,(SHORT*)&outputM) == FALSE) return FALSE;
	}
	else if(nPLCNum == 2)
	{
		if(IsConnected(CONN_PLC_2) == FALSE) return FALSE;
		if(m_MxComp3_PLC_2.ReadDeviceBlock2(temp,1,(SHORT*)&outputM) == FALSE) return FALSE;
	}
	else if(nPLCNum == 3)
	{
		if(IsConnected(CONN_PLC_3) == FALSE) return FALSE;
		if(m_MxComp3_PLC_3.ReadDeviceBlock2(temp,1,(SHORT*)&outputM) == FALSE) return FALSE;
	}

	switch (address_bit)
	{
	case 0:	Returnvel = outputM.m_xBit_0; break;
	case 1:	Returnvel = outputM.m_xBit_1; break;
	case 2:	Returnvel = outputM.m_xBit_2; break;
	case 3:	Returnvel = outputM.m_xBit_3; break;
	case 4:	Returnvel = outputM.m_xBit_4; break;
	case 5:	Returnvel = outputM.m_xBit_5; break;
	case 6:	Returnvel = outputM.m_xBit_6; break;
	case 7:	Returnvel = outputM.m_xBit_7; break;
	case 8:	Returnvel = outputM.m_xBit_8; break;
	case 9:	Returnvel = outputM.m_xBit_9; break;
	case 10: Returnvel = outputM.m_xBit_A;	break;
	case 11: Returnvel = outputM.m_xBit_B;	break;
	case 12: Returnvel = outputM.m_xBit_C;	break;
	case 13: Returnvel = outputM.m_xBit_D;	break;
	case 14: Returnvel = outputM.m_xBit_E;	break;
	case 15: Returnvel = outputM.m_xBit_F;	break;
	default: break;
	}

	return Returnvel;
}

BOOL CDataManager::PLC_Write_L(int nPLCNum, int address, bool input)
{
	int address16, bit;
	CString temp;
	SHORT outputL;
	BOOL bRet = FALSE;

	address16 = (address/16)*16;
	bit = address%16;
	temp.Format(_T("L%d"),address16);

	if(nPLCNum == 1)
	{
		if(IsConnected(CONN_PLC_1) == FALSE) return FALSE;
		if(m_MxComp3_PLC_1.ReadDeviceBlock2(temp,1,&outputL) == FALSE) return FALSE;
	}
	else if(nPLCNum == 2)
	{
		if(IsConnected(CONN_PLC_2) == FALSE) return FALSE;
		if(m_MxComp3_PLC_2.ReadDeviceBlock2(temp,1,&outputL) == FALSE) return FALSE;
	}
	else if(nPLCNum == 3)
	{
		if(IsConnected(CONN_PLC_3) == FALSE) return FALSE;
		if(m_MxComp3_PLC_3.ReadDeviceBlock2(temp,1,&outputL) == FALSE) return FALSE;
	}

	SHORT bitMask = 1 << bit;
	if (input)
		outputL = (outputL & (~bitMask)) | bitMask;
	else
		outputL = (outputL & (~bitMask));

	if(nPLCNum == 1) bRet = m_MxComp3_PLC_1.WriteDeviceBlock2(temp, 1, &outputL);
	else if(nPLCNum == 2) bRet = m_MxComp3_PLC_2.WriteDeviceBlock2(temp, 1, &outputL);
	else if(nPLCNum == 3) bRet = m_MxComp3_PLC_3.WriteDeviceBlock2(temp, 1, &outputL);

	return bRet;
}

//16(string)->10(int)
DWORD CDataManager::HexStrToInt(CString aStr)
{
	DWORD i;
	char *string, *stopstring;
 
	aStr.Delete(0);

	string = (char *)(LPCTSTR)aStr;
	strtod(string, &stopstring);
	i = (DWORD)strtoul(string, &stopstring, 16);
 
	return i;
}

void CDataManager::IsOn16Bit(int nAddress ,int count , MADDRESS* val)
{
	int address = nAddress;
	for(int i = 0; i < count; i++)
	{
		val[i].m_xBit_0 = m_PLCReaderM.IsON(address++);
		val[i].m_xBit_1 = m_PLCReaderM.IsON(address++);
		val[i].m_xBit_2 = m_PLCReaderM.IsON(address++);
		val[i].m_xBit_3 = m_PLCReaderM.IsON(address++);
		val[i].m_xBit_4 = m_PLCReaderM.IsON(address++);
		val[i].m_xBit_5 = m_PLCReaderM.IsON(address++);
		val[i].m_xBit_6 = m_PLCReaderM.IsON(address++);
		val[i].m_xBit_7 = m_PLCReaderM.IsON(address++);
		val[i].m_xBit_8 = m_PLCReaderM.IsON(address++);
		val[i].m_xBit_9 = m_PLCReaderM.IsON(address++);
		val[i].m_xBit_A = m_PLCReaderM.IsON(address++);
		val[i].m_xBit_B = m_PLCReaderM.IsON(address++);
		val[i].m_xBit_C = m_PLCReaderM.IsON(address++);
		val[i].m_xBit_D = m_PLCReaderM.IsON(address++);
		val[i].m_xBit_E = m_PLCReaderM.IsON(address++);
		val[i].m_xBit_F = m_PLCReaderM.IsON(address++);
	}
}

// LOT 관리 함수
BOOL CDataManager::IsExistLotInfo(LPCTSTR strLotID, LPCTSTR strRFID)
{
	for(int i=0;i<TOTAL_LOT_COUNT;i++)
	{
		if(m_arrayLOTINFO[i].m_bLive)
		{
			if(wcscmp(m_arrayLOTINFO[i].m_strLOTID, strLotID) == 0)
			{
				for(int j=0;j<m_arrayLOTINFO[i].m_nInputBundleCount;j++)
				{
					if(wcscmp(m_arrayLOTINFO[i].m_strRFID[j], strRFID) == 0)
					{
						return TRUE;
					}
				}
				return FALSE;
			}
		}
	}

	return FALSE;
}

BOOL CDataManager::IsExistLotInfoByLotID(LPCTSTR strLotID)
{
	for(int i=0;i<TOTAL_LOT_COUNT;i++)
	{
		if(m_arrayLOTINFO[i].m_bLive)
		{
			if(wcscmp(m_arrayLOTINFO[i].m_strLOTID, strLotID) == 0)
			{
				return TRUE;
			}
		}
	}

	return FALSE;
}

BOOL CDataManager::IsExistLotInfoByRFID(LPCTSTR strRFID)
{
	for(int i=0;i<TOTAL_LOT_COUNT;i++)
	{
		if(m_arrayLOTINFO[i].m_bLive)
		{
			for(int j=0;j<m_arrayLOTINFO[i].m_nInputBundleCount;j++)
			{
				if(wcscmp(m_arrayLOTINFO[i].m_strRFID[j], strRFID) == 0)
				{
					return TRUE;
				}
			}
		}
	}

	return FALSE;
}

int CDataManager::FindLotIndexByLotID(LPCTSTR strLotID)
{
	for(int i=0;i<TOTAL_LOT_COUNT;i++)
	{
		if(m_arrayLOTINFO[i].m_bLive)
		{
			if(wcscmp(m_arrayLOTINFO[i].m_strLOTID, strLotID) == 0)
			{
				return i;
			}
		}
	}

	return -1;
}

int CDataManager::FindLotIndexByPos(int nPos)
{
	for(int i=0;i<TOTAL_LOT_COUNT;i++)
	{
		if(m_arrayLOTINFO[i].m_bLive)
		{
			for(int j=0;j<m_arrayLOTINFO[i].m_nInputBundleCount;j++)
			{
				if(nPos >= 1 && nPos <= 2)
				{
					if(m_arrayLOTINFO[i].m_arrayBundleInfo[j].m_bLive && m_arrayLOTINFO[i].m_arrayBundleInfo[j].m_nCurPos == nPos)
					{
						return i;
					}
				}
				else
				{
					for(int k=0;k<CHILD_TP_COUNT;k++)
					if(m_arrayLOTINFO[i].m_arrayBOXInfo[j][k].m_bLive && m_arrayLOTINFO[i].m_arrayBOXInfo[j][k].m_nCurPos == nPos)
					{
						return i;
					}
				}
			}
		}
	}

	return -1;
}

int CDataManager::FindBundleIndexByRFID(int nFindLotIndex, LPCTSTR strRFID)
{
	if(nFindLotIndex < 0) return -1;

	for(int i=0;i<m_arrayLOTINFO[nFindLotIndex].m_nInputBundleCount;i++)
	{
		if(wcscmp(m_arrayLOTINFO[nFindLotIndex].m_strRFID[i], strRFID) == 0)
		{
			return i;
		}
	}

	return -1;
}

int CDataManager::FindBundleIndexByPos(int nFindLotIndex, int nPos)
{
	if(nFindLotIndex < 0) return -1;

	for(int i=0;i<m_arrayLOTINFO[nFindLotIndex].m_nInputBundleCount;i++)
	{
		if(nPos >= 1 && nPos <= 2)
		{
			if(m_arrayLOTINFO[nFindLotIndex].m_arrayBundleInfo[i].m_bLive && m_arrayLOTINFO[nFindLotIndex].m_arrayBundleInfo[i].m_nCurPos == nPos)
			{
				return i;
			}
		}
		else
		{
			for(int k=0;k<CHILD_TP_COUNT;k++)
			{
				if(m_arrayLOTINFO[nFindLotIndex].m_arrayBOXInfo[i][k].m_bLive && m_arrayLOTINFO[i].m_arrayBOXInfo[i][k].m_nCurPos == nPos)
				{
					return i;
				}
			}
		}
	}

	return -1;
}

int CDataManager::FindBoxIndexByBOXID(int nFindLotIndex, int nFindBundleIndex, int nBoxID)
{
	if(nFindLotIndex < 0 || nFindBundleIndex < 0) return -1;
	if(nBoxID <= 0) return -1;

	for(int i=0;i<CHILD_TP_COUNT;i++)
	{
		if(m_arrayLOTINFO[nFindLotIndex].m_arrayBOXInfo[nFindBundleIndex][i].m_bLive)
		{
			if(m_arrayLOTINFO[nFindLotIndex].m_arrayBOXInfo[nFindBundleIndex][i].m_nBoxNum == nBoxID)
			{
				return i;
			}
		}
	}

	return -1;
}

int CDataManager::FindBoxIndexByPos(int nFindLotIndex, int nFindBundleIndex, int nPos)
{
	if(nFindLotIndex < 0 || nFindBundleIndex < 0) return -1;
	if(nPos <= 0) return -1;

	for(int i=0;i<CHILD_TP_COUNT;i++)
	{
		if(m_arrayLOTINFO[nFindLotIndex].m_arrayBOXInfo[nFindBundleIndex][i].m_bLive)
		{
			if(m_arrayLOTINFO[nFindLotIndex].m_arrayBOXInfo[nFindBundleIndex][i].m_nCurPos == nPos)
			{
				return i;
			}
		}
	}

	return -1;
}

int CDataManager::FindEmptyBoxIndex(int nFindLotIndex, int nFindBundleIndex)
{
	if(nFindLotIndex < 0 || nFindBundleIndex < 0) return -1;

	for(int i=0;i<CHILD_TP_COUNT;i++)
	{
		if(m_arrayLOTINFO[nFindLotIndex].m_arrayBOXInfo[nFindBundleIndex][i].m_bLive == FALSE && m_arrayLOTINFO[nFindLotIndex].m_arrayBOXInfo[nFindBundleIndex][i].m_nCurPos == 0)
		{
			return i;
		}
	}

	return -1;
}

BOOL CDataManager::AddLotInfo(stMESInfo_READY_TO_LOAD_BARCODE stMESInfo)
{
	BOOL bExistLot = FALSE;
	int nFindIndex = 0;
	CAMKOR_MMIDlg *pMainWnd = (CAMKOR_MMIDlg *)AfxGetMainWnd();
	for(int i=0;i<TOTAL_LOT_COUNT;i++)
	{
		if(m_arrayLOTINFO[i].m_bLive)
		{
			if(_tcscmp(m_arrayLOTINFO[i].m_strLOTID, stMESInfo.m_strLOTID) == 0)
			{
				bExistLot = TRUE;
				nFindIndex = i;
				break;
			}
		}
	}
	if(bExistLot) // RFID 정보만 추가
	{
		// BUNDLE 정보 추가
		int nDeviceIndex = m_arrayLOTINFO[nFindIndex].m_nDeviceIndex;
		int nTrayIndex = CDeviceTraySetting::Instance()->GetDeviceTrayInfo().m_vecDeviceInfo[nDeviceIndex-1].m_nTrayIndex;
		int nOneTrayChipCount = CDeviceTraySetting::Instance()->GetDeviceTrayInfo().m_vecTrayInfo[nTrayIndex-1].m_nTrayMatrixRow * CDeviceTraySetting::Instance()->GetDeviceTrayInfo().m_vecTrayInfo[nTrayIndex-1].m_nTrayMatrixCol;
		int nBundleIndex = m_arrayLOTINFO[nFindIndex].m_nInputBundleCount;
		_tcscpy(m_arrayLOTINFO[nFindIndex].m_strRFID[nBundleIndex], stMESInfo.m_strRFID);
		m_arrayLOTINFO[nFindIndex].m_nBundleDeviceCount[nBundleIndex] = stMESInfo.m_nBundleDeviceCount;
		m_arrayLOTINFO[nFindIndex].m_nBundleTrayCount[nBundleIndex] = stMESInfo.m_nBundleDeviceCount/nOneTrayChipCount;
		if(stMESInfo.m_nBundleDeviceCount%nOneTrayChipCount > 0) m_arrayLOTINFO[nFindIndex].m_nBundleTrayCount[nBundleIndex] ++;

		m_arrayLOTINFO[nFindIndex].m_arrayBundleInfo[nBundleIndex].m_bLive = TRUE;
		m_arrayLOTINFO[nFindIndex].m_arrayBundleInfo[nBundleIndex].m_nBoxNum = 0;
		m_arrayLOTINFO[nFindIndex].m_arrayBundleInfo[nBundleIndex].m_nCurPos = 1;
		m_arrayLOTINFO[nFindIndex].m_arrayBundleInfo[nBundleIndex].m_nDeviceIndex = nDeviceIndex;
		m_arrayLOTINFO[nFindIndex].m_arrayBundleInfo[nBundleIndex].m_nTrayCount = m_arrayLOTINFO[nFindIndex].m_nBundleTrayCount[nBundleIndex];
		m_arrayLOTINFO[nFindIndex].m_arrayBundleInfo[nBundleIndex].m_nTrayIndex = nTrayIndex;
		_tcscpy(m_arrayLOTINFO[nFindIndex].m_arrayBundleInfo[nBundleIndex].m_strLOTID, m_arrayLOTINFO[nFindIndex].m_strLOTID);
		_tcscpy(m_arrayLOTINFO[nFindIndex].m_arrayBundleInfo[nBundleIndex].m_strRFID, stMESInfo.m_strRFID);

		m_arrayLOTINFO[nFindIndex].m_nInputBundleCount ++;

	}
	else // LOT 추가 및 RFID 정보 추가
	{
		nFindIndex = -1;
		// 빈 LOT INDEX 를 찾는다....
		for(int i=0;i<TOTAL_LOT_COUNT;i++)
		{
			if(m_arrayLOTINFO[i].m_bLive == FALSE)
			{
				nFindIndex = i;
				break;
			}
		}
		if(nFindIndex >= 0)
		{
			int nTrayIndex = 0;
			int nOneTrayChipCount = 0;
			int nDivideCount = 0;

			// LOT 초기화
			ZeroMemory(&m_arrayLOTINFO[nFindIndex],sizeof(stLotInfo));
			// LOT 기본정보
			_tcscpy(m_arrayLOTINFO[nFindIndex].m_strLOTID, stMESInfo.m_strLOTID);
			m_arrayLOTINFO[nFindIndex].m_bLive = TRUE;
			m_arrayLOTINFO[nFindIndex].m_nDeviceIndex = stMESInfo.m_nDeviceIndex;
			m_arrayLOTINFO[nFindIndex].m_nTotalDeviceCount = stMESInfo.m_nTotalDeviceCount;

			nTrayIndex = CDeviceTraySetting::Instance()->GetDeviceTrayInfo().m_vecDeviceInfo[m_arrayLOTINFO[nFindIndex].m_nDeviceIndex-1].m_nTrayIndex;
			nOneTrayChipCount = CDeviceTraySetting::Instance()->GetDeviceTrayInfo().m_vecTrayInfo[nTrayIndex-1].m_nTrayMatrixRow * CDeviceTraySetting::Instance()->GetDeviceTrayInfo().m_vecTrayInfo[nTrayIndex-1].m_nTrayMatrixCol;
			nDivideCount = CDeviceTraySetting::Instance()->GetDeviceTrayInfo().m_vecDeviceInfo[m_arrayLOTINFO[nFindIndex].m_nDeviceIndex-1].m_nTrayDivideCount;
			
			m_arrayLOTINFO[nFindIndex].m_nBoxInDeviceQty = nOneTrayChipCount*nDivideCount;
			m_arrayLOTINFO[nFindIndex].m_nTotalBoxCount = m_arrayLOTINFO[nFindIndex].m_nTotalDeviceCount/m_arrayLOTINFO[nFindIndex].m_nBoxInDeviceQty;
			if(m_arrayLOTINFO[nFindIndex].m_nTotalDeviceCount%m_arrayLOTINFO[nFindIndex].m_nBoxInDeviceQty > 0) m_arrayLOTINFO[nFindIndex].m_nTotalBoxCount ++;
			m_arrayLOTINFO[nFindIndex].m_nCurProcess = MES_ZEBRA_CODE_REQ;
			for(int j=0;j<5;j++)
			{
				_tcscpy(m_arrayLOTINFO[nFindIndex].m_strChipOCR[j], stMESInfo.m_strChipOCR[j]);
			} 
			// LOT 생성시간
			CString strDate, strTime;
			SYSTEMTIME st;
			GetLocalTime(&st);
			strDate.Format(_T("%04d%02d%02d"), st.wYear, st.wMonth, st.wDay);
			strTime.Format(_T("%02d%02d%02d"), st.wHour, st.wMinute, st.wSecond);
			_tcscpy(m_arrayLOTINFO[nFindIndex].m_strCreateDate, strDate);
			_tcscpy(m_arrayLOTINFO[nFindIndex].m_strCreateTime, strTime);

			m_stMCHINFO.m_stWorkInfo.m_nTotal_LOT_INPUT ++;
			pMainWnd->DB_UPDATE_WORKINFO();
			// LOG 생성
			GetLog()->Debug(_T("WorkInfo - LOT INPUT [%s]"), stMESInfo.m_strLOTID);
			CLogList::Instance()->Add_LogData(eLog_DEBUG, _T("WorkInfo - LOT INPUT [%s]"), stMESInfo.m_strLOTID);

			// BUNDLE 정보 추가
			int nDeviceIndex = m_arrayLOTINFO[nFindIndex].m_nDeviceIndex;
			nTrayIndex = CDeviceTraySetting::Instance()->GetDeviceTrayInfo().m_vecDeviceInfo[nDeviceIndex-1].m_nTrayIndex;
			nOneTrayChipCount = CDeviceTraySetting::Instance()->GetDeviceTrayInfo().m_vecTrayInfo[nTrayIndex-1].m_nTrayMatrixRow * CDeviceTraySetting::Instance()->GetDeviceTrayInfo().m_vecTrayInfo[nTrayIndex-1].m_nTrayMatrixCol;
			int nBundleIndex = m_arrayLOTINFO[nFindIndex].m_nInputBundleCount;
			_tcscpy(m_arrayLOTINFO[nFindIndex].m_strRFID[nBundleIndex], stMESInfo.m_strRFID);
			m_arrayLOTINFO[nFindIndex].m_nBundleDeviceCount[nBundleIndex] = stMESInfo.m_nBundleDeviceCount;
			m_arrayLOTINFO[nFindIndex].m_nBundleTrayCount[nBundleIndex] = stMESInfo.m_nBundleDeviceCount/nOneTrayChipCount;
			if(stMESInfo.m_nBundleDeviceCount%nOneTrayChipCount > 0) m_arrayLOTINFO[nFindIndex].m_nBundleTrayCount[nBundleIndex] ++;

			m_arrayLOTINFO[nFindIndex].m_arrayBundleInfo[nBundleIndex].m_bLive = TRUE;
			m_arrayLOTINFO[nFindIndex].m_arrayBundleInfo[nBundleIndex].m_nBoxNum = 0;
			m_arrayLOTINFO[nFindIndex].m_arrayBundleInfo[nBundleIndex].m_nCurPos = TPOS_G1_POS1;
			m_arrayLOTINFO[nFindIndex].m_arrayBundleInfo[nBundleIndex].m_nDeviceIndex = nDeviceIndex;
			m_arrayLOTINFO[nFindIndex].m_arrayBundleInfo[nBundleIndex].m_nTrayCount = m_arrayLOTINFO[nFindIndex].m_nBundleTrayCount[nBundleIndex];
			m_arrayLOTINFO[nFindIndex].m_arrayBundleInfo[nBundleIndex].m_nTrayIndex = nTrayIndex;
			_tcscpy(m_arrayLOTINFO[nFindIndex].m_arrayBundleInfo[nBundleIndex].m_strLOTID, m_arrayLOTINFO[nFindIndex].m_strLOTID);
			_tcscpy(m_arrayLOTINFO[nFindIndex].m_arrayBundleInfo[nBundleIndex].m_strRFID, stMESInfo.m_strRFID);

			m_arrayLOTINFO[nFindIndex].m_nInputBundleCount ++;
		}
		else
		{
			// LOG 생성
			GetLog()->Debug(_T("Add LotInfo Fail - Full LotInfo : Lot[%s],Rfid[%s]"), stMESInfo.m_strLOTID, stMESInfo.m_strRFID);
			CLogList::Instance()->Add_LogData(eLog_DEBUG, _T("Add LotInfo Fail - Full LotInfo : Lot[%s],Rfid[%s]"), stMESInfo.m_strLOTID, stMESInfo.m_strRFID);

			return FALSE;
		}
	}

	return TRUE;
}

void CDataManager::UpdateLotInfo()
{
	CString strTemp;
	int nDeviceIndex = 0;
	int nFindLotIndex = -1;
	int nFindBundleIndex = -1;
	int nFindBoxIndex = -1;
	stDeviceTrayInfo stDTI = CDeviceTraySetting::Instance()->GetDeviceTrayInfo();
	CAMKOR_MMIDlg *pMainWnd = (CAMKOR_MMIDlg *)AfxGetMainWnd();

	if(CAppSetting::Instance()->GetGroupNumber() == 1) 
	{
		// Bundle 정보 갱신
		if(m_arrayTrayPackInfo[TPOS_G1_POS2-1].m_nDeviceIndex != 0)
		{
			nFindLotIndex = FindLotIndexByLotID(m_arrayTrayPackInfo[TPOS_G1_POS2-1].m_strLOTID);
			nFindBundleIndex = FindBundleIndexByRFID(nFindLotIndex, m_arrayTrayPackInfo[TPOS_G1_POS2-1].m_strRFID);
			if(nFindLotIndex >= 0 && nFindBundleIndex >= 0)
			{
				m_arrayLOTINFO[nFindLotIndex].m_arrayBundleInfo[nFindBundleIndex].m_nCurPos = TPOS_G1_POS2;
			}
			
		}
		else
		{
			nFindLotIndex = FindLotIndexByPos(TPOS_G1_POS2);
			nFindBundleIndex = FindBundleIndexByPos(nFindLotIndex, TPOS_G1_POS2);
			if(nFindLotIndex >= 0 && nFindBundleIndex >= 0)
			{
				m_arrayLOTINFO[nFindLotIndex].m_arrayBundleInfo[nFindBundleIndex].m_bLive = FALSE;
			}
		}
		if(m_arrayTrayPackInfo[TPOS_G1_POS1-1].m_nDeviceIndex != 0)
		{
			nFindLotIndex = FindLotIndexByLotID(m_arrayTrayPackInfo[TPOS_G1_POS1-1].m_strLOTID);
			nFindBundleIndex = FindBundleIndexByRFID(nFindLotIndex, m_arrayTrayPackInfo[TPOS_G1_POS1-1].m_strRFID);
			if(nFindLotIndex >= 0 && nFindBundleIndex >= 0)
			{
				m_arrayLOTINFO[nFindLotIndex].m_arrayBundleInfo[nFindBundleIndex].m_nCurPos = TPOS_G1_POS1;
			}
		}

		// Box 정보 갱신 - 임시로 1G 만 처리하자...나중에 풀기
		if(m_arrayTrayPackInfo[TPOS_G1_POS8-1].m_nDeviceIndex != 0)
		{
			nFindLotIndex = FindLotIndexByLotID(m_arrayTrayPackInfo[TPOS_G1_POS8-1].m_strLOTID);
			nFindBundleIndex = FindBundleIndexByRFID(nFindLotIndex, m_arrayTrayPackInfo[TPOS_G1_POS8-1].m_strRFID);
			if(nFindLotIndex != -1 && nFindBundleIndex != -1)
			{
				nFindBoxIndex = FindBoxIndexByBOXID(nFindLotIndex, nFindBundleIndex, m_arrayTrayPackInfo[TPOS_G1_POS8-1].m_nBoxNum);
				if(nFindBoxIndex != -1) // 기존 존재하는 박스정보이므로 이동위치 갱신...
				{
					m_arrayLOTINFO[nFindLotIndex].m_arrayBOXInfo[nFindBundleIndex][nFindBoxIndex].m_nCurPos = TPOS_G1_POS8;
				}
			}
		}
		else // 임시 8번위치서 데이터가 없어지면 박스 정보 완료로 처리...
		{
			nFindLotIndex = FindLotIndexByPos(TPOS_G1_POS8);
			nFindBundleIndex = FindBundleIndexByPos(nFindLotIndex, TPOS_G1_POS8);
			if(nFindLotIndex >= 0 && nFindBundleIndex >= 0)
			{
				nFindBoxIndex = FindBoxIndexByPos(nFindLotIndex, nFindBundleIndex, TPOS_G1_POS8);
				if(nFindBoxIndex != -1) // 박스 배출 처리
				{
					m_arrayLOTINFO[nFindLotIndex].m_arrayBOXInfo[nFindBundleIndex][nFindBoxIndex].m_nCurPos = TPOS_MAX;
					m_arrayLOTINFO[nFindLotIndex].m_arrayBOXInfo[nFindBundleIndex][nFindBoxIndex].m_bComplete = TRUE;
					m_arrayLOTINFO[nFindLotIndex].m_arrayBOXInfo[nFindBundleIndex][nFindBoxIndex].m_bLive = FALSE;

					// WORK INFO 박스 배출 처리
					m_stMCHINFO.m_stWorkInfo.m_nTotal_BOX_OUTPUT ++;
					pMainWnd->DB_UPDATE_WORKINFO();
					// LOG 생성
					GetLog()->Debug(_T("WorkInfo - BOX OUTPUT [%s][%s][%03d]"), m_arrayLOTINFO[nFindLotIndex].m_arrayBOXInfo[nFindBundleIndex][nFindBoxIndex].m_strLOTID,
																			m_arrayLOTINFO[nFindLotIndex].m_arrayBOXInfo[nFindBundleIndex][nFindBoxIndex].m_strRFID,
																			m_arrayLOTINFO[nFindLotIndex].m_arrayBOXInfo[nFindBundleIndex][nFindBoxIndex].m_nBoxNum);
					CLogList::Instance()->Add_LogData(eLog_DEBUG, _T("WorkInfo - BOX OUTPUT [%s][%s][%03d]"), m_arrayLOTINFO[nFindLotIndex].m_arrayBOXInfo[nFindBundleIndex][nFindBoxIndex].m_strLOTID,
																		m_arrayLOTINFO[nFindLotIndex].m_arrayBOXInfo[nFindBundleIndex][nFindBoxIndex].m_strRFID,
																		m_arrayLOTINFO[nFindLotIndex].m_arrayBOXInfo[nFindBundleIndex][nFindBoxIndex].m_nBoxNum);

					m_arrayLOTINFO[nFindLotIndex].m_nOutputBoxCount ++;
					// 배출된 박스 + 제거된 박스수량 이 전체 박스수량이랑 같다면 LOT 완료 프로세스 진행해야한다.
					if(m_arrayLOTINFO[nFindLotIndex].m_nTotalBoxCount == m_arrayLOTINFO[nFindLotIndex].m_nOutputBoxCount + m_arrayLOTINFO[nFindLotIndex].m_nDeleteBoxCount)
					{
						// MES_LOTEND();
						
						m_stMCHINFO.m_stWorkInfo.m_nTotal_LOT_OUTPUT ++;
						pMainWnd->DB_UPDATE_WORKINFO();
						// LOG 생성
						GetLog()->Debug(_T("WorkInfo - LOT OUTPUT [%s]"), m_arrayLOTINFO[nFindLotIndex].m_strLOTID);
						CLogList::Instance()->Add_LogData(eLog_DEBUG, _T("WorkInfo - LOT OUTPUT [%s]"), m_arrayLOTINFO[nFindLotIndex].m_strLOTID);
						
						// 임시로 LOT 정보 제거
						ZeroMemory(&m_arrayLOTINFO[nFindLotIndex],sizeof(stLotInfo));
						// LOT LIST UPDATE
						 
					}
				}
			}
		}
		if(m_arrayTrayPackInfo[TPOS_G1_POS7-1].m_nDeviceIndex != 0)
		{
			nFindLotIndex = FindLotIndexByLotID(m_arrayTrayPackInfo[TPOS_G1_POS7-1].m_strLOTID);
			nFindBundleIndex = FindBundleIndexByRFID(nFindLotIndex, m_arrayTrayPackInfo[TPOS_G1_POS7-1].m_strRFID);
			if(nFindLotIndex != -1 && nFindBundleIndex != -1)
			{
				nFindBoxIndex = FindBoxIndexByBOXID(nFindLotIndex, nFindBundleIndex, m_arrayTrayPackInfo[TPOS_G1_POS7-1].m_nBoxNum);
				if(nFindBoxIndex != -1) // 기존 존재하는 박스정보이므로 이동위치 갱신...
				{
					m_arrayLOTINFO[nFindLotIndex].m_arrayBOXInfo[nFindBundleIndex][nFindBoxIndex].m_nCurPos = TPOS_G1_POS7;
				}
			}
		}
		if(m_arrayTrayPackInfo[TPOS_G1_POS6-1].m_nDeviceIndex != 0)
		{
			nFindLotIndex = FindLotIndexByLotID(m_arrayTrayPackInfo[TPOS_G1_POS6-1].m_strLOTID);
			nFindBundleIndex = FindBundleIndexByRFID(nFindLotIndex, m_arrayTrayPackInfo[TPOS_G1_POS6-1].m_strRFID);
			if(nFindLotIndex != -1 && nFindBundleIndex != -1)
			{
				nFindBoxIndex = FindBoxIndexByBOXID(nFindLotIndex, nFindBundleIndex, m_arrayTrayPackInfo[TPOS_G1_POS6-1].m_nBoxNum);
				if(nFindBoxIndex != -1) // 기존 존재하는 박스정보이므로 이동위치 갱신...
				{
					m_arrayLOTINFO[nFindLotIndex].m_arrayBOXInfo[nFindBundleIndex][nFindBoxIndex].m_nCurPos = TPOS_G1_POS6;
				}
			}
		}
		if(m_arrayTrayPackInfo[TPOS_G1_POS5-1].m_nDeviceIndex != 0)
		{
			nFindLotIndex = FindLotIndexByLotID(m_arrayTrayPackInfo[TPOS_G1_POS5-1].m_strLOTID);
			nFindBundleIndex = FindBundleIndexByRFID(nFindLotIndex, m_arrayTrayPackInfo[TPOS_G1_POS5-1].m_strRFID);
			if(nFindLotIndex != -1 && nFindBundleIndex != -1)
			{
				nFindBoxIndex = FindBoxIndexByBOXID(nFindLotIndex, nFindBundleIndex, m_arrayTrayPackInfo[TPOS_G1_POS5-1].m_nBoxNum);
				if(nFindBoxIndex != -1) // 기존 존재하는 박스정보이므로 이동위치 갱신...
				{
					m_arrayLOTINFO[nFindLotIndex].m_arrayBOXInfo[nFindBundleIndex][nFindBoxIndex].m_nCurPos = TPOS_G1_POS5;
				}
			}
		}
		if(m_arrayTrayPackInfo[TPOS_G1_POS4-1].m_nDeviceIndex != 0)
		{
			nFindLotIndex = FindLotIndexByLotID(m_arrayTrayPackInfo[TPOS_G1_POS4-1].m_strLOTID);
			nFindBundleIndex = FindBundleIndexByRFID(nFindLotIndex, m_arrayTrayPackInfo[TPOS_G1_POS4-1].m_strRFID);
			if(nFindLotIndex != -1 && nFindBundleIndex != -1)
			{
				nFindBoxIndex = FindBoxIndexByBOXID(nFindLotIndex, nFindBundleIndex, m_arrayTrayPackInfo[TPOS_G1_POS4-1].m_nBoxNum);
				if(nFindBoxIndex != -1) // 기존 존재하는 박스정보이므로 이동위치 갱신...
				{
					m_arrayLOTINFO[nFindLotIndex].m_arrayBOXInfo[nFindBundleIndex][nFindBoxIndex].m_nCurPos = TPOS_G1_POS4;
				}
			}
		}
		if(m_arrayTrayPackInfo[TPOS_G1_POS3-1].m_nDeviceIndex != 0)
		{
			nFindLotIndex = FindLotIndexByLotID(m_arrayTrayPackInfo[TPOS_G1_POS3-1].m_strLOTID);
			nFindBundleIndex = FindBundleIndexByRFID(nFindLotIndex, m_arrayTrayPackInfo[TPOS_G1_POS3-1].m_strRFID);
			if(nFindLotIndex != -1 && nFindBundleIndex != -1)
			{
				nFindBoxIndex = FindBoxIndexByBOXID(nFindLotIndex, nFindBundleIndex, m_arrayTrayPackInfo[TPOS_G1_POS3-1].m_nBoxNum);
				if(nFindBoxIndex != -1) // 기존 존재하는 박스정보이므로 이동위치 갱신...
				{
					m_arrayLOTINFO[nFindLotIndex].m_arrayBOXInfo[nFindBundleIndex][nFindBoxIndex].m_nCurPos = TPOS_G1_POS3;
				}
				else // 신규로 소분된 박스이므로 새로 등록하자...
				{
					int nNewBoxIndex = FindEmptyBoxIndex(nFindLotIndex, nFindBundleIndex);
					if(nNewBoxIndex != -1)
					{
						m_arrayLOTINFO[nFindLotIndex].m_arrayBOXInfo[nFindBundleIndex][nNewBoxIndex].m_bLive = TRUE;
						_tcscpy(m_arrayLOTINFO[nFindLotIndex].m_arrayBOXInfo[nFindBundleIndex][nNewBoxIndex].m_strLOTID, m_arrayLOTINFO[nFindLotIndex].m_strLOTID);
						_tcscpy(m_arrayLOTINFO[nFindLotIndex].m_arrayBOXInfo[nFindBundleIndex][nNewBoxIndex].m_strRFID, m_arrayLOTINFO[nFindLotIndex].m_strRFID[nFindBundleIndex]);
						m_arrayLOTINFO[nFindLotIndex].m_arrayBOXInfo[nFindBundleIndex][nNewBoxIndex].m_nDeviceIndex = m_arrayLOTINFO[nFindLotIndex].m_nDeviceIndex;
						
						m_arrayLOTINFO[nFindLotIndex].m_arrayBOXInfo[nFindBundleIndex][nNewBoxIndex].m_nTrayCount = m_arrayTrayPackInfo[TPOS_G1_POS3-1].m_nTrayCount;
						m_arrayLOTINFO[nFindLotIndex].m_arrayBOXInfo[nFindBundleIndex][nNewBoxIndex].m_nTrayIndex = m_arrayTrayPackInfo[TPOS_G1_POS3-1].m_nTrayIndex;
						m_arrayLOTINFO[nFindLotIndex].m_arrayBOXInfo[nFindBundleIndex][nNewBoxIndex].m_nBoxNum = m_arrayTrayPackInfo[TPOS_G1_POS3-1].m_nBoxNum;
						m_arrayLOTINFO[nFindLotIndex].m_arrayBOXInfo[nFindBundleIndex][nNewBoxIndex].m_nCurPos = TPOS_G1_POS3;

						if(m_arrayTrayPackInfo[TPOS_G1_POS3-1].m_nBoxNum == m_arrayLOTINFO[nFindLotIndex].m_nTotalBoxCount) m_arrayLOTINFO[nFindLotIndex].m_bLastBoxIDMake = TRUE;
						else m_arrayLOTINFO[nFindLotIndex].m_nCurMakeBoxID = m_arrayTrayPackInfo[TPOS_G1_POS3-1].m_nBoxNum;

						m_arrayLOTINFO[nFindLotIndex].m_nMakeBoxCount ++;

						m_stMCHINFO.m_stWorkInfo.m_nTotal_BOX_INPUT ++;
						pMainWnd->DB_UPDATE_WORKINFO();
						// LOG 생성
						GetLog()->Debug(_T("WorkInfo - BOX INPUT [%s][%s][%03d]"), m_arrayLOTINFO[nFindLotIndex].m_arrayBOXInfo[nFindBundleIndex][nNewBoxIndex].m_strLOTID,
																				m_arrayLOTINFO[nFindLotIndex].m_arrayBOXInfo[nFindBundleIndex][nNewBoxIndex].m_strRFID,
																				m_arrayLOTINFO[nFindLotIndex].m_arrayBOXInfo[nFindBundleIndex][nNewBoxIndex].m_nBoxNum);
						CLogList::Instance()->Add_LogData(eLog_DEBUG, _T("WorkInfo - BOX INPUT [%s][%s][%03d]"), m_arrayLOTINFO[nFindLotIndex].m_arrayBOXInfo[nFindBundleIndex][nNewBoxIndex].m_strLOTID,
																				m_arrayLOTINFO[nFindLotIndex].m_arrayBOXInfo[nFindBundleIndex][nNewBoxIndex].m_strRFID,
																				m_arrayLOTINFO[nFindLotIndex].m_arrayBOXInfo[nFindBundleIndex][nNewBoxIndex].m_nBoxNum);
					}
				}
			}
		}
	}
}

void CDataManager::DeleteBoxInfo(int nPos)
{
	int nFindLotIndex, nFindBundleIndex, nFindBoxIndex;
	CAMKOR_MMIDlg *pMainWnd = (CAMKOR_MMIDlg *)AfxGetMainWnd();

	nFindLotIndex = FindLotIndexByPos(nPos);
	nFindBundleIndex = FindBundleIndexByPos(nFindLotIndex, nPos);
	if(nFindLotIndex >= 0 && nFindBundleIndex >= 0)
	{
		nFindBoxIndex = FindBoxIndexByPos(nFindLotIndex, nFindBundleIndex, nPos);
		if(nFindBoxIndex != -1) // 박스 배출 처리
		{
			m_arrayLOTINFO[nFindLotIndex].m_arrayBOXInfo[nFindBundleIndex][nFindBoxIndex].m_bComplete = TRUE;
			m_arrayLOTINFO[nFindLotIndex].m_arrayBOXInfo[nFindBundleIndex][nFindBoxIndex].m_bForceDelete = TRUE;
			m_arrayLOTINFO[nFindLotIndex].m_arrayBOXInfo[nFindBundleIndex][nFindBoxIndex].m_bLive = FALSE;

			// WORK INFO 박스 배출 처리
			m_stMCHINFO.m_stWorkInfo.m_nTotal_BOX_DELETE ++;
			pMainWnd->DB_UPDATE_WORKINFO();
			// LOG 생성
			GetLog()->Debug(_T("WorkInfo - BOX DELETE [%s][%s][%03d]"), m_arrayLOTINFO[nFindLotIndex].m_arrayBOXInfo[nFindBundleIndex][nFindBoxIndex].m_strLOTID,
																	m_arrayLOTINFO[nFindLotIndex].m_arrayBOXInfo[nFindBundleIndex][nFindBoxIndex].m_strRFID,
																	m_arrayLOTINFO[nFindLotIndex].m_arrayBOXInfo[nFindBundleIndex][nFindBoxIndex].m_nBoxNum);
			CLogList::Instance()->Add_LogData(eLog_DEBUG, _T("WorkInfo - BOX DELETE [%s][%s][%03d]"), m_arrayLOTINFO[nFindLotIndex].m_arrayBOXInfo[nFindBundleIndex][nFindBoxIndex].m_strLOTID,
																m_arrayLOTINFO[nFindLotIndex].m_arrayBOXInfo[nFindBundleIndex][nFindBoxIndex].m_strRFID,
																m_arrayLOTINFO[nFindLotIndex].m_arrayBOXInfo[nFindBundleIndex][nFindBoxIndex].m_nBoxNum);

			m_arrayLOTINFO[nFindLotIndex].m_nDeleteBoxCount ++;
			// 배출된 박스 + 제거된 박스수량 이 전체 박스수량이랑 같다면 LOT 완료 프로세스 진행해야한다.
			if(m_arrayLOTINFO[nFindLotIndex].m_nTotalBoxCount == m_arrayLOTINFO[nFindLotIndex].m_nOutputBoxCount + m_arrayLOTINFO[nFindLotIndex].m_nDeleteBoxCount)
			{
				// MES_LOTEND();
						
				m_stMCHINFO.m_stWorkInfo.m_nTotal_LOT_OUTPUT ++;
				pMainWnd->DB_UPDATE_WORKINFO();
				// LOG 생성
				GetLog()->Debug(_T("WorkInfo - LOT OUTPUT [%s]"), m_arrayLOTINFO[nFindLotIndex].m_strLOTID);
				CLogList::Instance()->Add_LogData(eLog_DEBUG, _T("WorkInfo - LOT OUTPUT [%s]"), m_arrayLOTINFO[nFindLotIndex].m_strLOTID);
						
				// 임시로 LOT 정보 제거
				ZeroMemory(&m_arrayLOTINFO[nFindLotIndex],sizeof(stLotInfo));
				// LOT LIST UPDATE
						 
			}
		}
	}
}