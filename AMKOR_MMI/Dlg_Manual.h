#pragma once
#include "afxwin.h"
#include "UIExt/MatrixStatic.h"
#include "UIExt/CxStatic.h"
#include "UIExt/IconButton.h"
#include "UIExt/XPGroupBox.h"
#include "UIExt\/BorderStyleEdit.h"
#include "GC/gridctrl.h"

// CDlg_Manual 대화 상자입니다.

class CDlg_Manual : public CDialogEx
{
	DECLARE_DYNAMIC(CDlg_Manual)

public:
	CDlg_Manual(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CDlg_Manual();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DLG_MANUAL };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);

	void Cal_Area();
	void Cal_CtrlArea();
	void Cal_CtrlArea_Left();
	void Cal_CtrlArea_Mid();
	void Cal_CtrlArea_Right();

	bool Init_GC_1(int nRows, int nCols);
	afx_msg void OnGridClick_GC_1(NMHDR *pNotifyStruct, LRESULT* pResult);
	bool Init_GC_2(int nRows, int nCols);
	afx_msg void OnGridClick_GC_2(NMHDR *pNotifyStruct, LRESULT* pResult);

	void SetManualGroupSettingValue();
	void SetManualItemSettingValue(int nGroupIndex);
	void Update_SelItemIndex(int nItemIndex);
	void Update_PLC_Value();

	CRect m_rcLeft;
	CRect m_rcMid;
	CRect m_rcRight;

	BOOL m_bCtrl_FirstLoad;

	int m_nCurGroupPageNum;
	int m_nSelGroupIndex;
	int m_nSelItemIndex;
	int m_nCurItemPageNum;

	CXPGroupBox m_gb_1;
	CXPGroupBox m_gb_2;
	CXPGroupBox m_gb_3;
	CGridCtrl m_GC_1;
	CGridCtrl m_GC_2;
	CIconButton m_btn_1;
	CIconButton m_btn_2;
	CIconButton m_btn_3;
	CIconButton m_btn_4;
	CIconButton m_btn_5;
	CIconButton m_btn_6;
	CIconButton m_btn_7;
	CIconButton m_btn_8;
	CIconButton m_btn_9;
	CIconButton m_btn_10;
	CIconButton m_btn_11;
	CIconButton m_btn_12;
	CxStatic m_st_T1;
	CxStatic m_st_T2;
	CxStatic m_st_T3;
	CxStatic m_st_T4;
	CBorderStyleEdit m_edit_1;
	CBorderStyleEdit m_edit_2;
	CBorderStyleEdit m_edit_3;
	CBorderStyleEdit m_edit_4;

	afx_msg void OnBnClickedBtnMa1();
	virtual BOOL OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult);
	afx_msg void OnBnClickedBtnMa2();
	afx_msg void OnBnClickedBtnMa3();
	afx_msg void OnBnClickedBtnMa4();
	afx_msg void OnBnClickedBtnMa5();
	afx_msg void OnBnClickedBtnMa6();
	afx_msg void OnBnClickedBtnMa7();
	afx_msg void OnBnClickedBtnMa8();
	afx_msg void OnBnClickedBtnMa9();
	afx_msg void OnBnClickedBtnMa10();
	afx_msg void OnBnClickedBtnMa11();
	afx_msg void OnBnClickedBtnMa12();
};
