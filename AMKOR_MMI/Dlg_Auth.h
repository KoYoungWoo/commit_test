#pragma once
#include "afxwin.h"
#include "UIExt/IconButton.h"

// CDlg_Auth 대화 상자입니다.

class CDlg_Auth : public CDialogEx
{
	DECLARE_DYNAMIC(CDlg_Auth)

public:
	CDlg_Auth(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CDlg_Auth();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DLG_AUTH };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);

	int m_nAuthNumber; // 0-Operator, 1-Engineer, 2-Developer

	CIconButton m_btn_1;
	CIconButton m_btn_2;
	CIconButton m_btn_3;
	CIconButton m_btn_4;

	afx_msg void OnBnClickedBtnAuth1();
	afx_msg void OnBnClickedBtnAuth2();
	afx_msg void OnBnClickedBtnAuth3();
	afx_msg void OnBnClickedBtnAuth4();
};
