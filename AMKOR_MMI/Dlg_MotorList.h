#pragma once
#include "afxwin.h"
#include "UIExt/CxStatic.h"
#include "UIExt/IconButton.h"
#include "UIExt/XPGroupBox.h"
#include "GC/gridctrl.h"

// CDlg_MotorList 대화 상자입니다.

class CDlg_MotorList : public CDialogEx
{
	DECLARE_DYNAMIC(CDlg_MotorList)

public:
	CDlg_MotorList(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CDlg_MotorList();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DLG_MOTORLIST };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);

	void Cal_Area();
	void Cal_CtrlArea();
	void Cal_CtrlArea_Left();
	void Cal_CtrlArea_Right();

	bool Init_GC_1(int nRows, int nCols);
	afx_msg void OnGridClick_GC_1(NMHDR *pNotifyStruct, LRESULT* pResult);
	void Update_MotorList();

	BOOL m_bCtrl_FirstLoad;
	CString m_strCaption;
	int m_nSelMotorIndex;
	int m_nCurMotorPage;

	CGridCtrl m_GC_1;
	CIconButton m_btn_1;
	CIconButton m_btn_2;
	CIconButton m_btn_3;
	CIconButton m_btn_4;
	CIconButton m_btn_5;
	CIconButton m_btn_6;
	afx_msg void OnBnClickedBtnMl1();
	afx_msg void OnBnClickedBtnMl2();
	afx_msg void OnBnClickedBtnMl3();
	afx_msg void OnBnClickedBtnMl4();
	afx_msg void OnBnClickedBtnMl5();
	afx_msg void OnBnClickedBtnMl6();
};
