#pragma once

struct stTrayInfo
{
	CString m_strTraySize;
	double m_fTotalHeight;
	double m_fOverlapHeight;
	double m_fHeight;			// m_fTotalHeight - m_fOverlapHeight
	int m_nTrayMatrixRow;
	int m_nTrayMatrixCol;
	double m_fTraySizeX;
	double m_fTraySizeY;

	void Clear()
	{
		m_strTraySize.Empty();
		m_fTotalHeight = 0.0f;
		m_fOverlapHeight = 0.0f;
		m_fHeight = 0.0f;
		m_nTrayMatrixRow = 0;
		m_nTrayMatrixCol = 0;
		m_fTraySizeX = 0.0f;
		m_fTraySizeY = 0.0f;
	}
};

struct stDeviceInfo
{
	CString m_strDeviceName;
	CString m_strCustomer;
	// 트레이 정보
	int m_nTrayIndex;
	int m_nTrayDivideCount;			// 트레이 소분 개수
	// 장비 프로세스 정보
	int m_nMBB_Type;				// MBB 종류 선택 : 0 - 좁은폭, 1 - 넓은폭
	int m_nBOX_Type;				// BOX 종류 선택 : 0 - BrownBox, 1 - NVidia White
	int m_nBand_VCount;				// 세로 밴딩 카운트
	int m_nBand_HCount;				// 가로 밴딩 카운트
	int m_nEndCap;					// ENDCAP 유무
	int m_nTrayRotate;				// MBB에 트레이 삽입전 180 회전 여부
	int m_nBoxRotate;				// 박스 180 회전 여부
	int m_nBubbleSheet;				// BubbleSheet 유무
	int m_nBottomTray;				// Bottom Tray 추가 여부
	int m_nCoverTray;				// 커버트레이 추가 여부
	int m_nBoxLabelPos;				// 박스라벨 부착 위치 : 0:right, 1:left
	int m_nUseBox;					// Box사용유무 - 사용안할시 박스부터 바이패스 - 0: 사용안함, 1: 사용
	int m_nUseHIC_DSC;				// HIC/Dessicant 사용 유무 - 0: 사용안함, 1: 사용
	int m_nLabelSize;				// 라벨 용지 크기 : 0 - Big, 1 - Small
	int m_nSilRotate;				// 실링후 회전 여부
	int m_nUseInk;					// 잉크젯 프린트 사용 여부

	void Clear()
	{
		m_strDeviceName.Empty();
		m_strCustomer.Empty();
		// 트레이 정보
		m_nTrayIndex = 0;
		m_nTrayDivideCount = 0;
		// 장비 프로세스 정보
		m_nMBB_Type = 0;
		m_nBOX_Type = 0;
		m_nBand_VCount = 0;
		m_nBand_HCount = 0;
		m_nEndCap = 0;
		m_nTrayRotate = 0;
		m_nBoxRotate = 0;
		m_nBubbleSheet = 0;
		m_nBottomTray = 0;
		m_nCoverTray = 0;
		m_nBoxLabelPos = 0;
		m_nUseBox = 0;
		m_nUseHIC_DSC = 0;
		m_nLabelSize = 0;
		m_nSilRotate = 0;
		m_nUseInk = 0;
	}
};

struct stDeviceTrayInfo
{
	int m_nDeviceCount;
	std::vector<stDeviceInfo> m_vecDeviceInfo;

	int m_nTrayIndexCount;
	std::vector<stTrayInfo> m_vecTrayInfo;
};

class CDeviceTraySettingGC;
class CDeviceTraySetting  
{
public:
	friend class CDeviceTraySettingGC;
	static CDeviceTraySetting* Instance();

	CString GetExecuteDirectory(); // 현재 실행화일 경로 가져오자.

	void GetFileList();
	std::vector<CString> m_vecFileList;
	CString m_strSelFileName; // 선택된 파일명..
	CString m_strMCHName; // 선택한 장비이름..

	stDeviceTrayInfo GetDeviceTrayInfo() { return m_stDTInfo; };

	BOOL Load_DeviceSetting();
	BOOL Save_DeviceSetting();
	void BackupFile_DeviceSetting();
	void AddDeviceInfo();
	void DelDeviceInfo();
	void SetDeviceTrayIndex(int nDeviceIndex, int nTrayIndex);
	void SetDeviceInfo(int nDeviceIndex, int nTrayIndex, int nDivide, int nBandV, int nBandH,
		int nApron, int nCover, int nTRotate, int nEndCap, int nUseHICDSC, int nMBBType, int nBOXType,
		int nBRotate, int nUseBubble, int nBoxLabelPos, int nUseBox, int nLabelSize, int nSilRotate, int nUseInk);
	void SetDeviceInfo2(int nDeviceIndex, LPCTSTR strName, LPCTSTR strCustomer, int nTrayIndex, int nDivide,
		int nBandV, int nBandH, int nApron, int nCover, int nTRotate, int nEndCap, int nUseHICDSC, int nMBBType, int nBOXType,
		int nBRotate, int nUseBubble, int nBoxLabelPos, int nUseBox, int nLabelSize, int nSilRotate, int nUseInk);

	BOOL Load_TrayInfoSetting();
	BOOL Save_TrayInfoSetting();
	void BackupFile_TrayInfoSetting();
	void AddTrayInfo();
	void DelTrayInfo();
	void SetTrayInfo(int nTrayIndex, LPCTSTR strText, double dTotalH, double dOverlapH, int nMatRow, int nMatCol, double dSizeX, double dSizeY);

private:
	static CDeviceTraySetting* m_pThis;
	CDeviceTraySetting();
	~CDeviceTraySetting();

	stDeviceTrayInfo	m_stDTInfo;
};
