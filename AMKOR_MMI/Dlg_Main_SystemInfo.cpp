// Dlg_Main_SystemInfo.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "AMKOR_MMI.h"
#include "Dlg_Main_SystemInfo.h"
#include "Dlg_NumPad.h"
#include "afxdialogex.h"

// CDlg_Main_SystemInfo 대화 상자입니다.

IMPLEMENT_DYNAMIC(CDlg_Main_SystemInfo, CDialogEx)

CDlg_Main_SystemInfo::CDlg_Main_SystemInfo(CWnd* pParent /*=NULL*/)
	: CDialogEx(CDlg_Main_SystemInfo::IDD, pParent)
{
	m_bCtrl_FirstLoad = FALSE;
}

CDlg_Main_SystemInfo::~CDlg_Main_SystemInfo()
{
}

void CDlg_Main_SystemInfo::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_GB_M_SI_1, m_gb_1);
	DDX_Control(pDX, IDC_GB_M_SI_2, m_gb_2);
	DDX_Control(pDX, IDC_GC_M_SI_1, m_GC_1);
	DDX_Control(pDX, IDC_GC_M_SI_2, m_GC_2);
	DDX_Control(pDX, IDC_LIST_M_SI_1, m_list_1);
	DDX_Control(pDX, IDC_LIST_M_SI_V1, m_list_LogView);
	DDX_Control(pDX, IDC_GB_M_SI_3, m_gb_3);
}


BEGIN_MESSAGE_MAP(CDlg_Main_SystemInfo, CDialogEx)
	ON_WM_ERASEBKGND()
	ON_WM_SIZE()
	ON_WM_TIMER()
	ON_NOTIFY(NM_CLICK, IDC_GC_M_SI_1, OnGridClick_GC_1)
	ON_NOTIFY(NM_CLICK, IDC_GC_M_SI_2, OnGridClick_GC_2)
END_MESSAGE_MAP()


// CDlg_Main_SystemInfo 메시지 처리기입니다.


BOOL CDlg_Main_SystemInfo::OnEraseBkgnd(CDC* pDC)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	// 전체 배경색
	CRect rect;
    GetClientRect(&rect);
    CBrush myBrush(CR_MAIN_BKGND);	// dialog background color
    CBrush *pOld = pDC->SelectObject(&myBrush);
    BOOL bRes  = pDC->PatBlt(0, 0, rect.Width(), rect.Height(), PATCOPY);
    pDC->SelectObject(pOld);		// restore old brush
    return FALSE;     
	//return CDialogEx::OnEraseBkgnd(pDC);
}


void CDlg_Main_SystemInfo::OnSize(UINT nType, int cx, int cy)
{
	CDialogEx::OnSize(nType, cx, cy);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	if(m_bCtrl_FirstLoad == TRUE) Cal_CtrlArea();
}


void CDlg_Main_SystemInfo::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CDialogEx::OnTimer(nIDEvent);
}


BOOL CDlg_Main_SystemInfo::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	Cal_CtrlArea();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


BOOL CDlg_Main_SystemInfo::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message == WM_SYSKEYDOWN)
	{
		if(pMsg->wParam == VK_F4) 
			return TRUE;
	}

	if(pMsg->message == WM_KEYDOWN)
	{
		if( pMsg->wParam == VK_ESCAPE ||	
			pMsg->wParam == VK_RETURN ||	
			pMsg->wParam == VK_SPACE  ||	
			pMsg->wParam == VK_CANCEL )     
			return TRUE;        
	}

	return CDialogEx::PreTranslateMessage(pMsg);
}

void CDlg_Main_SystemInfo::Cal_Area()
{
	CRect ClientRect;
	GetClientRect(&ClientRect);

	int n33pH = (int)((float)ClientRect.Height()*0.33f);

	m_rcTop.SetRect(ClientRect.left, ClientRect.top, ClientRect.right, ClientRect.top+n33pH);
	m_rcMid.SetRect(ClientRect.left, m_rcTop.bottom, ClientRect.right, m_rcTop.bottom+n33pH);
	m_rcBottom.SetRect(ClientRect.left, m_rcMid.bottom, ClientRect.right, ClientRect.bottom);
}

void CDlg_Main_SystemInfo::Cal_CtrlArea()
{
	Cal_Area();

	Cal_CtrlArea_Top();
	Cal_CtrlArea_Mid();
	Cal_CtrlArea_Bottom();

	m_bCtrl_FirstLoad = TRUE;
}

void CDlg_Main_SystemInfo::Cal_CtrlArea_Top()
{
	CRect CalRect;
	CRect rcGB, rcGBInside;

	rcGB = m_rcTop;
	rcGB.DeflateRect(0,6,0,6);
	rcGBInside = rcGB;
	rcGBInside.DeflateRect(6,28,6,6);

	m_gb_1.SetXPGroupStyle(CXPGroupBox::XPGB_WINDOW_2);
	m_gb_1.SetBackgroundColor(CR_DARKGRAY, CR_GB_BKGND);
	m_gb_1.SetBorderColor(CR_BLACK);
	m_gb_1.SetCatptionTextColor(CR_BLACK);
	m_gb_1.MoveWindow(&rcGB);
	m_GC_1.MoveWindow(&rcGBInside);
	Init_GC_1(7,8);
}

void CDlg_Main_SystemInfo::Cal_CtrlArea_Mid()
{
	CRect CalRect;
	CRect rcGB, rcGBInside;

	rcGB = m_rcMid;
	rcGB.DeflateRect(0,6,0,6);
	rcGBInside = rcGB;
	rcGBInside.DeflateRect(6,28,6,6);

	m_gb_2.SetXPGroupStyle(CXPGroupBox::XPGB_WINDOW_2);
	m_gb_2.SetBackgroundColor(CR_DARKGRAY, CR_GB_BKGND);
	m_gb_2.SetBorderColor(CR_BLACK);
	m_gb_2.SetCatptionTextColor(CR_BLACK);
	m_gb_2.MoveWindow(&rcGB);
	m_GC_2.MoveWindow(&rcGBInside);
	Init_GC_2(8,6);
}

void CDlg_Main_SystemInfo::Cal_CtrlArea_Bottom()
{
	CRect CalRect;
	CRect rcGB, rcGBInside;

	rcGB = m_rcBottom;
	rcGB.DeflateRect(0,6,500,6);
	rcGBInside = rcGB;
	rcGBInside.DeflateRect(6,28,6,6);

	m_gb_3.SetXPGroupStyle(CXPGroupBox::XPGB_WINDOW_2);
	m_gb_3.SetBackgroundColor(CR_DARKGRAY, CR_GB_BKGND);
	m_gb_3.SetBorderColor(CR_BLACK);
	m_gb_3.SetCatptionTextColor(CR_BLACK);
	m_gb_3.MoveWindow(&rcGB);

	m_list_LogView.MoveWindow(&rcGBInside);
	m_list_1.ShowWindow(SW_HIDE);
}

bool CDlg_Main_SystemInfo::Init_GC_1(int nRows, int nCols)
{
	int nPLCNum = CAppSetting::Instance()->GetGroupNumber();
	CStringArray strTitle;
	strTitle.SetSize(8);
	if(nPLCNum == 1)
	{
		strTitle.SetAt(0,_T("DSC 공급"));
		strTitle.SetAt(1,_T("밴딩2(가로) && HIC"));
		strTitle.SetAt(2,_T("밴딩1(세로)"));
		strTitle.SetAt(3,_T("라벨 공급"));
		strTitle.SetAt(4,_T("사면 검사"));
		strTitle.SetAt(5,_T("비전 && 커버결합"));
		strTitle.SetAt(6,_T("소분"));
		strTitle.SetAt(7,_T("RFID 맵핑"));
	}
	else if(nPLCNum == 2)
	{
		strTitle.SetAt(0,_T(""));
		strTitle.SetAt(1,_T(""));
		strTitle.SetAt(2,_T(""));
		strTitle.SetAt(3,_T(""));
		strTitle.SetAt(4,_T(""));
		strTitle.SetAt(5,_T("실링 배출"));
		strTitle.SetAt(6,_T("실링"));
		strTitle.SetAt(7,_T("실링 대기"));
	}
	else if(nPLCNum == 3)
	{
		strTitle.SetAt(0,_T(""));
		strTitle.SetAt(1,_T(""));
		strTitle.SetAt(2,_T(""));
		strTitle.SetAt(3,_T("박스 바코드검사"));
		strTitle.SetAt(4,_T("박스 라벨"));
		strTitle.SetAt(5,_T("박스 날개접기"));
		strTitle.SetAt(6,_T("박스 투입"));
		strTitle.SetAt(7,_T("MBB 폴딩"));
	}

	BOOL m_bEditable = FALSE;
    BOOL m_bListMode = TRUE;
    int m_nRows = nRows;
    int m_nCols = nCols;
    int m_nFixRows = 1;
    int m_nFixCols = 0;

    m_GC_1.SetEditable(m_bEditable);
    m_GC_1.SetListMode(m_bListMode);
    m_GC_1.EnableDragAndDrop(FALSE);
    m_GC_1.SetTextBkColor(RGB(0xFF, 0xFF, 0xE0));

    m_GC_1.SetRowCount(m_nRows);
    m_GC_1.SetColumnCount(m_nCols);
    m_GC_1.SetFixedRowCount(m_nFixRows);
    m_GC_1.SetFixedColumnCount(m_nFixCols);

	m_GC_1.EnableSelection(false);
	m_GC_1.SetSingleColSelection(true);
	m_GC_1.SetSingleRowSelection(true);
	m_GC_1.SetFixedColumnSelection(false);
    m_GC_1.SetFixedRowSelection(false);

	m_GC_1.SetRowResize(false);
	m_GC_1.SetColumnResize(false);

	CRect ClientRect;
	GetDlgItem(IDC_GC_M_SI_1)->GetWindowRect(&ClientRect);
	if(ClientRect.Width() < 4) return false;
	if(ClientRect.Height() < 4) return false;
	int nClientWidth = ClientRect.Width()-4;
	int nClientHeight = ClientRect.Height()-4;
	int nDiv6pW = (int)((float)nClientWidth/8.0f);
	int nRemainW = nClientWidth - nDiv6pW*(m_nCols-1);
	int nCellH = nClientHeight/m_nRows;
	int nLastCellH = nClientHeight-nCellH*(m_nRows-1);
	int nRemainH = nLastCellH - nCellH;
	for (int i = 0; i < m_GC_1.GetRowCount(); i++) 
	{
		if(i == m_nRows-1)
		{
			m_GC_1.SetRowHeight(i,nCellH);
		}
		else 
		{
			if(nRemainH > 0)
			{
				m_GC_1.SetRowHeight(i,nCellH + 1);
				nRemainH --;
			}
			else
				m_GC_1.SetRowHeight(i,nCellH);
		}
	}
	for (int j = 0; j < m_GC_1.GetColumnCount(); j++)
	{
		if(j != m_GC_1.GetColumnCount()-1) m_GC_1.SetColumnWidth(j,nDiv6pW);
		else m_GC_1.SetColumnWidth(j,nRemainW);
	}
    DWORD dwTextStyle = DT_CENTER|DT_VCENTER|DT_SINGLELINE;

    // fill rows/cols with text
    for (int row = 0; row < m_GC_1.GetRowCount(); row++) 
	{
        for (int col = 0; col < m_GC_1.GetColumnCount(); col++) 
		{ 
            GV_ITEM Item;
            Item.mask = GVIF_TEXT|GVIF_FORMAT;
            Item.row = row;
            Item.col = col;
			Item.nFormat = dwTextStyle;

			if(row == 0)
			{
				Item.strText.Format(_T("%s"), strTitle.GetAt(col));
				m_GC_1.SetItemBkColour(row, col, CR_SLATEGRAY);
			}
			else if(row == 6)
			{
				Item.strText.Format(_T("Delete"));
				Item.crBkClr = CR_DARKGRAY;
			}
			m_GC_1.SetItemFgColour(row, col, RGB(0,0,0));
            m_GC_1.SetItem(&Item);  
        }
    }
       
    return TRUE;  // return TRUE  unless you set the focus to a control
}

void CDlg_Main_SystemInfo::OnGridClick_GC_1(NMHDR *pNotifyStruct, LRESULT* pResult)
{
	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*) pNotifyStruct;

	int nSelRow = pItem->iRow;
	int nSelCol = pItem->iColumn;

	if(nSelRow == 6) // Delete 처리
	{
		CDlg_NumPad dlg;
		dlg.m_strCaption = _T("Delete Tray Position Info");
		dlg.m_bStringMode = TRUE;
		if(dlg.DoModal() == IDOK)
		{
			if(dlg.m_strValue.CompareNoCase(_T("0000")) == 0)
			{
				// PLC 자재 정보 제거 M 어드레스에 값 넣기
				int nPLCNum = CAppSetting::Instance()->GetGroupNumber();
				int nStartMAddressBit = 8801 + (8-nSelCol-1);
				CDataManager::Instance()->PLC_Write_M(nPLCNum, nStartMAddressBit, TRUE);
				// DMS 자재 정보 제거
				if(nPLCNum == 1)
				{
					CDataManager::Instance()->DeleteBoxInfo(nSelCol+1);
				}
				else
				{
				}
				// LOG 남기기
				GetLog()->Debug( _T("DELETE TRAY POS = %d"), nSelCol );
				CLogList::Instance()->Add_LogData(eLog_DEBUG, _T("DELETE TRAY POS = %d"), nSelCol );
			}
		}
	}
}

bool CDlg_Main_SystemInfo::Init_GC_2(int nRows, int nCols)
{
	BOOL m_bEditable = FALSE;
    BOOL m_bListMode = TRUE;
    int m_nRows = nRows;
    int m_nCols = nCols;
    int m_nFixRows = 1;
    int m_nFixCols = 0;

    m_GC_2.SetEditable(m_bEditable);
    m_GC_2.SetListMode(m_bListMode);
    m_GC_2.EnableDragAndDrop(FALSE);
    m_GC_2.SetTextBkColor(RGB(0xFF, 0xFF, 0xE0));

    m_GC_2.SetRowCount(m_nRows);
    m_GC_2.SetColumnCount(m_nCols);
    m_GC_2.SetFixedRowCount(m_nFixRows);
    m_GC_2.SetFixedColumnCount(m_nFixCols);

	m_GC_2.EnableSelection(false);
	m_GC_2.SetSingleColSelection(true);
	m_GC_2.SetSingleRowSelection(true);
	m_GC_2.SetFixedColumnSelection(false);
    m_GC_2.SetFixedRowSelection(false);

	m_GC_2.SetRowResize(false);
	m_GC_2.SetColumnResize(false);

	CRect ClientRect;
	GetDlgItem(IDC_GC_M_SI_2)->GetWindowRect(&ClientRect);
	if(ClientRect.Width() < 4) return false;
	if(ClientRect.Height() < 4) return false;
	int nClientWidth = ClientRect.Width()-4;
	int nClientHeight = ClientRect.Height()-4;
	int nDiv6pW = (int)((float)nClientWidth/6.0f);
	int nRemainW = nClientWidth - nDiv6pW*(m_nCols-1);
	int nCellH = nClientHeight/m_nRows;
	int nLastCellH = nClientHeight-nCellH*(m_nRows-1);
	int nRemainH = nLastCellH - nCellH;
	for (int i = 0; i < m_GC_2.GetRowCount(); i++) 
	{
		if(i == m_nRows-1)
		{
			m_GC_2.SetRowHeight(i,nCellH);
		}
		else 
		{
			if(nRemainH > 0)
			{
				m_GC_2.SetRowHeight(i,nCellH + 1);
				nRemainH --;
			}
			else
				m_GC_2.SetRowHeight(i,nCellH);
		}
	}
	for (int j = 0; j < m_GC_2.GetColumnCount(); j++)
	{
		if(j != m_GC_2.GetColumnCount()-1) m_GC_2.SetColumnWidth(j,nDiv6pW);
		else m_GC_2.SetColumnWidth(j,nRemainW);
	}
    DWORD dwTextStyle = DT_CENTER|DT_VCENTER|DT_SINGLELINE;

    // fill rows/cols with text
    for (int row = 0; row < m_GC_2.GetRowCount(); row++) 
	{
        for (int col = 0; col < m_GC_2.GetColumnCount(); col++) 
		{ 
            GV_ITEM Item;
            Item.mask = GVIF_TEXT|GVIF_FORMAT;
            Item.row = row;
            Item.col = col;
			Item.nFormat = dwTextStyle;

			if(row == 0)
			{
				if(col == 0) Item.strText.Format(_T("LOT ID"));
				else if(col == 1) Item.strText.Format(_T("INPUT TIME"));
				else if(col == 2) Item.strText.Format(_T("TOTAL BOX"));
				else if(col == 3) Item.strText.Format(_T("MAKE BOX"));
				else if(col == 4) Item.strText.Format(_T("OUTPUT BOX"));
				else if(col == 5) Item.strText.Format(_T("DELETE BOX"));
			}

			if(row == 0) m_GC_2.SetItemBkColour(row, col, CR_SLATEGRAY);
			m_GC_2.SetItemFgColour(row, col, RGB(0,0,0));
            m_GC_2.SetItem(&Item);  
        }
    }
       
    return TRUE;  // return TRUE  unless you set the focus to a control
}

void CDlg_Main_SystemInfo::OnGridClick_GC_2(NMHDR *pNotifyStruct, LRESULT* pResult)
{
	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*) pNotifyStruct;

	int nSelRow = pItem->iRow;
	int nSelCol = pItem->iColumn;
}

void CDlg_Main_SystemInfo::Update_TrayPosInfo()
{
	int nPLCNum = CAppSetting::Instance()->GetGroupNumber();
	int nUpdateDataSize = 0;
	CStringArray strTitle;
	strTitle.SetSize(8);
	if(nPLCNum == 1)
	{
		nUpdateDataSize = 8;
		strTitle.SetAt(0,_T("DSC 공급"));
		strTitle.SetAt(1,_T("밴딩2(가로) && HIC"));
		strTitle.SetAt(2,_T("밴딩1(세로)"));
		strTitle.SetAt(3,_T("라벨 공급"));
		strTitle.SetAt(4,_T("사면 검사"));
		strTitle.SetAt(5,_T("비전 && 커버결합"));
		strTitle.SetAt(6,_T("소분"));
		strTitle.SetAt(7,_T("RFID 맵핑"));
	}
	else if(nPLCNum == 2)
	{
		nUpdateDataSize = 3;
		strTitle.SetAt(0,_T(""));
		strTitle.SetAt(1,_T(""));
		strTitle.SetAt(2,_T(""));
		strTitle.SetAt(3,_T(""));
		strTitle.SetAt(4,_T(""));
		strTitle.SetAt(5,_T("실링 배출"));
		strTitle.SetAt(6,_T("실링"));
		strTitle.SetAt(7,_T("실링 대기"));
	}
	else if(nPLCNum == 3)
	{
		nUpdateDataSize = 5;
		strTitle.SetAt(0,_T(""));
		strTitle.SetAt(1,_T(""));
		strTitle.SetAt(2,_T(""));
		strTitle.SetAt(3,_T("박스 바코드검사"));
		strTitle.SetAt(4,_T("박스 라벨"));
		strTitle.SetAt(5,_T("박스 날개접기"));
		strTitle.SetAt(6,_T("박스 투입"));
		strTitle.SetAt(7,_T("MBB 폴딩"));
	}

	DWORD dwTextStyle = DT_CENTER|DT_VCENTER|DT_SINGLELINE;
	stDeviceTrayInfo stDTI = CDeviceTraySetting::Instance()->GetDeviceTrayInfo();
    for (int row = 0; row < m_GC_1.GetRowCount(); row++) 
	{
        for (int col = 0; col < nUpdateDataSize; col++) 
		{ 
            GV_ITEM Item;
            Item.mask = GVIF_TEXT|GVIF_FORMAT|GVIF_BKCLR;
            Item.row = row;
            Item.col = 7-col;
			Item.nFormat = dwTextStyle;
			Item.crBkClr = RGB(0xFF, 0xFF, 0xE0);

			if(row == 0)
			{
				Item.strText.Format(_T("%s"), strTitle.GetAt(7-col));//.Format(_T("POS #8"));

				if(CDataManager::Instance()->m_arrayTrayPackInfo[col].m_nDeviceIndex > 0)
					Item.crBkClr = CR_LIMEGREEN;
				else
					Item.crBkClr = CR_SLATEGRAY;
			}
			else if(row == 1)
			{
				Item.strText.Format(_T("%s"), CDataManager::Instance()->m_arrayTrayPackInfo[col].m_strLOTID);
				Item.crBkClr = CR_GAINSBORO;
			}
			else if(row == 2)
			{
				Item.strText.Format(_T("%s"), CDataManager::Instance()->m_arrayTrayPackInfo[col].m_strRFID);
				Item.crBkClr = CR_GAINSBORO;
			}
			else if(row == 3)
			{
				if(CDataManager::Instance()->m_arrayTrayPackInfo[col].m_nDeviceIndex > 0)
				{
					Item.strText.Format(_T("%03d"), CDataManager::Instance()->m_arrayTrayPackInfo[col].m_nBoxNum);
				}
				else Item.strText.Format(_T(""));
				Item.crBkClr = CR_GAINSBORO;
			}
			else if(row == 4)
			{
				if(CDataManager::Instance()->m_arrayTrayPackInfo[col].m_nDeviceIndex > 0)
				{
					Item.strText = stDTI.m_vecDeviceInfo[CDataManager::Instance()->m_arrayTrayPackInfo[col].m_nDeviceIndex-1].m_strDeviceName;
				}
				else Item.strText.Format(_T(""));
				Item.crBkClr = CR_GAINSBORO;
			}
			else if(row == 5)
			{
				if(CDataManager::Instance()->m_arrayTrayPackInfo[col].m_nDeviceIndex > 0)
				{
					Item.strText.Format(_T("%03d"), CDataManager::Instance()->m_arrayTrayPackInfo[col].m_nTrayCount);
				}
				else Item.strText.Format(_T(""));
				Item.crBkClr = CR_GAINSBORO;
			}
			else
			{
				Item.strText.Format(_T("Delete"));
				Item.crBkClr = CR_DARKGRAY;
			}

			m_GC_1.SetItemFgColour(row, col, RGB(0,0,0));
            m_GC_1.SetItem(&Item);  
        }
    }

	m_GC_1.Invalidate();
}

void CDlg_Main_SystemInfo::Update_LotListInfo()
{
	int nPLCNum = CAppSetting::Instance()->GetGroupNumber();
	if(nPLCNum != 1) return;

	DWORD dwTextStyle = DT_CENTER|DT_VCENTER|DT_SINGLELINE;

    // fill rows/cols with text
    for (int row = 0; row < m_GC_2.GetRowCount(); row++) 
	{
        for (int col = 0; col < m_GC_2.GetColumnCount(); col++) 
		{ 
            GV_ITEM Item;
            Item.mask = GVIF_TEXT|GVIF_FORMAT;
            Item.row = row;
            Item.col = col;
			Item.nFormat = dwTextStyle;

			if(row == 0)
			{
				if(col == 0) Item.strText.Format(_T("LOT ID"));
				else if(col == 1) Item.strText.Format(_T("INPUT TIME"));
				else if(col == 2) Item.strText.Format(_T("TOTAL BOX"));
				else if(col == 3) Item.strText.Format(_T("MAKE BOX"));
				else if(col == 4) Item.strText.Format(_T("OUTPUT BOX"));
				else if(col == 5) Item.strText.Format(_T("DELETE BOX"));

				m_GC_2.SetItemBkColour(row, col, CR_SLATEGRAY);
			}
			else
			{
				if(col == 0) Item.strText.Format(_T("%s"), CDataManager::Instance()->m_arrayLOTINFO[row-1].m_strLOTID);
				else if(col == 1) Item.strText.Format(_T("%s/%s"), CDataManager::Instance()->m_arrayLOTINFO[row-1].m_strCreateDate, CDataManager::Instance()->m_arrayLOTINFO[row-1].m_strCreateTime);
				else if(col == 2) Item.strText.Format(_T("%d"), CDataManager::Instance()->m_arrayLOTINFO[row-1].m_nTotalBoxCount);
				else if(col == 3) Item.strText.Format(_T("%d"), CDataManager::Instance()->m_arrayLOTINFO[row-1].m_nMakeBoxCount);
				else if(col == 4) Item.strText.Format(_T("%d"), CDataManager::Instance()->m_arrayLOTINFO[row-1].m_nOutputBoxCount);
				else if(col == 5) Item.strText.Format(_T("%d"), CDataManager::Instance()->m_arrayLOTINFO[row-1].m_nDeleteBoxCount);
			}

			if(row == 0) m_GC_2.SetItemBkColour(row, col, CR_SLATEGRAY);
			m_GC_2.SetItemFgColour(row, col, RGB(0,0,0));
            m_GC_2.SetItem(&Item);  
        }
    }

	m_GC_2.Invalidate();
}

void CDlg_Main_SystemInfo::Update_UI()
{
	Update_TrayPosInfo();
	Update_LotListInfo();
}

void CDlg_Main_SystemInfo::Update_LogList()
{
	int nMaxView = 100;
	std::wstring strText;

	if(CLogList::Instance()->IsRefresh(eLog_DEBUG))
	{
		m_list_LogView.SetRedraw(FALSE);
		m_list_LogView.Clear();

		for(int i=0;i<nMaxView;i++)
		{
			if(CLogList::Instance()->GetDebugLogStringFromEnd(nMaxView-1-i, strText))
			{
				m_list_LogView.AppendString(strText.c_str(), RGB(0,0,0), RGB(50,50,50));
			}
		}
		
		m_list_LogView.SetRedraw(TRUE);
		CLogList::Instance()->SetRefresh(eLog_DEBUG, false);

		// 스크롤바 자동으로 내려주는 Event
		m_list_LogView.SendMessage(WM_VSCROLL, SB_BOTTOM);
	}
}
