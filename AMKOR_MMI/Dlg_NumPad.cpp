// Dlg_NumPad.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "AMKOR_MMI.h"
#include "Dlg_NumPad.h"
#include "afxdialogex.h"


// CDlg_NumPad 대화 상자입니다.

IMPLEMENT_DYNAMIC(CDlg_NumPad, CDialogEx)

CDlg_NumPad::CDlg_NumPad(CWnd* pParent /*=NULL*/)
	: CDialogEx(CDlg_NumPad::IDD, pParent)
{
	m_strCaption.Empty();
	m_strValue.Empty();

	m_bStringMode = FALSE;
	m_bPassword = FALSE;
}

CDlg_NumPad::~CDlg_NumPad()
{
}

void CDlg_NumPad::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_ST_NP_1, m_st_1);
	DDX_Control(pDX, IDC_BTN_NP_1, m_btn_1);
	DDX_Control(pDX, IDC_BTN_NP_2, m_btn_2);
	DDX_Control(pDX, IDC_BTN_NP_3, m_btn_3);
	DDX_Control(pDX, IDC_BTN_NP_4, m_btn_4);
	DDX_Control(pDX, IDC_BTN_NP_5, m_btn_5);
	DDX_Control(pDX, IDC_BTN_NP_6, m_btn_6);
	DDX_Control(pDX, IDC_BTN_NP_7, m_btn_7);
	DDX_Control(pDX, IDC_BTN_NP_8, m_btn_8);
	DDX_Control(pDX, IDC_BTN_NP_9, m_btn_9);
	DDX_Control(pDX, IDC_BTN_NP_10, m_btn_10);
	DDX_Control(pDX, IDC_BTN_NP_11, m_btn_11);
	DDX_Control(pDX, IDC_BTN_NP_12, m_btn_12);
	DDX_Control(pDX, IDC_BTN_NP_13, m_btn_13);
	DDX_Control(pDX, IDC_BTN_NP_14, m_btn_14);
	DDX_Control(pDX, IDC_BTN_NP_15, m_btn_15);
	DDX_Control(pDX, IDC_BTN_NP_16, m_btn_16);
}


BEGIN_MESSAGE_MAP(CDlg_NumPad, CDialogEx)
	ON_WM_ERASEBKGND()
	ON_WM_SIZE()
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BTN_NP_1, &CDlg_NumPad::OnBnClickedBtnNp1)
	ON_BN_CLICKED(IDC_BTN_NP_2, &CDlg_NumPad::OnBnClickedBtnNp2)
	ON_BN_CLICKED(IDC_BTN_NP_3, &CDlg_NumPad::OnBnClickedBtnNp3)
	ON_BN_CLICKED(IDC_BTN_NP_4, &CDlg_NumPad::OnBnClickedBtnNp4)
	ON_BN_CLICKED(IDC_BTN_NP_5, &CDlg_NumPad::OnBnClickedBtnNp5)
	ON_BN_CLICKED(IDC_BTN_NP_6, &CDlg_NumPad::OnBnClickedBtnNp6)
	ON_BN_CLICKED(IDC_BTN_NP_7, &CDlg_NumPad::OnBnClickedBtnNp7)
	ON_BN_CLICKED(IDC_BTN_NP_8, &CDlg_NumPad::OnBnClickedBtnNp8)
	ON_BN_CLICKED(IDC_BTN_NP_9, &CDlg_NumPad::OnBnClickedBtnNp9)
	ON_BN_CLICKED(IDC_BTN_NP_10, &CDlg_NumPad::OnBnClickedBtnNp10)
	ON_BN_CLICKED(IDC_BTN_NP_11, &CDlg_NumPad::OnBnClickedBtnNp11)
	ON_BN_CLICKED(IDC_BTN_NP_12, &CDlg_NumPad::OnBnClickedBtnNp12)
	ON_BN_CLICKED(IDC_BTN_NP_13, &CDlg_NumPad::OnBnClickedBtnNp13)
	ON_BN_CLICKED(IDC_BTN_NP_14, &CDlg_NumPad::OnBnClickedBtnNp14)
	ON_BN_CLICKED(IDC_BTN_NP_15, &CDlg_NumPad::OnBnClickedBtnNp15)
	ON_BN_CLICKED(IDC_BTN_NP_16, &CDlg_NumPad::OnBnClickedBtnNp16)
END_MESSAGE_MAP()


// CDlg_NumPad 메시지 처리기입니다.


BOOL CDlg_NumPad::OnEraseBkgnd(CDC* pDC)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	// 전체 배경색
	CRect rect;
    GetClientRect(&rect);
    CBrush myBrush(CR_MAIN_BKGND);	// dialog background color
    CBrush *pOld = pDC->SelectObject(&myBrush);
    BOOL bRes  = pDC->PatBlt(0, 0, rect.Width(), rect.Height(), PATCOPY);
    pDC->SelectObject(pOld);		// restore old brush
    return FALSE;     
	//return CDialogEx::OnEraseBkgnd(pDC);
}


void CDlg_NumPad::OnSize(UINT nType, int cx, int cy)
{
	CDialogEx::OnSize(nType, cx, cy);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
}


void CDlg_NumPad::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CDialogEx::OnTimer(nIDEvent);
}


BOOL CDlg_NumPad::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.

	SetWindowText(m_strCaption);
	if(m_bStringMode == FALSE)
	{
		if(m_strValue.IsEmpty()) m_st_1.SetWindowText(_T("0"));
	}
	else
	{
		m_st_1.SetWindowText(_T(""));
	}	

	m_st_1.SetBkColor(CR_WHITE);
	m_st_1.SetTextColor(CR_BLACK);
	m_st_1.SetFont(CTRL_FONT,52,FW_BOLD);

	m_btn_1.SetFontBtn(CTRL_FONT, 30, FW_BOLD);
	m_btn_1.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_1.SetTextColor(CR_BLACK);
	m_btn_1.SetFlatType(TRUE);

	m_btn_2.SetFontBtn(CTRL_FONT, 30, FW_BOLD);
	m_btn_2.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_2.SetTextColor(CR_BLACK);
	m_btn_2.SetFlatType(TRUE);

	m_btn_3.SetFontBtn(CTRL_FONT, 30, FW_BOLD);
	m_btn_3.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_3.SetTextColor(CR_BLACK);
	m_btn_3.SetFlatType(TRUE);

	m_btn_4.SetFontBtn(CTRL_FONT, 30, FW_BOLD);
	m_btn_4.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_4.SetTextColor(CR_BLACK);
	m_btn_4.SetFlatType(TRUE);

	m_btn_5.SetFontBtn(CTRL_FONT, 30, FW_BOLD);
	m_btn_5.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_5.SetTextColor(CR_BLACK);
	m_btn_5.SetFlatType(TRUE);

	m_btn_6.SetFontBtn(CTRL_FONT, 30, FW_BOLD);
	m_btn_6.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_6.SetTextColor(CR_BLACK);
	m_btn_6.SetFlatType(TRUE);

	m_btn_7.SetFontBtn(CTRL_FONT, 30, FW_BOLD);
	m_btn_7.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_7.SetTextColor(CR_BLACK);
	m_btn_7.SetFlatType(TRUE);

	m_btn_8.SetFontBtn(CTRL_FONT, 30, FW_BOLD);
	m_btn_8.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_8.SetTextColor(CR_BLACK);
	m_btn_8.SetFlatType(TRUE);

	m_btn_9.SetFontBtn(CTRL_FONT, 30, FW_BOLD);
	m_btn_9.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_9.SetTextColor(CR_BLACK);
	m_btn_9.SetFlatType(TRUE);

	m_btn_10.SetFontBtn(CTRL_FONT, 30, FW_BOLD);
	m_btn_10.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_10.SetTextColor(CR_BLACK);
	m_btn_10.SetFlatType(TRUE);

	m_btn_11.SetFontBtn(CTRL_FONT, 30, FW_BOLD);
	m_btn_11.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_11.SetTextColor(CR_BLACK);
	m_btn_11.SetFlatType(TRUE);

	m_btn_12.SetFontBtn(CTRL_FONT, 30, FW_BOLD);
	m_btn_12.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_12.SetTextColor(CR_BLACK);
	m_btn_12.SetFlatType(TRUE);

	m_btn_13.SetFontBtn(CTRL_FONT, 30, FW_BOLD);
	m_btn_13.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_13.SetTextColor(CR_BLACK);
	m_btn_13.SetFlatType(TRUE);

	m_btn_14.SetFontBtn(CTRL_FONT, 24, FW_BOLD);
	m_btn_14.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_14.SetTextColor(CR_BLACK);
	m_btn_14.SetFlatType(TRUE);

	m_btn_15.SetFontBtn(CTRL_FONT, 24, FW_BOLD);
	m_btn_15.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_15.SetTextColor(CR_BLACK);
	m_btn_15.SetIconSize(64,64);
	m_btn_15.SetIconID(IDI_CLOSE);
	m_btn_15.SetFlatType(TRUE);

	m_btn_16.SetFontBtn(CTRL_FONT, 30, FW_BOLD);
	m_btn_16.SetButtonColor(CR_LAVENDER, CR_LIGHTGRAY);
	m_btn_16.SetTextColor(CR_BLACK);
	m_btn_16.SetFlatType(TRUE);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


BOOL CDlg_NumPad::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message == WM_SYSKEYDOWN)
	{
		if(pMsg->wParam == VK_F4) 
			return TRUE;
	}

	if(pMsg->message == WM_KEYDOWN)
	{
		if( pMsg->wParam == VK_ESCAPE ||	
			pMsg->wParam == VK_RETURN ||	
			pMsg->wParam == VK_SPACE  ||
			pMsg->wParam == VK_CANCEL )     
			return TRUE;        
	}

	return CDialogEx::PreTranslateMessage(pMsg);
}

void CDlg_NumPad::SetNumeric(BOOL bNumeric)
{
	m_bStringMode = !bNumeric;
	m_bPassword = FALSE;
}

void CDlg_NumPad::SetPassword(BOOL bPassword)
{
	m_bStringMode = TRUE;
	m_bPassword = bPassword;
}

void CDlg_NumPad::OnBnClickedBtnNp1()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(m_strValue.GetLength() >= 11) return;
	m_strValue += _T("7");
	if(m_bPassword) 
	{
		int nSize = m_strValue.GetLength();
		CString strText;
		for(int i=0;i<nSize;i++) strText += _T("*");
		m_st_1.SetWindowText(strText);
	}
	else
	{
		m_st_1.SetWindowText(m_strValue);
	}
}


void CDlg_NumPad::OnBnClickedBtnNp2()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(m_strValue.GetLength() >= 11) return;
	m_strValue += _T("8");
	if(m_bPassword) 
	{
		int nSize = m_strValue.GetLength();
		CString strText;
		for(int i=0;i<nSize;i++) strText += _T("*");
		m_st_1.SetWindowText(strText);
	}
	else
	{
		m_st_1.SetWindowText(m_strValue);
	}
}


void CDlg_NumPad::OnBnClickedBtnNp3()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(m_strValue.GetLength() >= 11) return;
	m_strValue += _T("9");
	if(m_bPassword) 
	{
		int nSize = m_strValue.GetLength();
		CString strText;
		for(int i=0;i<nSize;i++) strText += _T("*");
		m_st_1.SetWindowText(strText);
	}
	else
	{
		m_st_1.SetWindowText(m_strValue);
	}
}


void CDlg_NumPad::OnBnClickedBtnNp4()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(m_strValue.GetLength() >= 11) return;
	m_strValue += _T("4");
	if(m_bPassword) 
	{
		int nSize = m_strValue.GetLength();
		CString strText;
		for(int i=0;i<nSize;i++) strText += _T("*");
		m_st_1.SetWindowText(strText);
	}
	else
	{
		m_st_1.SetWindowText(m_strValue);
	}
}


void CDlg_NumPad::OnBnClickedBtnNp5()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(m_strValue.GetLength() >= 11) return;
	m_strValue += _T("5");
	if(m_bPassword) 
	{
		int nSize = m_strValue.GetLength();
		CString strText;
		for(int i=0;i<nSize;i++) strText += _T("*");
		m_st_1.SetWindowText(strText);
	}
	else
	{
		m_st_1.SetWindowText(m_strValue);
	}
}


void CDlg_NumPad::OnBnClickedBtnNp6()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(m_strValue.GetLength() >= 11) return;
	m_strValue += _T("6");
	if(m_bPassword) 
	{
		int nSize = m_strValue.GetLength();
		CString strText;
		for(int i=0;i<nSize;i++) strText += _T("*");
		m_st_1.SetWindowText(strText);
	}
	else
	{
		m_st_1.SetWindowText(m_strValue);
	}
}


void CDlg_NumPad::OnBnClickedBtnNp7()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(m_strValue.GetLength() >= 11) return;
	m_strValue += _T("1");
	if(m_bPassword) 
	{
		int nSize = m_strValue.GetLength();
		CString strText;
		for(int i=0;i<nSize;i++) strText += _T("*");
		m_st_1.SetWindowText(strText);
	}
	else
	{
		m_st_1.SetWindowText(m_strValue);
	}
}


void CDlg_NumPad::OnBnClickedBtnNp8()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(m_strValue.GetLength() >= 11) return;
	m_strValue += _T("2");
	if(m_bPassword) 
	{
		int nSize = m_strValue.GetLength();
		CString strText;
		for(int i=0;i<nSize;i++) strText += _T("*");
		m_st_1.SetWindowText(strText);
	}
	else
	{
		m_st_1.SetWindowText(m_strValue);
	}
}


void CDlg_NumPad::OnBnClickedBtnNp9()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(m_strValue.GetLength() >= 11) return;
	m_strValue += _T("3");
	if(m_bPassword) 
	{
		int nSize = m_strValue.GetLength();
		CString strText;
		for(int i=0;i<nSize;i++) strText += _T("*");
		m_st_1.SetWindowText(strText);
	}
	else
	{
		m_st_1.SetWindowText(m_strValue);
	}
}


void CDlg_NumPad::OnBnClickedBtnNp10()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(m_strValue.GetLength() >= 11) return;
	if(m_bStringMode == FALSE)
	{
		if(m_strValue.GetLength() > 0)
		{
			m_strValue += _T("0");
			m_st_1.SetWindowText(m_strValue);
		}
	}
	else
	{
		m_strValue += _T("0");
		if(m_bPassword) 
		{
			int nSize = m_strValue.GetLength();
			CString strText;
			for(int i=0;i<nSize;i++) strText += _T("*");
			m_st_1.SetWindowText(strText);
		}
		else
		{
			m_st_1.SetWindowText(m_strValue);
		}
	}
}


void CDlg_NumPad::OnBnClickedBtnNp11()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(m_strValue.GetLength() >= 11) return;
	if(m_bStringMode == FALSE)
	{
		if(m_strValue.GetLength() == 0)
		{
			m_strValue += _T("0.");
			m_st_1.SetWindowText(m_strValue);
		}
		if(m_strValue.GetLength() > 0)
		{
			if(m_strValue.Find(_T(".")) == -1)
			{
				m_strValue += _T(".");
				m_st_1.SetWindowText(m_strValue);
			}
		}
	}
	else
	{
		m_strValue += _T(".");
		if(m_bPassword) 
		{
			int nSize = m_strValue.GetLength();
			CString strText;
			for(int i=0;i<nSize;i++) strText += _T("*");
			m_st_1.SetWindowText(strText);
		}
		else
		{
			m_st_1.SetWindowText(m_strValue);
		}
	}
}


void CDlg_NumPad::OnBnClickedBtnNp12()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_strValue.Delete(m_strValue.GetLength()-1);
	if(m_bStringMode == FALSE)
	{
		if(m_strValue.IsEmpty()) m_st_1.SetWindowText(_T("0"));
		else m_st_1.SetWindowText(m_strValue);
	}
	else
	{
		if(m_bPassword) 
		{
			int nSize = m_strValue.GetLength();
			CString strText;
			for(int i=0;i<nSize;i++) strText += _T("*");
			m_st_1.SetWindowText(strText);
		}
		else
		{
			m_st_1.SetWindowText(m_strValue);
		}
	}
}


void CDlg_NumPad::OnBnClickedBtnNp13()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_strValue.Empty();
	if(m_bStringMode == FALSE)
	{
		m_st_1.SetWindowText(_T("0"));
	}
	else
	{
		m_st_1.SetWindowText(m_strValue);
	}
}


void CDlg_NumPad::OnBnClickedBtnNp14()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CDialogEx::OnOK();
}


void CDlg_NumPad::OnBnClickedBtnNp15()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CDialogEx::OnCancel();
}


void CDlg_NumPad::OnBnClickedBtnNp16()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(m_bStringMode == FALSE)
	{
		if(m_strValue.GetLength() == 0)
		{
			m_strValue += _T("-");
			m_st_1.SetWindowText(m_strValue);
		}
	}
	else
	{
		m_strValue += _T("-");
		if(m_bPassword) 
		{
			int nSize = m_strValue.GetLength();
			CString strText;
			for(int i=0;i<nSize;i++) strText += _T("*");
			m_st_1.SetWindowText(strText);
		}
		else
		{
			m_st_1.SetWindowText(m_strValue);
		}
	}
}
