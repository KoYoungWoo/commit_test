// AppSetting.h: interface for the CAppSetting class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_APPSETTING_H__64F2F640_D51D_4952_A16E_FEA93A687DC1__INCLUDED_)
#define AFX_APPSETTING_H__64F2F640_D51D_4952_A16E_FEA93A687DC1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

struct IPv4Info
{
	BYTE cField0;
	BYTE cField1;
	BYTE cField2;
	BYTE cField3;

	void Init()
	{
		cField0 = 0;
		cField1 = 0;
		cField2 = 0;
		cField3 = 0;
	}

	const CString GetIPAddressString() const
	{
		CString strIP;
		strIP.Format( _T("%d.%d.%d.%d"), cField0, cField1, cField2, cField3 );
		return strIP;
	}

	const DWORD GetIPAddressDWORD() const
	{
		WORD w1	= MAKEWORD(cField1, cField0);
		WORD w2	= MAKEWORD(cField3, cField2);
		return MAKELPARAM(w2, w1);
	}

	void SetIPAddress( DWORD _dwIP )
	{
		WORD w1 = LOWORD(_dwIP);
		WORD w2 = HIWORD(_dwIP);

		cField0 = HIBYTE(w2);
		cField1 = LOBYTE(w2);
		cField2 = HIBYTE(w1);
		cField3 = LOBYTE(w1);		
	}

	void SetIPAddress( LPCTSTR lpszIP )
	{
		CString strTemp;
		int nDotPos;

		CString strIP = lpszIP;

		nDotPos = strIP.Find( _T('.') );
		ASSERT( nDotPos >= 0 );
		strTemp = strIP.Left( nDotPos ); 
		strIP.Delete( 0, nDotPos+1 );
		cField0 = (BYTE)_tcstoul( strTemp, NULL, 10 );
		
		nDotPos = strIP.Find( _T('.') );
		ASSERT( nDotPos >= 0 );
		strTemp = strIP.Left( nDotPos ); 
		strIP.Delete( 0, nDotPos+1 );
		cField1 = (BYTE)_tcstoul( strTemp, NULL, 10 );
		
		nDotPos = strIP.Find( _T('.') );
		ASSERT( nDotPos >= 0 );
		strTemp = strIP.Left( nDotPos ); 
		strIP.Delete( 0, nDotPos+1 );
		cField2 = (BYTE)_tcstoul( strTemp, NULL, 10 );
		
		strTemp = strIP;
		cField3 = (BYTE)_tcstoul( strTemp, NULL, 10 );
	}	
};

class CAppSettingGC;
class CAppSetting  
{
public:
	friend class CAppSettingGC;
	static CAppSetting* Instance();

	// Get Group Number
	const int GetGroupNumber() const;
	const int GetLogicalStationNumber(int nGroupNum) const;

	// ZPL Printer
	const IPv4Info& GetIP_ZPL_Printer(int nPrintNum);
	const DWORD GetIP_DWORD_ZPL_Printer(int nPrintNum);
	void SetIP_ZPL_Printer(int nPrintNum, DWORD dwAddress);
	const UINT GetPort_ZPL_Printer(int nPrintNum) const;
	void SetPort_ZPL_Printer(int nPrintNum, UINT nPort);

	BOOL Load();
	BOOL Save();

	CString GetExecuteDirectory(); // 현재 실행화일 경로 가져오자.

private:
	// MMI Group Num
	int			m_nGroupNumber;			// 현재 제어중인 그룹
	int			m_nLogicalStationNumber_G1;
	int			m_nLogicalStationNumber_G2;
	int			m_nLogicalStationNumber_G3;

	// ZPL Printer #1 IP
	IPv4Info	m_ZPLPrinter01_IP;
	UINT		m_ZPLPrinter01_PORT;
	// ZPL Printer #2 IP
	IPv4Info	m_ZPLPrinter02_IP;
	UINT		m_ZPLPrinter02_PORT;
	// ZPL Printer #3 IP
	IPv4Info	m_ZPLPrinter03_IP;
	UINT		m_ZPLPrinter03_PORT;
	// ZPL Printer #4 IP
	IPv4Info	m_ZPLPrinter04_IP;
	UINT		m_ZPLPrinter04_PORT;
	// ZPL Printer #5 IP
	IPv4Info	m_ZPLPrinter05_IP;
	UINT		m_ZPLPrinter05_PORT;

	static CAppSetting* m_pThis;
	CAppSetting();
	~CAppSetting();
};

#endif // !defined(AFX_APPSETTING_H__64F2F640_D51D_4952_A16E_FEA93A687DC1__INCLUDED_)
