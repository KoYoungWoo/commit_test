// ListenSocket.cpp : implementation file
//

#include "stdafx.h"
#include "AMKOR_MMI.h"
#include "AMKOR_MMIDlg.h"
#include "ListenSocket.h"
#include "ClientSocket.h"

// CListenSocket

CListenSocket::CListenSocket()
{
	m_bDestroy = FALSE;
}

CListenSocket::~CListenSocket()
{
}

// CListenSocket member functions
void CListenSocket::OnAccept(int nErrorCode)
{
	CClientSocket* pClient = new CClientSocket;

	if(Accept(*pClient))
	{
		pClient->SetListenSocket(this);
		m_ptrClientSocketList.AddTail(pClient);
		// 접속 여부 확인
		CString strIP;
		UINT nPort =0;
		CAMKOR_MMIDlg* pMain = (CAMKOR_MMIDlg*)AfxGetMainWnd();
		pClient->GetPeerName(strIP,nPort);
		if(strIP == _T("192.168.0.200"))
		{
			CDataManager::Instance()->SetConnect(CONN_VISION, TRUE);
			// LOG 남기기
			GetLog()->Debug( _T("VISION CONNECT OK.....") );
			CLogList::Instance()->Add_LogData(eLog_DEBUG, _T("VISION CONNECT OK.....") );

			// 접속시 타임 싱크
			//CString strTimeSinc;
			//strTimeSinc.Format(_T("01TS0024|%s|"), pMain->GetCurrentTime());
			//GetLog()->DebugT((LPCTSTR)strTimeSinc);
			//pMain->m_ListenSocket.SendData(pMain->m_dlg_GemScreen.m_strVision01IP, strTimeSinc);
		}
		else if(strIP == _T("127.0.0.1"))
		{
			CDataManager::Instance()->SetConnect(CONN_HMS_G1, TRUE);
			// LOG 남기기
			GetLog()->Debug( _T("HMS G1 CONNECT OK.....") );
			CLogList::Instance()->Add_LogData(eLog_DEBUG, _T("HMS G1 CONNECT OK.....") );
		}
		else if(strIP == _T("192.168.0.92"))
		{
			CDataManager::Instance()->SetConnect(CONN_HMS_G2, TRUE);
			// LOG 남기기
			GetLog()->Debug( _T("HMS G2 CONNECT OK.....") );
			CLogList::Instance()->Add_LogData(eLog_DEBUG, _T("HMS G2 CONNECT OK.....") );
		}
		pMain->SendMessage(WM_UPDATE_UI_CONN, 0, 0);
	}
	else
	{
		delete pClient;

		//To Do - 오류
		//AfxMessageBox(_T("ERROR: Failed to accept new client!"));
		//GetLog()->Debug(_T("CListenSocket::OnAccept(...) - Failed to accept new client nErrorCode = %d"),nErrorCode);
	}

	CAsyncSocket::OnAccept(nErrorCode);
}

void CListenSocket::CloseClientSocket(CSocket* pClient)
{
	if(pClient == NULL) return;

	POSITION pos;
	pos = m_ptrClientSocketList.Find(pClient);
	if(pos != NULL)
	{
		// 접속 여부 확인
		CString strIP;
		UINT nPort =0;
		CAMKOR_MMIDlg* pMain = (CAMKOR_MMIDlg*)AfxGetMainWnd();
		pClient->GetPeerName(strIP,nPort);
		if(strIP == _T("192.168.0.200"))
		{
			CDataManager::Instance()->SetConnect(CONN_VISION, FALSE);
			// LOG 남기기
			GetLog()->Debug( _T("VISION DISCONNECT.....") );
			CLogList::Instance()->Add_LogData(eLog_DEBUG, _T("VISION DISCONNECT.....") );
		}
		else if(strIP == _T("127.0.0.1"))
		{
			CDataManager::Instance()->SetConnect(CONN_HMS_G1, FALSE);
			// LOG 남기기
			GetLog()->Debug( _T("HMS G1 DISCONNECT.....") );
			CLogList::Instance()->Add_LogData(eLog_DEBUG, _T("HMS G1 DISCONNECT.....") );
		}
		else if(strIP == _T("192.168.0.92"))
		{
			CDataManager::Instance()->SetConnect(CONN_HMS_G2, FALSE);
			// LOG 남기기
			GetLog()->Debug( _T("HMS G2 DISCONNECT.....") );
			CLogList::Instance()->Add_LogData(eLog_DEBUG, _T("HMS G2 DISCONNECT.....") );
		}
		pMain->SendMessage(WM_UPDATE_UI_CONN, 0, 0);

		pClient->ShutDown();
		pClient->Close();

		m_ptrClientSocketList.RemoveAt(pos);
		delete pClient;
	}
}

void CListenSocket::SendDataAll(TCHAR* pszMessage)
{
	POSITION pos;
	pos = m_ptrClientSocketList.GetHeadPosition();

	CClientSocket* pClient = NULL;
	while(pos != NULL)
	{
		pClient = (CClientSocket*)m_ptrClientSocketList.GetNext(pos);
		if(pClient != NULL)
		{
			pClient->Send(pszMessage, lstrlen(pszMessage) * 2);
		}
	}
}

int CListenSocket::SendData(CString strIP, CString strSendData)
{
	POSITION pos;
	pos = m_ptrClientSocketList.GetHeadPosition();
	CClientSocket* pClient = NULL;
	CString strIPtemp;
	UINT nPort;

	CString strTmp;

	CHAR chSendPacket[1024*10];
	memset(chSendPacket,0x00,sizeof(chSendPacket));

	int nError = 0;
	
	((CAMKOR_MMIApp*)AfxGetApp())->Unicode2MBCS(strSendData.GetBuffer(),chSendPacket);
	strSendData.ReleaseBuffer();

	while(pos != NULL)
	{
		pClient = (CClientSocket*)m_ptrClientSocketList.GetNext(pos);
		if(pClient != NULL)
		{
			pClient->GetPeerName(strIPtemp,nPort);

			if(strIP == strIPtemp)
			{
				nError = pClient->Send((LPVOID)chSendPacket, strlen(chSendPacket) + 1);
			}		
		}
	}

	return nError;
}

