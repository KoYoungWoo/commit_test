#pragma once
#include "afxwin.h"
#include "UIExt/CxStatic.h"
#include "UIExt/IconButton.h"
#include "UIExt/XPGroupBox.h"
#include "exgem3ctrl1.h"

// CDlg_Gem 대화 상자입니다.

class CDlg_Gem : public CDialogEx
{
	DECLARE_DYNAMIC(CDlg_Gem)

public:
	CDlg_Gem(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CDlg_Gem();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DLG_GEM };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);

	void Cal_Area();
	void Cal_CtrlArea();
	void Cal_CtrlArea_Left();
	void Cal_CtrlArea_Right();

	CRect m_rcLeft;
	CRect m_rcRight;

	BOOL m_bCtrl_FirstLoad;

	CExgem3ctrl1 m_XGem300;
	CString m_strEditXGem;

	CIconButton m_btn_1;
	CIconButton m_btn_2;
	CIconButton m_btn_3;
	CIconButton m_btn_4;
	CIconButton m_btn_5;
	CIconButton m_btn_6;
	CIconButton m_btn_7;
	CIconButton m_btn_8;
	CIconButton m_btn_9;
	CIconButton m_btn_10;
	CXPGroupBox m_gb_1;
	CXPGroupBox m_gb_2;
	CXPGroupBox m_gb_3;
	CXPGroupBox m_gb_4;
	CxStatic m_st_1;
	CxStatic m_st_2;
	CxStatic m_st_3;
	CxStatic m_st_4;
	CxStatic m_st_5;
	CxStatic m_st_6;
	CxStatic m_st_7;
	CxStatic m_st_8;
	CComboBox m_cbo_1; //Event Combo
	CComboBox m_cbo_2; //ALID Combo
	CComboBox m_cbo_3; //RECIPE ID Combo
	CComboBox m_cbo_4; //Type Combo
	CComboBox m_cbo_5; //ZPL Combo
	CEdit m_edit_1;
	CEdit m_edit_2;
	CEdit m_edit_3;
	CEdit m_edit_4;
	CEdit m_edit_5;
	CEdit m_edit_6;
	CListBox m_list_1;

	DECLARE_EVENTSINK_MAP()
	void eXGEMStateEventExgem3ctrl1(long nState);
	void eGEMCommStateChangedExgem3ctrl1(long nState);
	void eGEMControlStateChangedExgem3ctrl1(long nState);
	void eGEMTerminalMessageExgem3ctrl1(long nTid, LPCTSTR sMsg);
	void eGEMTerminalMultiMessageExgem3ctrl1(long nTid, long nCount, BSTR* psMsg);
	void eGEMReqGetDateTimeExgem3ctrl1(long nMsgId);
	void eGEMReqDateTimeExgem3ctrl1(long nMsgId, LPCTSTR sSystemTime);
	void eGEMReqPPListExgem3ctrl1(long nMsgId);

	afx_msg void OnBnClickedBtnGem1();
	afx_msg void OnBnClickedBtnGem2();
	afx_msg void OnBnClickedBtnGem3();
	afx_msg void OnBnClickedBtnGem4();
	afx_msg void OnBnClickedBtnGem5();
	afx_msg void OnBnClickedBtnGem6();
	afx_msg void OnBnClickedBtnGem7();
	afx_msg void OnBnClickedBtnGem8();
	afx_msg void OnBnClickedBtnGem9();
	afx_msg void OnBnClickedBtnGem10();
	afx_msg void OnCbnCloseupCboGem1();
public:
	void	StartXGem300();
	void	StopXGem300();
	void	AddGemLog(CString strEvent);
	void	AddZebraLog(CString strEvent);
	int		m_nInputCnt;
	CString m_strSVID;
	CString m_strEventSVIDs;
	CString m_strInput;
	CString m_strEditZebra;
	void eGEMReqRemoteCommandExgem3ctrl1(long nMsgId, LPCTSTR sRcmd, long nCount, BSTR* psCpNames, BSTR* psCpVals);

	//ERS(Event Report Send) Functions
	BOOL	ReadyToLoadReq(CString strPort = L"LOAD"); //(1000)
	BOOL	MaterialStatRpt(CString strTrayStacker, CString strEndCap, CString strBanding, CString strHIC, CString strMBB, 
							CString Box, CString strSilicaGel, CString strLabel, CString strRibbon, CString strBubbleSheet); //(1100)
	BOOL	RFID_FullRpt(CString strRfidFullStatus); //(1200)
	BOOL	ReadyToLoadBcrReq(CString strBarcode); //(1300)
	BOOL	LotStartReq(CString strBarcode,CString strLotID,int nDeviceQty,int nTrayQty, int nBundleTrayQty); //(1400)
	BOOL	BoxProcResultInfoRpt(CString strLotID, int nLotQty, CString strOperCode, CString strInnerBoxID, int nBoxDeviceQty, 
								CString strPartial, CString strFlag); //(1500)
	BOOL	BoxEndReq(CString strBarcode, CString strLotID, int nLotQty, CString strInnerBoxID, int nBoxDeviceQty, 
					  CString strPartial); //(1600)
	BOOL	ReadyToUnloadReq(CString strBarcode, CString strLotID, int nLotQty); //(1700)
	BOOL	LotEndReq(CString strBarcode, CString strLotID, int nLotQty, int nInnerBoxTotalQty); //(1800)
	BOOL	ZebraCodeReq(CString strLotID); //(1900)
	BOOL	EquipStatRpt(CString strEqStatus,CString strLotID = L""); //(901,902,903) Equipment Status - RUN/STOP/IDLE
	BOOL	ControlStateRpt(); //(1,2,3) Control State (호출 불필요-함수정의 주석참조)- Offline/Local/Remote - XGem300 Driver 자동. EQ "OR" default
	BOOL	RecipeSelectedRpt(); //(8) To-Do

	//ARS(Alarm Report Send)
	BOOL	AlarmReportSend(long nALID,long nState); //

};
