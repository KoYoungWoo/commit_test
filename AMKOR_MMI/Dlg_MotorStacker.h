#pragma once
#include "afxwin.h"
#include "UIExt/CxStatic.h"
#include "UIExt/IconButton.h"
#include "UIExt/XPGroupBox.h"
#include "GC/gridctrl.h"

// CDlg_MotorStacker 대화 상자입니다.

class CDlg_MotorStacker : public CDialogEx
{
	DECLARE_DYNAMIC(CDlg_MotorStacker)

public:
	CDlg_MotorStacker(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CDlg_MotorStacker();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DLG_MOTORSTACKER };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);

	void Cal_Area();
	void Cal_CtrlArea();
	void Cal_CtrlArea_Up();
	void Cal_CtrlArea_Down();

	bool Init_GC_1(int nRows, int nCols);
	afx_msg void OnGridClick_GC_1(NMHDR *pNotifyStruct, LRESULT* pResult);
	afx_msg void OnEndLabel_GC_1(NMHDR *pNotifyStruct, LRESULT* pResult);

	bool Init_GC_2(int nRows, int nCols);
	afx_msg void OnGridClick_GC_2(NMHDR *pNotifyStruct, LRESULT* pResult);
	afx_msg void OnEndLabel_GC_2(NMHDR *pNotifyStruct, LRESULT* pResult);

	bool Init_GC_3(int nRows, int nCols);
	afx_msg void OnGridClick_GC_3(NMHDR *pNotifyStruct, LRESULT* pResult);
	afx_msg void OnEndLabel_GC_3(NMHDR *pNotifyStruct, LRESULT* pResult);

	bool Init_GC_4(int nRows, int nCols);
	afx_msg void OnGridClick_GC_4(NMHDR *pNotifyStruct, LRESULT* pResult);
	afx_msg void OnEndLabel_GC_4(NMHDR *pNotifyStruct, LRESULT* pResult);

	void Update_PLC_Value();
	void Update_PLC_Value_X();
	void Update_PLC_Value_Y();
	void Update_PLC_Value_Z();
	void Update_PLC_Value_T();

	CRect m_rcUp;
	CRect m_rcDown;

	BOOL m_bCtrl_FirstLoad;

	// Up Area
	CxStatic m_st_1;
	CxStatic m_st_2;
	CxStatic m_st_3;
	CxStatic m_st_4;
	CxStatic m_st_5;
	CxStatic m_st_6;
	CxStatic m_st_7;
	CxStatic m_st_8;
	CxStatic m_st_9;
	CxStatic m_st_10;
	CxStatic m_st_11;
	CxStatic m_st_12;
	CxStatic m_st_13;
	CxStatic m_st_14;
	CxStatic m_st_15;
	CxStatic m_st_16;
	CxStatic m_st_17;
	CxStatic m_st_18;
	CxStatic m_st_19;
	CxStatic m_st_20;
	CxStatic m_st_21;
	CxStatic m_st_22;
	CxStatic m_st_23;
	CxStatic m_st_24;
	CIconButton m_btn_1;
	CIconButton m_btn_2;
	CIconButton m_btn_3;
	CIconButton m_btn_4;
	CIconButton m_btn_5;
	CIconButton m_btn_6;
	CIconButton m_btn_7;
	CIconButton m_btn_8;
	CIconButton m_btn_9;
	CIconButton m_btn_10;
	CIconButton m_btn_11;
	CIconButton m_btn_12;
	CIconButton m_btn_13;
	// Down Area
	CXPGroupBox m_gb_1;
	CXPGroupBox m_gb_2;
	CXPGroupBox m_gb_3;
	CXPGroupBox m_gb_4;
	CGridCtrl m_GC_1;
	CGridCtrl m_GC_2;
	CGridCtrl m_GC_3;
	CGridCtrl m_GC_4;

	afx_msg void OnBnClickedBtnMs1();
	afx_msg void OnBnClickedBtnMs2();
	afx_msg void OnBnClickedBtnMs3();
	afx_msg void OnBnClickedBtnMs4();
	afx_msg void OnBnClickedBtnMs5();
	afx_msg void OnBnClickedBtnMs6();
	afx_msg void OnBnClickedBtnMs7();
	afx_msg void OnBnClickedBtnMs8();
	afx_msg void OnBnClickedBtnMs9();
	afx_msg void OnBnClickedBtnMs10();
	afx_msg void OnBnClickedBtnMs11();
	afx_msg void OnBnClickedBtnMs12();
	afx_msg void OnBnClickedBtnMs13();

	virtual BOOL OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult);
};
